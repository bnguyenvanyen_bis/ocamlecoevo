module U = Util
module F = U.Float

module Color =
  struct
    type t = unit

    let compare () () = 0
    let equal () () = true
    let to_string () = "()"
    let of_string =
      function
      | "()" ->
          Some ()
      | _ ->
          None
  end


module Point = Sim.Prm.Compare_time (Sim.Prm.Point (Color))


module P = Sim.Prm.Make (Point)


module SF = Simfit.Prm.Make (P)


let point ~k ~t ~u =
  Sim.Prm.{ c = () ; k ; t ; u }


let prior =
  let bounds = P.{
    origin = point ~k:0 ~t:F.zero ~u:F.zero ;
    vector = point ~k:1 ~t:F.one ~u:F.one ;
  }
  in
  let draw rng =
    let uksl = P.UKsl.rand_draw ~rng () bounds in
    let lambda = F.one in
    (uksl, lambda)
  in
  let log_dens_proba _ =
    [0.]
  in
  Fit.Dist.Base { draw ; log_dens_proba }


let likelihood = Fit.Dist.Flat


let propose =
  let draw rng (uksl, lambda) =
    let dlambda = F.of_float (U.rand_normal ~rng 0. (F.Pos.of_float 0.01)) in
    let lambda' =
      lambda
      |> F.add dlambda
      |> F.positive_part
    in
    let uksl' = SF.adjust_density_var_slice ~rng dlambda uksl in
    (uksl', lambda')
  in
  let log_pd_ratio x x' =
    [F.to_float (SF.log_pd_ratio_var_slice x x')]
  in
  Fit.Propose.Base { draw ; log_pd_ratio }


let run seed n_iter =
  let init, sample = Fit.Mcmc.integrate prior propose likelihood in
  let extract (uksl, lambda) =
    function
    | "lambda" ->
        F.to_string lambda
    | "nu" ->
        F.to_string (P.UKsl.density uksl)
    | _ ->
        invalid_arg "unrecognized column"
  in
  let columns = Fit.Csv.columns_sample_move in
  let extract = Fit.Csv.extract_sample_move ~extract in
  let n_burn = 0 in
  let n_thin = 1 in
  let chan = Util.Csv.Channel.Out.standard () in
  let output = Fit.Csv.convert ~n_burn ~n_thin ~chan ~columns ~extract in
  let x = Fit.Dist.draw_from (U.rng seed) prior in
  Fit.Mcmc.posterior ~output ?seed ~init ~sample ~n_iter x
