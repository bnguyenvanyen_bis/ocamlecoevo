
module L = BatList

module U = Util
module F = U.Float
module I = U.Int

module Fm = Sim.Prm.Fm


module type PRM =
  sig
    module P : Sim.Sig.PRM

    type intensities = U._float P.Cm.t Fm.t
    type t = P.t * intensities

    val prior :
      colors: P.color list ->
      vectors: (U.anyposint * P.point) list ->
      tf: 'a U.anypos ->
      h: 'b U.anypos ->
        P.t Fit.Dist.t

    val prior_intensities :
      colors: P.color list ->
      vectors: (U.anyposint * P.point) list ->
      tf: 'a U.anypos ->
      h: 'b U.anypos ->
        t Fit.Dist.t

    val prior_merge :
      colors: P.color list ->
      vectors: (U.anyposint * P.point) list ->
      vectors': (U.anyposint * P.point) list ->
      tf: 'a U.anypos ->
      h: 'b U.anypos ->
      P.Ps.t ->
        P.t Fit.Dist.t

    val propose_poisson_full :
      ?filter:(P.color -> bool) ->
      nredraws: int ->
        P.t Fit.Propose.t

    val propose_poisson_intensities :
      ?filter:(P.color -> bool) ->
      nredraws: int ->
        t Fit.Propose.t

    val jumps :
      color_groups: P.color list list ->
      ntranges: U.anyposint ->
      ntslices: U.anyposint ->
      float ->
        t Fit.Param.DJ.t list

    val log_pd_ratio : t -> t -> float list
  end
