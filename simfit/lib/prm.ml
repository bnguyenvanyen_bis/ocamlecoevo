module L = BatList

module U = Util
module F = U.Float
module I = U.Int

module Fm = Sim.Prm.Fm
module FIm = Sim.Prm.FIm


module Make (P : Sim.Sig.PRM) =
  struct
    module P = P

    module Cm = P.Cm

    module Pre = U.Precompute ( )

    type intensities = U._float Cm.t Fm.t

    type t = P.t * intensities


    let ntslices ~h ~tf =
      I.Pos.of_int (int_of_float (F.to_float (F.div tf h)))

    let prior ~colors ~vectors ~tf ~h =
      let time_range = (F.zero, tf) in
      let ntslices = ntslices ~h ~tf in
      let draw rng =
        let rngm = L.fold_left (fun m c ->
            P.Cm.add c (U.rand_rng rng) m
          ) P.Cm.empty colors
        in
        P.rand_draw ~rngm ~time_range ~ntslices ~vectors
      in
      let log_dens_proba nu =
        [F.to_float (P.logp nu)]
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    let one_intensities nu : intensities =
      P.fold_slices (fun tsl intensities ->
        let t = P.Tsl.key tsl in
        let lbds = P.Tsl.fold_slices (fun csl lbds ->
            Cm.add (P.Csl.color csl) (F.narrow F.one) lbds
          ) tsl Cm.empty
        in
        Fm.add t lbds intensities
      ) nu Fm.empty

    let prior_intensities ~colors ~vectors ~tf ~h =
      let time_range = (F.zero, tf) in
      let ntslices = ntslices ~h ~tf in
      let draw rng =
        let rngm = L.fold_left (fun m c ->
          P.Cm.add c (U.rand_rng rng) m
        ) P.Cm.empty colors
        in
        let nu = P.rand_draw ~rngm ~time_range ~ntslices ~vectors in
        let intensities = one_intensities nu in
        (nu, intensities)
      in
      let log_dens_proba (nu, _) =
        [F.to_float (P.logp nu)]
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    let prior_merge ~colors ~vectors ~vectors' ~tf ~h points =
      let time_range = (F.zero, tf) in
      let ntslices = ntslices ~h ~tf in
      let draw rng =
        let rngm = L.fold_left (fun m c ->
            P.Cm.add c (U.rand_rng rng) m
          ) P.Cm.empty colors
        in
        let prm = P.rand_draw ~rngm ~time_range ~ntslices ~vectors in
        (* here we would like logp to be 0. for prm' *)
        let prm' = P.create ~time_range ~ntslices ~vectors:vectors' points in
        P.merge prm prm'
      in
      let log_dens_proba m =
        let logp = F.to_float (P.logp m) in
        [logp]
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    (* proposal *)

    let uksl_redraw_partial ~rng uksl = P.(
      let v = UKsl.volume uksl in
      let k = I.to_int (UKsl.count uksl) in
      let k' = I.to_int (U.rand_poisson ~rng v) in
      UKsl.rand_move ~rng (k' - k) uksl
    )
   
    let uksl_redraw_full ~rng uksl = P.(
      UKsl.rand_draw ~rng (UKsl.color uksl) (UKsl.bounds uksl)
    )
    
    let map_u_slices f prm tsl c k =
      P.map_uk_slices (fun uksl ->
          if P.UKsl.number uksl = k then
            f uksl
          else
            uksl
        ) prm tsl c
    
    
    let redraw_once ~rng draw filter m = P.(
      (* choose a c slice that passes filter *)
      let t, c, tsl, csl = U.insist ~rng (fun ~rng ->
          (* t slices *)
          let tsl = rand_choose ~rng m in
          (* c slices *)
          let csl = Tsl.rand_choose ~rng tsl in
          let t = Tsl.key tsl in
          let c = Csl.color csl in
          if filter t c then
            Some (t, c, tsl, csl)
          else
            None
        )
      in
      (* choose a number *)
      (* number of k slices *)
      let nk = I.Pos.succ (I.Pos.of_int (Csl.kmax csl)) in
      let k = I.to_int (U.rand_int ~rng nk) in
      (* redraw all of its u slices *)
      let m' = map_u_slices (draw ~rng) m tsl c k in
      ((t, c), m')
    )
   
    let redraw_distinct ~rng draw nredraws filter m =
      let _, m' = U.int_fold ~f:(fun (fil', m') _ ->
          let (t, c), m'' = redraw_once ~rng draw fil' m' in
          let fil'' = (fun t' c' ->
              if F.Op.(t = t') && (P.Point.Color.equal c c') then
                false
              else
                fil' t c
            )
          in (fil'', m'')
        ) ~x:((fun _t c -> filter c), m) ~n:nredraws
      in m'

    let propose_poisson_full ?(filter=(fun _ -> true)) nredraws =
      let draw rng nu =
        redraw_distinct ~rng uksl_redraw_full nredraws filter nu
      in
      let log_pd_ratio nu nu' =
        (* to make it a bit simpler *)
        [F.to_float (P.logp nu) -. F.to_float (P.logp nu')]
      in
      Fit.Propose.Base { draw ; log_pd_ratio }

    let propose_poisson_intensities ?(filter=(fun _ -> true)) nredraws =
      let draw rng (nu, intensities) =
        let nu' = redraw_distinct ~rng uksl_redraw_full nredraws filter nu in
        (nu', intensities)
      in
      let log_pd_ratio (nu, _) (nu', _) =
        [F.to_float (P.logp nu) -. F.to_float (P.logp nu')]
      in
      Fit.Propose.Base { draw ; log_pd_ratio }

    (* compute the density in points of the discrete measure nu *)
    let density ~time_range ~color nu =
      let jt0, djt = time_range in
      let _, n_tslices = P.slicing nu in
      (* -1 : we starf from 0 *)
      let jtf = I.Pos.of_anyint I.Op.(jt0 + djt - I.one) in
      assert I.Op.(jtf < n_tslices) ;
      let _, dens = P.fold_slices (fun tsl (j, ds) ->
          if I.Op.((j < jt0) || (jtf < j)) then
            (I.Pos.succ j, ds)
          else
            let csl = P.Tsl.find tsl color in
            let c = P.Csl.count csl in
            let v = P.Csl.volume csl in
            let d = F.Pos.div (I.Pos.to_float c) v in
            (I.Pos.succ j, d :: ds)
        ) nu (I.zero, [])
      in
      assert (L.length dens = I.to_int djt) ;
      F.Pos.div (L.fold_left F.Pos.add F.zero dens) (I.Pos.to_float djt)

    let adjust_density_var_slice ~rng dlambda uksl =
      let vol = P.UKsl.volume uksl in
      let k = P.UKsl.count uksl in
      let k' =
        k
        |> I.Pos.to_float
        |> (fun x -> F.Op.(x + dlambda * vol))
        |> F.positive_part
        |> U.rand_poisson ~rng
      in
      let dk = I.to_int k' - I.to_int k in
      P.UKsl.rand_move ~rng dk uksl

    let adjust_density_delta_slice ~rng dlambda uksl =
      let vol = P.UKsl.volume uksl in
      let dk =
        match F.identify (F.mul vol dlambda) with
        | F.Zero _ ->
            I.zero
        | F.Neg x ->
            I.neg (U.rand_poisson ~rng (F.Neg.neg x))
        | F.Proba x ->
            U.rand_poisson ~rng x
        | F.Pos x ->
            U.rand_poisson ~rng x
      in
      match P.UKsl.rand_move ~rng (I.to_int dk) uksl with
      | res ->
          res
      | exception (Invalid_argument s | Failure s) ->
          raise (Fit.Sig.Draw_error s)

    let adjust_density ~rng ~slices dlambda (nu, intensities) =
      let (t0, dt), n_tslices = P.slicing nu in
      let delta_t = F.Pos.div dt (I.Pos.to_float n_tslices) in
      let color, jt = U.rand_unif_choose ~rng slices in
      (* is that the right one, or should it be the next ? *)
      let t = F.Pos.Op.(t0 + delta_t * (I.Pos.to_float jt + F.one)) in
      let tsl = P.t_slice_at nu t in
      let csl = P.Tsl.find tsl color in
      let tins = Fm.find t intensities in
      let lambda = Cm.find color tins in
      (* FIXME where do we catch the failure ? *)
      let lambda' = F.Op.(lambda + dlambda) in
      (* replaced update by add because weirdly it raised ? *)
      let tins' = Cm.add color lambda' tins in
      (* same here, using add instead of update *)
      let intensities' = Fm.add t tins' intensities in
      let csl' =
        P.Csl.map_slices (adjust_density_delta_slice ~rng dlambda) csl
      in
      (* make a new nu *)
      let tsl' = P.Tsl.with_slice csl' tsl in
      let nu' = P.with_slice tsl' nu in
      (nu', intensities')

    let jump ~time_range j colors =
      let jt0, djt = time_range in
      let slices = L.fold_left (fun sls color ->
          let csls = I.Pos.list_init ~n:djt ~f:(fun i ->
            (color, I.Pos.add jt0 i)
          ) in
          sls @ csls
        ) [] colors
      in
      let draw rng dx nui =
        adjust_density ~rng ~slices (F.of_float dx) nui
      in Fit.Param.DJ.{ draw ; jump = j }

    let jumps ~color_groups ~ntranges ~ntslices j =
      (* number of slices by sub range *)
      let djt = I.Pos.Op.(ntslices / ntranges) in
      (* remaining slices to put in the last range *)
      let rem = I.Pos.Op.(ntslices mod ntranges) in
      (* from the first time range to the penultimate time range *)
      let jumps = U.Int.Pos.fold ~f:(fun js k ->
          let jt0 = I.Pos.mul djt (I.Pos.of_anyint (I.pred k)) in
          let time_range = (jt0, djt) in
          let color_jumps = L.map (jump ~time_range j) color_groups in
          color_jumps @ js
        ) ~x:[] ~n:ntranges
      in
      (* put the last jump in its place *)
      if I.Op.(rem <> I.zero) then
        let jt0_last = I.Pos.of_anyint I.Op.(ntslices - rem) in
        let time_range = (jt0_last, rem) in
        let last_jumps = L.map (jump ~time_range j) color_groups in
        jumps @ last_jumps
      else
        jumps

    let log_pd_ratio_var_slice (uksl, lambda) (uksl', lambda') =
      let vol = P.UKsl.volume uksl in
      let k = I.to_float (P.UKsl.count uksl) in
      let k' = I.to_float (P.UKsl.count uksl') in
      let dlambda = F.Op.(lambda' - lambda) in
      let true_lambda = F.positive_part F.Op.(k' / vol - dlambda) in
      let true_lambda' = F.positive_part F.Op.(k / vol + dlambda) in
      (* need to be careful about the case k(') = 0 and true_lambda(') = 0 *)
      let klbd =
        if F.Op.(k = F.zero) then
          F.zero
        else
          F.Op.(k * F.Pos.log true_lambda)
      in
      let klbd' =
        if F.Op.(k' = F.zero) then
          F.zero
        else
          F.Op.(k' * F.Pos.log true_lambda')
      in F.Op.(
          klbd - klbd'
        + vol * (true_lambda' - true_lambda)
      )

    let log_pd_ratio_delta_slice (uksl, _) (uksl', _) =
      let vol = P.UKsl.volume uksl in
      let k = P.UKsl.count uksl in
      let k' = P.UKsl.count uksl' in
      let dk = I.Op.(k' - k) in
      let dkv =
        if I.Op.(dk = I.zero) then
          F.zero
        else
          F.Op.(I.to_float dk * F.Pos.log vol)
      in
      let logfact = Pre.Int.Pos.logfact in
      F.Op.(
        logfact k - logfact k' + dkv
      )

    let log_pd_ratio (nu, ins) (nu', ins') =
      let logp = Fm.fold (fun t tins logp ->
          let tins' = Fm.find t ins' in
          Cm.fold (fun col lambda logp ->
            let lambda' = Cm.find col tins' in
            if F.Op.(lambda <> lambda') then
              (* find the relevant slices *)
              let csl = P.Tsl.find (P.t_slice_at nu t) col in
              let csl' = P.Tsl.find (P.t_slice_at nu' t) col in
              (* fold over uksls *)
              P.Csl.fold_rvl_slices (fun uksl logp ->
                let uksl' = P.Csl.find csl' (P.UKsl.key uksl) in
                let logr_slice =
                  log_pd_ratio_delta_slice (uksl, lambda) (uksl', lambda')
                in
                F.add logp logr_slice
              ) csl logp
            else
              logp
          ) tins logp
        ) ins F.zero
      in [F.to_float F.Op.(P.logp nu' - P.logp nu + logp)]
  end
