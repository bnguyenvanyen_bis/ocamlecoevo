(* What should we do again here ? *)

open Sig


module Prm =
  struct
    module Make (S : Sim.Sig.PRM) =
      struct
        module Points = U.Csv.Make (struct
          type elt = int * S.Points.elt
          type t = int * S.Points.t

          let fold f (k, nu) x =
            S.Points.fold (fun pt x' -> f (k, pt) x') nu x

          let unfold f x =
            let nu = S.Points.unfold (fun x' ->
                match f x' with
                | None ->
                    None
                | Some (x'', (_, pt)) ->
                    Some (x'', pt)
              ) x
            in
            match f x with
            | None ->
                (0, nu)
            | Some (_, (k, _)) ->
                (k, nu)
        end)

        module Grid = U.Csv.Make (struct
          type elt = int * S.Slices.elt
          type t = int * S.Slices.t

          let fold f (k, nu) x =
            S.Slices.fold (fun uksl x' -> f (k, uksl) x') nu x

          let unfold f x =
            let nu = S.Slices.unfold (fun x' ->
                match f x' with
                | None ->
                    None
                | Some (x'', (_, uksl)) ->
                    Some (x'', uksl)
              ) x
            in
            match f x with
            | None ->
                (0, nu)
            | Some (_, (k, _)) ->
                (k, nu)
        end)

        let convert_prm_separate
            ?(n_burn=0) ?(n_thin=1)
            ~output_points ~output_grid ~to_prm path =
          let root k =
            Printf.sprintf "%s.%i.prm" path k
          in
          let out_points =
            let columns = S.Point.columns in
            let extract (_, pt) =
              S.Point.extract pt
            in
            let f = U.Csv.Row.unfold ~columns extract in
            (fun chan k nu -> Points.write ~f ~chan (k, nu))
          in
          let out_grid =
            let columns = S.columns_grid in
            let extract (_, uksl) =
              S.extract_grid uksl
            in
            let f = U.Csv.Row.unfold ~columns extract in
            (fun chan k nu -> Grid.write ~f ~chan (k, nu))
          in
          let output k x =
            match to_prm (Fit.Mcmc.return_to_sample x) with
            | None ->
                ()
            | Some nu ->
                if output_points then begin
                  let chan = U.Csv.open_out (root k ^ ".points.csv") in
                  out_points chan k nu ;
                  U.Csv.close_out chan
                end ;
                if output_grid then begin
                  let chan = U.Csv.open_out (root k ^ ".grid.csv") in
                  out_grid chan k nu ;
                  U.Csv.close_out chan
                end
          in
          let start k0 x0 =
            output k0 x0
          in
          let out k x =
            if (k >= n_burn) && (k mod n_thin = 0) then
            output k x
          in
          let return k x =
            (k, x)
          in
          U.Out.{ start ; out ; return }

        let convert_prm_append
            ?(n_burn=0) ?(n_thin=1) ~output_points ~output_grid ~to_prm path =
          let root =
            Printf.sprintf "%s.prm" path
          in
          let out_points =
            let columns = "k" :: S.Point.columns in
            let extract (k, pt) =
              function
              | "k" ->
                  string_of_int k
              | s ->
                  S.Point.extract pt s
            in
            let f = U.Csv.Row.unfold ~columns extract in
            (fun chan k nu -> Points.write ~f ~chan (k, nu))
          in
          let out_grid =
            let columns = "k" :: S.columns_grid in
            let extract (k, uksl) =
              function
              | "k" ->
                  string_of_int k
              | s ->
                  S.extract_grid uksl s
            in
            let f = U.Csv.Row.unfold ~columns extract in
            (fun chan k nu -> Grid.write ~f ~chan (k, nu))
          in
          let output ~chan_pts ~chan_grid k x =
            match to_prm (Fit.Mcmc.return_to_sample x) with
            | None ->
                ()
            | Some nu ->
                if output_points then begin
                  out_points chan_pts k nu
                end ;
                if output_grid then begin
                  out_grid chan_grid k nu
                end
          in
          let chan_r = ref None in
          let start k0 x0 =
            let chan_pts = U.Csv.open_out (root ^ ".points.csv") in
            let chan_grid = U.Csv.open_out (root ^ ".grid.csv") in
            chan_r := Some (chan_pts, chan_grid) ;
            output ~chan_pts ~chan_grid k0 x0
          in
          let out k x =
            if (k >= n_burn) && (k mod n_thin = 0) then begin
              match !chan_r with
              | None ->
                  failwith "Output not opened"
              | Some (chan_pts, chan_grid) ->
                  output ~chan_pts ~chan_grid k x
            end
          in
          let return k x =
            match !chan_r with
            | None ->
                failwith "Output not opened"
            | Some (chan_pts, chan_grid) ->
                begin
                  U.Csv.close_out chan_pts ;
                  U.Csv.close_out chan_grid ;
                  chan_r := None
                end ;
            (k, x)
          in
          U.Out.{ start ; out ; return }

        let convert_prm ?(append=false) =
          if append then
            convert_prm_append
          else
            convert_prm_separate
      end
  end


(* We will also have to functorize this part ? *)
let convert_traj ?(n_burn=0) ?(n_thin=1) ~sim_traj path =
  let output k x =
    let theta = Fit.Mcmc.return_to_sample x in
    let chan = open_out (Printf.sprintf "%s.%i.traj.csv" path k) in
    sim_traj ~chan theta ;
    close_out chan
  in
  let start k0 x0 =
    output k0 x0
  in
  let out k x =
    if (k >= n_burn) && (k mod n_thin = 0) then
      output k x
  in
  let return k x =
    (k, x)
  in
  Util.Out.{ start ; out ; return }


type 'a convert_prm = (
  ?append:bool ->
  ?n_burn:int ->
  ?n_thin:int ->
  string ->
    (int, 'a, int * 'a) U.Out.t
)


let convert_prm_traj ?traj_every ?prm_every ?n_burn ?append
    ~(convert_prm : 'a convert_prm) ~sim_traj path =
  let conv_prm =
    match prm_every with
    | Some n_thin ->
        convert_prm ?append ?n_burn ~n_thin path
    | None ->
        Util.Out.convert_null
  in
  let conv_traj =
    match traj_every with
    | Some n_thin ->
        convert_traj ?n_burn ~n_thin ~sim_traj path
    | None ->
        Util.Out.convert_null
  in Util.Out.combine (fun kres _ -> kres) conv_prm conv_traj
  

let convert_csv ?n_burn ?n_thin ~columns ~extract path =
  let chan = Util.Csv.open_out (Printf.sprintf "%s.theta.csv" path) in
  Fit.Csv.convert ?n_burn ?n_thin ~chan ~columns ~extract


let convert_chol ?n_burn ?chol_every path =
  match chol_every with
  | Some n_thin ->
      let chan = open_out (Printf.sprintf "%s.chol.csv" path) in
      Fit.Csv.convert_lower_triang ?n_burn ~n_thin chan
  | None ->
      Util.Out.convert_null


let convert_csv_traj
    ?n_burn ?n_thin
    ?traj_every
    ~columns ~extract
    ~sim_traj
    path =
  let conv_csv = convert_csv
    ?n_burn
    ?n_thin
    ~columns ~extract
    path
  in
  let conv_traj =
    match traj_every with
    | Some n_thin ->
        convert_traj ?n_burn ~n_thin ~sim_traj path
    | None ->
        Util.Out.convert_null
  in Util.Out.combine (fun kres _ -> kres) conv_csv conv_traj
    

let convert_csv_prm_traj
    ?n_burn ?n_thin
    ?prm_every ?traj_every
    ~columns ~extract
    ~convert_prm ~sim_traj
    path =
  let output_main =
    convert_csv ?n_burn ?n_thin ~columns ~extract path
  in
  let output_aux = convert_prm_traj
      ?n_burn 
      ?prm_every ?traj_every
      ~convert_prm ~sim_traj
      path
  in
  Util.Out.combine (fun kres _ -> kres) output_main output_aux


let convert_csv_ram
    ?n_burn ?n_thin
    ?chol_every
    ~columns ~extract
    path =
  let output_main =
    convert_csv ?n_burn ?n_thin ~columns ~extract path
  in
  let output_aux =
    convert_chol ?n_burn ?chol_every path
  in
  Util.Out.combine_tuple (fun (k, res) (_, chol) ->
      (k, res, chol)
    ) output_main output_aux


let convert_csv_traj_ram
    ?n_burn ?n_thin
    ?traj_every ?chol_every
    ~columns ~extract
    ~sim_traj
    path =
  let conv_csv_traj = convert_csv_traj
    ?n_burn ?n_thin
    ?traj_every
    ~columns ~extract
    ~sim_traj
    path
  in
  let conv_ram = convert_chol ?n_burn ?chol_every path in
  Util.Out.combine_tuple (fun (k, res) (_, chol) ->
      (k, res, chol)
    ) conv_csv_traj conv_ram


let convert_csv_traj_ram_prm
    ?n_burn ?n_thin
    ?chol_every ?prm_every ?traj_every
    ~columns ~extract
    ~convert_prm ~sim_traj path =
  let output_main = convert_csv_prm_traj
      ?n_burn ?n_thin
      ?prm_every ?traj_every
      ~columns ~extract
      ~convert_prm ~sim_traj
      path
  in
  let output_aux =
    convert_chol ?n_burn ?chol_every path
  in
  let cb (k, res) (_, chol) =
    (k, res, chol)
  in
  Util.Out.combine_tuple cb output_main output_aux
