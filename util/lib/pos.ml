type t = float

let of_float x : t =
  if x >= 0. then
    x
  else
    invalid_arg "Negative"

let of_float_unsafe x : t = x

let to_float x = x

let zero = 0.

let one = 1.

let two = 2.

let pi = Base.pi


let add = (+.)

let mul = ( *. )

let div = (/.)

let sub x y =
  let z = x -. y in
  assert (z >= 0.) ;
  z


let exp = exp
let log = log
let sq = Base.sq
let sqrt = sqrt
let neg = (~-.)
let pow = Base.pow
let gamma = Base.gamma


let (+) = add
let (-) = sub
let ( * ) = mul
let (/) = div
