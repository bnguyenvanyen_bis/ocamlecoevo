(* Some basic general functions and constants *)

open Sig


let pi = 4. *. atan 1.

let sq x = x *. x

let cub x = x *. x *. x

let int_fold ~f ~x ~n =
  let rec g y =
    function
    | 0 ->
        y
    | n when n < 0 ->
        invalid_arg "negative n"
    | n ->
        g (f y n) (n - 1)
  in g x n


let rec pow x n =
  if n < 0 then
    1. /. (pow x (~- n))
  else if n = 0 then
    1.
  else if (n mod 2) = 0 then
    pow (sq x) (n / 2)
  else if n = 1 then
    x
  else
    x *. (pow x (n - 1))


(* FIXME better implementation ? *)
let factorial k =
  assert (k >= 0) ;
  let rec f prod i =
    match i with
    | 0 -> prod
    | _ -> f (i * prod) (i - 1)
  in f 1 k


(* FIXME better implementation ? *)
let direct_logfact k =
  let rec f acc i =
    match i with
    | 0 -> acc
    | 1 -> acc
    | _ -> f (log (float i) +. acc) (i - 1)
  in f 0. k


let direct_logfact_array ?size k =
  let size =
    match size with
    | None ->
        k + 1
    | Some n ->
        n
  in
  let a = Array.make size 0. in
  for i = 2 to k do
    a.(i) <- a.(i - 1) +. log (float i)
  done ;
  a


let stirling_logfact k =
  let fk = float k in
    (fk -. 1. /. 2.) *. log fk -. fk
  +. 1. /. 2. *. log (2. *. pi) +. 1. /. (12. *. fk)


let logfact k =
  if k <= 256 then
    direct_logfact k
  else
    stirling_logfact k


let binomial_coeff k n =
  (factorial n) / ((factorial k) * (factorial (n - k)))


let logbin_coeff k n =
  logfact n -. logfact k -. logfact (n - k)


(* I don't remember where this implementation comes from... *)
let loggamma x =
  let a0 = ~-. 1.39243221690590e+00 in
  let a = [
    1.796443723688307e-01 ;
    ~-. 2.955065359477124e-02 ;
    6.410256410256410e-03 ;
    ~-. 1.917526917526918e-03 ;
    8.417508417508418e-04 ;
    ~-. 5.952380952380952e-04 ;
    7.936507936507937e-04 ;
    ~-. 2.777777777777778e-03 ;
    8.333333333333333e-02 ; 
  ] in
  let gl =
    if (x = 1.) || (x = 2.) then
      0.
    else begin
      let n, x0 =
        if x <= 7. then
          let fn = floor (7. -. x) in
          (int_of_float fn,
           x +. fn)
        else
          (0, x)
      in
      let x2 = 1. /. (x0 *. x0) in
      let xp = 2. *. pi in
      let gl0 = List.fold_left (fun y v -> y *. x2 +. v) a0 a in
      let gl = gl0 /. x0 +. 0.5 *. log xp +. (x0 -. 0.5) *. log x0 -. x0 in
      let rec mingl gl x0 k =
        match k with
        | 0 -> gl
        | _ ->
          mingl (gl -. log (x0 -. 1.)) (x0 -. 1.) (k - 1)
      in
      if x <= 7. then
        mingl gl x0 n
      else
        gl
    end
  in gl


(*
(* Lanczos approximation with g=5, n=6/7,
 * transcribed from James D McCaffrey's C# implementation
 * https://jamesmccaffrey.wordpress.com/2013/06/19/the-log-gamma-function-with-c/
 *)
let loggamma x =
  let a = [|
    76.18009172947146 ;
    ~-. 86.50532032941677 ;
    24.01409824083091 ;
    ~-. 1.231739572450155 ;
    0.1208650973866179e-2 ;
    ~-. 0.5395239384953e-5 ;
  |]
  in
  let f (series, denom) coef =
    (series +. coef /. denom, denom +. 1.)
  in
  let log_sqrt_two_pi = 0.91893853320467274178 in
  let series, _ = A.fold_left f (1.000000000190015, x +. 1.) a in
  let y = x +. 5.5 in
  log_sqrt_two_pi +. (x +. 0.5) *. log y -. y +. log (series /. x)
*)


let gamma x =
  exp (loggamma x)


let digamma _ =
  failwith "Not_implemented"


let trigamma _ =
  failwith "Not_implemented"


let l_repeat n x =
  let rec f accl n =
    match n with
    | 0 -> accl
    | _ -> f (x :: accl) (n - 1)
  in f [] n


let rec l_cumul_sum l =
  match l with
  | a :: b :: tl -> a :: (l_cumul_sum ((a +. b) :: tl))   
  | _ :: [] -> l
  | [] -> l (* this case should never happen *);;


let cumul_fold_left f x l =
  let f' (x, rsl) x' =
    let x'' = f x x' in
    (x'', x'' :: rsl)
  in
  List.fold_left f' (x, []) l



let l_cumul_sum_rev l =
  let tot, revsl = cumul_fold_left (+.) 0. l in
  (tot, List.rev revsl)


let l_fold_succ f x l =
  (* l without the last element *)
  let n = L.length l in
  let left_l = L.take (n - 1) l in
  let right_l = L.tl l in
  L.fold_left2 f x left_l right_l


let log_sum_exp xs =
  let xmax = List.fold_left max neg_infinity xs in
  let part_sum_exp = List.fold_left (fun sum x ->
    sum +. exp (x -. xmax)
  ) 0. xs in
  xmax +. log part_sum_exp


(* transpose a list list *)
let transpose ll =
  let llt_rev = L.fold_left (
    L.map2 (fun col x ->
      x :: col)
  ) (L.map (fun c -> [c]) (L.hd ll)) (L.tl ll)
  in
  L.map L.rev llt_rev


