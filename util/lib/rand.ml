(******* Functions using the random generator *******)
open Sig
open Base

module F = Float
module I = Int

type rng = Random.State.t

let rng = function
  | None -> Random.State.make_self_init ()
  | Some n -> Random.State.make (Array.init 1 (fun _ -> n))


(* doesn't guarantee that the PRNG has been initialized *)
(*
let some_rng = function
  | None -> Random.get_state ()
  | Some rng -> rng
*)


let rand_rng rng =
  Random.State.make [| Random.State.bits rng |]


(* exclusive *)
let rand_int ~rng top : 'a I.pos I.t =
  I.of_int_unsafe (Random.State.int rng (I.to_int top))


(* > 0. *)
let rand_float ~rng (top : 'a F.anypos F.t) : 'b F.pos F.t =
  let top' = F.to_float top in
  let rec f () =
    match Random.State.float rng top' with
    | 0. ->
        f ()
    | u  ->
        u
  in
  if top' = 0. then
    F.zero
  else
    F.of_float_unsafe (f ())


let rand_bool ~rng =
  Random.State.bool rng


let rand_proba ~rng : 'a F.proba F.t =
  F.promote_unsafe (rand_float ~rng F.one)


let thousand : I.anypos I.t = I.Pos.of_int 1_000

let insist ?(ntries=thousand) ~rng g =
  let rec f =
    function
    | 0 ->
        failwith "exhausted payload"
    | n when n < 0 ->
        assert false (* protected by the type *)
    | n ->
        match g ~rng with
        | None ->
            f (n - 1)
        | Some x ->
            x
  in f (I.to_int ntries)


let bound ~rng a b f =
  (* cadlag : left included, right excluded *)
  let p x = (x >= a) && (x < b) in

  let g ~rng =
    let x = f ~rng in
    if p x then Some x else None
  in
  insist ~rng g


let rand_exp ~rng lambda : 'a F.pos F.t =
  let u = rand_proba ~rng in
  F.promote_unsafe F.(Op.(~- (log1p (~- u)) / lambda))
  (*
  let nlu = F.Neg.neg (F.Proba.log u) in
  F.Pos.Op.(nlu / lambda)
  *)


(* Box-Muller method *)
(* FIXME Should we implement Ziggurat ? *)
let rand_normal ~rng m v =
  let v' = F.to_float v in
  let u = F.to_float (rand_proba ~rng) in
  let w = F.to_float (rand_proba ~rng) in
  (* N (0, 1) *)
  let x = (sqrt (~-. 2. *. (log u) )) *. (cos (2. *. pi *. w)) in
  (* rescale *)
  m +. (sqrt v') *. x


(* I think *)
let rand_lognormal ~rng ?(b=10.) m v =
  let x = rand_normal ~rng m v in
  b ** x


let four : I.anypos I.t = I.Pos.of_int 4

let rand_square ~rng a v =
  let x = F.to_float (rand_float ~rng a) in
  let y = rand_normal ~rng 0. v in
  let a' = F.to_float a in
  match I.to_int (rand_int ~rng four) with
  | 0 -> (x, y)
  | 1 -> (y, x)
  | 2 -> (x, y +. a')
  | 3 -> (y +. a', x)
  | _ -> failwith "Impossible case"


let rand_bernoulli ~rng p =
  let u = rand_proba ~rng in
  (u <= p)


(* a bit annoying because we basically rewrite
   l_cumul_sum and rand_choose *)
let rand_binomial ~rng n p =
  (* n trials with proba p of success *)
  let n' = Int.to_int n in
  let rec cumulbin k tot acc =
    match k with
    | 0 -> 
      let tot' = F.Pos.add tot (Proba.p_binomial n p k) in
      (tot', tot' :: acc)
    | _ ->
      let tot' = F.Pos.add tot (Proba.p_binomial n p k) in
      cumulbin (k - 1) tot' (tot' :: acc)
  in
  let probtot, pl = cumulbin n' F.zero [] in
  let u = rand_float ~rng probtot in
  let rec choose k pl =
    match pl with
    | p :: ptl ->
      if u <= p then
        k
      else
        choose (k - 1) ptl
    | [] ->
      (* should only be reached on entry with n = 0 *)
      0
  in choose n' (L.rev pl)


(* FIXME where does this come from ? *)
let rand_poisson_acrej ~rng (lbd : 'a F.anypos F.t) : 'b Int.pos Int.t =
  (* what are those for though ? unused in the C code *)
  (* let ls2pi = 0.91893853320467267 in *)
  (* let twelfth = 0.083333333333333333333333 in *)
  let lbd' = F.to_float lbd in
  let sqlbd = sqrt lbd' in
  let loglbd = log lbd' in
  let b = 0.931 +. 2.53 *. sqlbd in
  let a = ~-. 0.059 +. 0.02483 *. b in
  let invalpha = 1.1239 +. 1.1328 /. (b -. 3.4) in
  let vr = 0.9277 -. 3.6244 /. (b -. 2.) in
  let rec f =
    function
    | 0 ->
        failwith "Util.rand_poisson_acrej exhausted payload"
    | n when n < 0 ->
        invalid_arg "negative n"
    | n ->
        let u = F.to_float (rand_proba ~rng) -. 0.5 in
        let v = F.to_float (rand_proba ~rng) in
        let us = 0.5 -. abs_float u in
        let k = floor (u *. (2. *. a /. us +. b) +. lbd' +. 0.43) in
        if (us >= 0.07) && (v <= vr) then
          int_of_float k
        else if (k < 0.) || ((us < 0.013) && (v > us)) then
          f (n - 1)
        else if (log v +. log invalpha -. log (a /. (us *. us) +. b))
             <= (k *. loglbd -. lbd' -. loggamma (k +. 1.)) then
          int_of_float k
        else
          f (n - 1)
  in Int.of_int_unsafe (f 100)


(* FIXME it seems like this might be wrong for lbd = 5 for instance *)
let rand_poisson_exp ~rng (lbd : 'a F.anypos F.t) : 'b Int.pos Int.t =
  let rec f k t =
    let e = F.to_float (rand_exp ~rng F.one) in
    let nt = t -. e in
    if nt >= 0. then
      f (Int.Pos.succ k) nt
    else 
      k
  in Int.Pos.narrow (f Int.zero (F.to_float lbd))


let rand_poisson ~rng (lbd : 'a F.anypos F.t) : 'b Int.pos Int.t =
  let ten = F.of_float 10. in 
  if F.Op.(lbd > ten) then
    match rand_poisson_acrej ~rng lbd with
    | n ->
        n
    | exception Failure _ ->
        rand_poisson_exp ~rng lbd
  else if F.Op.(lbd = F.zero) then
    Int.zero
  else
    rand_poisson_exp ~rng lbd


(* logarithmic variable *)
let rand_log ~rng p =
  (* Can't manage to pass a proba x because of recursive calls *)
  let incr_series k (x : 'a F.anypos F.t) =
    let posk = Int.Pos.to_float k in
    let pk = F.Pos.pow p (Int.to_int k) in
    F.Pos.add x (F.Pos.div pk posk)
    (* F.Pos.Op.(x + pk / posk) *)
  in
  let u = rand_proba ~rng in
  (* ~ log (1 - p) *)
  let r = p |> F.Proba.compl |> F.Proba.log |> F.Neg.neg in
  let ru = F.Pos.mul r u in
  let rec stop_at k (x : 'a F.anypos F.t) =
    if F.Op.(ru <= x) then
      k
    else
      let k' = Int.Pos.succ k in
      let x' = incr_series k' x in
      stop_at k' x'
  in
  Int.Pos.narrow (stop_at Int.zero F.zero)


(* quite simple but not very efficient if lbd is big...
 * but gamma doesn't really seem better *)
let rand_negbin ~rng r p =
  let rec add_logs k =
    function
    | 0 ->
        k
    | n when n <= 0 ->
        invalid_arg "negative"
    | n ->
        let k' = Int.Pos.add k (rand_log ~rng p) in
        add_logs k' (n - 1)
  in
  let nlcp = p |> F.Proba.compl |> F.Proba.log |> F.Neg.neg in
  let lbd = F.Pos.mul r nlcp in
  let n = rand_poisson ~rng lbd in
  Int.Pos.narrow (add_logs Int.zero (Int.to_int n))


let over_disp_deparam ovd lbd =
  (* r = 1 / ovd *)
  let r = F.Pos.div F.one ovd in
  (* p = lbd / (lbd + r) *)
  let _, p, _ = F.Pos.proportions lbd r in
  (r, p)


let rand_negbin_over ~rng ovd lbd =
  let r, p = over_disp_deparam ovd lbd in
  rand_negbin ~rng r p


(* FIXME replace by integer part of rand_exp for p < 1. /. 10. *)
let rand_geom ~rng p =
  let rec f n =
    if rand_bernoulli ~rng p then
      n
    else
      f (Int.Pos.succ n)
  in Int.Pos.narrow (f Int.zero)


let rand_cumul_choose ?tot ~rng csp_l =
  (* In order of increasing cumulative weights *)
  let tot =
    match tot with
    | None ->
        (* assume sum to 1. *)
        F.one
    | Some x ->
        x
  in
  let u = rand_float ~rng tot in
  let rec f = function
    | [] -> 
        invalid_arg "Util.rand_cumul_choose: total too small"
    | (c, sp) :: tl ->
        (match F.Op.(u <= sp) with
         | true -> c 
         | false -> f tl)
  in
  match csp_l with
  | [] ->
      invalid_arg "Empty"
  | _ ->
      f csp_l


let rand_cumul_rev_choose ?tot ~rng csp_l =
  (* In order of decreasing cumulative weights *)
  let tot =
    match tot with
    | None ->
        (* assume sum to 1. *)
        F.one
    | Some x ->
        x
  in
  let u = rand_float ~rng tot in
  let rec f c = function
    | [] ->
        c
    | (c', sp) :: tl ->
        (match F.Op.(u >= sp) with
         | true -> c (* and not c' *)
         | false -> f c' tl)
  in
  match csp_l with
  | [] -> invalid_arg "Nothing to choose from"
  | (c, _) :: tl -> f c tl


(* Another more efficient possibility for rand_choose, to test
let rand_choose ~rng cp_l =
  (* c (for p) has already been seen *)
  let cumul (_, p) (c', p') =
    (c', F.Pos.add p p')
  in
  (* then we need a dummy choice (never chosen) for init *)
  let dummy_choice, _ = L.hd cp_l in
  let tot, revscp_l =
    l_cumul_fold_left cumul (dummy_choice, F.zero) cp_l
  in
  rand_cumul_rev_choose ~rng ~tot revscp_l
*)


let rand_choose ~rng cp_l =
  let c_l, p_l = L.split cp_l in
  let tot, revsp_l = cumul_fold_left F.Pos.add F.zero p_l in
  let sp_l = L.rev revsp_l in
  let csp_l = L.combine c_l sp_l in
  rand_cumul_choose ~rng ~tot:tot csp_l


let rand_unif_choose ~rng c_l =
  c_l
  |> L.length
  |> I.of_int_unsafe
  |> rand_int ~rng
  |> I.to_int
  |> L.nth c_l


let cumul_multinomial ~rng n csp_l =
  let rec f n l =
    match n with (* problem if n < 0 *)
    | 0 -> l
    | _ -> f (n - 1) (rand_cumul_choose ~rng csp_l :: l)
  in f n []
  

let multinomial ~rng n cp_l =
  (* FIXME we compute the cumul sum list many times *)
  let c_l, p_l = L.split cp_l in
  let _, revsp_l = cumul_fold_left F.Pos.add F.zero p_l in
  let sp_l = L.rev revsp_l in
  let csp_l = L.combine c_l sp_l in
  cumul_multinomial ~rng (Int.to_int n) csp_l


let subsample ~rng p l =
  let rec f acc l =
    match l with
    | x :: tl ->
      if rand_bernoulli ~rng p then
        f (x :: acc) tl
      else
        f acc tl
    | [] ->
      acc
  in L.rev (f [] l)


let p_monte_carlo p rand_f n : 'a F.proba F.t =
  let rec f k i =
    match i with
    | 0 ->
      k
    | _ ->
      let x = rand_f () in
      if p x then
        f (k + 1) (i - 1)
      else
        f k (i - 1)
  in
  let n' = Int.to_int n in
  let k = f 0 n' in
  F.of_float_unsafe ((float k) /. (float n'))


