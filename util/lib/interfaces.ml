module type EQUALABLE =
  sig
    type t

    val equal : t -> t -> bool
  end


module type COMPARABLE =
  sig
    type t
    
    val compare : t -> t -> int
  end


module type HASHABLE =
  sig
    type t

    val hash : t -> int
  end


module type REPRABLE =
  sig
    type t

    val to_string : t -> string
    val of_string : string -> t option
  end


module type MAPPABLE =
  sig
    type 'a t

    val map : ('a -> 'b) -> 'a t -> 'b t
  end


module type ITERABLE =
  sig
    type elt
    type t

    val iter : (elt -> unit) -> t -> unit
  end


module type FOLDABLE =
  sig
    type elt
    type t

    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
  end


module type UNFOLDABLE =
  sig
    type elt
    type t

    val unfold : ('a -> ('a * elt) option) -> 'a -> t
  end


module type SEQ =
  sig
    include FOLDABLE
    include UNFOLDABLE with type elt := elt
                        and type t := t
  end
