include Base

include Proba

include (Rand : (module type of Rand with module F := F))

include (Multi : (module type of Multi with module B := B))

include (Comix : (module type of Comix with module F := F))

module Interfaces = Interfaces

module Option = Option

module Out = Out

module Gc = Util__Gc

module Csv = Util__Csv

module Float = Float

module Int = Int

module Term = Term

(*
module Pos = Pos

module Posint = Posint

module Probability = Probability
*)

module Precompute = Precompute.Make



type _float = Float._float Float.t

type 'a anyfloat = 'a Float.anyfloat Float.t

type 'a pos = 'a Float.pos Float.t

type 'a anypos = 'a Float.anypos Float.t

type closed_pos = Float.closed_pos Float.t

type 'a proba = 'a Float.proba Float.t

type anyproba = Float.anyproba Float.t

type 'a neg = 'a Float.neg Float.t

type anyneg = Float.anyneg Float.t


type _int = Int._int Int.t

type 'a anyint = 'a Int.anyint Int.t

type 'a posint = 'a Int.pos Int.t

type anyposint = Int.anypos Int.t

type 'a negint = 'a Int.neg Int.t

type anynegint = Int.anyneg Int.t



let d_empirical ?floor compare d xs =
  Empirical.make_density ?floor compare d xs


let logd_empirical ?floor compare d xs =
  Empirical.make_log_density ?floor compare d xs


let d_empirical_normal xs =
  Empirical.make_normal_density xs

let logd_empirical_normal xs =
  Empirical.make_normal_log_density xs

let interpolate = Empirical.interpolate

let estimate_density = Empirical.estimate_density

let estimate_log_density = Empirical.estimate_log_density
