open Sig

module B = Base


type pos_def_mat =
  | Cov of Lac.Mat.t
  | Chol of Lac.Mat.t
  | Cov_chol of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
    }
  | Ccdi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      det : float ;
      inv : Lac.Mat.t ;
    }
  | Ccldi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      logdet : float ;
      inv : Lac.Mat.t ;
    }


type covariance = pos_def_mat ref


let covchol c =
  Lac.syrk ~up:false c

let cov_to_mat cov =
  match !cov with
  | Chol c ->
      covchol c
  | Cov cov
  | Cov_chol { cov ; _ }
  | Ccdi { cov ; _ }
  | Ccldi { cov ; _ } ->
      cov


let mat_to_cov c = ref (Chol c)


let dim1 cov =
  match !cov with
  | Chol c ->
      Lac.Mat.dim1 c
  | Cov cov
  | Cov_chol { cov ; _ }
  | Ccdi { cov ; _ }
  | Ccldi { cov ; _ } ->
      Lac.Mat.dim1 cov


let cholesky ?(triang=false) mat =
  let chol = Lac.lacpy mat in
  Lac.potrf ~up:false chol ;
  (* set the upper triangle to 0s *)
  if triang then Lac.lacpy ~uplo:`L chol else chol


let chol_inv chol =
  let inv = Lac.lacpy chol in
  Lac.potri ~up:false inv ;
  Lac.Mat.detri ~up:false inv ;
  inv


(* Code transcribed from ramcmc *)
let chol_update ~(up:[`Up | `Down]) a u =
  (* a should be lower triangular *)
  let n = Lac.Vec.dim u in
  let m1 = Lac.Mat.dim1 a in
  let m2 = Lac.Mat.dim2 a in
  assert ((n = m1) && (n = m2)) ;
  let fr =
    match up with
    | `Up ->
        (fun x y -> sqrt (B.sq x +. B.sq y))
    | `Down ->
        (fun x y -> sqrt (B.sq x -. B.sq y))
  in
  for i = 1 to n - 1 do
    let r = fr a.{i,i} u.{i} in
    let c = r /. a.{i,i} in
    let s = u.{i} /. a.{i,i} in
    (* plus or minus s *)
    let pms =
      match up with
      | `Up ->
          s
      | `Down ->
          ~-. s
    in
    (* a update *)
    (* a.{i+1:n,i} <- (a.{i+1:n,i} +. s *. u.{i+1:n}) /. c ; *)
    a.{i,i} <- r ;
    let ai = Lac.Mat.col a i in
    let ip1 = i + 1 in
    Lac.axpy ~alpha:pms ~ofsx:ip1 u ~ofsy:ip1 ai ;
    Lac.scal ~ofsx:ip1 (1. /. c) ai ;
    (* u update *)
    (* u.{i+1:n} <- c *. u.{i+1:n} -. s *. a.{i+1:n,i} *)
    Lac.scal c ~ofsx:ip1 u ;
    Lac.axpy ~alpha:(~-. s) ~ofsx:ip1 ai ~ofsy:ip1 u 
  done ;
  a.{n,n} <- fr a.{n,n} u.{n}


let chol_update_test ~(up:[`Up | `Down]) a u =
  let c = covchol a in
  let alpha =
    match up with
    | `Up -> 1.
    | `Down -> ~-. 1.
  in
  Lac.syr ~up:false ~alpha u c ;
  try
    let a' = cholesky c in
    ignore (Lac.lacpy ~b:a a')
  with Failure _ ->
    ()


let common_multi_normal mean cov v =
  (* if lchol, lower triangular matrix obtained by Cholesky decomposition
   * is given, it is used, otherwise it is computed. *)
  let n = Lac.Vec.dim v in
  let detinv c =
    let diag = Lac.Mat.copy_diag c in
    let det = B.sq (Lac.Vec.prod diag) in
    if det = 0. then begin
      for i = 1 to Lac.Vec.dim diag do
        Printf.eprintf "%f " diag.{i}
      done ;
      Printf.eprintf "\n" ;
    end ;
    let inv = chol_inv c in
    det, inv
  in
  let _, det, inv =
    match !cov with
    | Cov c ->
        let lchol = cholesky c in
        let det, inv = detinv lchol in
        cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
        lchol, det, inv
    | Chol lchol ->
        let c = covchol lchol in
        let det, inv = detinv lchol in
        cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
        lchol, det, inv
    | Cov_chol { cov = c ; chol = lchol } ->
        let det, inv = detinv lchol in
        cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
        lchol, det, inv
    | Ccdi { chol = lchol ; det ; inv ; _ } ->
        lchol, det, inv
    | Ccldi { chol = lchol ; logdet ; inv ; _ } ->
        lchol, exp logdet, inv
  in
  (* FIXME inv, the precision matrix,
   * probably has features that we could use *)
  let a = 1. /. (sqrt ((B.pow (2. *. B.pi) n) *. det)) in
  (* FIXME could reuse storage vector *)
  let centered =
    match mean with
    | `Centered ->
        v
    | `Mean m ->
        Lac.Vec.sub v m
  in
  let dotted = Lac.gemv inv centered in
  (* FIXME I think *)
  let sqmaha = Lac.dot dotted centered in
  let e = ~-. sqmaha /. 2. in
  (a, e)


let common_multi_normal_std x =
  let n = Lac.Vec.dim x in
  let a = 1. /. (sqrt (B.pow (2. *. B.pi) n)) in
  let sqmaha = Lac.dot x x in
  let e = ~-. sqmaha /. 2. in
  (a, e)


let d_multi_normal m cov v : 'a Float.pos Float.t =
  let a, e = common_multi_normal (`Mean m) cov v in
  Float.of_float_unsafe (a *. exp e)


let d_multi_normal_center cov v : 'a Float.pos Float.t =
  let a, e = common_multi_normal `Centered cov v in
  Float.of_float_unsafe (a *. exp e)


let d_multi_normal_std v : 'a Float.pos Float.t =
  let a, e = common_multi_normal_std v in
  Float.of_float_unsafe (a *. exp e)


let logd_multi_normal_general mean cov v =
  let n = Lac.Vec.dim v in
  let logdet_inv c =
    let diag = Lac.Mat.copy_diag c in
    let log_det = 2. *. (Lac.Vec.sum (Lac.Vec.log diag)) in
    let inv = chol_inv c in
    log_det, inv
  in
  let log_det, inv =
    match !cov with
    | Cov c ->
        let lchol = cholesky c in
        let logdet, inv = logdet_inv lchol in
        cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
        logdet, inv
    | Chol lchol ->
        let c = covchol lchol in
        let logdet, inv = logdet_inv lchol in
        cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
        logdet, inv
    | Cov_chol { cov = c ; chol = lchol } ->
        let logdet, inv = logdet_inv lchol in
        cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
        logdet, inv
    | Ccdi { det ; inv ; _ } ->
        log det, inv
    | Ccldi { logdet ; inv ; _ } ->
        logdet, inv
  in
  let loga = ~-. 1. /. 2. *. (float n *. (log 2. +. log B.pi) +. log_det) in
  let centered =
    match mean with
    | `Centered ->
        v
    | `Mean m ->
        Lac.Vec.sub v m
  in
  let dotted = Lac.gemv inv centered in
  let sqmaha = Lac.dot dotted centered in
  let e = ~-. sqmaha /. 2. in
  Float.of_float (loga +. e)


let logd_multi_normal_center cov v =
  logd_multi_normal_general `Centered cov v


let logd_multi_normal m cov v =
  logd_multi_normal_general (`Mean m) cov v


(*
let logd_multi_normal m cov v =
  let a, e = common_multi_normal m cov v in
  Float.of_float (log a +. e)
*)


let logd_multi_normal_std v =
  let a, e = common_multi_normal_std v in
  Float.of_float (log a +. e)


let update_mean_estimate m k v =
  (* m <- ((k - 1) m + v) / k *)
  Lac.scal (float (k - 1)) m ;
  Lac.axpy v m ;
  Lac.scal (1. /. float k) m


let single_wishart_mle ?y m v =
  let n = Lac.Vec.dim m in
  (* FIXME find way to reuse centered memory *)
  let centered = Lac.Vec.sub v m in
  let mat =
    match y with
    | None -> Lac.Mat.make0 n n
    | Some y -> y
  in
  Lac.syr ~up:false centered mat ;
  mat


let update_cov_mle cov m k v =
  (* assume real mean (or mean estimate already updated) *)
  (* cov <- ((k - 1) cov + w) / k *)
  let c =
    match !cov with
    | Cov c ->
        c
    (* in those cases it would be nice to not throw chol and inv away...
     * at least the memory spaces *)
    | Chol lchol ->
        covchol lchol
    | Cov_chol { cov = c ; _ }
    | Ccdi { cov = c ; _ }
    | Ccldi { cov = c ; _ } ->
        c
  in
  Lac.Mat.scal (float (k - 1)) c ;
  let c = single_wishart_mle ~y:c m v in
  Lac.Mat.scal (1. /. float k) c ;
  (* reinitialize cov (forget chol etc) *)
  cov := Cov c


let rand_multi_normal_std ~rng ?store n =
  let x =
    match store with
    | None -> Lac.Vec.make0 n
    | Some v -> v
  in
  for i = 1 to n do
    x.{i} <- Rand.rand_normal ~rng 0. Float.one
  done ;
  x


let rand_multi_normal_center ~rng ?store_std ?store cov =
  let a =
    match !cov with
    | Cov c ->
        let chol = cholesky c in
        cov := Cov_chol { cov = c ; chol } ;
        chol
    | Chol chol
    | Cov_chol { chol ; _ }
    | Ccdi { chol ; _ }
    | Ccldi { chol ; _ } ->
        chol
  in
  (* get a N(0, 1) random vector *)
  let n = Lac.Mat.dim1 a in
  if n = 0 then
    Lac.Vec.make0 n  (* or any empty vector *)
  else begin
    let x = rand_multi_normal_std ~rng ?store:store_std n in
    let y = Lac.copy ?y:store x in
    (* (lower) triangular matrix vector product *)
    Lac.trmv ~up:false a y ;
    y
  end


(* what do we do in the positive semi-definite case ? I'm not sure *)
let rand_multi_normal ~rng ?store_std ?store m cov =
  let x = rand_multi_normal_center ~rng ?store_std ?store cov in
  Lac.Vec.add ?z:store m x
