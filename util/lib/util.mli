(** Util provides commonly used functions throughout the library.
 *  
 *  Most functions are for {{!dists} computing probabilities and densities}
 *  under some useful distributions, logs of those probas/densities,
 *  and {{!prng} sampling} from the same useful distributions.
 *
 *  Some additional basic math functions are present,
 *  for computing factorials, and the logarithm version of various
 *  functions, with less overflows, in the {{!basics} Basics}.
 *)

open Sig

module Interfaces :
  sig
    include module type of Interfaces
  end


module Option :
  sig
    include module type of Option
  end


module Gc :
  sig
    val tune : unit -> unit
  end


module Out :
  sig
    include module type of Out
  end


module Csv :
  sig
    include module type of Util__Csv
  end


module Term :
  sig
    include module type of Term
  end


module Float :
  sig
    include module type of Float
  end

module Int :
  sig
    include module type of Int
  end


type _float = Float._float Float.t

type 'a anyfloat = 'a Float.anyfloat Float.t

type 'a pos = 'a Float.pos Float.t

type 'a anypos = 'a Float.anypos Float.t

type closed_pos = Float.closed_pos Float.t

type 'a proba = 'a Float.proba Float.t

type anyproba = Float.anyproba Float.t

type 'a neg = 'a Float.neg Float.t

type anyneg = Float.anyneg Float.t


type _int = Int._int Int.t

type 'a anyint = 'a Int.anyint Int.t

type 'a posint = 'a Int.pos Int.t

type anyposint = Int.anypos Int.t

type 'a negint = 'a Int.neg Int.t

type anynegint = Int.anyneg Int.t


(** Informative representation of positive definite matrix. 
 *  [m] : the matrix.
 *  [ltc] : lower triangular matrix from Cholesky decomposition.
 *  [det] : determinant of the matrix.
 *  [inv] : inverse of the matrix. *)
type pos_def_mat =
  | Cov of Lac.Mat.t
    (** [Cov m] *)
  | Chol of Lac.Mat.t
    (** [Chol ltc] *)
  | Cov_chol of { cov : Lac.Mat.t ; chol : Lac.Mat.t }
    (** [Cov_chol { cov = m ; chol = ltc }] *)
  | Ccdi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      det : float ;
      inv : Lac.Mat.t ;
    }
    (** [Ccdi { cov = m ; chol = ltc ; det ; inv)] *)
  | Ccldi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      logdet : float ;
      inv : Lac.Mat.t ;
    }
    (** [Ccldi { cov = m ; chol = ltc ; logdet ; inv)] *)

(** Stateful representation of a positive definite matrix,
 *  to not recompute decompositions, etc when unnecessary. *)
type covariance = pos_def_mat ref

(** [cov_to_mat cov] is the covariance matrix from cov. *)
val cov_to_mat : covariance -> Lac.Mat.t

(** [mat_to_cov mat] is [mat] wrapped as a 'covariance'. *)
val mat_to_cov : Lac.Mat.t -> covariance

(** [dim1 cov] is the first dimension of the covariance matrix. *)
val dim1 : covariance -> int


(** {1:basics Basic mathematical functions} *)

(** pi *)
val pi : float

(** [sq x] is [x *. x] *)
val sq : float -> float

(** [int_fold ~f ~x ~n] is the result of applying f recursively
 *  with inputs from [n] to [1]. *)
val int_fold :
  f:('a -> int -> 'a) ->
  x:'a ->
  n:int ->
    'a

(** [pow x n] is [x ** (float n)],
 *  the integer exponentiation implemented by squaring. *)
val pow : float -> int -> float

(** [factorial k] is [k!]. *)
val factorial : int -> int

(** [logfact k] is [log (k!)]. *)
val logfact : int -> float

(** [loggamma x] is the log of the gamma function of [x]. *)
val loggamma : float -> float

(** [l_repeat n x] is a list of [n] times [x]. *)
val l_repeat : int -> 'a -> 'a list

(** [transpose ll] is the transposed of the matrix [ll] as a list of lists. *)
val transpose : 'a list list -> 'a list list

(** [cumul_fold_left f x l] folds [f] over the elements of [l],
 *  and returns the intermediate and final results *)
val cumul_fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> ('a * 'a list)

(** [l_cumul_sum x_l] gives the cumulative sum of [x_l]. *)
val l_cumul_sum : float list -> float list

(** [l_cumul_sum_rev x_l] gives the total sum and cumulative sum of [x_l].
    When the total is needed, it avoids an additional rev of the list. *)
val l_cumul_sum_rev : float list -> float * float list

(** [log_sum_exp xs] is the log of the sum of the exponentials
 *  of the elements of [xs], with better stability. *)
val log_sum_exp : float list -> float

(** [cholesky ~triang mat] is the lower triangle
 *  of the cholesky decomposition of mat.
 *  By default ([~triang:false]), the fresh returned matrix
 *  isn't set to 0. in its upper triangle.
 *  With [~triang:true], it is. *)
val cholesky :
  ?triang:bool ->
  Lac.Mat.t ->
    Lac.Mat.t

(** [chol_update ~up a u] is the rank-one update
 *  of the Cholesky lower triangular matrix [a].
 *  With [up:`Up] it is an update [+ u u^T],
 *  with [up:`Down] a downdate [- u u^T]. *)
val chol_update :
  up : [`Up | `Down] ->
  Lac.Mat.t ->
  Lac.Vec.t ->
    unit


val chol_update_test :
  up : [`Up | `Down] ->
  Lac.Mat.t ->
  Lac.Vec.t ->
    unit


(** {1:dists Distributions} *)

(** All functions have the parameters of the distribution as first arguments,
 *  and the value where the function is evaluated as last argument.
 *  The documentation uses [Pr(.)] to denote the probability of some set,
 *  and [X ~ L] to indicate that [X] follows the law [L]. *)

(** {2:reals Real(and vector)-valued distributions} *)

(** The [d] prefixed functions compute densities,
 *  the [logd] functions compute log-of-densities,
 *  and the [p] functions compute [Pr(X <= x)]. *)

(** [d_unif_int a b] is the uniform integer density function
  * between a and b *)
val d_unif_int : int -> int -> int -> 'a pos

(** [d_unif_int a b] is the log of the uniform integer density function
  * between a and b *)
val logd_unif_int : int -> int -> int -> _float

(** [d_unif a b] is the uniform (constant) density function between a and b *)
val d_unif : float -> float -> float -> 'a pos

(** [logd_unif a b] is the log of the uniform density in [a, b].
    Can return neg_infinity. *)
val logd_unif : float -> float -> float -> _float

(** [d_exp lbd] is the density of an exponential variable
 *  of parameter [lbd]. *)
val d_exp : 'a anypos -> float -> 'b pos

(** [logd_exp lbd] is the log of {!d_exp} of [lbd]. *)
val logd_exp : 'a anypos -> float -> _float

(** [d_normal m v] is the density of a normal variable
 *  of mean [m] and [variance] v. *)
val d_normal : float -> 'a anypos -> float -> 'b pos

(** [p_normal m v] is the cdf of the normal distribution
 *  of mean [m] and variance [v]. *)
val p_normal : float -> 'a anypos -> float -> 'b proba

(** [logd_normal m v] is the log of {!d_normal} of [m v]. *)
val logd_normal : float -> 'a anypos -> float -> _float

(** [logsumd_normal m v xs] is the log of the sum of {!d_normal} of [m v x]
 *  for [x] in [xs]. *)
val logsumd_normal : float -> 'a anypos -> float list -> _float

(** [d_lognormal b m v] is the density of a [b]-lognormal variable
 *  of mean [m] and variance [v]. *)
val d_lognormal : ?b:float -> float -> 'a anypos -> float -> 'b pos

(** [logd_lognormal b m v] is the log of {!d_lognormal} of [b m v]. *)
val logd_lognormal : ?b:float -> float -> 'a anypos -> float -> _float

(** [d_multi_normal m cov v] is [f_X(v)] where [X]
 *  is a multivariate random normal variable of mean [m]
 *  and covariance matrix [cov]. *)
val d_multi_normal :
  Lac.Vec.t ->
  covariance ->
  Lac.Vec.t ->
    'a pos

(** [d_multi_normal_center cov v] is {!d_multi_normal} with [0] mean. *)
val d_multi_normal_center :
  covariance ->
  Lac.Vec.t ->
    'a pos


(** [d_multi_normal_std v] is [f_X(v)] where [X]
 *  is a standard multivariate normal random variable. *)
val d_multi_normal_std :
  Lac.Vec.t ->
    'a pos

(** [logd_multi_normal m cov v] is the log of {!d_multi_normal}
 *  of [m cov v]. *)
val logd_multi_normal :
  Lac.Vec.t ->
  covariance ->
  Lac.Vec.t ->
    _float

(** [logd_multi_normal_center cov v] is the log of
 *  {!d_multi_normal_center} of [cov v]. *)
val logd_multi_normal_center :
  covariance ->
  Lac.Vec.t ->
    _float

(** [logd_multi_normal_std v] is the log of {!d_multi_normal_std}
 *  of [v]. *)
val logd_multi_normal_std :
  Lac.Vec.t ->
    _float

(** [d_square a v] is the density of a 'square' variable of side length [a]
 *  and side variance [v]. *)
val d_square :
  'a anypos ->
  'b anypos ->
  float * float ->
    'c pos   

(** [logd_square a v] is the log of {!d_square} of [a v]. *)
val logd_square :
  'a anypos ->
  'b anypos ->
  float * float ->
    _float   

(** [d_empirical compare d xs y] is the empirical density of [y],
 *  given the comparison function [compare], the distance function [d],
 *  and the samples [xs]. *)
val d_empirical :
  ?floor : float ->
  ('a -> 'a -> int) ->
  ('a -> 'a -> float) ->
  ('a list) ->
  'a ->
    float

(** [logd_empirical compare d xs y] is the log of {!d_empirical} of 
 *  [compare d xs y]. *)
val logd_empirical :
  ?floor : float ->
  ('a -> 'a -> int) ->
  ('a -> 'a -> float) ->
  'a list ->
  'a ->
    float

(** [d_empirical_normal xs y] is the empirical density of [y],
 *  following the samples [xs] assuming they are normally distributed. *)
val d_empirical_normal :
  float list ->
  float ->
    'a pos

(** [logd_empirical_normal xs y] is the log of {!d_empirical_normal} of [xs y] *)
val logd_empirical_normal :
  float list ->
  float ->
    _float

(** [interpolate ~at xs] is [xs] interpolated linearly at points [at]. *)
val interpolate :
  ?mode : [`Linear | `Behave] ->
  at : float list ->
  (float -> float) ->
    (float -> float)

(** [estimate_density ~h ?interpol ?kernel xs] is a kernel density estimate
 * of [xs], with window width [h] and kernel specified by [kernel],
 * by default an Epanechnikov kernel. 
 * By default linear interpolation is done, ~interpol:false uses the
 * true kde. *)
val estimate_density :
  h : 'a pos ->
  ?interpol : bool ->
  ?kernel : [
    | `Uniform
    | `Triangular
    | `Epanechnikov
    | `Quartic
    | `Triweight
    | `Tricube
    | `Gaussian
    | `Cosine
    | `Logistic
    | `Sigmoid
    | `Silverman
  ] ->
  float list ->
    ('b anyfloat -> _float)

(** [estimate_log_density ~h ?interpol ?kernel xs] is the log of
 *  {!estimate_density} of [~h ?interpol ?kernel xs]. *)
val estimate_log_density :
  h : 'a pos ->
  ?interpol : bool ->
  ?kernel : [
    | `Uniform
    | `Triangular
    | `Epanechnikov
    | `Quartic
    | `Triweight
    | `Tricube
    | `Gaussian
    | `Cosine
    | `Logistic
    | `Sigmoid
    | `Silverman
  ] ->
  float list ->
    ('b anyfloat -> _float)

(** {2:discretes Discrete distributions} *)

(** The [p] prefixed functions compute probabilities ([Pr(N=k)]).
 *  The [logp] prefixed functions compute logs-of-probabilities. *)

(** [p_bernoulli p b] is [Pr(B=b)]
 *  where [B] is a Bernoulli variable of parameter [p]. *)
val p_bernoulli : anyproba -> bool -> 'a proba

(** [logp_bernoulli p b] is the log of {!p_bernoulli} of [p b]. *)
val logp_bernoulli : anyproba -> bool -> 'a neg

(** [p_binomial n p k] is [Pr(N=k)] where [N ~ B(n,p)]. *)
val p_binomial : anyposint -> anyproba -> int -> 'a proba

(** [logp_binomial n p k] is the log of [{!p_binomial} n p k]. *)
val logp_binomial : anyposint -> anyproba -> int -> 'a neg

(** [p_poisson lbd k] is [Pr(N=k)] where [N ~ P(lbd)]. *)
val p_poisson : 'a anypos -> int -> 'b proba

(** [logp_poisson lbd k] is the log of {!p_poisson} of [lbd k]. *)
val logp_poisson : 'a anypos -> int -> 'b neg

(** [logsump_poisson lbds k] is the log of the sum of {!p_poisson} of [lbd k]
 *  for [lbd] in [lbds]. *)
val logsump_poisson : 'a anypos list -> int -> _float

(** [p_log p k] is [Pr(N=k)]
 *  where [N] is a logarithmic random variate of parameter [p] *)
val p_log : anyproba -> int -> 'a proba

(** [p_negbin r p k] is [Pr(N=k)]
 *  where [N] is a negative binomial random variate of parameters [(r,p)]. *)
val p_negbin : 'a anypos -> anyproba -> int -> 'b proba

(** [p_negbin_over] is {!p_negbin}
 *  with the 'over-dispersion'parameterization. *)
val p_negbin_over : 'a anypos -> 'b anypos -> int -> 'c proba

(** [logp_negbin r p k] is the log of {!p_negbin} of [r p k]. *)
val logp_negbin : 'a anypos -> anyproba -> int -> 'b neg

(** [logp_negbin_over] is {!logp_negbin} with the 'over-dispersion'
 *  parameterization. *)
val logp_negbin_over : 'a anypos -> 'b anypos -> int -> 'c neg

(** [p_n_choice_k ~choose ~among] is the probability of choosing
 *  [choose] among [among] possibilities. *)
val p_n_choice_k : choose:int -> among:int -> 'a proba

(** [logp_n_choice_k ~choose ~among] is the log of {!p_n_choice_k} of
 *  [~choose ~among]. *)
val logp_n_choice_k : choose:int -> among:int -> 'a neg

(** [logd_poisson_process volume k] is the log of the density
 *  of a Poisson process of intensity 1.
 *  with [k] points in a volume [volume]. *)
val logd_poisson_process : 'a anypos -> anyposint -> 'b neg


(** {1:estimate Estimate functions} *)

(** [update_mean_estimate m k v] updates [m] to [((k - 1) m + v) / k]. 
 *  If [m] is the estimate of the mean of [k - 1] observations,
 *  and [v] is a new observation, [m] becomes the estimate
 *  of the mean of [k] observations. *)
val update_mean_estimate :
  Lac.Vec.t ->
  int ->
  Lac.Vec.t ->
    unit


val single_wishart_mle :
  ?y : Lac.Mat.t ->
  Lac.Vec.t ->
  Lac.Vec.t ->
    Lac.Mat.t

(** [update_cov_mle cov m k v] updates [cov],
 *  a covariance estimate from [k - 1] observations,
 *  to an estimate of [k] observations, with [m] the mean (true or estimate),
 *  and [v] the new observation. *)
val update_cov_mle :
  covariance ->
  Lac.Vec.t ->
  int ->
  Lac.Vec.t ->
    unit


(** {1:prng Functions using the PRNG} *)

(** All functions have an optional [rng] argument which defaults to
 *  [Random.get_state ()].
 *  This is useful to maintain multiple simultaneous 
 *  independent random generators instances. *)

type rng = Random.State.t

(** [rng seedo] initializes a PRNG from the given seed (or None) *)
val rng : int option -> Random.State.t

(** [rand_rng rng] initializes a PRNG from random bits obtained from [rng] *)
val rand_rng : rng -> rng

(** [rand_int ~rng top] is a uniform integer variable,
 *  between [0] (inclusive) and [top] (exclusive). *)
val rand_int : rng:rng -> anyposint -> 'a posint

(** [rand_proba ~rng] is a uniform variable between [0.] and [1.] (inclusive). *)
val rand_proba : rng:rng -> 'a proba

(** [rand_float ~rng top] is a uniform variable,
 *  between [0] (exclusive) and [top] (inclusive). *)
val rand_float : rng:rng -> 'a anypos -> 'b pos

(** [rand_bool ~rng] is true or false with probability 0.5 each] *)
val rand_bool : rng:rng -> bool

(** [insist ?ntries ~rng g] keeps calling [g] until [Some] value is obtained,
 *  or [ntries] [None], in which case [Failure] is raised. *)
val insist : ?ntries:anyposint -> rng:rng -> (rng:rng -> 'a option) -> 'a

(** [bound ~rng a b g] draws [x] from [g] until [a <= x < b]. *)
val bound : rng:rng -> float -> float -> (rng:rng -> float) -> float

(** [rand_exp ~rng lbd] is an exponential variable with parameter [lbd]. *)
val rand_exp : rng:rng -> 'a anypos -> 'b pos

(** [rand_normal ~rng m v] is a normal variable with mean [m] and variance [v].
 *  The implementation uses the Box-Muller method. *)
val rand_normal : rng:rng -> float -> 'a anypos -> float

(** [rand_lognormal ~rng ?b m v] is a [b]-lognormal variable
 *  so that its base [b] logarithm is a normal variable
 *  with mean [m] and variance [v]. *)
val rand_lognormal : rng:rng -> ?b:float -> float -> 'a anypos -> float


(** [rand_multi_normal_std ~rng ?store n] is a standard multi-variate normal 
 *  vector of length [n], stored in the (optional) vector [x]. *)
val rand_multi_normal_std :
  rng:rng ->
  ?store:Lac.Vec.t ->
  int ->
    Lac.Vec.t


(** [rand_multi_normal_center ~rng ?store cov] is a multi-variate normal
 *  vector with mean [0] and covariance matrix [cov],
 *  stored in the (optional) vector [store]. *)
val rand_multi_normal_center :
  rng:rng ->
  ?store_std:Lac.Vec.t ->
  ?store:Lac.Vec.t ->
  covariance ->
    Lac.Vec.t


(** [rand_multi_normal ~rng ?store m cov] is a multi-variate normal 
 *  vector with mean [m] and covariance matrix [cov],
 *  stored in the (optional) vector [store]. *)
val rand_multi_normal :
  rng:rng ->
  ?store_std:Lac.Vec.t ->
  ?store:Lac.Vec.t ->
  Lac.Vec.t ->
  covariance ->
    Lac.Vec.t

(** [rand_square ~rng a v] is a square variable of side length [a]
 *  and side variance [v]. *)
val rand_square :
  rng:rng ->
  'a anypos ->
  'b anypos ->
    float * float

(** [rand_bernoulli ~rng p] is a Bernoulli variable with probability [p].
 *  It is [true] with probability [p] and false with probability [1-p]. *)
val rand_bernoulli : rng:rng -> anyproba -> bool

(** [rand_binomial ~rng n p] is a binomial variable with [n] trials
 *  and probability [p]. *)
val rand_binomial : rng:rng -> anyposint -> anyproba -> int

(** [rand_poisson_exp ~rng lbd] is a poisson variable of parameter [lbd]. 
 *  It is generated by successive exponential draws. *)
val rand_poisson_exp : rng:rng -> 'a anypos -> 'b posint

(** [rand_poisson_acrej ~rng lbd] is a poisson variable of parameter [lbd].
 *  It is generated by rejection sampling. *)
val rand_poisson_acrej : rng:rng -> 'a anypos -> 'b posint

(** [rand_poisson ~rng lbd] is a poisson variable of parameter [lbd].
 *  It is generated with {!rand_poisson_exp} for [lbd <= 10]
 *  and with {!rand_poisson_acrej} for [lbd > 10]. *)
val rand_poisson : rng:rng -> 'a anypos -> 'b posint

(** [rand_log ~rng p] is a logarithmic variable of parameter [p]. *)
val rand_log : rng:rng -> anyproba -> 'a posint

(** [rand_negbin ~rng r p] is a negative-binomial variable of parameters [(r,p)]. *)
val rand_negbin : rng:rng -> 'a anypos -> anyproba -> 'b posint

(** [rand_negbin_over ~rng ovd lbd] is {!rand_negbin} 
 *  with a different parameterization :
 *  [lbd] is the mean,
 *  [ovd] the overdispersion *)
val rand_negbin_over : rng:rng -> 'a anypos -> 'b anypos -> 'c posint

(** [rand_geom ~rng p] is a geometric variable with success probability [p].
 *  It is the number of failures before a success. *)
val rand_geom : rng:rng -> anyproba -> 'a posint

(** [rand_cumul_choose ?tot ~rng csp_l] draws a choice from the list [l]
 *  with elements [(choice, cumul_weight)] in increasing order,
 *  and total weight [tot]. *)
val rand_cumul_choose :
  ?tot:'b anypos ->
  rng:rng ->
  ('a * 'c anypos) list ->
    'a

(** [rand_cumul_rev_choose ?tot ~rng csp_l] draws a choice from the list [l]
 *  with elements [(choice, cumul_weight)] in decreasing order,
 *  and total weight [tot]. *)
val rand_cumul_rev_choose :
  ?tot:'b anypos ->
  rng:rng ->
  ('a * 'c anypos) list ->
    'a

(** [rand_choose ~rng cp_l] draws a choice from the list
 *  with corresponding weights. *)
val rand_choose :
  rng:rng ->
  ('a * 'b anypos) list ->
    'a

(** [rand_unif_choose ~rng c_l] draws a choice from the list uniformly. *)
val rand_unif_choose :
  rng:rng ->
  'a list ->
    'a

(** [cumul_multinomial ~rng n csp_l] draws from the multinomial distribution,
 *  with cumulative weights. *)
val cumul_multinomial :
  rng:rng ->
  int ->
  ('a * 'b anypos) list ->
    'a list



(** [multinomial ~rng n cp_l] draws from the multinomial distribution. *)
val multinomial :
  rng:rng ->
  anyposint ->
  ('a * 'b anypos) list ->
    'a list

(** [subsample ~rng p l] is [l] where each element has probability p of being kept *)
val subsample : rng:rng -> anyproba -> 'a list -> 'a list

(** [p_monte_carlo p rand_f n] computes [Pr((p x)= true)]
    for [x] drawn with [rand_f] by Monte Carlo integration of [n] iterations. *)
val p_monte_carlo : ('a -> bool) -> (unit -> 'a) -> anyposint -> 'b proba

val rand_comixture :
  rng:rng ->
  closed_pos array array ->
    int list

(** Precompute [logfact k] for [k <= 256] to be used in an array *)
module Precompute : functor ( ) ->
  sig
    include Precompute.PRE
  end
