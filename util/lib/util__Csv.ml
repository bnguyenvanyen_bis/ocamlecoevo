module L = BatList

type 'a seq = 'a list
type in_channel = Csv.in_channel
type out_channel = Csv.out_channel
type row = Csv.Row.t

let conv_opt = Option.no_fail_opt

let float_of_string = conv_opt float_of_string

let int_of_string = conv_opt int_of_string


module Channel =
  struct
    module In =
      struct
        type t = in_channel

        let open_ path =
          path
          |> open_in
          |> Csv.of_channel ~has_header:true

        let close =
          Csv.close_in

        let transform chan =
          Csv.of_channel ~has_header:true chan

        let standard () = transform stdin
      end

    module Out =
      struct
        type t = out_channel

        let open_ path =
          path
          |> open_out
          |> Csv.to_channel

        let close =
          Csv.close_out

        let transform chan =
          Csv.to_channel chan

        let standard () = transform stdout
      end
  end


let open_in =
  Channel.In.open_

let close_in =
  Channel.In.close

let open_out =
  Channel.Out.open_

let close_out =
  Channel.Out.close


module Row =
  struct
    type t = row

    module Header =
      struct
        let make columns = Csv__.Csv_row.Header.of_names columns
      end

    let make header values =
      Csv__.Csv_row.Row.make header values

    let find row colname =
      match Csv.Row.find row colname with
      | "" ->
          None
      | s ->
          Some s

    let find_exn row colname =
      match Csv.Row.find row colname with
      | "" ->
          invalid_arg "column not found"
      | s ->
          s

    let find_conv conv row colname =
      Option.bind (find row colname) conv


    let fold ~columns f x row =
      (* for each column, call inject on [x] *)
      L.fold_left (fun x' col ->
        match f col with
        | None ->
            x'
        | Some f' ->
            f' (find_exn row col) x'
      ) x columns


    (* we rebuild header on every call to unfold *)
    let unfold ~columns f x =
      let header = Header.make columns in
      let line = L.map (f x) columns in
      make header line
  end


let columns = Csv.Rows.header


let read_first ~f ~chan =
  f (Csv.Rows.next chan)


let read_first_and_fold ~f ~path x =
  let c = open_in path in
  let columns = Csv.Rows.header c in
  let row = Csv.Rows.next c in
  close_in c ;
  Row.fold ~columns f x row


module Make (S : Interfaces.SEQ) =
  struct
    (* FIXME order not preserved ? *)
    let read_all ~f ~chan =
      let g chan =
        match Csv.Rows.next chan with
        | row ->
            Some (chan, f row)
        | exception End_of_file ->
            None
        (* can also raise Csv.Failure *)
      in S.unfold g chan

    let read_all_and_fold ~f ~path x =
      (* a channel with header *)
      let chan = open_in path in
      (* grab the header *)
      let columns = Csv.Rows.header chan in
      let g chan =
        match Csv.Rows.next chan with
        | row ->
            Some (chan, Row.fold ~columns f x row)
        | exception End_of_file ->
            None
        (* also Csv.Failure *)
      in
      let seq = S.unfold g chan in
      close_in chan ;
      seq
      (*
      *)

    (* what about a header ? *)
    let write ~f ~chan seq =
      S.fold (fun x () ->
        x
        |> f
        |> Csv.Row.to_list
        |> Csv.output_record chan
      ) seq ()

    let output ~columns ~extract ~path xs =
      let chan = open_out path in
      Csv.output_record chan columns ;
      write ~f:(Row.unfold ~columns extract) ~chan xs ;
      Csv.close_out chan
  end


let read_all ~f ~chan =
  L.rev (Csv.Rows.fold_left
    ~f:(fun ys row -> (f row) :: ys)
    ~init:[]
    chan
  )


let read_all_and_fold ~f ~path x =
  (* a channel with header *)
  let chan = open_in path in
  (* grab the header *)
  let columns = Csv.Rows.header chan in
  let seq = L.rev (
    Csv.Rows.fold_left
      ~f:(fun ys row -> (Row.fold ~columns f x row) :: ys)
      ~init:[]
      chan
  )
  in
  close_in chan ;
  seq


let write ~f ~chan seq =
  L.iter (fun x ->
    x
    |> f
    |> Csv.Row.to_list
    |> Csv.output_record chan
  ) seq


let output ~columns ~extract ~path xs =
  let chan = open_out path in
  Csv.output_record chan columns ;
  write ~f:(Row.unfold ~columns extract) ~chan xs ;
  Csv.close_out chan


let convert ~columns ~extract ~return ~chan =
  let output a b =
    match extract a b with
    | Some f ->
        let line = L.map f columns in
        Csv.output_record chan line
    | None ->
        ()
  in
  let start a0 b0 =
    Csv.output_record chan columns ;
    output a0 b0
  in
  let out a b =
    output a b
  in
  let return af bf =
    close_out chan ;
    return af bf
  in Out.{ start ; out ; return }
