

type 'a t = 'a option


let some =
  function
  | None ->
      invalid_arg "none"
  | Some x ->
      x


let no_fail_opt f x =
  match f x with
  | y ->
      Some y
  | exception Failure _ ->
      None


let map f =
  function
  | None ->
      None
  | Some x ->
      Some (f x)


let bind xo f =
  match xo with
  | None ->
      None
  | Some x ->
      f x


let map2 f xo yo =
  match xo, yo with
  | None, None
  | Some _, None
  | None, Some _ ->
      None
  | Some x, Some y ->
      Some (f x y)


let map3 f xo yo zo =
  match xo, yo, zo with
  | None, _, _
  | _, None, _
  | _, _, None ->
      None
  | Some x, Some y, Some z ->
      Some (f x y z)
