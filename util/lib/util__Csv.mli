(** CSV input/output.
 *
 *  This is a wrapper for the csv library, with more useful output functions.
 *)

type 'a seq = 'a list
type row = Csv.Row.t


(** {1:channels CSV channels} *)

type in_channel = Csv.in_channel
type out_channel = Csv.out_channel

module Channel :
  sig
    module In :
      sig
        type t = in_channel

        val open_ : string -> t
        
        val close : t -> unit

        val transform : Stdlib.in_channel -> t

        val standard : unit -> t
      end

    module Out :
      sig
        type t = out_channel
       
        val open_ : string -> t

        val close : t -> unit

        val transform : Stdlib.out_channel -> t

        val standard : unit -> t
      end
  end


val open_in : string -> in_channel
val close_in : in_channel -> unit

val open_out : string -> out_channel
val close_out : out_channel -> unit


(** {2:conv Conversion functions} *)


(** [conv_opt f x] is [Some (f x)] or [None] in case of [Failure]. *)
val conv_opt :
  (string -> 'a) ->
  string ->
    'a option


(** [float_of_string] is {!float_of_string} passed through {!conv_opt}. *)
val float_of_string : string -> float option


(** [int_of_string] is {!int_of_string} passed through {!conv_opt}. *)
val int_of_string : string -> int option


(** {3:row CSV rows} *)

module Row :
  sig
    type t = row

    (** [find row col] is [Some] {!Csv.Row.find} of [row col],
     *  or [None] if the column is not found (the result is the empty string). *)
    val find :
      t ->
      string ->
        string option

    (** [find_conv conv row col] is [None] if the column is not found,
     *  or if the conversion [conv] returns [None],
     *  and otherwise if [x] is found, it is [conv x]. *)
    val find_conv :
      (string -> 'a option) ->
      t ->
      string ->
        'a option

    (** [fold f x row] is [f] folded over the columns of [row]. *)
    val fold :
      columns: string list ->
      (string -> (string -> 'a -> 'a) option) ->
      'a ->
      t ->
        'a

    val unfold :
      columns : string list ->
      ('a -> string -> string) ->
      'a ->
        t
  end


val columns : in_channel -> string list


(** [read_first ~f ~chan x] reads the first row from the CSV file at [chan] with [f]. *)
val read_first :
  f:(row -> 'a) ->
  chan: in_channel ->
    'a


val read_first_and_fold :
  f:(string -> (string -> 'a -> 'a) option) ->
  path: string ->
  'a ->
    'a


(** {4:generic Generic input to and output from a foldable
 *   and unfoldable sequence type} *)

module Make (S : Interfaces.SEQ) :
  sig
    (** [read_all ~f ~chan] reads all rows from the CSV file at [chan] with [f]. *)
    val read_all :
      f:(row -> S.elt) ->
      chan: in_channel ->
        S.t

    val read_all_and_fold :
      f:(string -> (string -> S.elt -> S.elt) option) ->
      path: string ->
      S.elt ->
        S.t


    (* this does not really work as an interface : what header should we use ? *)
    (** [write ~f ~chan xs] writes the sequence [xs] to [chan] with [f]. *)
    val write :
      f:(S.elt -> row) ->
      chan: out_channel ->
      S.t ->
        unit

    (** [output ~columns ~extract ~path xs] outputs a CSV file to [path],
     *  with columns given by [columns] and values given by [extract],
     *  with one line per element of [xs]. *)
    val output :
      columns: string list ->
      extract: (S.elt -> string -> string) ->
      path: string ->
      S.t ->
        unit
  end


(** {5:list Input to and output from lists} *)

(** [read_all ~f ~chan] reads all rows from the CSV file at [chan] with [f]. *)
val read_all :
  f:(row -> 'a) ->
  chan: in_channel ->
    'a list

val read_all_and_fold :
  f:(string -> (string -> 'a -> 'a) option) ->
  path: string ->
  'a ->
    'a list


(* this does not really work as an interface : what header should we use ? *)
(** [write ~f ~chan xs] writes the sequence [xs] to [chan] with [f]. *)
val write :
  f:('a -> row) ->
  chan: out_channel ->
  'a list ->
    unit


(** [output ~columns ~extract ~path xs] outputs a CSV file to [path],
 *  with columns given by [columns] and values given by [extract],
 *  with one line per element of [xs]. *)
val output :
  columns: string list ->
  extract: ('a -> string -> string) ->
  path: string ->
  'a list ->
    unit


(** [convert ~columns ~extract ~chan] creates a CSV [output] to
 *  the channel [chan],
 *  with the columns given by [columns] and values given by [extract].
 *  It outputs on calling start and out,
 *  and closes [chan] on return. *)
val convert :
  columns: string list ->
  extract: ('a -> 'b -> (string -> string) option) ->
  return : ('a -> 'b -> 'c) ->
  chan: out_channel ->
  ('a, 'b, 'c) Out.t
