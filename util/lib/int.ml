

type 'a t = int


type _int = [ `Int ]

type 'a anyint = [< `Int | `Pos | `Neg > `Int] as 'a

type 'a pos = [< `Int | `Pos > `Int ] as 'a

type anypos = [ `Int | `Pos ]

type 'a neg = [< `Int | `Neg > `Int ] as 'a

type anyneg = [ `Int | `Neg ]



let of_int n = n [@@ inline]

let of_int_unsafe n = n [@@ inline]

let to_int n = n [@@ inline]

let to_float n = Float.of_float (float n)

let of_float x = int_of_float (Float.to_float x)

let narrow x = x [@@ inline]

let promote_unsafe x = x [@@ inline]


let zero = 0

let one = 1

let two = 2

let neg_one = ~- 1


let succ = succ

let pred = pred

let neg = (~-)

let add = (+)

let sub = (-)

let mul = ( * )

let div = (/)

let to_string = string_of_int

let compare (i : int) (i' : int) = compare i i'


module Op =
  struct
    let (~+) = of_int
    let (~-) = neg

    let (+) = add
    let (-) = sub
    let ( * ) = mul
    let (/) = div

    let (mod) = (mod)

    let (=) (i : int) (i' : int) = (i = i')
    let (<>) (i : int) (i' : int) = (i <> i')
    let (<) (i : int) (i' : int) = (i < i')
    let (<=) (i : int) (i' : int) = (i <= i')
    let (>) (i : int) (i' : int) = (i > i')
    let (>=) (i : int) (i' : int) = (i >= i')
  end


module Pos =
  struct
    let of_int n =
      if n >= 0 then
        n
      else
        invalid_arg "Strictly negative"

    let of_anyint = of_int

    let to_float n = Float.of_float_unsafe (float n)

    let of_float x = int_of_float (Float.to_float x)

    let narrow n = n [@@ inline]

    let succ = succ

    let pred n =
      if n > 0 then
        pred n
      else
        invalid_arg "Int.Pos.pred : 0"

    let neg = neg

    let add = add

    let mul = mul

    let div = div

    let factorial = Base.factorial

    let logfact k : 'a Float.pos Float.t =
      Float.of_float_unsafe (Base.logfact k)

    let fold = Base.int_fold

    let list_init ~f ~n =
      BatList.init n f

    let array_init ~f ~n =
      BatArray.init n f

    module Op =
      struct
        let (~-) = neg

        let (+) = add
        let ( * ) = mul
        let (/) = div

        let (mod) = (mod)
      end
  end


module Neg =
  struct
    let of_int n =
      if n < 0 then
        n
      else
        invalid_arg "Positive"

    let to_float n = Float.of_float_unsafe (float n)

    let narrow x = x


    let one = (~- 1)

    let pred = pred

    let neg = neg

    let add = add

    let mul = mul


    module Op =
      struct
        let (~-) = neg

        let (+) = add
        let ( * ) = mul
      end
  end
