open Cmdliner


let positive () =
  let parse_float = Arg.(conv_parser float) in
  let print_float = Arg.(conv_printer float) in
  let parse s =
    match parse_float s with
    | Ok x ->
        begin match Float.Pos.of_float x with
        | y ->
            Ok y
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | Error msg ->
        Error msg
  in
  let print fmt x =
    print_float fmt (Float.to_float x)
  in
  Arg.conv ~docv:"POS" (parse, print)


let proba () =
  let parse_float = Arg.(conv_parser float) in
  let print_float = Arg.(conv_printer float) in
  let parse s =
    match parse_float s with
    | Ok x ->
        begin match Float.Proba.of_float x with
        | y ->
            Ok y
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | Error msg ->
        Error msg
  in
  let print fmt x =
    print_float fmt (Float.to_float x)
  in
  Arg.conv ~docv:"PROBA" (parse, print)


let posint () =
  let parse_int = Arg.(conv_parser int) in
  let print_int = Arg.(conv_printer int) in
  let parse s =
    match parse_int s with
    | Ok n ->
        begin match Int.Pos.of_int n with
        | m ->
            Ok m
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | Error msg ->
        Error msg
  in
  let print fmt x =
    print_int fmt (Int.to_int x)
  in
  Arg.conv ~docv:"POSINT" (parse, print)


(*
let pos_float () =
  let pos = positive () in
  let parse_pos = Arg.conv_parser pos in
  let parse s =
    match parse_pos s with
    | Ok x ->
        Ok (F.to_float x)
    | e ->
        e
  in
  let print = Arg.(conv_printer float) in
  Arg.conv ~docv:"POS" (parse, print)
*)
