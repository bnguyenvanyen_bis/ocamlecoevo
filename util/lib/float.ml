type 'a t = float

type _float = [ `Float ]

type 'a anyfloat = [< `Float | `Neg | `Pos | `Proba > `Float ] as 'a

type 'a pos = [< `Float | `Pos > `Float ] as 'a

type 'a anypos = [< `Float | `Pos | `Proba > `Float `Pos ] as 'a

type 'a proba = 'a anypos

type anyproba = [ `Float | `Pos | `Proba ]

type 'a neg = [< `Float | `Neg > `Float ] as 'a

type anyneg = [ `Float | `Neg ]

type 'a anynoneg = [< `Float | `Pos | `Proba > `Float] as 'a

type closed_pos = [ `Float | `Pos ]

type identity =
  | Zero : _ anyfloat t -> identity
  | Proba : anyproba t -> identity
  | Pos : _ anypos t -> identity
  | Neg : anyneg t -> identity


let of_float x = x [@@ inline]

let of_float_unsafe x = x [@@ inline]

let to_float x = x [@@ inline]

let narrow x = x [@@ inline]

let promote_unsafe x = x [@@ inline]

let identify x =
  if x = 0. then
    Zero x
  else if x < 0. then
    Neg x
  else if x < 1. then
    Proba x
  else
    Pos x


let zero = 0.

let one = 1.

let two = 2.

let pi = Base.pi

let neg_one = ~-. 1.

let infinity = infinity

let neg_infinity = neg_infinity

let nan = nan


let neg = (~-.)

let invert x = 1. /. x

let add = (+.)

let sub = (-.)

let mul = ( *. )

let div = (/.)

let abs = abs_float

let sq = Base.sq

let exp = exp

let cos = cos

let sin  = sin
 
let tan = tan

let pow = Base.pow

let log = log

let log1p = log1p


let classify = classify_float

let to_string = string_of_float

let of_string = Option.no_fail_opt float_of_string

let compare (x : float) (x' : float) = compare x x'

let min (x : float) (x' : float) = min x x'

let max (x : float) (x' : float) = max x x'

let positive_part x = max 0. x

let nearly_equal ?(tol=1e-9) x y =
  abs (x -. y) < tol

let compare_approx ?tol x y =
  if nearly_equal ?tol x y then
    0
  else
    compare x y


module Op =
  struct
    (* might be confusing, but convenient *)
    let (~+) = of_float
    let (~-) = neg

    let (+) = add
    let (-) = sub
    let ( * ) = mul
    let (/) = div

    let (=) (x : float) (x' : float) = (x = x')
    let (=~) (x : float) (x' : float) = (nearly_equal x x')
    let (<>) (x : float) (x' : float) = (x <> x')
    let (<) (x : float) (x' : float) = (x < x')
    let (<=) (x : float) (x' : float) = (x <= x')
    let (>) (x : float) (x' : float) = (x > x')
    let (>=) (x : float) (x' : float) = (x >= x')
  end


module Pos =
  struct
    let of_float x =
      if x >= 0. then
        x
      else
        invalid_arg "Strictly negative"

    let of_string s = Option.map of_float (of_string s)

    let of_anyfloat = of_float

    let narrow x = x [@@ inline]

    let close x = x [@@ inline]

    let neg = neg

    let invert = invert

    let add = add

    let mul = mul

    let div = div

    let sqrt = sqrt

    let log = log

    let log1p = log1p

    let pow = pow

    let expow x y = x ** y

    let gamma = Base.gamma

    let bcos b x = b +. b *. cos x

    let bsin b x = b +. b *. sin x

    let proportions a b =
      let tot = a +. b in
      (tot, a /. tot, b /. tot)


    let min = min

    let max = max

    module Op =
      struct
        let (~-) = neg

        let (+) = add

        let ( * ) = mul

        let (/) = div

        let ( ** ) = expow
      end
  end


module Proba =
  struct
    let of_float x =
      if x <= 1. then
        Pos.of_float x
      else
        invalid_arg "Bigger than 1"
    
    let of_anyfloat = of_float

    let of_pos x =
      if x <= 1. then
        x
      else
        invalid_arg "Bigger than 1"

    let of_string s = Option.map of_float (of_string s)

    let narrow x = x [@@ inline]

    let compl p = (1. -. p)

    let mul = mul

    let log = log

    module Op =
      struct
        let ( * ) = mul
      end
  end


module Neg =
  struct
    let of_float x =
      if x < 0. then
        x
      else
        invalid_arg "Positive"

    let of_anyfloat = of_float

    let narrow x = x [@@ inline]

    let one = neg_one

    let infinity = neg_infinity

    let neg = neg

    let add = add

    let mul = mul

    let div = div

    let exp = exp

    module Op =
      struct
        let (+) = add
        let ( * ) = mul
        let (/) = div
      end
  end


(*
module More =
  struct
    (* not very safe *)

    let smallest ?(n=20) p t =
      (* FIXME problem if t = 0. : what should we do ? *)
      assert (t < infinity) ;
      let rec f n t =
        match n with
        | 0 -> failwith "exhausted payload"
        | _ ->
          let t' = two *. t in
          if p t' then
            t'
          else
            f (n - 1) t'
      in
      if t <> zero then
        f n t
      else
        f n 1e-1

    let bissect ?(tol=1e-8) p t t' =
      assert (t < t') ;
      assert (t' < infinity) ;
      let rec f t t' =
        if ((t' -. t) < tol) then t' else
        let t'' = ((t +. t') /. two) in
        if p t'' then
          f t t''
        else
          f t'' t'
      in f t t'

    module Pos =
      struct
        let smallest = smallest

        let bissect = bissect
      end
  end
*)
