

type ('a, 'b, 'c) t = {
  start : ('a -> 'b -> unit) ;
  out : ('a -> 'b -> unit) ;
  return : ('a -> 'b -> 'c) ;
}


let convert_null =
  let start _ _ =
    ()
  in
  let out _ _ =
    ()
  in
  let return a b =
    (a, b)
  in
  { start ; out ; return }


let combine f o o' = {
    start = (fun a b -> o.start a b ; o'.start a b) ;
    out = (fun a b -> o.out a b ; o'.out a b) ;
    return = (fun a b -> f (o.return a b) (o'.return a b)) ;
}


let combine_tuple f o o' = {
    start = (fun a (b, b') -> o.start a b ; o'.start a b') ;
    out = (fun a (b, b') -> o.out a b ; o'.out a b') ;
    return = (fun a (b, b') -> f (o.return a b) (o'.return a b')) ;
}


