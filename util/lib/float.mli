(** Representation of float with a phantom type,
 *  and additionally distinction of positive, negative, and probability values,
 *  through open variant types shenanigans.
 *
 *  Each module presents the functions relevant to it
 *  (taking values from its type as argument),
 *  and less particular functions (like {!Pos.add}),
 *  are compatible with more specific types (a [proba t]).
 *  while being conservative (returning a positive value in all cases).
 *
 *  A thing that doesn't work so well is unification of types,
 *  especially through recursive functions.
 *  a [proba t] can't be unified with a [pos t]
 *  (if we try to make it possible, types flow the wrong way, and a [pos t]
 *  can become a [proba t]...).
 *  Explicit (safe) cast functions are provided
 *  as the {!narrow} and {!Pos.narrow}, to make this possible.
 *
 *  Some functions check that the input is as it should to not break invariants
 *  - {!Pos.of_float} checks that the input is [>= 0.]
 *  - {!Proba.of_float} checks that the input is in [[0., 1.]]
 *  - {!Proba.of_pos} checks that the input is [<= 1.]
 *  - {!Neg.of_float} checks that the input is [< 0.]
 *
 *  A few unsafe functions are also exposed (they have [_unsafe] in their name).
 *  - {!of_float_unsafe} to obtain a [t] type that can be unified with any other,
 *    from a [float]
 *  - {!promote_unsafe} to obtain a [t] type that can be unified with any other,
 *    from a less general [t]
 *
 *  Operators (unary and infix) are provided in Op modules,
 *  {!Op}, {!Pos.Op}, {!Proba.Op} and {!Neg.Op}.
 *)


(** phantom type for float *)
type 'a t

(** unknown float type *)
type _float = [ `Float ]

(** any kind of float : most permissive type and so unsafe to give to values 
 *  (the only value really of this type is 0.) *)
type 'a anyfloat = [< `Float | `Neg | `Pos | `Proba > `Float ] as 'a

(** unknown positive type *)
type 'a pos = [< `Float | `Pos > `Float ] as 'a

(** any kind of positive value and so unsafe to give to values
 *  (the values of this type are the values in [[0., 1.]]) *)
type 'a anypos = [< `Float | `Pos | `Proba > `Float `Pos ] as 'a

type 'a proba = 'a anypos

(** any proba (actually a closed type) *)
type anyproba = [ `Float | `Pos | `Proba ]

(** unknown negative type *)
type 'a neg = [< `Float | `Neg > `Float ] as 'a

(** any kind of negative value *)
type anyneg = [ `Float | `Neg ]


(** weirder types that we need sometimes... *)
type 'a anynoneg = [< `Float | `Pos | `Proba > `Float] as 'a
type closed_pos = [ `Float | `Pos ]


(** an existential type to identify in which class we fall into *)
type identity =
  | Zero : _ anyfloat t -> identity
  | Proba : anyproba t -> identity
  | Pos : _ anypos t -> identity
  | Neg : anyneg t -> identity


(** [of_float x] is 'only' a [_float t] *)
val of_float : float -> _float t

(** [of_float_unsafe x] is 'any kind of' [t]. UNSAFE ! *)
val of_float_unsafe : float -> 'a anyfloat t

val to_float : 'a anyfloat t -> float

(** [narrow x] is 'only' a [_float t] *)
val narrow : 'a anyfloat t -> _float t

(** [promote_unsafe x] is 'any kind of' float. UNSAFE ! *)
val promote_unsafe : 'a anyfloat t -> 'b anyfloat t


(** [identify x] is the {!identity} of [x]. *)
val identify : 'a anyfloat t -> identity


(** [zero] is [0.]. It can be used as any kind of float. *)
val zero : 'a anyfloat t

(** [one] is [1.]. It can be used as any kind of positive value. *)
val one : 'a proba t

(** [two] is [2.]. *)
val two : 'a pos t

(** [pi] is [4. *. atan 1.] *)
val pi : 'a pos t

(** [neg_one] is [~-. 1.] *)
val neg_one : 'a neg t

val infinity : 'a pos t

val neg_infinity : 'a neg t

val nan : _float t


(** [neg x] is [~-. x] *)
val neg : 'a anyfloat t -> _float t

(** [invert x] is [1. /. x] *)
val invert : 'a anyfloat t -> _float t

(** [add x y] is [x +. y] *)
val add : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [sub x y] is [x -. y] *)
val sub : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [mul x y] is [x *. y] *)
val mul : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [div x y] is [x /. y] *)
val div : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [abs x] is [abs_float x] *)
val abs : 'a anyfloat t -> 'b pos t

(** [sq x] is [x ** 2.], the result is positive *)
val sq : 'a anyfloat t -> 'b pos t

(** [exp x] is the exponential of [x], the result is positive *)
val exp : 'a anyfloat t -> 'b pos t

(** [cos x] is the cosine of [x] *)
val cos : 'a anyfloat t -> _float t

(** [sin x] is the sine of [x] *)
val sin : 'a anyfloat t -> _float t

(** [tan x] is the tangent of [x] *)
val tan : 'a anyfloat t -> _float t

(** [pow x k] is [x] to the power [k] *)
val pow : 'a anyfloat t -> int -> _float t

(** [log x] *)
val log : 'a anyfloat t -> _float t

(** [log1p x] *)
val log1p : 'a anyfloat t -> _float t


(** [classify x] is [classify_float (to_float x)] *)
val classify : 'a anyfloat t -> fpclass

(** [to_string x] is {!string_of_float} [x] *)
val to_string : 'a anyfloat t -> string

(** [of_string s] is [Some {!float_of_string} s] if it returns,
 *  and [None] if it raises [Failure]. *)
val of_string : string -> _float t option

(** [compare] is the comparison function on floats. *)
val compare : 'a anyfloat t -> 'b anyfloat t -> int

(** [compare_approx ~tol] is a comparison function
  * where [x] and [x'] compare equal if they are apart by less than [tol]. *)
val compare_approx :
  ?tol : 'a anypos t ->
  'b anyfloat t ->
  'c anyfloat t ->
    int

(** [min x x'] is the smallest of [x] and [x']. *)
val min : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [max x x'] is the greater of [x] and [x']. *)
val max : 'a anyfloat t -> 'b anyfloat t -> _float t

(** [positive_part x] is [x] if it is positive, or else [zero]. *)
val positive_part : 'a anyfloat t -> 'b pos t

(** [nearly_equal ?tol x x'] is [true] iff [|x - x'| < tol]. *)
val nearly_equal :
  ?tol : 'a anypos t ->
  'b anyfloat t ->
  'c anyfloat t ->
    bool


module Op :
  sig
    (** {1:unary Unary operators} *)

    (** [(~+)] is {!of_float} *)
    val (~+) : float -> _float t

    (** [(~-)] is {!neg} *)
    val (~-) : 'a anyfloat t -> _float t

    (** {2:infix Infix operators} *)

    (** [(+)] is {!add} *)
    val (+) : 'a anyfloat t -> 'b anyfloat t -> _float t

    (** [(-)] is {!sub} *)
    val (-) : 'a anyfloat t -> 'b anyfloat t -> _float t

    (** [( * )] is {!mul} *)
    val ( * ) : 'a anyfloat t -> 'b anyfloat t -> _float t

    (** [(/)] is {!div} *)
    val (/) : 'a anyfloat t -> 'b anyfloat t -> _float t

    (** Comparisons without type unification *)

    (** [(=)] is the equality operator *)
    val (=) : 'a anyfloat t -> 'b anyfloat t -> bool

    (** [(<>)] is the non-equality operator *)
    val (<>) : 'a anyfloat t -> 'b anyfloat t -> bool

    (** [(=~)] is the approximate equality operator *)
    val (=~) : 'a anyfloat t -> 'b anyfloat t -> bool


    (** [(<)] is the strictly-smaller operator *)
    val (<) : 'a anyfloat t -> 'b anyfloat t -> bool

    (** [(<=)] is the smaller-or-equal operator *)
    val (<=) : 'a anyfloat t -> 'b anyfloat t -> bool

    (** [(>)] is the strictly-greater operator *)
    val (>) : 'a anyfloat t -> 'b anyfloat t -> bool

    (** [(>=)] is the greater-or-equal operator *)
    val (>=) : 'a anyfloat t -> 'b anyfloat t -> bool
  end


module Pos :
  sig
    (** [of_float x] is [x] as a [pos t] if [x] is positive ([>= 0.]),
     *  else [Invalid_argument] is raised. *)
    val of_float : float -> 'a pos t

    (** [of_string s] is {!of_string} of [s] if the result is positive,
     *  else [Invalid_argument] is raised. *)
    val of_string : string -> 'a pos t option

    (** [of_anyfloat x] is [x] as a [pos t] if [x] is positive ([>= 0.]),
     *  else [Invalid_argument] is raised.  *)
    val of_anyfloat : 'a anyfloat t -> 'b pos t

    (** [narrow x] is [x] as "only" a [minpos t] *)
    val narrow : 'a anypos t -> 'b pos t

    val close : 'a anypos t -> closed_pos t


    (** [neg] is {!neg} with a negative result *)
    val neg : 'a anypos t -> 'b neg t

    (** [invert] is {!invert} on positive numbers *)
    val invert : 'a anypos t -> 'b pos t

    (** [add] is {!add} with a positive result *)
    val add : 'a anypos t -> 'b anypos t -> 'c pos t

    (** [mul] is {!mul} with a positive result *)
    val mul : 'a anypos t -> 'b anypos t -> 'c pos t

    (** [div] is {!div} with a positive result *)
    val div : 'a anypos t -> 'b anypos t -> 'c pos t

    (** [sqrt x] is the square root of [x] *)
    val sqrt : 'a anypos t -> 'b pos t

    (** [log x] is the logarithm of [x] *)
    val log : 'a anypos t -> _float t

    (** [log1p x] is [log (1 + x)] *)
    val log1p : 'a anypos t -> 'b pos t

    (** [pow] is {!pow} with a positive result *)
    val pow : 'a anypos t -> int -> 'b pos t

    (** [expow x y] is [x ** y = exp (y log x)] *)
    val expow : 'a anypos t -> 'b anyfloat t -> 'c pos t

    (** [gamma] is {!Base.gamma} with a positive argument and result *)
    val gamma : 'a anypos t -> 'b pos t

    (** [bcos b x] is [b + b cos x] *)
    val bcos : 'a anypos t -> 'b anyfloat t -> 'c pos t

    (** [bsin b x] is [b + b sin x] *)
    val bsin : 'a anypos t -> 'b anyfloat t -> 'c pos t

    (** [proportions a b] is [(a + b, a / (a + b), b / (a + b))] *)
    val proportions : 'a anypos t -> 'b anypos t ->
      'c pos t * 'd proba t * 'e proba t

    (** [min] is {!min} for positive values. *)
    val min : 'a anypos t -> 'b anypos t -> 'c pos t

    (** [max] is {!max} for positive values. *)
    val max : 'a anypos t -> 'b anypos t -> 'c pos t

    module Op :
      sig
        val (~-) : 'a anypos t -> 'b neg t

        val (+) : 'a anypos t -> 'b anypos t -> 'c pos t
        val ( * ) : 'a anypos t -> 'b anypos t -> 'c pos t
        val (/) : 'a anypos t -> 'b anypos t -> 'c pos t

        val ( ** ) : 'a anypos t -> 'b anyfloat t -> 'c pos t
      end
  end


module Proba :
  sig
    (** [of_float x] is [x] as a [proba t] if [x] is positive and inferior
     *  to [1.], else [Invalid_argument] is raised. *)
    val of_float : float -> 'a proba t

    (** [of_anyfloat x] is [x] as a [proba t]
     *  if [x] is positive ([>= 0.]) and inferior to [1.],
     *  else [Invalid_argument] is raised.  *)
    val of_anyfloat : 'a anyfloat t -> 'b proba t

    (** [of_pos x] is [x] as a [proba t] if [x] is inferior to [one],
     *  else [Invalid_argument] is raised. *)
    val of_pos : 'a anypos t -> 'b proba t

    (** [of_string s] is {!of_string} of [s] if the result is positive,
     *  else [Invalid_argument] is raised. *)
    val of_string : string -> 'a proba t option

    val narrow : anyproba t -> 'a proba t

    (** [compl p] is [1. -. p] *)
    val compl : anyproba t -> 'a proba t


    (** [mul] is {!mul} *)
    val mul : anyproba t -> anyproba t -> 'a proba t

    (** [log p] is the logarithm of p, and is negative *)
    val log : anyproba t -> 'a neg t


    module Op :
      sig
        val ( * ) : anyproba t -> anyproba t -> 'a proba t
      end
  end


module Neg :
  sig
    (** [of_float x] is [x] if [x] is strictly negative,
     *  else [Invalid_argument] is raised. *)
    val of_float : float -> 'a neg t

    (** [of_anyfloat x] is [x] as a [neg t]
     *  if [x] is negative ([<= 0.]),
     *  else [Invalid_argument] is raised.  *)
    val of_anyfloat : 'a anyfloat t -> 'b proba t

    val narrow : anyneg t -> 'a neg t

    (** [one] is {!neg_one} *)
    val one : anyneg t

    (** [infinity] is {!neg_infinity} *)
    val infinity : anyneg t


    val neg : anyneg t -> 'a pos t


    val add : anyneg t -> anyneg t -> 'a neg t

    val mul : anyneg t -> anyneg t -> 'a pos t

    val div : anyneg t -> anyneg t -> 'a pos t

    (** [exp x] is the exponential of [x] and is between [0.] and [1.] *)
    val exp : anyneg t -> 'a proba t


    module Op :
      sig
        val (+) : anyneg t -> anyneg t -> 'a neg t
        val ( * ) : anyneg t -> anyneg t -> 'a pos t
        val (/) : anyneg t -> anyneg t -> 'a pos t
      end
  end


(* I managed to write a safe version for those directly in Sim.Loop
(** Additional useful functions *)
module More :
  sig
    val smallest :
      ?n:int ->
      ('a anyfloat t -> bool) ->
      'b anyfloat t ->
        _float t

    val bissect :
      ?tol:'a anypos t ->
      ('b anyfloat t -> bool) ->
      'c anyfloat t ->
      'd anyfloat t ->
        _float t

    module Pos :
      sig
        val smallest :
          ?n:int ->
          ('a anypos t -> bool) ->
          'b anypos t ->
            'c pos t

        val bissect :
          ?tol:'a anypos t ->
          ('b anypos t -> bool) ->
          'c anypos t ->
          'd anypos t ->
            'e pos t
      end
  end
*)
