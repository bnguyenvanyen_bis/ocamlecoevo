

type 'a t


type _int = [ `Int ]

type 'a anyint = [< `Int | `Pos | `Neg > `Int] as 'a

type 'a pos = [< `Int | `Pos > `Int ] as 'a

type anypos = [ `Int | `Pos ]

type 'a neg = [< `Int | `Neg > `Int ] as 'a

type anyneg = [ `Int | `Neg ]


val of_int : int -> _int t

val of_int_unsafe : int -> 'a anyint t

val to_int : 'a anyint t -> int

val to_float : 'a anyint t -> Float._float Float.t

val of_float : 'a Float.anyfloat Float.t -> _int t

val narrow : 'a anyint t -> _int t

val promote_unsafe : 'a anyint t -> 'b anyint t


val zero : 'a pos t

val one : 'a pos t

val two : 'a pos t

val neg_one : 'a neg t


val succ : 'a anyint t -> _int t

val pred : 'a anyint t -> _int t

val neg : 'a anyint t -> _int t


val add : 'a anyint t -> 'b anyint t -> _int t

val sub : 'a anyint t -> 'b anyint t -> _int t

val mul : 'a anyint t -> 'b anyint t -> _int t

val div : 'a anyint t -> 'b anyint t -> _int t

val to_string : 'a anyint t -> string

val compare : 'a anyint t -> 'b anyint t -> int


module Op :
  sig
    val (~+) : int -> _int t
    val (~-) : 'a anyint t -> _int t

    val (+) : 'a anyint t -> 'b anyint t -> _int t
    val (-) : 'a anyint t -> 'b anyint t -> _int t
    val ( * ) : 'a anyint t -> 'b anyint t -> _int t
    val (/) : 'a anyint t -> 'b anyint t -> _int t

    val (mod) : 'a anyint t -> 'b anyint t -> _int t

    val (=) : 'a anyint t -> 'b anyint t -> bool
    val (<>) : 'a anyint t -> 'b anyint t -> bool
    val (<) : 'a anyint t -> 'b anyint t -> bool
    val (<=) : 'a anyint t -> 'b anyint t -> bool
    val (>) : 'a anyint t -> 'b anyint t -> bool
    val (>=) : 'a anyint t -> 'b anyint t -> bool
  end


module Pos :
  sig
    val of_int : int -> 'a pos t

    val of_anyint : 'a anyint t -> 'b pos t

    val to_float : anypos t -> 'a Float.pos Float.t

    val of_float : 'a Float.anypos Float.t -> 'b pos t

    val narrow : anypos t -> 'a pos t


    val succ : anypos t -> 'a pos t

    (* not free : checks *)
    val pred : anypos t -> 'a pos t

    val neg : anypos t -> 'a neg t


    val add : anypos t -> anypos t -> 'a pos t

    val mul : anypos t -> anypos t -> 'a pos t


    val factorial : anypos t -> 'a pos t

    val logfact : anypos t -> 'a Float.pos Float.t

    val fold :
      f:('a -> anypos t -> 'a) ->
      x:'a ->
      n:anypos t ->
        'a

    val list_init :
      f:(anypos t -> 'a) ->
      n:anypos t ->
        'a list

    val array_init :
      f:(anypos t -> 'a) ->
      n:anypos t ->
        'a array

    module Op :
      sig
        val (~-) : anypos t -> 'a neg t

        val (+) : anypos t -> anypos t -> 'a pos t
        val ( * ) : anypos t -> anypos t -> 'a pos t
        val (/) : anypos t -> anypos t -> 'a pos t

        val (mod) : anypos t -> anypos t -> 'a pos t
      end
  end


module Neg :
  sig
    val of_int : int -> 'a neg t

    val to_float : anyneg t -> 'a Float.neg Float.t

    val narrow : anyneg t -> 'a neg t

    
    val one : anyneg t


    val pred : anyneg t -> 'a neg t

    val neg : anyneg t -> 'a pos t


    val add : anyneg t -> anyneg t -> 'a neg t

    val mul : anyneg t -> anyneg t -> 'a pos t

    module Op :
      sig
        val (~-) : anyneg t -> 'a pos t

        val (+) : anyneg t -> anyneg t -> 'a neg t
        val ( * ) : anyneg t -> anyneg t -> 'a neg t
      end
  end
