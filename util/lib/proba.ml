(******* Functions about random distributions *******)
open Sig

module B = Base
module F = Float


let d_unif_int a b x : 'a F.pos F.t =
  if (a <= x) && (x < b) then
    F.of_float_unsafe (1. /. float (b - a))
  else
    F.zero

let logd_unif_int a b x : F._float F.t =
  if (a <= x) && (x < b) then
    F.of_float (~-. (log (float (b - a))))
  else
    begin
      (* Printf.eprintf "x=%i outside [a,b]=[%i,%i]\n" x a b ; *)
      F.neg_infinity
    end

let d_unif a b x : 'a F.pos F.t =
  if (a <= x) && (x <= b) then
    F.of_float_unsafe (1. /. (b -. a))
  else
    F.zero

let logd_unif a b x : F._float F.t =
  if (a <= x) && (x <= b) then
    F.neg (F.Pos.log (F.of_float_unsafe (b -. a)))
  else
    begin
      (* Printf.eprintf "x=%f outside [a,b]=[%f,%f]\n" x a b ; *)
      F.neg_infinity
    end
  

let d_exp (lambda : 'a F.anypos F.t) x : 'b F.pos F.t =
  (* what about if lambda < 0. ? *)
  if x >= 0. then
    (* lambda * exp (- lambda * x) *)
    x |> F.of_float |> F.mul lambda |> F.neg |> F.exp |> F.Pos.mul lambda
  else
    F.zero


let logd_exp (lambda : 'a F.anypos F.t) x : F._float F.t =
  (* if lambda = 0., then the distribution is identically 0.,
   * and returns neg_infinity everywhere *)
  if x >= 0. then
    F.sub (F.Pos.log lambda) (F.mul lambda (F.of_float x))
  else
    begin
      (* FIXME this happens a lot *)
      (* Printf.eprintf "x=%f or lambda=%f <= 0.\n" x lambda ; *)
      F.neg_infinity
    end


let d_normal m v x = F.(
  let sqxm = sq (of_float (x -. m)) in
  let e = Pos.Op.(sqxm / (two * v)) |> neg |> exp in
  let s = Pos.sqrt Pos.Op.(two * pi * v) in
  Pos.Op.(e / s)
)

let p_normal m v x =
  let nx = (x -. m) /. sqrt (2. *. F.to_float v) in
  F.of_float_unsafe ((1. +. Erf.erf nx) /. 2.)


let logd_normal m v x = F.(
  let l2pv = Pos.log Pos.Op.(two * pi * v) in
  let sqxm = sq (of_float (x -. m)) in
  let a = Op.(l2pv / two) in
  let b = Pos.Op.(sqxm / (two * v)) in
  Op.(~- (a + b))
)


let logsumd_normal m v xs = F.(
  let l2pv = Pos.log Pos.Op.(two * pi * v) in
  let a = Op.(l2pv / two) in
  let renorm_xs = L.map (fun x ->
    let sqxm = sq (of_float (x -. m)) in
    to_float (neg Pos.Op.(sqxm / (two * v)))
  ) xs
  in
  let n = xs |> L.length |> Int.Pos.of_int |> Int.Pos.to_float in
  Op.(of_float (B.log_sum_exp renorm_xs) - Pos.log n - a)
)


let d_lognormal ?(b=10.) m v x =
  if x > 0. then
    let y = log x /. log b in
    d_normal m v y
  else
    F.zero


let logd_lognormal ?(b=10.) m v x =
  if x > 0. then
    let y = log x /. log b in
    logd_normal m v y
  else
    F.neg_infinity


let d_square a v =
  let a' = F.to_float a in
  let d0 = d_normal 0. v in
  let da = d_normal a' v in
  let four = F.of_float_unsafe 4. in
  let d (x, y) =
    let d00a = if (x >= 0.) && (x <= a') then d0 y else F.zero in
    let d0a0 = if (y >= 0.) && (y <= a') then d0 x else F.zero in
    let daaa = if (y >= 0.) && (y <= a') then da x else F.zero in
    let d0aa = if (x >= 0.) && (x <= a') then da y else F.zero in
    F.Pos.Op.((d00a + d0a0 + daaa + d0aa) / four)
  in d


let logd_square a v =
  let a' = F.to_float a in
  let ld0 = logd_normal 0. v in
  let lda = logd_normal a' v in
  let ld (x, y) =
    let ld00a =
      if (x >= 0.) && (x <= a') then
        F.to_float (ld0 y)
      else
        neg_infinity
    in
    let ld0a0 =
      if (y >= 0.) && (y <= a') then
        F.to_float (ld0 x)
      else
        neg_infinity
    in
    let ldaaa =
      if (y >= 0.) && (y <= a') then
        F.to_float (lda x)
      else
        neg_infinity
    in
    let ld0aa =
      if (x >= 0.) && (x <= a') then
        F.to_float (lda y)
      else
        neg_infinity
    in
    F.of_float (B.log_sum_exp [ld00a ; ld0a0 ; ldaaa ; ld0aa] -. log 4.)
  in ld


(* discrete *)


let p_bernoulli p b : 'a F.proba F.t =
  if b then F.Proba.narrow p else F.Proba.compl p


let logp_bernoulli p b =
  if b then
    p |> F.Proba.log
  else
    p |> F.Proba.compl |> F.Proba.log


let p_binomial n p k =
  if k >= 0 then
    let p' = F.to_float p in
    let n' = Int.to_int n in
    let newbin = B.binomial_coeff k n' in
    F.of_float_unsafe (
      float newbin *. (B.pow p' k) *. (B.pow (1. -. p') (n' - k))
    )
  else
    F.zero


(* FIXME claiming this is negative (unsafe) *)
let logp_binomial_with logbin_coeff n p k : 'a F.neg F.t =
  if k >= 0 then
    let p' = F.to_float p in
    let n' = Int.to_int n in
    let logbin = logbin_coeff k n' in
    F.of_float_unsafe (logbin +. (float k) *. log p' +. (float (n' - k)) *. log (1. -. p'))
  else
    F.neg_infinity


let logp_binomial n p k =
  logp_binomial_with B.logbin_coeff n p k


let p_poisson lbd k : 'a F.proba F.t = F.(
  if k >= 0 then
    let fk = k |> B.factorial |> float |> of_float_unsafe in
    promote_unsafe Pos.Op.((Pos.pow lbd k) / fk * exp (neg lbd))
  else
    zero
)


(* FIXME claiming (unsafe) that this is neg, should be tested *)
let logp_poisson_with logfact lbd k =
  if k >= 0 then
    let lbd' = F.to_float lbd in
    let lfk = logfact k in
    (* should we really handle lbd = 0. like this ? *)
    let kllbd =
      if k = 0 then
        0.
      else
        (float k) *. log lbd'
    in
    F.of_float_unsafe (kllbd -. lfk -. lbd')
  else
    F.neg_infinity

let logp_poisson lbd k = logp_poisson_with B.logfact lbd k


let logsump_poisson_with logfact lbds k : F._float F.t =
  if k >= 0 then
    let lfk = logfact k in
    let fmap lbd =
      let lbd' = F.to_float lbd in
      float k *. log lbd' -. lbd'
    in
    let lse = B.log_sum_exp (L.map fmap lbds) in
    F.of_float (lse -. lfk)
 else
    F.neg_infinity

let logsump_poisson lbds k = logsump_poisson_with B.logfact lbds k


let p_log p k : 'a F.proba F.t =
  if k >= 0 then
    let p' = F.to_float p in
    let fk = float k in
    (* FIXME use unsafe ? *)
    F.of_float_unsafe (~-. (B.pow p' k) /. (fk *. log (1. -. p')))
  else
    F.zero


let over_disp_deparam ovd lbd =
  let r = F.Pos.div F.one ovd in
  let _, p, _ = F.Pos.proportions lbd r in
  (r, p)


let p_negbin r p k : 'a F.proba F.t = F.(
  if k >= 0 then
    let k' = Int.of_int_unsafe k in
    (* cast to pos float *)
    let posk = Int.Pos.to_float k' in
    let factk = Int.Pos.to_float (Int.Pos.factorial k') in
    let binc = Pos.Op.(
      Pos.gamma (posk + r) / (factk * Pos.gamma r)
    ) in
    (* (1 - p) ** r *)
    let cpr = exp Op.(r * Proba.(p |> compl |> log)) in
    (* p ** k *)
    (* p ** k < 1 only if k is pos, but dependency cycle in this case *)
    let pk = Pos.pow p k in
    Pos.Op.(binc * pk * cpr) |> promote_unsafe
  else
    zero
)


let p_negbin_over ovd lbd k =
  let r, p = over_disp_deparam ovd lbd in
  p_negbin r p k


let logp_negbin_with logfact r p k =
  let r' = F.to_float r in
  let p' = F.to_float p in
  let fk = float k in
  let lfk = logfact k in
  let klp =
    if k = 0 then
      0.
    else
      fk *. log p'
  in
  let rl1mp =
    if r' = 0. then
      0.
    else
      r' *. log (1. -. p')
  in
  let logbinc = B.loggamma (fk +. r') -. lfk -. B.loggamma r' in
  F.of_float_unsafe (logbinc +. klp +. rl1mp)


let logp_negbin r p k = logp_negbin_with B.logfact r p k


let logp_negbin_over ovd lbd k =
  let r, p = over_disp_deparam ovd lbd in
  logp_negbin r p k


let p_n_choice_k ~choose ~among =
  F.of_float_unsafe (
       float (B.factorial choose * B.factorial (among - choose))
    /. float (B.factorial among)
  )


let logp_n_choice_k_with logfact ~choose ~among =
  F.of_float_unsafe (
    logfact choose +. logfact (among - choose) -. (logfact among)
  )


let logp_n_choice_k ~choose ~among =
  logp_n_choice_k_with B.logfact ~choose ~among


let logd_poisson_process_with logfact volume k =
  F.Pos.Op.(~- (volume + logfact k))


let logd_poisson_process volume k =
  logd_poisson_process_with Int.Pos.logfact volume k
