open Sig


let print xl =
  P.printf "[\n" ;
  L.iter (P.printf "%f ;\n") xl ;
  P.printf "]\n"


let%expect_test _ =
  let _, l = U.l_cumul_sum_rev [0. ; 1. ; 2.] in
  print l ;
  [%expect{|
    [
    0.000000 ;
    1.000000 ;
    3.000000 ;
    ] |}]
