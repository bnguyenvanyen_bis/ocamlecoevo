open Sig


let print_bool =
  function
  | true ->
      Printf.printf "true"
  | false ->
      Printf.printf "false"


let%expect_test _ =
  let rng = Random.State.make_self_init () in
  let b = U.rand_bernoulli ~rng F.zero in
  print_bool b ;
  [%expect{| false |}]


let%expect_test _ =
  let rng = Random.State.make_self_init () in
  let b = U.rand_bernoulli ~rng F.one in
  print_bool b ;
  [%expect{| true |}]
