open Sig


let%expect_test _ =
  P.printf "%i\n" (U.factorial 0) ;
  [%expect{| 1 |}]


let%expect_test _ =
  P.printf "%i\n" (U.factorial 1) ;
  [%expect{| 1 |}]


let%expect_test _ =
  P.printf "%i\n" (U.factorial 5) ;
  [%expect{| 120 |}]


let%test _ =
  match U.factorial (~-1) with
  | _ ->
      false
  | exception Assert_failure _ ->
      true


module Pre = U.Precompute ( )


let%test _ =
  (abs_float (Pre.logfact 0) < 1e-9)


let%test _ =
  (abs_float (Pre.logfact 0) < 1e-9)


(* confirmed with R *)
let%expect_test _ =
  P.printf "%f" (Pre.logfact 42) ;
  [%expect{| 117.771881 |}]
