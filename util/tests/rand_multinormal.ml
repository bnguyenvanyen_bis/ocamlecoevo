open Sig


let%expect_test _ =
  let rng = U.rng None in
  let cov = U.mat_to_cov (Lac.Mat.make0 2 2) in
  let x = U.rand_multi_normal_center ~rng cov in
  Printf.printf "%f %f" x.{1} x.{2} ;
  [%expect{| 0.000000 0.000000 |}]


let%expect_test _ =
  let rng = U.rng (Some 0) in
  let cov = U.mat_to_cov (
    Lac.Mat.of_array [| [| 1. ; 0. |] ; [| 1. ; 0. |] |]
  ) in
  let x = U.rand_multi_normal_center ~rng cov in
  Printf.printf "%f %f" x.{1} x.{2} ;
  [%expect{| 1.857254 1.857254 |}]
  
