open Sig


let%expect_test _ =
  let rng = U.rng None in
  let x = U.rand_normal ~rng 0. F.zero in
  print_float x ;
  [%expect{| 0. |}]
