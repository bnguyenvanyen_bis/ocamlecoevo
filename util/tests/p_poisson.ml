open Sig


let%expect_test _ =
  P.printf "%f" (F.to_float (U.p_poisson F.zero 0)) ;
  [%expect{| 1.000000 |}]


let%expect_test _ =
  P.printf "%f" (F.to_float (U.p_poisson F.zero 1)) ;
  [%expect{| 0.000000 |}]


let%expect_test _ =
  P.printf "%f" (F.to_float (U.p_poisson F.one (~-1))) ;
  [%expect{| 0.000000 |}]


(* confirmed with R *)
let%expect_test _ =
  P.printf "%f" (F.to_float (U.p_poisson F.one 1)) ;
  [%expect{| 0.367879 |}]


(* confirmed with R *)
let%expect_test _ =
  P.printf "%f" (F.to_float (U.logp_poisson F.one 1)) ;
  [%expect{| -1.000000 |}]


(* confirmed with R *)
let%expect_test _ =
  P.printf "%f" (F.to_float (U.logp_poisson F.two 7)) ;
  [%expect{| -5.673131 |}]
