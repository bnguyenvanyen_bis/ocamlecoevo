open Sig


let%test _ =
  let logp = U.logp_negbin F.one F.zero 0 in
  (abs_float (F.to_float logp) < 1e-9)


let%test _ =
  let logp = U.logp_negbin F.two F.zero 4 in
  F.Op.(logp = F.neg_infinity)


(* confirmed with R : careful our 'p' is the complementary of R's 'p'. *)
let%expect_test _ =
  let logp = U.logp_negbin (F.Pos.of_float 1.5) (F.Proba.of_float 0.3) 11 in
  P.printf "%f" (F.to_float logp) ;
  [%expect{| -12.425891 |}]
