open Sig


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_normal 0. F.one 0.)) ;
  [%expect{| 0.398942 |}]
