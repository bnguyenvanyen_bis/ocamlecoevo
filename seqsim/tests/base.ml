open Seqsim.Sig

let check_zero mat =
  U.int_fold ~f:(fun b i ->
    U.int_fold ~f:(fun b' j ->
      b' && (mat.{i,j} = 0.)
    ) ~x:b ~n:4
  ) ~x:true ~n:4

let check_zero_col j mat =
  U.int_fold ~f:(fun b i ->
    b && (mat.{i,j} = 0.)
  ) ~x:true ~n:4

let check_id mat =
  U.int_fold ~f:(fun b i ->
    U.int_fold ~f:(fun b' j ->
      b' && (if i = j then mat.{i,j} = 1. else mat.{i,j} = 0.)
    ) ~x:b ~n:4
  ) ~x:true ~n:4
