open Seqsim.Sig


module S =
  struct
    include Tree.State.Also_timed (struct
      type t = Seqs.t
      let to_string = Seqs.to_string
      let of_string s = Some (Seqs.of_string s)
    end)

    let to_seq (_, seq) = seq
  end


(* Felsenstein *)
module Fel = Seqsim.Tree.Make (S)

let node t stree =
  Fel.T.node (t, Seqs.empty) stree

let binode t ltree rtree =
  Fel.T.binode (t, Seqs.empty) ltree rtree

let seq = Seqs.of_string

let jc_loglik =
  let eq = Lac.Vec.make 4 0.25 in
  let par = Seqsim.Jc69.{ mu = 1. } in
  let trans_mat dt = Seqsim.Jc69.transition_probas par (F.Pos.of_float dt) in
  let loglik t =
    Fel.log_likelihood eq trans_mat (Fel.T.basic t)
  in loglik


let%expect_test _ =
  let lleaf = Fel.T.leaf (2., seq "A") in
  let rleaf = Fel.T.leaf (2.5, seq "A") in
  let tree = node 0. (binode 1. lleaf rleaf) in
  Printf.printf "%f" (jc_loglik tree) ;
  [%expect{| -2.552446 |}]


let%expect_test _ =
  let a = Fel.T.leaf (5., seq "ACCGATAT") in
  let b = Fel.T.leaf (6., seq "GTCTCACG") in
  let c = Fel.T.leaf (9., seq "GTGTCCAG") in
  let good_tree = node 0. (binode 2. a (binode 5. b c)) in
  let bad_tree = node 0. (binode 2.5 c (binode 4. a b)) in
  Printf.printf "%f < %f" (jc_loglik bad_tree) (jc_loglik good_tree) ;
  [%expect{| -33.489463 < -33.195217 |}]


let%expect_test _ =
  let a = Fel.T.leaf (9.01, seq "TTCAATGAATACATCGATCG") in
  let b = Fel.T.leaf (9.02, seq "ATCGATCGATCGGACGTTCG") in
  let c = Fel.T.leaf (9.03, seq "ACCGAGAGATCGGACGTTCG") in
  let good_tree = node 0. (binode 3. a (binode 6. b c)) in
  let bad_tree = node 0. (binode 3. b (binode 6. a c)) in
  Printf.printf "%f < %f" (jc_loglik bad_tree) (jc_loglik good_tree) ;
  [%expect{| -83.139385 < -83.064670 |}]
