open Seqsim
open Sig


(* if mu is 0., then Q is 0. *)
let%test _ =
  let par = Hky85.{ Hky85.default with mu = 0. } in
  let q = Hky85.instant_rates par in
  Base.check_zero q


(* if mu is 0., then P is Id *)
let%test _ =
  let par = Hky85.{ Hky85.default with mu = 0. } in
  let p = Hky85.transition_probas par F.one in
  Base.check_id p


(* if dt is 0., then P is Id *)
let%test _ =
  let p = Hky85.transition_probas Hky85.default F.zero in
  Base.check_id p


(* if pia is 0., we can't get any As *)
let%test _ =
  let par = Hky85.{ Hky85.default with pia = 0. } in
  let q = Hky85.instant_rates par in
  let p = Hky85.transition_probas par F.one in
     (q.{2,1} = 0.) && (q.{3,1} = 0.) && (q.{4,1} = 0.)
  && (p.{2,1} = 0.) && (p.{3,1} = 0.) && (p.{4,1} = 0.)
