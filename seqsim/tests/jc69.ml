open Seqsim
open Sig


(* if mu is 0., then Q is 0. *)
let%test _ =
  let par = Jc69.{ mu = 0. } in
  let q = Jc69.instant_rates par in
  Base.check_zero q


(* if mu is 0., then P is Id *)
let%test _ =
  let par = Jc69.{ mu = 0. } in
  let p = Jc69.transition_probas par F.one in
  Base.check_id p


(* if dt is 0., then P is Id *)
let%test _ =
  let par = Jc69.{ mu = 1. } in
  let p = Jc69.transition_probas par F.zero in
  Base.check_id p
