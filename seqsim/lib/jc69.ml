open Sig


type param = {
  mu : float ;
}

let default = {
  mu = 1e-4
}

let to_gtr (par : param) = Gtr.{
  mu = par.mu ;
  rct = 1. ;
  rat = 1. ;
  rgt = 1. ;
  rac = 1. ;
  rcg = 1. ;
  rag = 1. ;
  pia = 0.25 ;
  pic = 0.25 ;
  pig = 0.25 ;
  pit = 0.25 ;
}

let to_snp par = Gtr.to_snp @@ to_gtr @@ par

let mu par = F.Pos.of_float par.mu

let instant_rates ?mat par =
  let mat = Snp.zero_mat mat in
  let r = F.to_float (F.Pos.div (mu par) (F.Pos.of_float 4.)) in
  let q = ~-. 3. *. r in
  L.iteri (fun i _ ->
    let i' = i + 1 in
    L.iteri (fun j _ ->
      let j' = j + 1 in
      if i' = j' then
        mat.{i',j'} <- q
      else
        mat.{i',j'} <- r
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  mat
 

let transition_probas ?mat par dt =
  let mat = Snp.zero_mat mat in
  let e = exp (~-. (F.to_float dt) *. (F.to_float (mu par))) in
  let a = 1. /. 4. +. 3. /. 4. *. e in
  let b = 1. /. 4. -. 1. /. 4. *. e in
  L.iteri (fun i _ ->
    let i' = i + 1 in
    L.iteri (fun j _ ->
      let j' = j + 1 in
      if i' = j' then
        mat.{i',j'} <- a
      else
        mat.{i',j'} <- b
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  mat
