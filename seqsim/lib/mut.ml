open Sig


(* move this somewhere in seqsim ? *)
type param = {
  snp : Snp.param ;
  mu_del : float ; (* rate of del by start site *)
  mu_rep : float ; (* rate of rep by start site *)
  repdel_len : float ; (* param of the geometric law for repeats and deletes *)
}


let with_snp par x = { par with snp = x }
let with_mu_del par x = { par with mu_del = x }
let with_mu_rep par x = { par with mu_rep = x }
let with_repdel_len par x = { par with repdel_len = x }

let map_snp f par x = { par with snp = f par.snp x }

let specl = [
  ("-mu-del", La.Float with_mu_del,
   "Deletion mutation rate (by starting base).") ;
  ("-mu-rep", La.Float with_mu_rep,
   "Repetition mutation rate (by starting base).") ;
  ("-repdel-len", La.Float with_repdel_len,
   "Geometric law parameter for the length of repetitions/deletions.") ;
]

let output_par chan par =
  Printf.fprintf chan
  "Neutral sequence evolution with parameter_values :\n
  mu_del=%f ; mu_rep=%f ; repdel_len=%f ;\n"
  par.mu_del par.mu_rep par.repdel_len ;
  Snp.output_par chan par.snp

let out_par_head chan =
  Printf.fprintf chan
  "mu_del,mu_rep,repdel_len" ;
  Printf.fprintf chan "," ;
  Snp.out_par_head chan

let out_par_values chan par =
  Printf.fprintf chan
  "%f,%f,%f"
  par.mu_del par.mu_rep par.repdel_len ;
  Printf.fprintf chan "," ;
  Snp.out_par_values chan par.snp


let default = {
  snp = Jc69.to_snp Jc69.{ mu = 1e-4 } ;
  mu_del = 1e-6 ;
  mu_rep = 1e-6 ;
  repdel_len = 0.3 ;
}


let mu_del par = F.Pos.of_float par.mu_del

let mu_rep par = F.Pos.of_float par.mu_rep


let sd_modif sd t (seq, sdl) =
  let seq' = Seqs.patch sd seq in
  (seq', (F.to_float t, sd) :: sdl)


(* Event of substitution on a sequence *)
let subst_rate par _ (seq, _) =
  (* FIXME once we propagate Util types to Seq we can improve this *)
  F.Pos.Op.(I.Pos.to_float (Seqs.length seq) * Snp.mu par.snp)


let site_choice rng _ seq =
  Seqs.choose_site rng seq


let subst_modif par rng t (seq, sdl) =
  let i, symb = Seqs.choose_site rng seq in
  let nt = Seqs.Dna.of_uchar symb in
  let nt' = Snp.subst_modif par.snp rng nt in
  let symb' = Seqs.Dna.to_uchar nt' in
  let sd = Seqs.Sig.Sub (i, symb, symb') in
  sd_modif sd t (seq, sdl)


(* A function to choose some subsequence from a sequence, roughly uniformly,
 * so that each site has a roughly uniform probability of being in
 * a subsequence.
 * We select a middle site randomly then place the subsequence around it *)
let rand_subseq ~rng seq lbd =
  match Seqs.choose_site rng seq with Seqs.Sig.P mid, _ ->
  let lbd = F.to_float (U.rand_exp ~rng lbd) in
  let dec, flen = modf lbd in
  let subseq_len = int_of_float flen in
  let mid = I.to_int mid in
  (* dec is the decimal part of lbd, we use it
   * to make the result roughly symmetrical *)
  let i =
    let half_len = subseq_len / 2 in
    if (subseq_len mod 2) = 0 then
      mid - half_len
    else if (int_of_float (dec *. 10.) mod 2) = 0 then
      mid - half_len
    else
      mid - half_len - 1
  in
  (* if i < 0 we start at 0 *)
  let i' = max 0 i in
  (* in that case we need to adjust the length,
   * but we also need to not go beyond the end of the sequence *)
  (* maxlen is the remaining available length on the right *)
  let maxlen = I.to_int (Seqs.length seq) - i' - 1 in
  let len' = min maxlen (subseq_len + i - i') in
  (* FIXME would be better to prove it *)
  Seqs.Sig.P (I.Pos.of_int i'), Seqs.Sig.L (I.Pos.of_int len')



(* we aim for each site to have a roughly uniform rate of being in an event *)
let subseq_choice par rng seq =
  rand_subseq ~rng seq (F.Pos.of_float par.repdel_len)


let rep_subseq_rate par _ (seq, _) =
  F.Pos.Op.(I.Pos.to_float (Seqs.length seq) * (mu_rep par))


let rep_subseq_modif par rng t (seq, sdl) =
  let i, k = subseq_choice par rng seq in
  let sd = Seqs.Sig.Dup (i, Seqs.inc i k, k) in
  sd_modif sd t (seq, sdl)


let del_subseq_rate par _ (seq, _) =
  F.Pos.Op.(I.Pos.to_float (Seqs.length seq) * (mu_del par))


let del_subseq_modif par rng t (seq, sdl) =
  let n, k = subseq_choice par rng seq in
  let sd = Seqs.Sig.Del (n, Seqs.sub ~n ~k seq) in
  sd_modif sd t (seq, sdl)


let event par = Sim.Ctmjp.Util.(add_many [
  combine (subst_rate par) (subst_modif par) ;
  combine (rep_subseq_rate par) (rep_subseq_modif par) ;
  combine (del_subseq_rate par) (del_subseq_modif par) ;
])


type outparam = {
  chan : out_channel ;
  dt_print : float ;
}
