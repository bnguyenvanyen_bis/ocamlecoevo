open Sig


type param = {
  mu : float ;
  kappa : float ;
  pia : float ;
  pit : float ;
  pic : float ;
}


(* corresponds to Jc69 *)
let default = {
  mu = 1e-4 ;
  kappa = 1. ;
  pia = 0.25 ;
  pit = 0.25 ;
  pic = 0.25 ;
}


let mu par =
  F.Pos.of_float par.mu

let kappa par =
  F.Pos.of_float par.kappa

let pia par =
  F.Proba.of_float par.pia

let pit par =
  F.Proba.of_float par.pit

let pic par =
  F.Proba.of_float par.pic

let pig par =
  F.Proba.of_anyfloat F.Op.(F.one - pia par - pit par - pic par)

let ftf = F.to_float

let to_gtr par = Gtr.{
  mu = ftf (mu par) ;
  rag = ftf (kappa par) ;
  rct = ftf (kappa par) ;
  rat = 1. ;
  rgt = 1. ;
  rac = 1. ;
  rcg = 1. ;
  pia = ftf (pia par) ;
  pit = ftf (pit par) ;
  pic = ftf (pic par) ;
  pig = ftf (pig par) ;
}


let to_snp par = Gtr.to_snp @@ to_gtr @@ par


let rate par nt nt' =
  let lbd =
    match nt, nt' with
    | A, G | G, A | C, T | T, C ->
        kappa par
    | A, C | C, A | G, T | T, G | A, T | T, A | C, G | G, C ->
        F.one
    | A, A | T, T | C, C | G, G ->
        F.zero
  in
  let pi =
    match nt' with
    | A ->
        pia par
    | T ->
        pit par
    | C ->
        pic par
    | G ->
        pig par
  in
  let mu = mu par in
  F.Pos.Op.(lbd * pi * mu)


let instant_rates ?mat par =
  let mat = Snp.zero_mat mat in
  (* non diagonal *)
  L.iteri (fun i nt ->
    let i' = i + 1 in
    L.iteri (fun j nt' ->
      let j' = j + 1 in
      if i' <> j' then
        mat.{i',j'} <- F.to_float (rate par nt nt')
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  (* diagonal *)
  Snp.compensate_diagonal mat ;
  mat


let transition_probas ?mat par dt =
  let mu = mu par in
  let kappa = kappa par in
  let pia = pia par in
  let pit = pit par in
  let pic = pic par in
  let pig = pig par in
  (* purines *)
  let pir = F.Pos.Op.(pia + pig) in
  (* pyrimidines *)
  let piy = F.Pos.Op.(pic + pit) in
  (* from wikipedia *)
  let bnu = F.Pos.Op.(mu * dt /
      (F.two * pir * piy
     + F.two * kappa * (pia * pig + pic * pit))
  )
  in
  (* base exponent *)
  let e = F.(exp (neg bnu)) in
  (* minus e plus 1 *)
  let mep1 = F.Op.(F.one - e) in
  (* kappa minus 1 *)
  let km1 = F.Op.(kappa - F.one) in
  let ekr = F.exp F.(Op.(neg (F.one + pir * km1) * bnu)) in
  let eky = F.exp F.(Op.(neg (F.one + piy * km1) * bnu)) in
  let x1 = F.Pos.Op.(piy / pir * e) in
  let x2 = F.Pos.Op.(ekr / pir) in
  let x3 = F.Pos.Op.(pir / piy * e) in
  let x4 = F.Pos.Op.(eky / piy) in
  let proba nt nt' = F.Op.(
    match nt, nt' with
    | A, A ->
        pia + pia * x1 + pig * x2
    | A, G ->
        pig + pig * x1 - pig * x2
    | G, A ->
        pia + pia * x1 - pia * x2
    | G, G ->
        pig + pig * x1 + pia * x2
    | C, C ->
        pic + pic * x3 + pit * x4
    | C, T ->
        pit + pit * x3 - pit * x4
    | T, C ->
        pic + pic * x3 - pic * x4
    | T, T ->
        pit + pit * x3 + pic * x4
    | A, C | G, C ->
        pic * mep1
    | A, T | G, T ->
        pit * mep1
    | C, A | T, A ->
        pia * mep1
    | C, G | T, G ->
        pig * mep1
  )
  in
  let mat = Snp.zero_mat mat in
  L.iteri (fun i nt ->
    let i' = i + 1 in
    L.iteri (fun j nt' ->
      let j' = j + 1 in
      mat.{i',j'} <- F.to_float (proba nt nt')
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  mat

