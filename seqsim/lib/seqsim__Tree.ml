(*
 * Framework :
 * - a timed binary tree with aligned sequences at the tips
 * - a model of evolution
 *
 * We want to compute Felsenstein's likelihood on that tree.
 * First let's consider the case with no indels.
 * For each site :
 * - For each node from the latest one :
 *   - compute the probas that the ancestor site is in the four states
 * - When the root has been reached, compute against prior
 *   of site probabilities
 * Then multiply the probas for each site.
 *)

open Sig


(** A four element vector giving the probabilities
 *  of the site being in states A T C G *)
type site = Lac.Vec.t


module type STATE =
  sig
    include Tree.Timed.INPUT

    val to_seq : t -> Seqs.t
  end


module Make (St : STATE) =
  struct
    module T = Tree.Timed.Make (St)

    (* FIXME what do we do about indels -> gaps in the sequences ?
     * We'd like to ignore those sites most likely *)

    (* When all sites have the same transition rates,
     * then we can compute the transition probabilities only once
     * for each branch,
     * then reuse them for every site *)

    let homogen_trans_mat transition_matrix tree =
      let rec f prev_t =
        function
        | Tree.Leaf x ->
            let t = St.to_time x in
            let mat = transition_matrix (t -. prev_t) in
            Tree.Base.leaf (mat, x)
        | Tree.Node (x, stree) ->
            Tree.Base.node (Lac.Mat.empty, x) (f prev_t stree)
        | Tree.Binode (x, ltree, rtree) ->
            let t = St.to_time x in
            let mat = transition_matrix (t -. prev_t) in
            Tree.Base.binode (mat, x) (f t ltree) (f t rtree)
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (x, stree) ->
          Tree.Base.node (Lac.Mat.empty, x) (f (St.to_time x) stree)
    
    let homogen_site_likelihood site_of tree =
      let rec f =
        function
        | Tree.Leaf (trans, x) ->
            let site = site_of x in
            Lac.symv trans site
        | Tree.Node (_, stree) ->
            f stree
        | Tree.Binode ((trans, _), ltree, rtree) ->
            let lprobas = f ltree in
            let rprobas = f rtree in
            let v = Lac.Vec.mul lprobas rprobas in
            Lac.symv trans v
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (_, stree) ->
          f stree

    (* it looks like we could directly compute loglik instead ? *)
    let site_likelihood site_of transition_matrix tree =
      let rec f prev_t =
        function
        | Tree.Leaf x ->
            (* a dim 4 vec with three 0 and one 1 *)
            let site = site_of x in
            let t = St.to_time x in
            Lac.symv (transition_matrix  (t -. prev_t)) site
        | Tree.Node (_, stree) ->
            (* ignore node *)
            f prev_t stree
        | Tree.Binode (x, ltree, rtree) ->
            let t = St.to_time x in
            let lprobas = f t ltree in
            let rprobas = f t rtree in
            (* here is this really what we need ? *)
            let m = transition_matrix (t -. prev_t) in
            let v = Lac.Vec.mul lprobas rprobas in
            Lac.symv m v
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (x, stree) ->
          f (St.to_time x) stree

    let site_of uc = 
      let nt = Seqs.Dna.of_uchar uc in
      let i = Seqs.Dna.to_int nt in
      let v = Lac.Vec.make0 4 in
      v.{i + 1} <- 1. ;
      v

    let seq_length tree =
      let rec f =
        function
        | Tree.Leaf x ->
            let seq = St.to_seq x in
            Seqs.length seq
        | Tree.Node (_, stree) ->
            f stree
        | Tree.Binode (_, ltree, rtree) ->
            let ln = f ltree in
            let rn = f rtree in
            if not I.Op.(ln = rn) then
              invalid_arg "unaligned sequences" ;
            ln
      in f tree

    (* 'equilibrium' : or should we allow the root
     * to have different prior from the eq probas ? *)
    let log_likelihood equilibrium transition_matrix tree =
      let len = seq_length tree in
      let tree' = homogen_trans_mat transition_matrix tree in
      I.Pos.fold ~f:(fun loglik k ->
        let site_of x =
          let seq = St.to_seq x in
          let uc = Seqs.get ~n:(Seqs.Sig.P (I.Pos.pred k)) seq in
          site_of uc
        in
        let lik = homogen_site_likelihood site_of tree' in
        let post = Lac.dot equilibrium lik in
        loglik +. log post
      ) ~x:0. ~n:len
end


(* FIXME *)
(* We'd like some way to avoid too many recomputations in the MCMC :
 * Can we see that the computation will not change for some sites ?
 * Does that actually happen ?
 * It can happen if I just flip sequences over nodes,
 * for an invariant site.
 * Otherwise, even for an invariant site, if the branch lengths change,
 * then this will affect the probability of it not changing
 * across the whole tree.
 *
 * There is however a short path for those sites :
 * the probabilities for them will depend only on the total branch length.
 * I can compute it only once.
 * I think it is the only case where it happens however.
 * We could flag those sites as invariant and make them go through
 * a different function.
 * And for a tree that function can only take four values
 * (one for each nucleotide).
 * Note that this makes sense only for a site where we're certain of its value
 * for every sequence.
 * ~~~
 * To optimize we will need to find good ways to memoize :
 * typically, if several sites follow the same patterns over all
 * the sequences (not necessarily invariant),
 * then the computation is going to be the same.
 * It would be nice to find a way to memoize this.
 * But can it go through the site_of function ?
 *)
