module La = Lift_arg

type param = {
  mu : float ;
  rct : float ;
  rat : float ;
  rgt : float ;
  rac : float ;
  rcg : float ;
  rag : float ;
  pia : float ;
  pic : float ;
  pig : float ;
  pit : float ;
}


let specl = [
  ("-mu", La.Float (fun (par : param) x -> { par with mu = x }),
   "SNP base mutation rate.") ;
  ("-rCT", La.Float (fun par x -> { par with rct = x }),
   "C<->T relative mutation rate.") ;
  ("-rAT", La.Float (fun par x -> { par with rat = x }),
   "A<->T relative mutation rate.") ;
  ("-rGT", La.Float (fun par x -> { par with rgt = x }),
   "G<->T relative mutation rate.") ;
  ("-rAC", La.Float (fun par x -> { par with rac = x }),
   "A<->C relative mutation rate.") ;
  ("-rCG", La.Float (fun par x -> { par with rcg = x }),
   "C<->G relative mutation rate.") ;
  ("-rAG", La.Float (fun par x -> { par with rag = x }),
   "A<->G relative mutation rate.") ;
  ("-piA", La.Float (fun par x -> { par with pia = x }),
   "A equilibrium frequency.") ;
  ("-piC", La.Float (fun par x -> { par with pic = x }),
   "C equilibrium frequency.") ;
  ("-piG", La.Float (fun par x -> { par with pig = x }),
   "G equilibrium frequency.") ;
  ("-piT", La.Float (fun par x -> { par with pit = x }),
   "T equilibrium frequency.") ;
]



let to_snp par =
  assert (par.pia +. par.pic +. par.pig +. par.pit = 1.) ;
  let mu = par.mu in
  let relmu_a = par.rag *. par.pig +. par.rat *. par.pit +. par.rac *. par.pic in
  let relmu_c = par.rac *. par.pia +. par.rcg *. par.pig +. par.rct *. par.pit in
  let relmu_g = par.rag *. par.pia +. par.rcg *. par.pic +. par.rgt *. par.pit in
  let relmu_t = par.rat *. par.pia +. par.rct *. par.pic +. par.rgt *. par.pig in
  let snproba = Array.make_matrix 4 4 0. in
  (* fill in values for A : 0 *)
  snproba.(0).(1) <- par.rat /. relmu_a *. par.pit ; (* T *)
  snproba.(0).(2) <- par.rac /. relmu_a *. par.pic ; (* C *)
  snproba.(0).(3) <- par.rag /. relmu_a *. par.pig ; (* G *)
  (* fill in values for T : 1 *)
  snproba.(1).(0) <- par.rat /. relmu_t *. par.pia ; (* A *)
  snproba.(1).(2) <- par.rct /. relmu_t *. par.pic ; (* C *)
  snproba.(1).(3) <- par.rgt /. relmu_t *. par.pig ; (* G *)
  (* fill in values for C : 2 *)
  snproba.(2).(0) <- par.rac /. relmu_c *. par.pia ; (* A *)
  snproba.(2).(1) <- par.rct /. relmu_c *. par.pit ; (* T *)
  snproba.(2).(3) <- par.rcg /. relmu_c *. par.pig ; (* G *)
  (* fill in values for G *)
  snproba.(3).(0) <- par.rag /. relmu_g *. par.pia ; (* A *)
  snproba.(3).(1) <- par.rgt /. relmu_g *. par.pit ; (* T *)
  snproba.(3).(2) <- par.rcg /. relmu_g *. par.pic ; (* C *)
  { Snp.mu = mu ; relmu_a ; relmu_t ; relmu_c ; relmu_g ; snproba }
