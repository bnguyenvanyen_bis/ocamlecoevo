open Sig


(* FIXME we don't have this anymore *)
(* module Ev = Sim.Ctmjp.Event *)

type param = {
  mu : float ; (* max SNP rate *)
  relmu_a : float ; (* mu_snp * relmu_a is mutation rate of As *)
  relmu_t : float ; (* mu_snp * relmu_t is mutation rate of Ts *)
  relmu_c : float ; (* mu_snp * relmu_c is mutation rate of Cs *)
  relmu_g : float ; (* mu_snp * relmu_g is mutation rate of Gs *)
  snproba : float array array ;
  (* [| [| 0. ; A->T ; A->C ; A->G |] ;
        [| T->A ; 0. ; T->C ; T->G |] ;
        [| C->A ; C->T ; 0. ; C->G |] ;
        [| G->A ; G->T ; G->C ; 0. |] |] *)
}

let load_snproba_csv s =
  let csv = Csv.load s in
  let sa = Csv.to_array csv in
  A.map (fun col -> A.map (fun s -> float_of_string s) col) sa


let with_mu par x = { par with mu = x }
let with_relmu_a par x = { par with relmu_a = x }
let with_relmu_t par x = { par with relmu_t = x }
let with_relmu_c par x = { par with relmu_c = x }
let with_relmu_g par x = { par with relmu_g = x }

(* can we mutate those values or is that a problem ? *)
let with_p_at par x = (par.snproba.(0).(1) <- x ; par)
let with_p_ac par x = (par.snproba.(0).(2) <- x ; par)
let with_p_ag par x = (par.snproba.(0).(3) <- x ; par)
let with_p_ta par x = (par.snproba.(1).(0) <- x ; par)
let with_p_tc par x = (par.snproba.(1).(2) <- x ; par)
let with_p_tg par x = (par.snproba.(1).(3) <- x ; par)
let with_p_ca par x = (par.snproba.(2).(0) <- x ; par)
let with_p_ct par x = (par.snproba.(2).(1) <- x ; par)
let with_p_cg par x = (par.snproba.(2).(3) <- x ; par)
let with_p_ga par x = (par.snproba.(3).(0) <- x ; par)
let with_p_gt par x = (par.snproba.(3).(1) <- x ; par)
let with_p_gc par x = (par.snproba.(3).(2) <- x ; par)

let specl = [
  ("-snp", La.Float with_mu,
   "SNP base mutation rate.") ;
  ("-A", La.Float with_relmu_a,
   "Relative A mutation rate. Should be <= 1.") ;
  ("-T", La.Float with_relmu_t,
   "Relative T mutation rate. Should be <= 1.") ;
  ("-C", La.Float with_relmu_c,
   "Relative C mutation rate. Should be <= 1.") ;
  ("-G", La.Float with_relmu_g,
   "Relative G mutation rate. Should be <= 1.") ;
  ("-snproba", La.String (fun par s ->
     { par with snproba = load_snproba_csv s }
   ),
   "Probabilities of transitions to other nucleotides") ;
]


let mu par =
  F.Pos.of_float par.mu


let relmu par nt = Seqs.Sig.(
  let r = match nt with
    | A -> par.relmu_a
    | T -> par.relmu_t
    | C -> par.relmu_c
    | G -> par.relmu_g
  in F.Proba.of_float r
)

let proba par nt nt' =
  F.Proba.of_float (par.snproba.(Seqs.Dna.to_int nt).(Seqs.Dna.to_int nt'))

let rate par nt nt' =
  F.Pos.Op.(mu par * relmu par nt * proba par nt nt')

let erase_proba par nt =
  F.Proba.compl (relmu par nt)


let mutate par rng nt =
  let i = Seqs.Dna.to_int nt in
  let probas = L.map (fun p -> F.Proba.of_float p) (A.to_list par.snproba.(i)) in
  let choices = L.combine Seqs.Dna.nucleotides probas in
  Util.rand_choose ~rng choices


let subst_modif par =
  Sim.Ctmjp.Util.erase_auto (erase_proba par) (mutate par)


let output_par chan par =
  Printf.fprintf chan
  "Point mutations' parameter values :\n
   mu=%f ; relmu_a=%f ; relmu_t=%f ; relmu_c=%f ; relmu_g=%f ;\n"
  par.mu par.relmu_a par.relmu_t par.relmu_c par.relmu_g ;
  Printf.fprintf chan
  "snproba=[|\n
     [| 0. ; %.2f ; %.2f ; %.2f |] ;\n
     [| %.2f ; 0. ; %.2f ; %.2f |] ;\n
     [| %.2f ; %.2f ; 0. ; %.2f |] ;\n
     [| %.2f ; %.2f ; %.2f ; 0. |] ;\n
   |]\n"
  par.snproba.(0).(1) par.snproba.(0).(2) par.snproba.(0).(3)
  par.snproba.(1).(0) par.snproba.(1).(2) par.snproba.(1).(3)
  par.snproba.(2).(0) par.snproba.(2).(1) par.snproba.(2).(3)
  par.snproba.(3).(0) par.snproba.(3).(1) par.snproba.(3).(2)

let out_par_head chan =
  Printf.fprintf chan
  "mu_snp,relmu_a,relmu_t,relmu_c,relmu_g," ;
  Printf.fprintf chan
  "p_at,p_ac,p_ag,p_ta,p_tc,p_tg,p_ca,p_ct,p_cg,p_ga,p_gt,p_gc"

let out_par_values chan par =
  Printf.fprintf chan
  "%f,%f,%f,%f,%f,"
  par.mu par.relmu_a par.relmu_t par.relmu_c par.relmu_g ;
  let a = par.snproba in
  Printf.fprintf chan
  "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f"
  a.(0).(1) a.(0).(2) a.(0).(3)
  a.(1).(0) a.(1).(2) a.(1).(3)
  a.(2).(0) a.(2).(1) a.(2).(3)
  a.(3).(0) a.(3).(1) a.(3).(2)


let zero_mat =
  function
  | None ->
      Lac.Mat.make0 4 4
  | Some mat ->
      Lac.Mat.fill mat 0. ;
      mat


let compensate_diagonal mat =
  L.iteri (fun i _ ->
    let i' = i + 1 in
    let q = Lac.Mat.sum ~m:1 ~ar:i' mat in
    mat.{i',i'} <- ~-. q
  ) Seqs.Dna.nucleotides


let instant_rates ?mat par =
  let mat = zero_mat mat in
  (* Fill in the non-diagonal elements *)
  L.iteri (fun i nt ->
    let i' = i + 1 in
    L.iteri (fun j nt' ->
      let j' = j + 1 in
      if i' <> j' then
        mat.{i',j'} <- F.to_float (rate par nt nt')
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  (* Fill in the diagonal elements *)
  compensate_diagonal mat ;
  mat



(* no general formula for the exponential ? *)
