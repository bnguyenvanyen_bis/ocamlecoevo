module A = BatArray
module L = BatList

module Lac = Lacaml.D

module La = Lift_arg

module U = Util
module F = U.Float
module I = U.Int


type nuc = Seqs.Sig.nuc = A | T | C | G
