open Sig


module Auto =
  struct
    let record _ snap (z, snaps) = (z, snap :: snaps)
  end


module Timedep =
  struct
    let record _ = Auto.record
  end


(* FIXME we need to use output_genealogy instead *)


module T = Tree.Lengthed.Make (struct
  type t = float * Seqs.Sig.seqdiff
  let to_length (d, _) = d
  let with_length d (_, sdl) = (d, sdl)
  let to_string (d, x) =
    Printf.sprintf "%s:%.4f" (Seqs.Io.string_of_seqdiff x) d
  let of_string s =
    match Scanf.sscanf s "%s@:%f" (fun ss d -> (ss, d)) with
    | (ss, d) ->
        Some (d, Seqs.Io.seqdiff_of_string ss)
    | exception Scanf.Scan_failure _ ->
        None
end)


(* FIXME is this not already somewhere else ?
 * and what does it evend do ? *)
let untime l =
  let rec f acc l =
    match l with
    | [] ->
      acc
    | (_, _) :: [] ->
      acc
    | (t, _) :: (t', x') :: tl ->
      let acc' = ((t' -. t), x') :: acc in
      let l' = (t', x') :: tl in
      f acc' l'
  in match l with
  | [] -> []
  | y :: _ -> f [y] l  (* y should be timed at 0. typically *)


type outparam = {
  parchan : out_channel ;
  csvchan : out_channel ;
  seqchan : out_channel ;
  treechan : out_channel ;
}


module Out =
  struct
    module TGI = Tree.Genealogy.Intbl

    let open_out path = {
      parchan = open_out (Printf.sprintf "%s.par.csv" path) ;
      csvchan = open_out (Printf.sprintf "%s.traj.csv" path) ;
      seqchan = open_out (Printf.sprintf "%s.fasta" path) ;
      treechan = open_out (Printf.sprintf "%s.newick" path)
    }

    let specl = [
      ("-out", La.String (fun _ s -> open_out s),
       "File basename to output to.") ;
    ]

    let default = {
      parchan = stdout ;
      csvchan = stdout ;
      seqchan = stdout ;
      treechan = stdout ;
    }


    let output_seqs chan record =
      (* sequences output *)
      let itseq (i, (seq, sdl)) =
        match sdl with
        | (t, Seqs.Sig.Not) :: _ ->
            (i, t, seq)
        | [] ->
            invalid_arg "Seqpop.output_seqs : empty"
        | _ :: _ ->
            invalid_arg "Seqpop.output_seqs : invalid record"
      in
      let iseqs = L.map itseq (TGI.to_list record) in
      Seqs.Io.stamped_fasta_of_assoc chan iseqs

    let output_trees_from_sdl chan record =
      (* trees output *)
      let xll = L.map (fun (_, (_, sdl)) ->
        L.rev sdl
      ) (TGI.to_list record)
      in
      let xll = L.map (fun xl -> L.rev (untime xl)) xll in
      let forest = T.merge (0., Seqs.Sig.Not) xll in
      let f_tree i tree =
        Printf.fprintf chan "#tree %i\n" i ;
        T.output_newick chan tree ;
        output_string chan "\n\n"
      in
      L.iteri f_tree forest

    let output_trees_from_genealogy chan genealogy =
      let forest = Tree.Genealogy.forest_of genealogy in
      let f_tree i tree =
        Printf.fprintf chan "#tree %i\n" i ;
        Tree.Lt.output_newick chan tree ;
        output_string chan "\n\n"
      in
      L.iteri f_tree forest

    let close_par opar =
      close_out opar.parchan

    let close_csv opar =
      close_out opar.csvchan

    let close_seq opar =
      close_out opar.seqchan

    let close_tree opar =
      close_out opar.treechan

    let close opar =
      close_par opar ;
      close_par opar ;
      close_par opar ;
      close_par opar
  end
