#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os.path import splitext
import glob
import re
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


parser = argparse.ArgumentParser(
    description=
    "Plot 'case counts' and 'Neff' trajectories for many simulations."
)

parser.add_argument("--glob", nargs=2, default=[], action="append")

parser.add_argument("--focus", default=[], action="append")

parser.add_argument("--burn", type=int, default=0)

parser.add_argument("--multiples", type=int, nargs=2, default=[1, 0])

parser.add_argument("--bsp", action="store_true")

parser.add_argument(
  "--sources",
  nargs='+',
  choices=["cases", "neff"],
  default=["cases", "neff"],
)

parser.add_argument("--estim", action="store_true")

parser.add_argument("--out")

parser.add_argument(
  "--context",
  choices=["paper", "notebook", "talk", "poster"],
  default="notebook"
)

parser.add_argument("--split", action="store_true")



args = parser.parse_args()


patterns = args.glob
if len(patterns) == 0 :
  pattern_names = [ "no_glob" ]
else :
  pattern_names = [ pn for pn, _ in patterns ]


focuses = args.focus

sources = args.sources

burn = args.burn

mult, rem = args.multiples

sb.set_context(args.context)


k_pat = re.compile(r".(\d+).data.csv")
def keep_fname(fname) :
  match = k_pat.search(fname)
  if match :
    k = int(match.group(1))
    return ((k > burn) and (k % mult == rem))
  else :
    return True


bsp_pat = re.compile(r"neff:(\d+)")
def keep_col(col) :
  match = bsp_pat.search(col)
  if match :
    j = int(match.group(1))
    return (j % 10000 == 0)
  else :
    return False


def quants90(chunk) :
  bsp = chunk["source"].apply(lambda s : s[:5] == "neff:")
  q5 = chunk.loc[bsp, "value"].quantile(q=0.05)
  q95 = chunk.loc[bsp, "value"].quantile(q=0.95)
  df_q5 = pd.DataFrame({
    "t": chunk.loc[bsp, "t"].unique() ,
    "source": "neff_q5",
    "value": q5,
  })
  df_q95 = pd.DataFrame({
    "t": chunk.loc[bsp, "t"].unique() ,
    "source": "neff_q95",
    "value": q95,
  })

  return pd.concat(
    [
      chunk[chunk["source"].isin(["cases", "neff"])],
      df_q5,
      df_q95,
    ],
    axis=0,
    join="outer",
  )
  

# also differentiate between different patterns and focuses :
def read(fname, pattern_name=np.nan, pattern=np.nan, focus=False) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t"] + sources)

  if args.bsp and focus :
    bsp_cols = list(df.columns[3:])
  else :
    bsp_cols = []
  # change to long form
  df = df[["t"] + sources + bsp_cols].melt(
    id_vars=["t"],
    var_name="source",
    value_name="value",
  )

  if args.bsp and focus :
    df = df.groupby(
      ["t"],
      as_index=False,
      sort=False,
    ).apply(
      quants90
    )

  df = df.assign(focus=focus)
  df = df.assign(fname=fname)
  df = df.assign(pattern=pattern)
  df = df.assign(pattern_name=pattern_name)

  return df


def read_all(patterns, focuses) :
  reads_glob = [
    read(fname, pattern_name, pattern, focus=False)
    for pattern_name, pattern in patterns
    for fname in glob.glob(pattern)
    if keep_fname(fname)
  ]

  if reads_glob == [] :
    df_glob = pd.DataFrame({
      "t" : [np.nan] * len(sources),
      "source" : sources,
      "pattern" : ["no_glob"] * len(sources),
      "pattern_name" : ["no_glob"] * len(sources),
      "fname" : ["no_glob"] * len(sources),
      "focus" : [False] * len(sources),
    })
  else :
    df_glob = pd.concat(
      reads_glob,
      ignore_index=True,
    )

  reads_focus = [ read(fname, focus=True) for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  df = pd.concat([df_glob, df_focus])

  return df


try :
  fname_save = splitext(args.out)[0] + ".csv"
  df = pd.read_csv(fname_save)
except (FileNotFoundError, AttributeError) :
  df = read_all(patterns, focuses)


print(df)


if len(focuses) == 1 :
  focus_palette = sb.color_palette(["black"])
else :
  focus_palette = sb.cubehelix_palette(
    n_colors=len(focuses),
    light=0.6,
    dark=0.,
    rot=1.,
    hue=0.8,
    start=0.,
    reverse=True,
  )


def plot_patterns(df, estim, col="pattern_name", hue="pattern_name", hue_order=pattern_names, col_order=pattern_names, palette=None) :
  if estim :
    kw = {
      "ci" : 99.,
    }
  else :  # default
    kw = {
      "units" : "fname",
      "estimator" : None,
      "alpha" : 0.1,
    }
  f = sb.relplot(
    kind="line",
    x="t",
    y="value",
    row="source",
    col=col,
    hue=hue,
    col_order=col_order,
    hue_order=hue_order,
    palette=palette,
    facet_kws={
      "sharey":"row",
    },
    data=df[df["source"].isin(["cases", "neff"])],
    aspect=3.,
    **kw
  )

  return f


def plot(df, cols=True) :
  print("plot")
  print(df)

  def plot_focuses(x, y, data, **kw) :
    # we just need to find on what source we're on
    sources = data["source"].unique()
    assert (len(sources) == 1)
    source = sources[0]
    data = df[(df["source"] == source) & df["focus"]]
    ax = plt.gca()
    try :
      ax.set_ylim((data[y].min(), data[y].max()))
    except ValueError :
      pass
    sb.lineplot(
      x=x,
      y=y,
      estimator=None,
      hue="fname",
      palette=focus_palette,
      # We cheat to try to control the linewidth
      size="fname",
      sizes=(2., 2.),
      data=data,
      ax=ax
    )

  def plot_bsp_focuses(x, y, data, **kw) :
    sources = data["source"].unique()
    assert (len(sources) == 1)
    source = sources[0]
    if source == "neff" :
      data = df[(df["source"].isin(["neff_q5", "neff_q95"])) & df["focus"]]
      ax = plt.gca()
      try :
        ax.set_ylim((data[y].min(), data[y].max()))
      except ValueError :
        pass
      sb.lineplot(
        x=x,
        y=y,
        ci=99,  # the interval remains very small, why ?
        hue="fname",
        palette=focus_palette,
        data=data,
        ax=ax
      )

  if cols :
    kw = {}
  else :
    kw = {"col" : None, "col_order" : None}

  f = plot_patterns(df, args.estim, **kw)

  f = f.map_dataframe(plot_focuses, "t", "value")

  if args.bsp :
    f = f.map_dataframe(plot_bsp_focuses, "t", "value")

  f.set_titles("{row_name}")
    
  return f


if not args.split :
  f = plot(df)
else :
  if patterns == [] :
    patts = [ (np.nan, np.nan) ]
  else :
    patts = patterns

  df_nan = pd.DataFrame({
    "t" : [ np.nan for _ in sources for _ in patts ],
    "value" : [ np.nan for _ in sources for _ in patts ],
    "source" : [ src for src in sources for _ in patts ],
    "pattern" : [ patt for _ in sources for _, patt in patts ],
    "pattern_name" : [ pn for _ in sources for pn, _ in patts ],
    "focus" : [ False for _ in sources for _ in patts ]
  })
  f_split = {
    pn : {
      src : plot(
        pd.concat(
          [
            df[(df["pattern_name"] == pn)&(df["source"] == src)],
            df_nan[df_nan["source"] == src],
          ],
          axis=0,
          join="outer",
        ),
        cols=False
      )
      for src in sources
    }
    for pn, _ in patts
  }


def save(f, out) :
  f.fig.tight_layout()
  f.savefig(out, dpi=200)


if args.out :
  print("save")
  if not args.split :
    save(f, args.out)
  else :
    print("split")
    for pn, d in f_split.items() :
      for src, f in d.items() :
        save(f, args.out.format("{}.{}".format(pn, src)))
  
  df.to_csv(
    splitext(args.out)[0] + ".csv",
    index=False
  )  
else :
  plt.show()
