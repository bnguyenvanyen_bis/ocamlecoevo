#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os.path import splitext
import glob
import re
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


all_compartments = ["s", "e", "i", "r", "c", "o", "n"]

def compartment_label(comp) :
  d = {
    "s" : "Susceptibles",
    "e" : "Latent",
    "i" : "Infected",
    "r" : "Removed",
    "c" : "Cases",
    "o" : "Out",
    "n" : "Total population"
  }

  return d[comp]


parser = argparse.ArgumentParser(
    description=
    "Plot 'S', 'E', 'I', and 'R' trajectories for many simulations."
)

parser.add_argument(
  "--glob-label",
  default="pattern_name",
  help="How to label patterns (legend title for instance).",
)

parser.add_argument(
  "--glob-template",
  default="{}",
  help="template string to pass 'glob' elements through.",
)

parser.add_argument(
  "--glob",
  type=str,
  default=[],
  action="append",
  help="each element 's' specifies a glob pattern "
       "'{glob_template}.format(s)' pointing to trajectory files.",
)

parser.add_argument(
  "--focus",
  default=[],
  action="append",
  help="each element is a focus trajectory. "
       "Plotted in black with style variations (solid, dashed, etc).",
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore trajectory files with 'k < {burn}'.",
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore trajectory files with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--compartments",
  nargs="+",
  default=all_compartments,
  help="compartments for which to plot trajectories."
)

parser.add_argument(
  "--estim",
  action="store_true",
  help="plot 99 confidence intervals instead of low opacity lines "
       "for glob trajectories.",
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.1,
  help="opacity of the trajectories if non estim. Default 0.1",
)

parser.add_argument(
  "--colors",
  type=int,
  nargs=2,
  help="Number of colors to use in the glob palette and which to start from."
)

parser.add_argument(
  "--out",
  help="optional output filename (template). Interactive plot if not given.",
)

parser.add_argument(
  "--context",
  choices=["paper", "notebook", "talk", "poster"],
  default="notebook",
  help="seaborn plotting context.",
)

parser.add_argument(
  "--no-cols",
  action="store_true",
  help="plot all glob trajectories in one column.",
)

parser.add_argument(
  "--split",
  action="store_true",
  help="plot each compartment and glob on a different figure. "
       "Each figure is saved to '{out}.format(glob_name, compartment)'.",
)


args = parser.parse_args()

def glob_label(pattern_name) :
    try :
      int(pattern_name)
      float(pattern_name)
    except ValueError :
      return pattern_name
    else :
      return "glob_{}".format(pattern_name)


patterns = [ (glob_label(pn), args.glob_template.format(pn)) for pn in args.glob ]
if len(args.glob) == 0 :
  pattern_names = [ "no_glob" ]
else :
  # Careful : pattern names should not be numbers
  pattern_names = [ pn for pn, _ in patterns ]



focuses = args.focus

burn = args.burn

mult, rem = args.multiples


sb.set_context(args.context)


# compartment_string = args.compartments
# assert all([ c in all_compartments for c in compartment_string])


# compartments = [ c for c in all_compartments if c in compartment_string ]
compartments = args.compartments


print("compartments :", compartments)
print("pattern names :", pattern_names)


k_pat = re.compile(r".(\d+).traj.csv")
def keep_fname(fname) :
  match = k_pat.search(fname)
  if match :
    k = int(match.group(1))
    return ((k > burn) and (k % mult == rem))
  else :
    return True


# also differentiate between different patterns and focuses :
def read(fname, pattern_label, pattern_name=np.nan, pattern=np.nan, focus=False) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t"] + compartments)

  # only keep compartments and change to long form
  df = df[["t"] + compartments].melt(
    id_vars=["t"],
    var_name="compartment",
    value_name="count",
  )
  df = df.assign(focus=focus)
  df = df.assign(fname=fname)
  df = df.assign(pattern=pattern)
  df = df.assign(**{pattern_label:pattern_name})
  df = df.assign(**{"Compartment":df["compartment"].apply(compartment_label)})

  return df.astype({pattern_label:str})


def read_all(patterns, focuses, pattern_label) :
  reads_glob = [
    read(fname, pattern_label, pattern_name, pattern, focus=False)
    for pattern_name, pattern in patterns
    for fname in glob.glob(pattern)
    if keep_fname(fname)
  ]

  if reads_glob == [] :
    nb_comp = len(compartments)
    df_glob = pd.DataFrame({
      "t" : [np.nan] * nb_comp,
      "compartment" : compartments,
      "Compartment" : [ compartment_label(comp) for comp in compartments ],
      "pattern" : ["no_glob"] * nb_comp,
      pattern_label : ["no_glob"] * nb_comp,
      "fname" : ["no_glob"] * nb_comp,
      "focus" : [False] * nb_comp,
    })
  else :
    df_glob = pd.concat(
      reads_glob,
      ignore_index=True,
    )

  reads_focus = [ read(fname, pattern_label, focus=True)
                  for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  df = pd.concat(
    [df_glob, df_focus],
    sort=False
  )

  return df


try :
  fname_save = splitext(args.out)[0] + ".csv"
  df = pd.read_csv(fname_save)
except (FileNotFoundError, AttributeError) :
  df = read_all(patterns, focuses, args.glob_label)

  if args.out :
    df.to_csv(
      splitext(args.out)[0] + ".csv",
      index=False
    )  



focus_palette = sb.color_palette(["black"])

if args.colors :
  n_colors, start_color = args.colors
else :
  n_colors = len(pattern_names)
  start_color = 0

pal = sb.color_palette(n_colors=n_colors)
glob_palette = { pn : pal[i + start_color] for i, pn in enumerate(pattern_names) }


def plot_patterns(df, estim, no_cols, split, alpha, pattern_label) :
  kw = {
    "kind" : "line",
    "x" : "t",
    "y" : "count",
    "aspect" : 3.,
  }
    
  if estim :
    kw_estim = {
      "ci" : 99.,
    }
  else :  # default
    kw_estim = {
      "units" : "fname",
      "estimator" : None,
      "alpha" : alpha,
    }

  kw_full = {
    "row" : "Compartment",
    "hue" : pattern_label,
    "hue_order" : pattern_names,
    "palette" : glob_palette,
    "facet_kws" : {
      "sharey":"row",
    },
  }

  if no_cols :
    kw_cols = {}
  else :
    kw_cols = {
      "col" : pattern_label,
      "col_order" : pattern_names,
    }

  def plot_full() :
    f = sb.relplot(
      data=df,
      **kw,
      **kw_estim,
      **kw_full,
      **kw_cols,
    )
#   kind="line",
#   x="t",
#   y="count",
#   row="Compartment",
#   col=pattern_label,
#   hue=pattern_label,
#   col_order=pattern_names,
#   hue_order=pattern_names,
#   palette=glob_palette,
#   color="black",
#   facet_kws={
#     "sharey":"row",
#   },
#   data=df,
#   aspect=3.,
#   # legend=False,
#   **kw
# )
    # f = f.set_titles("{row_name}")
    return f

  def plot_split(pn, comp) :
    color = glob_palette[pn]
    data = df[(df[pattern_label] == pn)&(df["compartment"]==comp)]
    print(data.shape)
    f = sb.relplot(
      data=data,
      color=color,
      **kw,
    )
#   kind="line",
#   x="t",
#   y="count",
#   color=color,
#   data=data,
#   aspect=3.,
#   # legend=False,
#   **kw
# )
    f.axes[0,0].set_title(compartment_label(comp))
    # f.axes[0,0].legend()
    return f

  if split :
    return {
      pn : {
        comp : plot_split(pn, comp)
        for comp in compartments
      }
      for pn in pattern_names
    }
  else :
    return plot_full()


def plot(df, estim, no_cols, split, alpha, pattern_label) :
  print("plot")

  def plot_focuses(x, y, data, **kw) :
    # we just need to find what compartment we're on
    comps = data["compartment"].unique()
    assert (len(comps) == 1)
    comp = comps[0]
    data = df[(df["compartment"] == comp) & df["focus"]]
    ax = plt.gca()
    try :
      ax.set_ylim((data[y].min(), data[y].max()))
    except ValueError :
      print("could not set ylim")
    sb.lineplot(
      x=x,
      y=y,
      estimator=None,
      style="fname",
      # We cheat to try to control the linewidth
      size="fname",
      sizes=(2., 2.),
      data=data,
      ax=ax,
      color="black",
      # legend="full",
    )
    # ax.legend()

  def plot_focuses_fig(f) :
    f = f.map_dataframe(plot_focuses, "t", "count")
    f.set_titles("{row_name}")
    # f.add_legend()
    return f

  if split :
    fs = plot_patterns(
      df,
      estim=estim,
      no_cols=no_cols,
      split=split,
      alpha=alpha,
      pattern_label=pattern_label,
    )
    return {
      pn : {
        comp : plot_focuses_fig(f)
        for comp, f in figs.items()
      }
      for pn, figs in fs.items()
    }
  else :  
    f = plot_patterns(
      df,
      estim=estim,
      no_cols=no_cols,
      split=split,
      alpha=alpha,
      pattern_label=pattern_label,
    )
    return plot_focuses_fig(f)

  return f


f = plot(
  df,
  estim=args.estim,
  no_cols=args.no_cols,
  split=args.split,
  alpha=args.alpha,
  pattern_label=args.glob_label,
)


def save(f, out) :
  f.fig.tight_layout()
  f.savefig(out, dpi=200)


if args.out :
  if not args.split :
    save(f, args.out)
  else :
    for pn, d in f.items() :
      for comp, fig in d.items() :
        save(fig, args.out.format("{}.{}".format(pn, comp)))
  
  df.to_csv(
    splitext(args.out)[0] + ".csv",
    index=False
  )  
else :
  plt.show()
