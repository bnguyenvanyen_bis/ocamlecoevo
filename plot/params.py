#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from math import floor

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

sb.set_context("poster")


parser = argparse.ArgumentParser(
  description=
  "Compare parameter estimations from different MCMC runs."
)

parser.add_argument(
  "--read-template",
  default="{}",
  help="template string to pass the 'read' arguments through."
)

parser.add_argument(
  "--read",
  nargs=2,
  action="append",
  help="Each element (label, s) specifies a location"
       "to be read as '{read_template}.format(s)'."
)

parser.add_argument(
  "--read-label",
  default="posterior",
  help="to label the legend with."
)

parser.add_argument(
  "--target",
  help="target parameter values."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="plot only for iterations > {burn}."
)

parser.add_argument(
  "--parameters",
  nargs="+",
  help="parameters to plot for."
)

parser.add_argument(
  "--accepted-only",
  action="store_true",
  help="only plot accepted samples (old MCMC run compatibility).",
)

parser.add_argument(
  "--out",
  help="optional path to output. if not given, interactive plot.",
)

parser.add_argument(
  "--no-trace",
  action="store_true",
  help="don't plot the traces.",
)

parser.add_argument(
  "--no-pair",
  action="store_true",
  help="don't plot the pair scatterplots.",
)

parser.add_argument(
  "--context",
  choices=["paper", "notebook", "talk", "poster"],
  default="notebook",
  help="seaborn plotting context.",
)

parser.add_argument(
  "--split-labels",
  action="store_true",
  help="split the pair plot by read label.",
)

parser.add_argument(
  "--split-params",
  action="store_true",
  help="split the pair plot by parameter.",
)


def not_log_prior_lik(params) :
  return [ p for p in params if p not in ["logprior", "loglik"] ]


def sample_cov(df, params) :
  cols = not_log_prior_lik(params)
  cov = df[cols].cov()

  return cov


def batch_means_sample_cov(df, params, batch_exponent=(1./2.)) :
  cols = not_log_prior_lik(params)
  a = df[cols].values
  n_rows = df.shape[1]
  batch = floor(n_rows ** batch_exponent)
  n_batches = floor(n_rows / batch)  # FIXME ?
  batch_means = np.stack([
    a[k*batch:(k+1)*batch, :].mean(axis=0)
    for k in range(n_batches)
  ])
  cov = batch * np.cov(batch_means, rowvar=False)

  return cov


# FIXME this is wrong
def multi_ess(df, params, **kw) :
  cols = not_log_prior_lik(params)
  n = df.shape[0]
  p = len(cols)
  print(n, "samples")
  print(p, "dims")
  if p >= 2 :  # apparently this needs to be 2
    lbd = np.linalg.det(sample_cov(df, params))
    sgm = np.linalg.det(batch_means_sample_cov(df, params, **kw))
    return (n * (lbd / sgm) ** (1 / p))


def hist_scaled(x, bins, color, scl=1., **kwargs) :
  hist, bin_edges = np.histogram(x, bins)
  bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.
  scl_hist = scl * hist
  plt.fill_between(bin_centers, scl_hist, color=color, alpha=0.75)


def pair_plot(df, df_target, label_col, labels, row_params, col_params, palette=None, color=None) :
  g = sb.PairGrid(
    df[df[label_col].isin(labels)],
    hue=label_col,
    hue_order=labels,
    x_vars=row_params,
    y_vars=col_params,
    palette=palette,
  )
  try :
    g = g.map_diag(hist_scaled, color=color, bins=25)
  except (TypeError, ValueError) :
    # sometimes this fails (on split_params for example)
    pass

  if color :
    g = g.map_offdiag(plt.scatter, color=color, alpha=0.2, marker=',', s=2)
  else :
    g = g.map_offdiag(plt.scatter, alpha=0.2, marker=',', s=2)
  g = g.add_legend()

  pal = g.palette

  def improve_legend() :
    # set alpha and size for markers in legend
    for lh in g._legend.legendHandles :
      lh.set_alpha(1)
      lh._sizes = [100]

  improve_legend()

  def plot_targets() :
    # plot black lines for target
    for i, nmi in enumerate(row_params) :
      for j, nmj in enumerate(col_params) :
        try :
          x = df_target[nmj].values[0]
          g.axes[i, j].axvline(x=x, color='black', linewidth=1)
        except KeyError :
          print("no target for parameter", nmj)
        if i != j :
          try :
            y = df_target[nmi].values[0]
            g.axes[i, j].axhline(y=y, color='black', linewidth=1)
          except KeyError :
            print("no target for parameter", nmi)

  try :
    plot_targets()
  except TypeError :
    print("No targets to plot")

  return g


def trace_plot(df, label_col, labels, params) :
  g = sb.relplot(
    kind="line",
    x="k",
    y="value",
    hue=label_col,
    hue_order=labels,
    row="param",
    col=label_col,
    col_order=labels,
    facet_kws={
      "sharey":False,
    },
    data=df.melt(
      id_vars=[label_col, "k"],
      value_vars=params,
      var_name="param",
      value_name="value",
    ),
    aspect=3.
  )

  def plot_targets() :
    for i, nmi in enumerate(params) :
      try :
        y = df_target[nmi].values[0]
        g.axes[i].axhline(y=y, color='black', linewidth=1)
      except KeyError :
        print("no target for param", nmi)

  return g




args = parser.parse_args()


print(args.parameters)

sb.set_context(args.context)


if len(args.read) == 0 :
  read_args = [
    ("prior", "none.none"),
    ("cases", "negbin.none"),
    ("coal", "none.negbin"),
    ("joint", "negbin.negbin"),
  ]
else :
  read_args = args.read


read_pairs = [ (label, args.read_template.format(read))
               for (label, read) in read_args ]

read_labels = [ lab for (lab, _) in read_pairs ]

print(read_labels)

label_col = args.read_label


def read(s) :
  print("read", s)
  try :
    return pd.read_csv(
      s,
      delimiter=',',
      header=0
    )
  except FileNotFoundError :
    return None


df_d = {
  label : read(readv)
  for (label, readv) in read_pairs
  if read(readv) is not None
}


for label, read_v in read_pairs :
  try :
    df_d[label][label_col] = label
  except KeyError :
    pass


df = pd.concat(
  list(df_d.values()),
  ignore_index=True,
  sort=False,
)


burn = (df["k"] > args.burn)

if args.accepted_only :
  accept = (df["rejected"] == "accept")
  cond = (burn & accept)
else :
  cond = burn

df_hist = df[(cond)]


try :
  df_target = pd.read_csv(args.target, delimiter=',', header=0)
  # ranges = [ (0.25 * df_target[par].values[0],
  #             1.75 * df_target[par].values[0])
  #            for par in args.parameters ]
except (FileNotFoundError, ValueError) :
  df_target = None


pal = sb.color_palette(n_colors=len(read_labels))

glob_palette = { lab : pal[i] for i, lab in enumerate(read_labels) }


print("The multiESS for the samples is :")
print(multi_ess(df_hist, args.parameters))


if not args.no_trace :
  g_trace = trace_plot(
    df_hist,
    label_col=label_col,
    labels=read_labels,
    params=args.parameters
  )


if not args.no_pair :
  if (not args.split_labels) and (not args.split_params) :
    g_pair = pair_plot(
      df_hist,
      df_target,
      label_col=args.read_label,
      labels=read_labels,
      row_params=args.parameters,
      col_params=args.parameters,
      palette=glob_palette,
    )
  elif args.split_labels and (not args.split_params) :
    g_pairs = {
      label : pair_plot(
        df_hist,
        df_target,
        label_col=args.read_label,
        labels=[label],
        row_params=args.parameters,
        col_params=args.parameters,
        color=glob_palette[label],
      )
      for label in read_labels
    }
  elif (not args.split_labels) and args.split_params :
    g_pairs = {
      row_par : {
        col_par : pair_plot(
          df_hist,
          df_target,
          label_col=args.read_label,
          labels=read_labels,
          row_params=[row_par],
          col_params=[col_par],
          palette=glob_palette,
        )
        for col_par in args.parameters
      }
      for row_par in args.parameters
    }
  else :  # split both
    g_pairs = {
      label : {
        row_par : {
          col_par : pair_plot(
            df_hist,
            df_target,
            label_col=args.read_label,
            labels=[label],
            row_params=[row_par],
            col_params=[col_par],
            color=glob_palette[label],
          )
          for col_par in args.parameters
        }
        for row_par in args.parameters
      }
      for label in read_labels
    }


def save(g, out) :
  g.fig.tight_layout()
  g.savefig(out, dpi=200)


if args.out :
  if not args.no_trace :
    save(g_trace, args.out.format("trace"))

  if not args.no_pair :
    if args.split_labels and args.split_params :
      for label, gpars in g_pairs.items() :
        for row_par, gcols in gpars.items() :
          for col_par, g in gcols.items() :
            save(
              g,
              args.out.format("{}.pair.{}.{}".format(label, row_par, col_par))
            )
    elif args.split_labels and (not args.split_params) :
      for label, g in g_pairs.items() :
        save(g, args.out.format("{}.pair".format(label)))
    elif (not args.split_labels) and args.split_params :
      for row_par, gcols in g_pairs.items() :
        for col_par, g in gcols.items() :
          save(g, args.out.format("pair.{}.{}".format(row_par, col_par)))
    else :
      save(g_pair, args.out.format("pair"))
else :
  plt.show()
