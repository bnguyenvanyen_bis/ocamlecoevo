#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import re
import glob

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


parser = argparse.ArgumentParser(
    description=
    "Plot discrete measure counts."
)

parser.add_argument(
  "--glob-label",
  default="pattern_name",
  help="How to label patterns (legend title for instance).",
)

parser.add_argument(
  "--glob-template",
  default="{}",
  help="template string to pass 'glob' elements through.",
)

parser.add_argument(
  "--glob",
  type=str,
  action="append",
  nargs=2,
  default=[],
  help="each element 's' specifies a glob pattern "
       "'{glob_template}.format(s)' pointing to grid files.",
)

parser.add_argument(
  "--colors",
  nargs="+",
  help="event colors to plot for."
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore files with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore grid files with 'k < {burn}'.",
)

parser.add_argument(
  "--out",
  help="optional path to output."
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.1,
  help="opacity of the density trajectories.",
)

parser.add_argument(
  "--estim",
  action="store_true",
  help="plot confidence interval instead of individual trajectories in the density plot."
)

parser.add_argument(
  "--plot-colors",
  type=int,
  nargs=2,
  help="Number of colors for the density palette and which one to start with.",
)

# parser.add_argument(
#   "--no-heatmap",
#   action="store_true",
#   help="don't plot the heatmap.",
# )

parser.add_argument(
  "--context",
  choices=["paper", "notebook", "talk", "poster"],
  default="notebook",
  help="seaborn plotting context.",
)


def volume(row) :
  return (row["vector_t"] * row["vector_u"] * row["vector_k"])


def density(row) :
  return (row["count"] / row["volume"])


# FIXME this is slow
def aggregate_csl(df, glob_label) :
  def single(ser) :
    return ser.unique()[0]

  def augment(group) :
    csl_vector_u = group["vector_u"].sum()
    csl_count = group["count"].sum()
    csl_volume = group["volume"].sum()
    csl_density = csl_count / csl_volume

    return pd.Series({
      "origin_c" : single(group["origin_c"]),
      "origin_t" : single(group["origin_t"]),
      "fname"    : single(group["fname"]),
      glob_label : single(group[glob_label]),
      "origin_u" : single(group["origin_u"]),
      "vector_c" : single(group["vector_c"]),
      "vector_t" : single(group["vector_t"]),
      "vector_u" : csl_vector_u,
      "count"    : csl_count,
      "volume"   : csl_volume,
      "density"  : csl_density,
    })

  return df.groupby(
    ["origin_c", "origin_t", "fname"],
    as_index=False,
    sort=False,
  ).apply(
    augment
  ).reset_index(
    drop=True
  )



# FIXME : average for vector_u
def plot_density(df, colors, estim, glob_label, hue_order, palette) :
  if estim :
    kw = {
      "ci" : 99.,
    }
  else :
    kw = {
      "units" : "fname",
      "estimator" : None,
      "alpha" : args.alpha,
    }

  # slow
  data = aggregate_csl(
    df[df["origin_c"].isin(colors)],
    glob_label=glob_label
  )
  f = sb.relplot(
    kind="line",
    x="origin_t",
    y="density",
    row="origin_c",
    hue=glob_label,
    hue_order=hue_order,
    facet_kws={
      "sharey":"row",
    },
    data=data,
    palette=palette,
    aspect=3.,
    **kw
  )
  # f = f.set(ylim=(0.75, 1.25))
  f = f.set_xlabels("time")
  f = f.set_ylabels("point density")
  f = f.set_titles("{row_name}")

  return f


k_pat = re.compile(r".(\d+).prm.grid.csv")
def keep_fname(fname, burn=-1, mult=1, rem=0) :
  match = k_pat.search(fname)
  if match :
    k = int(match.group(1))
    return (k > burn) and (k % mult == rem)
  else :
    return True


def read(fname, glob_label, glob_name) :
  print(fname)
  df = pd.read_csv(fname)
  df = df.assign(fname=fname)
  df = df.assign(**{glob_label:glob_name})

  return df


def read_all(patterns, glob_label, glob_template, colors, burn, mult, rem) :
  reads = [
    read(fname, glob_label, glob_name)
    for glob_name, pattern in patterns
    for fname in glob.glob(glob_template.format(pattern))
    if keep_fname(fname, burn, mult, rem)
  ]

  if reads == [] :
    raise ValueError("bad glob")
  else :
    df = pd.concat(
      reads,
      ignore_index=True,
      sort=False,
    )

  return df


def save(f, out) :
  f.tight_layout()
  f.savefig(
    out,
    dpi=200,
  )


args = parser.parse_args()

sb.set_context(args.context)

mult, rem = args.multiples

df = read_all(
  patterns=args.glob,
  glob_label=args.glob_label,
  glob_template=args.glob_template,
  colors=args.colors,
  burn=args.burn,
  mult=mult,
  rem=rem
)

df = df.assign(volume=volume)
df = df.assign(density=density)

pattern_names = [ name for (name, _) in args.glob ]

if args.plot_colors :
  n_colors, start_color = args.plot_colors
  palette = sb.color_palette(n_colors=n_colors)
  palette = { name : palette[i + start_color]
              for i, name in enumerate(pattern_names) }
else :
  palette = None


f_dens = plot_density(
  df,
  colors=args.colors,
  estim=args.estim,
  glob_label=args.glob_label,
  hue_order=pattern_names,
  palette=palette
)


if args.out :
  # if not args.no_heatmap :
  #   save(f_heat, args.out.format("heatmap"))
  save(f_dens.fig, args.out.format("line"))
else :
  plt.show()
