
OCAML-ECOEVO
============

Simulations and inference for ecology and evolution.

Includes libraries:
- sim : Simulation of ODEs (odepack) and CTMJPs
- fit : Bayesian inference
- pop : Simulate population of individuals with evolving trait
- tree : Binary timed trees
- seqs : DNA / aa sequences
- seqsim : Simulate nucleotide evolution
- eco : Simulate some ecological systems
- epi : Simulate S(E)IR(S) systems
- epifit : Inference for S(E)IR(S) systems

Disclaimer : The interface is unstable.


Installation
------------

The simplest way to install the ecoevo package is
```
> opam create switch 4.08.1+flambda
> opam pin add ecoevo git+https://gitlab.com/bnguyenvanyen/ocamlecoevo.git
```

The project is built with dune, so you can also build with
```
> opam create switch 4.08.1+flambda
> opam install dune cmdliner batteries csv menhir lacaml odepack cairo2 ocamlgraph
> git clone https://gitlab.com/bnguyenvanyen/ocamlecoevo.git
> dune build
```

To run tests
```
> dune runtest
```

To install
```
> dune install
```


Documentation
-------------

The API can be built locally with
```
> dune build @doc
```

You can also take a look at the .mli interface files directly.

# FIXME : make it available online

(the remainder is not up to date)


Basics
======


Lift_arg
--------

A type and few functions to use the Arg module in a more functional way,
to reuse specs across modules.

Util
----

Mostly drawing from random distributions, plus pdfs etc.
Gets used by nearly all other libraries.

Jump
----

Dealing with càdlàg functions.
For stair processes and piecewise-linear functions,
implemented as association lists.


Sim
===

This library provides deterministic and stochastic integrators.
The Ode module contains ODE solvers
(in particular Sim.Ode.Lsoda uses ocaml-odepack bindings to ODEPACK's LSODA).
The Ctmjp module contains integrators for Continuous time Markov jump processes.
The main thing it implements is simulation by acceptance-rejection :
Simulate too many proposal events, choose a focal invidivual,
and reject with a certain probability depending on the individual.

This makes very step of the simulation `O(1)` instead of `O(n)`.
Even [Gillespie](src/sim/gill.ml) can then work pretty well.


Dependencies
------------

(ocaml-)csv, lacaml, (ocaml-)odepack, lift_arg, util, jump

Interface
---------

Integrators return a `next` function that can be used with
`simulate` and `simulate_until` functions from
`Sim.Loop`, `Sim.Csv` and `Sim.Cadlag`.

- [Sim.Ode.Dopri5](sim/lib/ode/ode_Dopri5.ml) :
  Classical deterministic integrator using the Dormand-Prince method
  (see https://en.wikipedia.org/wiki/Dormand%E2%80%93Prince_method ).
  Uses Lacaml to bind to BLAS.
- [Sim.Ode.Midpoint](sim/lib/ode/ode_Midpoint.ml) :
  Midpoint method, uses Lacaml to bind to BLAS.
- [Sim.Ode.Lsoda](sim/lib/ode/ode_Lsoda.ml) :
  ODEPACK's LSODA solver, uses ocaml-odepack to bind to ODEPACK.
- [Sim.Ctmjp.Gill](sim/lib/ctmjp/ctmjp_Gill.ml) :
  Stochastic integrator using the Gillespie method. Pure Ocaml.
- [Sim.Ctmjp.Euler_Poisson](sim/lib/ctmjp/ctmjp_Poisson.ml) :
  Exact stochastic integrator like Gill, but uses Poisson variables
  with fixed steps. Can be faster (x2) than Gill.
- [Sim.Loop](sim/lib/loop.ml) :
  Call a `next` function until some condition is satisfied,
  performing side-effects along the way.
- [Sim.Csv](sim/lib/csv.ml) :
  Call a `next` function until some condition is satisfied,
  storing the results into a CSV file.
- [Sim.Cadlag](sim/lib/cadlag.ml) :
  Call a `next` function until some condition is satisfied,
  storing the result into a `'a cadlag = (float * 'a) list`.



Fit
===

This library provides a few classical algorithms to do bayesian inference
and optimization.
It fits well with using Sim for simulations, but does not depend on it.
It can do MCMC, ABC, and adaptive MCMC.

Dependencies
------------

lift_arg, util, jump

Interface
---------

- [Fit.Dist](fit/lib/dist.ml) :
  GADT for distributions that you can draw from
  and calculate loglikelihoods on.
- [Fit.Mcmc](fit/lib/mcmc.ml) : Bayesian Metropolis-Hastings
- [Fit.Abc](fit/lib/abc.ml) : ABC
- [Fit.Am](fit/lib/am.ml) : Adaptive Metropolis
- [Fit.Mg](fit/lib/mg.ml) : Metropolis within Gibbs
- [Fit.Ram](fit/lib/ram.ml) : Robust Adaptive Metropolis


Tree
====

This library provides a functional (functors) interface to work
with binary timestamped trees.
Among other things, it can read/write in a customized Newick format,
(pretty naively) plot trees, and compute distances between trees.


Dependencies
------------

batteries, cairo2 (plotting), ocamlgraph (BHV distance)


Interface
---------

Functors with input node state. Everything is under Tree.

- [Tree.Base](src/tree/tree_base.ml) :
  Some functions with polymorphic trees.
- [Tree.Path](src/tree/tree_path.ml) :
  Paths along a tree and unions of paths.
- [Tree.Timed](src/tree/timed.ml) :
  Unlabeled timestamped binary trees.
  Parsing / writing to (customized) Newick.
  Safe constructors, then some useful functions.
- [Tree.Labeled](src/tree/labeled.ml) :
  Labeled timestamped binary trees.
  Draw trees (simplified version of Jean-Gil's algorithm ?)
  Compute tree distances : Kendall Colijn (should work) or BHV (broken).
- [Tree.Genealogy](src/tree/genealogy.ml) :
  Hashtbl representation of trees and conversion from/to trees


Seq
===

Handle sequences as ropes (BatText.t) that can undergo substitutions,
insertions, deletions, duplications, repetitions and reversions.

A mutation is represented by a `seqdiff` and a string of those can be used to
define homology between sequences.


Pop
===

This library serves as a helper for the simulation of populations,
when event rates vary from individual to individual.
Populations are implemented in an efficient and flexible way.



