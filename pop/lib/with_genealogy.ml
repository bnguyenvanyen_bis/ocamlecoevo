open Sig

module TG = Tree.Genealogy
module Itb = TG.Intbl


module Augment_partial
  (P : POP_WITH_DYN_WO_CREATE with type 'a isid = 'a) =
  struct
    type 'a stoch = 'a P.stoch
    type 'a isid = 'a P.isid
    type 'a trait = 'a P.trait
    type 'a group = 'a P.group
    type 'a indiv = 'a P.indiv

    module I = P.I
    module T = P.T

    type group_data = P.group_data

    (* information we keep in the genealogy *)
    module Iwp =
      struct
        type t = {
          trait : id trait ;
          t_birth : float ;
          mutable t_death : float ;
          parent_id : int option ;
          mutable children_ids : int list ;
          mutable erasable : bool ;
        }

        (* unused
         * let to_indiv { trait = x ; _ } i = I.id x i
         *)

        let new_of_indiv (type a) ?(parent : a I.t option) t (I.Id (x, i)) =
          let idpo =
            match parent with
            | None ->
                None
            | Some (I.Nonid _) ->
                None
            | Some (I.Id (_, idp)) ->
                Some idp
          in (i, {
            trait = x ;
            t_birth = t ;
            t_death = infinity ;
            parent_id = idpo ;
            children_ids = [] ;
            erasable = true ;
          })

        let erasable { erasable ; t_death ; children_ids ; _ } =
          erasable && (t_death < infinity) && (children_ids = [])

        let forbid_erase iwp =
          iwp.erasable <- false
      end

    type indiv_with_parent = Iwp.t

    type t = {
      pop : P.t ;
      genealogy : indiv_with_parent Itb.t ;
    }

    (*
    let scores ~i z = P.scores ~i z.pop

    let compares ~i z = P.compares ~i z.pop
    *)

    (*
    let create ?mem ?groups (* ~scores *) nindivs = {
      pop = P.create ?mem ?groups (* ~scores *) nindivs ;
      (* nindivs not really informative but better than nothing *)
      genealogy = Itb.create nindivs ;
    }
    *)

    let copy z = {
      pop = P.copy z.pop ;
      genealogy = Itb.map (fun _ iwp ->
        Iwp.({ iwp with trait = T.copy iwp.trait })
      ) z.genealogy ;
    }

    (*
    let add_new_group ~group z = P.add_new_group ~group z.pop
    *)

    (*
    let new_indiv_nonid = P.new_indiv_nonid
    *)

    let new_indiv z = P.new_indiv z.pop

    let indiv_from_group = P.indiv_from_group

    let count ~group z = P.count ~group z.pop

    (* let max ~i z = P.max ~i z.pop *)

    let fold_groups f z y0 = P.fold_groups f z.pop y0

    let fold_indivs f z g y0 = P.fold_indivs f z.pop g y0

    let choose ~rng ~group z = P.choose ~rng ~group z.pop

    let check_counts z = P.check_counts z.pop

    let add_child_id genealogy parent_id id =
      match parent_id with
      | None ->
          ()
      | Some pid ->
          begin
            (* Printf.eprintf "add child id %i to %i\n%!" id pid ; *)
            let p = Itb.find genealogy pid in
            Iwp.(p.children_ids <- id :: p.children_ids)
          end


    let add (type a b) ?(parent : a I.t option) t z (ind : b isid I.t) =
      match ind with
      | (I.Nonid _) as nind ->
          P.add z.pop nind
      | (I.Id _) as iind ->
          let i, iwp = Iwp.new_of_indiv ?parent t iind in
          (* Printf.eprintf "add %i\n%!" i ; *)
          Itb.add z.genealogy i iwp ;
          add_child_id z.genealogy iwp.parent_id i ;
          P.add z.pop iind

    let remove (type a) t z (ind : a isid indiv) =
      begin
        match ind with
        | I.Nonid _ ->
            ()
        | I.Id (_, i) ->
            (* Printf.eprintf "remove %i\n%!" i ; *)
            let iwp = Itb.find z.genealogy i in
            iwp.t_death <- t
      end ;
      P.remove z.pop ind

    let erase (type a) z : (a isid indiv -> unit) =
      let rec f_id i =
        (* Printf.eprintf "erase %i\n%!" i ; *)
        let iwp = Itb.find z.genealogy i in
        (* if indiv is dead and has no children *)
        if Iwp.erasable iwp then begin
          Itb.remove z.genealogy i ;
          match iwp.parent_id with
          | None ->
              ()
          | Some idp ->
              let p = Itb.find z.genealogy idp in
              (* remove i from its parent's children_ids *)
              p.children_ids <- L.remove p.children_ids i ;
              (* recurse *)
              f_id idp
        end else
          (* cannot be erased *)
          ()
      in
      function
      | I.Nonid _ ->
          ()
      | I.Id (_, i) ->
          f_id i
     
    let forbid_erase (type a) z : (a isid indiv -> unit) =
      function
      | I.Nonid _ ->
          ()
      | I.Id (_, i) ->
          let iwp = Itb.find z.genealogy i in
          Iwp.forbid_erase iwp

    let tree_parent_id js j =
      function
      | None ->
          js := j :: !js ;
          TG.forest_root
      | Some i ->
          i

    let remove_erase_living tf z =
      (* Printf.eprintf "remove_erase_living\n%!" ; *)
      (* first record all indivs in genealogy that are still alive :
       * t_death = infinity *)
      let living = Itb.fold (fun i Iwp.{ t_death ; trait ; _ } inds ->
          if (t_death = infinity) then begin
            (* Printf.eprintf "alive %i\n%!" i ; *)
            (I.id trait i) :: inds
          end else
            inds
        ) z.genealogy []
      in
      (* then remove those indivs *)
      L.iter (remove tf z) living ;
      (* then erase them *)
      L.iter (fun ind -> try erase z ind with Not_found -> ()) living


    let to_tree_genealogy z =
      (* Printf.eprintf "to_tree_genalogy\n%!" ; *)
      let tree_roots = ref [] in
      let g = Itb.map (fun i Iwp.{ t_birth ; t_death ; parent_id ; children_ids ; _ } ->
        TG.{
          t_birth ;
          t_death ;
          parent_id = tree_parent_id tree_roots i parent_id ;
          children_ids = List.rev children_ids (* ordered by birth *) ;
        }
      ) z.genealogy
      in
      Itb.add g TG.forest_root TG.{
          t_birth = neg_infinity ;
          t_death = infinity ;
          parent_id = TG.forest_root ;
          children_ids = List.rev !tree_roots (* in increasing order ? *) ;
      } ;
      g

    let to_recorded_traits z =
      Itb.map (fun _ Iwp.({ trait ; _ }) -> trait) z.genealogy

    let to_recorded_death_traits z =
      Itb.map (fun _ Iwp.({ trait ; t_death ; _ }) ->
          (t_death, trait)
      ) z.genealogy

    let to_recorded_leaf_traits z =
      Itb.filter_map (fun _ Iwp.({ trait ; children_ids ; _ }) ->
        match children_ids with
        | [] ->
            Some trait
        | _ ->
            None
      ) z.genealogy

    let to_recorded_leaf_death_traits z =
      Itb.filter_map (fun _ Iwp.({ trait ; t_death ; children_ids ; _ }) ->
        match children_ids with
        | [] ->
            Some (t_death, trait)
        | _ ->
            None
      ) z.genealogy

    let output_genealogy string_of chan z =
      Printf.fprintf chan
      "ident,trait,birth,death,parent_id,children_ids\n" ;
      Itb.iter (fun i (iwp : Iwp.t) ->
        Printf.fprintf chan "%i,%s,%f,%f,%s,%s\n"
        i
        (string_of iwp.trait) 
        iwp.t_birth
        iwp.t_death
        (match iwp.parent_id with
         | None ->
             ""
         | Some p ->
             string_of_int p)
        (L.fold_left
          (Printf.sprintf "%s%i;")
          ""
          iwp.children_ids)
      ) z.genealogy

  end


module Augment
  (P : POP_ID_WITH_DYN with type 'a isid = 'a) =
  struct
    include Augment_partial (P)

    let create ?mem ~ngroups ~nindivs = {
      pop = P.create ?mem ~ngroups ~nindivs ;
      (* nindivs not really informative but better than nothing *)
      genealogy = Itb.create nindivs ;
    }
  end


module Augment_max
  (P : POP_WITH_MAX_DYN with type 'a isid = 'a) =
  struct
    include Augment_partial (P)

    let create ?mem ~ngroups ~scores ~nindivs = {
      pop = P.create ?mem ~ngroups ~scores ~nindivs ;
      (* nindivs not really informative but better than nothing *)
      genealogy = Itb.create nindivs ;
    }

    let max ~i z = P.max ~i z.pop
    let compares ~i z = P.compares ~i z.pop
    let scores ~i z = P.scores ~i z.pop
  end


module Make (T : TRAIT) =
  struct
    include Augment (With_id.Make (T))
  end


(*
module Make_max (T : TRAIT) =
  struct
    include Augment_max (With_max.Make (T))
  end
*)
