open Sig

module L = BatList
module H = BatHashtbl


(*
module Make (P : POP_WITH_MAX_DYN) :
  (POP_WITH_MAX_GENEALOGY_DYN
      with type 'a isid = 'a P.isid
       and type 'a trait = 'a P.trait
       and type 'a group = 'a P.group
       and type 'a I.t = 'a P.I.t
       and type group_data = P.group_data
       and type t = P.t) =
  struct
*)

module Augment_partial (P : POP_WITH_DYN_WO_CREATE) =
  struct
    module T = P.T
    module I = P.I

    type 'a stoch = 'a P.stoch
    type 'a isid = 'a P.isid
    type 'a trait = 'a P.trait
    type 'a group = 'a P.group
    type 'a indiv = 'a P.indiv

    type group_data = P.group_data

    type t = P.t

    (*
    let add_new_group ~group z =
      P.add_new_group ~group z
    *)

    let copy z = P.copy z

    (*
    let new_indiv_nonid x = P.new_indiv_nonid x
    *)

    let new_indiv z x = P.new_indiv z x

    let indiv_from_group = P.indiv_from_group

    let count ~group z = P.count ~group z

    let fold_groups f z y0 = P.fold_groups f z y0

    let fold_indivs f z g y0 = P.fold_indivs f z g y0

    let choose ~rng ~group z = P.choose ~rng ~group z

    let check_counts z = P.check_counts z

    let add ?parent _ z ind =
      ignore parent ;
      P.add z ind

    let remove _ z ind =
      P.remove z ind

    let erase _ _ =
      ()

    let forbid_erase _ _ = ()

    let remove_erase_living _ _ = ()

    let tgi_empty () = Tree.Genealogy.Intbl.create 0

    let to_tree_genealogy _ = tgi_empty ()

    let to_recorded_traits _ = tgi_empty ()

    let to_recorded_death_traits _ = tgi_empty ()

    let to_recorded_leaf_traits _ = tgi_empty ()

    let to_recorded_leaf_death_traits _ = tgi_empty ()

    let output_genealogy _ _ _ =
      ()
  end


module Augment (P : POP_WITH_DYN) =
  struct
    include Augment_partial (P)

    let create ?mem ~ngroups ~nindivs =
      ignore mem ;
      ignore nindivs ;
      P.create ~ngroups
  end


module Augment_id (P : POP_ID_WITH_DYN) =
  struct
    include Augment_partial (P)

    let create ?mem ~ngroups ~nindivs =
      P.create ?mem ~ngroups ~nindivs
  end


module Augment_max (P : POP_WITH_MAX_DYN) =
  struct
    include Augment_partial (P : POP_WITH_DYN_WO_CREATE
      with type 'a stoch = 'a P.stoch
       and type 'a isid = 'a P.isid
       and type 'a trait = 'a P.trait
       and type 'a group = 'a P.group
       and type 'a I.t = 'a P.I.t
       and type group_data = P.group_data
       and type t = P.t
    )

    let create ?mem ~ngroups ~scores ~nindivs =
      P.create ?mem ~ngroups ~scores ~nindivs

    let scores = P.scores
    
    let compares = P.compares

    let max ~i z = P.max ~i z
  end


module Make (T : TRAIT) =
  struct
    include Augment_max (With_max.Make (T))
  end
