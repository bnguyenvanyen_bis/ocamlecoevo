(** Signatures used throughout the pop library *)


(** {1:intro Introduction to the module} *)

(**  Welcome into functor spaghetti. This paragraph aims to help
 *  to get you through it.
 *
 *  The basic functor goes from a {!TRAIT} to a {!POP},
 *  meaning that if we know enough about the [trait] values of individuals,
 *  meant to be interpreted more as a phenotype in the general sense,
 *  than as a genotype, so that it might change during the lifetime
 *  of the individual: For instance, it should include information about
 *  geographical position if relevant.
 *
 *  We then make the assumption that the information about the population
 *  is in the group counts (how many individuals in each group),
 *  and in the individual traits.
 *
 *  The different signatures then relate to how much detail about
 *  the population we need to know, of three kinds :
 *  - specific individual traits, so that individuals in a group
 *    are different from one another. This is the "with id" case
 *  - trait values reaching maximum scores (from user-supplied functions)
 *  - ancestry between individuals
 *
 *  For a total of 5 signatures resulting from combinations
 *  - {!POP_NONID} with none of this information.
 *  - {!POP_SIMPLE} with specific individual traits.
 *  - {!POP_WITH_MAX} with ids and scores.
 *  - {!POP_WITH_GENEALOGY} with ids and ancestry.
 *  - {!POP_COMPLEX} with ids, scores and ancestry.
 *
 *  Additional convenience functions can then be provided like in
 *  the {!WITH_EVENTS} signature.
 *  
 *  If we track individual trait values inside a group,
 *  we need to 'identify' the individuals of that group.
 *  meaning that each of those individuals is given a unique id.
 *  This is also necessary if we want to track ancestry.
 *
 *  This can be decided group by group, via the anonymous type parameter :
 *  an [id group] will contain [id indiv]s with [id trait] values,
 *  and a [nonid group] will contain [nonid indiv]s with [nonid trait] values.
 *  [nonid group]s will be censused but their indivs won't be recoverable.
 *  Actually we will assume that it is possible to know the
 *  [nonid trait] value from the [nonid group] only :
 *  The [TRAIT] input should include a value
 *  [val of_group : nonid trait -> nonid group].
 * 
 *  To record max scoring individuals, score functions are supplied
 *  by the user to the [create] function when creating the population.
 * 
 *  To track ancestry, an optional parent can be given
 *  when [add]ing an individual, and the time of addition and removal
 *  of individuals must also be known.
 *  An additional [erase] function also makes it possible to forget
 *  a particular individual's history, up to the last binary or more
 *  node in its ancestry.
 * 
 *)

module L = BatList
module A = BatArray
module H = BatHashtbl
module Map = BatMap

module U = Util
module F = U.Float
module TG = Tree.Genealogy


(** equality between types *)
type (_, _) eq = Eq : ('a, 'a) eq

(* needed to compile before 4.06 for destructive substitution *)
type 'a rng = U.rng

(** comparison function *)
type 'a comparison = 'a -> 'a -> int

(** the type of an identified value (trait/group/indiv). *)
type id = [ `Id ]

(** the type of a non identified value (trait/group/indiv). *)
type nonid = [ `Nonid ]

(** [id] or [nonid] *)
type anyid = [ id | nonid ]


(*
type 'a lessisid = [< `Id | `Nonid ] as 'a
type 'a idor = [< `Id | `Nonid > `Id ] as 'a
type 'a nonidor = [< `Id | `Nonid > `Nonid ] as 'a
*)

type 'a isid =
  | Isid of ('a, id) eq
  | Isnotid of ('a, nonid) eq


(** {2:trait Trait of an individual} *)
 
(**  Our basic premise is that individuals carry a trait value
 *   that characterizes them,
 *   and that individuals can be further regrouped into [group]s
 *   depending on their trait value,
 *   and that the number of individuals in each group is important.
 *   A group could be a species, a location, a compartment, etc...
 *)


(** Basic requirements about a trait.
 *  We need to be able to derive a group from a trait in all cases,
 *  and to derive a trait from a group in the [nonid] case,
 *  and we need to be able to [copy] traits (which might be mutable). *)
module type TRAIT =
  sig
    (* This might be useful, but, also, annoying
    (** Groups/traits/indivs are [nonid] or [[id | nonid]] *)
    type isid
    *)

    (** The type of trait values *)
    type 'a t

    (** The type of groups *)
    type 'a group

    (** A group is either [id] or [nonid].
     *  [isid g] is [`Id] if [g] is an [id] group,
     *  and [`Nonid] if [g] is a [nonid] group. *)
    val isid : 'a group -> 'a isid

    (** A trait belongs to a [group].
     *  [group_of x] is the group to which [x] belongs. *)
    val group_of : 'a t -> 'a group

    (** A [nonid group] defines a [nonid] trait.
     *  [of_group g] is the unique [nonid] trait value corresponding
     *  to the [nonid] group [g]. *)
    val of_group : nonid group -> nonid t

    (** A trait can be copied.
     *  [copy x] is a (deep) copy of [x]. *)
    val copy : 'a t -> 'a t
  end


(** More (sometimes useful (?)) requirements about traits *)
module type TRAIT_MORE =
  sig
    include TRAIT

    (** equality between traits. *)
    val equal : 'a t -> 'a t -> bool

    (** hash trait *)
    val hash : 'a t -> int
  end


(** {3:indiv Signature for individuals} *)

(** Individuals are [id] or [nonid], can be copied, and a trait value
 *  can be derived from them. [id indiv]s are identified by an [ident]. *)
module type INDIV =
  sig
    (** type for trait values *)
    type 'a trait

    (** type for individuals, which can be [id] or [nonid] *)
    type _ t = private
      | Id : id trait * int -> id t
      | Nonid : nonid trait -> nonid t

    (** An identified individual [id t] is defined by a [trait] value
     *  and an identifier.
     *  [id x i] is an [id] individual [ind] with [trait_of ind = x]
     *  and [ident ind = i]. *)
    val id : id trait -> int -> id t

    (** A non-identified individual [nonid t] is defined by a [trait] value.
     *  [nonid x] is a [nonid] individual [ind] with [trait_of ind = x]. *)
    val nonid : nonid trait -> nonid t

    (** An individual has a trait value.
     *  [trait_of ind] is the trait value of the individual [ind]. *)
    val trait_of : 'a t -> 'a trait

    (** An individual can be copied.
     * [copy ind] is a copy of the indiv [ind] with a trait value
     *  copied correctly. *)
    val copy : 'a t -> 'a t

    (** An identified individual has an identifier
     *  [ident ind] is the identifier of [ind]. *)
    val ident : id t -> int
  end



(** {4:pop Signatures for a population of individuals} *)


(** Most basic functions for [nonid] [group]s, [trait]s and [indiv]s. *)
module type POP =
  sig
    (** Groups/traits/indivs are [nonid] or [[id | nonid]] *)
    type 'a isid

    (** The type of groups *)
    type 'a group

    (** The type of trait values *)
    type 'a trait

    (** The type of stochasticity source (only used for {!choose}) *)
    type 'a stoch

    (** The type of populations *)
    type t

    (** A population trait is a trait (from TRAIT) *)
    module T : (TRAIT with type 'a t = 'a trait
                       and type 'a group = 'a group)

    (** A population consists of individuals *)
    module I : (INDIV with type 'a trait = 'a trait)

    (** The type of individuals *)
    type 'a indiv = 'a I.t

    (** data for a group *)
    type group_data

    (** [copy z] is a copy of the pop [x] *)
    val copy : t -> t

    (** [new_indiv z x] is an individual with trait [x] in pop [z]. *)
    val new_indiv : t -> 'a isid trait -> 'a isid indiv

    (** [indiv_grom_group g] is a [nonid indiv] of group [g]. *)
    val indiv_from_group : nonid group -> nonid indiv

    (** [count ~group z] is the number of individuals in [group] of [z]. *)
    val count : group:'a isid group -> t -> 'b Util.posint

    (** [fold_groups f z y] is [f] folded
     *  over the groups of [z]. *)
    val fold_groups :
      (group_data -> 'c -> 'c) ->
      t ->
      'c ->
        'c
    
    (** [fold_indivs f z g y] is [f] folded over the individuals
     *  of the group [g] of [z]. *)
    val fold_indivs :
      ('a isid indiv -> 'c -> 'c) ->
      t ->
      'a isid group ->
      'c ->
        'c

    (** [choose ~rng ~group z] is an individual chosen uniformly
     *  from group [group] in [z]. *)
    val choose : rng:'a stoch -> group:'b isid group -> t -> 'b isid indiv

    (** [check_counts z] asserts that indiv counts are positive
     *  for all groups in [z]. *)
    val check_counts : t -> unit
  end


module type WITH_CREATE =
  sig
    type 'a isid
    type 'a group
    type t

    val create :
      ngroups:int ->
      t
  end


module type WITH_ID_CREATE =
  sig
    type 'a isid (* = anyid *)
    type 'a group
    type t

    (** [create ?mem ?groups nindivs]
     *  is an empty population with initial space for [nindivs] indivs,
     *  groups [groups] already added in,
     *  a memory of length [mem]. *)
    val create :
      ?mem:int ->
      ngroups:int ->
      nindivs:int ->
        t
  end


module type WITH_MAX_CREATE =
  sig
    type 'a isid (* = anyid *)
    type 'a trait
    type 'a group
    type t

    (** [create ?mem ?groups ~scores nindivs]
     *  is an empty population with initial space for [nindivs] indivs,
     *  score functions [scores],
     *  groups [groups] already added in,
     *  a memory of length [mem]. *)
    val create :
      ?mem:int ->
      ngroups:int ->
      scores:(id trait -> float) list ->
      nindivs:int ->
        t

    (** [scores ~i z] is the [i]-th score function used for [z]. *)
    val scores : i: int -> t -> (id trait -> float)

    (** [compares ~i z] is the [i]-th comparison function used
     *  for [z]. *)
    val compares : i: int -> t -> id trait comparison

    (** [max ~i z] is the maximum trait value in [z]
     *  under {!compares} [~i z]. *)
    val max : i: int -> t -> float * id trait
  end


module type WITH_DYN =
  sig
    type 'a isid (* = anyid *)
    type 'a indiv
    type t

    (** [add z ind] adds indiv [ind] to [z]. *)
    val add : t -> 'a isid indiv -> unit

    (** [remove z ind] removes indiv [ind] from [z]. *)
    val remove : t -> 'a isid indiv -> unit
  end


module type WITH_TIMED_DYN =
  sig
    type 'a isid
    type 'a indiv
    type t

    (** [add t z ind] adds indiv [ind] to [z], at time [t]. *)
    val add :
      float ->
      t ->
      'a isid indiv ->
        unit

    (** [remove t z ind] removes indiv [ind] from [z], at time [t]. *)
    val remove :
      float ->
      t ->
      'a isid indiv ->
        unit
  end


module type WITH_GENEALOGY_DYN =
  sig
    type 'a isid
    type 'a trait
    type 'a indiv
    type t

    (*
    (** [add_nonid t z ind] adds indiv [ind] to [z], at time [t]. *)
    val add_nonid :
    t -
      nonid indiv ->
        unit
    *)

    (** [add ?parent t z ind] adds indiv [ind] with optional [parent]
     *  to [z]. It only makes sense to pass a [~parent] if the [indiv]
     *  and its [parent] are both [id]. *)
    val add :
      ?parent:'a isid indiv ->
      float ->
      t ->
      'b isid indiv ->
        unit

    (*
    (** [add_id ?parent t z ind] adds indiv [ind] with parent [parent] to [z],
     *  at time [t]. *)
    val add_id :
      ?parent:id indiv ->
      float ->
      t ->
      id indiv ->
        unit
    *)

    (** [remove t z ind] removes indiv [ind] from [z], at time [t]. *)
    val remove :
      float ->
      t ->
      'a isid indiv ->
        unit

    (** [erase z ind] erases indiv [ind] from the genealogy,
     *  and recursively all ancestors that end up
     *  without descendants in the process.
     *  This can be used to progressively prune the genealogy.
     *  It only really makes sense to call this on an [id] indiv. *)
    val erase :
      t ->
      'a isid indiv ->
        unit

    (*
    val erase_id :
      t ->
      id indiv ->
        unit
    *)

    (** [forbid_erase z ind] forbids the erasure of [ind] from [z]. *)
    val forbid_erase : t -> 'a isid indiv -> unit

    (** [remove_erase_living tf z] removes and erases all living individuals
     *  from [z]. *)
    val remove_erase_living : float -> t -> unit

    (** [to_tree_genealogy z] is the {!Tree.Genealogy.t} genealogy
     *  of the population [z]. *)
    val to_tree_genealogy : t -> TG.t

    (** [to_recorded_traits z] is the hash table of trait values
     *  from individuals recorded in the genealogy of the population [z]. *)
    val to_recorded_traits : t -> id trait TG.Intbl.t

    (** [to_recorded_death_traits z] is {!to_recorded_traits} of [z],
     *  but also including times of death. *)
    val to_recorded_death_traits : t -> (float * id trait) TG.Intbl.t

    (** [to_recorded_leaf_traits z] is the hash table of trait values
     *  from individuals at leaves in the genealogy of the population [z]
     *  (those without recorded children). *)
    val to_recorded_leaf_traits : t -> id trait TG.Intbl.t

    (** [to_recorded_leaf_death_traits z] is {!to_recorded_leaf_traits}
     *  of [z] but also including times of death. *)
    val to_recorded_leaf_death_traits : t -> (float * id trait) TG.Intbl.t

    (** [output_genealogy f chan z] outputs the genealogy of the population [z]
     *  to the channel [chan] with trait values converted with [f]. *)
    val output_genealogy :
      (id trait -> string) ->
      out_channel ->
      t ->
        unit

  end


module type WITH_EVENTS =
  sig
    open Sim.Ctmjp.Sig

    type 'a isid
    type 'a trait
    type 'a group
    type 'a indiv
    type t


    (** {1:grp Adapt to population structure} *)

    type ('a, 'stoch, 's) indiv_modif =
      ('a isid indiv -> (t, 'stoch, 's) modif)

    type ('a, 'b, 'stoch, 's) pair_indiv_modif =
      ('a isid indiv -> 'b isid indiv -> (t, 'stoch, 's) modif)

    (** [add_new ?k z x] adds [k] (default [1]) identified individuals
     *  of trait [x] to the population [z]. *)
    val add_new :
      ?k:int ->
      t ->
      'a isid trait ->
        unit

    (** {2:single Modifications for single individuals} *)

    (** [birth_modif ?mut ind] is a modif
     *  that adds a descendant to the individual [ind].
     *  The new trait value is given by [mut]
     *  (by default it doesn't change). *)
    val birth_modif :
      ?mut : ('a isid trait, 'stoch, 't) modif ->
      ('a, 'stoch, 't) indiv_modif

    (** [death_modif ind] is a modif
     *  that removes the individual [ind]. *)
    val death_modif :
      ('a, 'stoch, 't) indiv_modif

    (** [birth_modif_maybe ?mut ind] is a modif
     *  that adds a descendant to the individual [ind],
     *  if [mut] returns some trait value,
     *  in which case it is the trait value of the new indiv,
     *  otherwise nothing happens.
     *  By default a descendant is added with a trait equal to its parent's. *)
    val birth_modif_maybe :
      mut:('stoch -> 't U.pos -> 'a isid trait -> 'b isid trait option) ->
      ('a, 'stoch, 't) indiv_modif

    (** {3:pair Modifications for pairs of individuals} *)

    (** Function name conventions
     *  'b' will indicate birth,
     *  'd' death
     *)
    
    type ('a, 'b, 'c, 's, 't) pair_single_mut =
      ('s -> 't U.pos -> 'a isid trait -> 'b isid trait -> 'c isid trait)

    type ('a, 'b, 's, 't) pair_pair_mut =
      ('a isid trait * 'b isid trait, 's, 't) modif


    (** [pair_bb_modif ?mut ind1 ind2] is like {!birth_modif_id}
     *  with both [ind1] and [ind2] giving birth,
     *  and [mut] applied to get the children's trait values. *)
    val pair_bb_modif :
      ?mut : ('a, 'b, 'stoch, 't) pair_pair_mut ->
      ('a, 'b, 'stoch, 't) pair_indiv_modif

    (** [pair_bd_modif ?mut ind1 ind2] is a modif
     *  where [ind1] gives birth (with [mut] applied),
     *  and [ind2] is removed. *)
    val pair_bd_modif :
      mut : ('a, 'b, 'c, 'stoch, 't) pair_single_mut option ->
      ('a, 'b, 'stoch, 't) pair_indiv_modif

    (** [pair_dd_modif ind1 ind2] is a modif
     *  that removes [ind1] and [ind2]. *)
    val pair_dd_modif :
      ('a, 'b, 'stoch, 't) pair_indiv_modif
  end


(** A population where no individuals are identified.
 *  A functor returning this signature is {!Pop.Nonid.Make}. *)
module type POP_WITH_DYN =
  sig
    include POP
    include WITH_CREATE with type 'a isid := 'a isid
                         and type 'a group := 'a group
                         and type t := t
    include WITH_DYN with type 'a isid := 'a isid
                      and type 'a indiv := 'a indiv
                      and type t := t
  end


module type POP_WITH_DYN_WO_CREATE =
  sig
    include POP
    include WITH_DYN with type 'a isid := 'a isid
                      and type 'a indiv := 'a indiv
                      and type t := t
  end


(** A population where there are [id] and [nonid] groups
 *  (and traits and indivs),
 *  but traits values are not scored, and ancestry is not tracked.
 *  A functor returning this signature is {!Pop.Simple.Make}. *)
module type POP_ID_WITH_DYN =
  sig
    include POP_WITH_DYN_WO_CREATE
    include WITH_ID_CREATE with type 'a isid := 'a isid
                            and type 'a group := 'a group
                            and type t := t
  end


(** A population with [id] and [nonid] groups where [id] indivs
 *  are scored by some array of functions, and the traits scoring
 *  the maximum value for each function are remembered.
 *  A functor returning this signature is {!Pop.With_max.Make}. *)
module type POP_WITH_MAX_DYN =
  sig
    include POP_WITH_DYN_WO_CREATE
    include WITH_MAX_CREATE with type 'a isid := 'a isid
                             and type 'a trait := 'a trait
                             and type 'a group := 'a group
                             and type t := t
  end


module type POP_WITH_GENEALOGY_DYN_WO_CREATE =
  sig
    include POP
    include WITH_GENEALOGY_DYN with type 'a isid := 'a isid
                                and type 'a trait := 'a trait
                                and type 'a indiv := 'a indiv
                                and type t := t
  end


(** A population with [id] and [nonid] groups where [id] indivs
 *  have their parent, children, and birth/death times recorded. *)
module type POP_WITH_GENEALOGY_DYN =
  sig
    include POP_WITH_GENEALOGY_DYN_WO_CREATE
    include WITH_ID_CREATE with type 'a isid := 'a isid
                            and type 'a group := 'a group
                            and type t := t
  end


module type POP_WITH_EVENTS =
  sig
    include POP
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end


module type POP_WITH_EVENTS_DYN_WO_CREATE =
  sig
    include POP_WITH_DYN_WO_CREATE
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end


(** A population with [id] and [nonid] groups,
 *  max scoring trait values remembered,
 *  and ancestry (parent, children, birth and death times) recorded.
 *  A functor returning this signature is {!Pop.With_genealogy.Make},
 *  as well as {!Pop.With_dummy_genealogy.Make},
 *  that satisfies the signature but actually doesn't track the genealogy. *)
module type POP_WITH_MAX_GENEALOGY_DYN =
  sig
    include POP_WITH_GENEALOGY_DYN_WO_CREATE
    include WITH_MAX_CREATE with type 'a isid := 'a isid
                             and type 'a trait := 'a trait
                             and type 'a group := 'a group
                             and type t := t
  end


module type POP_WITH_EVENTS_DYN =
  sig
    include POP_WITH_DYN
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end


module type POP_ID_WITH_EVENTS_DYN =
  sig
    include POP_ID_WITH_DYN
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end


module type POP_WITH_MAX_EVENTS_DYN =
  sig
    include POP_WITH_MAX_DYN
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end


(** A population with [id] and [nonid] groups,
 *  max scoring trait values and ancestry recorded,
 *  and convenience functions for typical events in the population.
 *  A functor returning this signature,
 *  with a {!POP_WITH_MAX_GENEALOGY_DYN} input,
 *  is {!Pop.With_events.Augment_max_genealogy}. *)
module type POP_WITH_ALL =
  sig
    include POP_WITH_MAX_GENEALOGY_DYN
    include WITH_EVENTS with type 'a isid := 'a isid
                         and type 'a trait := 'a trait
                         and type 'a group := 'a group
                         and type 'a indiv := 'a indiv
                         and type t := t
  end
