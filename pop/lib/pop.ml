(** @canonical Pop.Sig *)
module Sig = Sig

(** @canonical Pop.Nonid *)
module Nonid = Nonid

(** @canonical Pop.Nonid_unchecked *)
module Nonid_unchecked = Nonid_unchecked

(** @canonical Pop.With_id *)
module With_id = With_id

(** @canonical Pop.With_max *)
module With_max = With_max

(** @canonical Pop.With_genealogy *)
module With_genealogy = With_genealogy

(** @canonical Pop.With_dummy_id *)
module With_dummy_id = With_dummy_id

(** @canonical Pop.With_dummy_max *)
module With_dummy_max = With_dummy_max

(** @canonical Pop.With_dummy_genealogy *)
module With_dummy_genealogy = With_dummy_genealogy

(** @canonical Pop.With_events *)
module With_events = With_events

