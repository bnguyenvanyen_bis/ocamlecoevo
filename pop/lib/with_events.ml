open Sig

(* resurected with non symbolic modifs for now *)


module Augment_partial
  (P : POP_WITH_DYN_WO_CREATE) =
  struct
    include P

    type ('a, 'stoch, 's) indiv_modif =
      ('a isid indiv -> (t, 'stoch, 's) Sim.Ctmjp.Sig.modif)

    type ('a, 'b, 'stoch, 's) pair_indiv_modif =
      ('a isid indiv -> 'b isid indiv -> (t, 'stoch, 's) Sim.Ctmjp.Sig.modif)

    type ('a, 'b, 'c, 's, 't) pair_single_mut =
      ('s -> 't U.pos -> 'a isid trait -> 'b isid trait -> 'c isid trait)

    type ('a, 'b, 's, 't) pair_pair_mut =
      ('a isid trait * 'b isid trait, 's, 't) Sim.Ctmjp.Sig.modif


    (* generic pop initialization functions *)
    let add_new ?(k=1) z x =
      for _ = 1 to k do
        ignore (add z (new_indiv z x))
      done

    (* identified *)
    let birth_modif ?(mut=(fun _ _ x -> x)) ind stoch t z =
      (* can raise Not_found (which makes sense) *)
      let x = I.trait_of ind in
      let ind' = new_indiv z (mut stoch t x) in
      (* add_id ~parent:ind (F.to_float t) z ind' ; *)
      add z ind' ;
      z

    let death_modif ind _ _ z =
      (* can raise Not_found (which makes sense) *)
      (* remove (F.to_float t) z ind ; *)
      remove z ind ;
      z

    let birth_modif_maybe ~mut ind stoch t z =
      let x = I.trait_of ind in
      match mut stoch t x with
      | None ->
          z
      | Some x' ->
          let ind' = new_indiv z x' in
          add z ind' ;
          z

    (* pair modifs *)

    (* b : birth ; d : death ; _ : nothing *)

    let pair_bb_modif ?(mut=(fun _ _ x -> x)) ind1 ind2 stoch t z =
      let x1 = I.trait_of ind1 in
      let x2 = I.trait_of ind2 in
      let x1', x2' = mut stoch t (x1, x2) in
      let ind1' = new_indiv z x1' in
      let ind2' = new_indiv z x2' in
      add z ind1' ;
      add z ind2' ;
      z

    let pair_bd_modif ~mut =
      match mut with
      | None ->
          (fun ind1 ind2 _ _ z ->
            let x1 = I.trait_of ind1 in
            let ind1' = new_indiv z x1 in
            add z ind1' ;
            remove z ind2 ;
            z
          )
      | Some f ->
          (fun ind1 ind2 stoch t z ->
            let x1 = I.trait_of ind1 in
            let x2 = I.trait_of ind2 in
            let x1' = f stoch t x1 x2 in
            let ind1' = new_indiv z x1' in
            add z ind1' ;
            remove z ind2 ;
            z
          )

    (*
    let pair_bd_modif ?(mut=(fun _ _ x _ -> x)) ind1 ind2 stoch t z =
      let x1 = I.trait_of ind1 in
      let x2 = I.trait_of ind2 in
      let x1' = mut stoch t x1 x2 in
      let ind1' = new_indiv z x1' in
      add z ind1' ;
      remove z ind2 ;
      z
    *)

    let pair_dd_modif ind1 ind2 _ _ z =
      remove z ind1 ;
      remove z ind2 ;
      z
  end


module Augment
  (P : POP_WITH_DYN) =
  struct
    include Augment_partial (P)

    let create = P.create
  end


module Augment_id
  (P : POP_ID_WITH_DYN) =
  struct
    include Augment_partial (P)

    let create = P.create
  end


module Augment_max
  (P : POP_WITH_MAX_DYN) =
  struct
    include Augment_partial (P)

    let create = P.create
    let max = P.max
    let compares = P.compares
    let scores = P.scores
  end


module Augment_max_genealogy
  (P : POP_WITH_MAX_GENEALOGY_DYN) =
  struct
    include P

    type ('a, 'stoch, 's) indiv_modif =
      ('a isid indiv -> (t, 'stoch, 's) Sim.Ctmjp.Sig.modif)

    type ('a, 'b, 'stoch, 's) pair_indiv_modif =
      ('a isid indiv -> 'b isid indiv -> (t, 'stoch, 's) Sim.Ctmjp.Sig.modif)

    type ('a, 'b, 'c, 's, 't) pair_single_mut =
      ('s -> 't U.pos -> 'a isid trait -> 'b isid trait -> 'c isid trait)

    type ('a, 'b, 's, 't) pair_pair_mut =
      ('a isid trait * 'b isid trait, 's, 't) Sim.Ctmjp.Sig.modif


    (* generic pop initialization functions *)
    let add_new ?(k=1) z x =
      for _ = 1 to k do
        ignore (add 0. z (new_indiv z x))
      done

    (* identified *)
    let birth_modif ?(mut=(fun _ _ x -> x)) ind stoch t z =
      (* can raise Not_found (which makes sense) *)
      let x = I.trait_of ind in
      let ind' = new_indiv z (mut stoch t x) in
      add ~parent:ind (F.to_float t) z ind' ;
      z

    let death_modif ind _ t z =
      (* can raise Not_found (which makes sense) *)
      remove (F.to_float t) z ind ;
      z

    let birth_modif_maybe ~mut ind stoch t z =
      let x = I.trait_of ind in
      match mut stoch t x with
      | None ->
          z
      | Some x' ->
          let ind' = new_indiv z x' in
          add ~parent:ind (F.to_float t) z ind' ;
          z

    (* pair modifs *)

    (* b : birth ; d : death ; _ : nothing *)

    let pair_bb_modif ?(mut=(fun _ _ x -> x)) ind1 ind2 stoch t z =
      let x1 = I.trait_of ind1 in
      let x2 = I.trait_of ind2 in
      let x1', x2' = mut stoch t (x1, x2) in
      let ind1' = new_indiv z x1' in
      let ind2' = new_indiv z x2' in
      add ~parent:ind1 (F.to_float t) z ind1' ;
      add ~parent:ind2 (F.to_float t) z ind2' ;
      z

    let pair_bd_modif ~mut =
      match mut with
      | None ->
          (fun ind1 ind2 _ t z ->
            let x1 = I.trait_of ind1 in
            let ind1' = new_indiv z x1 in
            add ~parent:ind1 (F.to_float t) z ind1' ;
            remove (F.to_float t) z ind2 ;
            z
          )
      | Some f ->
          (fun ind1 ind2 stoch t z ->
            let x1 = I.trait_of ind1 in
            let x2 = I.trait_of ind2 in
            let x1' = f stoch t x1 x2 in
            let ind1' = new_indiv z x1' in
            add ~parent:ind1 (F.to_float t) z ind1' ;
            remove (F.to_float t) z ind2 ;
            z
          )

    (*
    let pair_bd_modif ?(mut=(fun _ _ x _ -> x)) ind1 ind2 stoch t z =
      let x1 = I.trait_of ind1 in
      let x2 = I.trait_of ind2 in
      let x1' = mut stoch t x1 x2 in
      let ind1' = new_indiv z x1' in
      add ~parent:ind1 (F.to_float t) z ind1' ;
      remove (F.to_float t) z ind2 ;
      z
    *)

    let pair_dd_modif ind1 ind2 _ t z =
      remove (F.to_float t) z ind1 ;
      remove (F.to_float t) z ind2 ;
      z

    let create = P.create
    let max = P.max
    let compares = P.compares
    let scores = P.scores
  end


module Make (T : TRAIT) =
  struct
    include Augment_max_genealogy (
      With_genealogy.Augment_max (
        With_max.Make (T)
      )
    )
  end
