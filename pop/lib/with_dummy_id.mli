open Sig

(** Here we return POP_WITH_DYN instead of POP_ID_WITH_DYN
 *  because they differ only in the type of [create],
 *  which breaks abstraction : its arguments are details of the implementation,
 *  but here in this dummy version, they are ignored.
 *  So we keep the version without them *)


module Augment :
  functor (P : POP_WITH_DYN with type 'a isid = nonid
                             and type group_data = Nonid.group_data) ->
    (POP_WITH_DYN with type 'a stoch = 'a
                   and type 'a isid = 'a
                   and type 'a trait = 'a P.trait
                   and type 'a group = 'a P.group
                   and type 'a I.t = 'a P.I.t
                   and type group_data = P.group_data)


module Make :
  functor (T : TRAIT) ->
    (POP_WITH_DYN with type 'a stoch = 'a
                   and type 'a isid = 'a
                   and type 'a trait = 'a T.t
                   and type 'a group = 'a T.group
                   and type group_data = Nonid.group_data)
