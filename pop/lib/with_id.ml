open Sig


type group_data_id = {
  mutable count : int ;
  idents : Argrouper.t ;
}

type group_data =
  | Id of group_data_id
  | Nonid of Nonid.group_data


module Augment
  (P : POP_WITH_DYN with type 'a isid = nonid
                     and type group_data = Nonid.group_data) =
  struct
    type 'a stoch = U.rng

    type 'a isid = 'a
    type 'a trait = 'a P.trait
    type 'a group = 'a P.group
    type 'a indiv = 'a P.indiv

    module I = P.I
    module T = P.T

    type nonrec group_data = group_data

    (* FIXME also add [nindivs_per_group] in the record ? *)
    type t = {
      groups_nonid : P.t ;
      groups_id : (id group, group_data_id) H.t ;
      indivs : (int, id indiv) H.t ;
      queue : (id indiv, int) Finitemap.t ;
      mutable maxid : int ;
    }


    let create ?(mem=11) ~ngroups ~nindivs =
      let z = {
        groups_nonid = P.create ~ngroups ;
        groups_id = H.create ngroups ;
        indivs = H.create nindivs ;
        queue = Finitemap.create mem ;
        maxid = 0 ;
      } in z

    let copy_groups_id =
      H.map (fun _ { count ; idents } ->
        { count ; idents = Argrouper.copy idents }
      )

    let copy z = {
      groups_nonid = P.copy z.groups_nonid ;
      groups_id = copy_groups_id z.groups_id ;
      indivs = H.map (fun _ indiv -> I.copy indiv) z.indivs ;
      queue = Finitemap.copy z.queue ;
      maxid = z.maxid ;
    }
    
    let new_indiv_id z x =
      let i = z.maxid in
      z.maxid <- i + 1 ;
      I.id x i

    let new_indiv (type a) z (x : a trait) : a indiv =
      match T.isid (T.group_of x) with
      | Isnotid Eq ->
          I.nonid x
      | Isid Eq ->
          new_indiv_id z x

    let indiv_from_group =
      P.indiv_from_group

    let find_id z g =
      match H.find z.groups_id g with
      | gdi ->
          gdi
      | exception Not_found ->
          (* 997 might end up very insufficient in some cases...
           * what can we do ? *)
          let gdi = { count = 0 ; idents = Argrouper.make 997 } in
          H.add z.groups_id g gdi ;
          gdi

    let count (type a) ~(group : a group) z =
      match T.isid group with
      | Isnotid Eq ->
          P.count ~group z.groups_nonid
      | Isid Eq ->
          match find_id z group with { count ; _ } ->
            U.Int.of_int_unsafe count

   
    (* FIXME make f also take g as a parameter *)
    let fold_groups f z y0 =
      let y = P.fold_groups (fun (gdni : Nonid.group_data) y'->
        f (Nonid gdni) y'
      ) z.groups_nonid y0 
      in
      H.fold (fun _ gdi y' -> f (Id gdi) y') z.groups_id y


    let fold_indivs (type a) (f : a indiv -> 'b -> 'b) z (g : a group) y0 =
      match T.isid g with
      | Isnotid Eq ->
          P.fold_indivs f z.groups_nonid g y0
      | Isid Eq ->
          let f' i y =
            let indiv = H.find z.indivs i in
            f indiv y
          in
          match find_id z g with { idents ; _ } ->
            Argrouper.fold f' idents y0


    let add :
      type a. t -> a I.t -> unit =
      fun z ->
      function
      | (I.Nonid _ as ind) ->
          P.add z.groups_nonid ind
      | (I.Id (x, i) as ind) ->
          let g = T.group_of x in
          (match find_id z g with ({ count ; idents } as idgd) ->
             idgd.count <- count + 1 ;
             H.add z.indivs i ind ;
             ignore (Argrouper.insert i idents)
          )


    let remove :
      type a. t -> a I.t -> unit =
      fun z ->
      function
      | (I.Nonid _ as ind) ->
          P.remove z.groups_nonid ind
      | (I.Id (x, i) as ind) ->
          let g = T.group_of x in
          (match find_id z g with ({ count ; idents } as idgd) ->
             if count <= 0 then raise Not_found ;
             idgd.count <- count - 1 ;
             H.remove z.indivs i ;
             let k =
               match Finitemap.find ind z.queue with
               | k ->
                   k
               | exception Not_found ->
                   Argrouper.find i idents
             in Argrouper.remove k idents
          )

    let choose (type a) ~rng ~(group : a group) z : a I.t =
      match T.isid group with
      | Isnotid Eq ->
          indiv_from_group group
      | Isid Eq ->
          (match find_id z group with { idents ; _ } ->
             let k, i = Argrouper.choose ~rng idents in
             let ind = H.find z.indivs i in
             Finitemap.add ind k z.queue ;
             ind
          )

    let check_counts z =
      let f gd () =
        match gd with
        | Id { count ; _ } | Nonid { count } ->
            assert (count >= 0)
      in
      fold_groups f z ()
  end


module Make (T : TRAIT) (* :
  (POP_ID_WITH_DYN ...) *) =
  struct
    include Augment (Nonid.Make (T))
  end
