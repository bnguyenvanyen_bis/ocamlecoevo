open Sig


let option_map f =
  function
  | Some x -> Some (f x)
  | None -> None


module Augment
  (P : POP_ID_WITH_DYN with type 'a isid = 'a) =
  struct
    type 'a stoch = 'a P.stoch

    type 'a isid = 'a P.isid
    type 'a trait = 'a P.trait
    type 'a group = 'a P.group
    type 'a indiv = 'a P.indiv

    module I = P.I
    module T = P.T

    type group_data = P.group_data

    type t = {
      groups : P.t ;
      scores : (id trait -> float) array ;
      mutable maxtraits : (float * id trait) array option ;
    }

    let scores ~i z = z.scores.(i)

    let compares ~i z =
      let sc = scores ~i z in
      (fun x x' -> compare (sc x) (sc x'))

    let create ?mem ~ngroups ~scores ~nindivs = {
      groups = P.create ?mem ~ngroups ~nindivs ;
      maxtraits = None ;
      scores = A.of_list scores ;
    }

    let new_indiv z = P.new_indiv z.groups

    let indiv_from_group = P.indiv_from_group

    let copy z = {
      groups = P.copy z.groups ;
      scores = z.scores ;
      maxtraits = option_map (A.map (fun (sc, x) ->
        (sc, T.copy x))
      ) z.maxtraits
    }

    let count ~group z = P.count ~group z.groups
    
    let max ~i z =
      match z.maxtraits with
      | None ->
          raise Not_found
      | Some a ->
          a.(i)

    let fold_groups f z y0 =
      P.fold_groups f z.groups y0

    let fold_indivs f z g y0 =
      P.fold_indivs f z.groups g y0

    let choose ~rng ~group z =
      P.choose ~rng ~group z.groups
    
    let update_max z (x : id trait) =
      let greatcomp sc_a i score =
        let sc = score x in
        let sc_max, _ = sc_a.(i) in
        if sc_max < sc then
          sc_a.(i) <- (sc, x)
      in
      begin
        match z.maxtraits with
        | None ->
            (* initialization *)
            z.maxtraits <- Some (Array.map (fun score ->
              (score x, x))
            z.scores)
        | Some sc_a -> 
            Array.iteri (greatcomp sc_a) z.scores
      end
    
    let add :
      type a. t -> a isid I.t -> unit =
      fun z ->
      function
      | (I.Id _) as ind ->
          update_max z (I.trait_of ind) ;
          P.add z.groups ind
      | (I.Nonid _ as ind) ->
          P.add z.groups ind

    let remove z ind =
      P.remove z.groups ind

    let check_counts z =
      P.check_counts z.groups
  end


module Make (T : TRAIT) =
  struct
    include Augment (With_id.Make (T))
  end
