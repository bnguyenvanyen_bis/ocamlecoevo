
type trait = A | B

type param = {
  b : float ;
  d : float ;
  mua : float ;
  mub : float ;
}

let specl = [
  ("-b", Sim.Arg.Float (fun par x -> { par with b = x }),
   "Individual (neutral) birth rate") ;
  ("-d", Sim.Arg.Float (fun par x -> { par with d = x }),
   "Individual (neutral) death rate") ;
  ("-muA", Sim.Arg.Float (fun par x -> { par with mua = x }),
   "Individual mutation rate A -> B") ;
  ("-muB", Sim.Arg.Float (fun par x -> { par with mub = x }),
   "Individual mutation rate B -> A") ;
]

module Trait =
  struct
    type t = trait
    type x = trait
    let phenotype_of x = x
    let equal i j =
      match i, j with
      | ((A, A) | (B, B)) -> true
      | ((A, B) | (B, A)) -> false
    let hash p =
      match p with
      | A -> 0
      | B -> 1
    let string_of x =
      match x with
      | A -> "A"
      | B -> "B"
    let parse s =
      if s = "A" then A
      else if s = "B" then B
      else invalid_arg "invalid string"
  end

let color x =
  match x with
  | A -> (255., 0., 0.)
  | B -> (0., 0., 255.)


module System =
  struct
    type nonrec param = param
    type nonrec trait = trait
    type group = trait
    module Make (P : Pop.Sig.POP with type trait = trait
                                  and type group = group) :
      (Pop.Bdmi.POPEVENTS with type t = P.t
                           and type trait = P.trait
                           and type param = param) =
      struct
        include Pop.Bdmi.Eventypes (struct
          include P
          type nonrec param = param
        end)

        let birth_maxrate par _ _ =
          par.b

        let death_maxrate par _ z =
          par.d *. float (P.count_all z - 1)

        let mutation_maxrate_a par _ _ =
          par.mua

        let mutation_maxrate_b par _ _ =
          par.mub

        let mutate _ _ _ x =
          match x with
          | A -> B
          | B -> A

        let count_a z = P.count z A
        let count_b z = P.count z B

        let draw _ rng _ z = P.choose_any rng z
        let draw_a _ rng _ z =
          match P.choose rng z A with
          | None -> failwith "None"
          | Some i -> i
        let draw_b _ rng _ z =
          match P.choose rng z B with
          | None -> failwith "None"
          | Some i -> i

        let rel1 _ _ _ _ = 1.

        let event_l = [
          Birth (birth_maxrate,
                 P.count_all,
                 draw,
                 rel1) ;
          Death (death_maxrate,
                 P.count_all,
                 draw,
                 rel1) ;
          Mutation (mutation_maxrate_a,
                    count_a,
                    draw_a,
                    rel1,
                    mutate) ;
          Mutation (mutation_maxrate_b,
                    count_b,
                    draw_b,
                    rel1,
                    mutate)
        ]

      end
  end
