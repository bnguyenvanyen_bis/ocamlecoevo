(** A simple intra-host model
 *  [(x, y)] with [x] the number of parasites, and [y] the immune response level.
 *  - parasite birth at rate [b x]
 *  - parasite death at rate [d x]
 *  - parasite culling by the immune system at rate [mu x y]
 *  - immune response increase at rate [alpha x]
 *)

open Sig
module SCU = Sim.Ctmjp.Util


type param = {
  b : float ;
  d : float ;
  mu : float ;
  alpha : float ;
}


module T =
  struct
    open Pop.Sig

    type _ t =
      | V : nonid t  (* virus *)
      | M : nonid t  (* immune cell *)

    type _ group =
      | Vir : nonid group
      | Imu : nonid group

    let copy x = x

    let group_of : type a. a t -> a group =
      function
      | V -> Vir
      | M -> Imu

    let of_group : nonid group -> nonid t =
      function
      | Vir -> V
      | Imu -> M

    let isid : type a. a group -> a isid =
      function
      | Vir -> Isnotid Eq
      | Imu -> Isnotid Eq

    let equal = (=)

    let hash = Hashtbl.hash
  end


module P = Pop.With_events.Augment (
  Pop.With_dummy_id.Augment (
    Pop.Nonid.Make (T)
  )
)


module Param =
  struct
    type t = param

    let default = {
      b = 1. ;
      d = 0. ;
      mu = 1. ;
      alpha = 1. ;
    }

    let birth par = F.Pos.of_float par.b

    let death par = F.Pos.of_float par.d

    let culling par = F.Pos.of_float par.mu

    let response par = F.Pos.of_float par.alpha
  end


module Rates =
  struct
    let float_count ~group z =
      I.Pos.to_float (P.count ~group z)

    let birth p _ z =
      F.Pos.mul (Param.birth p) (float_count ~group:T.Vir z)

    let death p _ z =
      F.Pos.mul (Param.death p) (float_count ~group:T.Vir z)

    let culling p _ z =
      F.Pos.Op.(
          (Param.culling p)
        * (float_count ~group:T.Vir z)
        * (float_count ~group:T.Imu z)
      )

    (* alpha * x *)
    let response p _ z =
      F.Pos.mul (Param.response p) (float_count ~group:T.Vir z)
  end


module Modifs =
  struct
    let ifg = P.indiv_from_group

    let birth = P.birth_modif (ifg T.Vir)

    let death = P.death_modif (ifg T.Vir)

    let culling = P.death_modif (ifg T.Vir)

    let response = P.birth_modif (ifg T.Imu)
  end


module Out =
  struct
    let par_columns = [
      "b" ;
      "d" ;
      "mu" ;
      "alpha" ;
      "seed" ;
      "dt" ;
      "tf" ;
      "n_parasites_0" ;
    ]

    let par_extract (seed, dt, tf, n_parasites_0, p) =
      let sof = string_of_float in
      function
      | "seed" ->
          begin match seed with
          | Some n ->
              string_of_int n
          | None ->
              ""
          end
      | "dt" ->
          begin match dt with
          | Some x ->
              F.to_string x
          | None ->
              ""
          end
      | "tf" ->
          F.to_string tf
      | "n_parasites_0" ->
          I.to_string n_parasites_0
      | "b" ->
          sof p.b
      | "d" ->
          sof p.d
      | "mu" ->
          sof p.mu
      | "alpha" ->
          sof p.alpha
      | _ ->
          invalid_arg "unrecognized column"


    let columns = [
      "t" ;
      "n_parasites" ;
      "immune_response" ;
    ]


    let extract t z =
      function
      | "t" ->
          F.to_string t
      | "n_parasites" ->
          I.to_string (P.count ~group:T.Vir z)
      | "immune_response" ->
          I.to_string (P.count ~group:T.Imu z)
      | _ ->
          invalid_arg "unrecognized column"


    let line t z = L.map (extract t z) columns


    let convert ?seed ?dt ~out par n0 tf =
      let chan = open_out (out ^ ".traj.csv") in
      let output = Sim.Csv.convert ?dt ~header:[columns] ~line ~chan in
      let start t0 y0 =
        let par_path = out ^ ".par.csv" in
        Util.Csv.output
          ~columns:par_columns
          ~extract:par_extract
          ~path:par_path
          [(seed, dt, tf, n0, par)] ;
        output.start t0 y0
      in
      let return tf yf =
        let ret = output.return tf yf in
        close_out chan ;
        ret
      in { output with start ; return }
  end


let events par = [
  (Rates.birth par, Modifs.birth) ;
  (Rates.death par, Modifs.death) ;
  (Rates.culling par, Modifs.culling) ;
  (Rates.response par, Modifs.response) ;
]


let event_sum par =
  let evs = events par in
  let rate_modifs = L.map (fun (r, m) -> SCU.combine r m) evs in
  SCU.add_many rate_modifs


let simulate_until ?seed ?dt ~out par n0 tf =
  (* output *)
  let output = Out.convert ?seed ?dt ~out par n0 tf in
  (* initial population with n0 viruses *)
  let z0 = P.create ~ngroups:2 in
  ignore (P.add_new ~k:(I.to_int n0) z0 T.V) ;
  (* simulation *)
  let ev = event_sum par in
  let next = Sim.Ctmjp.Gill.integrate ev (Util.rng seed) in
  Sim.Loop.simulate_until ~output ~next z0 tf


module Term :
  sig
    open Cmdliner

    val simulate_until : (U.closed_pos * P.t) Term.t
    val info : Term.info
  end =
  struct
    (* We copy from coalfit/lib/term.ml
     * We should probably lift this in some Util module
     *)

    open Cmdliner

    let positive_opt ~doc ~docv ~arg =
      Arg.(value
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    let positive_req ~doc ~docv ~arg =
      Arg.(required
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    let tf =
      let doc = "Simulate the system until $(docv)." in
      positive_req ~doc ~docv:"TF" ~arg:"tf"

    let seed =
      let doc = "Initialize the PRNG with the seed $(docv)." in
      Arg.(value & opt (some int) None & info ["seed"] ~docv:"SEED" ~doc)

    let dt =
      let doc = "Print the state at least every $(docv)." in
      positive_opt ~doc ~docv:"DT" ~arg:"dt"

    let out =
      let doc = "Output results to files starting by $(docv) as CSV." in
      Arg.(value & pos 0 string "out" & info [] ~docv:"OUT" ~doc)

    let n_parasite_0 =
      let doc =
        "Initial number of pathogens."
      in
      Arg.(value
         & opt (U.Term.posint ()) I.one
         & info ["n-parasites-0"] ~docv:"N-PARASITES-0" ~doc
      )

    let birth =
      let doc =
        "Individual pathogen birth rate."
      in positive_opt ~doc ~docv:"BIRTH" ~arg:"birth"

    let death =
      let doc =
        "Individual natural pathogen death rate."
      in positive_opt ~doc ~docv:"DEATH" ~arg:"death"

    let mu =
      let doc =
        "Immune response pathogen killing rate."
      in positive_opt ~doc ~docv:"MU" ~arg:"mu"
     
    let alpha =
      let doc =
        "Immune response activation rate."
      in positive_opt ~doc ~docv:"ALPHA" ~arg:"alpha"

    let fold_with_opt fos x =
      L.fold_left (fun x' ->
        function
        | None ->
            x'
        | Some f ->
            f x'
      ) x fos

    let param =
      let opt_map = U.Option.map in
      let set_par b d mu alpha =
        let f_b = opt_map (fun x p -> { p with b = x }) b in
        let f_d = opt_map (fun x p -> { p with d = x }) d in
        let f_mu = opt_map (fun x p -> { p with mu = x }) mu in
        let f_alpha = opt_map (fun x p -> { p with alpha = x }) alpha in
        fold_with_opt [ f_b ; f_d ; f_mu ; f_alpha ] Param.default
      in
      let pos_to_float = Term.const (opt_map F.to_float) in
      Term.(const set_par
          $ (pos_to_float $ birth)
          $ (pos_to_float $ death)
          $ (pos_to_float $ mu)
          $ (pos_to_float $ alpha)
      )

    let simulate_until =
      let sim_flat seed dt out par n0 tf =
        simulate_until ?seed ?dt ~out par n0 tf
      in Term.(const sim_flat
             $ seed
             $ dt
             $ out
             $ param
             $ n_parasite_0
             $ tf
      )

    let info =
      let doc =
        "Simulate a simple stochastic intra-host model
        with Gillespie's algorithm."
      in Term.info
        "eco-intra"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end


