(** Logistic birth-death process
 *  Linear birth, linear death, plus death by competition
 *  (when a pair interacts, an individual of the pair dies)
 *)


open Sig
module SCU = Sim.Ctmjp.Util


type param = {
  b : float ;
  d : float ;
  c : float ;
}


module T =
  struct
    open Pop.Sig

    type _ t = T : nonid t

    type _ group = G : nonid group

    let copy x = x

    let group_of : type a. a t -> a group =
      function
      | T -> G

    let of_group G = T

    let isid : type a. a group -> a isid =
      function
      | G -> Isnotid Eq

    let equal = (=)

    let hash = Hashtbl.hash
  end


module P = Pop.With_events.Augment (
  Pop.With_dummy_id.Augment (
    Pop.Nonid.Make (T)
  )
)


module Param =
  struct
    type t = param

    let default = {
      b = 1. ;
      d = 0. ;
      c = 1. ;
    }

    let birth par = F.Pos.of_float par.b

    let death par = F.Pos.of_float par.d

    let comp par = F.Pos.of_float par.c
  end


module Rates =
  struct
    let count z = P.count ~group:T.G z

    let float_count z = I.Pos.to_float (count z)

    let birth p _ z =
      F.Pos.mul (Param.birth p) (float_count z)

    let death p _ z =
      F.Pos.mul (Param.death p) (float_count z)

    let competition p _ z =
      let c = count z in
      if I.Op.(c = I.zero) then
        F.zero
      else
        let cm1 = I.Pos.of_anyint (I.pred c) in
        F.Pos.mul
          (Param.comp p) 
          I.Pos.(to_float (mul c cm1))
  end


module Modifs =
  struct
    let indiv = P.indiv_from_group T.G

    let birth stoch t z =
      P.birth_modif indiv stoch t z

    let death stoch t z =
      P.death_modif indiv stoch t z

    let competition stoch t z =
      P.death_modif indiv stoch t z
  end


module Color =
  struct
    type t = [
      | `Birth
      | `Death
      | `Competition
    ]

    let to_int =
      function
      | `Birth ->
          0
      | `Death ->
          1
      | `Competition ->
          2

    let compare c c' =
      (to_int c) - (to_int c')

    let equal c c' = (compare c c' = 0)

    let to_string =
      function
      | `Birth ->
          "Birth"
      | `Death ->
          "Death"
      | `Competition ->
          "Competition"

    let of_string =
      function
      | "Birth" ->
          Some `Birth
      | "Death" ->
          Some `Death
      | "Competition" ->
          Some `Competition
      | _ ->
          None
  end


module Point = Sim.Prm.Point (Color)


module Prm_color = Sim.Prm.Make (Sim.Prm.Compare_color (Point))


module Out =
  struct
    let par_columns = [
      "b" ;
      "d" ;
      "c" ;
      "seed" ;
      "dt" ;
      "tf" ;
      "x0" ;
    ]

    let par_extract (seed, dt, tf, x0, p) =
      let sof = string_of_float in
      function
      | "seed" ->
          begin match seed with
          | Some n ->
              string_of_int n
          | None ->
              ""
          end
      | "dt" ->
          begin match dt with
          | Some x ->
              F.to_string x
          | None ->
              ""
          end
      | "tf" ->
          F.to_string tf
      | "x0" ->
          I.to_string x0
      | "b" ->
          sof p.b
      | "d" ->
          sof p.d
      | "c" ->
          sof p.c
      | _ ->
          invalid_arg "unrecognized column"

    let columns = [
      "t" ;
      "x" ;
    ]

    let extract t z =
      function
      | "t" ->
          F.to_string t
      | "x" ->
          I.to_string (P.count ~group:T.G z)
      | _ ->
          invalid_arg "unrecognized column"

    let line t z = L.map (extract t z) columns

    let csv_convert ?seed ?dt ~out par x0 tf =
      let chan = open_out (out ^ ".traj.csv") in
      let output = Sim.Csv.convert_compat ?dt ~header:[columns] ~line ~chan in
      let start t0 y0 =
        let par_path = out ^ ".par.csv" in
        Util.Csv.output
          ~columns:par_columns
          ~extract:par_extract
          ~path:par_path
          [(seed, dt, tf, x0, par)] ;
        output.start t0 y0
      in
      let return tf yf =
        let ret = output.return tf yf in
        close_out chan ;
        ret
      in { output with start ; return }

    let conv _ z =
      [ P.count ~group:T.G z ]

    let stack_convert = Sim.Cadlag.convert ~conv
  end


module Simulate =
  struct
    module Prm_sim = Sim.Ctmjp.Prm_approx.Make (Point)

    module Cm = Prm_color.Cm


    let colors = [ `Birth ; `Death ; `Competition ]

    let vectors ~h =
      let du0 = F.Pos.of_float 11. in
      let umax = F.Pos.Op.(du0 / h) in
      L.map (fun c ->
        (I.one, Sim.Prm.{ c ; k = 1 ; t = F.zero ; u = umax })
      ) colors

    let events par = [
      `Birth, (Rates.birth par, Modifs.birth) ;
      `Death, (Rates.death par, Modifs.death) ;
      `Competition, (Rates.competition par, Modifs.competition) ;
    ]

    let gill_event_sum par =
      let evs = events par in
      let _, rms = L.split evs in
      let rate_modifs = L.map (fun (r, m) -> SCU.combine r m) rms in
      SCU.add_many rate_modifs

    let prm_event_map par =
      let evs = events par in
      let evm = L.fold_left (fun m (c, rm) ->
          Cm.add c rm m
        ) Cm.empty evs
      in evm
        
    let tf =
      function
      | `Gill (_, tf) ->
          tf
      | `Prm_approx nu ->
          let _, tf = Prm_color.time_range nu in
          tf

    let create x0 =
      let z0 = P.create ~ngroups:1 in
      ignore (P.add_new ~k:(I.to_int x0) z0 T.T) ;
      z0

    let until ~out ~tech ?dt par x0 =
      let convert ?seed tf =
        match out with
        | `Stack ->
            Out.stack_convert ?dt
        | `Csv path ->
            Out.csv_convert ?seed ?dt ~out:path par x0 tf
      in
      let z0 = create x0 in
      match tech with
      | `Gill (seed, tf) ->
          let ev = gill_event_sum par in
          let next = Sim.Ctmjp.Gill.integrate ev (Util.rng seed) in
          let output = convert ?seed tf in
          Sim.Loop.simulate_until ~output ~next z0 tf
      | `Prm_approx nu ->
          let evm = prm_event_map par in
          let output = convert (tf tech) in
          Prm_sim.simulate_until ~output evm z0 nu
  end


module Term :
  sig
    open Cmdliner

    val simulate_until : (U.closed_pos * 'a U.posint list) list Term.t
    val info : Term.info
  end =
  struct
    (* We copy from coalfit/lib/term.ml and eco/lib/intra.ml
     * We should probably lift this in some Util module
     *)

    open Cmdliner

    let positive_opt ~doc ~docv ~arg =
      Arg.(value
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    let positive_req ~doc ~docv ~arg =
      Arg.(required
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    let tf =
      let doc = "Simulate the system until $(docv)." in
      positive_req ~doc ~docv:"TF" ~arg:"tf"

    let seed =
      let doc = "Initialize the PRNG with the seed $(docv)." in
      Arg.(value & opt (some int) None & info ["seed"] ~docv:"SEED" ~doc)

    let dt =
      let doc = "Print the state at least every $(docv)." in
      positive_opt ~doc ~docv:"DT" ~arg:"dt"

    let out =
      let doc = "Output results to files starting by $(docv) as CSV." in
      Arg.(value & pos 0 string "out" & info [] ~docv:"OUT" ~doc)

    let x0 =
      let doc =
        "Initial number of individuals."
      in
      Arg.(value
         & opt (U.Term.posint ()) I.one
         & info ["x0"] ~docv:"X0" ~doc
      )

    let birth =
      let doc =
        "Individual pathogen birth rate."
      in positive_opt ~doc ~docv:"BIRTH" ~arg:"birth"

    let death =
      let doc =
        "Individual natural pathogen death rate."
      in positive_opt ~doc ~docv:"DEATH" ~arg:"death"
    
    let competition =
      let doc =
        "Competitive death rate."
      in positive_opt ~doc ~docv:"COMP" ~arg:"comp"

    let fold_with_opt fos x =
      L.fold_left (fun x' ->
        function
        | None ->
            x'
        | Some f ->
            f x'
      ) x fos

    let param =
      let opt_map = U.Option.map in
      let set_par b d c =
        let f_b = opt_map (fun x p -> { p with b = x }) b in
        let f_d = opt_map (fun x p -> { p with d = x }) d in
        let f_c = opt_map (fun x p -> { p with c = x }) c in
        fold_with_opt [ f_b ; f_d ; f_c ] Param.default
      in
      let pos_to_float = Term.const (opt_map F.to_float) in
      Term.(const set_par
          $ (pos_to_float $ birth)
          $ (pos_to_float $ death)
          $ (pos_to_float $ competition)
      )

    let simulate_until =
      let sim_flat (seed : int option) dt out par x0 (tf : 'a U.anypos) =
        let tech = `Gill (seed, tf) in
        Simulate.until ~tech ?dt ~out:(`Csv out) par x0
      in Term.(const sim_flat
             $ seed
             $ dt
             $ out
             $ param
             $ x0
             $ tf
      )

    let info =
      let doc =
        "Simulate the logistic birth-death process."
      in Term.info
        "eco-logistic"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end
