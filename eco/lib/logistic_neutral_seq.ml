(* Logistic birth and death process with neutral sequences *)
open Sig

module Ss = Seqsim
module Ssp = Ss.Seqpop

module SCU = Sim.Ctmjp.Util

type param = {
  b : float ;  (* individual birth rate *)
  c : float ;  (* competitive death rate *)
  d : float ;  (* natural death rate *)
  evo : Ss.Mut.param ;
  p_keep : float ;
}

(* record history and merge (with seqpop) *)
type trait = Seqs.t * (float * Seqs.Diff.t) list

let to_mut mu' =
  let jc = Ss.Jc69.{ mu = mu' } in
  let snp = Ss.Jc69.to_snp jc in
  Ss.Mut.{ snp ; mu_del = 0. ; mu_rep = 0. ; repdel_len = 0. }


let default = {
  b = 1. ;
  c = 1. ;
  d = 0. ;
  evo = to_mut 1. ;
  p_keep = 1.
}


module T :
  sig
    open Pop.Sig
    include TRAIT
    val gid : id group
    val score : id t -> float
    val id : trait -> id t
    val unphantom : 'a t -> trait
  end =
  struct
    open Pop.Sig

    type _ t =
      | Seq : trait -> id t

    type _ group =
      | Id : id group

    let copy : type a. a t -> a t =
      function
      | Seq x ->
          Seq (Seqsim.Trait.copy x)
      | _ ->
          .

    let group_of : type a. a t -> a group =
      function
      | Seq _ -> Id
      | _ -> .

    let of_group _ = failwith "never nonid"

    let isid : type a. a group -> a isid =
      function
      | Id -> Isid Eq
      | _ -> .

    let gid = Id

    let score (Seq (seq, _)) =
      F.to_float (I.to_float (Seqs.length seq))

    let id x = Seq x

    let unphantom : type a. a t -> trait =
      function
      | Seq x -> x
      | _ -> .
  end

module P = Pop.With_events.Make (T)

let and_payload ind = (ind, T.unphantom (P.I.trait_of ind))


let birth par = F.Pos.of_float par.b

let comp par = F.Pos.of_float par.c

let death par = F.Pos.of_float par.d


let birth_rate par _ z =
  F.Pos.mul (birth par) (I.Pos.to_float (P.count ~group:T.gid z))


(*
let birth_modif par = P.birth_modif_id
 *)


let comp_death_rate par _ z =
  let c = comp par in
  let n = P.count ~group:T.gid z in
  (* we distinguish the 0 case because of nm1 *)
  if I.Op.(n = I.zero) then
    F.zero
  else
    let nm1 = I.Pos.pred n in
    let fn = I.Pos.to_float n in
    let fnm1 = I.Pos.to_float nm1 in
    F.Pos.Op.(c * fn * fnm1)


let nat_death_rate par _ z =
  let n = P.count ~group:T.gid z in
  F.Pos.mul (death par) (I.Pos.to_float n)


let update_on_record t (seq, sdl) =
  (seq, (t, Seqs.Sig.Not) :: sdl)


let record_proba par _ _ = F.Proba.of_float par.p_keep


(*
 * let no_record_proba par t z =
 * F.Proba.compl (record_proba par t z)
 *)


let maybe_record par ind =
  SCU.choose_between
    (record_proba par)
    (fun _ _ z -> P.forbid_erase z ind ; z)
    (fun _ _ z -> P.erase z ind ; z)


(* FIXME on death we need to update the sdl with Seqs.Not ,
 * or use something else instead of output_seqs *)

let record_death_modif par ind = Sim.Ctmjp.Util.(
  chain
    (* kill the individual *)
    (P.death_modif ind)
    (* and maybe remove it from the genealogy (proba 1 - pkeep) *)
    (maybe_record par ind)
)


(* FIXME should we use seq len as score, or one score per mutation type ? *)

let mut_rate sub_rate par t z =
  let _, x = P.max ~i:0 z in
  let n = P.count ~group:T.gid z in
  let fn = I.Pos.to_float n in
  F.Pos.mul fn (sub_rate par.evo t (T.unphantom x))


let mut_subst_rate = mut_rate Ss.Mut.subst_rate

(*
let mut_rep_rate = mut_rate Ss.Mut.rep_subseq_rate

let mut_del_rate = mut_rate Ss.Mut.del_subseq_rate
*)


let mut_modif sub_modif par =
  let mut = Some (fun rng t x _ ->
    x |> T.unphantom |> sub_modif par.evo rng t |> T.id
  )
  in
  let f ind =
    P.pair_bd_modif ~mut ind ind
  in f


let mut_subst_modif = mut_modif Ss.Mut.subst_modif

(*
let mut_rep_modif = mut_modif Ss.Mut.rep_subseq_modif

let mut_del_modif = mut_modif Ss.Mut.del_subseq_modif
*)


let of_indiv_modif m rng t z =
  let ind = P.choose ~rng ~group:T.gid z in
  m ind rng t z


let of_pair_modif m rng t z =
  let ind = P.choose ~rng ~group:T.gid z in
  let ind' = P.choose ~rng ~group:T.gid z in
  m ind ind' rng t z


let event par = Sim.Ctmjp.Util.(add_many [
  combine (birth_rate par) (of_indiv_modif P.birth_modif) ;
  combine (comp_death_rate par) (of_indiv_modif (record_death_modif par)) ;
  combine (nat_death_rate par) (of_indiv_modif (record_death_modif par)) ;
  combine (mut_subst_rate par) (of_indiv_modif (mut_subst_modif par)) ;
  (* combine (mut_rep_rate par) (of_indiv_modif (mut_rep_modif par)) ; *)
  (* combine (mut_del_rate par) (of_indiv_modif (mut_del_modif par)) ; *)
])


module Out =
  struct
    module O = Seqsim.Seqpop.Out

    let specl = O.specl

    let default = O.default

    let output_par chan par =
      Printf.fprintf chan
      "Logistic birth death process parameter values :\n
       b=%f ; c=%f ; d=%f ; p_keep=%f\n"
      par.b par.c par.d par.p_keep ;
      Ss.Mut.output_par chan par.evo

    let header =
      [["t" ; "n" ; "coal"]]

    let line par t z =
      let sof = string_of_float in
      let soi = string_of_int in
      let n = I.to_int (P.count ~group:T.gid z) in
      let coal = par.b /. (float n) in
      [ sof (F.to_float t) ; soi n ; sof coal ]

    (*
    let output_seqs chan record =
      O.output_seqs chan (O.TGI.map (fun _ x ->
          T.unphantom x
        ) record)
    *)

    let output_seqs chan record =
      let fmap (i, (t_death, trait)) =
        let seq, _ = T.unphantom trait in
        (i, t_death, seq)
      in
      record
      |> Tree.Genealogy.Intbl.to_list
      |> L.map fmap
      |> Seqs.Io.stamped_fasta_of_assoc chan

    let output_trees chan genealogy =
      O.output_trees_from_genealogy chan genealogy

    let convert ?dt ~out par =
      output_par out.Ssp.parchan par ;
      close_out out.Ssp.parchan ;
      let chan = out.Ssp.csvchan in
      let line = line par in
      let output = Sim.Csv.convert ?dt ~header ~line ~chan in
      let return tf zf =
        let ret = output.return tf zf in
        P.remove_erase_living (F.to_float tf) zf ;
        let genealogy = P.to_tree_genealogy zf in
        (* FIXME or should we only keep leaves ? *)
        let record = P.to_recorded_leaf_death_traits zf in
        output_seqs out.Ssp.seqchan record ;
        O.output_trees_from_genealogy out.Ssp.treechan genealogy ;
        O.close out ;
        ret
      in { output with return = return }
  end


(* need to use this to have the right P.max to use *)
let create ~nindivs =
  P.create ~mem:3 ~ngroups:1 ~scores:[T.score] ~nindivs


let simulate_until ?seed ?dt out par tf z0 =
  let next = Sim.Ctmjp.Gill.integrate (event par) (Util.rng seed) in
  let output = Out.convert ?dt ~out par in
  Sim.Loop.simulate_until ~output ~next z0 tf


module Term =
  struct
    open Cmdliner

    let positive_opt ~doc ~docv ~arg =
      Arg.(value
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    let fold_with_opt fos x =
      L.fold_left (fun x' ->
        function
        | None ->
            x'
        | Some f ->
            f x'
      ) x fos

    let b =
      let doc = "Individual birth rate." in
      positive_opt ~doc ~docv:"B" ~arg:"b"

    let d =
      let doc = "Natural individual death rate." in
      positive_opt ~doc ~docv:"D" ~arg:"d"

    let c =
      let doc = "Pair death rate from competition." in
      positive_opt ~doc ~docv:"C" ~arg:"c"

    let mu =
      let doc = "Nucleotide substitution rate." in
      positive_opt ~doc ~docv:"MU" ~arg:"mu"

    let p_keep =
      let doc = "Probability of sampling a sequence on death." in
      Arg.(value
         & opt (some (U.Term.proba ())) None
         & info ["p-keep"] ~docv:"P-KEEP" ~doc
      )

    let par =
      let opt_map = U.Option.map in
      let set_par b d c mu p_keep =
        let set_b = opt_map (fun x p -> { p with b = x }) b in
        let set_d = opt_map (fun x p -> { p with d = x }) d in
        let set_c = opt_map (fun x p -> { p with c = x }) c in
        let set_mu = opt_map (fun x p -> { p with evo = to_mut x }) mu in
        let set_pk = opt_map (fun x p -> { p with p_keep = x }) p_keep
        in
        fold_with_opt [ set_b ; set_d ; set_c ; set_mu ; set_pk ] default
      in
      let float_opt = Term.const (opt_map F.to_float) in
      Term.(const set_par
          $ (float_opt $ b)
          $ (float_opt $ d)
          $ (float_opt $ c)
          $ (float_opt $ mu)
          $ (float_opt $ p_keep)
      )

    let seed =
      let doc = "Initialize the PRNG with the seed $(docv)." in
      Arg.(value
         & opt (some int) None
         & info ["seed"] ~docv:"SEED" ~doc
      )

    let dt : U.closed_pos option Term.t =
      let doc = "Print the system state at least every $(docv)." in
      positive_opt ~doc ~docv:"DT" ~arg:"dt"

    let tf : U.closed_pos Term.t =
      let doc = "Simulate the system for a duration $(docv)." in
      Arg.(required
         & opt (some (U.Term.positive ())) None
         & info ["tf"] ~docv:"TF" ~doc
      )

    let out =
      let doc =
        "Output results to file names starting by $(docv) in the CSV format."
      in
      let path = Arg.(value
          & pos 0 string "out"
          & info [] ~docv:"OUT" ~doc
        )
      in
      Term.(const Ssp.Out.open_out $ path)

    let z0 =
      let doc = "Path to fasta sequences file." in
      let path = Arg.(value
          & opt (some string) None
          & info ["sequences"] ~docv:"SEQUENCES" ~doc
        )
      in
      let read =
        function
        | None ->
            create ~nindivs:0
        | Some path ->
            let c = open_in path in
            let kseqs = Seqs.Io.assoc_of_fasta c in
            close_in c ;
            let z0 = create ~nindivs:(L.length kseqs) in
            L.iter (fun (k, seq) ->
              ignore (P.add_new ~k z0 (T.id (seq, [(0., Seqs.Sig.Not)])))
            ) kseqs ;
            z0
      in
      Term.(const read $ path)

  end


