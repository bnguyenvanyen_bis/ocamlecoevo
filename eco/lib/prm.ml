(* experimental module to see if we can integrate with Prm
 * then we'll make a more general version *)

module U = Util
module F = U.Float
module I = U.Int

module Color =
  struct
    type t = [ `Birth | `Death ]

    let to_int =
      function
      | `Birth ->
          0
      | `Death ->
          1

    let compare pt pt' = (to_int pt) - (to_int pt')

    let equal pt pt' = (compare pt pt' = 0)

    let to_string =
      function
      | `Birth -> "birth"
      | `Death -> "death"

    let of_string =
      function
      | "birth" ->
          Some `Birth
      | "death" ->
          Some `Death
      | _ ->
          None
  end


module Point = Sim.Prm.Point (Color)

module S = Sim.Ctmjp.Prm.Make (Point)
module Prm = S.P

module T =
  struct
    open Pop.Sig

    type _ t = Nonid : nonid t

    type _ group = G : nonid group

    let isid : type a. a group -> a isid =
      function
      | G -> Isnotid Eq
      | _ -> .

    let group_of : type a. a t -> a group =
      function
      | Nonid -> G
      | _ -> .

    let of_group G = Nonid
    let copy x = x
  end


module P = Pop.With_events.Augment (Pop.Nonid.Make (T))


type param = {
  b : float ;
  c : float ;
}


let create ~nindivs =
  let z = P.create ~ngroups:1 in
  P.add_new ~k:nindivs z T.Nonid ;
  z


let birth par = F.Pos.of_float par.b

let death par = F.Pos.of_float par.c

let birth_rate par _ z =
  F.Pos.mul (birth par) (I.Pos.to_float (P.count ~group:T.G z))

let birth_modif pt t z =
  let ind = P.indiv_from_group T.G in
  P.birth_modif ind pt t z


let death_rate par _ z =
  let c = death par in
  let n = P.count ~group:T.G z in
  let nm1 = I.Pos.pred n in
  let fn = I.Pos.to_float n in
  let fnm1 = I.Pos.to_float nm1 in
  F.Pos.Op.(c * fn * fnm1)


let death_modif pt t z =
  let ind = P.I.nonid (T.of_group T.G) in
  P.death_modif ind pt t z


module Cm = Point.Colors

let evm par =
  Cm.empty
  |> Cm.add `Birth (Sim.Ctmjp.Util.combine (birth_rate par) birth_modif)
  |> Cm.add `Death (Sim.Ctmjp.Util.combine (death_rate par) death_modif)


let rngm ?seed () =
  Cm.empty
  |> Cm.add `Birth (U.rng seed)
  |> Cm.add `Death (U.rng seed)


let uvectors n uf = [
  (n, Sim.Prm.{ c = `Birth ; k = 1 ; t = F.zero ; u = uf }) ;
  (n, Sim.Prm.{ c = `Death ; k = 1 ; t = F.zero ; u = uf }) ;
]


let header = [["t" ; "n"]]

let line t z =
  [ string_of_float (F.to_float t) ;
    string_of_int (I.to_int (P.count ~group:T.G z)) ]

let simulate_until ?(k=1) ?seed ?(chan=stdout) ?dt par tf y0 =
  let evm = evm par in
  let rngm = rngm ?seed () in
  (* should scale with pop size *)
  let vectors = uvectors (I.Pos.of_int 10) (F.Pos.of_float 150_000.) in
  let prm = S.P.rand_draw
    ~rngm
    ~time_range:(F.zero, tf)
    ~ntslices:(I.Pos.of_int 20)
    ~vectors
  in
  Prm.iter_slices (fun tsl ->
    Prm.graft_upto tsl `Birth (F.Pos.of_float 200_000.)
  ) prm ;
  Printf.printf "After prm draw : %f\n%!" (Sys.time ()) ;
  let output = Sim.Csv.convert ?dt ~header ~line ~chan in
  let sim = S.simulate_until ~output evm in
  (*
    Sim.Csv.output ~header ~line ?chan ?dt (S.simulate_until evm)
  in
  *)
  for j = 1 to k do
    Printf.eprintf "j=%i\n%!" j ;
    let y0' = P.copy y0 in
    ignore (sim y0' prm)
  done
