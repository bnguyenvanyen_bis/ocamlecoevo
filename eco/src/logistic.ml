module T = Eco.Logistic.Term
open Cmdliner

let () = Term.exit @@ Term.eval (T.simulate_until, T.info)
