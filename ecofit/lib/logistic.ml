(** Inference for the logistic birth-death process *)

open Sig

module Lo = Eco.Logistic


module Param =
  struct
    type t = {
      tf : float ;
      h : float ;
      birth_mean : float ;
      birth_var : float ;
      birth_jump : float ;
      death_mean : float ;
      death_var : float ;
      death_jump : float ;
      comp_mean : float ;
      comp_var : float ;
      comp_jump : float ;
    }

    let default = {
      tf = 1. ;
      h = 0.1 ;
      birth_mean = 1. ;
      birth_var = 0.1 ;
      birth_jump = 0.1 ;
      death_mean = 0. ;
      death_var = 0.1 ;
      death_jump = 0.1 ;
      comp_mean = 0. ;
      comp_var = 0.1 ;
      comp_jump = 0.1 ;
    }


    let fpof = F.Pos.of_float

    let tf hy = fpof hy.tf
    let h hy = fpof hy.h
    let birth_mean hy = hy.birth_mean
    let birth_var hy = fpof hy.birth_var
    let birth_jump hy = fpof hy.birth_jump
    let death_mean hy = hy.death_mean
    let death_var hy = fpof hy.death_var
    let death_jump hy = fpof hy.death_jump
    let comp_mean hy = hy.comp_mean
    let comp_var hy = fpof hy.comp_var
    let comp_jump hy = fpof hy.comp_jump

  end


module Prm_fit = Simfit.Prm.Make (Lo.Prm_color)

module Prior =
  struct
    module D = Fit.Dist

    let birth hy =
      D.Lognormal (Param.birth_mean hy, Param.birth_var hy)

    let death hy =
      D.Lognormal (Param.death_mean hy, Param.death_var hy)

    let comp hy =
      D.Lognormal (Param.comp_mean hy, Param.comp_var hy)

    let prm hy =
      let tf = Param.tf hy in
      let h = Param.h hy in
      let colors = Lo.Simulate.colors in
      let vectors = Lo.Simulate.vectors ~h in
      D.Option (F.neg_infinity, Prm_fit.prior ~h ~tf ~vectors ~colors)
  end


module Propose =
  struct
    let normal_prop sigma =
      Fit.Propose.Dist (fun x -> Fit.Dist.Normal (x, F.sq sigma))

    let birth hy = 
      normal_prop (Param.birth_jump hy)

    let death hy =
      normal_prop (Param.death_jump hy)

    let comp hy =
      normal_prop (Param.comp_jump hy)

    let prm_poisson =
      let nredraws = 1 in
      let base = Prm_fit.propose_poisson_full nredraws in
      let draw rng =
        function
        | None ->
            None
        | Some m ->
            Some (Fit.Propose.draw_from rng m base)
      in
      let log_pd_ratio mo mo' =
        match mo, mo' with
        | None, None ->
            [0.]
        | Some _, None | None, Some _ ->
            [neg_infinity]
        | Some m, Some m' ->
            Fit.Propose.log_dens_proba_ratio base m m'
      in
      Fit.Propose.Base { draw ; log_pd_ratio }
  end


module Likelihood =
  struct
    (* need to fill this in but for now let's just say it's 0. *)
  end


module Mcmc =
  struct
    (* basically for now, to try to debug the prior logp, we want :
     * - a prior on prm,
     * - a non adaptive proposal on prm
     * - a flat likelihood (so no simulations)
     * 
     * Then we try to see if we hit positive logp,
     * with only one time slice, by iterating.
     * If we can get one in not too many steps, then we win.
     * I will also cheat for now about what is param *)


    let columns = Fit.Csv.columns_sample_only

    let extract =
      let inv _ = invalid_arg "unrecognized column" in
      Fit.Csv.extract_sample_only ~extract:inv

    module Prm_out = Simfit.Out.Prm.Make (Lo.Prm_color)

    let convert_prm = Prm_out.convert_prm
      ~to_prm:(fun nu -> nu)
      ~output_points:false
      ~output_grid:true

    let convert ?n_thin path =
      Simfit.Out.convert_csv_prm_traj
        ?n_thin
        ?prm_every:n_thin
        ~traj_every:1
        ~columns
        ~extract
        ~convert_prm
        ~sim_traj:(fun ~chan _ -> ignore chan)
        path


    let prior hy =
      Prior.prm hy

    let propose =
      Propose.prm_poisson

    let likelihood =
      let draw _ =
        failwith "Not_implemented"
      in
      let rng = U.rng (Some 0) in
      let log_dens_proba =
        function
        | None ->
            [0.]
        | Some nu ->
            let lbd = F.Pos.of_float (1. /. 11.) in
            let r_rvl = U.rand_exp ~rng lbd in
            let r_hide = U.rand_exp ~rng lbd in
            let tsl = Lo.Prm_color.t_slice_of nu F.zero in
            Lo.Prm_color.reveal_graft_upto tsl `Birth r_rvl ;
            Lo.Prm_color.hide_downto tsl `Birth r_hide ;
            [0.]
      in Fit.Dist.Base { draw ; log_dens_proba }

    let posterior ?seed ?n_thin ~n_iter ~path hy (* data paro *) =
      let init = Fit.Mcmc.init (prior hy) likelihood in
      let sample = Fit.Mcmc.sample (prior hy) propose likelihood in
      let output = convert ?n_thin path in
      Fit.Mcmc.posterior ~output ?seed ~init ~sample ~n_iter None
  end


module Term :
  sig
    open Cmdliner
    
    val posterior : (int * Lo.Prm_color.t option Fit.Mcmc.return) Term.t
    val info : Term.info
  end =
  struct
    open Cmdliner

    let int_opt ~doc ~docv ~arg =
      Arg.(value
         & opt (some int) None
         & info [arg] ~docv ~doc
      )

    let positive_opt ~doc ~docv ~arg =
      Arg.(value
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )

    (*
    let positive_req ~doc ~docv ~arg =
      Arg.(required
         & opt (some (U.Term.positive ())) None
         & info [arg] ~docv ~doc
      )
    *)


    let seed =
      let doc = "Initialize the PRNG with the seed $(docv)." in
      Arg.(value & opt (some int) None & info ["seed"] ~docv:"SEED" ~doc)

    let tf =
      let doc = "Simulate the system until $(docv)." in
      positive_opt ~doc ~docv:"TF" ~arg:"tf"

    let width =
      let doc =
        "Width of time slices for the estimated prm."
      in positive_opt ~doc ~docv:"WIDTH" ~arg:"width"

    let path =
      let doc = "Output results to files starting by $(docv) as CSV." in
      Arg.(value & pos 0 string "path" & info [] ~docv:"PATH" ~doc)

    let n_iter =
      let doc =
        "Run $(docv) iterations of adaptive MCMC. Equal 0 by default."
      in
      Arg.(value
         & opt int 0
         & info ["niter"] ~docv:"NITER" ~doc
      )

    let n_thin =
      let doc =
        "Output sample every $(docv) iterations.
         By default output at every iteration."
      in int_opt ~doc ~docv:"NTHIN" ~arg:"nthin"

    let fold_with_opt fos x =
      L.fold_left (fun x' ->
        function
        | None ->
            x'
        | Some f ->
            f x'
      ) x fos

    let hyper =
      let opt_map = U.Option.map in
      let set_hyper tf h =
        let f_tf = opt_map (fun x hy -> Param.{ hy with tf = x }) tf in
        let f_h = opt_map (fun x hy -> Param.{ hy with h = x }) h in
        fold_with_opt [
          f_tf ; f_h ;
        ] Param.default
      in
      let pos_to_float = Term.const (opt_map F.to_float) in
      Term.(const set_hyper
          $ (pos_to_float $ tf)
          $ (pos_to_float $ width)
      )

    let posterior =
      let post_flat seed n_thin n_iter path hy =
        Mcmc.posterior ?seed ?n_thin ~n_iter ~path hy
      in Term.(const post_flat
             $ seed
             $ n_thin
             $ n_iter
             $ path
             $ hyper
      )

    let info =
      let doc =
        "Run MCMC for a logistic birth death process inference."
      in Term.info
        "ecofit-logistic"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end
