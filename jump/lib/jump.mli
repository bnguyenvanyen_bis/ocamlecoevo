(** Càdlàg processes used to represent statistics of time-embedded trees,
 *  and of trajectories of Ctmjps in general.
 *
 *  Càdlàg = Continuous on the right and limit on the left,
 *  that is, X(t+) = X(t) but X(t-) <> X(t).
 *
 *  We represent both piecewise-constant and piecewise linear functions. *)

(** ordered times and values associated *)
type 'a cadlag = (float * 'a) list

(** piecewise linear function as (jump time, (starting point, slope)) list *)
type piecelin = (float * float) cadlag

(** {1:basics General purpose construction and destruction} *)

(** constructor *)
val cons : (float * 'a) -> 'a cadlag -> 'a cadlag

(** [hd c] is the first element of [c]. *)
val hd : 'a cadlag -> (float * 'a)

(** [last c] is the last element of [c]. *)
val last : 'a cadlag -> (float * 'a)

(** [rev c] is the reverse of [c] (decreasing times). *)
val rev : 'a cadlag -> 'a cadlag

(** [translate t c] is [c] with times translated by [t],
 *  (new times are old times [+ t]). *)
val translate : float -> 'a cadlag -> 'a cadlag

(** [base (t, x) c] is [c] with [(t, x)] prepended,
    if it comes strictly before [c], otherwise it is [c]. *)
val base : (float * 'a) -> 'a cadlag -> 'a cadlag

(** [rebase (t, x) c] is [c] with [(t, x)] at the beginning,
    unless it comes strictly after the start of [c],
    in which case it is [c].
    The difference with base is on the equal case. *)
val rebase : (float * 'a) -> 'a cadlag -> 'a cadlag

(** [find_right c t] is [c] starting at its last jump before [t]. *)
val find_right : 'a cadlag -> float -> 'a cadlag

(** [cut_left t c] is [c] starting in [t+]. *)
val cut_left : float -> 'a cadlag -> 'a cadlag

(** [cut_at t c] is the couple of the cadlag finishing at [t-]
 *  and the cadlag starting at [t]. *)
val cut_at : float -> 'a cadlag -> 'a cadlag * 'a cadlag

(** [cut_right t c] is [c] finishing in [t-]. *)
val cut_right : float -> 'a cadlag -> 'a cadlag

(** [eval c t] is the value of [c] in [t]. *)
val eval : 'a cadlag -> float -> 'a

(** [piecelin_eval c t] is the value of [c] in [t-]. *)
val piecelin_eval : piecelin -> float -> float

(** [times c] are the jump times of [c]. *)
val times : 'a cadlag -> float list

(** [values c] are the values of [c]. *)
val values : 'a cadlag -> 'a list

(** [time] is {!Stdlib.fst} for [float]s. *)
val time : (float * 'a) -> float

(** [value] is {!Stdlib.snd}. *)
val value : ('a * 'b) -> 'b

(** [output to_str chan c] outputs [c] to chan
 *  with its values converted by [to_str]. *)
val output : ('a -> string) -> out_channel -> 'a cadlag -> unit

(** string representation *)
val sprint_jump_times : 'a cadlag -> string
val sprint_float : float cadlag -> string

(** [cumul_dist_of_sample xl] is the empirical cumulative distribution
    for the sample [xl] (should be positive numbers). *)
val cumul_dist_of_sample : float list -> float cadlag

(** [map f c] is [c] with [f] applied to its values *)
val map : ('a -> 'b) -> 'a cadlag -> 'b cadlag

(** [split c] is like List.split for cadlags. *)
val split : ('a * 'b) cadlag -> 'a cadlag * 'b cadlag

(** [for_all p c] is [true] if [p] is [true] all along [c]. *)
val for_all : ('a -> bool) -> 'a cadlag -> bool

(** [fold_left f c] is [f] folded over values of [c]. *)
val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b cadlag -> 'a

(** [map2 f c1 c2] is the cadlag where [f] has been applied to [c1]
 *  and [c2] at each time point. *)
val map2 : ('a -> 'b -> 'c) -> 'a cadlag -> 'b cadlag -> 'c cadlag

(** [merge f c1 c2] reproduces the behaviour of [Map.S.merge] for cadlags. *)
val merge : (bool -> 'a -> bool -> 'b -> 'c) -> 'a cadlag -> 'b cadlag -> 'c cadlag

(** [prune c] is [c] with subsequent identical values removed. *)
val prune : 'a cadlag -> 'a cadlag

(** [bissect ~tol c p t t'] is the time [t''] and value [x] at which
    [p t'' x] first becomes [true], with a tolerance [tol],
    if [p t xt] is [false] and [p t' xt'] is [true]. *)
val bissect :
  ?tol : float ->
  'a cadlag ->
  (float -> 'a -> bool) ->
  float ->
  float ->
  float * 'a

(** [inf_bissect c p t] is appropriate when the last point of [c] is at infinity.
    It will look for a smaller [t'] for which [p] is [true],
    then [bissect] from there. *)
val inf_bissect :
  'a cadlag ->
  (float -> 'a -> bool) ->
  float ->
  float * 'a

(** [regulate c dt] is [c] evaluated every [dt]
 *  until after its last jump. It should not have an infinity time. *)
val regulate : 'a cadlag -> float -> 'a cadlag

(** [jump_times c] is the list of jump times of [c]. *)
val jump_times : 'a cadlag -> float list

(** [jump_values] [c] is the list of values of [c]. *)
val jump_values : 'a cadlag -> 'a list


(** {1:ints int cadlag functions} *)

(** [float_of_int c] is [c] as a float cadlag. *)
val float_of_int : int cadlag -> float cadlag

(** [add_int c1 c2] is the sum of [c1] and [c2]. *)
val add_int : int cadlag -> int cadlag -> int cadlag

(** {1:floats float cadlag functions} *)

(** [add c1 c2] is the sum of [c1] and [c2]. *)
val add : float cadlag -> float cadlag -> float cadlag

(** [sub c1 c2] is [c1 - c2] pointwise. *)
val sub : float cadlag -> float cadlag -> float cadlag

(** [mul c1 c2] is [c1 * c2] pointwise. *)
val mul : float cadlag -> float cadlag -> float cadlag

(** [div c1 c2] is [c1 / c2] pointwise. *)
val div : float cadlag -> float cadlag -> float cadlag

(** [scale lbd c] is [c] scaled by [lbd]. *)
val scale : float -> float cadlag -> float cadlag

(** [sq c] is [c] squared. *)
val sq : float cadlag -> float cadlag

(** [abs c] is the absolute value of [c]. *)
val abs : float cadlag -> float cadlag

(** [inferior c1 c2] is [true] if [c1] is less than [c2] everywhere. *)
val inferior : float cadlag -> float cadlag -> bool

(** [piecelin_of c] is the piecewise linear representation of a stairs process.
 *  So, every slope coefficient is 0. *)
val piecelin_of : float cadlag -> piecelin

(** [primitive c] is the primitive of [c] as a piecewise linear function *)
val primitive : float cadlag -> piecelin

(** [integr tmax c] is the integral from [0.] to [tmax] of [c]. *)
val integr : float cadlag -> float -> float

(** [l2_norm c tmax] is the L2 norm of [c] stopped at [tmax]. *)
val l2_norm : float cadlag -> float -> float

(** [wmean lbd1 c1 lbd2 c2] is the mean of [c1] and [c2]
 *  weighted respectively by [lbd1] and [lb2]. *)
val wmean : float -> float cadlag -> float -> float cadlag -> float cadlag

(** [cwmean c1 c1' c2 c2'] is the mean of [c1'] and [c2']
 *  weighted respectively by [c1] and [c2], cadlags themselves. *)
val cwmean : float cadlag -> float cadlag -> float cadlag -> float cadlag -> float cadlag

(** [min c] is the minimum value of [c]. *)
val min : 'a cadlag -> 'a

(** [argmin c] is the first time when [c] reaches its minimum. *)
val argmin : 'a cadlag -> float

(** [max c] is the maximum value of [c]. *)
val max : 'a cadlag -> 'a

(** [argmax c] is the first time when [c] reaches its maximum. *)
val argmax : 'a cadlag -> float

val longer_leaves : float -> int cadlag -> int cadlag

(** piecewise linear function difference *)
val p_sub : piecelin -> piecelin -> piecelin

(** L2 norm for piecewise linear functions *)
val p_l2_norm : piecelin -> float -> float

(** [p_regulate pl pred pi] is the time-series of values of [pl]
 *  every [pi] stopped when [pred] becomes [true]. *)
val p_regulate : piecelin -> (float -> float -> bool) -> float -> float cadlag

(** [p_diff pl] is the cadlag where the value at [t_i]
 *  is the value of [pl] at [t_i] minus the value of [pl] at [t_(i-1)] *)
val p_diff : piecelin -> float cadlag

(** [int_diff c] is a cadlag such that,
 *  if [c] has points [(t(i), n(i))],
 *  then [int_diff c] has points [(t(i), n(i) - n(i-1))],
 *  where the first time of [c] is lost. *)
val int_diff : int cadlag -> int cadlag


module Pos :
  sig
    (** [areas ?tf c] is the cadlag of areas under the segments of [c].
     *  if [c] has points [(t(i), x(i))],
     *  then [areas c] has points [(t(i), (t(i) - t(i-1)) * x(i-1))],
     *  where the first time of [c] is lost.
     *  If [tf] is given, then a last point [tf] is present. *)
    val areas :
      ?tf:float ->
      'a Util.anypos cadlag ->
        'b Util.pos cadlag

    (** [primitive c] is {!primitive} [c]
     *  but for [c] a positive cadlag, so that the result is increasing. *)
    val primitive :
      Util.closed_pos cadlag ->
        (Util.closed_pos * Util.closed_pos) cadlag
  end
