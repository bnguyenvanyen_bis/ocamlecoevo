module L = BatList

(** an association list with ordered jump time keys *)
type 'a cadlag = (float * 'a) list

(* piecewise linear *)
type piecelin = (float * float) cadlag

(* FIXME introduce cons operator ? *)
let cons ((t1 : float), x1) c =
  match c with
  | (t2, _) :: _ ->
      assert (t1 < t2) ; (t1, x1) :: c
  | [] ->
      (t1, x1) :: []

let hd = L.hd

let last = L.last

let rev = L.rev

let time (t, _) = t

let value (_, x) = x


let split c =
  L.split @@ (L.map (fun (t, (a, b)) -> ((t, a), (t, b)))) @@ c


let translate t = L.map (fun (s, x) -> (s +. t, x))

let base ((t : float), x) c =
  match c with
  | (t', _) :: _ ->
      if (t < t') then
        (t, x) :: c
      else
        c
  | [] ->
      (t, x) :: c

let rebase ((t : float), x) c =
  match c with
  | (t', _) :: _ when t < t' ->
      (t, x) :: c
  | (t', _) :: tc when t = t' ->
      (t, x) :: tc
  | _ :: _ ->
      c
  | [] ->
      (t, x) :: []

let find_right c t =
  let rec f c =
    match c with
    | [] ->
        []
    | (t1, _) :: _ when t < t1 ->
        c
    | (t1, x1) :: [] when t >= t1 ->
        (t1, x1) :: []
    | (t1, x1) :: (t2, x2) :: tc when (t1 <= t && t < t2) ->
        (t1, x1) :: (t2, x2) :: tc
    | (_, _) :: tc ->  (* t1 < t *)
      f tc
  in f c

let cut_left t' c =
  match find_right c t' with
  | [] ->
      []
  | (_, x) :: tc ->
      (t', x) :: tc

let cut_at t c =
  let rec f acc c =
    match c, acc with
    | [], [] ->
        ([], [])
    | [], (_, x) :: _ ->
        (rev acc, [(t, x)])
    | (t', _) :: _, (_, x) :: _ when t <= t' ->
        (rev acc, (t, x) :: c)
    | (t', _) :: _, [] when t <= t' ->
        ([], c)
    | (t', x') :: tc, _ -> (* t > t' *)
        f ((t', x') :: acc) tc
  in f [] c


let cut_right t c =
  let cutl, _ = cut_at t c in
  cutl


let eval c t =
  let tc = cut_left t c in
  match tc with
  | [] ->
    failwith "Jump.eval : empty jump function"
  | (_, x) :: _ ->
    x

let piecelin_eval c t' =
  (* FIXME don't we have t = t' sadly ? *)
  let tc = find_right c t' in
  match tc with
  | [] ->
    failwith "Jump.piecelin_eval : empty jump function"
  | (t, (y, a)) :: _ ->
    y +. (t' -. t) *. a


let times c =
  let t, _ = L.split c in
  t

let values c =
  let _, v = L.split c in
  v


let output to_str chan c =
  L.iter (fun (t, x) ->
    Printf.fprintf chan "(%.3f, %s) " t (to_str x)
  ) c ;
  Printf.fprintf chan "\n"


let sprint_jump_times (c : 'a cadlag) =
  match c with
  | [] -> ""
  | (t, _) :: tc ->
    let init = Printf.sprintf "%.2f" t in
    let out s (t', _) = Printf.sprintf "%s;%.2f" s t' in
    L.fold_left out init tc

let sprint_float (c : float cadlag) =
  match c with
  | [] -> ""
  | (t, x) :: tc ->
    let init = Printf.sprintf "(%.2f;%.2f)" t x in
    let out s (t', x') = Printf.sprintf "%s;(%.2f,%.2f)" s t' x' in
    L.fold_left out init tc


let cumul_dist_of_sample xl =
  let m = L.length xl in
  let rec f n l =
    match l with
    | [] -> []
    | x :: tl -> (x, (n + 1)) :: (f (n + 1) tl)
  in
  let c = (0., 0) :: (f 0 (L.sort compare xl)) in
  L.map (fun (t, n) -> (t, float_of_int n /. float_of_int m)) c


let map f (c : 'a cadlag) =
  L.map (fun (t, x) -> (t, f x)) c

let for_all f (c : 'a cadlag) =
  L.for_all (fun (_, x) -> f x) c

let fold_left (f : ('a -> 'b -> 'a)) (x0 : 'a) (c : 'b cadlag) =
  L.fold_left (fun x (_, y) -> f x y) x0 c

(* FIXME : make tail recursive *)
let map2 f (c1 : 'a cadlag) (c2 : 'b cadlag) =
  let rec g c3 px1 px2 c1 c2 =
    match c1, c2 with
    | [], [] ->
        c3
    | ((t1, x1) :: tc1), [] ->
        let c3 = (t1, f x1 px2) :: c3 in
        g c3 x1 px2 tc1 c2
    | [], ((t2, x2) :: tc2) ->
        let c3 = (t2, f px1 x2) :: c3 in
        g c3 px1 x2 c1 tc2
    | ((t1, x1) :: tc1), ((t2, x2) :: tc2) when t2 = t1 ->
        let c3 = (t1, f x1 x2) :: c3 in
        g c3 x1 x2 tc1 tc2
    | ((t1, x1) :: tc1), ((t2, _) :: _) when t2 > t1 ->
        let c3 = (t1, f x1 px2) :: c3 in
        g c3 x1 px2 tc1 c2
    | ((_, _) :: _), ((t2, x2) :: tc2) ->  (* t2 < t1 *)
        let c3 = (t2, f px1 x2) :: c3 in
        g c3 px1 x2 c1 tc2
  in
  match c1, c2 with
  | (t1, x1) :: tc1, (t2, x2) :: tc2 when t1 = t2 ->
      rev (g [(t1, f x1 x2)] x1 x2 tc1 tc2)
  | [], [] ->
      []
  | (_ :: _, _ :: _) | ([], _ :: _) | (_ :: _, []) ->
      failwith "Jump.map2 : Should start at the same point"


let merge f c1 c2 =
  let rec g px1 px2 c1 c2 =
    match c1, c2 with
    | [], [] ->
        []
    | ((t1, x1) :: tc1), [] ->
        (t1, f false x1 true px2) :: (g x1 px2 tc1 c2)
    | [], ((t2, x2) :: tc2) ->
        (t2, f true px1 false x2) :: (g px1 x2 c1 tc2)
    | ((t1, x1) :: tc1), ((t2, x2) :: tc2) when t2 = t1 ->
        (t1, f false x1 false x2) :: (g x1 x2 tc1 tc2)
    | ((t1, x1) :: tc1), ((t2, _) :: _) when t2 > t1 ->
        (t1, f false x1 true px2) :: (g x1 px2 tc1 c2)
    | ((_, _) :: _), ((t2, x2) :: tc2) ->  (* t2 < t1 *)
        (t2, f true px1 false x2) :: (g px1 x2 c1 tc2)
  in
  match c1, c2 with
  | (t1, x1) :: tc1, (t2, x2) :: tc2 when t1 = t2 ->
      (t1, f false x1 false x2) :: (g x1 x2 tc1 tc2)
  | [], [] ->
      []
  | (_ :: _, _ :: _) | ([], _ :: _) | (_ :: _, []) ->
      Printf.eprintf "fail :\nc1 : %s\nc2 : %s\n"
      (sprint_jump_times c1)
      (sprint_jump_times c2) ;
      failwith "Jump.merge : Should start at the same point"
 

let rec prune cc =
  match cc with
  | (t, n) :: (_, n') :: tcc when n = n' ->
      prune ((t, n) :: tcc)
  | (t, n) :: tcc ->  (* n <> n' or tcc = [] *)
      (t, n) :: (prune tcc)
  | [] ->
      []


let rec bissect ?(tol=epsilon_float) c p t t' =
  (* we start from knowing that at t' p has become true,
     but that at t < t', it is false.
     We want to find the earliest time at which it becomes true *)
  (* we need a stopping condition on t' - t, if too close should stop *)
  if t' -. t < tol then
    (t, eval c t)
  else
    begin
      (* if not done already, we discard everything before t,
         since we won't need it *)
      let c = cut_left t c in
      let t'' = t +. (t' -. t) /. 2. in
      let x = eval c t'' in
      let b = p t'' x in
      if b then  (* look left *)
        bissect c p t t''
      else  (* look right *)
        bissect c p t'' t'
    end


let inf_bissect c p t =
  (* find last not-infinity jump time *)
  let rc = rev c in
  let rec find_max_non_inf rc =
    match rc with
    | (t, x) :: _ when t < infinity ->
        (t, x)
    | _ :: trc ->  (* infinity *)
        find_max_non_inf trc
    | [] ->
        invalid_arg "empty cadlag"
  in
  let max_t, max_x = find_max_non_inf rc in
  let rec find_valid t =
    if p t max_x then t else find_valid (2. *. t)
  in
  let t' = find_valid max_t in
  bissect c p t t'


let regulate c period =
  (* evaluate c every period until p is true then bissect *)
  (* FIXME infinity in c *)
  let rec f acc c t =
    let nt = t +. period in
    match c with
    | (t', _) :: _ when nt < t' ->
        failwith "oops"
    | (_, x') :: (t'', _) :: _ when nt < t'' ->
        let nacc = (nt, x') :: acc in
        f nacc c nt
    | (_, _) :: (t'', x'') :: tc ->  (* nt > t'' *)
        let nc = (t'', x'') :: tc in
        f acc nc t
    | (_, x') :: [] ->
        let nacc = (nt, x') :: acc in
        (nt, nacc)
    | [] ->
        (* should only be reached on init c=[] already matched *)
        failwith "should not be reached (matched already on init)"
  in match c with
  | [] ->
      []
  | (t, x) :: _ ->
      let _, rreg = f [(t, x)] c t in
      rev rreg
      

let jump_times c = L.map (fun (t, _) -> t) c
let jump_values c = L.map (fun (_, x) -> x) c

(* on int cadlag *)
let float_of_int = map float_of_int
let add_int = map2 (+)
(* on float cadlag *)
let add = map2 (+.)
let sub = map2 (-.)
let mul = map2 ( *.)
let div = map2 (/.)
let scale lbd c = map (fun x -> lbd *. x) c
let sq c = map (fun x -> x ** 2.) c
let abs c = map (fun x -> abs_float x) c
let inferior c1 c2 = for_all (fun x -> x >= 0.) (sub c2 c1)
let piecelin_of c = map (fun x -> (x, 0.)) c

let primitive (c : float cadlag) =
  let rec f (t, (y, a)) =
    function
    | [] ->
        []
    | (t', x') :: tc ->
        let y' = y +. (t' -. t) *. a in
        let tya' = (t', (y', x')) in
        tya' :: f tya' tc
  in match c with
  | [] ->
      []
  | (t0, x0) :: tc ->
      let tya0 = (t0, (0., x0)) in
      tya0 :: (f tya0 tc)


(*
let old_integr c tmax =
  (* the 0. cases should only be reached is c matches them first *)
  let rec f c =
    match c with
    | [] ->
        0.
    | (t, x) :: [] when t < tmax ->
        x *. (tmax -. t)
    | (_, _) :: [] -> (* t >= tmax *)
        0.
    | (t1, x1) :: (t2, x2) :: tc when t2 < tmax ->
        x1 *. (t2 -. t1) +. (f ((t2, x2) :: tc))
    | (t1, x1) :: (_, _) :: _ when t1 < tmax -> (* t1 < tmax <= t2 *)
        x1 *. (tmax -. t1)
    | (_, _) :: (_, _) :: _ -> (* t1 >= tmax *)
        0.
  in f c
*)

(* swap arguments to get the primitive, pretty much ? *)
let integr c tmax =
  let p = primitive c in
  piecelin_eval p tmax


let l2_norm c tmax =
  sqrt (integr (sq c) tmax)

let wmean lbd1 c1 lbd2 c2 =
  scale (1. /. (lbd1 +. lbd2)) (add (scale lbd1 c1) (scale lbd2 c2))

let cwmean c1 c1' c2 c2' =
  let specdiv x y =
    if y = 0. then 
      (assert (x = 0.) ; 0.)
    else
      x /. y
  in map2 specdiv (add (mul c1 c1') (mul c2 c2'))
                  (add c1 c2)

let min c =
  snd (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> compare x1 x2) c))

let argmin c =
  fst (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> compare x1 x2) c))

let max c =
  snd (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> ~- (compare x1 x2)) c))

let argmax c =
  fst (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> ~- (compare x1 x2)) c))


let rec longer_leaves margin (c : int cadlag) =
  match c with
  | (t1, n1) :: (t2, n2) :: tc when n2 = n1 - 1 ->
    (t1, n1) :: longer_leaves margin ((t2 +. margin, n2) :: tc)
  | (t1, n1) :: tc ->
    (t1, n1) :: longer_leaves margin tc
  | [] ->
    []


let p_sub = map2 (fun (x, a) (x', a') -> (x -. x', a -. a'))

let p_l2_norm p tmax =
  (* with fun (t + s) -> (x +. a *. s) ** 2., s >= 0. *)
  let fsq x a s = x ** 2. *. s
               +. x *. a *. s ** 2.
               +. 1. /. 3. *. a ** 2. *. s ** 3. in
  let rec f tot c =
    (* the 0. cases should only be reached is c matches them first *)
    match c with
    | [] ->
        tot
    | (t, (x, a)) :: [] when t < tmax ->
        tot +. fsq x a (tmax -. t)
    | (_, _) :: [] -> (* t >= tmax *)
        tot
    | (t, (x, a)) :: (t', xa') :: tc when t' < tmax ->
        let ntot = tot +. fsq x a (t' -. t) in
        f ntot ((t', xa') :: tc)
    | (t, (x, a)) :: (_, _) :: _ when t < tmax -> (* t < tmax <= t' *)
        tot +. fsq x a (tmax -. t)
    | _ :: _ :: _ -> (* t1 >= tmax *)
      tot
  in sqrt (f 0. p)


let p_regulate (pl : piecelin) pred period =
  let rec f acc c t =
    let nt = t +. period in
    match c with
    | (t', _) :: _ when nt < t' ->
        failwith "oops"
    | (t', (x', a')) :: (t'', _) :: _ when nt < t'' ->
        let y' = x' +. a' *. (nt -. t') in
        let nacc = (nt, y') :: acc in
        if (pred nt y') then
          (nt, nacc)
        else
          f nacc c nt
    | (_, _) :: (t'', xa'') :: tc ->  (* nt > t'' *)
        let nc = (t'', xa'') :: tc in
        f acc nc t
    | (t', (x', a')) :: [] ->
        let y' = x' +. a' *. (nt -. t') in
        let nacc = (nt, y') :: acc in
        if (pred nt y') then
          (nt, nacc)
        else
          f nacc c nt
    | [] ->
        (* should only be reached on init c=[] already matched *)
        failwith "should not be reached (matched already on init)"
  in
  match pl with
  | [] ->
      []
  | (t, (x, _)) :: _ ->
      let _, rreg = f [(t, x)] pl t in
      rev rreg


let p_diff pl =
  let rec f =
    function
    | [] ->
        []
    | _ :: [] ->
        []
    | (_, (x, _)) :: (t', (x', a')) :: tl ->
        let dx = x' -. x in
        (* Printf.eprintf "dx=%f ?> 0.\n%!" dx ; *)
        (t', dx) :: f ((t', (x', a')) :: tl)
  in (0., 0.) :: f pl


let int_diff c =
  let rec f =
    function
    | [] ->
        []
    | _ :: [] ->
        []
    | (_, x) :: (t', x') :: tl ->
        (t', (x' - x)) :: f ((t', x') :: tl)
  in f c


module Pos =
  struct
    module U = Util
    module F = U.Float
    module P = F.Pos

    let areas ?tf c =
      let area x t t' =
        let dt = P.of_float (t' -. t) in
        P.mul dt x
      in
      let rec f =
        function
        | [] ->
            []
        | (t, x) :: [] ->
            begin match tf with
            | None ->
                []
            | Some tf ->
                (tf, area x t tf) :: []
            end
        | (t, x) :: ((t', _) :: _ as tc) ->
            (t', area x t t') :: f tc
      in f c

    let primitive c =
      let rec f (t, (y, a)) =
        function
        | [] ->
            []
        | (t', x') :: tc ->
            let dt = P.of_float (t' -. t) in
            let y' = P.narrow P.Op.(y + dt * a) in
            let tya' = (t', (y', x')) in
            tya' :: f tya' tc
      in
      match c with
      | [] ->
          []
      | (t0, x0) :: tc ->
          let tya0 = (t0, (F.zero, P.narrow x0)) in
          tya0 :: (f tya0 tc)
  end
