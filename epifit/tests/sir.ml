open Epifit
open Sig

module M = Sir.Mcmc ( )


(* test reporting loglik on itself *)
let%expect_test _ =
  let hy = new Sir.hyper in
  let th = (new Episim.theta) in
  let traj = Sir.Fit.S.simulate ~out:`Cadlag ~tech:Sig.Ode hy th in
  let cases = Epi.Data.case_data traj in
  let rate = Epi.Traj.reporting_rate traj in
  let loglik = Observe.reported_cases ~obs:`Poisson hy cases rate in
  Printf.printf "%f" (L.fold_left (+.) 0. loglik) ;
  [%expect{| -21898.319151 |}]


(*
(* test M.run *)
let%expect_test _ =
  let hy = new Sir.hyper in
  let th = (new Episim.theta) in
  let seed = 543 in
  (* create a temp dir for output files ? *)
  let path = in
  let lik = { Default_init.lik with cases = `Poisson ;
                                    alpha = 0.1 ;
  } in
  let recipe = M.Recipe.[ {
    name = "iter" ;
    tech = Sig.Prm ;
    adapt = false ;
    infer = new Sir.Fit.infer ;
    n_iter = 1 ;
    theta_every = Some 1 ;
    traj_every = None ;
    chol_every = None ;
    prm_every = None ;
  } ]
  in
  (* get some data from the input param value *)
  let traj = 
  let data = Data.{
    cases = ;
    coal_true = None ;
    coal_ests = None ;
    neff_ests = None ;
  }
  in
  M.run ~seed ~path ~lik ~recipe hy data th
  (* then read the files and delete them *)
*)
