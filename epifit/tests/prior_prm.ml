open Epifit
open Sig


type hyper = Sir.hyper

let hy = new Sir.hyper


let draw () =
  let rng = Util.rng (Some 0) in
  let prior = Prior.prm [`I_infection] hy in
  Fit.Dist.draw_from rng prior


let%test _ =
  match draw () with
  | Some _ ->
      true
  | None ->
      false


let%test _ =
  match draw () with
  | Some (nu, _) ->
      Epi.Prm_color.colors nu = [`I_infection]
  | None ->
      false


let%expect_test _ =
  match draw () with
  | Some (nu, _) ->
      Printf.printf "%f" (F.to_float (Epi.Prm_color.logp nu)) ;
      [%expect{| -2576.177438 |}]
  | None ->
      assert false
