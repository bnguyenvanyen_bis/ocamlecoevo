
type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_latent
  inherit Param.prior_circular
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.prop_latent
  inherit Param.prop_circular
end


class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end


let prior_specl =
    Specs.prior_base
  @ Specs.prior_latent
  @ Specs.prior_circular


let prop_specl =
    Specs.prop_base
  @ Specs.prop_stochastic
  @ Specs.prop_latent
  @ Specs.prop_circular


let lik_specl = Specs.lik


class infer_pars = object
  inherit Infer.base
  inherit Infer.latent
  inherit Infer.circular
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.latent
  | Tag.circular
  | Tag.stochastic
]


module Fit =
  struct
    module S = Episim.Make (Epi.Seirs.Model)

    type nonrec hyper = hyper

    type nonrec theta = theta

    type infer = infer_pars

    type nonrec tag = tag

    type 'a elist =
      El : (tag, theta, _, 'a) Fit.Param.List.t -> 'a elist

    let model_name = S.model_name

    let par_columns = Sirs.Fit.par_columns @ [
      "sigma_mean" ;
      "sigma_var" ;
      "sigma_jump" ;
    ]

    let par_extract (hy : hyper) s =
      match Sirs.Fit.par_extract (hy :> Sirs.hyper) s with
      | s' ->
          s'
      | exception Failure _ ->
          Seir.Fit.par_extract (hy :> Seir.hyper) s

    let to_hyper_sim hy = (hy :> Param.sim)

    let to_prm th = th#prm

    let load_par hy = S.load_par hy

    let load_theta = S.load_theta

    let hyper_term =
      let f base latent circular lik stochastic =
        (new hyper)
        |> base
        |> latent
        |> circular
        |> lik
        |> stochastic
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.latent ()
        $ Term.Hyper.circular ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ latent @ circular @ stochastic)

    let tag_write = Symbols.Write.any

    let tag_columns = Symbols.Write.columns

    let infer mode =
      function
      | (`Beta | `Nu | `Rho | `Sir
         | `Betavar | `Phase | `Gamma | `Eta
         | `Prm | `Seed) as tag ->
          Sirs.Fit.infer mode tag
      | `Sigma as tag ->
          Infer.infer_latent mode tag

    let fix theta =
      function
      | (`Beta | `Nu | `Rho | `Sir
         | `Betavar | `Phase | `Gamma | `Eta
         | `Prm | `Seed) as tag ->
          Sirs.Fit.fix theta tag
      | `Sigma as tag ->
          Infer.fix_latent theta tag

    let params (infer : infer) (hy : hyper) =
      El Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        betavar hy infer#betavar ;
        phase hy infer#phase ;
        sigma hy infer#sigma ;
        nu hy infer#nu ;
        gamma hy infer#gamma ;
        rho hy infer#rho ;
        eta hy infer#eta ;
        sir hy infer#sir ;
        prm S.E.Prm.color_groups hy infer#prm ;
      ])

    let simulate = S.sim

    let csv_simulate = S.csv_sim

    let extract th s =
      match Sirs.Fit.extract th s with
      | s' ->
          s'
      | exception Failure _ ->
          Seir.Fit.extract th s
  end


module Mcmc ( ) =
  struct
    module M = Mcmc.Make (Fit)
    include M

    let default_hyper = new hyper
    let specl : (string * hyper Lift_arg.spec * string) list =
        prior_specl
      @ prop_specl
      @ lik_specl
      @ (Specs.sim ())
  end
