open Sig


class init_sir = object(self)
  inherit Epi.Param.init_sir_float

  method with_delta_s0_dx dx =
    {< s0_v = s0_v +. dx ;
       r0_v = r0_v -. dx >}

  method with_delta_s0 x =
    let dx = x -. s0_v in
    self#with_delta_s0_dx dx

  method with_delta_i0_dx dx =
    {< s0_v = s0_v -. 1. /. 2. *. dx ;
       i0_v = i0_v +. dx ;
       r0_v = r0_v -. 1. /. 2. *. dx >}

  method with_delta_i0 x =
    let dx = x -. i0_v in
    self#with_delta_i0_dx dx

  method with_delta_r0_dx dx =
    {< s0_v = s0_v -. dx ;
       r0_v = r0_v +. dx >}

  method with_delta_r0 x =
    let dx = x -. r0_v in
    self#with_delta_r0_dx dx
end


class init_seir = object
  inherit Epi.Param.init_e_float
  inherit init_sir

  method seir = (s0_v, e0_v, i0_v, r0_v)
  
  method with_seir (s0, e0, i0, r0) =
    {< s0_v = s0 ; e0_v = e0 ; i0_v = i0 ; r0_v = r0 >}
end


class lik = object
  val tf_v = 1.
  val dt_v = 2e-2
  val coal_var_v = 0.1
  val n_best_of_v = 100
  (* When those are 0. we get nans in the likelihood *)
  val cases_over_disp_v = 1e-6
  val coal_over_disp_v = 1e-6

  method tf = tf_v
  method dt = dt_v
  method coal_var = coal_var_v
  method n_best_of = n_best_of_v
  method cases_over_disp = cases_over_disp_v
  method coal_over_disp = coal_over_disp_v

  method with_tf x = {< tf_v = x >}
  method with_dt x = {< dt_v = x >}
  method with_coal_var x = {< coal_var_v = x >}
  method with_n_best_of x = {< n_best_of_v = x >}
  method with_cases_over_disp x = {< cases_over_disp_v = x >}
  method with_coal_over_disp x = {< coal_over_disp_v = x >}
end


class odeable = object
  val ode_apar_v = Sim.Ode.Lsoda.default

  method ode_apar = ode_apar_v

  method with_ode_apar x = {< ode_apar_v = x >}
end


class sim = object
  inherit lik
  inherit odeable
end


class prior_base = object
  (* is that slow ? (high upper bound) *)
  val seed_range_v = 1_000_000
  val beta_mean_v = 1.85
  val beta_var_v = 0.1
  val nu_mean_v = 1.8
  val nu_var_v = 0.1
  val rho_range_v = 1.
  val n_v = 1e6
  val prevalence_v = 1e-4

  method seed_range = seed_range_v
  method beta_mean = beta_mean_v
  method beta_var = beta_var_v
  method nu_mean = nu_mean_v
  method nu_var = nu_var_v
  method rho_range = rho_range_v
  method n = n_v
  method prevalence = prevalence_v

  method with_seed_range x = {< seed_range_v = x >}
  method with_beta_mean x = {< beta_mean_v = x >}
  method with_beta_var x = {< beta_var_v = x >}
  method with_nu_mean x = {< nu_mean_v = x >}
  method with_nu_var x = {< nu_var_v = x >}
  method with_rho_range x = {< rho_range_v = x >}
  method with_n x = {< n_v = x >}
  method with_prevalence x = {< prevalence_v = x >}
end


class prop_base = object
  val beta_jump_v = 5e-2
  val nu_jump_v = 5e-2
  val rho_jump_v = 5e-3
  val sr_jump_v = 1e-3
  val i_jump_v = 1e-6

  method beta_jump = beta_jump_v
  method nu_jump = nu_jump_v
  method rho_jump = rho_jump_v
  method sr_jump = sr_jump_v
  method i_jump = i_jump_v

  method with_beta_jump x = {< beta_jump_v = x >}
  method with_nu_jump x = {< nu_jump_v = x >}
  method with_rho_jump x = {< rho_jump_v = x >}
  method with_sr_jump x = {< sr_jump_v = x >}
  method with_i_jump x = {< i_jump_v = x >}
end


class prior_latent = object
  val sigma_mean_v = 1.5
  val sigma_var_v = 0.1
  (* val exp_prevalence_v = 1e-4 *)

  method sigma_mean = sigma_mean_v
  method sigma_var = sigma_var_v
  (* method exp_prevalence = exp_prevalence_v *)

  method with_sigma_mean x = {< sigma_mean_v = x >}
  method with_sigma_var x = {< sigma_var_v = x >}
  (* method with_exp_prevalence x = {< exp_prevalence_v = x >} *)
end


class prop_latent = object
  val sigma_jump_v = 5e-2

  method sigma_jump = sigma_jump_v

  method with_sigma_jump x = {< sigma_jump_v = x >}
end


class prior_circular = object
  val gamma_mean_v = ~-. 1.
  val gamma_var_v = 1e-2
  val eta_mean_v = 1.

  method gamma_mean = gamma_mean_v
  method gamma_var = gamma_var_v
  method eta_mean = eta_mean_v

  method with_gamma_mean x = {< gamma_mean_v = x >}
  method with_gamma_var x = {< gamma_var_v = x >}
  method with_eta_mean x = {< eta_mean_v = x >}
end


class prop_circular = object
  val betavar_jump_v = 1e-2
  val phase_jump_v = 1e-2
  val gamma_jump_v = 1e-3
  val eta_jump_v = 1e-3

  method betavar_jump = betavar_jump_v
  method phase_jump = phase_jump_v
  method gamma_jump = gamma_jump_v
  method eta_jump = eta_jump_v

  method with_betavar_jump x = {< betavar_jump_v = x >}
  method with_phase_jump x = {< phase_jump_v = x >}
  method with_gamma_jump x = {< gamma_jump_v = x >}
  method with_eta_jump x = {< eta_jump_v = x >}
end


class prop_stochastic = object
  val prm_nredraws_v = 1
  val prm_ntranges_v = 1
  val prm_jump_v = 0.01

  method prm_nredraws = prm_nredraws_v
  method prm_ntranges = prm_ntranges_v
  method prm_jump = prm_jump_v

  method with_prm_nredraws n = {< prm_nredraws_v = n >}
  method with_prm_ntranges n = {< prm_ntranges_v = n >}
  method with_prm_jump x = {< prm_jump_v = x >}
end


let fpof = F.Pos.of_float
let ipoi = I.Pos.of_int

let tf p = fpof p#tf
let dt p = fpof p#dt
let ode_apar p = p#ode_apar
let h p = p#ode_apar.Sim.Ode.Lsoda.mimic_h

let prm_ntslices p = ipoi p#prm_ntslices
let prm_nuslices p = ipoi p#prm_nuslices

let seed_range p = p#seed_range
let beta_mean p = p#beta_mean
let beta_var p = fpof p#beta_var
let nu_mean p = p#nu_mean
let nu_var p = fpof p#nu_var
let rho_range p = F.Proba.of_float p#rho_range
let n p = fpof p#n
let prevalence p = fpof p#prevalence

let beta_jump p = fpof p#beta_jump
let nu_jump p = fpof p#nu_jump
let rho_jump p = fpof p#rho_jump
let sr_jump p = fpof p#sr_jump
let i_jump p = fpof p#i_jump

let sigma_mean p = p#sigma_mean
let sigma_var p = fpof p#sigma_var
(* let exp_prevalence p = fpof p#exp_prevalence *)

let sigma_jump p = fpof p#sigma_jump

let gamma_mean p = p#gamma_mean
let gamma_var p = fpof p#gamma_var
let eta_mean p = fpof p#eta_mean

let betavar_jump p = fpof p#betavar_jump
let phase_jump p = fpof p#phase_jump
let gamma_jump p = fpof p#gamma_jump
let eta_jump p = fpof p#eta_jump

let prm_nredraws p = ipoi p#prm_nredraws
let prm_ntranges p = ipoi p#prm_ntranges

let prm_jump p = fpof p#prm_jump

let coal_var p = F.Pos.of_float p#coal_var
let cases_over_disp p = F.Pos.of_float p#cases_over_disp
let coal_over_disp p = F.Pos.of_float p#coal_over_disp
