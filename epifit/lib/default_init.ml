let out = None

let chol_every = None

let prm_every = None

let traj_every = None

let data = None

let seed = None

let n_batch = max_int

let n_thin = 1

let n_thin_costly = 100

let n_estim = 1

let n_prior = 10_000

let n_ode = 10_000

let n_iter = 100_000

let stochastic_n_prior = false

let paro = None

let prop = `Custom

let tech = `Ode

let lik = Likelihood.({ cases = `None ; coal = `None ; alpha = None })
