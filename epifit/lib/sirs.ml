
type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_circular
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.prop_circular
end


class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end



let prior_specl =
    Specs.prior_base
  @ Specs.prior_circular


let prop_specl =
    Specs.prop_base
  @ Specs.prop_stochastic
  @ Specs.prop_circular


let lik_specl = Specs.lik


class infer_pars = object
  inherit Infer.base
  inherit Infer.circular
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.circular
  | Tag.stochastic
]


module Fit =
  struct
    module S = Episim.Make (Epi.Sirs.Model)

    type nonrec hyper = hyper

    type nonrec theta = theta

    type infer = infer_pars

    type nonrec tag = tag

    type 'a elist =
      El : (tag, theta, _, 'a) Fit.Param.List.t -> 'a elist

    let model_name = S.model_name

    let par_columns = Sir.Fit.par_columns @ [
      "gamma_mean" ;
      "gamma_var" ;
      "eta_mean" ;
      "betavar_jump" ;
      "phase_jump" ;
      "gamma_jump" ;
      "eta_jump" ;
    ]

    let par_extract (hy : hyper) =
      let sof = string_of_float in
      function
      | "gamma_mean" ->
          sof hy#gamma_mean
      | "gamma_var" ->
          sof hy#gamma_var
      | "eta_mean" ->
          sof hy#eta_mean
      | "betavar_jump" ->
          sof hy#betavar_jump
      | "phase_jump" ->
          sof hy#phase_jump
      | "gamma_jump" ->
          sof hy#gamma_jump
      | "eta_jump" ->
          sof hy#eta_jump
      | s ->
          Sir.Fit.par_extract (hy :> Sir.hyper) s

    let to_hyper_sim hy = (hy :> Param.sim)

    let to_prm th = th#prm

    let load_par hy = S.load_par hy

    let load_theta th = S.load_theta th

    let hyper_term =
      let f base circular lik stochastic =
        (new hyper)
        |> base
        |> circular
        |> lik
        |> stochastic
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.circular ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ circular @ stochastic)

    let tag_write = Symbols.Write.any

    let tag_columns = Symbols.Write.columns

    let infer mode =
      function
      | (`Beta | `Nu | `Rho | `Sir | `Prm | `Seed) as tag ->
          Sir.Fit.infer mode tag
      | (`Betavar | `Phase | `Gamma | `Eta) as tag ->
          Infer.infer_circular mode tag

    let fix theta =
      function
      | (`Beta | `Nu | `Rho | `Sir | `Prm | `Seed) as tag ->
          Sir.Fit.fix theta tag
      | (`Betavar | `Phase | `Gamma | `Eta) as tag ->
          Infer.fix_circular theta tag

    let params infer hy =
      El Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        betavar hy infer#betavar ;
        phase hy infer#phase ;
        nu hy infer#nu ;
        gamma hy infer#gamma ;
        rho hy infer#rho ;
        eta hy infer#eta ;
        sir hy infer#sir ;
        prm S.E.Prm.color_groups hy infer#prm ;
      ])

    let simulate = S.sim

    let csv_simulate = S.csv_sim

    let extract th =
      let sof = string_of_float in
      function
      | "betavar" ->
          sof th#betavar
      | "phase" ->
          sof th#phase
      | "gamma" ->
          sof th#gamma
      | "eta" ->
          sof th#eta
      | s ->
          Sir.Fit.extract th s
  end


module Mcmc ( ) =
  struct
    module M = Mcmc.Make (Fit)
    include M

    let default_hyper = new hyper
    let specl : (string * hyper Lift_arg.spec * string) list =
        prior_specl
      @ prop_specl
      @ lik_specl
      @ (Specs.sim ())
  end
