(** Data to compute the likelihood on.
 *
 *  The data is divided into several independant sources,
 *  each with its own log-likelihood function :
 *
 *  - [cases] corresponds to the periodic count of observed cases
 *    for the epidemic.
 *    The likelihood depends on the trajectory of total cases accumulated
 *    during the study period.
 *
 *  - [sequences] corresponds to the pathogen sequences sampled in the
 *    population, with the time of sampling, and already aligned.
 *
 *  - [trees] corresponds to the (timed) phylogenies reconstructed
 *    from the sequences.
 *
 *  - [coal] corresponds to the inferred pair lineage coalescence rate
 *    trajectory in the population of interest,
 *    inferred for instance by Bayesian skyline from the sequences.
 *
 *  Not all of the sources of data might be required together for inference.
 *  Actually it doesn't make too much sense to use more than one of
 *  [coal], [sequences] and [trees] as they are clearly not independent :
 *  they correspond to different stages of information extraction
 *  from the sequence data.
 *
 *)

open Sig


(* source of data *)
type ('obs, 'sim) source = {
  observed : 'obs ;
  simulated : 'sim ;
  (* also need hypar input ? *)
  loglik : ('obs -> 'sim -> float list) ;
}


type rate_cadlag = U.closed_pos J.cadlag

type cases = (int J.cadlag, rate_cadlag) source

type prevalence = (int J.cadlag, int J.cadlag) source


type coal_obs = {
  (* number of lineages in time *)
  nlineages : int J.cadlag ;
  (* number of samples in time *)
  nsamples : int J.cadlag ;
  (* coalescence times *)
  coal_times : float list ;
}

type coal = (coal_obs, rate_cadlag) source

type seq_sample = U.closed_pos * Seqs.t

type sequences = (seq_sample list, (* Coal.Prm.t * *) rate_cadlag) source

type tree_sample = Tree.L.t

type trees = (tree_sample list * (int * Seqs.t) list, rate_cadlag) source


(* We should be able to simply translate the data so that it starts in 0. ? *)


type t = {
  (* number of cases on each time interval *)
  cases : int J.cadlag option ;
  (* prevalence counted at each time *)
  prevalence : int J.cadlag option ;
  (* true coal data *)
  coal_true : coal_obs option ;
  (* estimated coal data *)
  coal_ests : coal_obs array option ;
  (* estimated neff trajectories *)
  neff_ests : float J.cadlag array option ;
}


let translate_coal t { nlineages ; nsamples ; coal_times } =
  let tr c = J.translate (~-. t) c in
  let nlineages = tr nlineages in
  let nsamples = tr nsamples in
  let coal_times = L.map (fun s -> s -. t) coal_times in
  { nlineages ; nsamples ; coal_times }


let translate { cases ; prevalence ; coal_true ; coal_ests ; neff_ests } =
  (* need to find the origin.
   * look into neff_ests and fail if it's None for now.
   * FIXME don't fail *)
  match neff_ests with
  | None ->
      invalid_arg "Epifit.Data.translate"
  | Some a ->
      let t0 = J.time (J.hd a.(0)) in
      let cases = U.Option.map (J.translate (~-. t0)) cases in
      let prevalence = U.Option.map (J.translate (~-. t0)) prevalence in
      let coal_true = U.Option.map (translate_coal t0) coal_true in
      let coal_ests = U.Option.map (A.map (translate_coal t0)) coal_ests in
      let neff_ests = U.Option.map (A.map (J.translate (~-. t0))) neff_ests in
      { cases ; prevalence ; coal_true ; coal_ests ; neff_ests }
