(** Specs (for Arg) *)

module La = Lift_arg

let prior_base = La.([
  ("-seed-range", Int (fun hy x -> hy#with_seed_range x),
   "Seed range for the PRNG for simulations.") ;
  ("-beta-mean", Float (fun hy x -> hy#with_beta_mean x),
   "Mean of the beta prior.") ;
  ("-beta-var", Float (fun hy x -> hy#with_beta_var x),
   "Variance of the beta prior.") ;
  ("-nu-mean", Float (fun hy x -> hy#with_nu_mean x),
   "Mean of the nu prior.") ;
  ("-nu-var", Float (fun hy x -> hy#with_nu_var x),
   "Variance of the nu prior.") ;
  ("-rho-range", Float (fun hy x -> hy#with_rho_range x),
   "Range (max possible value) of the reporting rate rho.
    Should be inferior to 1. (default 1.)") ;
  ("-size", Float (fun hy x -> hy#with_n x),
   "Fixed size of the host population.") ;
  ("-prevalence", Float (fun hy x -> hy#with_prevalence x),
   "Initial expected proportion of infected.") ;
])

let prop_base = La.([
  ("-beta-jump", Float (fun hy x -> hy#with_beta_jump x),
   "Variance of beta proposals.") ;
  ("-nu-jump", Float (fun hy x -> hy#with_nu_jump x),
   "Variance of nu proposals.") ;
  ("-rho-jump", Float (fun hy x -> hy#with_rho_jump x),
   "Variance of rho proposals.") ;
  ("-sr-jump", Float (fun hy x -> hy#with_sr_jump x),
   "Variance of S0 and R0 proposals relative to the population size.") ;
  ("-i-jump", Float (fun hy x -> hy#with_i_jump x),
   "Variance of I0 proposals relative to the population size.") ;
])

let lik = La.([
  ("-tf", Float (fun hy x -> hy#with_tf x),
   "Final time of the data.") ;
  ("-dt", Float (fun hy x -> hy#with_dt x),
   "Interval between datapoints.") ;
  ("-coal-var", Float (fun hy x -> hy#with_coal_var x),
   "Expected error variance on coalescence rate estimates.") ;
  ("-nbest-of", Int (fun hy x -> hy#with_n_best_of x),
   "Number of trajectories to take the best likelihood from.") ;
  ("-cases-over-disp", Float (fun hy x -> hy#with_cases_over_disp x),
   "Overdispersion of cases.") ;
  ("-coal-over-disp", Float (fun hy x -> hy#with_coal_over_disp x),
   "Overdispersion of coal Neff.") ;
])


let sim () =
  La.map
    (fun hy apar -> hy#with_ode_apar apar)
    (fun hy -> hy#ode_apar)
    Sim.Ode.Lsoda.specl


let prior_latent = La.([
  ("-sigma-mean", Float (fun hy x -> hy#with_sigma_mean x),
   "10-log of the sigma prior mean.") ;
  ("-sigma-var", Float (fun hy x -> hy#with_sigma_var x),
   "10-log-variance of the sigma prior.") ;
])

let prop_latent = [
  ("-sigma-jump", La.Float (fun hy x -> hy#with_sigma_jump x),
   "Variance of sigma proposals.")
]

let prior_circular = La.([
  ("-gamma-mean", Float (fun hy x -> hy#with_gamma_mean x),
   "'mean' of the 10-lognormal gamma prior.") ;
  ("-gamma-var", Float (fun hy x -> hy#with_gamma_var x),
   "'variance' of the 10-lognormal gamma prior.") ;
  ("-eta-mean", Float (fun hy x -> hy#with_eta_mean x),
   "Mean of the exponential eta prior.") ;
])

let prop_circular = La.([
  ("-betavar-jump", Float (fun hy x -> hy#with_betavar_jump x),
   "Scale of standard deviation for betavar jumps.") ;
  ("-phase-jump", Float (fun hy x -> hy#with_phase_jump x),
   "Scale of standard deviation for phase jumps.") ;
  ("-gamma-jump", Float (fun hy x -> hy#with_gamma_jump x),
   "Scale of standard deviation for gamma jumps.") ;
  ("-eta-jump", Float (fun hy x -> hy#with_eta_jump x),
   "Scale of standard deviation for eta jumps.") ;
])

let prop_stochastic = La.([
  ("-prm-nredraws", Int (fun hy n -> hy#with_prm_nredraws n),
   "Number of color slices to redraw on proposals for prm.") ;
  ("-prm-ntranges", Int (fun hy n -> hy#with_prm_ntranges n),
   "Number of time ranges to consider for adaptive proposals for prm.") ;
  ("-prm-jump", Float (fun hy x -> hy#with_prm_jump x),
   "Initial standard deviation for prm jumps.") ;
])


(** non "hyper" specs *)

let out = (
  "-out",
  La.String (fun _ s -> Some s),
  "Mandatory prefix of filenames to output to."
)

let chol_every = (
  "-chol-every",
  La.Int (fun _ n -> Some n),
  "Output the adaptive covariance matrix every 'n' iterations.
   By default, do not output."
)

let prm_every = (
  "-prm-every",
  La.Int (fun _ n -> Some n),
  "Output the PRM realisation every 'n' iterations.
   By default, do not output."
)

let traj_every = (
  "-traj-every",
  La.Int (fun _ n -> Some n),
  "Output the system trajectory every 'n' iterations.
   By default, do not output."
)

let data = (
  "-data",
  La.String (fun _ s -> Some (Cli.load_data s)),
  "Path to CSV containing the data.
   Should have columns 't', 'cases', 'coal'."
)

let seed = (
  "-seed",
  La.Int (fun _ n -> Some n ),
  "Seed of the PRNG for the MCMC run."
)

let nestim = (
  "-nestim",
  La.Int (fun _ n -> n),
  "Number of Neff trajectories to estimate from on each iteration."
)

let nbatch = (
  "-nbatch",
  La.Int (fun _ n -> n),
  "Number of iterations before resetting the covariance estimate."
)

let nthin = (
  "-nthin",
  La.Int (fun _ n -> n),
  "Number of additional iterations BY iteration (to decorrelate)."
)

let nprior = (
  "-nprior",
  La.Int (fun _ n -> n),
  "Number of iterations done proposing with the prior before real MCMC."
)

let node = (
  "-node",
  La.Int (fun _ n -> n),
  "Number of iterations done with the deterministic model,
   before using the stochastic one."
)

let niter = (
  "-niter",
  La.Int (fun _ n -> n),
  "Number of iterations."
)

let stochastic_nprior r = (
  "-stochastic-nprior",
  Arg.Set r,
  "Use the PRM integrator for the 'nprior' iterations,
   and redraw the discrete measure from the PRM prior at every iteration."
)

let from load = (
  "-init-from",
  La.String load,
  "Initialize theta from a CSV file (its last row)."
)

let par_from load = (
  "-par-from",
  La.String load,
  "Initialize the hyper parameters from a CSV file (its only row)."
)

let from_prior symbols prepare_from_prior = (
  "-from-prior",
  La.Symbol (symbols, prepare_from_prior),
  "Draw the initial value of this parameter from the prior.
   Can be called several times.
   Should come after '-from' on the line if it is given."
)

let infer_custom symbols update = (
  "-infer-custom",
  La.Symbol (symbols, update `Custom),
  "Infer this parameter with a custom proposal.
   Can be called several times.
   Should come after '-from' on the line if it is given.
   By default, infer-custom for all non float parameters."
)

let infer_adapt symbols update = (
  "-infer-adapt",
  La.Symbol (symbols, update `Adapt),
  "Infer this parameter with an adaptive proposal.
   Can be called several times.
   Should come after '-from' on the line if it is given.
   By default, infer-adapt for all float parameters."
)

let fix symbols update = (
  "-fix",
  La.Symbol_value (symbols, update),
  "Fix this parameter to the given value.
   '.' can be given to fix to its default value.
   Can be called several times.
   Should come after '-from' on the line if it is given.
   For 'sir', the input format is '(s,i,r)'."
)

let proposal = (
  "-proposal",
  La.Symbol (Cli.proposal_symbols,
             (fun _ -> Cli.tag_prop)),
  "Proposal to use. Default custom proposal."
)

let tech = (
  "-tech",
  La.Symbol (Cli.tech_symbols,
             (fun _ -> Cli.tag_tech)),
  "Simulation technique to use. Default Ode (Dopri5)."
)

let likelihood = (
  "-likelihood",
  La.Symbol (Cli.likelihood_symbols,
             Cli.tag_lik),
  "Likelihood function to use. Default flat.
    Can be specified several times :
      If targeting a new component, will also be taken into account.
      If targeting an already specified component, will update."
)

let lik_scale = (
  "-lik-scale",
  La.Float Cli.change_lik_alpha,
  "Likelihood factor to scale coalescent likelihood by, by default 1."
)
