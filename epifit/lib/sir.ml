open Sig


type theta = Episim.theta


class hyper = object
  inherit Param.prior_base
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.sim
end


class infer_pars = object
  inherit Infer.base
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.stochastic
]


module Fit =
  struct
    module S = Episim.Make (Epi.Sir.Model)

    type nonrec hyper = hyper

    type nonrec theta = theta

    type infer = infer_pars

    type nonrec tag = tag

    type 'a elist =
      El : (tag, theta, _, 'a) Fit.Param.List.t -> 'a elist

    let model_name = S.model_name

    let par_columns = [
      "dt" ;
      "tf" ;
      "h" ;
      "coal_var" ;
      "n_best_of" ;
      "cases_over_disp" ;
      "coal_over_disp" ;
      "prm_nredraws" ;
      "prm_ntranges" ;
      "beta_mean" ;
      "beta_var" ;
      "nu_mean" ;
      "nu_var" ;
      "rho_range" ;
      "size" ;
      "prevalence" ;
      "prm_jump" ;
      "beta_jump" ;
      "nu_jump" ;
      "rho_jump" ;
      "sr_jump" ;
      "i_jump" ;
    ]

    let par_extract (hy : hyper) =
      let sof = string_of_float in
      let soi = string_of_int in
      function
      | "dt" ->
          sof hy#dt
      | "tf" ->
          sof hy#tf
      | "h" ->
          sof (F.to_float (Param.h hy))
      | "coal_var" ->
          sof hy#coal_var
      | "n_best_of" ->
          soi hy#n_best_of
      | "cases_over_disp" ->
          sof hy#cases_over_disp
      | "coal_over_disp" ->
          sof hy#coal_over_disp
      | "prm_nredraws" ->
          soi hy#prm_nredraws
      | "prm_ntranges" ->
          soi hy#prm_ntranges
      | "beta_mean" ->
          sof hy#beta_mean
      | "beta_var" ->
          sof hy#beta_var
      | "nu_mean" ->
          sof hy#nu_mean
      | "nu_var" ->
          sof hy#nu_var
      | "rho_range" ->
          sof hy#rho_range
      | "size" ->
          sof hy#n
      | "prevalence" ->
          sof hy#prevalence
      | "prm_jump" ->
          sof hy#prm_jump
      | "beta_jump" ->
          sof hy#beta_jump
      | "nu_jump" ->
          sof hy#nu_jump
      | "rho_jump" ->
          sof hy#rho_jump
      | "sr_jump" ->
          sof hy#sr_jump
      | "i_jump" ->
          sof hy#i_jump
      | _ ->
          failwith "unrecognized column"

    let to_hyper_sim hy = (hy :> Param.sim)

    let to_prm th = th#prm

    let load_par hy = S.load_par hy

    let load_theta = S.load_theta

    let hyper_term =
      let f base lik stochastic =
        (new hyper)
        |> base
        |> lik
        |> stochastic
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ stochastic)

    let tag_write = Symbols.Write.any

    let tag_columns = Symbols.Write.columns

    let infer mode =
      function
      | (`Beta | `Nu | `Rho | `Sir) as tag ->
          Infer.infer_base mode tag
      | (`Prm | `Seed) as tag ->
          Infer.infer_stochastic mode tag

    let fix theta =
      function
      | (`Beta | `Nu | `Rho | `Sir) as tag ->
          Infer.fix_base theta tag
      | (`Prm | `Seed) as tag ->
          Infer.fix_stochastic theta tag

    let params (infer : infer) (hy : hyper) : 'a elist =
      El Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        nu hy infer#nu ;
        rho hy infer#rho ;
        sir hy infer#sir ;
        prm S.E.Prm.color_groups hy infer#prm ;
      ])

    let simulate = S.sim

    let csv_simulate = S.csv_sim

    let extract th =
      let sof = string_of_float in
      function
      | "beta" ->
          sof th#beta
      | "nu" ->
          sof th#nu
      | "rho" ->
          sof th#rho
      | "s0" ->
          sof th#s0
      | "i0" ->
          sof th#i0
      | "r0" ->
          sof th#r0
      | "prm" ->
          begin match th#prm with
          | None ->
              ""
          | Some (nu, _) ->
              F.to_string (Epi.Prm_color.density nu)
          end
      | _ ->
          failwith "unrecognized column"
  end


module Mcmc ( ) =
  struct
    module M = Mcmc.Make (Fit)
    include M
    let default_hyper = new hyper
    let specl : (string * hyper Lift_arg.spec * string) list =
        Specs.prior_base
      @ Specs.prop_base
      @ Specs.prop_stochastic
      @ Specs.lik
      @ (Specs.sim ())
  end
