(** Getsets *)

let beta =
  (`Beta,
   (fun th -> th#beta),
   (fun th x -> th#with_beta x))

let betavar =
  (`Betavar,
   (fun th -> th#betavar),
   (fun th x -> th#with_betavar x))

let phase =
  (`Phase,
   (fun th -> th#phase),
   (fun th x -> th#with_phase x))

let sigma =
  (`Sigma,
   (fun th -> th#sigma),
   (fun th x -> th#with_sigma x))

let nu =
  (`Nu,
   (fun th -> th#nu),
   (fun th x -> th#with_nu x))

let gamma =
  (`Gamma,
   (fun th -> th#gamma),
   (fun th x -> th#with_gamma x))

let rho =
  (`Rho,
   (fun th -> th#rho),
   (fun th x -> th#with_rho x))

let eta =
  (`Eta,
   (fun th -> th#eta),
   (fun th x -> th#with_eta x))

let sir_s =
  (`Sir,
   (fun th -> th#s0),
   (fun th x -> th#with_delta_s0 x))

let seir_s =
  (`Seir,
   (fun th -> th#s0),
   (fun th x -> th#with_delta_s0 x))

let seir_e =
  (`Seir,
   (fun th -> th#e0),
   (fun th x -> th#with_delta_e0 x))

let sir_i =
  (`Sir,
   (fun th -> th#i0),
   (fun th x -> th#with_delta_i0 x))

let seir_i =
  (`Seir,
   (fun th -> th#i0),
   (fun th x -> th#with_delta_i0 x))
