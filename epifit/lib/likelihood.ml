open Sig


(* might have to think about making this a theta dist for efficiency *)
let as_dist ?scale loglikfs sim hypar data =
  let hdloglikfs = L.map (fun f -> f hypar data) loglikfs in
  let draw _ =
    failwith "Not_implemented"
  in
  let log_dens_proba =
    match hdloglikfs with
    | [] ->
        (fun _ -> [0.])
    | _ ->
        begin match scale with
        | None ->
            (fun theta ->
              let rhotraj = sim hypar theta in
              L.fold_left (fun l f ->
                l @ (f rhotraj)
              ) [] hdloglikfs)
        | Some alphas ->
            (fun theta ->
              let rhotraj = sim hypar theta in
              L.fold_left2 (fun l f alph ->
                l @ (L.map (fun x -> alph *. x) (f rhotraj))
              ) [] hdloglikfs alphas)
        end
  in
  D.Base { draw ; log_dens_proba }


(*
let cases ~(obs : [`Poisson | `Negbin]) sim hy th =
  let loglikf = Loglik.reporting ~obs in
  let f = Jump.map (fun (_, rr) -> rr) in
  let f' (cases, _, _, _) = cases in
  as_dist loglikf f f' sim hy th
*)

(*
let coal_rates ~(obs : [`Poisson | `Negbin | `Normal]) sim hy th =
  let loglikf = Loglik.coal_rates ~obs in
  let f = Jump.map (fun (coa, _) -> coa) in
  let f' (_, data_rates, _, _) = data_rates in
  as_dist loglikf f f' sim hy th
*)

(*
let coal_counts sim hy th =
  let loglikf = Loglik.coal_counts in
  let f = Jump.map (fun (coa, _) -> coa) in
  let f' (_, _, nlineages, coal_times) = (nlineages, coal_times) in
  as_dist loglikf f f' sim hy th
*)

type lik = {
  cases : [`None | `Poisson | `Negbin | `Prevalence] ;
  coal : [`None | `Poisson | `Negbin | `Normal | `Empirical | `Counts] ;
  alpha : float option ;
}

let flat _ _ _ =
  D.Constant_flat ([], [], [], [])


(* This is float *)
let cases_loglik =
  function
  | `None ->
      None
  | (`Poisson | `Negbin | `Prevalence) as obs ->
      Some (fun hypar Data.({ cases ; _ }) ->
        match cases with
        | None ->
            invalid_arg "Epifit.Likelihood.cases_loglik : no cases data"
        | Some c ->
            begin match obs with
            | `Prevalence ->
                let f = Observe.reported_prevalence c in
                (fun (rho, traj) ->
                  let i_traj =
                    traj
                    |> Epi.Traj.infected
                    |> Jump.map I.Pos.of_float
                  in f rho i_traj
                )
            | (`Poisson | `Negbin) as obs ->
                let f = Observe.reported_cases ~obs hypar c in
                (fun (_, traj) ->
                  let rates = Epi.Traj.reporting_rate traj in
                  f rates
                )
            end
      )


(* This is anyfloat *)
let coal_loglik ?seed ?k =
  function
  | `None ->
      None
  (* FIXME could k be a part of the tag ? *)
  | (`Poisson | `Negbin | `Normal) as obs ->
      Some (fun hypar Data.({ neff_ests ; _ }) ->
        match neff_ests with
        | None ->
            invalid_arg "Epifit.Likelihood.coal_loglik : no neff data"
        | Some data_neffs ->
            let lcnm = Observe.coal_neff_many ?seed ?k ~obs hypar data_neffs in
            (fun (_, traj) ->
              let sim_neff = Epi.Traj.expected_neff traj in
              lcnm sim_neff
            )
      )
  | `Empirical ->
      Some (fun hypar Data.({ neff_ests ; _ }) ->
        match neff_ests with
        | None ->
            invalid_arg "Epifit.Likelihood.coal_loglik : no neff data"
        | Some data_neffs ->
            let lcne = Observe.coal_neff_empirical hypar data_neffs in
            (* dumb *)
            (fun (_, traj) ->
              let sim_neff = Epi.Traj.expected_neff traj in
              L.map F.to_float (lcne sim_neff)
            )
      )
  | `Counts ->
      failwith "Not_implemented"
      (* coal_counts *)


let composite ?seed ?k { cases ; coal ; alpha } =
  let casesf = cases_loglik cases in
  let coalf = coal_loglik ?seed ?k coal in
  (* keep only the loglik components that are [Some] *)
  let loglikfs = L.filter_map (fun x -> x) [ casesf ; coalf ] in
  let scale =
    match alpha with
    | None ->
        None
    | Some alph ->
        Some [1. ; alph]
  in
  as_dist ?scale loglikfs
