
type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_latent
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.prop_latent
end

class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end


let prior_specl =
    Specs.prior_base
  @ Specs.prior_latent

let prop_specl =
    Specs.prop_base
  @ Specs.prop_stochastic
  @ Specs.prop_latent

let lik_specl = Specs.lik


class infer_pars = object
  inherit Infer.base
  inherit Infer.latent
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.latent
  | Tag.stochastic
]


module Fit =
  struct
    module S = Episim.Make (Epi.Seir.Model)

    type nonrec hyper = hyper

    type nonrec theta = theta

    type infer = infer_pars

    type nonrec tag = tag

    type 'a elist =
      El : (tag, theta, _, 'a) Fit.Param.List.t -> 'a elist

    let model_name = S.model_name

    let par_columns = Sir.Fit.par_columns @ [
      "sigma_mean" ;
      "sigma_var" ;
      "sigma_jump" ;
    ]

    let par_extract (hy : hyper) =
      let sof = string_of_float in
      function
      | "sigma_mean" ->
          sof hy#sigma_mean
      | "sigma_var" ->
          sof hy#sigma_var
      | "sigma_jump" ->
          sof hy#sigma_jump
      | s ->
          Sir.Fit.par_extract (hy :> Sir.hyper) s

    let to_hyper_sim hy = (hy :> Param.sim)

    let to_prm th = th#prm

    let load_par hy = S.load_par hy

    let load_theta = S.load_theta

    let hyper_term =
      let f base latent lik stochastic =
        (new hyper)
        |> base
        |> latent
        |> lik
        |> stochastic
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.latent ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ latent @ stochastic)

    let tag_write = Symbols.Write.any

    let tag_columns = Symbols.Write.columns

    let infer mode =
      function
      | (`Beta | `Nu | `Rho | `Sir | `Prm | `Seed) as tag ->
          Sir.Fit.infer mode tag
      | `Sigma as tag ->
          Infer.infer_latent mode tag

    let fix theta =
      function
      | (`Beta | `Nu | `Rho | `Sir | `Prm | `Seed) as tag ->
          Sir.Fit.fix theta tag
      | `Sigma as tag ->
          Infer.fix_latent theta tag

    let params infer hy =
      El Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        sigma hy infer#sigma ;
        nu hy infer#nu ;
        rho hy infer#rho ;
        sir hy infer#sir ;
        prm S.E.Prm.color_groups hy infer#prm ;
      ])

    let simulate = S.sim

    let csv_simulate = S.csv_sim

    let extract th =
      let sof = string_of_float in
      function
      | "sigma" ->
          sof th#sigma
      | s ->
          Sir.Fit.extract th s
  end


module Mcmc ( ) =
  struct
    module M = Mcmc.Make (Fit)
    include M

    let default_hyper = new hyper
    let specl : (string * hyper Lift_arg.spec * string) list =
        prior_specl
      @ prop_specl
      @ lik_specl
      @ (Specs.sim ())
  end
