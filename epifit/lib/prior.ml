open Sig

module P = Param


let seed hypar =
  D.Option (F.zero, D.Iuniform (0, P.seed_range hypar))


let beta hypar =
  D.Lognormal (P.beta_mean hypar, P.beta_var hypar)


let betavar _ =
  D.Uniform (0., 1.)


let phase _ =
  D.Uniform (0., 1.)


let sigma hypar =
  D.Lognormal (P.sigma_mean hypar, P.sigma_var hypar)


let nu hypar =
  D.Lognormal (P.nu_mean hypar, P.nu_var hypar)


let gamma hypar =
  D.Lognormal (P.gamma_mean hypar, P.gamma_var hypar)


let rho hypar =
  D.Uniform (0., hypar#rho_range)


let eta hypar =
  let eta_mean = P.eta_mean hypar in
  D.Exponential F.Pos.Op.(F.one / eta_mean)


let sir hypar =
  (* uniform simplex : n exponential variables (of any rate),
  * then renormalize on the sum, then multiply by hypar#n *)
  let draw rng =
    (* should be uniform between s and r, but biased
     * towards smaller values for i
     * (depending on relative expected prevalence) *)
    let se = U.rand_exp ~rng F.one in
    let ie = U.rand_exp ~rng F.Pos.Op.(F.one / (P.prevalence hypar)) in
    let re = U.rand_exp ~rng F.one in
    let ne = F.Pos.Op.(se + ie + re) in
    let a = F.Pos.Op.(P.n hypar / ne) in
    let s, i, r = F.Pos.Op.((se * a, ie * a, re * a)) in
    (F.to_float s, F.to_float i, F.to_float r)
  in
  let prev = P.prevalence hypar in
  (* log (1 + prev) + log prev *)
  let cst = F.add (F.Pos.log prev) (F.Pos.log1p prev) in
  let log_dens_proba (s, i, r) =
    let n = s +. i +. r in
    if (i < 0.)
    || (n -. (F.to_float (P.n hypar)) > 1.)
    || (s < 0.)
    || (r < 0.) then
      [neg_infinity]
    else
      let li = if i = 0. then 0. else log (i /. n) in
      (* can be nan if both log i and log prev are neg_infty ? *)
      let loglik = (F.to_float prev -. 1.) *. li +. F.to_float cst in
      [loglik]
  in D.Base { draw ; log_dens_proba }


let prm colors hypar =
  let tf = P.tf hypar in
  let h = P.h hypar in
  let vectors = Epi.Point.vectors ~h colors in
  (* D.Option (F.neg_infinity, Prmfit.prior ~h ~tf ~vectors ~colors) *)
  D.Option (
    F.Neg.of_float (~-.1000.),
    Prmfit.prior_intensities ~h ~tf ~vectors ~colors
  )
