open Sig

(* FIXME the "= round" assert can fail ! We need to fix this *)
let simplex_round xs =
  let iof = int_of_float in
  let with_indices = L.mapi (fun i x -> (i, x)) in
  let fracts, _floors = L.split @@ (L.map modf) @@ xs in
  let n = L.length xs in
  (* in the (strict) simplex, is k actually necessarily 1 ? *)
  let fk = L.fsum fracts in
  (* Printf.eprintf "(float)k = %f ; (floor)k = %f\n" fk (floor fk) ; *)
  assert (abs_float (floor (fk +. 0.5) -. fk) < 1e-6) ;
  (* We need to round not to truncate (to conserve the mass) *)
  (* FIXME add a function Util.int_in_radius ? *)
  let k = iof (fk +. 0.5) in
  (* Printf.eprintf "k = %i\n" k ; *)
  let sorted_fracts = L.sort (fun (_, x) (_, x') ->
    compare x x'
  ) (with_indices fracts)
  in
  let _dims_to_floor, dims_to_ceil =
    (L.split_at (n - k)) @@ (L.map (fun (i, _) -> i)) @@ sorted_fracts
  in
  let rounds = L.mapi (fun i x ->
    if L.mem i dims_to_ceil then
      iof x + 1
    else
      iof x
  ) xs
  in
  let tot = iof (0.5 +. L.fsum xs) in
  (* Printf.eprintf "total = %i\n" tot ; *)
  (* Printf.eprintf "round total = %i\n" (L.sum rounds) ; *)
  assert (L.sum rounds = tot) ;
  rounds


let sir_to_float (s, i, r) =
  (F.to_float s, F.to_float i, F.to_float r)


let seir_to_float (s, e, i, r) =
  (F.to_float s, F.to_float e, F.to_float i, F.to_float r)


(*
let sir_round ((x1, x2, x3) as x) =
  let split ((a, a'), (b, b'), (c, c')) =
    ((a, b, c), (a', b', c'))
  in
  let sum (a, b, c) = a +. b +. c in
  let fracts, _floors = split (modf x1, modf x2, modf x3) in
  let n = sum x in
  let fk = sum fracts in
  assert (floor fk = fk) ;
  let k = truncate fk in
  (* round up (ceil) the k largest dimensions, round down (floor) the rest *)
  (* I think here, either k is 1 or 2 *)
  let s1, s2, s3 = fracts in
  let iof = int_of_float in
  let n1, n2, n3 =
    if (k = 1) then
      if (s1 >= s2) && (s1 >= s3) then
        (iof x1 + 1, iof x2, iof x3)
      else if (s2 >= s1) && (s2 >= s3) then
        (iof x1, iof x2 + 1, iof x3)
      else if (s3 >= s1) && (s3 >= s2) then
        (iof x1, iof x2, iof x3 + 1)
      else
        failwith "forgot a case"
    else if k = 2 then
      let n1 =
        if (s1 >= s2) || (s1 >= s3) then
          iof x1 + 1
        else
          iof x1
      in
      let n2 =
        if (s2 >= s1) || (s2 >= s3) then
          iof x2 + 1
        else
          iof x2
      in
      let n3 =
        if (s3 >= s1) || (s3 >= s2) then
          iof x3 + 1
        else
          iof x3
      in
      (n1, n2, n3)
    else
      failwith "k should be 1 or 2 here"
  in
  assert (n1 + n2 + n3 = iof n) ;
  (n1, n2, n3)
*)


(* By laziness I use the same function as for seir_round to fix it faster *)
let sir_round x =
  let list_of (x1, x2, x3) = [x1 ; x2 ; x3] in
  let tuple_of =
    function
    | [x1 ; x2 ; x3] ->
        (x1, x2, x3)
    | _ ->
        assert false
  in
  tuple_of @@ simplex_round @@ list_of @@ x


let seir_round x =
  let list_of (x1, x2, x3, x4) = [x1 ; x2 ; x3 ; x4] in
  let tuple_of =
    function
    | [x1 ; x2 ; x3 ; x4] ->
        (x1, x2, x3, x4)
    | _ ->
        assert false
  in
  tuple_of @@ simplex_round @@ list_of @@ x


let fold_dists dist pars =
  List.fold_left (fun d f -> f d) dist pars




(* then we need the distances and likelihoods on trajectories *)
module Distance =
  struct
    (* For now let's just leave it empty. *)

    (* Let's just use the l2 in jump for now,
     * a kind of Skorokhod would be better.
     * Also we need something for point measures. *)
    (* cumulative case numbers *)
    (* look only at common times ? *)
    (* error on unmatched point ? *)

    (* coalescence rates *)

    (* cumulative coalescence numbers ? *)

  end



