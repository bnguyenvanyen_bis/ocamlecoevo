module I = Fit.Infer

type 'a infer = 'a I.t

let custom = I.custom
let adapt = I.adapt
let free = I.free
let fixed = I.fixed


class base = object
  val betav = adapt
  val nuv = adapt
  val rhov = adapt
  val sirv = adapt

  method beta : float infer = betav
  method nu  : float infer = nuv
  method rho : float infer = rhov
  method sir : (float * float * float) infer = sirv

  method with_beta x = {< betav = x >}
  method with_nu x = {< nuv = x >}
  method with_rho x = {< rhov = x >}
  method with_sir x = {< sirv = x >}
end


class latent = object
  val sigmav = adapt
  
  method sigma : float infer = sigmav

  method with_sigma x = {< sigmav = x >}
end


class circular = object
  val betavarv = adapt
  val phasev = adapt
  val gammav = adapt
  val etav = adapt

  method betavar : float infer = betavarv
  method phase : float infer = phasev
  method gamma : float infer = gammav
  method eta : float infer = etav

  method with_betavar x = {< betavarv = x >}
  method with_phase x = {< phasev = x >}
  method with_gamma x = {< gammav = x >}
  method with_eta x = {< etav = x >}
end


class stochastic = object
  val seedv = custom
  val prmv = custom
  
  method seed : int option infer = seedv
  method prm : Prmfit.t option infer = prmv

  method with_seed no = {< seedv = no >}
  method with_prm m = {< prmv = m >}
end


let fix_base theta symb s infer =
  match symb with
  | `Beta ->
      let x =
        match s with
        | "." -> 
            theta#beta
        | s ->
            float_of_string s
      in infer#with_beta (fixed x)
  | `Nu ->
      let x =
        match s with
        | "." ->
            theta#nu
        | s ->
            float_of_string s
      in infer#with_nu (fixed x)
  | `Rho ->
      let x =
        match s with
        | "." ->
            theta#rho
        | s ->
            float_of_string s
      in infer#with_rho (fixed x)
  | `Sir ->
      let x =
        match s with
        | "." ->
            theta#sir
        | s ->
            Scanf.sscanf s "(%f,%f,%f)" (fun s i r -> (s, i, r))
      in infer#with_sir (fixed x)


let fix_latent theta symb s infer =
  match symb with
  | `Sigma ->
      let x =
        match s with
        | "." ->
            theta#sigma
        | s ->
            float_of_string s
      in infer#with_sigma (fixed x)


let fix_circular theta symb s infer =
  match symb with
  | `Betavar ->
      let x =
        match s with
        | "." ->
            theta#betavar
        | s ->
            float_of_string s
      in infer#with_betavar (fixed x)
  | `Phase ->
      let x = match s with
        | "." ->
            theta#phase
        | s ->
            float_of_string s
      in infer#with_phase (fixed x)
  | `Gamma ->
      let x = match s with
        | "." ->
            theta#gamma
        | s ->
            float_of_string s
      in infer#with_gamma (fixed x)
  | `Eta ->
      let x =
        match s with
        | "." ->
            theta#eta
        | s ->
            float_of_string s
      in infer#with_eta (fixed x)


let fix_stochastic theta symb s infer =
  match symb with
  | `Seed ->
      let x =
        match s with
        | "." ->
            theta#seed
        | "NA" ->
            None
        | s ->
            Some (int_of_string s)
      in infer#with_seed (fixed x)
  | `Prm ->
      let x =
        match s with
        | "." ->
            theta#prm
        | _ ->
            raise (Arg.Bad "fix prm")
      in infer#with_prm (fixed x)



let infer_base is_adapt symb infer =
  match symb with
  | `Beta ->
      infer#with_beta (free is_adapt)
  | `Nu ->
      infer#with_nu (free is_adapt)
  | `Rho ->
      infer#with_rho (free is_adapt)
  | `Sir ->
      infer#with_sir (free is_adapt)


let infer_latent is_adapt symb infer =
  match symb with
  | `Sigma ->
      infer#with_sigma (free is_adapt)


let infer_circular is_adapt symb infer =
  match symb with
  | `Betavar ->
      infer#with_betavar (free is_adapt)
  | `Phase ->
      infer#with_phase (free is_adapt)
  | `Gamma ->
      infer#with_gamma (free is_adapt)
  | `Eta ->
      infer#with_eta (free is_adapt)


let infer_stochastic is_adapt symb infer =
  match symb with
  | `Seed ->
      begin match is_adapt with
      | `Custom ->
          infer#with_seed custom
      | _ ->
          invalid_arg "seed free-custom only"
      end
  | `Prm ->
      infer#with_prm (free is_adapt)
