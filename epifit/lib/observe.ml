open Sig

module Pre = U.Precompute ( )


let assert_synchro ?(tol=1e-6) t t' =
  match assert (abs_float (t' -. t) < tol) with
  | () ->
      ()
  | exception (Assert_failure _ as e) ->
      begin
        Printf.eprintf "|%f - %f| > tol\n" t' t ;
        raise e
      end



(* Likelihood for observed case count data.
 * Observations can be Poisson or negative binomial.
 * If [rate], the total case reporting rate cadlag starts with times
 * (t_0, t_1, t_2, ...),
 * then [cases], the case counts, should start with (t_1, t_2, ...).
 *)
let reported_cases ?tol ~(obs : [`Poisson | `Negbin]) hypar cases =
  (* let tf = hypar#tf in *)
  let logp =
    match obs with
    | `Poisson ->
        Pre.logp_poisson
    | `Negbin ->
        let ovd = Param.cases_over_disp hypar in
        Pre.logp_negbin_over ovd
  in
  let rep rate =
    (* integrate over intervals *)
    let pos_binned_rate = J.Pos.areas rate in
    try
      L.map2 (fun (t, lbd) (t', c) ->
        assert_synchro ?tol t t' ;
        (* We want to avoid neg_infinity terms so that we can still continue
         * improving the likelihood.
         * We put a [~-. 1000.] term instead,
         * so the likelihood is false, but only a little.
         *)
        max (~-. 1000.) (F.to_float (logp lbd c))
      ) pos_binned_rate cases
    with Invalid_argument _ as e ->
      begin
        Printf.eprintf
        "binned rate goes from %f to %f\ncases goes from %f to %f\n"
        (pos_binned_rate |> J.hd |> J.time)
        (pos_binned_rate |> J.last |> J.time)
        (cases |> J.hd |> J.time)
        (cases |> J.last |> J.time)
        ;
        Fit.Dist.eprint ~name:"pos_binned_rate" (J.times pos_binned_rate) ;
        raise e
      end
  in rep


let reported_prevalence prev =
  let rep rho i_traj =
    L.map2 (fun (t, i) (t', c) ->
      assert_synchro t t' ;
      max (~-. 1000.) (F.to_float (Pre.logp_binomial i rho c))
    ) i_traj prev
  in rep


(* poisson / negbin / normal error on 1 / coalescence rates *)
let coal_neff ?tol ~(obs : [`Poisson | `Negbin | `Normal]) hypar data_neff =
  let iof = int_of_float in
  let logp =
    match obs with
    | `Poisson ->
        (fun neff neff' -> Pre.logp_poisson neff (iof neff'))
    | `Negbin ->
        let ovd = Param.coal_over_disp hypar in
        (fun neff neff' -> Pre.logp_negbin_over ovd neff (iof neff'))
    | `Normal ->
        let cv = Param.coal_var hypar in
        (fun neff neff' -> U.logd_normal (F.to_float neff) cv neff')
  in
  let coa sim_neff =
    L.map2 (fun (t, neff) (t', neff') ->
      assert_synchro ?tol t t' ;
      (* FIXME same _float remark *)
      F.to_float (logp neff neff')
    ) sim_neff data_neff
  in coa


(* Empirical neff estimates *)
let coal_neff_empirical _ data_neffs =
  let neffs_by_times = A.of_list @@ U.transpose @@ A.to_list @@ data_neffs in
  (* FIXME problem : h should vary. maybe we need some automated way to choose it *)
  let logd = A.map (fun c ->
    c |> J.values |> (U.estimate_log_density ~h:(F.Pos.of_float 100.))
  ) neffs_by_times
  in
  let coa =
    L.mapi (fun i (_, neff) -> logd.(i) neff)
  in coa


(* Can we get rid of rng ? *)
let coal_neff_many ?tol ?seed ?(k=1)
                   ~(obs : [`Poisson | `Negbin | `Normal]) hypar =
  let rng = U.rng seed in
  let iof = int_of_float in
  let fk = float k in
  let logp =
    match obs with
    | `Poisson ->
        (* FIXME this is weird : neff is not a proba ? *)
        (fun neff neff' -> Pre.logp_poisson neff (iof neff'))
    | `Negbin ->
        let ovd = Param.coal_over_disp hypar in
        (fun neff neff' -> Pre.logp_negbin_over ovd neff (iof neff'))
    | `Normal ->
        let cv = Param.coal_var hypar in
        (fun neff neff' -> U.logd_normal (F.to_float neff) cv neff')
  in
  (* make this better and faster *)
  let coa data_neffs sim_neff =
    let n : 'a U.posint = I.of_int_unsafe (A.length data_neffs) in
    let m = L.length sim_neff in
    assert (k <= I.to_int n) ;
    let sample = L.init k (fun _ -> I.to_int (U.rand_int ~rng n)) in
    let logliks = L.map (fun j ->
      L.map2 (fun (t, neff) (t', neff') ->
        assert_synchro ?tol t t' ;
        (* FIXME we could instead keep using _float *)
        F.to_float (logp neff neff')
      ) sim_neff data_neffs.(j)
    ) sample
    in
    (* FIXME again same *)
    let sum_logliks = L.fold_left (L.map2 (+.)) (L.init m (fun _ -> 0.)) logliks in
    L.map (fun x -> x /. fk) sum_logliks
  in coa

  
(* cumulative coalescence numbers ? *)

let succ_map f ts =
  let rec map acc = function
    | t :: t' :: tls ->
        let acc' = (f t t') :: acc in
        map acc' (t' :: tls)
    | _ :: [] | [] ->
        acc
  in map [] ts


let coal_times _ sim_rate (data_nlineages, data_coal_times) =
  let a = data_nlineages in
  let am1 = J.add_int a [(0., ~-1)] in
  let aam1 = J.map2 (fun i i' -> float (i * i')) a am1 in
  let tot_coal_rate = J.mul sim_rate aam1 in
  let coal_mass = J.primitive tot_coal_rate in
  let jpecm = J.piecelin_eval coal_mass in
  let volume_elements = succ_map (fun t t' ->
    jpecm t' -. jpecm t) data_coal_times
  in 
  L.map (U.logd_exp F.one) volume_elements
