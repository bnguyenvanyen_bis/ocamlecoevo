module A = BatArray
module L = BatList

module Lac = Lacaml.D

module U = Util
module F = U.Float
module I = U.Int
module D = Fit.Dist
module J = Jump


type _ tech =
  | Ode : Lac.Vec.t tech
  | Prm : Epi.Process.Unit.P.t tech


module type FITSIM =
  sig
    type hyper

    type theta

    type infer

    type tag

    type sim

    type 'a elist =
      El : (tag, theta, _, 'a) Fit.Param.List.t -> 'a elist

    val model_name : string

    val par_columns : string list

    val par_extract : hyper -> string -> string

    val to_hyper_sim : hyper -> sim

    val to_prm : theta -> Prmfit.t option

    val hyper_term : hyper Cmdliner.Term.t

    val default_theta : theta

    val load_theta : string option -> theta -> theta

    val default_infer : infer

    val infer_tags : tag list

    val tag_write : tag -> string

    val tag_columns : tag -> string list

    val infer : [`Custom | `Adapt | `Prior] -> tag -> infer -> infer

    val fix : theta -> tag -> string -> infer -> infer

    val params :
      infer ->
      hyper ->
        'a elist

    val simulate :
      tech : 'b tech ->
      sim ->
      theta ->
        'a U.proba * Epi.Traj.t

    val csv_simulate :
      tech : 'b tech ->
      chan : out_channel ->
      sim ->
      theta ->
        unit

    val extract :
      theta ->
      string ->
        string
  end
