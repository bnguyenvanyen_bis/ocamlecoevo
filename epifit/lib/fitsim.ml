open Sig

module FP = Fit.Param
module FI = Fit.Infer


let beta hy = FI.maybe_adaptive_float
  ~tag:`Beta
  ~get:(fun th -> th#beta)
  ~set:(fun th x -> th#with_beta x)
  ~prior:(Prior.beta hy)
  ~proposal:(Propose.beta hy)
  ~jump:hy#beta_jump


let betavar hy = FI.maybe_adaptive_float
  ~tag:`Betavar
  ~get:(fun th -> th#betavar)
  ~set:(fun th x -> th#with_betavar x)
  ~prior:(Prior.betavar hy)
  ~proposal:(Propose.betavar hy)
  ~jump:hy#betavar_jump


let phase hy = FI.maybe_adaptive_float
  ~tag:`Phase
  ~get:(fun th -> th#phase)
  ~set:(fun th x -> th#with_phase x)
  ~prior:(Prior.phase hy)
  ~proposal:(Propose.phase hy)
  ~jump:hy#phase_jump


let sigma hy = FI.maybe_adaptive_float
  ~tag:`Sigma
  ~get:(fun th -> th#sigma)
  ~set:(fun th x -> th#with_sigma x)
  ~prior:(Prior.sigma hy)
  ~proposal:(Propose.sigma hy)
  ~jump:hy#sigma_jump


let nu hy = FI.maybe_adaptive_float
  ~tag:`Nu
  ~get:(fun th -> th#nu)
  ~set:(fun th x -> th#with_nu x)
  ~prior:(Prior.nu hy)
  ~proposal:(Propose.nu hy)
  ~jump:hy#nu_jump


let gamma hy = FI.maybe_adaptive_float
  ~tag:`Gamma
  ~get:(fun th -> th#gamma)
  ~set:(fun th x -> th#with_gamma x)
  ~prior:(Prior.gamma hy)
  ~proposal:(Propose.gamma hy)
  ~jump:hy#gamma_jump


let rho hy = FI.maybe_adaptive_float
  ~tag:`Rho
  ~get:(fun th -> th#rho)
  ~set:(fun th x -> th#with_rho x)
  ~prior:(Prior.rho hy)
  ~proposal:(Propose.rho hy)
  ~jump:hy#rho_jump


let eta hy = FI.maybe_adaptive_float
  ~tag:`Eta
  ~get:(fun th -> th#eta)
  ~set:(fun th x -> th#with_eta x)
  ~prior:(Prior.eta hy)
  ~proposal:(Propose.eta hy)
  ~jump:hy#eta_jump


let sir hy = FI.maybe_adaptive
  ~tag:`Sir
  ~get:(fun th -> th#sir)
  ~set:(fun th x -> th#with_sir x)
  ~prior:(Prior.sir hy)
  ~proposal:(Propose.sir hy)
  ~jumps:[
    {
      set=(fun dx th -> th#with_delta_s0_dx dx) ;
      jump=(hy#sr_jump *. hy#n) ;
    } ;
    { 
      set=(fun dx th -> th#with_delta_i0_dx dx) ;
      jump=(hy#i_jump *. hy#n) ;
    } ;
  ]


let prm color_groups hy =
  let colors = L.reduce (@) color_groups in
  let get th = th#prm in
  let set th x = th#with_prm x in
  let prior = Prior.prm colors hy in
  let proposal = Propose.prm hy in
  let tf = Param.tf hy in
  let h = Param.h hy in
  let prm_jumps = Prmfit.jumps
    ~color_groups
    ~ntranges:(Param.prm_ntranges hy)
    ~ntslices:(Epi.Point.ntslices ~h tf)
    hy#prm_jump
  in
  let jumps = L.map (FP.DJ.map
      ~get:(fun th -> U.Option.some (get th))
      ~set:(fun th x -> set th (Some x))
    ) prm_jumps
  in
  let log_pd_ratio = Some (fun th th' ->
    match th#prm, th'#prm with
    | None, None ->
        [0.]
    | Some nu, Some nu' ->
        Prmfit.log_pd_ratio (* ~colors *) nu nu'
    | Some _, None
    | None, Some _ ->
        [neg_infinity]
  )
  in
  FI.maybe_adaptive_random
    ~tag:`Prm
    ~get
    ~set
    ~prior
    ~proposal
    ~jumps
    ~log_pd_ratio


let draw_from_prior tag l =
  let combine :
    type p a.
    (U.rng -> p -> p) option ->
    ('tag, p, a) FP.t ->
      (U.rng -> p -> p) option =
    FP.(
    fun draw fitp ->
    let set_from_prior tag' set prior =
      if tag = tag' then
        Some (fun rng x -> set x (D.draw_from rng prior))
      else
        None
    in
    match draw with
    | Some _ ->
        draw
    | None ->
        begin match fitp with
        | Custom { tag ; set ; prior ; _ } ->
            set_from_prior tag set prior
        | Fixed { tag=tag' ; set ; value ; _ } ->
            if tag = tag' then
              Some (fun _ x -> set x value)
            else
              None
        | Adaptive { tag ; set ; prior ; _ } ->
            set_from_prior tag set prior
        | Adaptive_fragments { tag ; set ; prior ; _ } ->
            set_from_prior tag set prior
        | Adaptive_frag_rand { tag ; set ; prior ; _ } ->
            set_from_prior tag set prior
        end
  )
  in
  match FP.List.fold_left { f = combine } None l with
  | None ->
      (* if tag is an open variant type this can not happen (?) *)
      raise Not_found
  | Some draw ->
      draw

