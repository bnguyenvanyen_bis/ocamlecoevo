open Sig

module P = Param


let prop_of d = Fit.Propose.Dist d


let normal_prop sigma =
  prop_of (fun x -> D.Normal (x, F.sq sigma))


let beta hypar =
  normal_prop (P.beta_jump hypar)


let betavar hypar =
  normal_prop (P.betavar_jump hypar)


let phase hypar =
  normal_prop (P.phase_jump hypar)


let sigma hypar =
  normal_prop (P.sigma_jump hypar)


let nu hypar =
  normal_prop (P.nu_jump hypar)


let gamma hypar =
  normal_prop (P.gamma_jump hypar)


let rho hypar =
  normal_prop (P.rho_jump hypar)


let eta hypar =
  normal_prop (P.eta_jump hypar)


(* FIXME that's pretty bad no ? *)
let sir hypar =
  let vi = F.sq F.Op.(P.i_jump hypar * P.n hypar) in
  let vsr = F.sq F.Op.(P.sr_jump hypar * P.n hypar) in
  let try_draw rng (s, i, r) =
    let dsr = U.rand_normal ~rng 0. vsr in
    let di = U.rand_normal ~rng 0. vi in
    (s +. dsr -. di /. 2., i +. di, r -. dsr -. di /. 2.)
  in
  let valid (x1, x2, x3) =
    (x1 > 0.) && (x2 > 0.) && (x3 > 0.)
  in
  let draw rng x =
    let rec f rng =
      let x' = try_draw rng x in
      if valid x' then x' else f rng
    in f rng
  in
  let log_pd_ratio _ _ =
    [0.]
  in 
  Fit.Propose.Base { draw ; log_pd_ratio }


(*
let prm_poisson_partial hy =
  let nredraws = I.to_int (Param.prm_nredraws hy) in
  Fit.Propose.Option (Prmfit.propose_poisson nredraws)
*)


let prm_poisson hy =
  let nredraws = I.to_int (Param.prm_nredraws hy) in
  Fit.Propose.Option (Prmfit.propose_poisson_full nredraws)


let prm hy =
  let n = I.to_int (Param.prm_nredraws hy) in
  Fit.Propose.Option (Prmfit.propose_poisson_intensities n)
