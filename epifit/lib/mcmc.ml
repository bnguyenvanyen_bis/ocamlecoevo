open Sig


module Make (S : FITSIM with type sim := Param.sim) =
  struct
    (* does it make sense to mix custom and adaptive, however ?
     * Or when it's adaptive should it be all adaptive (and fixed) ?
     * Does the algorithm still work otherwise ?
     *)

    type 'a recipe_step = {
      name : string ;
      tech : 'a tech ;
      adapt : bool ;
      infer : S.infer ;
      (* not enough to allow the flexibility we would like *)
      (* prior_as_proposal : bool ; *)
      n_iter : int ;
      theta_every : int option ;
      traj_every : int option ;
      chol_every : int option ;
      prm_every : int option ;
    }

    module Recipe =
      struct
        type (_, _) t = 
          | [] :
              ('v, 'v) t
          | (::) :
              'a recipe_step * ('ty, 'v) t ->
                ('a * 'ty, 'v) t

        type 'b poly =
          { f : 'a. 'b -> 'a recipe_step -> 'b } 

        let rec fold_left :
          type a v. 'b poly -> 'b -> (a, v) t -> 'b =
          fun { f } y ->
          function
          | [] ->
              y
          | x :: tl ->
              fold_left { f } (f y x) tl

        let to_list rcp =
          let l = fold_left { f = (fun (type a) l (rcp : a recipe_step) ->
              let tech =
                match rcp.tech with
                | Prm ->
                    "prm"
                | Ode ->
                    "ode"
              in
              L.cons
                (rcp.name, tech, rcp.n_iter, rcp.theta_every,
                 rcp.traj_every, rcp.chol_every, rcp.prm_every)
                l
            ) } ([] : 'b list) rcp
          in L.rev l
      end

    (* Or one "par" file, and one "recipe" file ? *)

    let par_columns =
        S.par_columns
      @ [ "seed" ]

    let recipe_columns = [
      "name" ;
      "tech" ;
      "niter" ;
      "theta_every" ;
      "traj_every" ;
      "chol_every" ;
      "prm_every" ;
      (* also stuff for infer ? *)
    ]

    let recipe_extract (name, tech, n_iter, theta_every, traj_every, chol_every, prm_every) =
      let soi = string_of_int in
      let soio =
        function
        | None ->
            ""
        | Some n ->
            soi n
      in
      function
      | "name" ->
          name
      | "tech" ->
          tech
      | "niter" ->
          soi n_iter
      | "theta_every" ->
          soio theta_every
      | "traj_every" ->
          soio traj_every
      | "chol_every" ->
          soio chol_every
      | "prm_every" ->
          soio prm_every   
      | _ ->
          invalid_arg "unrecognized column"

    let par_extract (seed, hy) =
      let soi = string_of_int in
      let soio =
        function
        | None ->
            ""
        | Some n ->
            soi n
      in
      function
      | "seed" ->
          soio seed
      | s ->
          S.par_extract hy s
 
    let output_par ?seed ~path ~recipe hypar =
      Util.Csv.output
        ~columns:par_columns
        ~extract:par_extract
        ~path:(Printf.sprintf "%s.par.csv" path)
        [(seed, hypar)] ;
      Util.Csv.output
        ~columns:recipe_columns
        ~extract:recipe_extract
        ~path:(Printf.sprintf "%s.recipe.csv" path)
        (Recipe.to_list recipe)

    let columns pars =
      let tags = Fit.Param.infer_tags pars in
      let th_cols = L.fold_left (fun cs tag ->
          cs @ (S.tag_columns tag)
        ) [] tags
      in
      let move_cols = L.fold_left (fun cs tag ->
          cs @ (L.map (fun s -> "move_" ^ s) (S.tag_columns tag))
        ) [] tags
      in
      Fit.Csv.columns_sample_move @ th_cols @ move_cols
 
    let extract =
      Fit.Csv.extract_sample_move ~extract:S.extract
  
    let update_par ?seed draw tags par =
      let rng = U.rand_rng (U.rng seed) in
      let update p tag =
        draw tag rng p
      in
      List.fold_left update par tags

    let update_paro ?seed draw tags =
      function
      | None ->
          None
      | Some par ->
          Some (update_par ?seed draw tags par)

    let chol_get pars mat =
      let tags = A.of_list (Fit.Param.adapt_tags pars) in
      fun tag tag' ->
      match A.findi (fun t -> t = tag) tags,
            A.findi (fun t -> t = tag') tags
      with
      | (i, j) ->
          Some mat.{i+1, j+1}
      | exception Not_found ->
          None

    let new_chol pars' =
      (* ' is new *)
      let mat' =
        pars'
        |> Fit.Param.jumps
        |> A.of_list
        |> Lac.Vec.of_array
        |> Lac.Mat.of_diag
      in
      function
      | None ->
          mat'
      | Some (S.El pars, mat) ->
          let get = chol_get pars mat in
          let tags' = Fit.Param.adapt_tags pars' in
          L.iteri (fun i tagi ->
            L.iteri (fun j tagj ->
              match get tagi tagj with
              | None ->
                  ()
              | Some x ->
                  mat'.{i+1, j+1} <- x
            ) tags'
          ) tags' ;
          mat'

    let run ?seed ~path ~lik ~recipe hypar data theta =
      let hypsim = S.to_hyper_sim hypar in
      let likelihood tech = Likelihood.composite
        ?seed
        (* ?k:n_estim *)
        lik
        (S.simulate ~tech)
        hypsim
        data 
      in
      let sim_traj ~chan tech th =
        match S.csv_simulate ~tech (S.to_hyper_sim hypar) ~chan th with
        | () ->
            ()
        | exception Fit.Sig.Simulation_error _ ->
            ()
      in
      let leg
        (type a)
        (theta, old_o)
        ({ tech : a tech ; adapt ; infer ; traj_every ; chol_every ; prm_every ; _ }
         as step) =
        Printf.printf "MCMC step %s\n" step.name ;
        match S.params infer hypar with S.El pars ->
        let columns = columns pars in
        let path = Printf.sprintf "%s.%s" path step.name in
        let theta = Fit.Param.fix_par pars theta in
        let prior = Fit.Param.prior theta pars in
        let proposal = Fit.Param.custom_proposal pars in
        let likelihood = likelihood tech in
        let draws = A.of_list (Fit.Param.draws pars) in
        let log_pd_ratio = Fit.Param.log_pd_ratio pars in
        let chol = new_chol pars old_o in
        let sim_traj = sim_traj tech in
        let output =
          match tech with
          | Ode ->
              Simfit.Out.convert_csv_traj_ram
              ?n_thin:step.theta_every
              ?traj_every
              ?chol_every
              ~columns
              ~extract
              ~sim_traj
              path
          | Prm ->
              let module Out_prm = Simfit.Out.Prm.Make (Epi.Prm_color) in
              let convert_prm = Out_prm.convert_prm
                ~to_prm:(fun th -> U.Option.map (fun (nu, _) -> nu) (S.to_prm th))
                ~output_grid:true
                ~output_points:false
              in
              Simfit.Out.convert_csv_traj_ram_prm
              ?n_thin:step.theta_every
              ?traj_every
              ?chol_every
              ?prm_every
              ~columns
              ~extract
              ~convert_prm
              ~sim_traj
              path
        in
        let _, ret, chol =
          Fit.Ram.posterior
            ~output
            ~prior
            ~proposal
            ~likelihood
            ~draws
            ?log_pd_ratio
            ~adapt
            ~chol
            (* ~k0 *)
            ?seed
            ~n_iter:step.n_iter
            theta
        in (Fit.Mcmc.return_to_sample ret, Some (S.El pars, chol))
      in
      output_par ?seed ~path ~recipe hypar ;
      let th, _ = Recipe.fold_left { f = leg } (theta, None) recipe in
      th


    module Term =
      struct
        open Cmdliner
      
        module Di = Default_init

        type etech = Et : _ tech -> etech

        type erecipe = Er : (_, _) Recipe.t -> erecipe

        let seed =
          let doc = "Seed the PRNG with $(docv)." in
          Arg.(value
             & opt (some int) None
             & info ["seed"] ~docv:"SEED" ~doc
          )

        let path =
          let doc =
            "Output results to paths starting by $(docv)."
          in
          Arg.(required
             & pos 0 (some string) None
             & info [] ~docv:"PATH" ~doc
          )

        let likelihood_components =
          let doc =
            "Add the component $(docv) to the likelihood function."
          in
          let tag =
            Arg.enum (L.map (fun s -> (s, s)) Cli.likelihood_symbols)
          in
          let arg =
            Arg.(value
               & opt_all tag []
               & info ["likelihood"] ~docv:"LIKELIHOOD" ~doc
            )
          in
          let f lik_comps lik =
            L.fold_left Cli.tag_lik lik lik_comps
          in
          Term.(const f
              $ arg
          )

        let likelihood_scale =
          let doc =
            "Scale the coalescent likelihood by $(docv)."
          in
          let arg =
            Arg.(value
               & opt (some float) None
               & info ["lik-scale"] ~docv:"LIK-SCALE" ~doc
            )
          in
          let f alpha lik =
            Likelihood.{ lik with alpha }
          in
          Term.(const f
              $ arg
          )

        let likelihood =
          let f lik_comps lik_scale =
            Di.lik
            |> lik_comps
            |> lik_scale
          in
          Term.(const f
              $ likelihood_components
              $ likelihood_scale
          )

        let theta_every =
          let doc =
            "Output the estimated parameter values every $(docv) iterations."
          in
          Arg.(value
             & opt (some int) None
             & info ["theta-every"] ~docv:"THETA-EVERY" ~doc
          )

        let traj_every =
          let doc =
              "Output the estimated trajectory every $(docv) iterations. "
            ^ "By default do not output."
          in
          Arg.(value
             & opt (some int) None
             & info ["traj-every"] ~docv:"TRAJ-EVERY" ~doc
          )

        let chol_every =
          let doc =
              "Output the estimated covariance matrix Cholesky decomposition "
            ^ "every $(docv) iterations. By default do not output."
          in
          Arg.(value
             & opt (some int) None
             & info ["chol-every"] ~docv:"CHOL-EVERY" ~doc
          )

        let prm_every =
          let doc =
              "Output the estimated Poisson random measure "
            ^ "every $(docv) iterations. By default do not output."
          in
          Arg.(value
             & opt (some int) None
             & info ["prm-every"] ~docv:"PRM-EVERY" ~doc
          )

        let step =
          let doc =
            "Add a step to the recipe, specified by 'name,tech,adapt,n_iter'."
          in
          let tech = Arg.enum [("ode", Et Ode) ; ("prm", Et Prm)] in
          let arg =
            Arg.(value
               & opt_all (t4 string tech bool int) []
              & info ["step"] ~docv:"STEP" ~doc
            )
          in
          Term.(const L.rev $ arg)

        module Infer =
          struct
            let infer_tags =
              S.infer_tags
              |> L.map (fun tag -> (S.tag_write tag, tag))
              |> Arg.enum

            let infer_custom_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use its custom proposal."
              in
              Arg.(value
                 & opt_all infer_tags []
                 & info ["infer-custom"] ~docv:"INFER-CUSTOM" ~doc
              )

            let infer_adapt_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use the adaptive proposal."
              in
              Arg.(value
                 & opt_all infer_tags []
                 & info ["infer-adapt"] ~docv:"INFER-ADAPT" ~doc
              )
            
            let infer_prior_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use its prior as proposal."
              in
              Arg.(value
                 & opt_all infer_tags []
                 & info ["infer-prior"] ~docv:"INFER-PRIOR" ~doc
              )

            let fix_tags =
              let doc =
                  "Fix the specified parameter to the given value "
                ^ "for all steps."
              in
              Arg.(value
                 & opt_all (pair infer_tags string) []
                 & info ["fix"] ~docv:"FIX" ~doc
              )
            
            let infer_custom_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use its custom proposal."
              in
              Arg.(value
                   & opt_all (pair string infer_tags) []
                   & info ["infer-custom-step"] ~docv:"INFER-CUSTOM-STEP" ~doc
              )

            let infer_adapt_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use the adaptive proposal."
              in
              Arg.(value
                 & opt_all (pair string infer_tags) []
                 & info ["infer-adapt-step"] ~docv:"INFER-ADAPT-STEP" ~doc
              )

            let infer_prior_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use its prior as proposal."
              in
              Arg.(value
                 & opt_all (pair string infer_tags) []
                 & info ["infer-prior-step"] ~docv:"INFER-PRIOR-STEP" ~doc
              )

            let fix_step_tags =
              let doc =
                  "Fix the specified parameter for the specified step "
                ^ "to the given value."
              in
              Arg.(value
                 & opt_all (t3 string infer_tags string) []
                 & info ["fix-step"] ~docv:"FIX-STEP" ~doc
              )

            let infer_prior_step =
              let doc =
                  "For the specified step, use the prior as proposal for all "
                ^ "parameters being inferred."
              in
              Arg.(value
                 & opt_all string []
                 & info ["infer-prior-step-all"] ~docv:"INFER-PRIOR-STEP-ALL" ~doc
              )
          end

        let infer_step =
          let f fix_tags fix_step_tags
                custom_tags custom_step_tags
                adapt_tags adapt_step_tags
                prior_tags prior_step_tags
                prior_names
                name theta =
            let valid (name', tag) =
              if name = name' then
                Some tag
              else
                None
            in
            let valid_fix (name', tag, s) =
              if name = name' then
                Some (tag, s)
              else
                None
            in
            (* tags for this step *)
            let fix = L.filter_map valid_fix fix_step_tags in
            let custom = L.filter_map valid custom_step_tags in
            let adapt = L.filter_map valid adapt_step_tags in
            let prior = L.filter_map valid prior_step_tags in
            S.default_infer
            (* global settings *)
            |> L.fold_right (fun (tag, s) -> S.fix theta tag s) fix_tags
            |> L.fold_right (S.infer `Custom) custom_tags
            |> L.fold_right (S.infer `Adapt) adapt_tags
            |> L.fold_right (S.infer `Prior) prior_tags
            (* globally switch all params in custom+adapt tags to `Prior *)
            |> (if L.mem name prior_names then begin
                  L.iter (fun tag ->
                    Printf.printf "%s switching %s to prior\n"
                    name (S.tag_write tag)
                  ) (custom_tags @ adapt_tags) ;
                  L.fold_right (S.infer `Prior) (custom_tags @ adapt_tags)
                end else
                  fun x -> x
               )
            (* step specific settings *)
            |> L.fold_right (fun (tag, s) -> S.fix theta tag s) fix
            |> L.fold_right (S.infer `Custom) custom
            |> L.fold_right (S.infer `Adapt) adapt
            |> L.fold_right (S.infer `Prior) prior
          in
          Infer.(
          Term.(const f
              $ fix_tags
              $ fix_step_tags
              $ infer_custom_tags
              $ infer_custom_step_tags
              $ infer_adapt_tags
              $ infer_adapt_step_tags
              $ infer_prior_tags
              $ infer_prior_step_tags
              $ infer_prior_step
          )
          )

        let erecipe =
          let f theta_every traj_every chol_every prm_every
                steps infer_steps theta =
            L.fold_left (function Er recipe ->
              function (name, Et tech, adapt, n_iter) ->
                let infer = infer_steps name theta in
                Er Recipe.({
                  name ;
                  tech ;
                  adapt ;
                  infer ;
                  n_iter ;
                  theta_every ;
                  traj_every ;
                  chol_every ;
                  prm_every ;
                } :: recipe)
            ) (Er Recipe.[]) steps
          in
          Term.(const f
              $ theta_every
              $ traj_every
              $ chol_every
              $ prm_every
              $ step
              $ infer_step
          )


        let data =
          let doc =
              "Path prefix to input dataset. "
            ^ "The CSV case data file is loaded from '$(docv).data.csv', "
            ^ "and should have columns 't', 'cases' and 'coal'. "
            ^ "The CSV lineage data file is loaded from '$(docv).nlclft.csv', "
            ^ "and should have columns 't', 'nleaves', 'ncoals', 'nlineages'."
          in
          let arg =
            Arg.(required
             & opt (some string) None
             & info ["data"] ~docv:"DATA" ~doc
            )
          in
          Term.(const Cli.load_data $ arg)

        let par_from load =
          let doc =
            "Path to (hyper) parameter values."
          in
          let arg = 
            Arg.(value
               & opt (some string) None
               & info ["par-from"] ~docv:"PAR-FROM" ~doc
            )
          in
          Term.(const load $ arg)

        let theta =
          let doc =
            "Path to starting theta values."
          in
          let arg =
            Arg.(value
               & opt (some string) None
               & info ["init-from"] ~docv:"INIT-FROM" ~doc
            )
          in
          Term.(const S.load_theta $ arg $ const S.default_theta)

        (* for now no infer (the hardest part) *)
        let run =
          let flat seed path lik erecipe hyper data theta =
            match erecipe theta with Er recipe ->
              run ?seed ~path ~lik ~recipe hyper data theta
          in
          Term.(const flat
              $ seed
              $ path
              $ likelihood
              $ erecipe
              $ S.hyper_term
              $ data
              $ theta
          )

        let info =
          let doc = "Estimate the parameters and the events of a "
                  ^ S.model_name ^ " model with Robust Adaptive Metropolis "
                  ^ " MCMC runs."
          in
          Cmdliner.Term.info
            ("epifit-" ^ S.model_name)
            ~version:"%%VERSION%%"
            ~doc
            ~exits:Cmdliner.Term.default_exits
            ~man:[]

      end
  end
