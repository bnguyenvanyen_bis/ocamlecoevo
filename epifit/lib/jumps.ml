(** Jumps *)

let beta hy = (`Beta, hy#beta_jump)

let betavar hy = (`Betavar, hy#betavar_jump)

let phase hy = (`Phase, hy#phase_jump)

let sigma hy = (`Sigma, hy#sigma_jump)

let nu hy = (`Nu, hy#nu_jump)

let gamma hy = (`Gamma, hy#gamma_jump)

let rho hy = (`Rho, hy#rho_jump)

let eta hy = (`Eta, hy#eta_jump)
 
let sir_sr hy = (`Sir, hy#sr_jump *. hy#n)
let seir_sr hy = (`Seir, hy#sr_jump *. hy#n)
let sir_i hy = (`Sir, hy#sr_jump *. hy#n)
let seir_i hy = (`Seir, hy#i_jump *. hy#n)
