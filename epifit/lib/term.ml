open Cmdliner


module Hyper =
  struct
    let arg ~cv ~doc ~arg ~docv ~(f : 'b -> 'a -> 'a) =
      let arg =
        Arg.(value
           & opt (some cv) None
           & info [arg] ~docv ~doc
        )
      in
      let g =
        function
        | None ->
            (fun x -> x)
        | Some y ->
            f y
      in
      Term.(const g $ arg)

    let float_arg ~f = arg ~cv:Arg.float ~f

    let int_arg ~f = arg ~cv:Arg.int ~f

    (* prior_base *)

    let beta_mean () =
      let doc =
        "Mean of the beta prior."
      in
      let f x hy = hy#with_beta_mean x in
      float_arg ~doc ~f ~arg:"beta-mean" ~docv:"BETA-MEAN"

    let beta_var () =
      let doc =
        "Variance of the beta prior."
      in
      let f x hy = hy#with_beta_var x in
      float_arg ~doc ~f ~arg:"beta-var" ~docv:"BETA-VAR"

    let nu_mean () =
      let doc =
        "Mean of the nu prior."
      in
      let f x hy = hy#with_nu_mean x in
      float_arg ~doc ~f ~arg:"nu-mean" ~docv:"NU-MEAN"

    let nu_var () =
      let doc =
        "Variance of the nu prior."
      in
      let f x hy = hy#with_nu_var x in
      float_arg ~doc ~f ~arg:"nu-var" ~docv:"NU-VAR"

    let rho_range () =
      let doc =
        "Max possible value for the reporting probability rho. <= 1."
      in
      let f x hy = hy#with_rho_range x in
      float_arg ~doc ~f ~arg:"rho-range" ~docv:"RHO-RANGE"

    let size () =
      let doc =
        "Reference size of the host population."
      in
      let f x hy = hy#with_n x in
      float_arg ~doc ~f ~arg:"size" ~docv:"SIZE"

    let prevalence () =
      let doc =
        "Initial expected proportion of infected."
      in
      let f x hy = hy#with_prevalence x in
      float_arg ~doc ~f ~arg:"prevalence" ~docv:"PREVALENCE"


    (* prop_base *)

    let beta_jump () =
      let doc =
        "Standard deviation of beta proposal."
      in
      let f x hy = hy#with_beta_jump x in
      float_arg ~doc ~f ~arg:"beta-jump" ~docv:"BETA-JUMP"

    let nu_jump () =
      let doc =
        "Standard deviation of nu proposal."
      in
      let f x hy = hy#with_nu_jump x in
      float_arg ~doc ~f ~arg:"nu-jump" ~docv:"NU-JUMP"

    let rho_jump () =
      let doc =
        "Standard deviation of rho proposal."
      in
      let f x hy = hy#with_rho_jump x in
      float_arg ~doc ~f ~arg:"rho-jump" ~docv:"RHO-JUMP"

    let sr_jump () =
      let doc =
        "Standard deviation of S0 and R0 initial proportion."
      in
      let f x hy = hy#with_sr_jump x in
      float_arg ~doc ~f ~arg:"sr-jump" ~docv:"SR-JUMP"

    let i_jump () =
      let doc =
        "Standard deviation of I0 initial proportion."
      in
      let f x hy = hy#with_i_jump x in
      float_arg ~doc ~f ~arg:"i-jump" ~docv:"I-JUMP"


    (* lik *)

    let tf () =
      let doc =
        "Total duration of the data."
      in
      let f x hy = hy#with_tf x in
      float_arg ~doc ~f ~arg:"tf" ~docv:"TF"

    let dt () =
      let doc =
        "Time interval between datapoints."
      in
      let f x hy = hy#with_dt x in
      float_arg ~doc ~f ~arg:"dt" ~docv:"DT"

    let coal_var () =
      let doc =
        "Expected error variance on coalescence rate estimates."
      in
      let f x hy = hy#with_coal_var x in
      float_arg ~doc ~f ~arg:"coal-var" ~docv:"COAL-VAR"

    let n_best_of () =
      let doc =
        "Number of trajectories to take the best likelihood from."
      in
      let f x hy = hy#with_n_best_of x in
      int_arg ~doc ~f ~arg:"nbest-of" ~docv:"NBEST-OF"

    let cases_over_disp () =
      let doc =
        "Overdispersion of case count data."
      in
      let f x hy = hy#with_cases_over_disp x in
      float_arg ~doc ~f ~arg:"cases-over-disp" ~docv:"CASES-OVER-DISP"
    
    let coal_over_disp () =
      let doc =
        "Overdispersion of coalescent effective population size data."
      in
      let f x hy = hy#with_coal_over_disp x in
      float_arg ~doc ~f ~arg:"coal-over-disp" ~docv:"COAL-OVER-DISP"

    let ode_apar () =
      let f map_apar hy =
        hy#with_ode_apar (map_apar hy#ode_apar)
      in
      Term.(const f
          $ Sim.Ode.Lsoda.Term.apar
      )


    (* prior_latent *)

    let sigma_mean () =
      let doc =
        "10-log of the sigma prior mean."
      in
      let f x hy = hy#with_sigma_mean x in
      float_arg ~doc ~f ~arg:"sigma-mean" ~docv:"SIGMA-MEAN"
         
    let sigma_var () =
      let doc =
        "10-log-variance of the sigma prior."
      in
      let f x hy = hy#with_sigma_var x in
      float_arg ~doc ~f ~arg:"sigma-var" ~docv:"SIGMA-VAR"


    (* prop_latent *)

    let sigma_jump () =
      let doc =
        "Standard deviation of sigma proposal."
      in
      let f x hy = hy#with_sigma_jump x in
      float_arg ~doc ~f ~arg:"sigma-jump" ~docv:"SIGMA-JUMP"


    (* prior_circular *)

    let gamma_mean () =
      let doc =
        "Mean of the 10-lognormal gamma prior."
      in
      let f x hy = hy#with_gamma_mean x in
      float_arg ~doc ~f ~arg:"gamma-mean" ~docv:"GAMMA-MEAN"

    let gamma_var () =
      let doc =
        "Variance of the 10-lognormal gamma prior."
      in
      let f x hy = hy#with_gamma_var x in
      float_arg ~doc ~f ~arg:"gamma-var" ~docv:"GAMMA-VAR"

    let eta_mean () =
      let doc =
        "Mean of the exponential eta prior."
      in
      let f x hy = hy#with_eta_mean x in
      float_arg ~doc ~f ~arg:"eta-mean" ~docv:"ETA-MEAN"


    (* prop_circular *)

    let betavar_jump () =
      let doc =
        "Standard deviation of betavar proposal."
      in
      let f x hy = hy#with_betavar_jump x in
      float_arg ~doc ~f ~arg:"betavar-jump" ~docv:"BETAVAR-JUMP"

    let phase_jump () =
      let doc =
        "Standard deviation of phase proposal."
      in
      let f x hy = hy#with_phase_jump x in
      float_arg ~doc ~f ~arg:"phase-jump" ~docv:"PHASE-JUMP"

    let gamma_jump () =
      let doc =
        "Standard deviation of gamma proposal."
      in
      let f x hy = hy#with_gamma_jump x in
      float_arg ~doc ~f ~arg:"gamma-jump" ~docv:"GAMMA-JUMP"

    let eta_jump () =
      let doc =
        "Standard deviation of eta proposal."
      in
      let f x hy = hy#with_eta_jump x in
      float_arg ~doc ~f ~arg:"eta-jump" ~docv:"ETA-JUMP"


    (* prop_stochastic *)

    let prm_nredraws () =
      let doc =
        "Number of color slices to redraw on proposals for prm."
      in
      let f x hy = hy#with_prm_nredraws x in
      int_arg ~doc ~f ~arg:"prm-nredraws" ~docv:"PRM-NREDRAWS"

    let prm_ntranges () =
      let doc =
        "Number of time ranges to consider for adaptive proposals for prm."
      in
      let f x hy = hy#with_prm_ntranges x in
      int_arg ~doc ~f ~arg:"prm-ntranges" ~docv:"PRM-NTRANGES"

    let prm_jump () =
      let doc =
        "Standard deviation of prm proposal."
      in
      let f x hy = hy#with_prm_jump x in
      float_arg ~doc ~f ~arg:"prm-jump" ~docv:"PRM-JUMP"

    let base () =
      let f beta_m beta_v beta_j
            nu_m nu_v nu_j
            rho_range rho_j
            size prevalence sr_j i_j
            hy =
        hy
        |> beta_m
        |> beta_v
        |> beta_j
        |> nu_m
        |> nu_v
        |> nu_j
        |> rho_range
        |> rho_j
        |> size
        |> prevalence
        |> sr_j
        |> i_j
      in
      Term.(const f
          $ beta_mean ()
          $ beta_var ()
          $ beta_jump ()
          $ nu_mean ()
          $ nu_var ()
          $ nu_jump ()
          $ rho_range ()
          $ rho_jump ()
          $ size ()
          $ prevalence ()
          $ sr_jump ()
          $ i_jump ()
      )

    (* FIXME ode_apar *)
    let lik () =
      let f tf dt coal_v n_best_of cases_ovd coal_ovd ode_apar hy =
        hy
        |> tf
        |> dt
        |> coal_v
        |> n_best_of
        |> cases_ovd
        |> coal_ovd
        |> ode_apar
      in
      Term.(const f
          $ tf ()
          $ dt ()
          $ coal_var ()
          $ n_best_of ()
          $ cases_over_disp ()
          $ coal_over_disp ()
          $ ode_apar ()
      )

    let latent () =
      let f sigma_m sigma_v sigma_j hy =
        hy
        |> sigma_m
        |> sigma_v
        |> sigma_j
      in
      Term.(const f
          $ sigma_mean ()
          $ sigma_var ()
          $ sigma_jump ()
      )

    let circular () =
      let f bv_j gam_m gam_v gam_j eta_m eta_j hy =
        hy
        |> bv_j
        |> gam_m
        |> gam_v
        |> gam_j
        |> eta_m
        |> eta_j
      in
      Term.(const f
          $ betavar_jump ()
          $ gamma_mean ()
          $ gamma_var ()
          $ gamma_jump ()
          $ eta_mean ()
          $ eta_jump ()
      )

    let stochastic () =
      let f ndraws nranges prm_j hy =
        hy
        |> ndraws
        |> nranges
        |> prm_j
      in
      Term.(const f
          $ prm_nredraws ()
          $ prm_ntranges ()
          $ prm_jump ()
      )
  end
