open Sig


let base = [
  `Beta ;
  `Nu ;
  `Rho ;
  `Sir ;
]

let latent = [
  `Sigma ;
]

let circular = [
  `Betavar ;
  `Phase ;
  `Gamma ;
  `Eta ;
]

let stochastic = [
  `Seed ;
  `Prm ;
]


let kill_none f x =
  match f x with
  | Some y ->
      y
  | None ->
      assert false


module Read =
  struct
    let beta =
      function
      | "beta" ->
          Some `Beta
      | _ ->
          None

    let betavar =
      function
      | "betavar" ->
          Some `Betavar
      | _ ->
          None

    let phase =
      function
      | "phase" ->
          Some `Phase
      | _ ->
          None

    let sigma =
      function
      | "sigma" ->
          Some `Sigma
      | _ ->
          None

    let nu =
      function
      | "nu" ->
          Some `Nu
      | _ ->
          None

    let gamma =
      function
      | "gamma" ->
          Some `Gamma
      | _ ->
          None

    let rho =
      function
      | "rho" ->
          Some `Rho
      | _ ->
          None

    let eta =
      function
      | "eta" ->
          Some `Eta
      | _ ->
          None

    let sir =
      function
      | "sir" ->
          Some `Sir
      | _ ->
          None

    let prm =
      function
      | "prm" ->
          Some `Prm
      | _ ->
          None
         
    let seed =
      function
      | "seed" ->
          Some `Seed
      | _ ->
          None

    let combine l =
      L.fold_left (fun g f ->
        fun s ->
        match g s with
        | None ->
            f s
        | (Some _) as some_tag ->
            some_tag
      ) (fun _ -> None) l
  end


module Write =
  struct
    let any =
      function
      | `Beta ->
          "beta"
      | `Betavar ->
          "betavar"
      | `Phase ->
          "phase"
      | `Sigma ->
          "sigma"
      | `Nu ->
          "nu"
      | `Gamma ->
          "gamma"
      | `Rho ->
          "rho"
      | `Eta ->
          "eta"
      | `Sir ->
          "sir"
      | `Prm ->
          "prm"
      | `Seed ->
          "seed"

    (* for 'theta' output *)
    let columns =
      function
      | (`Beta | `Betavar | `Phase | `Sigma
        | `Nu | `Gamma | `Rho | `Eta | `Prm) as symb ->
          [any symb]
      | `Sir ->
          ["s0" ; "i0" ; "r0"]
      | `Seed ->
          []
  end
