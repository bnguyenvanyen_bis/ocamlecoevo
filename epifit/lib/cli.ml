module A = BatArray
module L = BatList
module String = BatString

module U = Util
module J = Jump

let proposal_symbols =
  ["prior" ; "custom" ; "AM" ; "MG" ; "RAM" ; "recipe"]

let tech_symbols =
  ["ode" ; "ctmjp"]

let likelihood_symbols =
  ["cases_none" ; "cases_poisson" ; "cases_negbin" ; "cases_prevalence" ;
   "coal_none" ; "coal_poisson" ; "coal_negbin" ; "coal_normal" ; "coal_empirical"]


let float_from row colname =
  float_of_string (Csv.Row.find row colname)


let int_from row colname =
  int_of_string (Csv.Row.find row colname)


let load_cases_data path =
  let f =
    function
    | "t" ->
        Some (fun s (_, c) -> (float_of_string s, c))
    | "cases" ->
        Some (fun s (t, _) -> (t, int_of_float (float_of_string s)))
    | _ ->
        None
  in
  match U.Csv.read_all_and_fold ~f ~path (0., 0) with
  | res ->
      Some res
  | exception Sys_error _ ->
      None


let load_prevalence_data path =
  let f =
    function
    | "t" ->
        Some (fun s (_, c) -> (float_of_string s, c))
    | "prevalence" ->
        Some (fun s (t, _) -> (t, int_of_float (float_of_string s)))
    | _ ->
        None
  in
  match U.Csv.read_all_and_fold ~f ~path (0., 0) with
  | res ->
      Some res
  | exception Sys_error _ ->
      None


let load_neff_data fname =
  match U.Csv.open_in fname with
  | chan ->
      let all_columns = U.Csv.columns chan in
      let neff_cols = L.filter (fun s ->
        (String.left s 5) = "neff:"
      ) all_columns
      in
      let ks = L.map (fun s ->
        Scanf.sscanf s "neff:%i" (fun k -> k)
      ) neff_cols
      in
      let columns =
        "t" :: (if neff_cols = [] then ["neff"] else neff_cols)
      in
      let inject =
        if L.length ks = 0 then
          function
          | "t" ->
              Some (fun s (_, neffs) -> (float_of_string s, neffs))
          | "neff" ->
              Some (fun s (t, neffs) -> (t, (float_of_string s) :: neffs))
          | _ ->
              None
        else
          (fun s ->
            if s = "t" then
              Some (fun s (_, neffs) -> (float_of_string s, neffs))
            else
              match Scanf.sscanf s "neff:%i" (fun _ -> ()) with
              | () ->
                  Some (fun s (t, neffs) -> (t, (float_of_string s) :: neffs))
              | exception Scanf.Scan_failure _ ->
                  None
          )
      in
      let f = U.Csv.Row.fold ~columns inject (0., []) in
      let neff_lists = U.Csv.read_all ~f ~chan in
      let neff_cads = L.map (fun (t, l) ->
        L.map (fun neff -> (t, neff)) l
      ) neff_lists
      in
      Some (A.of_list (U.transpose neff_cads))
  | exception Sys_error _ ->
      None


let load_nlclft_data fname =
  let inject =
    function
    | "t" ->
        Some (fun s (_, (nlv, nln)) -> (float_of_string s, (nlv, nln)))
    | "nleaves" ->
        Some (fun s (t, (_, nln)) -> (t, (int_of_string s, nln)))
    | "nlineages" ->
        Some (fun s (t, (nlv, _)) -> (t, (nlv, int_of_string s)))
    | _ ->
        None
  in
  match U.Csv.open_in fname with
  | chan ->
      let columns = U.Csv.columns chan in
      let f = U.Csv.Row.fold ~columns inject (0., (0, 0)) in
      let nlclft = U.Csv.read_all ~f ~chan in
      let nsamples, nlineages = J.split nlclft in
      let dns = J.int_diff nlineages in
      let coal_times = J.times @@ L.filter (fun (_, dn) -> dn = 1) @@ dns in
      Some Data.({ nlineages ; nsamples ; coal_times })
  | exception Sys_error _ ->
      None


let load_data path_root =
  let fname_cases = path_root ^ ".data.cases.csv" in
  let fname_prev = path_root ^ ".data.prevalence.csv" in
  let fname_neff = path_root ^ ".data.neff.csv" in
  let fname_nlclft = path_root ^ ".data.nlclft.csv" in
  let cases = load_cases_data fname_cases in
  let prevalence = load_prevalence_data fname_prev in
  let neff_ests = load_neff_data fname_neff in
  let coal_true = load_nlclft_data fname_nlclft in
  let coal_ests = U.Option.map (fun x -> A.make 0 x) coal_true in
  Data.translate Data.{
    cases ;
    prevalence ;
    coal_true ;
    coal_ests ;
    neff_ests
  }


let tag_tech = function
  | "ode" -> `Ode
  | "ctmjp" -> `Ctmjp
  | _ -> raise (Arg.Bad "invalid symbol")

let tag_prop = function
  | "prior" -> `Prior
  | "custom" -> `Custom
  | "AM" -> `AM
  | "MG" -> `MG
  | "RAM" -> `RAM
  | "recipe" -> `Recipe
  | _ -> raise (Arg.Bad "invalid symbol")


let tag_lik lik = Likelihood.(function
  | "cases_none" ->
      { lik with cases = `None }
  | "cases_poisson" ->
      { lik with cases = `Poisson }
  | "cases_negbin" ->
      { lik with cases = `Negbin }
  | "cases_prevalence" ->
      { lik with cases = `Prevalence }
  | "coal_none" ->
      { lik with coal = `None }
  | "coal_poisson" ->
      { lik with coal = `Poisson }
  | "coal_negbin" ->
      { lik with coal = `Negbin }
  | "coal_normal" ->
      { lik with coal = `Normal }
  | "coal_empirical" ->
      { lik with coal = `Empirical }
  | "coal_counts" ->
      { lik with coal = `Counts }
  | _ ->
      raise (Arg.Bad "invalid symbol")
)


let change_lik_alpha lik x = Likelihood.({ lik with alpha = Some x })
