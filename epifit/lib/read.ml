module ERR = Epi.Read.Reader

let req_float column using = ERR.{
  column ;
  optional = false ; 
  read = fun s th -> using th (float_of_string s) ;
}


let tf = ERR.tf

let dt = ERR.dt

let coal_var () =
  ERR.req_pos "coal_var" (fun hy -> hy#with_coal_var)

let nbest_of () =
  ERR.req_pos "nbest_of" (fun hy -> hy#with_n_best_of)

let cases_over_disp () =
  ERR.req_pos "cases_over_disp" (fun hy -> hy#with_cases_over_disp)

let coal_over_disp () =
  ERR.req_pos "coal_over_disp" (fun hy -> hy#with_coal_over_disp)

let beta_mean () =
  req_float "beta_mean" (fun hy -> hy#with_beta_mean)

let beta_var () =
  ERR.req_pos "beta_var" (fun hy -> hy#with_beta_var)

let nu_mean () =
  req_float "nu_mean" (fun hy -> hy#with_nu_mean)

let nu_var () =
  ERR.req_pos "nu_var" (fun hy -> hy#with_nu_var)

let rho_range () =
  ERR.req_pos "rho_range" (fun hy -> hy#with_rho_range)

let size () =
  ERR.req_pos "size" (fun hy -> hy#with_n)

let prevalence () =
  ERR.req_pos "prevalence" (fun hy -> hy#with_prevalence)

let beta_jump () =
  ERR.req_pos "beta_jump" (fun hy -> hy#with_beta_jump)

let nu_jump () =
  ERR.req_pos "nu_jump" (fun hy -> hy#with_nu_jump)

let rho_jump () =
  ERR.req_pos "rho_jump" (fun hy -> hy#with_rho_jump)

let sr_jump () =
  ERR.req_pos "sr_jump" (fun hy -> hy#with_sr_jump)

let i_jump () =
  ERR.req_pos "i_jump" (fun hy -> hy#with_i_jump)
