

type base = [
  | `Beta
  | `Nu
  | `Rho
  | `Sir
]


type latent = [
  | `Sigma
]


type circular = [
  | `Betavar
  | `Phase
  | `Gamma
  | `Eta
]


type stochastic = [
  | `Seed
  | `Prm
]
