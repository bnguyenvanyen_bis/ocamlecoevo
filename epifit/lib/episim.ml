open Sig


class prmfit_able = object
  val prm_v : Prmfit.t option = None

  method prm = prm_v

  method with_prm x = {< prm_v = x >}

  method without_intensities nuo =
    let nuio = U.Option.map (fun nu ->
        let intensities = Prmfit.one_intensities nu in
        (nu, intensities)
      ) nuo
    in
    {< prm_v = nuio >}
end


(* let's lose some type distinction *)
class theta = object
  inherit Epi.Param.t_unit
  inherit Epi.Param.seedable
  inherit prmfit_able
  inherit Param.init_seir
end


module Make (M : Epi.Sig.MODEL_SPEC) =
  struct
    module E = Epi.Process.Unit.Make (M)
    module Om = Epi.Process.Ode.Make (M)

    type tmp = Lac.Vec.t * Lac.Vec.t * Lac.Vec.t

    let model_name = Epi.Seirco.model_name M.sp

    let ode_init () =
      let d = Om.dim in
      Lac.Vec.((make0 d, make0 d, make0 d), make0 d)


    (* Epi.Model could save us to unify functions *)

    let of_tuple : type a. a tech -> (theta -> a) =
      function
      | Ode ->
          let _, z = ode_init () in
          if M.sp.latency then
            (fun th ->
              th
              |> Epi.Param.sir
              |> Om.sepir_to_seir th
              |> Om.of_seir ~z
            )
          else
            (fun th ->
              th
              |> Epi.Param.sir
              |> Om.of_sir ~z
            )
      | Prm ->
          if M.sp.latency then
            (fun th ->
              th
              |> Epi.Param.sir
              |> E.sepir_to_seir th
              |> Seirco.seir_to_float
              |> Seirco.seir_round
              |> E.of_seir
            )
          else
            (fun th ->
              th
              |> Epi.Param.sir
              |> Seirco.sir_to_float
              |> Seirco.sir_round
              |> E.of_sir
            )


    let simulate (type a) ~out ~(tech : a tech) hy =
      let conv : (Epi.Param.t_unit -> 's U.anypos -> a -> 'b) =
        match tech with
        | Ode ->
            Om.conv
        | Prm ->
            E.conv
      in
      let line : (Epi.Param.t_unit -> 't U.anypos -> a -> 'c) =
        match tech with
        | Ode ->
            Om.line
        | Prm ->
            E.line
      in
      let output : (theta -> (a, 'd, 'u) Sim.Sig.output) =
        match out with
        | `Cadlag ->
            let dt = Param.dt hy in
            (fun th ->
              let par = (th :> Epi.Param.t_unit) in
              let conv = conv par in
              Sim.Cadlag.convert ~dt ~conv
            )
        | `Csv chan ->
            let dt = Param.dt hy in
            let header = E.Csv.header in
            (fun th ->
              let par = (th :> Epi.Param.t_unit) in
              let line = line par in
              Sim.Csv.convert_compat ~dt ~chan ~header ~line
            )
      in
      let of_tuple = of_tuple tech in
      match tech with
      | Ode ->
          let tf = Param.tf hy in
          let apar = Param.ode_apar hy in
          (fun th ->
            let par = (th :> Epi.Param.t_unit) in
            let z0 = of_tuple th in
            let output = output th in
            Om.sim ~apar par z0 ~output z0 tf
          )
      | Prm ->
          (fun th ->
            match th#prm with
            | None ->
                Printf.eprintf "simulate : no prm\n" ;
                raise Sim.Sig.Simulation_error
            | Some (prm, _) ->
                let par = (th :> Epi.Param.t_unit) in
                let z0 = of_tuple th in
                let output = output th in
                match E.Prm_approx.sim par ~output z0 prm with
                | res ->
                    res
                | exception Not_found ->  (* example Pop.remove *)
                    raise Sim.Sig.Simulation_error
          )


    let sim ~tech hy =
      let f = simulate ~out:`Cadlag ~tech hy in
      (fun th ->
        match f th with
        | res ->
            (Epi.Param.rho th, res)
        | exception Invalid_argument s ->
            (* for instance bad param value *)
            raise (Fit.Sig.Simulation_error ("invalid_arg: " ^ s))
        | exception Failure s ->
            (* for instance bad param value *)
            raise (Fit.Sig.Simulation_error ("failure: " ^ s))
        | exception Sim.Sig.Simulation_error ->
            (* for instance no prm or bad simulation *)
            raise (Fit.Sig.Simulation_error "simulation_error")
      )


    let csv_sim ~tech ~chan hy th =
      match simulate ~out:(`Csv chan) ~tech hy th with
      | res ->
          ignore res
      | exception Sim.Sig.Simulation_error ->
          raise (Fit.Sig.Simulation_error "simulation_error")

    (* We rewrite it because we need it to be different *)
    let load_par fname hyper =
      Epi.Cli.load_par_from [Read.tf () ; Read.dt ()] fname hyper

    let theta_reads = Epi.Read.Reader.(
      let fos = float_of_string in
        [s0 fos]
      @ [i0 fos ; r0 fos]
      @ (if M.sp.circular then circular () else [])
      @ (if M.sp.latency then latent () else [])
      @ [beta () ; nu () ; rho ()]
    )

    let load_theta_row row theta =
      Epi.Cli.load_par_row theta_reads row theta

    let load_theta =
      function
      | None ->
          (fun th -> th)
      | Some s ->
          (fun th -> Epi.Cli.load_par_from theta_reads th s)

    let load_prm hy par fname =
      let tf = Param.tf hy in
      let h = Param.h hy in
      let time_range = (F.zero, tf) in
      let ntslices = Epi.Point.ntslices ~h tf in
      match Epi.Prm_color.read ~time_range ~ntslices Epi.Point.v0 fname with
      | prmo ->
          Some (par#without_intensities prmo)
      (* could Sys_error be something else than 'no such file' ? *)
      | exception Sys_error _ ->
          None

    let sim_accepted (type a) ~(tech : a tech) hy th path =
      (* Printf.eprintf "load_par\n%!" ; *)
      (* read tf and dt from '.par.csv' file *)
      let hy' = load_par hy (Printf.sprintf "%s.par.csv" path) in
      (* read 'accepted' lines from '.theta.csv' file *)
      let in_fname, out_fname =
        match tech with
        | Ode ->
            (Printf.sprintf "%s.theta.csv" path,
             (fun k -> Printf.sprintf "%s.%i.traj.ode.csv" path k))
        | Prm ->
            (Printf.sprintf "%s.iter.theta.csv" path,
             (fun k -> Printf.sprintf "%s.%i.traj.prm.csv" path k))
      in
      let in_chan = Csv.of_channel ~has_header:true (open_in in_fname) in
      let sim k th' =
        let chan = open_out (out_fname k) in
        ignore (simulate ~out:(`Csv chan) ~tech hy' th') ;
        close_out chan
      in
      (* simulate and output to out_fname file *)
      Csv.Rows.iter ~f:(fun row ->
        if Csv.Row.find row "rejected" = "accept" then
          let k = int_of_string (Csv.Row.find row "k") in
          (* Printf.eprintf "load_theta\n" ; *)
          let th' = load_theta_row row th in
          match tech with
          | Ode ->
              sim k th'
          | Prm ->
              let fname = Printf.sprintf "%s.%i.prm.points.csv" path k in
              Printf.eprintf "load_prm at %s\n%!" fname ;
              begin match load_prm hy' th' fname with
              | Some th'' ->
                  Printf.eprintf "found prm file\n%!" ;
                  sim k th''
              | None ->
                  ()
              end
      ) in_chan
  end
