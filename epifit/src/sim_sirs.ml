

module E = Epifit.Episim.Make (Epi.Sirs.Model)
module M = Epifit.Sirs.Mcmc ( )


let in_r = ref None
let tech_r = ref "ode"


let specl = [
  ("-in",
   Arg.String (fun s -> in_r := Some s),
   "Path to read from.") ;
  ("-tech",
   Arg.Symbol (["prm" ; "ode"], (fun s -> tech_r := s)),
   "Technique to resimulate -- 'ode' or 'prm'.") ;
]

let anon_fun s =
  Printf.printf "Ignored anonymous argument : %s" s

let usage_msg =
  " Simulate Prm_approx trajectories for accepted samples of a SIRS mcmc run."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let path =
      match !in_r with
      | None ->
          raise (Arg.Bad "missing mandatory argument '-in'")
      | Some s ->
          s
    in
    let hy = M.default_hyper in
    let th = Epifit.Sirs.Fit.default_theta in
    match !tech_r with
    | "prm" ->
        E.sim_accepted ~tech:Epifit.Sig.Prm hy th path
    | "ode" ->
        E.sim_accepted ~tech:Epifit.Sig.Ode hy th path
    | _ ->
        raise (Arg.Bad "Bad symbol")
  end;;


main ()
