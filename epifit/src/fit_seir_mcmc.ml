open Epifit

module La = Lift_arg
module Di = Default_init

module M = Seir.Mcmc ( )

let default_theta = new Episim.theta

let cv = Epi.Read.float_from
let reads = Epi.Read.([
  s0 cv ;
  i0 cv ;
  r0 cv ;
  seed ;
  beta ;
  sigma ;
  nu ;
  rho ;
])


let load_par iparo s =
  match iparo with
  | None ->
      Some (Epi.Cli.load_par reads default_theta s)
  | Some ipar ->
      Some (Epi.Cli.load_par reads ipar s)


let set_fix infpars theta = Infer.(
  function
  | (`Beta | `Nu | `Rho | `Sir) as symb ->
      fix_base infpars theta symb
  | `Sigma as symb ->
      fix_latent infpars theta symb
  | (`Prm | `Seed) as symb ->
      fix_stochastic infpars theta symb
)


let set_infer infpars is_adapt = Infer.(
  function
  | (`Beta | `Nu | `Rho | `Sir) as symb ->
      infer_base infpars is_adapt symb
  | `Sigma as symb ->
      infer_latent infpars is_adapt symb
  | (`Prm | `Seed) as symb ->
      infer_stochastic infpars is_adapt symb
)


let symbol_read = Symbols.(
  kill_none (Read.(combine [
    beta ;
    sigma ;
    nu ;
    rho ;
    sir ;
    prm ;
    seed
  ]))
)


let update_infer is_adapt infer symb =
  set_infer infer is_adapt (symbol_read symb)


let update_fix paro_r symb infer s =
  let theta = match !paro_r with
    | None -> default_theta
    | Some th -> th
  in
  set_fix infer theta (symbol_read symb) s


let par_symbols = Symbols.(
  L.map Write.any (base @ latent @ stochastic)
)


let out_r = ref Di.out

let chol_every_r = ref Di.chol_every

let prm_every_r = ref Di.prm_every

let traj_every_r = ref Di.traj_every

let data_r = ref Di.data

let seed_r = ref Di.seed

let n_thin_r = ref Di.n_thin

let n_thin_costly_r = ref Di.n_thin_costly

let n_estim_r = ref Di.n_estim

let n_prior_r = ref Di.n_prior

let n_ode_r = ref Di.n_ode

let n_iter_r = ref Di.n_iter

let stoch_n_prior_r = ref Di.stochastic_n_prior

let hypar_r = ref M.default_hyper

let paro_r = ref Di.paro

let infer_r = ref (new Seir.infer_pars)

(* let tags_from_prior_r = ref [] *)

let lik_r = ref Di.lik


let specl = Specs.([
  La.spec_of out_r out ;
  La.spec_of chol_every_r chol_every ;
  La.spec_of prm_every_r prm_every ;
  La.spec_of traj_every_r traj_every ;
  La.spec_of data_r data ;
  La.spec_of seed_r seed ;
  La.spec_of n_thin_r nthin ;
  La.spec_of n_estim_r nestim ;
  La.spec_of n_prior_r nprior ;
  La.spec_of n_ode_r node ;
  La.spec_of n_iter_r niter ;
  stochastic_nprior stoch_n_prior_r ;
  La.spec_of paro_r (from load_par) ;
  La.spec_of lik_r likelihood ;
  La.spec_of lik_r lik_scale ;
]) @ (La.specl_of infer_r Specs.[
  infer_custom par_symbols update_infer ;
  infer_adapt par_symbols update_infer ;
  fix par_symbols (update_fix paro_r) ;
]) @ (Lift_arg.specl_of hypar_r M.specl)


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Fit a SEIR model via MCMC to some case and coal rate data."


let main () =
  begin
    Util.Gc.tune () ;
    Arg.parse specl anon_fun usage_msg ;
    let infer = !infer_r in
    let lik_spec = !lik_r in
    (* let from_prior = !tags_from_prior_r in *)
    let path =
      match !out_r with
      | None ->
          raise (Arg.Bad "-out is a mandatory argument")
      | Some s ->
          s
    in
    let chol_every = !chol_every_r in
    let prm_every = !prm_every_r in
    let traj_every = !traj_every_r in
    let seed = !seed_r in
    let n_thin = !n_thin_r in
    let n_estim = !n_estim_r in
    let n_prior = !n_prior_r in
    let n_ode = !n_ode_r in
    let n_iter = !n_iter_r in
    let stoch_n_prior = !stoch_n_prior_r in
    let hypar = !hypar_r in
    let paro = !paro_r in
    match !data_r with
    | None ->
        invalid_arg "Some data must be given"
    | Some data ->
        begin
          Printf.eprintf "Recipe (sample from prior then `RAM)\n" ;
          M.recipe
            ~path ?chol_every ?prm_every ?traj_every
            ~n_thin ~n_estim
            ~n_prior ~n_ode ~n_iter ~stoch_n_prior
            ?seed ~infer ~lik_spec
            hypar data paro
        end
  end;;


main ()
