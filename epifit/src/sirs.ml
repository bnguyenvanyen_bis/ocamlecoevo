module M = Epifit.Sirs.Mcmc ( )

let () = Cmdliner.Term.exit @@ Cmdliner.Term.eval (M.Term.run, M.Term.info)
