module L = BatList
module A = BatArray
module U = Util
module F = U.Float
module J = Jump

open Epifit


let main () =
  begin
    let pfp = Printf.fprintf in
    let window = F.Pos.of_float 200. in
    (* allow in *)
    let fname = "data/sim_sirs/vietnam_hcmc_dengue_like.78901.cut.bsp" in
    let o_i = open_out "test_empirical_kde.interpol.csv" in
    let o_t = open_out "test_empirical_kde.true.csv" in
    let data = Cli.load_data fname in
    let data_neffs = data.neff_ests in
    let neffs_by_time = A.of_list @@ U.transpose @@ A.to_list @@ data_neffs in
    let logd_interpol = A.map (fun c ->
      c |> J.values |> (U.estimate_log_density ~interpol:true ~h:window)
    ) neffs_by_time
    in
    let logd_true = A.map (fun c ->
      c |> J.values |> (U.estimate_log_density ~interpol:false ~h:window)
    ) neffs_by_time
    in
    let times = neffs_by_time |> A.to_list |> L.map (fun c ->
      c |> J.times |> L.hd
    )
    in
    let qs = L.map F.of_float (L.frange (~-. 100.) `To 2_000. 2_100) in
    let ymat_interpol = L.mapi (fun i _ -> L.map logd_interpol.(i) qs) times in
    let ymat_true = L.mapi (fun i _ -> L.map logd_true.(i) qs) times in
    (* output *)
    (* interpol *)
    pfp o_i "t,q" ;
    L.iter (fun q -> pfp o_i ",%f" (F.to_float q)) qs ;
    pfp o_i "\n" ;
    L.iter2 (fun t ys ->
      pfp o_i "%f," t ;
      L.iter (fun y -> pfp o_i ",%f" (F.to_float y)) ys ;
      pfp o_i "\n"
    ) times ymat_interpol ;
    close_out o_i ;
    (* true *)
    pfp o_t "t,q" ;
    L.iter (fun q -> pfp o_t ",%f" (F.to_float q)) qs ;
    pfp o_t "\n" ;
    L.iter2 (fun t ys ->
      pfp o_t "%f," t ;
      L.iter (fun y -> pfp o_t ",%f" (F.to_float y)) ys ;
      pfp o_t "\n"
    ) times ymat_true ;
    close_out o_t ;
  end;;

main ()
