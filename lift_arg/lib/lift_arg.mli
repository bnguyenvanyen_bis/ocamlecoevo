(** This module provides an additional convenience layer on top of Arg
    from the standard library.
    This allows the definition of more context independent specs.
    That way, components of the library can define what command line
    arguments they expect should be required when they are used.
    This helps to reduce code redundancy *)

(** spec with function. can then be used with an argument *)
type 'a spec =
  | Bool of ('a -> bool -> 'a)
  | Int of ('a -> int -> 'a)
  | Float of ('a -> float -> 'a)
  | String of ('a -> string -> 'a)
  | Symbol of string list * ('a -> string -> 'a)
  | Symbol_value of string list * (string -> ('a -> string -> 'a))
  | Tuple of 'a spec list

(** same as spec but for an object's method typically *)
type 'a method_spec =
  | Mbool of (bool -> 'a)
  | Mint of (int -> 'a)
  | Mfloat of (float -> 'a)
  | Mstring of (string -> 'a)
  | Msymbol of string list * (string -> 'a)
  | Mtuple of 'a method_spec list

(** [spec_of xref spec] is a stdlib's Arg.spec *)
val spec_of :
  'a ref ->
  Arg.key * 'a spec * Arg.doc ->
    Arg.key * Arg.spec * Arg.doc

(** [specl_of xref specl] is a stdlib's Arg.specl
 *  where xref will (potentially) get modified on Arg.parse *)
val specl_of :
  'a ref ->
  (Arg.key * 'a spec * Arg.doc) list ->
  (Arg.key * Arg.spec * Arg.doc) list


(** [specl_of_method xref mspecl] is a stdlib's Arg.specl *)
val specl_of_method :
  'a ref ->
  (Arg.key * 'a method_spec * Arg.doc) list ->
  (Arg.key * Arg.spec * Arg.doc) list


(** [map g g' specl] is specl where the spec applies
  * to a first argument of type b instead of a,
  * mapped with the functions g and g' *)
val map :
  ('b -> 'a -> 'b) ->
  ('b -> 'a) ->
  (Arg.key * 'a spec * Arg.doc) list ->
  (Arg.key * 'b spec * Arg.doc) list

(** [handle_chan s] is either open_out s or stdout(err) if s = "stdout(err)" *)
val handle_chan :
  ?open_flags : open_flag list option ->
  ?perm : int ->
  string ->
  out_channel

(** [handle_chan_in s] is either open_in s or stdin if s = "stdin" or "-" *)
val handle_chan_in : string -> in_channel

(** [open_chans out_fname input output lines] is an input channel
 *  and a list of lines (from input) and corresponding output channels,
 *  as specified by [input], [output] and [lines].
 *  If [input] is [Some s] but [output] is [None], the output filename
 *  for each [line] in [lines] will be [out_fname ~line s].
 *  If [lines] is empty or contains only one element,
 *  [out_fname s] will be used.
 *  If [input] and [output] are both [None], [stdin] and [stdout] are used. *)
val open_chans :
  (?line:int -> string -> string) ->
  string option ->
  string option ->
  int list ->
    in_channel * (int * out_channel) list
