open Sig


let print sl =
  P.printf "[\n" ;
  L.iter (P.printf "%s ;\n") sl ;
  P.printf "]\n"


let%expect_test _ =
  print (B.flat (B.leaf "A")) ;
  [%expect{|
    [
    A ;
    ] |}]


let%expect_test _ =
  print B.(flat (binode "A" (leaf "B") (leaf "C"))) ;
  [%expect{|
    [
    A ;
    B ;
    C ;
    ] |}]
