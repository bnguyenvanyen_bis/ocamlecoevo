open Sig

module B = Base


module Make_core (State : STATE) =
  struct
    let basic tree = tree  [@@ inline]
    let flat = B.flat
    let leaves = B.leaves
    let map = B.map
    let left_fold = B.left_fold
    let contour_iter = B.contour_iter
    let root_value = B.root_value
    let find_node = B.find_node
    let descend = B.descend
    let replace_subtree = B.replace_subtree
    let genealogy = B.genealogy
    let all_paths = B.all_paths
    let mrca = B.mrca

    module M = B.Merge(Set.Make(struct
      type t = State.t
      let compare = compare
    end))
    let merge = M.merge

  end


module Make (State : STATE) : (BINTREE with type state = State.t) =
  struct
    type state = State.t
    type t = state B.tree

    include Make_core (State)

    let validate tree = tree  [@@ inline]

    let leaf = B.leaf
    let node = B.node
    let binode = B.binode

    module T =
      struct
        module State = State
        let leaf = leaf
        let node = node
        let binode = binode
      end

    include Io.Make (T)
  end
