open Sig


module type INPUT =
  sig
    include STATE
    include TIMED with type t := t
    include LABELED with type t := t
  end


module type S =
  sig
    include BINTREE
    include TIME_TREE with type t := t and type state := state
    include HAS_LEN_TREE with type t := t and type state := state
    include LABEL_TREE with type t := t and type state := state

    module State : INPUT with type t = state and type label = label
  end


module Make (State : INPUT) :
  (S with type state = State.t and type label = State.label) =
  struct
    module State = State

    type state = State.t
    type t = state Base.tree

    include Bintree.Make_core (State)
    include Labeled.Make_core (State)
    include Timed.Make_core (State)
    include Timed.Make_io (State)
    include Timed.Make_has_len (struct
      module State = State
      let leaf = leaf
      let node = node
      let binode = binode
    end)
  end
