(* tracking relationships with int idents *)

module B = Base

module Intbl = BatHashtbl.Make (struct
  type t = int
  let hash n = n
  let equal = (=)
end)

(* new approach : just go up event by event from the most recent one *)
(* might use a set to keep sorted times into *)
module Tmap = BatMap.Make (struct
  type t = float
  let compare x y = ~- (compare x y)
end)

module Iset = BatSet.Make (struct
  type t = int
  let compare = compare
end)

module Imap = BatMap.Make (struct
  type t = int
  let compare = compare
end)

type ev =
  | Co of Iset.t
  | Ap of Iset.t
  | Apco of Iset.t * Iset.t  (* simultaneously one death one coalescence (= mutation) *)

(* birth time * death time * parent ident * children idents
 * children should be in order of birth *)
type indiv = {
  t_birth : float ;
  t_death : float ;
  parent_id : int ;
  children_ids : int list ;
}
type t = indiv Intbl.t


let forest_root = ~-1


(* FIXME can add trait_of : int -> 'a *)
let iter_descend f gen =
  let rec g_step i =
    let ind = Intbl.find gen i in
    f i ind ;
    List.iter g_step ind.children_ids
  in g_step forest_root

let get_birth_time i gen =
  (Intbl.find gen i).t_birth


let ancestry i gen =
  let rec add_ancestor anc_l k =
    let parent = (Intbl.find gen k).parent_id in
    if k = forest_root then (* reached the root *)
      parent :: anc_l
    else
      add_ancestor (parent :: anc_l) parent
  in add_ancestor [i] i

let tmrca anc anc' gen =
  let rec descend_ancestry l l' =
    match l, l' with
    | [], [] -> (* identical ancestries *)
      get_birth_time forest_root gen
    | (i :: _, []) | ([], i :: _) -> (* one descends from the other *)
      get_birth_time i gen 
    | i :: tl, i' :: tl' when i = i' ->
      descend_ancestry tl tl'
    | i :: _, i' :: _ ->
      let t = get_birth_time i gen in
      let t' = get_birth_time i' gen in
      min t t'
  in descend_ancestry anc anc'


let ev_merge _ iseto iseto' =
  match iseto, iseto' with
  | Some iset, None ->
    Some (Co iset)
  | None, Some iset ->
    Some (Ap iset)
  | Some iset, Some iset' ->
    Some (Apco (iset', iset))
  | None, None ->
    None


let pool_mrca iset gen =
  let anc_m = Iset.fold (fun i m -> Imap.add i (ancestry i gen) m) iset Imap.empty in
  (* first get tmrca for each unordered pair as a (t, i, j) list *)
  let rec f i anc sanc_m pairs =
    let sub_append i' anc' mrca_l = (tmrca anc anc' gen, i, i') :: mrca_l in
    let pairs' = Imap.fold sub_append sanc_m pairs in
    (* then call f on an element of sanc_m and the rest of sanc_m *)
    let i', anc' = Imap.min_binding sanc_m in
    let sanc_m' = Imap.remove i' sanc_m in
    if sanc_m' = Imap.empty then
      pairs'
    else
      f i' anc' sanc_m' pairs'
  in
  let i, anc = Imap.choose anc_m in
  let sanc_m = Imap.remove i anc_m in
  let pairs = f i anc sanc_m [] in
  (* then pool identical times *)
  let f_pool pool_m (t, i, i') =
    let iset =
      try
        Tmap.find t pool_m
      with Not_found ->
        Iset.empty
    in Tmap.add t (Iset.add i (Iset.add i' iset)) pool_m
  in List.fold_left f_pool Tmap.empty pairs


let choose_alive t iset ngen =
  (* hypothesis only two indivs of iset are alive at t *)
  let p_alive i =
    let ind = Intbl.find ngen i in
    (ind.t_birth <= t) && (t <= ind.t_death)
  in
  let fiset = Iset.filter p_alive iset in
  (* min_elt on the left : guarantees we keep 0 *)
  let mi = Iset.min_elt fiset in
  let rem = Iset.remove mi fiset in
  mi, rem
    

let coalesce rng p gen =
  (* decide ids that we keep (with proba p) *)
  (* we suppose all event times different *)
  (* also have death time ? *)
  let f_keep (i : int) _ iset =
    (* missing 0 in test_seq : why ? *)
    if (Util.rand_bernoulli ~rng p) then
      Iset.add i iset
    else
      iset
  in
  let keep_i_s = Intbl.fold f_keep gen (Iset.singleton forest_root) in
  let tmrca_m = pool_mrca keep_i_s gen in
  let f_death i dm =
    let td = (Intbl.find gen i).t_death in
    let iset =
      try
        Tmap.find td dm
      with Not_found ->
        Iset.empty
    in Tmap.add td (Iset.add i iset) dm
  in
  let death_m = Iset.fold f_death keep_i_s Tmap.empty in
  let ev_m = Tmap.merge ev_merge tmrca_m death_m in
  (*
  let print_ev t ev =
    match ev with
    | Ap iset -> Printf.printf "%f : death of %s\n" t
      (Iset.fold (fun i s -> Printf.sprintf "%s, %i" s i) iset "")
    | Apco (iset, iset') -> Printf.printf "%f : death of %s and coal of %s\n"
      t
      (Iset.fold (fun i s -> Printf.sprintf "%s, %i" s i) iset "")
      (Iset.fold (fun i s -> Printf.sprintf "%s, %i" s i) iset' "")
    | Co iset -> Printf.printf "%f : coal of %s\n" t (Iset.fold (fun i s -> Printf.sprintf "%s, %i" s i) iset "")
  in
  Tmap.iter print_ev ev_m ;
  *)
  let nindivs = Iset.cardinal keep_i_s in
  let ngen = Intbl.create nindivs in
  (* and now we can only take out events and not add them *)
  (* no need to reparent either, when we reach a Co
   * we just look for the only two indivs alive in ngen *)
  let appear t iset =
    Iset.iter (fun i ->
      Intbl.add ngen i { t_birth = 0. ;
                         t_death = t ;
                         parent_id = forest_root ;
                         children_ids = [] }
    ) iset
  in
  let coal t iset =
    let i, jset = choose_alive t iset ngen in
    let jl = Iset.elements jset in
    (* arbitrarily, keep i, kill jset *)
    let indi = Intbl.find ngen i in
    Intbl.replace ngen i { indi with children_ids = jl @ indi.children_ids } ;
    let f_reparent j =
      let indj = Intbl.find ngen j in
      Intbl.replace ngen j { indj with t_birth = t ;
                                       parent_id = i }
    in List.iter f_reparent jl
  in
  let rec f_back tm =
    (* FIXME how do we know when to stop ? in Co somewhere *)
    (* last event happening *)
    if tm = Tmap.empty then
      ()
    else
      begin
        let t, ev = Tmap.min_binding tm in
        match ev with
        | Ap iset ->  (* appearance of i *)
          (* first time we see i *)
          appear t iset ;
          let tm = Tmap.remove t tm in
          f_back tm
        | Co iset ->  (* coalescence of iset *)
          coal t iset ;
          let tm = Tmap.remove t tm in
          f_back tm
        | Apco (iset, iset') ->
          (* first time we see i *)
          (* handle Ap *)
          appear t iset ;
          (* then handle Co *)
          coal t iset' ;
          let tm = Tmap.remove t tm in
          f_back tm
      end
  in
  (* I think we necessarily start from a leaf so no children *)
  f_back ev_m ;
  ngen



  

(* FIXME more economic : build the tree pruned *)
let prune rng p gen =
  (* start from index 0, keep the roots, then descend through children
   * for each index, with proba p, forget the individual :
   * remove it from the children of its parents, add to these children
   * its children, and change the parent of all its children *)
  let forget i ind =
    (* FIXME can raise Not_found : why ? *)
    let pind = Intbl.find gen ind.parent_id in
    (* Pb : for tree_of we count on order of siblings so we need to sort
     * which is costly
     * What if we replaced by set ? *)
    let nsiblings = List.sort
      (fun j k -> compare (get_birth_time j) (get_birth_time k))
      (ind.children_ids @ (List.filter (fun j -> j <> i) pind.children_ids))
    in
    (* FIXME this replace doesn't necessarily work *)
    Intbl.replace gen ind.parent_id { pind with children_ids = nsiblings } ;
    let replace_parent k =
      let cind = Intbl.find gen k in
      (* normally parentc = i *)
      Intbl.replace gen k { cind with parent_id = ind.parent_id }
    in
    List.iter replace_parent ind.children_ids ;
    Intbl.remove gen i
  in
  let f_iter i value =
    (* forget with proba p except if it's the root (then don't forget) *)
    if (Util.rand_bernoulli ~rng p) || (i = forest_root) then
      ()
    else
      forget i value
  in iter_descend f_iter gen


(* not tail rec :( *)
let rec tree_of ?(i=0) gen =
  let ind = Intbl.find gen i in
  (* trees of the descendents, which should be ordered by birth time *)
  let ttree_l = List.map (fun i -> tree_of ~i gen) ind.children_ids in
  let rec f treel =
    match treel with
    | (tb, _, tree) :: treetl ->
      (* special case for mutation
       * corresponding to f treetl = leaf at tb *)
      (let ltree = f treetl in
       match Lt.basic ltree with
       | B.Leaf (t, i) when t = tb ->
           Lt.node (tb, i) tree
       | _ ->
           Lt.binode (tb, i) ltree tree)
    | [] ->
        Lt.leaf (ind.t_death, i)
  in
  (ind.t_birth, ind.t_death, f ttree_l)
    

let forest_of gen =
  (* should have tf to kill all indivs at tf ? *)
  let ind0 = Intbl.find gen forest_root in
  List.map (fun i ->
    let (_, _, tree) = tree_of ~i gen in
    tree
  ) (List.rev ind0.children_ids)
  (* rev : start by the most recent tree *) 


let of_forest tree_l =
  let gen = Intbl.create 997 in
  (* follow convention that parent is on the left *)
  (* difft rec functions for left / right branch ? *)
  let rec descend tree tb parent_id children_ids =
    match tree with
    | B.Leaf (t, i) ->
        (* leaf = death of the indiv we were following *)
        Intbl.add gen i
        { t_birth = tb ; t_death = t ; parent_id ; children_ids } ;
        i
    | B.Node ((_, _), stree) ->
        (* node = mutation of the indiv we are following
         * ignore it *)
        descend stree tb parent_id children_ids
    | B.Binode ((t, i), ltree, rtree) ->
        (* rtree = new child born at t from parent i *)
        let child = descend rtree t i [] in
        descend ltree tb parent_id (child :: children_ids)
  in
  let init tree =
    match tree with
    | B.Node ((t, _), stree) ->
        ignore (descend stree t forest_root [])
    | _ ->
        invalid_arg "unrooted tree"
  in List.iter (fun tree -> init @@ Lt.basic @@ tree) tree_l ;
  gen
