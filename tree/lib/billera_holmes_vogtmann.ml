(** This module is a work in progress as the minimal cover is inexact *)
module B = Base


module type INPUT = Labeltimed.S


module type BHV =
  sig
    module T : INPUT
    type label
    module Labelset : Set.S with type elt = label
    type edge = Labelset.t * Labelset.t
    module Intset : Set.S with type elt = int
    module Intmap : Map.S with type key = int
    module Intgraph : Graph.Sig.P with type V.t = int
    module Flowgraph : Graph.Sig.P with type V.t = int and type E.label = float
    module Edgeset : Set.S with type elt = edge
    module Edgemap : Map.S with type key = edge
    module Edgelmap : Map.S with type key = edge list
    type igsplit =
      | Unsplit of Intgraph.t
      | Split of Intgraph.t * Intgraph.t

    val cut_at_common_edges :
      (edge * float) B.tree * Edgeset.t ->
      (edge * float) B.tree * Edgeset.t ->
      (float Edgemap.t * float Edgemap.t * (float * float) Edgemap.t) Edgelmap.t

    val cover_from_matching :
      (int -> bool) ->
      Flowgraph.t ->
      Intgraph.t ->
      Intgraph.t ->
      Intset.t * Intset.t

    val min_weight_vertex_cover :
      (int -> bool) ->
      (edge * float) Intmap.t ->
      Intgraph.t ->
      igsplit

    val space_geodesic :
      float Edgemap.t ->
      float Edgemap.t ->
      (int -> bool) * (edge * float) Intmap.t * Intgraph.t list

    val distance : T.t -> T.t -> float
  end


module Complement (T : INPUT) :
  (BHV with type label = T.label
        and module T = T) =
  struct
    (* unused
    exception Not_extant
    *)
    
    module T = T

    module Label = T.State.Label

    type label = T.label
    module Labelset = Set.Make (Label)

    (* Can we use a better symmetric type ? (a, b) = (b, a) *)
    type edge = Labelset.t * Labelset.t

    (* unused
    let symedge (ls1, ls2) = (ls2, ls1)
    *)

    let cmpedge (lls, rls) (lls', rls') =
      (* we find the side with the root *)
      (* actually we would probably like a way to always keep the root on the left on creation *)
      let ls = if Labelset.mem Label.root_label lls then lls else rls in
      let ls' = if Labelset.mem Label.root_label lls' then lls' else rls' in
      Labelset.compare ls ls'

    (* tree edge as vertex *)
    module Edge = struct
      type t = edge
      let compare = 
        cmpedge
      (* FIXME possibly Intgraph.fold *)
      (*
      let hash e =
        let rec concat_int n il =
          match il with
          | i :: itl ->
            (* does it work if i is bigger than 997 ? *)
            (BatInt.pow 997 n) * i + (concat_int (n + 1) itl)
          | [] ->
            0
        in
        let hash_labelset ls =
          concat_int 0 (List.map (fun lab -> S.hash_label lab) (Labelset.elements ls))
        in
        let ls1, ls2 = e in
        (hash_labelset ls1) * (hash_labelset ls2)
      *)

      (* unused
      let equal e1 e2 = (compare e1 e2 = 0)
      *)
    end

    module EdgeList = struct
      type t = edge list

      let compare el1 el2 =
        let rec f el1 el2 =
          match el1, el2 with
          | [], [] -> 0
          | _ :: _, [] -> 1
          | [], _ :: _ -> -1
          | e1 :: etl1, e2 :: etl2 ->
            let n = cmpedge e1 e2 in
            if n = 0 then f etl1 etl2 else n
        in f el1 el2
    end

    module IntVertex = struct
      type t = int
      let compare = compare
      let hash n = n
      let equal n1 n2 = (n1 = n2)
    end

    (*
    module IntEdge = struct
      type t = int
      let compare = compare
      let default = 1
    end
    *)
  
    module FlowEdge = struct
      type t = float
      let compare = compare
      let default = 1.
    end

    module Flow = struct
      type t = float
      type label = float
      let max_capacity l = l
      let min_capacity _ = 0.
      let flow _ = 0. (* FIXME maybe ? *)

      let add = (+.)
      let sub = (-.)
      let zero = 0.
      let compare = compare
    end

    module Intset = Set.Make (IntVertex)
    module Intmap = Map.Make (IntVertex)
    module Edgeset = Set.Make (Edge)
    module Edgemap = Map.Make (Edge)
    module Edgelmap = Map.Make (EdgeList)
    (* Possibly unlabeled for Intgraph (only 0 or 1 edges) *)
    module GPDCB = Graph.Persistent.Digraph.ConcreteBidirectional
    module GPDCBL = Graph.Persistent.Digraph.ConcreteBidirectionalLabeled
    (* module Intgraph = GPDCBL (EdgeVertex) (IntEdge) *)
    module Intgraph = GPDCB (IntVertex)
    module EGOP = Graph.Oper.P (Intgraph)
    (* module Flowgraph = GPDCBL (EdgeVertex) (FlowEdge) *)
    module Flowgraph = GPDCBL (IntVertex) (FlowEdge)

    module GB = Graph.Flow.Goldberg_Tarjan (Flowgraph) (Flow)
    module FF = Graph.Flow.Ford_Fulkerson (Flowgraph) (Flow)

    let isempty = Intset.empty
    let imempty = Intmap.empty
    let esempty = Edgeset.empty
    let emempty = Edgemap.empty

    type igsplit =
      | Unsplit of Intgraph.t
      | Split of Intgraph.t * Intgraph.t

    (* FIXME switch from Edgeset to ints *)
    (*
    let edgeset em g =
      Intgraph.fold_vertex (fun n es -> Edgeset.add (Intmap.find n em) es) g Edgeset.empty
    *)

    let nodeset g =
      Intgraph.fold_vertex (fun n iset -> Intset.add n iset) g isempty

    (* FIXME switch from Edgeset to ints *)
    (*
    let edgeset_flow em fg =
      Flowgraph.fold_vertex (fun n es -> Edgeset.add (Intmap.find n em) es) fg Edgeset.empty
    *)

    let str_of_ifun f fg =
      let f_str (i1, x, i2) s =
        Printf.sprintf "%s\n  (%d, %d) -> %f" s i1 i2 (f (i1, x, i2))
      in Flowgraph.fold_edges_e f_str fg ""

    let str_of_intset iset =
      let f i s =
        s ^ " " ^ (string_of_int i)
      in "{" ^ (Intset.fold f iset "") ^ "}"

    let str_of_labelset labs =
      let str_fold lab s =
        s ^ " " ^ (Label.to_string lab)
      in "{" ^ (Labelset.fold str_fold labs "") ^ "}"

    let str_of_edge (labs1, labs2) =
      (str_of_labelset labs1) ^ " | " ^ (str_of_labelset labs2)

    (* unused
    let int_of_edge em e =
      try (em, Edgemap.find e em) with
      | Not_found ->
        let n = Edgemap.cardinal em in
        (Edgemap.add e n em, n)
    *)

    let edgegraph_vertices g =
      Intgraph.fold_vertex (fun i iset -> Intset.add i iset) g isempty

    (*
    let add_edgeset es eg =
      Edgeset.fold (fun v g -> Intgraph.add_vertex g v) es eg

    let add_edgeset_flow es fg = 
      Edgeset.fold (fun v g -> Flowgraph.add_vertex g v) es fg
    *)

    (* FIXME TEST IT *)
    (*
    let edgegraph_diff g1 g2 =
      (* removes from g1 vertices in g2 *)
      let f_rm i g =
        Intgraph.remove_vertex g i
      in
      Intgraph.fold_vertex f_rm g2 g1
    *)

    (* FIXME TEST IT *)
    let edgegraph_partition f g =
      (* f boolean evaluated on vertices. 
       * If start and end of an edge get true (resp. false),
       * the edge is there too. *)
      let vertex_part i (g_true, g_false) =
        if f i then
          (Intgraph.add_vertex g_true i, g_false)
        else
          (g_true, Intgraph.add_vertex g_false i)
      in
      let edge_part i_start i_end (g_true, g_false) =
        if f i_start && f i_end then
          (Intgraph.add_edge g_true i_start i_end, g_false)
        else if not (f i_start) && not (f i_end) then
          (g_true, Intgraph.add_edge g_false i_start i_end)
        else (* lost edge *)
          (g_true, g_false)
      in
      let g1, g2 = Intgraph.empty, Intgraph.empty in
      let g1, g2 = Intgraph.fold_vertex vertex_part g (g1, g2) in
      Intgraph.fold_edges edge_part g (g1, g2)


    let eg_with_vertices fg =
      Flowgraph.fold_vertex (fun n g -> Intgraph.add_vertex g n) fg Intgraph.empty

    let fg_with_vertices g =
      Intgraph.fold_vertex (fun n fg -> Flowgraph.add_vertex fg n) g Flowgraph.empty

    (* unused
    let union_edgemap et1 et2 =
      let f _ yo1 yo2 =
        match yo1, yo2 with
        | (None, Some y) | (Some y, None) ->
            Some y
        | Some _, Some _ ->
            invalid_arg "Identical edges"
        | None, None ->
            None
      in Edgemap.merge f et1 et2
    *)
      
    
    (* see Gmap ? *)
    let flow_of_edge_graph g =
      (* simply replace int by float *)
      let fg = fg_with_vertices g in
      let f e fg =
        let w = 1. in
        let ne = Flowgraph.E.create (Intgraph.E.src e) w (Intgraph.E.dst e)
        in Flowgraph.add_edge_e fg ne
      in Intgraph.fold_edges_e f g fg


    let edgetree (tree : T.t) =
      (* We ignore nodes *)
      let rec without_node tr =
        match tr with
        | B.Leaf x ->
            B.leaf x
        | B.Node (_, stree) -> 
            without_node stree
        | B.Binode (x, ltree, rtree) ->
            B.binode x (without_node ltree) (without_node rtree)
      in
      (* FIXME We need to keep the leaves somehow *)
      let rec to_edgetree prevx tr =
        match tr with
        | B.Node _ ->
            invalid_arg "should be without Node"
        | B.Leaf x ->
            let len = (T.State.to_time x) -. (T.State.to_time prevx) in
            let labs = Labelset.singleton (T.State.to_label x) in
            (B.leaf (labs, len), labs)
        | B.Binode (x, ltree, rtree) ->
            let len = (T.State.to_time x) -. (T.State.to_time prevx) in
            let (nltree, llabs) = to_edgetree x ltree in
            let (nrtree, rlabs) = to_edgetree x rtree in
            (* we only consider the leaf (and root) labels *)
            let labs = Labelset.union llabs rlabs in
            (B.binode (labs, len) nltree nrtree, labs)
      in match T.basic tree with
      | B.Leaf _
      | B.Binode _ ->
          invalid_arg "unrooted tree"
      | B.Node (x, stree) ->
          let wntree = without_node stree in
          let ntree, labs = to_edgetree x wntree in
          (* add the root to the labelset *)
          let all_labs = Labelset.add (T.State.to_label x) labs in
          B.map (fun (labs, len) ->
            ((labs, Labelset.diff all_labs labs), len)
          ) ntree


    let pair_subtrees (emm1 : (float Edgemap.t * float Edgemap.t) Edgelmap.t) emm2 =
      (* each (edge list) key associated to
       * a map of interior edges and their lengths
       * and a map of extant edges and their lengths *)
      let f_submerge _ xo1 xo2 =
        match xo1, xo2 with
        | Some x1, Some x2 ->
            Some (x1, x2)
        | ((Some _, None) | (None, Some _)) ->
            invalid_arg "key not present in both"
        | None, None ->
            None
      in
      let f_merge _ emo1 emo2 =
        (* FIXME should we also keep the lengths here ? *)
        match emo1, emo2 with
        | Some (interior_1, extant_1), Some (interior_2, extant_2) ->
            Printf.printf "n interior 1 : %d ; n interior 2 : %d\n"
            (Edgemap.cardinal interior_1) (Edgemap.cardinal interior_2) ;
            Some (
              interior_1,
              interior_2,
              Edgemap.merge f_submerge extant_1 extant_2
            )
        | ((Some _, None) | (None, Some _)) ->
          (* FIXME trees of different sizes fail with this. Identify them earlier ? *)
            failwith "non identical cut sequences"
        | None, None ->
            None
      in Edgelmap.merge f_merge emm1 emm2


    let cut_at_common_edges (tree1, es1) (tree2, es2) =
      let inters = Edgeset.inter es1 es2 in
      let is_common e = Edgeset.mem e inters in
      (* can we get this out to have less infixing ? *)
      (* gather_edges em esl emm tr descends the tree tr recursively.
       * esl is a sequence of common edges encountered
       * (corresponding to cuts into subtrees)
       * emm is an accumulator map of edgemaps indexed by an edge list
       * em is a local accumulator edgemap between cuts
       *)
      let rec gather_edges gath_e gath_lv common_e_l gath_e_map tr =
        match tr with
        | B.Node _ ->
            invalid_arg "not without node"
        | B.Leaf (e, x) ->
            (* all leaf edges are common *)
            (gath_e, Edgemap.add e x gath_lv, gath_e_map)
        | B.Binode ((e, x), ltree, rtree) ->
            if is_common e then
              (* longer cut sequence *)
              let ncommon_e_l = e :: common_e_l in
              (* gather on subtrees ltree rtree *)
              let ngath_e, ngath_lv, ngath_e_map =
                gather_edges emempty emempty ncommon_e_l gath_e_map ltree
              in
              let ngath_e, ngath_lv, ngath_e_map =
                gather_edges ngath_e ngath_lv ncommon_e_l ngath_e_map rtree
              in
              (* cut the common edge : half for each subtree as leaf *)
              let up_gath_lv = Edgemap.singleton e (x /. 2.) in
              let down_gath_lv = Edgemap.add e (x /. 2.) ngath_lv in
              (* update the gathered edges map *)
              let nngath_e_map =
                Edgelmap.add ncommon_e_l (ngath_e, down_gath_lv) ngath_e_map
              in
              (emempty, up_gath_lv, nngath_e_map)
            else
              (* no cut *)
              (* gather on subtrees ltree rtree *)
              let ngath_e, ngath_lv, ngath_e_map =
                gather_edges gath_e gath_lv common_e_l gath_e_map ltree
              in
              let ngath_e, ngath_lv, ngath_e_map =
                gather_edges ngath_e ngath_lv common_e_l ngath_e_map rtree
              in
              (* add the edge to edges *)
              let nngath_e = Edgemap.add e x ngath_e in
              (nngath_e, ngath_lv, ngath_e_map)
      in
      (* gather edges on tree1 *)
      let gath_e1, gath_lv1, gath_e_map1 =
        gather_edges emempty emempty [] Edgelmap.empty tree1
      in
      (* gather edges on tree2 *)
      let gath_e2, gath_lv2, gath_e_map2 =
        gather_edges emempty emempty [] Edgelmap.empty tree2
      in
      (* gather the remaining edges on [] binding *)
      (* FIXME root | rest is always a common edge so [] should get nothing ? *)
      (* FIXME the root edge should probably be part of the leaves ? *)
      let ngath_e_map1 = Edgelmap.add [] (gath_e1, gath_lv1) gath_e_map1 in
      let ngath_e_map2 = Edgelmap.add [] (gath_e2, gath_lv2) gath_e_map2 in
      pair_subtrees ngath_e_map1 ngath_e_map2

    let sq_edge_norm im iset =
      let f_add i x =
        let _, y = Intmap.find i im in
        Printf.printf "edge norm cmp : %f\n" y ;
        x +. y ** 2.
      in
      let sqnorm = Intset.fold f_add iset 0. in
      sqnorm

    (* unused
    let l2_edge_norm im iset =
      let sqnorm = sq_edge_norm im iset in
      sqrt sqnorm
    *)

    let sq_graph_part_norm is_left im ig =
      let iset = nodeset ig in
      let iset_left, iset_right = Intset.partition is_left iset in
      Printf.printf "n left : %d ; n right : %d\n"
      (Intset.cardinal iset_left)
      (Intset.cardinal iset_right) ;
      let sq_gnorm_left = sq_edge_norm im iset_left in
      let sq_gnorm_right = sq_edge_norm im iset_right in
      Printf.printf "sq norm left : %f ; sq norm right : %f\n"
      sq_gnorm_left sq_gnorm_right ;
      (sq_gnorm_left, sq_gnorm_right)

    let graph_norm is_left im ig =
      let sq_gnorm_left, sq_gnorm_right = sq_graph_part_norm is_left im ig in
      (sqrt sq_gnorm_left) +. (sqrt sq_gnorm_right)

    (* unused
    let edge_equal sp1 sp2 =
      let u1, d1 = sp1 and u2, d2 = sp2 in
         (u1 = u2 && d1 = d2)
      || (u1 = d2 && d1 = u2)
    *)

    let edge_compat e1 e2 =
      let u1, d1 = e1 and u2, d2 = e2 in
         Labelset.is_empty (Labelset.inter u1 u2)
      || Labelset.is_empty (Labelset.inter u1 d2)
      || Labelset.is_empty (Labelset.inter d1 u2)
      || Labelset.is_empty (Labelset.inter d1 d2)

    let flow_graph is_left nmap ig =
      let sq_norm_l, sq_norm_r = sq_graph_part_norm is_left nmap ig in
      (* FIXME or should it be /. (sqnorm_a + sqnorm_b) ? *)
      (* or something else ? *)
      (* some unused ints for source and sink make them globals ? *)
      let source = ~-1 in
      let sink = ~-2 in
      let fg0 = flow_of_edge_graph ig in
      let fg0 = Flowgraph.add_vertex fg0 source in
      let fg0 = Flowgraph.add_vertex fg0 sink in
      let f_add_edge i fg =
        let _, x = Intmap.find i nmap in
        let e = 
          if is_left i then 
            (source, x ** 2. /. sq_norm_l, i)
          else
            (i, x ** 2. /. sq_norm_r, sink)
        in Flowgraph.add_edge_e fg e
      in 
      let fg = Intgraph.fold_vertex f_add_edge ig fg0 in
      let f_print (n1, x, n2) =
        Printf.printf "  edge (%d -> %d) : %f\n" n1 n2 x
      in
      print_string "fg\n" ;
      Flowgraph.iter_edges_e f_print fg ;
      fg

    (* this is costly and doesn't really have a point *)
    (* FIXME need to test. how do we do that ? *)
    let matched_of_flow flowf fg =
      let f (i_start, x, i_end) ig = 
        if flowf (i_start, x, i_end) > 0. then
          (*
          let n = int_of_float (flowf e) in
          (* so it should be >= 1. ? *)
          *)
          (Printf.printf "with edge : (%d, %d)\n" i_start i_end ;
           Intgraph.add_edge ig i_start i_end)
        else
          ig
      in
      (* should not have -1 -2 and corresponding edges *)
      let fg = Flowgraph.remove_vertex fg ~-1 in
      let fg = Flowgraph.remove_vertex fg ~-2 in
      let eg = eg_with_vertices fg in
      Flowgraph.fold_edges_e f fg eg

    (* FIXME test *)
    let unmatched mg =
      (* find all vertices not connected to an edge *)
      let f_add i is =
        if Intgraph.out_degree mg i = 0 && Intgraph.in_degree mg i = 0 then
          Intset.add i is
        else
          is
      in
      Intgraph.fold_vertex f_add mg isempty

    (* FIXME test *)
    let alternating ug mg u =
      let rec f g1 g2 v es =
        Intset.union es (Intgraph.fold_succ (f g2 g1) g1 v isempty)
      in f ug mg u isempty

    let cover_from_matching is_left fg ig mg =
      (* FIXME apparently this doesn't work *)
      let us = unmatched mg in
      Printf.printf "unmatched nodes : %s\n" (str_of_intset us) ;
      let ug = EGOP.complement mg in
      (* follow alternating paths from unmatched nodes *)
      (* FIXME clarify this *)
      let f u b = Intset.union (alternating ug mg u) b in
      let z = Intset.fold f isempty us in
      if not (z = isempty) then
        let g_left, g_right = edgegraph_partition is_left ig in
        let iset_left = edgegraph_vertices g_left in
        let iset_right = edgegraph_vertices g_right in
        (* FIXME could there be more minimal ?
         * There's still an unresolved ambiguity (sometimes we have the choice) *)
        ((Intset.diff iset_left z), (Intset.inter iset_right z))
      else
        (* choose left / right depending on weight *)
        (* for each edge of the matching, take the node with the least weight *)
        (print_string "No unmatched nodes, choosing\n" ;
         let f_choose n1 n2 (iset1, iset2) =
           Printf.printf "edge n1=%d -> n2=%d\n" n1 n2 ;
           let _, x1, _ = Flowgraph.find_edge fg ~-1 n1 in
           let _, x2, _ = Flowgraph.find_edge fg n2 ~-2 in
           if x1 <= x2 then
             (Intset.add n1 iset1, iset2)
           else
             (iset1, Intset.add n2 iset2)
         in Intgraph.fold_edges f_choose mg (isempty, isempty))


    (* extension problem *)
    let min_weight_vertex_cover (is_left : (int -> bool)) (nmap : (edge * float) Intmap.t) (ig : Intgraph.t) =
      if Intgraph.nb_vertex ig <= 2 then
        (print_string "Unsplit, too few nodes\n" ;
         Unsplit ig)
        (* does this make sense ? *)
      else
        let wl, wr = sq_graph_part_norm is_left nmap ig in
        let fg = flow_graph is_left nmap ig in
        let flowf, _tot_flow = GB.maxflow fg ~-1 ~-2 in
        Printf.printf "flowf :%s" (str_of_ifun flowf fg) ;
        let mg = matched_of_flow flowf fg in
        let c1, d2 = cover_from_matching is_left fg ig mg in
        (* FIXME could we get this from the graph instead to be sure we
           're coherent with the computations ? *)
        let wc1 = (sq_edge_norm nmap c1) /. wl in
        let wd2 = (sq_edge_norm nmap d2) /. wr in
        let wcov = wc1 +. wd2 in
        if wcov < 1. then
          (let _left, right_set = Intset.partition is_left (nodeset ig) in
           let fst_set = Intset.union c1 (Intset.diff right_set d2) in
           let is_fst i = Intset.mem i fst_set in
           let fst_g, snd_g = edgegraph_partition is_fst ig in
           (* FIXME missing edges *)
           let fst_set = nodeset fst_g in
           let snd_set = nodeset snd_g in
           Printf.printf "fst : %s ; snd : %s\n"
           (str_of_intset fst_set)
           (str_of_intset snd_set) ;
           Printf.printf "Split with wcov : %f\n" wcov ;
           Split (fst_g, snd_g))
        else
          (Printf.printf "Unsplit, cover too big : %f\n" wcov ;
           Unsplit ig)

    let make_int_graph (em1 : float Edgemap.t) em2 =
      (* em1 em2 should have no common edges, as output by cut_at_common_edges *)
      let consume_edgeset e x (nm, em, n) =
        Printf.printf "new bind %d : (%s, %f)\n" (n + 1) (str_of_edge e) x ;
        (Intmap.add (n + 1) (e, x) nm, Edgemap.add e (n + 1) em, n + 1)
      in
      let (nm, eml, n) = Edgemap.fold consume_edgeset em1 (imempty, emempty, 0) in
      let (nm, emr, _) = Edgemap.fold consume_edgeset em2 (nm, emempty, n) in
      (* put all vertices in graph *)
      let g = Intmap.fold (fun n _ g -> Intgraph.add_vertex g n) nm Intgraph.empty in
      (* add edges for incompatible pairs *)
      let f_add_edge e1 n1 e2 n2 ig =
        if not (edge_compat e1 e2) then
          Intgraph.add_edge ig n1 n2
        else
          ig
      in
      let g = Edgemap.fold (fun e1 n1 ig1 -> Edgemap.fold (fun e2 n2 ig2 -> f_add_edge e1 n1 e2 n2 ig2) emr ig1) eml g in
      (* em1 is on the left *)
      (* change this to use eml *)
      let f_add _ n ns =
        Intset.add n ns
      in
      let is_left_s = Edgemap.fold f_add eml isempty in
      let is_left n = Intset.mem n is_left_s in
      (g, nm, is_left)

    (* Mostly we're cutting the initial incompatibility graph into pieces.
     * Is there an efficient way to do that, or should we just rebuild smaller graphs ? *)

    let str_of_space_geodesic is_left nmap igl =
      let f_sub_str i s =
        let e, _ = Intmap.find i nmap in
        s ^ ", " ^ (str_of_edge e)
      in
      let f_str s ig =
        let iset = nodeset ig in
        Printf.printf "nodeset : %s\n" (str_of_intset iset) ;
        let iset_l, iset_r = Intset.partition is_left iset in
        let subs_l = "(" ^ (Intset.fold f_sub_str iset_l "") ^ ")" in
        let subs_r = "(" ^ (Intset.fold f_sub_str iset_r "") ^ ")" in
        s ^ " ; " ^ subs_l ^ "+" ^ subs_r
      in List.fold_left f_str "" igl

    let space_geodesic (em1 : float Edgemap.t) em2 =
      let ig0, nmap, is_left = make_int_graph em1 em2 in
      Printf.printf "n em left : %d ; n em right : %d ; n nmap : %d ; n ig0 : %d\n"
      (Edgemap.cardinal em1) (Edgemap.cardinal em2) (Intmap.cardinal nmap) (Intgraph.nb_vertex ig0) ;
      let rec f (ig : Intgraph.t) =
        let igs = min_weight_vertex_cover is_left nmap ig in
        match igs with
        | Split (ig1, ig2) ->
          (f ig1) @ (f ig2)
        | Unsplit ig ->
          [ig]
      in
      let igsl = f ig0 in
      (is_left, nmap, igsl)

    let sub_path_length (is_left : (int -> bool)) (nmap : (edge * float) Intmap.t) (igl : Intgraph.t list) (extant_m : (float * float) Edgemap.t) =
      let rec f_interior isl =
        match isl with
        | [] ->
          0.
        | ig :: tl ->
          (* FIXME we need to know what's on the left too ? or not ? *)
          let cmp = (graph_norm is_left nmap ig) ** 2. in
          Printf.printf "interior : %f\n" cmp ;
          cmp +. (f_interior tl)
      in
      let f_merge_leaves _ (x1, x2) y =
        (x1 -. x2) ** 2. +. y
      in
      let length_interior = f_interior igl in
      let length_extant = Edgemap.fold f_merge_leaves extant_m 0. in
      Printf.printf "length_interior : %f ; length_extant : %f\n"
      length_interior length_extant ;
      (* pourquoi 0 interior ? *)
      length_interior +. length_extant


    let distance tree1 tree2 =
      (* trees of interior edges, with simple nodes taken out *)
      let etree1 = edgetree tree1 in
      let etree2 = edgetree tree2 in
      (* add edge *)
      let f_add es (e, _) = Edgeset.add e es in
      (* set of edges for both trees *)
      (* FIXME could directly be returned by edgetree ? *)
      let es1 = List.fold_left f_add esempty (B.flat etree1) in
      let es2 = List.fold_left f_add esempty (B.flat etree2) in
      Printf.printf "n es1 : %d ; n es2 : %d\n"
      (Edgeset.cardinal es1) (Edgeset.cardinal es2) ;
      (* edgemap pair edgelmap *)
      let empm = cut_at_common_edges (etree1, es1) (etree2, es2) in
      let f_space (interior1, interior2, extant) =
        let is_left, nmap, igl = space_geodesic interior1 interior2 in
        Printf.printf "%s\n" (str_of_space_geodesic is_left nmap igl) ;
        (is_left, nmap, igl, extant)
      in
      print_string "Finding the sequences of orthants containing the geodesics\n" ;
      let path_space_m = Edgelmap.map f_space empm in
      print_string "Computing the lengths of the geodesics\n" ;
      let f_add _ (is_left, imap, igsl, extm) y =
        (sub_path_length is_left imap igsl extm) +. y
      in sqrt (Edgelmap.fold f_add path_space_m 0.)
  end


module Make (State : Labeltimed.INPUT) :
  (BHV with type T.state = State.t
        and type label = State.label) =
  struct
    include Complement (Labeltimed.Make (State))
  end
