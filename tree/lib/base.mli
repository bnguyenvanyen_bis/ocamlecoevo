(** Basic trees *)

(** Basic binary tree type with unary nodes for state change *)
type 'a tree = private
  | Leaf of 'a
  | Node of 'a * 'a tree
  | Binode of 'a * 'a tree * 'a tree


val basic : 'a tree -> 'a tree
val validate : 'a tree -> 'a tree


(** [leaf x] is a tree with one leaf of state [x]. *)
val leaf : 'a -> 'a tree


(** [node x stree] is a tree with a node [x] leading to a tree [stree]. *)
val node : 'a -> 'a tree -> 'a tree


(** [binode x ltree rtree] is a binary node [x] leading to
 *  [ltree] on the left and [rtree] on the right. *)
val binode : 'a -> 'a tree -> 'a tree -> 'a tree


(** [root_value tree] is the value at the root of [tree] *)
val root_value : 'a tree -> 'a


(** flat representation *)
val flat : 'a tree -> 'a list


(** [nnodes tree] is the number of nodes in [tree] *)
val nnodes : 'a tree -> int


(** [leaves tree] is the list of leaves of [tree]
  *  and the paths leading up to them 
  *  with optional root [i] for paths *)
val leaves : ?i:int -> 'a tree -> (Path.t * 'a) list


(** [map f tree] applies [f] to all the nodes of [tree] *)
val map : ('a -> 'b) -> 'a tree -> 'b tree


(** [map_cps f tree] is {!map} of [f tree] in CPS version *)
val map_cps : ('a -> 'b) -> 'a tree -> 'b tree


(** [left_iter f tree] iterates [f] over [tree] going left first *)
val left_iter :
  ('a -> unit) ->
  'a tree ->
    unit


(** [right_iter f tree] iterates [f] over [tree] going right first *)
val right_iter :
  ('a -> unit) ->
  'a tree ->
    unit


(** [contour_iter ~fleft ~fmid ~fleaf ~fright tree]
  * iterates along the contour of [tree], calling
  * [fleft] going down,
  * [fmid] between nodes of a Binode, 
  * [fleaf] at a leaf, and
  * [fright] going back up *)
val contour_iter :
  fleft : ('a -> unit) ->
  fmid : ('a -> unit) ->
  fleaf : ('a -> unit) ->
  fright : ('a -> unit) ->
  'a tree ->
  unit


(** [contour_iter_cps] is {!contour_iter} in CPS version *)
val contour_iter_cps :
  fleft : ('a -> unit) ->
  fmid : ('a -> unit) ->
  fleaf : ('a -> unit) ->
  fright : ('a -> unit) ->
  'a tree ->
  unit


(** [left_fold f tree y] folds [f] over [tree] with initial value [y],
 *  going left first *)
val left_fold :
  ('a -> 'b -> 'b) ->
  'a tree ->
  'b ->
    'b


(** [right_fold f tree y] fold [f] over [tree] with initial value [y],
  * going right first *)
val right_fold :
  ('a -> 'b -> 'b) ->
  'a tree ->
  'b ->
    'b


(** [find_node p tree] finds the only node of [tree]
  *  to satisfy the predicate [p].
  *  @raise Failure if there is more than one match in [tree]. *)
val find_node : ('a -> bool) -> 'a tree -> (Path.t * 'a) option


(** [descend path tree] is the subtree at [path] in [tree]. *)
val descend : Path.t -> 'a tree -> 'a tree


(** [replace_subtree subtree path tree] is [tree]
  *  with [subtree] put at [path] *)
val replace_subtree : 'a tree -> Path.t -> 'a tree -> 'a tree


(** [genealogy path tree] is [tree] reduced to the genealogy of [path] *)
val genealogy : Path.split -> 'a tree -> 'a tree


(** [all_paths tree] is the list of paths to leaves in [tree] *)
val all_paths : ?i:int -> 'a tree -> Path.t list


(** [mrca tree pred1 pred2] is the most recent common ancestor of
  * the unique leaf matching [pred1],
  * and the unique leaf matching [pred2].
  * It is [None] if those leaves are not found.
  * @raise Failure if one matches more than once. *)
val mrca : 'a tree -> ('a -> bool) -> ('a -> bool) -> 'a option


(** module for merging on a set *)
module Merge : functor (S : Set.S) -> sig
  val merge :
    S.elt ->
    S.elt list list ->
      S.elt tree list
end


(** [merge default xll] is the tree formed by combining the lists of [xll],
 *  splitting it when elements diverge *)
val merge : 'a -> 'a list list -> 'a tree list

(** [merge_onto default xl tree] is [xl] merged onto [tree],
 *  following the same principle as {!merge} *)
val merge_onto : 'a -> 'a list -> 'a tree -> 'a tree

module P : (sig
  exception Error
  val tree :
    (Lexing.lexbuf -> Tokens.token) ->
    Lexing.lexbuf ->
      string tree option
end)

(** [read_newick chan] is the string tree formed by reading a tree
 *  in Newick format from [chan]. *)
val read_newick : in_channel -> string tree

(** [output_newick to_string chan tree] outputs the Newick representation
 *  of [tree] to [chan]. *)
val output_newick :
  ('a -> string) ->
  out_channel ->
  'a tree ->
    unit

(** [of_newick s] is some tree if [s] is its Newick representation,
 *  or [None]. *)
val of_newick : string -> string tree option

(** [to_newick to_string tree] is the Newick representation of [tree]. *)
val to_newick :
  ('a -> string) ->
  'a tree ->
    string
