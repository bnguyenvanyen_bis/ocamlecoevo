{
  module T = Tokens
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']
let letter = lower | upper

let number = digit+
let frac = '.' digit*
let exp = ['e' 'E'] ['+' '-']? digit+
let float = digit* frac? exp?
let word = (upper? lower+) | (upper+)

let unused_sign = ['<' '>' '|' '[' ']' '{' '}' '-' '_' '#' '@' '/' '+' '*']
let other = digit | float | letter | unused_sign | white
let state = other* (':' float)?

rule read =
  parse
  | white   { read lexbuf }
  | newline { read lexbuf }
  | state   { T.STATE (Lexing.lexeme lexbuf) }
  | '_'     { T.UNDER }
  | '/'     { T.SLASH }
  | ';'     { T.SEMIC }
  | ']'     { T.RSQUARE }
  | ')'     { T.RPAREN }
  | '}'     { T.RCURLY }
  | '['     { T.LSQUARE }
  | '('     { T.LPAREN }
  | '{'     { T.LCURLY }
  | eof     { T.EOF }
  | '='     { T.EQUAL }
  | '#'     { T.DIESE }
  | ','     { T.COMMA }
  | ':'     { T.COLON }
  | '|'     { T.BAR }
  | word    { T.WORD (Lexing.lexeme lexbuf) }
  | number  { T.NUMBER (Lexing.lexeme lexbuf) }

