(** Build trees from int hash tables *)

(** Hashtbl for integers *)
module Intbl : (BatHashtbl.S with type key = int)

(** Maps ordered by increasing time *)
module Tmap : (BatMap.S with type key = float)

(** birth time * death time * parent id * children id list *)
type indiv = {
  t_birth : float ;
  t_death : float ;
  parent_id : int ;
  children_ids : int list ;
}

type t = indiv Intbl.t


(** [forest_root] is the index reserved for the root of the forest,
 *  from which we can find all trees (its children) *)
val forest_root : int

(** [coalesce rng p z] is a coalescent formed by sampling individuals
 *  with probability [p] from [z] *)
val coalesce : Util.rng -> Util.anyproba -> t -> t


val prune : Util.rng -> Util.anyproba -> t -> unit

(** [tree_of z] forms a tree from [z] *)
val tree_of : ?i:int -> t -> float * float * Lt.t

(** [forest_of z] forms a list of tree from [z] *)
val forest_of : t -> Lt.t list

(** [of_forest trees] is a genealogy converted from the [trees] *)
val of_forest : Lt.t list -> t
