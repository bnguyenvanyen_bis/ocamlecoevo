open Sig

module B = Base


module type INPUT =
  sig
    module State : STATE

    val leaf : State.t -> State.t B.tree
    val node : State.t -> State.t B.tree -> State.t B.tree
    val binode : State.t -> State.t B.tree -> State.t B.tree -> State.t B.tree
  end


module Make (T : INPUT) =
  struct
    module T' =
      struct
        type t = T.State.t B.tree
        type state = T.State.t
        module State = T.State
        let leaf = T.leaf
        let node = T.node
        let binode = T.binode
      end

    module P = Parse.Make (T')

    let read_newick chan =
      match P.tree Lex.read (Lexing.from_channel chan) with
      | Some tree ->
          tree
      | None ->
          failwith "Empty input"

    let of_newick s =
      P.tree Lex.read (Lexing.from_string s)

    let output_newick chan tree =
      B.output_newick T.State.to_string chan tree

    let to_newick tree =
      B.to_newick T.State.to_string tree
  end
