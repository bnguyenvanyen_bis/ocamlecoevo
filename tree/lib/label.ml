module Int =
  struct
    type t = int
    let root_label = 0
    let equal (x : int) (y : int) =
      x = y
    let compare (x : int) (y : int) =
      compare x y
    let hash n = n
    let to_string = string_of_int
    let of_string s =
      match int_of_string s with
      | n ->
          Some n
      | exception Failure _ ->
          None
  end
