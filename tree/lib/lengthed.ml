open Sig

module B = Base

module State =
  struct
    type t = float
    let length_of t = t
    let with_length t' _ = t'
    let to_string t = Printf.sprintf ":%.2f" t
    let parse s = Scanf.sscanf s ":%f" (fun t -> t)
  end


module With_length (S : STATE) :
  sig
    include STATE with type t = float * S.t
    include LENGTHED with type t := t
  end =
  struct
    type t = float * S.t
    let to_length (d, _) = d
    let with_length d (_, x) = (d, x)
    let to_string (d, x) = Printf.sprintf "%s:%.2f" (S.to_string x) d
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun ss d -> (ss, d)) with
      | (ss, d) ->
          begin match S.of_string ss with
          | Some x ->
              Some (d, x)
          | None ->
              None
          end
      | exception Scanf.Scan_failure _ ->
          None
  end


module type INPUT =
  sig
    include STATE
    include LENGTHED with type t := t
  end


module type S =
  sig
    include BINTREE
    include LEN_TREE with type t := t and type state := state
  end


module Make (State : INPUT) :
  (S with type state = State.t) =
  struct
    type state = State.t

    type 'a alias = 'a B.tree = private
      | Leaf of 'a
      | Node of 'a * 'a B.tree
      | Binode of 'a * 'a B.tree * 'a B.tree

    type t = state alias

    module State = State

    include Bintree.Make_core (State)

    let leaf x =
      assert (State.to_length x >= 0.) ;
      B.leaf x

    let node x stree =
      assert (State.to_length x >= 0.) ;
      B.node x stree

    let binode x ltree rtree =
      assert (State.to_length x >= 0.) ;
      B.binode x ltree rtree

    let rec validate = function
      | B.Leaf x -> leaf x
      | B.Node (x, stree) -> node x (validate stree)
      | B.Binode (x, ltree, rtree) -> binode x (validate ltree) (validate rtree)

    module T =
      struct
        module State = State
        let leaf = leaf
        let node = node
        let binode = binode
      end

    include Io.Make (T)

    let branch_length tree =
      B.left_fold (fun x len -> len +. State.to_length x) tree 0.
  end
