
type 'a t = 'a Base.tree = private
  | Leaf of 'a
  | Node of 'a * 'a Base.tree
  | Binode of 'a * 'a Base.tree * 'a Base.tree


(** @canonical Tree.Path *)
module Path = Path

(** @canonical Tree.Tokens *)
module Tokens = Tokens

(** @canonical Tree.Lex *)
module Lex = Lex

(** @canonical Tree.Parse *)
module Parse = Parse


(** @canonical Tree.Base *)
module Base = Base


(** @canonical Tree.Sig *)
module Sig = Sig


(** @canonical Tree.Label *)
module Label = Label

(** @canonical Tree.State *)
module State = State

(** @canonical Tree.Ltx *)
module Ltx = Ltx


(** @canonical Tree.Lengthed *)
module Lengthed = Lengthed

(** @canonical Tree.Timed *)
module Timed = Timed

(** @canonical Tree.Labeled *)
module Labeled = Labeled

(** @canonical Tree.Labeltimed *)
module Labeltimed = Labeltimed

(** @canonical Tree.Genealogy *)
module Genealogy = Genealogy

(** @canonical Tree.Descent *)
module Descent = Descent

(** @canonical Tree.Draw *)
module Draw = Draw

(** @canonical Tree.Full *)
module Full = Full


(** @canonical Tree.T *)
module T = T

(** @canonical Tree.L *)
module L = L

(** @canonical Tree.Lt *)
module Lt = Lt

(** @canonical Tree.F *)
module F = F
