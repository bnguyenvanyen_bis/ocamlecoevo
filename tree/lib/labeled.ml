open Sig

module B = Base


module type INPUT =
  sig
    include STATE
    include LABELED with type t := t
  end


module type S =
  sig
    include BINTREE
    include LABEL_TREE with type t := t and type state := state

    module State :
      sig
        include STATE
        include LABELED with type t := t
      end
  end


module Make_core (State : INPUT) =
  struct
    type label = State.label

    let has_label id x' =
      let id' = State.to_label x' in
      State.Label.equal id id'

    let find_label tree id =
      Base.find_node (has_label id) tree

    let label_mrca tree id1 id2 =
      Base.mrca tree (has_label id1) (has_label id2)
  end


module Make (State : INPUT) :
  (S with type state = State.t
      and type label = State.label) =
  struct
    type state = State.t
    type t = state B.tree

    include Bintree.Make_core (State)
    include Make_core (State)

    module State = State

    let validate tree = tree  [@@ inline]

    let leaf = B.leaf
    let node = B.node
    let binode = B.binode

    module T =
      struct
        module State = State
        let leaf = leaf
        let node = node
        let binode = binode
      end

    include Io.Make (T)
  end
