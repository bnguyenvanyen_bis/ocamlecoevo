
type 'a t = 'a Base.tree = private
  | Leaf of 'a
  | Node of 'a * 'a Base.tree
  | Binode of 'a * 'a Base.tree * 'a Base.tree


module Path :
  sig
    include module type of Path
  end


module Tokens :
  sig
    include module type of Tokens
  end


module Lex :
  sig
    include module type of Lex
  end


module Parse :
  sig
    include module type of Parse
  end


module Base :
  sig
    include module type of Base
  end


module Sig :
  sig
    include module type of Sig
  end


module Label :
  sig
    include module type of Label
  end


module State :
  sig
    include module type of State
  end


module Ltx :
  sig
    include module type of Ltx
  end


module Lengthed :
  sig
    module type INPUT = Lengthed.INPUT

    module type S = Lengthed.S

    module Make :
      functor (State : INPUT) ->
        (S with type state = State.t)
  end


module Timed :
  sig
    module type INPUT = Timed.INPUT

    module type S = Timed.S

    module Make :
      functor (State : INPUT) ->
        (S with type state = State.t)
  end


module Labeled :
  sig
    module type INPUT = Labeled.INPUT

    module type S = Labeled.S

    module Make :
      functor (State : INPUT) ->
        (S with type state = State.t
            and type label = State.label)
  end


module Labeltimed :
  sig
    module type INPUT = Labeltimed.INPUT

    module type S = Labeltimed.S

    module Make :
      functor (State : INPUT) ->
        (S with type state = State.t
            and type label = State.label)
  end


module Genealogy :
  sig
    include module type of Genealogy
  end


module Descent :
  sig
    include module type of Descent
  end


module Draw :
  sig
    include module type of Draw
  end


module Full :
  sig
    module type INPUT = Full.INPUT

    module type S = Full.S

    module Make :
      functor (State : INPUT) ->
        (S with type state = State.t
            and type label = State.label)
  end


module T :
  sig
    include module type of T
  end


module L :
  sig
    include module type of L
  end


module Lt :
  sig
    include module type of Lt
  end


module F :
  sig
    include module type of F
  end
