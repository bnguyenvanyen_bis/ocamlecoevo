include Labeled.Make (struct
  type t = int
  type label = int
  module Label = Label.Int
  let to_label x = x  [@@ inline]
  let to_string = Label.to_string
  let of_string = Label.of_string
end)
