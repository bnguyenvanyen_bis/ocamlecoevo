module U = Util
open Path


type 'a tree =
  | Leaf of 'a
  | Node of 'a * 'a tree
  | Binode of 'a * 'a tree * 'a tree


let leaf x = Leaf x


let node x stree = Node (x, stree)


let binode x ltree rtree = Binode (x, ltree, rtree)


let basic tree = tree  [@@ inline]

let validate tree = tree  [@@ inline]


let root_value t =
  match t with
  | Leaf x
  | Node (x, _)
  | Binode (x, _, _) ->
      x


(* not tail recursive :( *)
let rec flat t =
  match t with
  | Leaf x ->
      x :: []
  | Node (x, st) ->
      x :: (flat st)
  | Binode (x, lt, rt) ->
      x :: (flat lt) @ (flat rt)


let nnodes t = List.length @@ flat @@ t


let leaves ?(i=0) tree =
  let rec f p t =
    match t with
    | Leaf x ->
        (p, x) :: []
    | Node (_, st) ->
        f (Forward p) st
    | Binode (_, lt, rt) ->
        (f (Right p) lt) @ (f (Left p) rt)
  in
  let l = f (End i) tree in
  List.map (fun (p, x) -> (rev ~i:i p, x)) l


let map f tree =
  let rec m t =
    match t with
    | Leaf x ->
        Leaf (f x)
    | Node (x, st) ->
        Node (f x, m st)
    | Binode (x, lt, rt) ->
        Binode (f x, m lt, m rt)
  in m tree


(* map in CPS *)
let map_cps f tree =
  let rec map tree treef =
    match tree with
    | Leaf x ->
        treef (Leaf (f x))
    | Node (x, stree) ->
        map stree (fun mstree -> treef (Node (f x, mstree)))
    | Binode (x, ltree, rtree) ->
        map ltree (fun mltree ->
          map rtree (fun mrtree ->
            treef (Binode (f x, mltree, mrtree))
          )
        )
  in map tree (fun mtree -> mtree)


let rec left_iter f t =
  match t with
  | Leaf x ->
      f x
  | Node (x, st) ->
      f x ; left_iter f st
  | Binode (x, lt, rt) ->
      f x ; left_iter f lt ; left_iter f rt


let rec right_iter f t =
  match t with
  | Leaf x ->
      f x
  | Node (x, st) ->
      f x ; right_iter f st
  | Binode (x, lt, rt) ->
      f x ; right_iter f rt ; right_iter f lt


(* not tail rec version *)
let contour_iter ~fleft ~fmid ~fleaf ~fright tree =
  let rec ci t =
    match t with
    | Leaf x ->
        fleaf x
    | Node (x, st) ->
        fleft x ; ci st ; fright x
    | Binode (x, lt, rt) ->
        fleft x ; ci lt ; fmid x ; ci rt ; fright x
  in ci tree


(* tail rec version - let's just copy map ! *)
let contour_iter_cps ~fleft ~fmid ~fleaf ~fright tree =
  let rec ci t (f : ((unit -> unit) -> (unit -> unit))) =
    match t with
    | Leaf x ->
        f (fun () -> fleaf x)
    | Node (x, st) ->
        ci st (fun sf -> f (fun () -> fleft x ; sf () ; fright x))
    | Binode (x, lt, rt) ->
        ci lt (fun lf ->
          ci rt (fun rf ->
            f (fun () -> fleft x ; lf () ; fmid x ; rf () ; fright x)
          )
        )
  in
  let bf f = (fun () -> f ()) in
  let ef = ci tree bf in
  ef ()


let convert_cps ~fleaf ~fnode ~fbi tree =
  let rec ci t (f : ('b -> 'b)) =
    match t with
    | Leaf x ->
        f (fleaf x)
    | Node (x, st) ->
        ci st (fun sf -> f (fnode x sf))
    | Binode (x, lt, rt) ->
        ci lt (fun lf ->
          ci rt (fun rf ->
            f (fbi x lf rf)
          )
        )
  in
  ci tree (fun b -> b)


(* goes left first *)
let rec left_fold f t y =
  match t with
  | Leaf x ->
      f x y
  | Node (x, st) ->
      left_fold f st (f x y)
  | Binode (_, lt, rt) ->
      left_fold f rt (left_fold f lt y)


(* goes right first *)
let rec right_fold f t y =
  match t with
  | Leaf x ->
      f x y
  | Node (x, st) ->
      right_fold f st (f x y)
  | Binode (_, lt, rt) ->
      right_fold f lt (right_fold f rt y)


let find_node (type a) (p : a -> bool) (tree : a tree) =
  let f x =
    if p x then
      Some (Path.End 0, x)
    else
      None
  in
  let fleaf x =
    f x
  in
  let fnode x res =
    match f x, res with
    | None, None ->
        None
    | None, Some (p, y) ->
        Some (Path.Forward p, y)
    | Some _ as found, None ->
        found
    | Some _, Some _ ->
        failwith "more than one match"
  in
  let fbi x left_res right_res =
    match f x, left_res, right_res with
    | None, None, None ->
        None
    | Some _ as found, None, None ->
        found
    | None, Some (p, y), None ->
        Some (Path.Left p, y)
    | None, None, Some (p, y) ->
        Some (Path.Right p, y)
    | Some _, Some _, Some _
    | None, Some _, Some _
    | Some _, None, Some _
    | Some _, Some _, None ->
        failwith "more than one match"
  in convert_cps ~fleaf ~fnode ~fbi tree


(* more complex *)
let rec descend p t =
  match (p, t) with
  | End _, _ ->
      t
  | Forward sp, Node (_, st) ->
      descend sp st
  | Left lp, Binode (_, lt, _) ->
      descend lp lt
  | Right rp, Binode (_, _, rt) ->
      descend rp rt
  | Forward _, (Binode _ | Leaf _) ->
      invalid_arg "Path doesn't follow tree"
  | (Left _ | Right _), (Node _ | Leaf _) ->
      invalid_arg "Path doesn't follow tree"


let replace_subtree sst p t =
  let rec f p t =
    match (p, t) with
    | End _, _ ->
        sst
    | Forward sp, Node (x, st) ->
        Node (x, f sp st)
    | Left lp, Binode (x, lt, rt) ->
        Binode (x, f lp lt, rt)
    | Right rp, Binode (x, lt, rt) ->
        Binode (x, lt, f rp rt)
    | Forward _, (Binode _ | Leaf _) ->
        invalid_arg "Path doesn't follow tree"
    | (Left _ | Right _), (Node _ | Leaf _) ->
        invalid_arg "Path doesn't follow tree"
  in f p t


let rec genealogy sp t =
  match sp, t with
  | Send, _ ->
      Leaf (root_value t)
  | Sforward sp, Node (x, st) ->
      Node (x, genealogy sp st)
  | Sleft lp, Binode (x, lt, _) ->
      Node (x, genealogy lp lt)
  | Sright rp, Binode (x, _, rt) ->
      Node (x, genealogy rp rt)
  | Split (lp, rp), Binode (x, lt, rt) ->
      Binode (x, genealogy lp lt, genealogy rp rt)
  | Sforward _, (Binode _ | Leaf _) ->
      invalid_arg "Path doesn't follow tree"
  | (Sleft _ | Sright _ | Split _), (Node _ | Leaf _) ->
      invalid_arg "Path doesn't follow tree"


let all_paths ?(i=0) tree =
  (* all paths that end from left to right as a list *)
  let rec f p t =
    match t with
    | Leaf _ -> 
        p :: []
    | Node (_, st) ->
        f (Forward p) st
    | Binode (_, lt, rt) ->
        (f (Right p) lt) @ (f (Left p) rt)
  in
  let l = f (End i) tree in
  List.map (fun p -> rev ~i:i p) l


let mrca tree pred1 pred2 =
  let fleaf x =
    match pred1 x, pred2 x with
    | true, false ->
        `Match1
    | false, true ->
        `Match2
    | false, false ->
        `No_match
    | true, true ->
        `Bad_match
  in
  let fnode _ res = res in
  let fbi x left_res right_res =
    match left_res, right_res with
    | `Match1, `Match2
    | `Match2, `Match1 ->
        `Mrca x
    | `Match1, `No_match
    | `No_match, `Match1 ->
        `Match1
    | `Match2, `No_match
    | `No_match, `Match2 ->
        `Match2
    | (`Mrca _ as mrca), `No_match
    | `No_match, (`Mrca _ as mrca) ->
        mrca
    | `No_match, `No_match ->
        `No_match
    | `Bad_match, `Bad_match
    | `Bad_match, (`Mrca _ | `Match1 | `Match2 | `No_match)
    | (`Mrca _ | `Match1 | `Match2 | `No_match ), `Bad_match
    | `Match1, `Match1
    | `Match2, `Match2
    | `Mrca _, `Mrca _
    | (`Match1 | `Match2), `Mrca _
    | `Mrca _, (`Match1 | `Match2) ->
        `Bad_match
  in
  match convert_cps ~fleaf ~fnode ~fbi tree with
  | `No_match | `Match1 | `Match2 ->
      None
  | `Mrca x ->
      Some x
  | `Bad_match ->
      failwith "more than one match"


module Merge (S : Set.S) =
  struct
    type t = S.elt

    let regroup (p : t * t list -> bool) (passed, remains) =
      let l, l' = List.partition p remains in
      (l :: passed, l')

    let unpack ll =
      let rec f acc ll =
        match ll with
        | [] -> acc
        | l :: tll ->
          (match l with
          | x :: tl -> f ((x, tl) :: acc) tll
          | [] -> f acc tll)
      in f [] ll

    (* tail-recursive functions written in continuation passing style *)
    let descend ~dflt x xll =
      let rec chain_binode x xltlll treef =
        match xltlll with
        | ([] | _ :: []) ->
          failwith "should not be reached"
        | lxltll :: rxltll :: [] ->
          let lxl, ltll = List.split lxltll in
          let rxl, rtll = List.split rxltll in
          let lx = List.hd lxl in
          let rx = List.hd rxl in
          descend lx ltll (fun treeleft -> 
            descend rx rtll (fun treeright ->
              treef (Binode (x, treeleft, treeright))))
        | xltll :: xltlltl ->
          let xl, tll = List.split xltll in
          let x' = List.hd xl in
          descend x' tll (fun treeleft ->
            chain_binode dflt xltlltl (fun treeright ->
              treef (Binode (x, treeleft, treeright) )))
      and descend x xll treef =
        (* first value of each list *)
        let xltll = unpack xll in
        let xl, tll = List.split xltll in
        let xs = S.of_list xl in
        (* might chain Binode in that case ... *)
        if xs = S.empty then
          treef (Leaf x)
        else if S.cardinal xs = 1 then
          let x' = S.min_elt xs in
          descend x' tll (fun tree -> treef (Node (x, tree)))
        else
          begin
            let regrouper x acc = regroup (fun (y, _) -> (y = x)) acc in
            (* Stack_overflow could happen on this fold, but not likely *)
            let xltlll, empty = S.fold regrouper xs ([], xltll) in
            assert (empty = []) ;
            chain_binode x (List.rev xltlll) (fun tree -> treef tree)
          end
      in descend x xll (fun tree -> tree)

    let merge dflt xll =
      let xltll = unpack xll in
      let xl, _ = List.split xltll in
      let xs = S.of_list xl in
      (* fold on all distinct values
       * [], xltll : nothing has been partitioned
       * xltlll, _ : everything has been partitioned, forget the right member which should be [] *)
      let xltlll, empty = S.fold (fun x acc ->
        regroup (fun (y, _) -> (y = x)) acc
      ) xs ([], xltll)
      in
      assert (empty = []) ;
      let start_branch (xltll : (t * t list) list) =
        let xl, tll = List.split xltll in
        (* we already know all elements of xl are the same *)
        (* FIXME if we have chained Binode, the tree might be unrooted *)
        let unrooted = descend ~dflt (List.hd xl) tll in
        Node (dflt, unrooted)
      in List.map start_branch (List.rev xltlll)

  end


(* FIXME in some functor to not have a functor call inside the function *)
let merge : type v. v -> v list list -> v tree list = fun dflt xll ->
  (* differences on first element : forest in compare order 
   * on subsequent elements : chain Binode in compare order *)
  let module S = Set.Make (struct type t = v let compare = compare end) in
  let module M = Merge(S) in
  M.merge dflt xll


let rec tree_of xl =
  match xl with
  | [] -> invalid_arg "Tree_base.tree_of"
  | x :: [] -> Leaf x
  | x :: xtl -> Node (x, tree_of xtl)


(* FIXME bad bad bad write something better *)
let merge_onto dflt xl tree =
  (* we compare the head of the tree to the head of the list *)
  (* we ignore default nodes as we go *)
  let rec follow_branch x' ltree rtree txl =
    let lx = root_value ltree in
    let rx = root_value rtree in
    match txl with
     | [] ->
       tree
     | x'' :: _ ->
       if rx = x'' then
         Binode (x', ltree, follow txl rtree)
       else if lx = x'' then
         Binode (x', follow txl ltree, rtree)
       else if rx = dflt then
         Binode (x', ltree, follow txl rtree)
       else if lx = dflt then
         Binode (x', follow txl ltree, rtree)
       else
         Binode (x', Binode (dflt, ltree, rtree), tree_of txl)
  and follow xl tree =
    match xl with
    | [] ->
      tree
    | x :: txl ->
      (* FIXME match on tree right away ? *)
      let x' = root_value tree in
      (* three cases : x' = x, x' = dflt, other *)
      if x' = x then
        match tree with
        | Leaf _ -> tree_of xl
        | Node (x', stree) -> Node (x', follow txl stree)
        | Binode (x', ltree, rtree) ->
          (* follow left or right ? depends on the next elements
           * we follow if one is equal then if none if one is dflt *)
          follow_branch x' ltree rtree txl
      else
        (* jump dflt if Binode - if not Binode it's not a special value *)
        match tree with
        | (Leaf _ | Node _) ->
          Binode (dflt, tree, tree_of xl)
        | Binode (x', ltree, rtree) ->
          if x' = dflt then
            follow_branch dflt ltree rtree xl
          else
            Binode (dflt, tree, tree_of xl)
  in follow xl tree
             

module X =
  struct
    type state = string
    type t = string tree

    module State =
      struct
        type t = string

        let to_string s = s
        let of_string s = Some s
      end

    let leaf s = Leaf s
    let node s t = Node (s, t)
    let binode s lt rt = Binode (s, lt, rt)
  end


module P = Parse.Make (X)


let read_newick chan =
  let lxbf = Lexing.from_channel chan in
  match P.tree Lex.read lxbf with
  | Some t -> t
  | None -> failwith "Empty input"


let of_newick s =
  let lxbf = Lexing.from_string s in
  P.tree Lex.read lxbf


module R = BatText


let to_rope to_string tree =
  let module U = BatUChar in
  let com = U.of_char ',' in
  let lpar = U.of_char '(' in
  let rpar = U.of_char ')' in
  let fleaf x = R.of_string (to_string x) in
  let fnode x r =
    let nr =
      r
      |> R.append_char rpar
      |> R.prepend_char lpar
    in
    x
    |> to_string
    |> R.of_string
    |> R.append nr
  in
  let fbi x lr rr =
    let nlr =
      lr
      |> R.append_char com
      |> R.prepend_char lpar
    in
    let nrr = R.append_char rpar rr in
    x
    |> to_string
    |> R.of_string
    |> R.append nrr
    |> R.append nlr
  in
  let r = convert_cps ~fleaf ~fnode ~fbi tree in
  R.append_char (U.of_char ';') r


(* fairly unreadable *)
let output_newick to_string chan tree =
  let out = BatIO.output_channel chan in
  let r = to_rope to_string tree in
  R.output_text out r ;
  BatIO.close_out out


let to_newick to_string tree =
  let r = to_rope to_string tree in
  R.to_string r
