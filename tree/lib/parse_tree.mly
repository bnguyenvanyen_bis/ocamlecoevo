%parameter<Tree : 
  sig
    type t
    type state
    module State : (Util.Interfaces.REPRABLE with type t = state)
    val leaf : state -> t
    val node : state -> t -> t
    val binode : state -> t -> t -> t
  end
>

%start <Tree.t option> tree

%%

tree:
  | t = state_tree(binode, snode, leaf, state)
      { t }
  ;


binode(node, state):
  | LPAREN ; lt = node ; COMMA ; rt = node ; RPAREN ; z = state
      { Util.Option.map3 Tree.binode z lt rt }
  ;


snode(node, state):
  | LPAREN ; t = node ; RPAREN ; z = state
      { Util.Option.map2 Tree.node z t }
  ;


leaf(state):
  | z = state
      { Util.Option.map Tree.leaf z }
  ;
