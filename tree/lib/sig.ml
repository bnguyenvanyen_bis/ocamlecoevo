(** Modules signatures used in the library (mostly for functors) *)

(** Minimum state to make a printable / parsable tree *)
module type STATE = Util.Interfaces.REPRABLE


(** Length *)
module type LENGTHED =
  sig
    type t

    (** [to_length x] is the length of [x]. *)
    val to_length : t -> float

    (** [with_length d x] is [x] with its length replaced by [d]. *)
    val with_length : float -> t -> t
  end


(** Timestamp + state *)
module type TIMED =
  sig
    type t

    (** A [t] has a time.
     *  [to_time x] is the time of the node associated with [x]. *)
    val to_time : t -> float

    (** A [t] can have its time changed.
     *  [at_time t x] is [x] with its time replaced by [t]. *)
    val at_time : float -> t -> t
  end


(** Label *)
module type LABEL =
  sig
    type t
    
    open Util.Interfaces

    include EQUALABLE with type t := t
    include COMPARABLE with type t := t
    include HASHABLE with type t := t
    include REPRABLE with type t := t
  
    val root_label : t
  end


module type LABELED =
  sig
    type t

    (** [label] to identify a state by. *)
    type label 

    module Label : (LABEL with type t = label)

    (** A state has a [label] associated with it.
     *  [to_label x] is the label associated with [x]. *)
    val to_label : t -> label
  end


(** Trait *)
module type TRAIT = Util.Interfaces.REPRABLE


module type TRAITED =
  sig
    type t

    type trait

    module Trait : (TRAIT with type t = trait)

    (** A [t] has a [trait] associated with it.
     *  [to_trait x] is the trait associated with [x]. *)
    val to_trait : t -> trait
  end


module type BINTREE =
  sig
    (** type along the branches *)
    type state

    (** type of the tree *)
    type t

    (** Base.tree equivalent (remove the type abstraction) *)
    val basic : t -> state Base.tree

    (** safe constructor functions *)

    (** [leaf x] is a leaf with state [x]. *)
    val leaf : state -> t

    (** [node x st] is a unary node with state [x] and subtree [st]. *)
    val node : state -> t -> t

    (** [binode x lt rt] is a binary node
     *  with left tree [lt] and right tree [rt]. *)
    val binode : state -> t -> t -> t

    (** [validate tree] returns a [t] or raises an [Assertion error] *)
    val validate : state Base.tree -> t

    (** [read_newick chan] reads a tree in (customized) Newick format
     *  from [chan]. *)
    val read_newick : in_channel -> t

    (** [output_newick] writes a tree in Newick format to [chan]. *)
    val output_newick : out_channel -> t -> unit

    (** [of_newick s] is some tree if [s] is its Newick representation,
     *  or [None]. *)
    val of_newick : string -> t option

    (** [to_newick tree] is the Newick representation of [tree]. *)
    val to_newick : t -> string

    (** flat representation *)
    val flat : t -> state list

    (** [leaves tree] is the list of leaves of [tree]
     *  and the paths leading up to them *)
    val leaves : ?i:int -> t -> (Path.t * state) list

    (** [map f tree] applies [f] to all the nodes of [tree] *)
    val map : (state -> state) -> t -> t

    (** [contour_iter ~fleft ~fmid ~fleaf ~fright tree]
      * iterates along the contour of [tree], calling
      * [fleft] going down,
      * [fmid] between nodes of a Binode, 
      * [fleaf] at a leaf, and
      * [fright] going back up *)
    val contour_iter :
      fleft : (state -> unit) ->
      fmid : (state -> unit) ->
      fleaf : (state -> unit) ->
      fright : (state -> unit) ->
      t ->
      unit

    (** [merge xll] is the forest obtained by progressively combining
     *  the lists in [xll] by identical values. *)
    val merge : state -> state list list -> t list

    (** [root_value tree] is the state at the first node of [tree]. *)
    val root_value : t -> state

    (** [find_node p tree] finds the only node of [tree]
     *  to satisfy the predicate [p].
     *  @raise Failure if there is more than one match in [tree]. *)
    val find_node : (state -> bool) -> t -> (Path.t * state) option

    (** [descend path tree] is the subtree at [path] in [tree]. *)
    val descend : Path.t -> t -> t

    (** [replace_subtree subtree path tree] is [tree]
     *  with [subtree] put at [path] *)
    val replace_subtree : t -> Path.t -> t -> t

    (** [genealogy path tree] is [tree] reduced to the genealogy of [path] *)
    val genealogy : Path.split -> t -> t

    (** [all_paths tree] is the list of paths to leaves in [tree] *)
    val all_paths : ?i:int -> t -> Path.t list

    (** [mrca tree pred1 pred2] is the most recent common ancestor of
     *  the unique leaf matching [pred1],
     *  and the unique leaf matching [pred2].
     *  It is [None] if those leaves are not found.
     *  @raise Failure if one matches more than once. *)
    val mrca : t -> (state -> bool) -> (state -> bool) -> state option
  end


(** Binary tree with branch lengths *)
module type LEN_TREE =
  sig
    type t
    type state

    module State :
      sig
        include STATE with type t = state
        include LENGTHED with type t := t
      end

    (** [branch_length tree] is the total branch length of [tree] *)
    val branch_length : t -> float
  end


module type HAS_LEN_TREE =
  sig
    type t
    type state

    (** type of the corresponding length-tree *)
    type t'

    (** module for the lengthed version *)
    module Len :
      sig
        include BINTREE with type t = t' and type state = state
        include LEN_TREE with type t := t and type state := state
      end

    (** [time_tree t tree] roots [tree] in [t]
        and computes the subsequent times from branch lengths *)
    val time_tree : float -> t' -> t

    (** [untime_tree tree] replaces the times by the lengths of branches
        leading up to the nodes. (and 0. at the root) *)
    val untime_tree : t -> t'

    (** [read_lengthed_newick chan] reads from the classical Newick format,
      * where branch lengths are given *)
    val read_lengthed_newick : in_channel -> t

    (** [output_lengthed_newick chan tree] outputs [tree] to [chan],
     *  with branch lengths *)
    val output_lengthed_newick : out_channel -> t -> unit
  end


module type LABEL_TREE =
  sig
    type t
    type state
    type label

    (** [find_label tree id] is the path and state
     *  of the only node with label [id],
     *  or [None] if the label is absent.
     *  @raise Failure if there is more than one. *)
    val find_label : t -> label -> (Path.t * state) option

    (** [label_mrca tree id1 id2] is the state of the most recent
     *  common ancestor of the leaves corresponding to [id1] and [id2],
     *  or [None] if no such leaves exist in [tree].
     *  @raise Failure if there is more than one match. *)
    val label_mrca : t -> label -> label -> state option
  end



(** Time-embedded binary tree *)
module type TIME_TREE =
  sig
    type t
    type state

    (** [tip_value tree] is the state at the last (in time) leaf of tree *)
    val tip_value : t -> state

    (** [left_fold f tree y] folds [f] over [tree] with initial value [y],
     *  going left first. *)
    val left_fold :
      (state -> 'a -> 'a) ->
      t ->
      'a ->
        'a

    (** count stuff through time *)

    val count_leaves : t -> int Jump.cadlag
    val count_leaves_backwards : t -> int Jump.cadlag
    val count_coals : t -> int Jump.cadlag
    val count_coals_backwards : t -> int Jump.cadlag

    (** [nlineages tree] is the number of lineages through time of tree *)
    val nlineages : t -> int Jump.cadlag
    val nlineages_backwards : t -> int Jump.cadlag

    (** [branch_length tree] is the total branch length of [tree] *)
    val branch_length : t -> float

    (** [cut_before t0 tree] is the forest obtained by keeping
     *  the lineages of [tree] only after [t0]. *)
    val cut_before : float -> t -> t list

    (** [cut_after tf tree] is [tree] stopped at [tf] *)
    val cut_after : float -> t -> t

    (** [cut_between t0 tf tree] is the forest obtained by keeping
     *  the lineages of [tree] only between [t0] and [tf].
     *  Applies {!cut_after} then {!cut_before}. *)
    val cut_between : float -> float -> t -> t list

    (** [ultra_restr tf tree] is the ultrametric tree obtained
        by keeping only the genealogy of the individuals alive at tf *)
    val ultra_restr : float -> t -> t
  end


(** RGB color representation *)
type draw_rgb = float * float * float

(** colored point in the plane *)
type draw_point = {
  x : float ;
  y : float ;
  c : draw_rgb
}


module type PLOT_TREE =
  sig
    type t
    type state

    (** [offset_tree color ttoy w mg tree] is a draw_point tree
     *  obtained by coloring the [tree],
     *  scaling the y-axis with [ttoy],
     *  with width [w] and margin [mg] around it. *)
    val offset_tree :
      (state -> draw_rgb) ->
      (float -> float) ->
      float ->
      float ->
      t ->
      draw_point Base.tree

    (** [plot cr color ttoy width margin tree] plots the [tree]
     *  as a SVG using Cairo. *)
    val plot :
      Cairo.context ->
      (state -> draw_rgb) ->
      (float -> float) ->
      float ->
      float ->
      t ->
      unit
  end
