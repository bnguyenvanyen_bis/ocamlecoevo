open Sig

module type INPUT = Labeltimed.INPUT


(** Labeled time-embedded binary tree with plotting and distances *)
module type S =
  sig
    include BINTREE

    include TIME_TREE with type t := t and type state := state
    include LABEL_TREE with type t := t and type state := state
    include HAS_LEN_TREE with type t := t and type state := state
    include PLOT_TREE with type t := t and type state := state

    module State : (INPUT with type t = state and type label = label)

    (** [distance_kc alph tree1 tree2] is the alpha-Kendall-Colijn distance
     *  between [tree1] and [tree2] *)
    val distance_kc :
      float ->
      t ->
      t ->
      float

    (** [distance_bhv tree1 tree2] is the BHV distance
      *  between [tree1] and [tree2]. *)
    val distance_bhv :
      t ->
      t ->
      float
  end


module Make
  (State : INPUT) :
  (S with type state = State.t
      and type label = State.label) =
  struct
    module T = Labeltimed.Make (State)

    module D = Draw.Complement (T)
    module KC = Kendall_colijn.Complement (T)
    module BHV = Billera_holmes_vogtmann.Complement (T)

    include T

    let offset_tree = D.offset_tree
    let plot = D.plot
    let distance_kc = KC.distance
    let distance_bhv = BHV.distance
  end
