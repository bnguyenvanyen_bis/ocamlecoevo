(* Plot a tree in Newick format in SVG. *)
module Ustate =
  struct
    type t = float * unit
    let to_time (t, _) = t
    let at_time t (_, ()) = (t, ())
    let to_string (t, _) = Printf.sprintf ":%.2f" t
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun _ t -> t) with
      | t ->
          Some (t, ())
      | exception Scanf.Scan_failure _ ->
          None
  end

module T = Tree.Draw.Complement (Tree.Timed.Make (Ustate))

let out_svg ?line s =
  let n = String.length s in
  let suffix = String.sub s (n - 7) 7 in
  assert (suffix = ".newick") ;
  let s' = String.sub s 0 (n - 7) in
  match line with
  | None ->
      s' ^ ".tree.svg"
  | Some i ->
      Printf.sprintf "%s.tree.%i.svg" s' i

let add_line lr s =
  lr := (int_of_string s) :: !lr


let tree_of_line ~width ~height chan_in chan_out =
  let tree = T.read_lengthed_newick chan_in in
  let tf, _ = T.tip_value tree in
  let color _ = Tree.Draw.black in
  let ttoy t = t *. height /. tf in
  let stream = output_string chan_out in
  let surface = Cairo.SVG.create_for_stream stream ~w:width ~h:height in
  let cr = Cairo.create surface in
  Cairo.set_line_width cr 0.1 ;
  Cairo.set_line_join cr Cairo.JOIN_ROUND ;
  T.plot cr color ttoy width 1. tree ;
  Cairo.Surface.finish surface

let input_r = ref None
let output_r = ref None
let lines_r = ref []

let w_r = ref 120.
let h_r = ref 120.

let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin.") ;
  ("-out", Arg.String (fun s -> output_r := Some s),
   "Path to output. Default stdout,
    or input filename ending in '.tree.svg'.") ;
  ("-w", Arg.Set_float w_r,
   "Figure width.") ;
  ("-h", Arg.Set_float h_r,
   "Figure height.") ;
  ("-lines", Arg.Rest (add_line lines_r),
   "Lines to parse. By default only the first line.")
]

let anon_fun s = Printf.printf "Ignored anonymous argument %s" s
let usage_msg = "  Plot the binary tree at 'in' all in black,
                 ignoring the node information, then save it at 'out'."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chans_out =
      Lift_arg.open_chans out_svg !input_r !output_r !lines_r
    in
    let width = !w_r in
    let height = !h_r in
    let rec consume i = function
      | [] ->
          ()
      | (i', chan_out) :: tl ->
          (* [i] is last line consumed,
           * need to consume until we reach i' *)
          for j = i + 1 to i' - 1 do
            print_int j ;
            ignore (input_line chan_in)
          done ;
          tree_of_line ~height ~width chan_in chan_out ;
          consume i' tl ;
          close_out chan_out
    in
    consume 0 chans_out ;
    close_in chan_in
  end ;;

main ()
