(* Number of leaves, coalescences and lineages backwards through time *)
module Ustate =
  struct
    type t = float * unit
    let to_time (t, _) = t
    let at_time t (_, ()) = (t, ())
    let to_string (t, _) = Printf.sprintf ":%.4f" t
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun _ t -> t) with
      | t ->
          Some (t, ())
      | exception Scanf.Scan_failure _ ->
          None
  end

module T = Tree.Timed.Make (Ustate)

let header = ["t" ; "nleaves" ; "ncoals" ; "nlineages"]

let out_csv ?line s =
  let n = String.length s in
  let suffix = String.sub s (n - 7) 7 in
  assert (suffix = ".newick") ;
  let s' = String.sub s 0 (n - 7) in
  match line with
  | None ->
      s' ^ ".data.nlclft.csv"
  | Some i ->
      Printf.sprintf "%s.data.nlclft.%i.csv" s' i


let add_line lr s =
  lr := (int_of_string s) :: !lr

let sof = string_of_float
let soi = string_of_int

let nlclft_of_line chan_in chan_out =
  let tree = T.read_lengthed_newick chan_in in
  let leaves = T.count_leaves_backwards tree in
  let coals = T.count_coals_backwards tree in
  let nlineages = T.nlineages_backwards tree in 
  let concat = Jump.map2 (fun (l, c) nl -> (l, c, nl))
    (Jump.map2 (fun l c -> (l, c)) leaves coals) nlineages
  in
  let csv = header :: List.map (fun (t, (lv, c, nl)) ->
    [sof t ; soi lv ; soi c ; soi nl]
  ) concat
  in
  let csv_chan = Csv.to_channel chan_out in
  Csv.output_all csv_chan csv



let input_r = ref None
let output_r = ref None
let lines_r = ref []

let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Input file -- default stdin.") ;
  ("-out", Arg.String (fun s -> output_r := Some s),
   "Output file -- default stdout,
    or input filename ending in '.data.nlclft.csv'.") ;
  ("-lines", Arg.Rest (add_line lines_r),
   "Lines to parse. By default only the first line.")
]

let anon_fun s = Printf.printf "Ignored anonymous argument %s" s
let usage_msg = "  Read the binary tree at 'in', ignoring the node information,
                  then return the number of leaves, coalescences and lineages
                  backwards through time at 'out'."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chans_out =
      Lift_arg.open_chans out_csv !input_r !output_r !lines_r
    in
    let rec consume i = function
      | [] ->
          ()
      | (i', chan_out) :: tl ->
          (* [i] is last line consumed,
           * need to consume until we reach i' *)
          for j = i + 1 to i' - 1 do
            print_int j ;
            ignore (input_line chan_in)
          done ;
          nlclft_of_line chan_in chan_out ;
          consume i' tl ;
          close_out chan_out
    in
    consume 0 chans_out ;
    close_in chan_in
  end ;;

main ()
