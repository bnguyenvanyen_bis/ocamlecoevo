module A = BatArray
module H = BatHashtbl
module Yb = Yojson.Basic
module Ybu = Yb.Util

module B = Impl.Base
module T = Zen__Pop.Trait


module Make (P : Zen__Pop.COMPLEX) =
  struct
    module S = Symbolic.Make (Impl.Make (P))
    module R = Rates.Make (P)

    module Ar_parse = Atom_rate_parse.Make (S)
    module Mu_parse = Mutate_parse.Make (S)

    open S

    type wrap_choose_modif =
      Exim : 'a choose * 'a Modif.symb -> wrap_choose_modif
    type wrap_event = Exe : 'a event -> wrap_event

    let read_value h s =
      match (s |> float_of_string) with
      | x ->
          x |> Value.literal
      | exception (Failure _) -> (* "float_of_string" *)
          Value.bind h s

    let read_pos_value h s =
      match (s |> float_of_string) with
      | x ->
          x |> Pos.of_float |> Value.literal
      | exception (Failure _) -> (* "float_of_string" *)
          Value.bind_pos h s


    let rec symb_choose_modif h = Modif.(
      function
      | [] ->
          Exim (Nil, Nil)
      | (g, json) :: tl ->
          (match symb_choose_modif h tl with Exim (choose, modif) ->
            let s = Ybu.to_string json in
            let read = Scanf.sscanf s "%s" (fun s' -> s') in
            match g, read with
              | (("" | "_"), ("new_nonid" | "new_id")) ->
                  failwith "Not_implemented"
              | g, "ignore" ->
                  Exim (Group (g, choose), Ignore modif)
              | g, "remove" ->
                  Exim (Group (g, choose), Remove modif)
              | g, "copy" ->
                  Exim (Group (g, choose), Copy modif)
              | g, "mutate" ->
                  let module Parse = Mu_parse (struct
                    let params = h
                  end) in
                  let lxbf = Lexing.from_string s in
                  let fm = Parse.mutate Lex.read lxbf in
                  Exim (Group (g, choose), fm modif)
              | _ ->
                  invalid_arg "Invalid group / effect"
          )
    )


    let symb_trait_rate h s = R.(
      let module Parse = Value_parse.Make (struct
        let params = h
      end) in
      match Scanf.sscanf s "%s" (fun s -> s) with
      | "single_dim_abs" ->
          Scanf.sscanf s "%_s (%i)"
          (fun d -> (`Single (Single.dim_abs "indiv0" d), Bound.Single.dim_abs d))
      | "single_dim_normal" ->
          Scanf.sscanf s "%_s (%i, %s, %s)"
          (fun d sm sv ->
            let m = read_value h sm in
            let lxbf = Lexing.from_string sv in
            let v = Parse.value Lex.read lxbf in
            (`Single (Single.dim_normal "indiv0" d m v),
             Bound.Single.dim_normal v))
      | "double_dim_normal" ->
          Scanf.sscanf s "%_s (%i, %s, %s)"
          (fun d sm sv ->
            let m = read_value h sm in
            let lxbf = Lexing.from_string sv in
            let v = Parse.value Lex.read lxbf in
            (`Double (Double.dim_normal "indiv0" "indiv1" d m v),
             Bound.Double.dim_normal v))
      | _ ->
          invalid_arg "Invalid trait rate keyword"
    )


    let symb_trait_rate_opt h =
      function
      | None ->
          None
      | Some s ->
          Some (symb_trait_rate h s)


    let symb_atom_rate htb s =
      let module Parse = Ar_parse (struct
        let params = htb
      end)
      in
      let lxbf = Lexing.from_string s in
      Parse.rate Lex.read lxbf


    type none = unit
    type single = id indiv * unit
    type double = id indiv * (id indiv * unit)

    type wrap_trait_rate = [
      | `None of none Trait.rate
      | `Single of single Trait.rate
      | `Double of double Trait.rate
    ]


    let validate_trait_rate :
      type a.
      a choose ->
      (wrap_trait_rate * Bound.rate) option ->
        a Trait.rate  * Bound.rate =
      fun indivs wtrbndo ->
      match wtrbndo with
      | None ->
          R.None.one, R.Bound.None.one
      | Some (wtr, bnd) ->
          (match indivs, wtr with
           | Nil, `None tr ->
               tr, bnd
           | Group (_, Nil), `Single tr ->
               tr, bnd
           | Group (_, Group (_, Nil)), `Double tr ->
               tr, bnd
           | _ ->
               failwith "Invalid indivs / trait rate combination"
          )


    let symb_atom_rate_opt h =
      function
      | None ->
          Atom.Holling_I { a = Literal Pos.one }
      | Some s ->
          symb_atom_rate h s


    let symb_event params json =
      let name =
        json
        |> Ybu.member "name"
        |> Ybu.to_string
      in
      match
        json
        |> Ybu.member "indivs"
        |> Ybu.to_assoc
        (* the order needs to be the same as in trait_rate *)
        |> List.rev
        |> symb_choose_modif params
      with Exim (indivs, modif) ->
      let trbndo =
        json
        |> Ybu.member "trait_rate"
        |> Ybu.to_string_option
        |> symb_trait_rate_opt params
      in
      let trait_rate, bound_rate =
        validate_trait_rate indivs trbndo
      in
      let atom_rate =
        json
        |> Ybu.member "atom_rate"
        |> Ybu.to_string_option
        |> symb_atom_rate_opt params
      in
        Exe { name ; indivs ; trait_rate ; bound_rate ; atom_rate ; modif }


    let rec symb_events params =
      function
      | [] ->
          Nil_event
      | json :: tl ->
          (match symb_event params json with Exe ev ->
           And_event (ev, symb_events params tl))


    let symb_model params json =
      let groups_id =
        json
        |> Ybu.member "groups"
        |> Ybu.member "id"
        |> Ybu.to_list
        |> List.map Ybu.to_string
      in
      let groups_nonid =
        json
        |> Ybu.member "groups"
        |> Ybu.member "nonid"
        |> Ybu.to_list
        |> List.map Ybu.to_string
      in
      let events =
        json
        |> Ybu.member "events"
        |> Ybu.to_list
        |> symb_events params
      in
      let groups = (groups_id, groups_nonid) in
      { groups ; events }
  end


let symb_mode =
  function
  | None ->
      `Demography
  | Some "demography" ->
      `Demography
  | Some "genealogy" ->
      `Genealogy
  | _ ->
      invalid_arg "Invalid mode"



let read_out =
  function
  | None ->
      "zen.run.out"
  | Some s ->
      s

let (|->) xo f =
  match xo with
  | None ->
      None
  | Some x ->
      Some (f x)


let (|<) xo x =
  match xo with
  | None ->
      x
  | Some x' ->
      x'


let read_float_list s =
  let rec f xs s =
    match s with
    | "]" ->
        List.rev xs
    | _ ->
        Scanf.sscanf s "; %f %s" (fun x s' -> f (x :: xs) s')
  in Scanf.sscanf s "[ %f %s" (fun x s' -> f [x] s')


let read_init json =
  let init_id =
    json
    |> Ybu.member "init"
    |> Ybu.to_list
    |> Ybu.filter_map (fun json ->
      let nb =
        json
        |> Ybu.member "number"
        |> Ybu.to_int_option
      in
      let group =
        json
        |> Ybu.member "group_id"
        |> Ybu.to_string_option
      in
      let v =
        json
        |> Ybu.member "trait"
        |> Ybu.to_string
        |> read_float_list
        |> A.of_list
      in
      match group with
      | None ->
          None
      | Some g ->
          Some (nb, T.Id (g, v))
    )
  in
  let init_nonid =
    json
    |> Ybu.member "init"
    |> Ybu.to_list
    |> Ybu.filter_map (fun json ->
      let nb =
        json
        |> Ybu.member "number"
        |> Ybu.to_int_option
      in
      json
      |> Ybu.member "group_nonid"
      |> Ybu.to_string_option
      |-> (fun g -> (nb, T.Nonid g))
    )
  in (init_id, init_nonid)


let validate_init (init_id, _) =
  let _ = List.fold_left (fun ko (_, T.Id (_, v)) ->
    let k' = A.length v in
    (match ko with
    | None ->
        ()
    | Some k ->
        assert (k = k')
    ) ;
    Some k'
  ) None init_id
  in ()


type ctxz = {
  mode : [`Demography | `Genealogy] ;
  params : (string, float) H.t ;
  seed : int option ;
  tf : float ;
  init : (int option * Pop.Sig.id T.t) list
       * (int option * Pop.Sig.nonid T.t) list ;
  nindivs : int ;
  dt : float option ;
  out : string ;
}


let contextualize json =
  (* this should be a way to chose the Pop module. How ? *)
  let mode =
    json
    |> Ybu.member "mode"
    |> Ybu.to_string_option
    |> symb_mode
  in
  let seed =
    json
    |> Ybu.member "seed"
    |> Ybu.to_int_option
  in
  let tf =
    json
    |> Ybu.member "tf"
    |> Ybu.to_float
  in
  let dt =
    json
    |> Ybu.member "dt"
    |> Ybu.to_float_option
  in
  (* FIXME also need something for initialization = 
   * need to read in arrays somehow *)
  let init = read_init json in
  validate_init init ;
  let nindivs =
    json
    |> Ybu.member "nindivs"
    |> Ybu.to_int_option
    |< 97
  in
  let out =
    json
    |> Ybu.member "out"
    |> Ybu.to_string_option
    |> read_out
  in
  let params = H.create 10 in
  json
  |> Ybu.member "params"
  |> Ybu.to_assoc
  |> List.iter (fun (name, js) ->
      H.add params name (Ybu.to_float js)
  ) ;
  { mode ; params ; seed ; tf ; init ; nindivs ; dt ; out }
