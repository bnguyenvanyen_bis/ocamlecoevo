{

exception SyntaxError of string

module P = Parse_intl


}

let digit = ['0'-'9']

rule read =
  parse
  | digit    { P.INT (Lexing.lexeme lexbuf) }
  | ';'      { P.SEMIC }
  | eof      { P.EOF }
  | _        { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
