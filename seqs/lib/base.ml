(* sequences implemented as a thin wrapper around ropes from BatText *)

open Sig

type t = seq

let empty = BatText.empty

let inc n k =
  match n, k with
  | P n, L k -> P I.Pos.Op.(n + k)

let pos_to_int = function P i -> (I.to_int i)

let len_to_int = function L k -> (I.to_int k)

let ofc c = BatUChar.of_char c

let length seq : 'a U.posint = I.of_int_unsafe (BatText.length seq)

let is_empty seq = BatText.is_empty seq

let get ~n seq =
  match n with
  | P n -> BatText.get seq (I.to_int n)


let set ~n ~x seq =
  match n with P n -> BatText.set seq (I.to_int n) x


let choose_site rng seq =
  let n = P (Util.rand_int ~rng (length seq)) in
  (n, get ~n seq)


let of_rope ?(k=4) rope = ignore k ; rope


let of_string s =
  let rope = BatText.of_string s in
  (* need to count chars *)
  of_rope rope


let random ?(chars=['A';'T';'C';'G']) ?(rng=Random.get_state ()) k =
  let n = I.of_int_unsafe (List.length chars) in
  let f _ =
    let i = Util.rand_int ~rng n in
    ofc (List.nth chars (I.to_int i))
  in
  let rope = BatText.init k f in
  of_rope ~k rope


let equal seq seq' = BatText.equal seq seq'


let to_string seq = BatText.to_string seq


let to_char_list seq = List.map BatUChar.char_of (BatText.explode seq)


let to_string_list seq =
  let s = to_string seq in
  let rec f acc i =
    match i with
    | 0 -> ((String.sub s 0 1) :: acc)
    | _ -> f ((String.sub s i 1) :: acc) (i - 1)
  in f [] (String.length s - 1)


let of_nuc_list nc =
  let rope = BatText.implode (List.map Dna.to_uchar nc) in
  of_rope rope

let to_nuc_list seq = List.map Dna.of_char (to_char_list seq)

let append = BatText.append


(* Don't copy rope *)
let copy seq = seq


(* subseq ~n ~n' is the subsequence from seq from n (inclusive) to n' (exclusive) *)
let sub ~n ~k seq =
  BatText.sub seq (pos_to_int n) (len_to_int k)


let remove ~n ~k seq =
  BatText.remove (pos_to_int n) (len_to_int k) seq


let insert ~n ~r seq =
  BatText.insert (pos_to_int n) r seq


(* duplicate the sequence between n and n' at n'' *)
let duplicate ~n ~k ~n' seq =
  let r = BatText.sub seq (pos_to_int n) (len_to_int k) in
  insert ~n:n' ~r seq

(* repeat ~n ~n' seq repeats (inserts) the sequence from n to n' just after *)
let repeat ~n ~k seq =
  let n' = inc n k in
  duplicate ~n ~k ~n' seq


(* invert ~n ~n' seq inverts the subsequence from n (incl) to n' (excl) *)
let invert ~n ~k seq =
  let n = pos_to_int n in
  let k = len_to_int k in
  let invr =
    BatText.of_enum @@ BatText.backwards (BatText.sub seq n k)
  in
  BatText.splice seq n k invr


let patch (sd : seqdiff) (seq : seq) =
  match sd with
  | Not ->
    seq
  | Sub (n, _, x') ->
    set ~n ~x:x' seq
  | Del (n, s) ->
    let k = L (length s) in
    remove ~n ~k seq
  | Ins (n, s) ->
    insert ~n ~r:s seq
  | Dup (n, n', k) ->
    duplicate ~n ~k ~n' seq
  | Undup (_, n', k) ->
    remove ~n:n' ~k seq
  | Inv (n, k) ->
    invert ~n ~k seq


let equality seq seq' =
  (* for now we put it in a bool array array -- something in Owl ? *)
  let l = I.to_int (length seq) in
  let l' = I.to_int (length seq') in
  let a = Array.make_matrix l' l false in
  let fill k' c' k c =
    if c = c' then
      a.(k').(k) <- true
  in
  BatText.iteri (fun k' c' -> BatText.iteri (fill k' c') seq) seq' ;
  a


let homology seq sdl =
  let iti = I.to_int in
  let n = iti (length seq) in
  (* how do we find homology ? *)
  let a = Array.make_matrix n n false in
  (* fill the "diagonal" with true *)
  Array.iteri (fun i suba -> try (suba.(i) <- true) with Invalid_argument _ -> ()) a ;
  (* probably find the true as we step through the sdl *)
  let del j k a =
    let n' = Array.length a in
    Array.append (Array.sub a 0 j) (Array.sub a (j + k) (n' - j - k))
  in
  let ins j k a =
    let n' = Array.length a in
    Array.append
      (Array.append 
        (Array.sub a 0 j)
        (Array.make_matrix k n false))
      (Array.sub a j (n' - j))
  in
  let dup j j' k a =
    let n = Array.length a in
    Array.append
      (Array.append
        (Array.sub a 0 j')
        (Array.sub a j k))
      (Array.sub a j' (n - j'))
  in
  let inv j k a =
    let rec f i =
      match i with
      | 0 -> ()
      | _ ->
        (* exchange columns (j + i - 1) and (j + k - i) *)
        let col = a.(j + i - 1) in
        a.(j + i - 1) <- a.(j + k - i) ;
        a.(j + k - i) <- col ;
        f (i - 1)
    in f (k / 2)
  in
  let record_homology (a, seq') sd =
    let seq' = patch sd seq' in
    match sd with
    | Not ->
      (a, seq')
    | Sub (P _, _, _) ->
      (a, seq')
    | Del (P j, s) ->
      let k = length s in
      (* assert (equal s (subseq ~n:j ~n':(j+k) seq) ; *)
      (del (iti j) (iti k) a, seq')
    | Ins (P j, s) ->
      let k = length s in
      (ins (iti j) (iti k) a, seq')
    | Dup (P j, P j', L k) ->
      (dup (iti j) (iti j') (iti k) a, seq')
    | Undup (P _, P j', L k) ->
      (del (iti j') (iti k) a, seq')
    | Inv (P j, L k) ->
      inv (iti j) (iti k) a ;
      (a, seq')
  in
  let a, seq' = List.fold_left record_homology (a, copy seq) sdl in
  (seq, seq', a)


let csv_of seq seq' a =
  let n = I.to_int (length seq) in
  let n' = I.to_int (length seq') in
  assert ((Array.length a = n') && (Array.length a.(0) = n)) ;
  let l = Array.to_list a in
  let l = List.map Array.to_list l in
  (* then replace true and false by '0' and '1' *)
  let l = List.map (fun sl -> List.map (fun b -> if b then "1" else "0") sl) l in
  (* then put the symbols in *)
  let first_col = "" :: (to_string_list seq) in
  let first_line = to_string_list seq' in
  first_col :: (List.map2 (fun s sl -> s :: sl) first_line l)


let fold = BatText.fold
