(* internal modules *)
module Sig = Sig
module Io = Io
module Diff = Diff
module Dna = Dna
module Prot = Prot

(* also make some functions from Base directly available *)
include Base
