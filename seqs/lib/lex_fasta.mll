{
open Lexing

exception SyntaxError of string

module type PARSER =
  sig
    type token =
      | OTHER of string
      | NEWLINE
      | EOF
      | CHEVRON
      | BLANKLINE
  end

module type LEX =
  sig
    type t
    val read : lexbuf -> t
  end

module Make (P : PARSER) : (LEX with type t = P.token) =
  struct
    type t = P.token
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let blankline = newline newline
let digit = ['0' - '9']
let letter = ['A'-'Z' 'a'-'z']
let symbol = ['.' ',' ';' ':' '-' '_' '/']
(* nothing or any of digit|letter|symbol or newlines in the middle *)
let other = (digit | letter | symbol)*
(* let seq = ['A' 'T' 'C' 'G' 'U' 'a' 't' 'c' 'g' 'u']* *)

rule read =
  parse
  | white     { read lexbuf }
  | blankline { P.BLANKLINE }
  | newline   { P.NEWLINE }
  | other     { P.OTHER (Lexing.lexeme lexbuf) }
  | '>'       { P.CHEVRON }
  | eof       { P.EOF }
  | _         { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }

{
  end
}
