open Sig

type t = nuc = A | T | C | G

let nucleotides = [ A ; T ; C ; G ]

let of_char c =
  if (c = 'A') || (c = 'a') then
    A
  else if (c = 'T') || (c = 't') then
    T
  else if (c = 'C') || (c = 'c') then
    C
  else if (c = 'G') || (c = 'g') then
    G
  else
    invalid_arg "invalid character"

let of_uchar uc = of_char @@ BatUChar.char_of @@ uc

let to_char n =
  match n with
  | A -> 'A'
  | T -> 'T'
  | C -> 'C'
  | G -> 'G'

let to_uchar n = BatUChar.of_char @@ to_char @@ n

let to_int x =
  match x with
  | A -> 0
  | T -> 1
  | C -> 2
  | G -> 3

let of_int k =
  match (k mod 4) with
  | 0 -> A
  | 1 -> T
  | 2 -> C
  | 3 -> G
  | _ -> failwith "impossible after mod"

