{

exception SyntaxError of string

module P = Parse_seqdiff


}

let digit = ['0'-'9']+
let seq = ['A'-'Z' 'a'-'z']+

rule read =
  parse
  | '_'      { P.UNDER }
  | seq      { P.SEQ (Lexing.lexeme lexbuf) }
  | ';'      { P.SEMIC }
  | '>'      { P.RCHEVR }
  | '<'      { P.LCHEVR }
  | digit    { P.INT (Lexing.lexeme lexbuf) }
  | eof      { P.EOF }
  | '['      { P.BRACK }
  | '|'      { P.BAR }
  | _        { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
