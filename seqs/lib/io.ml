open Sig

module U = BatUChar

module Intparse =
  struct
    type t = int
    let parse s = Scanf.sscanf s "%i" (fun i -> i)
  end


module Parse = Parse_fasta.Make (Intparse)
module Lex = Lex_fasta.Make (Parse)


let of_string = Base.of_string

let to_string = Base.to_string

(* FIXME rewrite this without Printf ? *)
let string_of_seqdiff sd =
  let module B = Buffer in
  match sd with
  | Not ->
      "_"
  | Sub (P n, c, c') ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_char b '|' ;
      B.add_char b (U.char_of c) ;
      B.add_char b '>' ;
      B.add_char b (U.char_of c') ;
      let s = B.contents b in
      B.clear b ;
      s
  | Ins (P n, s) ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_char b '>' ;
      B.add_string b (Base.to_string s) ;
      B.add_char b '<' ;
      let s = B.contents b in
      B.clear b ;
      s
  | Del (P n, s) ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_char b '<' ;
      B.add_string b (Base.to_string s) ;
      B.add_char b '>' ;
      let s = B.contents b in
      B.clear b ;
      s
  | Dup (P n, P n', L k) ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n')) ;
      B.add_string b ">[" ;
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_char b ';' ;
      B.add_string b (string_of_int (I.to_int n + I.to_int k)) ;
      B.add_string b "[<" ;
      let s = B.contents b in
      B.clear b ;
      s
  | Undup (P n, P n', L k) ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n')) ;
      B.add_string b "<[" ;
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_char b ';' ;
      B.add_string b (string_of_int (I.to_int n + I.to_int k)) ;
      B.add_string b "[>" ;
      let s = B.contents b in
      B.clear b ;
      s
  | Inv (P n, L k) ->
      let b = B.create 10 in
      B.add_string b (string_of_int (I.to_int n)) ;
      B.add_string b "<<" ;
      B.add_string b (string_of_int (I.to_int n + I.to_int k)) ;
      let s = B.contents b in
      B.clear b ;
      s


let seqdiff_of_string s =
  let lxbf = Lexing.from_string s in
  Parse_seqdiff.input Lex_seqdiff.read lxbf


let fasta_of_assoc chan iseql =
  (* output the idents/trait correspondance in nexus format *)
  let out = BatIO.output_channel chan in
  let f (i, seq) =
    output_string chan ">" ;
    output_string chan (string_of_int i) ;
    output_string chan "\n" ;
    BatText.output_text out seq ;
    output_string chan "\n\n"
  in List.iter f iseql ;
  BatIO.close_out out


let assoc_of_fasta chan =
  let lxbf = Lexing.from_channel chan in
  Parse.input Lex.read lxbf


module Stamparse =
  struct
    type t = int * float

    let parse s = Scanf.sscanf s "%i:%f" (fun i t -> (i, t))
  end


let stamped_fasta_of_assoc chan itseql =
  let out = BatIO.output_channel chan in
  let f (i, t, seq) =
    output_string chan ">" ;
    output_string chan (string_of_int i) ;
    output_string chan ":" ;
    output_string chan (string_of_float t) ;
    output_string chan "\n" ;
    BatText.output_text out seq ;
    output_string chan "\n\n"
  in List.iter f itseql ;
  BatIO.close_out out


module Stp_parse = Parse_fasta.Make (Stamparse)
module Stp_lex = Lex_fasta.Make (Stp_parse)


let assoc_of_stamped_fasta chan =
  let lxbf = Lexing.from_channel chan in
  let it_seql = Stp_parse.input Stp_lex.read lxbf in
  List.map (fun ((i, t), seq) -> (i, t, seq)) it_seql
