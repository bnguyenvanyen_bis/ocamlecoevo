open Sig

type t = seqdiff

let copy =
  function
  | Not ->
      Not
  | Sub _ as sd ->
      sd
  | Del (n, s) ->
      Del (n, Base.copy s)
  | Ins (n, s) ->
      Ins (n, Base.copy s)
  | Dup _ as sd ->
      sd
  | Undup _ as sd ->
      sd
  | Inv _ as sd ->
      sd


let undo =
  function
  | Not ->
      Not
  | Sub (n, x, x') ->
      Sub (n, x', x)
  | Del (n, s) ->
      Ins (n, s)
  | Ins (n, s) ->
      Del (n, s)
    (* Is that ok for Dup ? becomes ambiguous *)
  | Dup (n, n', k) ->
      Undup (n, n', k)
  | Undup (n, n', k) ->
      Dup (n, n', k)
  | Inv (n, k) ->
      Inv (n, k)


let undo_list sdl = List.map undo (List.rev sdl)


let diff sdl sdl' =
  (* sdl and sdl' should start from the same reference point
   * otherwise diffdiff just assumes they start at the right point *)
  (* cut the common part of sdl and sdl' *)
  let rec cut l l' =
    match l, l' with
    | x :: tl, x' :: tl' when x = x' -> cut tl tl'
    | _, _ -> l, l'
  in
  let csdl, csdl' = cut sdl sdl' in
  (undo_list csdl) @ csdl'


