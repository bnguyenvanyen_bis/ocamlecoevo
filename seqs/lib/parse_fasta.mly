%parameter<S :
  sig
    type t
    val parse : string -> t
  end
>

%token <string> OTHER
%token NEWLINE
%token EOF
%token CHEVRON
%token BLANKLINE

%start <(S.t * Sig.seq) list> input
%%

input:
  | CHEVRON ; h = head ; NEWLINE ; x = seq ; BLANKLINE ; v = input
  { (h, x) :: v }
  | EOF
  { [] }
  ;

head:
  | h = OTHER
  { S.parse h }
  ;

seq:
  | x = OTHER
  { Base.of_string x }
  | x = OTHER ; NEWLINE ; y = seq
  { Base.append (Base.of_string x) y }
