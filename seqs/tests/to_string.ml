open Sig


let%expect_test _ =
  P.printf "%s" (Seqs.Io.to_string (Seqs.Io.of_string "ATCG")) ;
  [%expect{| ATCG |}]
