(* Load a fasta file, for timestamped sequences,
 * and only keep the data between time [t0] and time [tf]. *)

let cut_fasta s =
  (* drops s's separator, and replaces the end by '_data.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 6) 6 in
  assert (suffix = ".fasta") ;
  let s' = String.sub s 0 (n - 6) in
  s' ^ ".cut.fasta"

let input_r = ref None
let output_r = ref None
let t0_r = ref 0.
let tf_r = ref infinity

let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin.") ;
  ("-out", Arg.String (fun s -> output_r := Some s),
   "Path to output. Default stdout, then if an input filename is given,
    same filename, suffixed by '.cut.fasta'.") ;
  ("-t0", Arg.Set_float t0_r,
   "Time to start from.") ;
  ("-tf", Arg.Set_float tf_r,
   "Time to stop at.") ;
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Filter a timestamped FASTA file to between [t0] and [tf]."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chan_out =
      match !input_r, !output_r with
      | None, None ->
        stdin, stdout
      | Some s, Some s' ->
        (open_in s), (open_out s')
      | Some s, None ->
        (open_in s), (open_out (cut_fasta s))
      | None, Some s' ->
        stdin, (open_out s')
    in
    let t0 = !t0_r in
    let tf = !tf_r in
    let assoc = Seqs.Io.assoc_of_stamped_fasta chan_in in
    let cut_assoc = List.filter (fun (_, t, _) ->
      (t0 <= t) && (t <= tf)
    ) assoc
    in
    Seqs.Io.stamped_fasta_of_assoc chan_out cut_assoc ;
    close_in chan_in ;
    close_out chan_out
  end;;

main ()
