open Cmdliner
open Sig


let int_opt ~doc ~docv ~arg =
  Arg.(value
     & opt (some int) None
     & info [arg] ~docv ~doc
  )


let float_opt ~doc ~docv ~arg =
  Arg.(value
     & opt (some float) None
     & info [arg] ~docv ~doc
  )


let positive_opt ~doc ~docv ~arg =
  Arg.(value
     & opt (some (Util.Term.positive ())) None
     & info [arg] ~docv ~doc
  )


let arg ~cv ~doc ~arg ~docv ~(f : 'b -> 'a -> 'a) =
  let arg =
    Arg.(value
       & opt (some cv) None
       & info [arg] ~docv ~doc
    )
  in
  let g =
    function
    | None ->
        (fun x -> x)
    | Some y ->
        f y
  in
  Term.(const g $ arg)


let float_arg ~f = arg ~cv:Arg.float ~f

let pos_arg ~f = arg ~cv:(Util.Term.positive ()) ~f

let int_arg ~f = arg ~cv:Arg.int ~f


module Hyper =
  struct
    let lambda_mean () =
      let doc =
        "Logarithm of the mean of the lognormal prior on 'lambda'."
      in
      let f x hy = hy#with_lambda_mean x in
      float_arg ~doc ~f ~docv:"LAMBDA-MEAN" ~arg:"lambda-mean"

    let lambda_var () =
      let doc =
        "Logarithm of the variance of the lognormal prior on 'lambda'."
      in
      let f x hy = hy#with_lambda_var x in
      float_arg ~doc ~f ~docv:"LAMBDA-VAR" ~arg:"lambda-var"

    let lambda_jump () =
      let doc =
        "Initial standard deviation of the gaussian proposals for 'lambda'."
      in
      let f x hy = hy#with_lambda_jump x in
      float_arg ~doc ~f ~docv:"LAMBDA-JUMP" ~arg:"lambda-jump"

    let mu_mean () =
      let doc =
        "Logarithm of the mean of the lognormal prior on 'mu'."
      in
      let f x hy = hy#with_mu_mean x in
      float_arg ~doc ~f ~docv:"MU-MEAN" ~arg:"mu-mean"

    let mu_var () =
      let doc =
        "Logarithm of the variance of the lognormal prior on 'mu'."
      in
      let f x hy = hy#with_mu_var x in
      float_arg ~doc ~f ~docv:"MU-VAR" ~arg:"mu-var"

    let mu_jump () =
      let doc =
        "Initial standard deviation of the gaussian proposals for 'mu'."
      in
      let f x hy = hy#with_mu_jump x in
      float_arg ~doc ~f ~docv:"MU-JUMP" ~arg:"mu-jump"

    let tf () =
      let doc =
        "Estimate the system up to $(docv) in time."
      in
      let f x hy = hy#with_tf x in
      float_arg ~doc ~f ~docv:"TF" ~arg:"tf"

    let width () =
      let doc =
        "Width of time slices for the estimated prm."
      in
      let f x hy = hy#with_h x in
      float_arg ~doc ~f ~docv:"WIDTH" ~arg:"width"

    let prm_nredraws () =
      let doc =
        "Redraw $(docv) k slices on prm proposal."
      in
      let f x hy = hy#with_prm_nredraws x in
      int_arg ~doc ~f ~docv:"PRM-NREDRAWS" ~arg:"prm-nredraws"
  end


let hyper () =
  let f lbd_m lbd_v lbd_j mu_m mu_v mu_j tf h nred =
    (new Param.hyper)
    |> lbd_m
    |> lbd_v
    |> lbd_j
    |> mu_m
    |> mu_v
    |> mu_j
    |> tf
    |> h
    |> nred
  in
  Term.(Hyper.(const f
      $ lambda_mean ()
      $ lambda_var ()
      $ lambda_jump ()
      $ mu_mean ()
      $ mu_var ()
      $ mu_jump ()
      $ tf ()
      $ width ()
      $ prm_nredraws ()
  ))


module Theta0 =
  struct
    let mu () =
      let doc =
        "Nucleotide substitution rate."
      in
      let f x th = th#with_mu x in
      float_arg ~doc ~f ~docv:"MU" ~arg:"mu"

    let lambda () =
      let doc =
        "Lineage pair coalescence rate."
      in
      let f x th = th#with_lambda x in
      float_arg ~doc ~f ~docv:"LAMBDA" ~arg:"lambda"
  end


let theta0 () =
  let f mu lbd =
    (new Param.theta)
    |> mu
    |> lbd
  in
  Term.(const f
      $ Theta0.mu ()
      $ Theta0.lambda ()
  )



let path =
  let doc = "Output results to file names starting by $(docv)." in
  Arg.(value & pos 0 string "path" & info [] ~docv:"PATH" ~doc)


let n_thin =
  let doc =
    "Output sample every $(docv) iterations.
     By default output at every iteration."
  in int_opt ~doc ~docv:"NTHIN" ~arg:"nthin"


let chol_every =
  let doc =
    "Output covariance matrix every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"CHOL-EVERY" ~arg:"chol-every"


let prm_every =
  let doc =
    "Ouptut point process realisation every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"PRM-EVERY" ~arg:"prm-every"


let traj_every =
  let doc =
    "Output system trajectory every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"TRAJ-EVERY" ~arg:"traj-every"


let n_prior =
  let doc =
    "At the start of the MCMC run, use the prior as proposal
     during $(docv) iterations. Equal 0 by default."
  in
  Arg.(value
     & opt int 0
     & info ["nprior"] ~docv:"NPRIOR" ~doc
  )


let n_iter =
  let doc =
    "Run $(docv) iterations of adaptive MCMC. Equal 0 by default."
  in
  Arg.(value
     & opt int 0
     & info ["niter"] ~docv:"NITER" ~doc
  )


let seed =
  let doc =
    "Initialize the PRNG with the seed $(docv)."
  in int_opt ~doc ~docv:"SEED" ~arg:"seed"


let sequences =
  let doc =
    "Sample times and sequences to estimate the phylogeny from."
  in
  let fname = Arg.(
        required
      & opt (some string) None
      & info ["sequences"] ~docv:"SEQUENCES" ~doc
    )
  in
  let read_samples fname =
    let chan = open_in fname in
    let itseqs = Seqs.Io.assoc_of_stamped_fasta chan in
    close_in chan ;
    L.map (fun (_, t, seq) ->
        (F.to_float (F.Pos.of_float t), seq)
      ) itseqs
  in
  Term.(const read_samples $ fname)
