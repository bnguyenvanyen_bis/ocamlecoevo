open Sig

module Prmi = Coal.Constant_internal.Simulate.P


class theta = object
  val lambda_v = 1.
  val mu_v = 1.
  val prm_v : Coal.Prm_time.t option = None
  val prm_internal_v : Prmi.t option = None

  method lambda = lambda_v
  method mu = mu_v

  method prm =
    match prm_v with
    | None ->
        invalid_arg "unset prm"
    | Some m ->
        m

  method prm_internal =
    match prm_internal_v with
    | None ->
        invalid_arg "unset internal prm"
    | Some nu ->
        nu


  method with_lambda x = {< lambda_v = x >}
  method with_mu x = {< mu_v = x >}
  method with_prm m = {< prm_v = Some m >}
  method with_prm_internal nu = {< prm_internal_v = Some nu >}
end


class prior = object
  val lambda_mean_v = 0.
  val lambda_var_v = 0.1

  val mu_mean_v = 0.
  val mu_var_v = 0.1

  method lambda_mean = lambda_mean_v
  method lambda_var = lambda_var_v

  method mu_mean = mu_mean_v
  method mu_var = mu_var_v

  method with_lambda_mean x = {< lambda_mean_v = x >}
  method with_lambda_var x = {< lambda_var_v = x >}

  method with_mu_mean x = {< mu_mean_v = x >}
  method with_mu_var x = {< mu_var_v = x >}
end


class prop = object
  val lambda_jump_v = 1e-2
  val mu_jump_v = 1e-2
  val prm_jump_v = 1e-3

  method lambda_jump = lambda_jump_v
  method mu_jump = mu_jump_v
  method prm_jump = prm_jump_v

  method with_lambda_jump x = {< lambda_jump_v = x >}
  method with_mu_jump x = {< mu_jump_v = x >}
  method with_prm_jump x = {< prm_jump_v = x >}
end


class env = object
  val t0_v = 0.
  val tf_v = 1.
  val h_v = 0.5
  val prm_nredraws_v = 1

  method t0 = t0_v
  method tf = tf_v
  method h = h_v
  method prm_nredraws = prm_nredraws_v

  method with_t0 x = {< t0_v = x >}
  method with_tf x = {< tf_v = x >}
  method with_h x = {< h_v = x >}
  method with_prm_nredraws n = {< prm_nredraws_v = n >}
end


class hyper = object
  inherit prior
  inherit prop
  inherit env
end


let fpof = F.Pos.of_float

let lambda p = fpof p#lambda
let mu p = fpof p#mu
let prm p = p#prm
let prm_internal p = p#prm_internal

let lambda_mean p = p#lambda_mean
let lambda_var p = fpof p#lambda_var

let mu_mean p = p#mu_mean
let mu_var p = fpof p#mu_var

let lambda_jump p = fpof p#lambda_jump
let mu_jump p = fpof p#mu_jump

let prm_jump p = fpof p#prm_jump

let t0 p = p#t0
let tf p = p#tf
let h p = fpof p#h
let prm_nredraws p = p#prm_nredraws
