open Sig

module FP = Fit.Param
module FI = Fit.Infer


let lambda hy infer =
  FI.maybe_adaptive_float
    ~tag:`Lambda
    ~get:(fun th -> th#lambda)
    ~set:(fun th x -> th#with_lambda x)
    ~prior:(Prior.lambda hy)
    ~proposal:(Propose.lambda hy)
    ~jump:(F.to_float (Param.lambda_jump hy))
    infer


let mu hy infer =
  FI.maybe_adaptive_float
    ~tag:`Mu
    ~get:(fun th -> th#mu)
    ~set:(fun th x -> th#with_mu x)
    ~prior:(Prior.mu hy)
    ~proposal:(Propose.mu hy)
    ~jump:(F.to_float (Param.mu_jump hy))
    infer


let prm hy samples =
  FI.only_custom
    ~tag:`Prm
    ~get:(fun th -> th#prm)
    ~set:(fun th x -> th#with_prm x)
    ~prior:(Prior.prm hy samples)
    ~proposal:(Propose.prm_poisson hy)
