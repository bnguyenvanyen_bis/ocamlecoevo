open Sig

module CCi = Coal.Constant_internal


module Data =
  struct
    type t = {
      n : int ;
      samples : (float * Seqs.t) M.Int.t ;
    }

    let read tseqs =
      let n = L.length tseqs in
      let samples = CCi.Simulate.sample_map tseqs in
      { n ; samples }
  end


module Prior =
  struct
    let lambda = Prior.lambda

    let mu = Prior.mu

    module Sim = CCi.Simulate
    module Ps = CCi.Simulate.P.Points

    let prm Data.{ n ; samples } hy =
      let tf = Param.tf hy in
      let draw rng =
        let nu = Sim.rand_mergers ~rng ~n in
        Sim.add_sampling samples ~tf nu
      in
      let log_dens_proba nu =
        (* sum the log dens exp for all Merge events *)
        let logp = CCi.Cm.fold (fun c ps x ->
            if CCi.Color.is_merge c then
              if Ps.cardinal ps = 1 then
                let pt = Ps.any ps in
                let t = CCi.Evm.Point.time pt in
                x +. F.to_float (U.logd_exp F.one (F.to_float t))
              else
                neg_infinity
            else
              x
          ) nu 0.
        in [logp]
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Propose =
  struct
    let lambda = Propose.lambda

    let mu = Propose.mu

    module Sim = CCi.Simulate
    module Ps = CCi.Simulate.P.Points

    let prm Data.{ n ; _ } hy =
      let v = Param.prm_jump hy in
      let pn = I.Pos.of_int n in
      let draw rng nu =
        (* choose a random Merge point *)
        (* add a normal variable to its exponential variable *)
        let i = I.to_int (U.rand_int ~rng pn) + 1 in
        let j = I.to_int (U.rand_int ~rng pn) + 1 in
        let color = CCi.Color.Merge (i, j) in
        let ps = CCi.Cm.find color nu in
        assert (Ps.cardinal ps = 1) ;
        let pt = Ps.any ps in
        let u = U.rand_normal ~rng 0. v in
        let t =
          match F.Pos.of_anyfloat (F.add pt.t (F.of_float u)) with
          | t ->
              t
          | exception Invalid_argument _ -> (* negative *)
              raise (Fit.Sig.Draw_error "negative internal time")
        in
        let pt' = { pt with t } in
        (* normally add is enough to replace *)
        CCi.Cm.add color (Ps.singleton pt') nu
      in
      let log_pd_ratio _ _ =
        (* assume they come from the proposal,
         * then it's 0. (by symmetry)
         * (otherwise it would be neg_infinity but it's annoying to prove it,
         * and it should never happen in the normal run of the MCMC)
         *)
        [0.]
      in Fit.Propose.Base { draw ; log_pd_ratio }
  end


module Likelihood =
  struct
    module Make' = Likelihood.Make'

    let sim ~t0 ~tf ~n theta =
      let lambda = Param.lambda theta in
      let nu = Param.prm_internal theta in
      CCi.Simulate.until ~lambda ~n ~t0 ~tf nu

    let loglik = Likelihood.loglik

    (* FIXME for now replace by something simpler that we are sure works *)
    let compute hy Data.{ n ; samples } =
      let t0 = Param.t0 hy in
      let tf = Param.tf hy in
      let basic = CCi.Evs.T.basic in
      let sequences = M.Int.map (fun (_, seq) -> seq) samples in
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba th =
        let forest = sim ~t0 ~tf ~n th in
        [loglik basic sequences th forest]
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Infer =
  struct
    let lambda = Infer.lambda

    let mu = Infer.mu

    let prm data hy =
      Fit.Infer.only_custom
        ~tag:`Prm
        ~get:Param.prm_internal
        ~set:(fun th x -> th#with_prm_internal x)
        ~prior:(Prior.prm data hy)
        ~proposal:(Propose.prm data hy)
  end


module Estimate =
  struct
    module Ps = CCi.Simulate.P.Points

    type (_, _, _) elist =
      El : ('tag, 'theta, _, 'a) Fit.Param.List.t -> ('tag, 'theta, 'a) elist

    let columns =
      Fit.Csv.columns_sample_move @ [
        "mu" ;
        "lambda" ;
        "move_mu" ;
        "move_lambda" ;
      ]

    let extract =
      let sof = string_of_float in
      Fit.Csv.extract_sample_move ~extract:(fun th ->
        function
        | "mu" ->
            sof th#mu
        | "lambda" ->
            sof th#lambda
        | _ ->
            failwith "unrecognized column"
      )

    let params data hy =
      El Fit.Param.List.[
        Infer.lambda hy Fit.Infer.adapt ;
        Infer.mu hy Fit.Infer.adapt ;
        Infer.prm data hy Fit.Infer.custom ;
      ]

    let sim_traj Data.{ n ; _ } hy ~chan th =
      let t0 = Param.t0 hy in
      let tf = Param.tf hy in
      let forest = Likelihood.sim ~t0 ~tf ~n th in
      CCi.Evs.output ~chan forest

    let output_points chan nu =
      CCi.Cm.iter (fun color pts ->
        let pt, rem = Ps.pop pts in
        assert (Ps.is_empty rem) ;
        let t = CCi.Evm.Point.time pt in
        Printf.fprintf chan "%s,%f\n"
        (CCi.Color.to_string color) (F.to_float t)
      ) nu

    let convert_prm ?append ?(n_burn=0) ?(n_thin=1) path =
      ignore append ;
      let output k x =
        let chan = open_out (Printf.sprintf "%s.%i.prm.points.csv" path k) in
        let th = Fit.Mcmc.return_to_sample x in
        output_points chan th#prm_internal ;
        close_out chan
      in
      let start k0 x0 =
        output k0 x0
      in
      let out k x =
        if (k >= n_burn) && (k mod n_thin = 0) then
          output k x
      in
      let return k x =
        (k, x)
      in
      U.Out.{ start ; out ; return }

    let posterior
      ?seed ?theta_every ?traj_every ?chol_every ?prm_every
      ~path ~n_iter hypar tseqs theta =
      let data = Data.read tseqs in
      (* existential *)
      match params data hypar with El pars ->
      let prior = Fit.Param.prior (new Param.theta) pars in
      let proposal = Fit.Param.custom_proposal pars in
      let likelihood = Likelihood.compute hypar data in
      let draws = A.of_list (Fit.Param.draws pars) in
      let log_pd_ratio = Fit.Param.log_pd_ratio pars in
      let chol =
        pars
        |> Fit.Param.jumps
        |> A.of_list
        |> Lac.Vec.of_array
        |> Lac.Mat.of_diag
      in
      let sim_traj = sim_traj data hypar in
      let output =
        Simfit.Out.convert_csv_traj_ram_prm
          ?n_thin:theta_every
          ?traj_every
          ?chol_every
          ?prm_every
          ~columns
          ~extract
          ~sim_traj
          ~convert_prm
          path
      in
      let nu = Fit.Dist.draw_from (U.rng seed) (Prior.prm data hypar) in
      let theta' = theta#with_prm_internal nu in
      let _, ret, _ = Fit.Ram.posterior
        ~output
        ~prior
        ~proposal
        ~likelihood
        ~draws
        ?log_pd_ratio
        ~chol
        ?seed
        ~n_iter
        theta'
      in Fit.Mcmc.return_to_sample ret
  end


module Term =
  struct
    open Term
    open Cmdliner

    let t0 () =
      let doc =
        "Estimate the system starting from time $(docv)."
      in
      let f x hy = hy#with_t0 x in
      float_arg ~doc ~f ~docv:"T0" ~arg:"t0"

    let prm_jump () =
      let doc =
        "Standard deviation of prm jumps."
      in
      let f x hy = hy#with_prm_jump x in
      float_arg ~doc ~f ~arg:"prm-jump" ~docv:"PRM-JUMP"

    let hyper () =
      let f lbd_m lbd_v lbd_j mu_m mu_v mu_j t0 tf prm_j =
        (new Param.hyper)
        |> lbd_m
        |> lbd_v
        |> lbd_j
        |> mu_m
        |> mu_v
        |> mu_j
        |> t0
        |> tf
        |> prm_j
      in
      Term.(Hyper.(const f
          $ lambda_mean ()
          $ lambda_var ()
          $ lambda_jump ()
          $ mu_mean ()
          $ mu_var ()
          $ mu_jump ()
          $ t0 ()
          $ tf ()
          $ prm_jump ()
      ))

    let theta_every =
      let doc =
          "Output sample every $(docv) iterations. "
        ^ "By default, do not output."
      in int_opt ~doc ~arg:"theta-every" ~docv:"THETA-EVERY"

    let prm_every =
      let doc =
          "Output prm sample every $(docv) iterations. "
        ^ "By default, do not output."
      in int_opt ~doc ~arg:"prm-every" ~docv:"PRM-EVERY"

    let run =
      let flat seed theta_every traj_every chol_every prm_every
               path n_iter hy tseqs th =
        Estimate.posterior
          ?seed ?theta_every ?traj_every ?chol_every ?prm_every
          ~path ~n_iter hy tseqs th
      in
      Term.(const flat
          $ seed
          $ theta_every
          $ traj_every
          $ chol_every
          $ prm_every
          $ path
          $ n_iter
          $ hyper ()
          $ sequences
          $ theta0 ()
      )

    let info =
      let doc =
        "Estimate a phylogeny under Kingman's coalescent and JC69
         substitutions from timed DNA sequences."
      in Term.info
        "coalfit-constant-internal"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end
