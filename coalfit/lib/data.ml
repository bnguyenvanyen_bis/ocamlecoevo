open Sig


type 'a t = {
  sample_draw : ('a U.anypos -> unit) ;
  points : Pt.Ps.t ;
  sequences : Seqs.t M.Int.t ;
}


(* extract times and sequences *)
let extract tseqs =
  L.fold_lefti (fun (ts, seqs) k (t, seq) ->
      (M.Int.add k t ts, M.Int.add k seq seqs)
    ) (M.Int.empty, M.Int.empty) tseqs



(* get sample_draw and sample_points *)
let points ~tf ts =
  ts
  |> M.Int.bindings
  |> L.map (fun (k, t) -> (t, k, ()))
  |> Coal.Constant.S.D.sample_points ~tf



let read hy tseqs =
  let tf = Param.tf hy in
  let kts, sequences = extract tseqs in
  let sample_draw, points = points ~tf kts in
  { sample_draw ; points ; sequences }
