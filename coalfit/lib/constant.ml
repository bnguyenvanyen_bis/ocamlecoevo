open Sig


let output_par ?seed ?n_thin ?n_thin_costly ~n_prior ~n_iter ~path hy =
  let sof = string_of_float in
  let sop x = F.to_string x in
  let soi = string_of_int in
  let soio =
    function
    | None ->
        ""
    | Some i ->
        soi i
  in
  U.Csv.output
    ~columns:["seed" ; "n_thin" ; "n_thin_costly" ; "n_prior" ; "n_iter" ;
              "mu_mean" ; "mu_var" ; "mu_jump" ;
              "lambda_mean" ; "lambda_var" ; "lambda_jump"]
    ~extract:(fun (seed, n_thin, n_thin_costly, n_prior, n_iter, hy) ->
        function
        | "seed" ->
            soio seed
        | "n_thin" ->
            soio n_thin
        | "n_thin_costly" ->
            soio n_thin_costly
        | "n_prior" ->
            soi n_prior
        | "n_iter" ->
            soi n_iter
        | "mu_mean" ->
            sof (Param.mu_mean hy)
        | "mu_var" ->
            sop (Param.mu_var hy)
        | "mu_jump" ->
            sop (Param.mu_jump hy)
        | "lambda_mean" ->
            sof (Param.lambda_mean hy)
        | "lambda_var" ->
            sop (Param.lambda_var hy)
        | "lambda_jump" ->
            sop (Param.lambda_jump hy)
        | _ ->
            assert false
      )
    ~path
    [(seed, n_thin, n_thin_costly, n_prior, n_iter, hy)]


let columns = 
  Fit.Csv.columns_accepted @ [ "mu" ; "lambda" ]


let extract =
  let sof = string_of_float in
  Fit.Csv.extract_sample
    ~extract:(fun th ->
      function
      | "mu" ->
          sof th#mu
      | "lambda" ->
          sof th#lambda
      | _ ->
          failwith "unrecognized column"
    )


let params hy samples =
  Infer.(Fit.Param.List.[
    lambda hy Fit.Infer.adapt ;
    mu hy Fit.Infer.adapt ;
    prm hy samples Fit.Infer.custom ;
  ])


let prior hy Data.{ points ; _ }  =
  Fit.Param.prior (new Param.theta) (params hy points)


let proposal hy Data.{ points ; _ } =
  Fit.Param.custom_proposal (params hy points)


let draws hy Data.{ points ; _ } =
  A.of_list (Fit.Param.draws (params hy points))


let jumps hy Data.{ points ; _ } =
  A.of_list (Fit.Param.jumps (params hy points))


let likelihood =
  Likelihood.likelihood


let sim_traj hy spdraw ~chan th =
  let tf = Param.tf hy in
  let f_out ks tree =
    (* Printf.fprintf chan "#tree %i\n" i ; *)
    Printf.fprintf chan "#tree %s\n" (Coal.Inter.to_string ks) ;
    Coal.Constant.S.D.E.T.output_newick chan tree ;
    output_string chan "\n\n"
  in
  let _, forest = Likelihood.sim ~tf spdraw th in
  Coal.Events.Ism.iter f_out forest


let sample_of_accept =
  function
  | Fit.Mcmc.Accept { sample ; _ } ->
      sample
  | _ ->
      failwith "not accept"


module Mcmc ( ) =
  struct
    let posterior
        ~path ?n_thin ?chol_every ?prm_every ?traj_every
        ~n_prior ~n_iter
        ?seed hypar tseqs par =
      output_par ~path ~n_prior ~n_iter ?seed ?n_thin hypar ;
      let data = Data.read hypar tseqs in
      let prior = prior hypar data in
      let likelihood = likelihood hypar data in
      (* for starters : n_prior times from the prior *)
      let init = Fit.Mcmc.init prior likelihood in
      let prior_prop = Fit.Propose.Dist (fun _ -> prior) in
      let sample = Fit.Mcmc.sample prior prior_prop likelihood in
      let npar =
        let output = Util.Out.convert_null in
        let _, res =
          Fit.Mcmc.posterior ~output ?seed ~init ~sample ~n_iter:n_prior par
        in sample_of_accept res
      in
      (* then n_iter times with adaptive proposal *)
      let draws = draws hypar data in
      let proposal = proposal hypar data in
      let diag = jumps hypar data in
      let chol = Lac.Mat.of_diag (Lac.Vec.of_array diag) in
      let module Prm_out = Simfit.Out.Prm.Make (Coal.Prm_time) in
      let convert_prm = Prm_out.convert_prm
        ~to_prm:(fun th -> Some (Param.prm th))
        ~output_points:true
        ~output_grid:true
      in
      let output = Simfit.Out.convert_csv_traj_ram_prm
        ?n_thin ?chol_every ?prm_every ?traj_every
        ~columns ~extract
        ~convert_prm
        ~sim_traj:(sim_traj hypar data.sample_draw)
        path
      in
      Fit.Ram.posterior
        ~output
        ~prior ~proposal ~likelihood ~draws ~chol ?seed ~n_iter npar

  end
