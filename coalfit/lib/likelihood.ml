open Sig


module type SAMPLES =
  sig
    val seqs : Seqs.t M.Int.t
  end

module Make (Samples : SAMPLES) =
  struct
    include Coal.Events.With_time_label (Coal.Constant.Trait)

    let to_seq (_, k, ()) =
      M.Int.find k Samples.seqs
  end


module Make' (Samples : SAMPLES) =
  struct
    include Coal.State.Make (Coal.Constant.Trait)

    let to_seq (_, ks, ()) =
      (* ks should be a singleton *)
      let k, rem = BatSet.Int.pop ks in
      assert (BatSet.Int.is_empty rem) ;
      M.Int.find k Samples.seqs
  end


let sim ~tf spdraw theta =
  let lambda = Param.lambda theta in
  let prm = Param.prm theta in
  Coal.Constant.simulate_until ~tf lambda spdraw prm


let loglik basic kseqs theta =
  (* Does flambda simplify this enough ? *)
  let module X = Make' (struct let seqs = kseqs end) in
  let module Fel = Seqsim.Tree.Make (X) in
  let eq = Lac.Vec.make 4 0.25 in
  let par = Seqsim.Jc69.{ mu = F.to_float (Param.mu theta) } in
  let trans_mat dt =
    Seqsim.Jc69.transition_probas par (F.Pos.of_float dt)
  in
  let loglik_curry = Fel.log_likelihood eq trans_mat in
  let f forest =
    (* sum log-likelihood over trees *)
    Coal.Events.Ism.fold (fun _ tree loglik ->
        loglik +. loglik_curry (basic tree)
      ) forest 0.
  in f


let likelihood hy Data.{ sample_draw ; sequences ; _ } =
  let tf = Param.tf hy in
  let basic = Coal.Constant.S.D.E.T.basic in
  let draw _ =
    failwith "Not_implemented"
  in
  let log_dens_proba th =
    let _, forest = sim ~tf sample_draw th in
    [loglik basic sequences th forest]
  in Fit.Dist.Base { draw ; log_dens_proba }
