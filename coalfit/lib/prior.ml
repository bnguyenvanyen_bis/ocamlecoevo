open Sig

module P = Param


let lambda hy =
  D.Lognormal (P.lambda_mean hy, P.lambda_var hy)


let mu hy =
  D.Lognormal (P.mu_mean hy, P.mu_var hy)


module Prmfit = Simfit.Prm.Make (Coal.Prm_time)


let prm hy samples =
  let nsamples = Coal.Prm_time.Ps.cardinal samples in
  (* the backward time tf *)
  let tf = F.Pos.of_float (P.tf hy -. P.t0 hy) in
  let h = P.h hy in
  let vectors = Coal.Prm_time.vectors h nsamples in
  let vectors' = Coal.Prm_time.sample_vectors nsamples in
  let colors = Coal.Color.[ Mutation ; Merge ] in
  (* we need a 'merge' version ? *)
  Prmfit.prior_merge ~colors ~vectors ~vectors' ~tf ~h samples
