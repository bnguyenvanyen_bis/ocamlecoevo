open Sig


let lambda hy =
  Fit.Propose.Dist (fun x -> D.Normal (x, Param.lambda_jump hy))


let mu hy =
  Fit.Propose.Dist (fun x -> D.Normal (x, Param.mu_jump hy))


module Prmfit = Simfit.Prm.Make (Coal.Prm_time)


let prm_poisson hy =
  Prmfit.propose_poisson_full
    ~filter:(fun c ->
           (Coal.Color.equal Coal.Color.Mutation c)
        || (Coal.Color.equal Coal.Color.Merge c)
      )
    (Param.prm_nredraws hy)
