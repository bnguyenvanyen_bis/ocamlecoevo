open Cmdliner
module CCi = Coalfit.Constant_internal

let () = Term.exit @@ Term.eval (CCi.Term.run, CCi.Term.info)
