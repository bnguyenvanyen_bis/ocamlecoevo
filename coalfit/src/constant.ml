open Coalfit

module Cterm = Cmdliner.Term
module T = Term

module M = Constant.Mcmc ( )


let constant_term =
  let posterior_term = Cterm.const (
      fun path n_thin chol_every prm_every traj_every
          n_prior n_iter seed hypar samples theta0 ->
        M.posterior ~path ?n_thin ?chol_every ?prm_every ?traj_every
                    ~n_prior ~n_iter ?seed hypar samples theta0
    )
  in Cterm.(posterior_term
          $ T.path
          $ T.n_thin
          $ T.chol_every
          $ T.prm_every
          $ T.traj_every
          $ T.n_prior
          $ T.n_iter
          $ T.seed
          $ T.hyper ()
          $ T.sequences
          $ T.theta0 ()
  )


let info =
  let doc =
    "Estimate a phylogeny from a time-stamped sample of sequences
     under Kingman's coalescent and Jc69 homogeneous evolution."
  in Cterm.info
    "coalfit-constant"
    ~version:"%%VERSION%%"
    ~doc
    ~exits:Cterm.default_exits
    ~man:[]


let () = Cterm.exit @@ Cterm.eval (constant_term, info)
