open Sig

module Color = Color
module Colors = BatMap.Make (Color)

type color = Color.t

type t = {
  c : color ;
  k : int ;
  t : U.closed_pos ;
  u : U.closed_pos ;
  i : int ;
}


let zero c = { c ; k = 0 ; t = F.zero ; u = F.zero ; i = 0 }

let color pt = pt.c
let number pt = pt.k
let time pt = F.Pos.narrow pt.t
let value pt = F.Pos.narrow pt.u

let number' pt = pt.i

let with_number k pt = { pt with k = k }
let with_time t pt = { pt with t = F.Pos.close t }
let with_value u pt = { pt with u = F.Pos.close u }


let add pt pt' =
  if not (Color.equal pt.c pt'.c) then
    invalid_arg "Point.add : different colors" ;
  {
    c = pt.c ;
    k = pt.k + pt'.k  ;
    t = F.Pos.add pt.t pt'.t ;
    u = F.Pos.add pt.u pt'.u ;
    i = pt.i + pt'.i ;
  }
 

let rand_point ~rng ?(k=0) c ~origin ~vector =
  let t = F.Pos.add origin.t (U.rand_float ~rng vector.t) in
  let u = F.Pos.add origin.u (U.rand_float ~rng vector.u) in
  let i = origin.i + I.to_int (U.rand_int ~rng (I.Pos.of_int vector.i)) in
  { c ; k ; t ; u ; i }


let volume ~vector =
  let dk = I.Pos.to_float (I.Pos.of_int vector.k) in
  let dt = vector.t in
  let du = vector.u in
  let di = I.Pos.to_float (I.Pos.of_int vector.i) in
  F.Pos.Op.(dk * dt * du * di)


let inside ~origin ~vector pt =
  let dest = add origin vector in
  if not (Color.equal (color origin) (color pt)) then
    invalid_arg "Point.inside : different colors" ;
  let inside_tu =
    F.Op.((origin.t <= pt.t)
       && (origin.u <= pt.u)
       && (pt.t < dest.t)
       && (pt.u < dest.u))
  in
  (* FIXME < or <= for dest ? *)
  let inside_ki =
          (origin.k <= pt.k)
       && (origin.i <= pt.i)
       && (pt.k < dest.k)
       && (pt.i < dest.i)
  in inside_tu && inside_ki


let lower pt pt' =
  if not (Color.equal (color pt) (color pt')) then
    invalid_arg "Point.lower : different colors" ;
  let c = color pt in
  let k = min (number pt) (number pt') in
  let t = F.Pos.min (time pt) (time pt') in
  let u = F.Pos.min (value pt) (value pt') in
  let i = min (number' pt) (number' pt') in
  { c ; k ; t ; u ; i }


let upper pt pt' =
  if not (Color.equal (color pt) (color pt')) then
    invalid_arg "Point.upper : different colors" ;
  let c = color pt in
  let k = max (number pt) (number pt') in
  let t = F.Pos.max (time pt) (time pt') in
  let u = F.Pos.max (value pt) (value pt') in
  let i = max (number' pt) (number' pt') in
  { c ; k ; t ; u ; i }


let columns = [ "c" ; "k" ; "t" ; "u" ; "i" ]


let extract { c ; k ; t ; u ; i } =
  function
  | "c" ->
      Color.to_string c
  | "k" ->
      string_of_int k
  | "t" ->
      F.to_string t
  | "u" ->
      F.to_string u
  | "i" ->
      string_of_int i
  | _ ->
      invalid_arg "unrecognized column"


let line pt =
  L.map (extract pt) columns


let read row =
  let float_of = U.Csv.conv_opt (fun s ->
    F.Pos.of_float (float_of_string s)
  )
  in
  let int_of = U.Csv.conv_opt int_of_string in
  let c_o = U.Csv.Row.find_conv Color.of_string row "c" in
  let k_o = U.Csv.Row.find_conv int_of row "k" in
  let t_o = U.Csv.Row.find_conv float_of row "t" in
  let u_o = U.Csv.Row.find_conv float_of row "u" in
  let i_o = U.Csv.Row.find_conv int_of row "i" in
  match c_o, k_o, t_o, u_o, i_o with
  | Some c, Some k, Some t, Some u, Some i ->
      Some { c ; k ; t ; u ; i }
  | _ ->
      None
