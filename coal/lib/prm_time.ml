open Sig

let du0 = F.Pos.of_float 11.

let ntslices ~h tf =
  I.Pos.of_int (int_of_float (F.to_float (F.div tf h)))

let sample_vectors nsamples =
  [(I.one, Point.{
      c = Color.Sample ;
      k = nsamples ;
      t = F.zero ;
      u = F.two ;
      i = 1 ;
  })]

let vectors h nsamples =
  let umax = F.Pos.Op.(du0 / h) in
  L.map (fun c ->
    (I.one, Point.{
        c ;
        k = nsamples ;
        t = F.zero ;
        u = umax ;
        i = nsamples
    })
  ) Color.[ Mutation ; Merge ]


include Sim.Prm.Make (Sim.Prm.Compare_time (Point))


(* expect that rngm and colors have colors Mutation and Merge,
 * but not Sample *)
let rand_merge ~rngm ~time_range ~ntslices ~vectors sample_points =
  let prm = rand_draw ~rngm ~time_range ~ntslices ~vectors in
  let nsamples = Ps.cardinal sample_points in
  let prm_sample = create
      ~time_range
      ~ntslices
      ~vectors:(sample_vectors nsamples)
      sample_points
  in merge prm prm_sample


