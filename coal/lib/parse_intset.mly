
%start <BatSet.Int.t option> set_option

%%

set_option:
  | EOF
      { None }
  | ks = set
      { Some ks }
  ;


%public
set:
  | l = delimited(LCURLY, separated_list(SEMIC, NUMBER), RCURLY)
      { BatList.fold_left (fun ks k ->
          BatSet.Int.add (int_of_string k) ks
        ) BatSet.Int.empty l }
  ;
