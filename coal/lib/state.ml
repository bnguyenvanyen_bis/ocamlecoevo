open Sig

module Make (Trait : TRAIT) =
  struct
    type trait = Trait.t
    type label = Inter.t
    type t = float * Inter.t * trait

    module Label = Inter

    let to_label (_, k, _) = k

    let to_time (t, _, _) = t

    let at_time t' (_, k, x) = (t', k, x)

    let to_string (t, k, x) =
      Printf.sprintf "%s|%s:%.3f"
      (Label.to_string k)
      (Trait.to_string x)
      t

    let of_string s =
      match Scanf.sscanf s "%s@|%s@:%f" (fun ks xs t -> (ks, xs, t)) with
      | ks, xs, t ->
          U.Option.map2 (fun k x ->
              (t, k, x)
            ) (Label.of_string ks) (Trait.of_string xs)
      | exception Scanf.Scan_failure _ ->
          None
  end
