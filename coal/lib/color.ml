type t =
  | Sample
  | Mutation
  | Merge


let to_int =
  function
  | Sample -> 0
  | Mutation -> 1
  | Merge -> 2


let compare c c' = (to_int c) - (to_int c')


let equal c c' = (compare c c' = 0)


let to_string =
  function
  | Sample ->
      "sample"
  | Mutation ->
      "mutation"
  | Merge ->
      "merge"


let of_string =
  function
  | "sample" ->
      Some Sample
  | "mutation" ->
      Some Mutation
  | "merge" ->
      Some Merge
  | _ ->
      None
