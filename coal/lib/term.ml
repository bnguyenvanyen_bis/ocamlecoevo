open Cmdliner
open Sig



let positive_opt ~doc ~docv ~arg =
  Arg.(value
     & opt (some (U.Term.positive ())) None
     & info [arg] ~docv ~doc
  )


let positive_req ~doc ~docv ~arg =
  Arg.(required
     & opt (some (U.Term.positive ())) None
     & info [arg] ~docv ~doc
  )


let out =
  let doc = "Output results to $(docv) as CSV." in
  Arg.(value & pos 0 string "out" & info [] ~docv:"OUT" ~doc)


(* we'd like to have a pos reader *)

let dt () =
  let doc = "Print the state at least every $(docv)." in
  positive_opt ~doc ~docv:"DT" ~arg:"dt"


(* I make it a required 'optional' argument (so a named argument),
 * because I'm bad. *)
let width () =
  let doc = "Use a prm time slice width of $(docv)." in
  positive_req ~doc ~docv:"WIDTH" ~arg:"width"


let t0 () =
  let doc = "Simulate the system starting at time $(docv)." in
  Term.(const (U.Option.map F.to_float)
      $ positive_opt ~doc ~docv:"T0" ~arg:"t0"
  )


let tf () =
  let doc = "Simulate the system until time $(docv)." in
  Term.(const F.to_float
      $ positive_req ~doc ~docv:"TF" ~arg:"tf"
  )


let seed =
  let doc = "Initialize the PRNG with the seed $(docv)." in
  Arg.(value & opt (some int) None & info ["seed"] ~docv:"SEED" ~doc)


let samples () =
  let doc = "Sampling times of the coalescent." in
  let times = Arg.(
        non_empty
      & opt (list ~sep:';' (U.Term.positive ())) []
      & info ["samples"] ~docv:"SAMPLES" ~doc
    )
  in
  Term.(const (L.mapi (fun k t -> (F.to_float t, k, ()))) $ times)


let lambda () =
  let doc = "Pair coalescence rate." in
  positive_req ~doc ~docv:"LAMBDA" ~arg:"lambda"
