%{
  module T = Inter_tree.Make (Trait)
%}

%start <Inter_tree.Make(Trait).t Intermap.t option> forest

%%

forest:
  | EOF
      { Some Intermap.empty }
  | DIESE ; WTREE ; ks = set ; NEWLINE ; t = tree ; NEWLINE ; NEWLINE ; f = forest
      { Util.Option.map2 (fun t f -> Intermap.add ks t f) t f }
  ;


tree:
  | t = state_tree(binode, snode, leaf, state)
      { t }
  ;


binode(node, state):
  | LPAREN ; lt = node ; COMMA ; rt = node ; RPAREN ; z = state
      { Util.Option.map3 T.binode z lt rt }
  ;


snode(node, state):
  | LPAREN ; t = node ; RPAREN ; z = state
      { Util.Option.map2 T.node z t }
  ;


leaf(state):
  | z = state
      { Util.Option.map T.leaf z }
  ;
