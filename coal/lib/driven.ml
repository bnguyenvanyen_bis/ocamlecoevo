open Sig


module Points = Prm_time.Ps


module Make (Trait : TRAIT) =
  struct
    module E = Events.Make (Trait)

    module Traits = BatSet.Make (Trait)

    (* actually maybe Mutation / Merge is enough,
     * but no time now *)

    let reverse_time ~tf t =
      tf -. t

    (* FIXME bad implementation *)
    let rank x traits =
      let i, _ = L.findi (fun _ -> Trait.equal x) (Traits.to_list traits) in
      I.Pos.of_int i

    let draw_sample traits =
      (* draw and inv_draw create a correspondence between 
       * u values and trait values *)
      let n = Traits.cardinal traits in
      let du = F.Pos.Op.(F.one / I.Pos.(to_float (of_int n))) in
      let half_du = F.Pos.div du F.two in
      let draw u =
        let rec f i xs =
          match Traits.pop_min xs with
          | x, xs' ->
              if F.Op.(u <= du * I.to_float i) then
                x
              else
                f (I.Pos.succ i) xs'
          | exception Not_found ->
              failwith "draw_sample"
        in f I.one traits
      in
      (* each sampled trait value is given a u value so that it can
       * be found again from the point *)
      let inv_draw x =
        let i = rank x traits in
        F.Pos.Op.(du * (I.Pos.to_float i) + half_du)
      in draw, inv_draw

    (* For now let's forbid simultaneous sampling :
     * all samples must have distinct times *)
    let sample_points ~tf samples =
      let traits = L.fold_left (fun xs (_, _, x) ->
          Traits.add x xs
        ) Traits.empty samples
      in
      let draw, inv_draw = draw_sample traits in
      let point k t x = Point.{
          c = Color.Sample ;
          k ;
          t = F.Pos.of_float (reverse_time ~tf t) ;
          u = inv_draw x ;
          i = 0 ;
        }
      in
      let points = L.fold_left (fun pts (t, k, x) ->
          Points.add (point k t x) pts
        ) Points.empty samples
      in draw, points

    let rate_sample _ _ =
      F.one

    let modif_sample ~tf draw pt t z =
      let k = Point.number pt in
      let u = Point.value pt in
      (* find the sample trait value from draw *)
      let t' = reverse_time ~tf (F.to_float t) in
      E.sample ~k t' (draw u) z

    let modif_mutation ~tf mut pt t z =
      (* there can be modif if lineage k is present in z *)
      assert (Color.equal (Point.color pt) Color.Mutation) ;
      let k = Point.number pt in
      let u = Point.value pt in
      let t' = reverse_time ~tf (F.to_float t) in
      match E.state ~k z with
      | None ->
          (* reject modif
           * -- with good 'hide_k' calls this should not happen *)
          z
      | Some x ->
          begin match mut u x with
          | None ->
              (* reject modif *)
              z
          | Some target ->
              E.mutate ~k ~target t' z
          end

    (* Missing hide_k call *)
    let modif_merge ~tf mut pt t z =
      assert (Color.equal (Point.color pt) Color.Merge) ;
      (* A merge between k and i happens if k and i are different,
       * and if number is k and number' is i,
       * or if number is i and number' is k.
       * But we want it to happen only in half of those cases,
       * so that the total rate is lambda and not 2 * lambda *)
      let mut' u x y =
        (* u is uniform in [0, 1],
         * the merge can happen only if u < 0.5 *)
        let u' = F.Pos.mul F.two u in
        if F.Op.(u' > F.one) then
          None
        else
          mut u' x y
      in
      let k = Point.number pt in
      let i = Point.number' pt in
      let u = Point.value pt in
      let t' = reverse_time ~tf (F.to_float t) in
      match E.state ~k z, E.state ~k:i z with
      | None, None
      | Some _, None
      | None, Some _ ->
          (* reject modif *)
          z
      | Some x, Some y ->
          begin match mut' u x y with
          | None ->
              (* reject modif *)
              z
          | Some target ->
              E.merge ~k ~i ~target t' z
          end
  end


module Make' (Trait : TRAIT) =
  struct
    module D = Make (Trait)
    module E = Events.Make' (Trait)

    module Traits = D.Traits

    let reverse_time = D.reverse_time

    let rank = D.rank

    let draw_sample = D.draw_sample
    
    let sample_points = D.sample_points

    let rate_sample _ _ =
      F.one

    let modif_sample ~tf draw pt t z =
      let k = Point.number pt in
      let u = Point.value pt in
      (* find the sample trait value from draw *)
      let t' = reverse_time ~tf (F.to_float t) in
      E.sample ~k t' (draw u) z

    let modif_mutation ~tf mut pt t z =
      (* there can be modif if lineage k is present in z *)
      assert (Color.equal (Point.color pt) Color.Mutation) ;
      let mut' u m x =
        (* mutation can only happen if m * u < 1 *)
        let u' = F.Pos.mul (F.Pos.of_float (float m)) u in
        if F.Op.(u' > F.one) then
          None
        else
          mut u' x
      in
      let k = Point.number pt in
      let u = Point.value pt in
      (* u is uniform in [0, 1],
       * we need to correct for the number of lineages that
       * k has already coalesced with *)
      let t' = reverse_time ~tf (F.to_float t) in
      match E.state ~k z with
      | None ->
          (* reject modif
           * -- with good 'hide_k' calls this should not happen *)
          z
      | Some (m, x) ->
          begin match mut' u m x with
          | None ->
              (* reject modif *)
              z
          | Some target ->
              E.mutate ~k ~target t' z
          end

    (* Missing hide_k call *)
    let modif_merge ~tf mut pt t z =
      assert (Color.equal (Point.color pt) Color.Merge) ;
      let mut' u m m' x x' =
        (* correct for m, m' and exchange of lineages *)
        let pm = F.Pos.of_float (float m) in
        let pm' = F.Pos.of_float (float m') in
        let u' = F.Pos.Op.(pm * pm' * F.two * u) in
        if F.Op.(u' > F.one) then
          None
        else
          mut u x x'
      in
      let k = Point.number pt in
      let i = Point.number' pt in
      let u = Point.value pt in
      let t' = reverse_time ~tf (F.to_float t) in
      match E.state ~k z, E.state ~k:i z with
      | None, None
      | Some _, None
      | None, Some _ ->
          (* reject modif *)
          z
      | Some (m, x), Some (m', x') ->
          begin match mut' u m m' x x' with
          | None ->
              (* reject modif *)
              z
          | Some target ->
              E.merge ~k ~i ~target t' z
          end
  end
