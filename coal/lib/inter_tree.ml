open Sig

module Make (Trait : TRAIT) = Tree.Labeltimed.Make (State.Make (Trait))
