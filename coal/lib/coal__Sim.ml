open Sig


module S = Sim.Ctmjp.Prm.Make (Point)


module Make (Trait : TRAIT) =
  struct
    module D = Driven.Make' (Trait)

    module Cm = Point.Colors

    let combine = Sim.Ctmjp.Util.combine


    let events ~tf ~sample ~mutation ~merge =
      let sample_event = combine D.rate_sample (D.modif_sample ~tf sample) in
      let mut_rate, mut_target = mutation in
      let mut_modif = D.modif_mutation ~tf mut_target in
      let mutation_event = combine mut_rate mut_modif in
      let merge_rate, merge_target = merge in
      let merge_modif = D.modif_merge ~tf merge_target in
      let merge_event = combine merge_rate merge_modif in
      Cm.empty
      |> Cm.add Color.Sample sample_event
      |> Cm.add Color.Mutation mutation_event
      |> Cm.add Color.Merge merge_event


    let simulate_until ~tf ~output sample mutation merge prm =
      let x0 = D.E.empty in
      let evm = events ~tf ~sample ~mutation ~merge in
      let tf', xf = S.simulate_until ~output evm x0 prm in
      tf', D.E.root_at (D.reverse_time ~tf (F.to_float tf')) xf
  end
