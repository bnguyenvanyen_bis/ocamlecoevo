open Sig


module Im = BatMap.Int


module With_time_label (Trait : TRAIT) =
  struct
    type trait = Trait.t
    type t = float * int * trait

    module Label = Tree.Label.Int

    let to_time (t, _, _) = t

    let at_time t' (_, k, x) = (t', k, x)

    let to_string (t, k, x) =
      Printf.sprintf "%i|%s:%.3f" k (Trait.to_string x) t

    let of_string s =
      match Scanf.sscanf s "%i|%s@:%f" (fun k xs t -> (k, xs, t)) with
      | k, xs, t ->
          Util.Option.map (fun x -> (t, k, x)) (Trait.of_string xs)
      | exception Scanf.Scan_failure _ ->
          None

    let label_of (_, k, _) = k
  end


module Make (Trait : TRAIT) =
  struct
    exception Already_sampled

    module X = With_time_label (Trait)
    module T = Tree.Timed.Make (X)

    type trait = Trait.t

    type tree = T.t

    type t = tree Im.t

    let empty = Im.empty

    let root_at t =
      Im.map (fun tree ->
        let _, ks, x = T.root_value tree in
        T.node (t, ks, x) tree
      )

    let find ~k z =
      match Im.find k z with
      | tree ->
          Some tree
      | exception Not_found ->
          None

    let state ~k z =
      match find ~k z with
      | Some tree ->
          let _, _, x = T.root_value tree in
          Some x
      | None ->
          None

    let sample ~k t x z =
      let tree = T.leaf (t, k, x) in
      if Im.mem k z then
        raise Already_sampled
      else
        Im.add k tree z

    let mutate ~k ~target t z =
      let tree = Im.find k z in
      let tree' = T.node (t, k, target) tree in
      Im.update k k tree' z

    let merge ~k ~i ~target t z =
      if k = i then
        z
      else
        let tree = Im.find k z in
        let tree' = Im.find i z in
        let j = min k i in
        let tree'' = T.binode (t, j, target) tree tree' in
        (* We might like some other rule *)
        z
        |> Im.remove k
        |> Im.remove i
        |> Im.add j tree''
  end


module With_time_inter (Trait : TRAIT) =
  struct
    type trait = Trait.t
    type t = float * Inter.t * trait

    module Label = Inter
    
    let time_of (t, _, _) = t

    let at_time t' (_, k, x) = (t', k, x)

    let to_string (t, k, x) =
      Printf.sprintf "%s|%s:%.3f"
      (Label.to_string k)
      (Trait.to_string x)
      t

    let of_string s =
      match Scanf.sscanf s "%s@|%s@:%f" (fun ks xs t -> (ks, xs, t)) with
      | ks, xs, t ->
          U.Option.map2 (fun k x ->
              (t, k, x)
            ) (Label.of_string ks) (Trait.of_string xs)
      | exception Scanf.Scan_failure _ ->
          None
  end


(* Int set map *)
module Ism = Intermap


module Make' (Trait : TRAIT) =
  struct
    exception Already_sampled

    module T = Inter_tree.Make (Trait)
    module P = Parse.Make (Trait)

    type trait = Trait.t

    type tree = T.t

    type t = tree Ism.t

    let empty = Ism.empty

    let root_at t =
      Ism.map (fun tree ->
        let _, ks, x = T.root_value tree in
        T.node (t, ks, x) tree
      )

    let output ~chan =
      Ism.iter (fun ks tree ->
        Printf.fprintf chan "#tree %s\n" (Inter.to_string ks) ;
        T.output_newick chan tree ;
        output_string chan "\n\n"
      )

    let to_string forest =
      Ism.fold (fun ks tree s ->
        Printf.sprintf "%s#tree %s\n%s\n\n"
          s (Inter.to_string ks) (T.to_newick tree)
      ) forest ""

    let read ~chan =
      let lxbf = Lexing.from_channel chan in
      P.forest Lex.read lxbf
      (*
      let schan = Scanf.Scanning.from_channel chan in
      let rec read_tree x =
        match Scanf.bscanf schan "#tree %s@\n" (fun s -> s) with
        | s ->
            let ks = U.Option.some (Inter.of_string s) in
            let tree = T.of_newick chan in
            let x' = Ism.add ks tree x in
            (* consume linebreaks *)
            Scanf.bscanf schan "\n\n" () ;
            read_tree x'
        | exception Scanf.Scan_failure _ ->
            x
      in read_tree Ism.empty
      *)

    let of_string s =
      let lxbf = Lexing.from_string s in
      P.forest Lex.read lxbf

    let find ~k z =
      match Ism.find (Inter.singleton k) z with
      | tree ->
          let _, ks, _ = T.root_value tree in
          Some (ks, tree)
      | exception Not_found ->
          None

    let state ~k z =
      U.Option.map (fun (ks, tree) ->
        let m = Inter.cardinal ks in
        let _, _, x = T.root_value tree in
        (m, x)
      ) (find ~k z)

    let sample ~k t x z =
      let ks = Inter.singleton k in
      let tree = T.leaf (t, ks, x) in
      if Ism.mem ks z then
        raise Already_sampled
      else
        Ism.add ks tree z

    let mutate ~k ~target t z =
      match find ~k z with
      | Some (ks, tree) ->
          let tree' = T.node (t, ks, target) tree in
          Ism.update ks ks tree' z
      | None ->
          raise Not_found

    let merge ~k ~i ~target t z =
      let zo = U.Option.map2 (fun (ks, tree) (is, tree') ->
          if Inter.equal ks is then
            z
          else
            let js = Inter.union ks is in
            let tree'' = T.binode (t, js, target) tree tree' in
            z
            |> Ism.remove ks
            |> Ism.remove is
            |> Ism.add js tree''
        ) (find ~k z) (find ~k:i z)
      in
      match zo with
      | Some z' ->
          z'
      | None ->
          raise Not_found

    let time_mrca z i j =
      let is_label_singleton i x =
        (T.State.to_label x) = (Inter.singleton i)
      in
      match find ~k:i z, find ~k:j z with
      | None, None
      | Some _, None
      | None, Some _ ->
          (* if one or both label is absent, then None *)
          None
      | Some (is, itree), Some (_js, jtree) ->
          if (Inter.mem i is) && (Inter.mem j is) then begin
            (* in the same tree : coalesced already *)
            assert (itree = jtree) ;
            (* cannot use T.label_mrca, which uses Inter.equal,
             * which would match everywhere *)
            itree
            |> (fun t -> T.mrca t (is_label_singleton i) (is_label_singleton j))
            |> U.Option.map T.State.to_time
          end else
            None
  end
