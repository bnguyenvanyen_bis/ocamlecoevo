open Sig


module Is = BatSet.Int
module Im = BatMap.Int


module Color =
  struct
    type t =
      | Sample of int
      | Mutation of int
      | Merge of int * int
      (* this is actually symmetric Merge (i, j) = Merge (j, i),
       * can we use that ? *)

    let compare col col' =
      match col, col' with
      | Sample n, Sample n' ->
          compare n n'
      | Mutation n, Mutation n' ->
          compare n n'
      | Merge (i, j), Merge (i', j') ->
          let c = compare i i' in
          if c = 0 then
            compare j j'
          else
            c
      | Sample _, Mutation _
      | Sample _, Merge _ ->
          ~-1
      | Mutation _, Sample _
      | Merge _, Sample _ ->
          1
      | Mutation _, Merge _ ->
          ~-1
      | Merge _, Mutation _ ->
          1

    let equal c c' = (compare c c' = 0)

    let to_string =
      function
      | Sample n ->
          Printf.sprintf "sample(%i)" n
      | Mutation n ->
          Printf.sprintf "mutation(%i)" n
      | Merge (i, j) ->
          Printf.sprintf "merge(%i,%i)" i j

    let of_string s =
      let scan = Scanf.sscanf in
      match scan s "sample(%i)" (fun n -> Sample n) with
      | col ->
          Some col
      | exception Scanf.Scan_failure _ ->
          begin match scan s "mutation(%i)" (fun n -> Mutation n) with
          | col ->
              Some col
          | exception Scanf.Scan_failure _ ->
              begin match scan s "merge(%i,%i)" (fun i j -> Merge (i, j)) with
              | col ->
                  Some col
              | exception Scanf.Scan_failure _ ->
                  None
              end
          end

    let with_number k =
      function
      | Sample _ ->
          Sample k
      | Mutation _ ->
          Mutation k
      | Merge (_, j) ->
          (* breaks symmetry *)
          Merge (k, j)

    let add col col' =
      match col, col' with
      | Sample n, Sample n' ->
          Sample (n + n')
      | Mutation n, Mutation n' ->
          Mutation (n + n')
      | Merge (i, j), Merge (i', j') ->
          Merge (i + i', j + j')
      | Sample _, Mutation _
      | Sample _, Merge _
      | Mutation _, Sample _
      | Mutation _, Merge _
      | Merge _, Sample _
      | Merge _, Mutation _ ->
          invalid_arg "colors are not compatible"

    let is_merge =
      function
      | Merge _ ->
          true
      | Sample _ | Mutation _ ->
          false
  end


module Cm = M.Make (Color)


(* FIXME can we provide another interface where we don't have a 'value'
 * as we don't need it ?
 * Otherwise it fucks with the volume for instance *)
module Trait =
  struct
    type t = unit

    let to_string () = "_"

    let of_string =
      function
      | "_" -> Some ()
      | _ -> None

    let compare () () = 0

    let equal () () = true
  end


(* Do we use Events.Make or Events.Make' ? *)

module Evs = Events.Make' (Trait)


module Evm =
  struct
    module Color = Color
    module Point = Sim.Ctmjp.Prm_internal.Point (Color)

    type color = Color.t
    type point = Point.t

    module Colors = BatMap.Make (Color)

    type ('a, 'b) submodif = (Point.t -> 'b -> 'a * 'b)

    type ('a, 's, 't) elt =
        ('s U.anypos -> 'a -> 't U.pos)
      * ('s U.anypos -> 'a -> Point.t -> ('a, 's, 't) t -> 'a * ('a, 's, 't) t)
    and ('a, 's, 't) t = {
      active : Is.t ;
      events : ('a, 's, 't) elt Cm.t ;
    }
      constraint 's = 's F.anypos
      constraint 't = 't F.pos

    let any { events ; _ } =
      let c, _ = Cm.any events in
      c

    let find c { events ; _ } =
      Cm.find c events

    let map f { events ; _ } =
      Cm.mapi f events
    
  end


module Simulate =
  struct
    module SCU = Sim.Ctmjp.Util
    module SCP = Sim.Ctmjp.Prm_internal

    module P = SCP.Make (Evm)

    let reverse_time ~tf t =
      tf -. t

    let activate merge_event k evm = Evm.(
      let active = Is.add k evm.active in
      let events =
        evm.events
        |> Cm.remove (Color.Sample k)
        |> Is.fold (fun i evs ->
            (* FIXME what about (i, k) ? *)
            Cm.add (Color.Merge (k, i)) (merge_event k i) evs
           ) evm.active
      in { active ; events }
    )

    let disactivate ks is evm = Evm.(
      let events = Is.fold (fun k evs ->
        Is.fold (fun i evs' ->
          evs'
          |> Cm.remove (Color.Merge (k, i))
          |> Cm.remove (Color.Merge (i, k))
        ) is evs
      ) ks evm.events
      in { evm with events }
    )

    (* one single state -> no mut for now *)
    let modif_merge ~tf (k, i) t z _ evm =
      let t' = reverse_time ~tf (F.to_float t) in
      match Evs.find ~k z, Evs.find ~k:i z with
      | None, None
      | Some _, None
      | None, Some _ ->
          (z, evm)
      | Some (ks, _), Some (is, _) ->
          (* time has already been consumed at the correct rate at this point *)
          let z' = Evs.merge ~k ~i ~target:() t' z in
          let evm' = disactivate ks is evm in
          (z', evm')

    let modif_sample activ ~tf k t z _ evm =
      let t' = reverse_time ~tf (F.to_float t) in
      let z' = Evs.sample ~k t' () z in
      let evm' = activ k evm in
      (z', evm')

    (* With the usual algorithms, the rate cannot know about the point,
     * but in Prm_internal it would be ok.
     * Here we do need to know the color in the rate.
     * So how do we do it ?
     * I think I need to update the types in *)

    let rate_sample _ _ = F.one

    let rate_merge lbd (k, i) _ z =
      match Evs.state ~k z, Evs.state ~k:i z with
      | Some (m, _), Some (m', _) ->
          (* FIXME we also need something that checks
           * that they're not in the same globule
           * as it is it will still work but it seems wasteful *)
          let pm = F.Pos.of_float (float m) in
          let pm' = F.Pos.of_float (float m') in
          F.Pos.Op.(lbd / (pm * pm'))
      | Some _, None | None, Some _ | None, None ->
          F.zero

    let events ~tf ~n lbd =
      let event_merge k i =
        (rate_merge lbd (k, i),
         modif_merge ~tf (k, i))
      in
      let event_sample i =
          (rate_sample,
           modif_sample (activate event_merge) ~tf i)
      in
      Cm.empty
      |> (fun cm -> U.int_fold ~f:(fun m i ->
            Cm.add (Color.Sample i) (event_sample i) m
          ) ~n ~x:cm)
      |> (fun cm -> U.int_fold ~f:(fun m i ->
          U.int_fold ~f:(fun m' j ->
            Cm.add (Color.Merge (i, j)) (event_merge i j) m'
          ) ~n ~x:m
         ) ~n ~x:cm
        )

    let evm ~tf ~n lbd =
      let active = Is.empty in
      let events = events ~tf ~n lbd in
      Evm.{ active ; events }

    let sample_map samples =
      (* sort by decreasing time *)
      let sorted = L.sort (fun (t, _) (t', _) -> compare t' t) samples in
      (* indices from 1 to n *)
      L.fold_lefti (fun im i (t, seq) ->
        (* Printf.eprintf "%i -> (%f, %s)\n" (i + 1) t (Seqs.to_string seq) ; *)
        Im.add (i + 1) (t, seq) im
      ) Im.empty sorted

    let rand_mergers ~rng ~n =
      (* indices from 1 to n *)
      (* 
       * FIXME can we put only half of the Merge events in ?
       * we would need a good way to know which one...
       * like i < j or something
       *)
      U.int_fold ~f:(fun nu i ->
        U.int_fold ~f:(fun nu' j ->
          let color = Color.Merge (i, j) in
          let t = U.rand_exp ~rng F.one in
          let point = SCP.{ t ; c = color } in
          Cm.add color (P.Points.singleton point) nu'
        ) ~x:nu ~n
      ) ~x:Cm.empty ~n

    let add_sampling samples ~tf nu =
      Im.fold (fun i (t, _) nu' ->
        let color = Color.Sample i in
        let t' = F.Pos.of_float (reverse_time ~tf t) in
        let point = SCP.{ c = color ; t = t' } in
        Cm.add color (P.Points.singleton point) nu'
      ) samples nu

    let until ~lambda ~n ~t0 ~tf nu =
      let output = Util.Out.convert_null in
      let evm = evm ~tf ~n lambda in
      let z0 = Evs.empty in
      let back_tf = F.Pos.of_float (reverse_time ~tf t0) in
      let _, zf = P.simulate_until ~output evm z0 nu back_tf in
      Evs.root_at t0 zf

    let rand_until ?seed ~samples ~lambda ~t0 ~tf =
      let n = L.length samples in
      let sample_map = sample_map samples in
      let rng = U.rng seed in
      let nu = rand_mergers ~rng ~n in
      let nu = add_sampling sample_map ~tf nu in
      let zf = until ~lambda ~n ~t0 ~tf nu in
      sample_map, zf

    let rand_until_out ?seed ~samples ~lambda ~t0 ~tf path =
      let _, zf = rand_until ?seed ~samples ~lambda ~t0 ~tf in
      let chan = open_out path in
      Evs.output ~chan zf ;
      close_out chan
  end


module Term =
  struct
    open Term
    open Cmdliner

    let t0 =
      let doc =
        "Simulate the system starting at time $(docv)."
      in
      Arg.(value
         & opt float 0.
         & info ["t0"] ~docv:"T0" ~doc
      )

    let out =
      let doc =
        "Output the resulting coalescent forest to $(docv)."
      in
      Arg.(value
         & pos 0 string "out"
         & info [] ~docv:"OUT" ~doc
      )

    let samples =
      let doc = "Sampling times of the coalescent." in
      let times = Arg.(
          non_empty
        & opt (list float) []
        & info ["samples"] ~docv:"SAMPLES" ~doc
      )
      in
      Term.(const (L.map (fun t -> (t, ())))
          $ times
      )

    let rand_until_out =
      let flat seed samples lambda t0 tf path =
        Simulate.rand_until_out ?seed ~samples ~lambda ~t0 ~tf path
      in
      Term.(const flat
          $ seed
          $ samples
          $ lambda ()
          $ t0
          $ tf ()
          $ out
      )

    let info =
      let doc =
        "Simulate Kingman's coalescent with internal times."
      in
      Cmdliner.Term.info
        "coal-constant-internal"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Cmdliner.Term.default_exits
        ~man:[]
  end
