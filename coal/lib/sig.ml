module L = BatList
module M = BatMap

module U = Util
module F = U.Float
module I = U.Int



module type TRAIT =
  sig
    open Util.Interfaces

    include Tree.Sig.STATE

    include EQUALABLE with type t := t
    include COMPARABLE with type t := t
  end


(*
(* Can we make it not as a functor ? *)
module type DRIVER =
  sig
    type t

    module Trait : TRAIT

    (* can we ask for them as a list/set of points ?
     * Then we'd add them to the prm.
     * But it would be expensive to do that all the time.
     * Otherwise it needs to be part of the prm,
     * but we should not redraw on it.
     * How can we make that possible ? *)

    (* only dependent on time *)
    type ('s, 'a) pair_coalescence_rate =
      x:Trait.t ->
      y:Trait.t ->
      's U.anypos ->
        'a U.pos

    type ('s, 'a) transition_rate =
      source:Trait.t ->
      target:Trait.t ->
      's U.anypos ->
        'a U.pos

  end
*)
