%parameter<Trait : Sig.TRAIT>

%%

%public
state:
  | ks = set ; BAR ; xo = trait ; COLON ; t = time
      { Util.Option.map (fun x -> (t, ks, x)) xo }
  ;


time:
  | k = FLOAT
      { float_of_string k }
  ;


trait:
  | s = any
      { Trait.of_string s }
  ;


any:
  | l = nonempty_list(any_token)
      { BatList.reduce (^) l }
;


any_token:
  | s = WORD
      { s }
  | UNDER
      { "_" }
  | SLASH
      { "/" }
  | SEMIC
      { ";" }
  | RSQUARE
      { "]" }
  | RCURLY
      { "}" }
  | k = NUMBER
      { k }
  | LSQUARE
      { "[" }
  | LCURLY
      { "{" }
  | EQUAL
      { "=" }
  | DIESE
      { "#" }
  ;
