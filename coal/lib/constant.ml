open Sig


module Trait =
  struct
    type t = unit

    let to_string () = "_"

    let of_string =
      function
      | "_" -> Some ()
      | _ -> None

    let compare () () = 0

    let equal () () = true
  end


module S = Coal__Sim.Make (Trait)


let mut_rate _ _ = F.zero

let mut_target _ () = Some ()

let mutation = (mut_rate, mut_target)


(* lambda is the rate at which a pair of lineages A and B merge. *)
let merge_rate lambda _ _ = lambda

let merge_target _ () () = Some ()

let merge lambda = (merge_rate lambda, merge_target)


let simulate_until ~tf lambda sample_draw prm =
  let output = Util.Out.convert_null in
  S.simulate_until ~tf ~output sample_draw mutation (merge lambda) prm


let merge_simulate_until ~tf ~samples lambda prm =
  (* Should we put a label on each sample somehow to identify it ? *)
  let samples' = L.map (fun (t, k) -> (t, k, ())) samples in
  let sample_draw, points = S.D.sample_points ~tf samples' in
  let time_range, ntslices = Prm_time.slicing prm in
  let vectors = Prm_time.sample_vectors (L.length samples) in
  let prm_sample = Prm_time.create ~time_range ~ntslices ~vectors points in
  let prm' = Prm_time.merge prm prm_sample in
  let output = Util.Out.convert_null in
  S.simulate_until ~tf ~output sample_draw mutation (merge lambda) prm'


let rsu ?seed ~h ~samples ~lambda ?t0 ~tf chan =
  let rng = U.rng seed in
  let rngm = Prm_time.rngm ~rng Color.[ Mutation ; Merge ] in
  (* arguments t0 and tf are in the domain of samples,
   * we convert to the domain of simulations (backward time) *)
  let tf_rev =
    match t0 with
    | None ->
        F.Pos.of_float tf
    | Some t0 ->
        F.Pos.of_float (tf -. t0)
  in
  let time_range = (F.zero, tf_rev) in
  let ntslices = Prm_time.ntslices ~h tf_rev in
  let vectors = Prm_time.vectors h (L.length samples) in
  let sample_draw, points = S.D.sample_points ~tf samples in
  let prm = Prm_time.rand_merge ~rngm ~time_range ~ntslices ~vectors points in
  let output = Util.Out.convert_null in
  let _, zf =
    S.simulate_until ~tf ~output sample_draw mutation (merge lambda) prm
  in
  Intermap.iter (fun ks tree ->
      Printf.fprintf chan ">%s\n" (Inter.to_string ks) ;
      S.D.E.T.output_newick chan tree ;
      Printf.fprintf chan "\n"
    ) zf


let rand_simulate_until ?seed ~h ~samples ~lambda ?t0 ~tf out =
  let chan = open_out out in
  rsu ?seed ~h ~samples ~lambda ?t0 ~tf chan ;
  close_out chan
