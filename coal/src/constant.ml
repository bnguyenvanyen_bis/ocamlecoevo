open Coal


let constant_term =
  Cmdliner.Term.(
      const (fun seed h samples lambda t0 tf out ->
        Coal.Constant.rand_simulate_until ?seed ~h ~samples ~lambda ?t0 ~tf out)
    $ Term.seed
    $ Term.width ()
    $ Term.samples ()
    $ Term.lambda ()
    $ Term.t0 ()
    $ Term.tf ()
    $ Term.out
  )


let info =
  let doc = "Simulate Kingman's coalescent" in
  Cmdliner.Term.info
    "coal-constant"
    ~version:"%%VERSION%%"
    ~doc
    ~exits:Cmdliner.Term.default_exits
    ~man:[]


let () = Cmdliner.Term.exit @@ Cmdliner.Term.eval (constant_term, info)
