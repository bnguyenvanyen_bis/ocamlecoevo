open Coal
open Sig


let%expect_test _ =
  let seed = 0 in
  let h = F.Pos.of_float 0.1 in
  let samples = [
    (0.47, 0, ()) ;
    (0.62, 1, ()) ;
    (0.93, 2, ()) ;
    (0.99, 3, ()) ;
  ]
  in
  let lambda = F.Pos.of_float 10. in
  let tf = 1. in
  Constant.rsu ~seed ~h ~samples ~lambda ~tf stdout ;
  [%expect{|
    Accepted points : 156 out of 3492
    >{0;1;2;3}
    (((({0}|_:0.470,{3}|_:0.990){0;3}|_:0.446,{2}|_:0.930){0;2;3}|_:0.320,{1}|_:0.620){0;1;2;3}|_:0.208){0;1;2;3}|_:0.000; |}]
