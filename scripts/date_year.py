#!/usr/bin/python3
# -*- coding: utf-8 -*-

import datetime
import time
from math import floor


def date_to_year(date) :
  def since_epoch(date): # returns seconds since epoch
    return time.mktime(date.timetuple())

  year = date.year
  start_of_this_year = datetime.date(year=year, month=1, day=1)
  start_of_next_year = datetime.date(year=year + 1, month=1, day=1)

  year_elapsed = since_epoch(date) - since_epoch(start_of_this_year)
  year_duration = since_epoch(start_of_next_year) - since_epoch(start_of_this_year)
  fraction = year_elapsed / year_duration

  return date.year + fraction


def year_to_date(year) :
  def since_epoch(date): # returns seconds since epoch
    return time.mktime(date.timetuple())

  yr = floor(year)
  fract = year - yr

  start_year = datetime.date(year=yr, month=1, day=1)
  next_year = datetime.date(year=yr + 1, month=1, day=1)

  year_duration = since_epoch(next_year) - since_epoch(start_year)

  # the second should correspond to the very first second of the day
  # but with rounding errors, we might end up
  # with the last second of the previous day
  # so we add + 1
  seconds_elapsed_year = year_duration * fract + 1
  seconds_total = since_epoch(start_year) + seconds_elapsed_year

  return datetime.date.fromtimestamp(seconds_total)


