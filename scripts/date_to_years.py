#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd

from date_year import date_to_year


parser = argparse.ArgumentParser(
  description=
  "Convert a date cases csv file to a years cases csv file."
)

parser.add_argument(
  "--in",
  type=argparse.FileType('r'),
  default='-',
  help="Input csv file path. Should have a 'date' column. Default stdin."
)

parser.add_argument(
  "--out",
  type=argparse.FileType('w'),
  default='-',
  help="Output csv file path. Default stdout."
)


args = parser.parse_args()


df = pd.read_csv(vars(args)["in"], converters = { "date" : pd.to_datetime })

assert("date" in df.columns)

cols = [ c for c in df.columns if c != "date" ]

df = df.assign(t = df["date"].apply(date_to_year))

df.to_csv(args.out, index=False, columns = ["t"] + cols)
