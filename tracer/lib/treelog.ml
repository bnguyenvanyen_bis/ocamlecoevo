module T = Tree.T
module B = Tree.Base


let binode_states tree =
  let rec f = function
    | B.Leaf _ ->
        []
    | B.Node (_, stree) ->
        f stree
    | B.Binode (x, ltree, rtree) ->
        [x] @ (f ltree) @ (f rtree)
  in f tree


let open_in fname =
  let chan_in = open_in fname in
  (* drop_nontree chan_in ; *)
  (chan_in, Lexing.from_channel chan_in)


let close_in (chan, _) =
  close_in chan


module P = Parse_nexus.Make (T.Len)


let next (_, lxbf) =
  match P.nexus Lex_k.read lxbf with
  | None ->
      raise End_of_file
  | Some (_, None) ->
      invalid_arg "Treelog.next : Bad input"
  | Some (k, Some tree) ->
      let t_tree = T.time_tree 0. tree in
      let tip = T.tip_value t_tree in
      (k, T.map (fun t -> t -. tip) t_tree)


let burn ~n tchan =
  let rec f = function
    | 0 ->
        ()
    | n when n < 0 ->
        invalid_arg "Trace.Log.burn : negative n"
    | n ->
        ignore (next tchan) ; f (n - 1)
  in f n
