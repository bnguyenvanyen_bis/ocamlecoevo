module L = BatList
module S = BatString

type chan = Csv.in_channel

type sample = {
  k : int ;
  posterior : float ;
  likelihood : float ;
  prior : float ;
  treeLikelihood : float ;
  treeHeight : float ;
  clockRate : float ;
  bayesianSkyline : float ;
  groups : (int * float) list ;
}


let logchan (c : Csv.in_channel) : chan = c
let csvchan (c : chan) : Csv.in_channel = c


(* drop all lines starting with # *)
(* we lose the first character of the first uncommented line *)
let drop_commented chan_in =
  let rec f () =
    let c = input_char chan_in in
    if c = '#' then
      (ignore (input_line chan_in) ; f ())
    else
      ()
  in f ()


let ngroups_from_header l =
  let subl = L.filter (fun s -> S.starts_with s "bPopSizes") l in
  let istr = L.map (S.lchop ~n:10) subl in
  let i = L.map int_of_string istr in
  L.fold_left max 0 i


let read_row ngroups r =
  let ios = int_of_string in
  let fos = float_of_string in
  let soi = string_of_int in
  let from conv col row =
    let x = Csv.Row.find row col
    in conv x
    (* conv (Csv.Row.find row col) *)
  in {
    (* small subtility here : we've lost the first 'S' *)
    k = from ios "ample" r ;
    posterior = from fos "posterior" r ;
    likelihood = from fos "likelihood" r ;
    prior = from fos "prior" r ;
    treeLikelihood = from fos "treeLikelihood" r ;
    treeHeight = from fos "TreeHeight" r ;
    clockRate = from fos "clockRate" r ;
    bayesianSkyline = from fos "BayesianSkyline" r ;
    groups = L.init ngroups (fun i ->
      let si = soi (i + 1) in
      (from ios ("bGroupSizes." ^ si) r,
       from fos ("bPopSizes." ^ si) r)
    ) ;
  }


let open_in fname =
  let chan_in = open_in fname in
  drop_commented chan_in ;
  logchan @@ (Csv.of_channel ~separator:'\t' ~has_header:true) @@ chan_in

let close_in chan =
  Csv.close_in @@ csvchan @@ chan

let ngroups chan =
  ngroups_from_header @@ Csv.Rows.header @@ csvchan @@ chan


let next ~ngroups chan =
  read_row ngroups (Csv.Rows.next (csvchan chan))


let burn ~n chan =
  let csv_chan = csvchan chan in
  let rec f = function
    | 0 ->
        ()
    | n when n < 0 ->
        invalid_arg "Trace.Log.burn : negative n"
    | n ->
        ignore (Csv.Rows.next csv_chan) ; f (n - 1)
  in f n


