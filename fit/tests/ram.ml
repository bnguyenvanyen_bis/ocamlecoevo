module Lac = Lacaml.D

module U = Util
module F = U.Float


(* Test adjust *)

let adj k chol u r =
  let gamma = 2. /. 3. in
  let target_ratio = 0.234 in
  let n = 1 in
  Fit.Ram.adjust ~gamma n k chol u (min 1. r -. target_ratio)


(* when triang and u are 0., adjusting does nothing *)
let%test _ =
  let n = 1 in
  let triang = Lac.Mat.make0 n n in
  let u = Lac.Vec.make0 n in
  adj 0 triang u 0.5 ;
  (u.{1} = 0.) && (triang.{1,1} = 0.)


(* when u is a typical value and the ratio is > target,
 * we increase the variance : more aggressive proposal *)
let%expect_test _ =
  let n = 1 in
  let triang = Lac.Mat.identity n in
  let u = Lac.Vec.make n 1. in
  adj 0 triang u 0.5 ;
  Printf.printf "%f" u.{1} ;
  [%expect{|
    1.000000 |}] ;
  Printf.printf "%f" triang.{1,1} ;
  [%expect{| 1.125167 |}]


(* when u is a typical value and the ratio is < target,
 * we decrease the variance : more conservative proposal *)
let%expect_test _ =
  let n = 1 in
  let triang = Lac.Mat.identity n in
  let u = Lac.Vec.make n 1. in
  adj 0 triang u 0.1 ;
  Printf.printf "%f" u.{1} ;
  [%expect{|
    1.000000 |}] ;
  Printf.printf "%f" triang.{1,1} ;
  [%expect{| 0.930591 |}]


(* when ratio = target_ratio, adjusting does nothing *)
let%test _ =
  let n = 1 in
  let triang = Lac.Mat.identity n in
  let u = Lac.Vec.make n 1. in
  adj 0 triang u 0.234 ;
  (triang.{1,1} = 1.)


(* Test posterior on basic 1D example,
 * and check that the result makes sense *)
let%expect_test _ =
  let output = Util.Out.{
      start = (fun _ _ -> ()) ;
      out = (fun k (ret, chol) ->
          if (k mod 100 = 0) then
            let s = string_of_float (Fit.Mcmc.return_to_sample ret) in
            Printf.printf "%i,%s,%f\n" k s chol.{1,1}
      ) ;
      return = (fun a b -> (a, b)) ;
  }
  in
  let prior = Fit.Dist.Flat in
  let draws = [|
    (fun _ dx x -> x +. dx) ;
  |]
  in
  let likelihood = Fit.Dist.Normal (10., F.Pos.of_float 0.01) in
  let seed = 0 in
  let par = 0. in
  let n_iter = 1_000 in
  let _, (ret, cholf) = Fit.Ram.posterior
    ~output
    ~prior
    ~draws
    ~likelihood
    ~seed
    ~n_iter
    par
  in
  [%expect{|
    0,1.85725363608,1.328909
    100,9.99283633318,1.484370
    200,9.72846401541,1.061412
    300,10.1504845504,0.922531
    400,10.0547285572,0.870710
    500,10.0039830273,0.817061
    600,10.0715158127,0.752445
    700,9.87654602438,0.729039
    800,10.116531681,0.730117
    900,9.8938027298,0.680302 |}] ;
  begin match ret with
  | Fit.Mcmc.Bad_proposal _ ->
      ()
  | Bad_prior { move = x ; _ }
  | Bad_likelihood { move = x ; _ }
  | Reject { move = x ; _ }
  | Accept { sample = x ; _ } ->
      Printf.printf "%f" x
  end ;
  [%expect{| 9.853608 |}] ;
  Lac.pp_mat Format.std_formatter cholf ;
  Format.pp_print_flush Format.std_formatter () ;
  [%expect{| 0.676352 |}]


(* Test posterior on simple 2D example,
 * and check that the result makes sense *)

let%expect_test _ =
  let output = Util.Out.{
      start = (fun _ _ -> ()) ;
      out = (fun k (ret, chol) ->
          if (k mod 100 = 0) then
            let v = Fit.Mcmc.return_to_sample ret in
            let s = string_of_float v.{1} in
            Printf.printf "%i,%s,%f\n" k s chol.{1,1}
      ) ;
      return = (fun a b -> (a, b)) ;
  }
  in
  let prior = Fit.Dist.Constant_flat (Lac.Vec.make0 2) in
  let draws = [|
    (fun _ dx v -> v.{1} <- v.{1} +. dx ; v) ;
    (fun _ dx v -> v.{2} <- v.{2} +. dx ; v) ;
  |]
  in
  let likelihood = Fit.Dist.Multinormal {
    store = None ;
    mean = Lac.Vec.of_array [| ~-. 1. ; 1. |] ;
    cov = ref (U.Chol (
        Lac.Mat.of_array [|
          [| 0.2 ; 0. |] ;
          [| 0.1 ; 0.15 |] ;
        |]
    )) ;
  }
  in
  let seed = 0 in
  let par = Lac.Vec.of_array [| 0. ; 0. |] in
  let n_iter = 1_000 in
  let _, (ret, cholf) = Fit.Ram.posterior
    ~output
    ~prior
    ~draws
    ~likelihood
    ~seed
    ~n_iter
    par
  in
  [%expect{|
    0,1.85725363608,0.894670
    100,0.0360737590015,0.251521
    200,-2.24156172838,0.170953
    300,-1.98238620206,0.125928
    400,-2.61897829844,0.100854
    500,-2.44081279968,0.081689
    600,-2.84890542693,0.068219
    700,-2.35245338455,0.057825
    800,-2.71063038934,0.050004
    900,-2.89354117562,0.044005 |}] ;
  let v = Fit.Mcmc.return_to_sample ret in
  Lac.pp_vec Format.std_formatter v ;
  Format.pp_print_flush Format.std_formatter () ;
  [%expect{|
    -2.94364
     -6.4138 |}] ;
  Lac.pp_mat Format.std_formatter cholf ;
  Format.pp_print_flush Format.std_formatter () ;
  [%expect{|
      0.0383364         0
    -0.00524345 0.0363317 |}]
