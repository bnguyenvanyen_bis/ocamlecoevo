open Sig


let convert
  ?(n_burn=0) ?(n_thin=1) ?(chan=Util.Csv.Channel.Out.standard ())
  ~columns ~extract =
  let extract k x =
    if (k >= n_burn) && (k mod n_thin) = 0 then
      extract k x
    else
      None
  in
  Util.Csv.convert ~columns ~extract ~return:(fun k x -> (k, x)) ~chan



let columns_sample = [
  "k" ;
  "outcome" ;
  "logprior" ;
  "loglik" ;
  "logv" ;
]


(* deprecated *)
(* an 'extract' function for MCMC samples *)
let extract_sample ~extract k res =
  let soi = string_of_int in
  let sof = string_of_float in
  let sum = L.fold_left (+.) 0. in
  let _th, outcome, move_o, logp, loglik, logv = Mcmc.(
    match res with
    | Bad_proposal { sample ; _ } ->
        sample, "bad_proposal", None, nan, nan, neg_infinity
    | Bad_prior { sample ; move ; move_logprior ; _ } ->
        sample, "bad_prior", Some move,
        sum move_logprior, nan, neg_infinity
    | Bad_likelihood { sample ; move ; move_logprior ; move_loglik ; _ } ->
        sample, "bad_likelihood", Some move,
        sum move_logprior, sum move_loglik, neg_infinity
    | Reject { sample ; move ; move_logprior ; move_loglik ; logv ; _ } ->
        sample, "reject", Some move,
        sum move_logprior, sum move_loglik, logv
    | Accept { sample ; logprior ; loglik ; logv ; _ } ->
        sample, "accept", None,
        sum logprior, sum loglik, logv
  )
  in
  (* FIXME what about the sample ? *)
  let ext =
    function
    | "k" ->
        soi k
    | "outcome" ->
        outcome
    | "logprior" ->
        sof logp
    | "loglik" ->
        sof loglik
    | "logv" ->
        sof logv
    | s ->
        begin match move_o with
        | None ->
            ""
        | Some th ->
            extract th s
        end
  in Some ext


let columns_sample_only = [
  "k" ;
  "logprior" ;
  "loglik" ;
]


let extract_sample_only ~extract k res =
  let soi = string_of_int in 
  let sof = string_of_float in
  let sum = L.fold_left (+.) 0. in
  let logp, loglik, th = Mcmc.(
    match res with
    | Bad_proposal { sample ; logprior ; loglik }
    | Bad_prior { sample ; logprior ; loglik ; _ }
    | Bad_likelihood { sample ; logprior ; loglik ; _ }
    | Reject { sample ; logprior ; loglik ; _ }
    | Accept { sample ; logprior ; loglik ; _ } ->
        sum logprior, sum loglik, sample
  )
  in
  let ext =
    function
    | "k" ->
        soi k
    | "logprior" ->
        sof logp
    | "loglik" ->
        sof loglik
    | s ->
        extract th s
  in Some ext


let columns_accepted = [
  "k" ;
  "outcome" ;
  "logprior" ;
  "loglik" ;
  "logv" ;
]


let extract_accepted ~extract k =
  let soi = string_of_int in
  let sof = string_of_float in
  let sum = L.fold_left (+.) 0. in
  Mcmc.(function
  | Bad_proposal _
  | Bad_prior _
  | Bad_likelihood _
  | Reject _ ->
      None
  | Accept { sample ; logprior ; loglik ; logv ; _ } ->
      Some (
        function
        | "k" ->
            soi k
        | "outcome" ->
            "accept"
        | "logprior" ->
            sof (sum logprior)
        | "loglik" ->
            sof (sum loglik)
        | "logv" ->
            sof logv
        | s ->
            extract sample s
      )
  )


let columns_sample_move = [
  "k" ;
  "outcome" ;
  "logprior" ;
  "loglik" ;
  "logu" ;
  "p'p" ;
  "qq'" ;
  "l'l" ;
  "logv" ;
]


let extract_sample_move ~extract k res =
  let soi = string_of_int in 
  let sof = string_of_float in
  let sum = L.fold_left (+.) 0. in
  let sample, outcome, logprior, loglik, logu, p'p, qq', l'l, logv, move_o =
    Mcmc.(
    match res with
    | Bad_proposal { sample ; logprior ; loglik } ->
        sample, "bad_proposal", logprior, loglik, 0.,
        nan, neg_infinity, nan, neg_infinity, None
    | Bad_prior { sample ; logprior ; loglik ; move ; _ } ->
        sample, "bad_prior", logprior, loglik, 0.,
        neg_infinity, nan, nan, neg_infinity, Some move
    | Bad_likelihood { sample ; logprior ; loglik ; move ; p'p ; qq' ; _ } ->
        sample, "bad_likelihood", logprior, loglik, 0.,
        p'p, qq', neg_infinity, neg_infinity, Some move
    | Reject { sample ; logprior ; loglik ; move ; p'p ; qq' ; l'l ; logv ; logu ; _ } ->
        sample, "reject", logprior, loglik, logu,
        p'p, qq', l'l, logv, Some move
    | Accept { sample ; logprior ; loglik ; p'p ; qq' ; l'l ; logv ; logu ; _ } ->
        sample, "accept", logprior, loglik, logu,
        p'p, qq', l'l, logv, Some sample
  )
  in
  let ext =
    function
    | "k" ->
        soi k
    | "outcome" ->
        outcome
    | "logprior" ->
        sof (sum logprior)
    | "loglik" ->
        sof (sum loglik)
    | "logu" ->
        sof logu
    | "p'p" ->
        sof p'p
    | "qq'" ->
        sof qq'
    | "l'l" ->
        sof l'l
    | "logv" ->
        sof logv
    | s ->
        begin match Scanf.sscanf s "move_%s" (fun s' -> s') with
        | s' ->
            begin match move_o with
            | Some move ->
                extract move s'
            | None ->
                ""
            end
        | exception Scanf.Scan_failure _ ->
            extract sample s
        end
  in Some ext


let convert_array ?(n_burn=0) ?(n_thin=1) chan =
  let output k a =
    let n = A.length a in
    Printf.fprintf chan "%i," k ;
    for i = 1 to n do
      Printf.fprintf chan "%f," a.(i)
    done ;
    Printf.fprintf chan "\n"
  in
  let start k0 x0 =
    output k0 x0
  in
  let out k x =
    if (k >= n_burn) && (k mod n_thin = 0) then
      output k x
  in
  let return kf xf =
    (kf, xf)
  in
  Util.Out.{ start ; out ; return }


let convert_lower_triang ?(n_burn=0) ?(n_thin=1) chan =
  let n_r = ref None in
  let output k mat =
    let n = Lac.Mat.dim1 mat in
    assert (Lac.Mat.dim2 mat = n) ;
    assert (Some n = !n_r) ;
    let line = U.int_fold ~f:(fun s i ->
        let i' = n - i + 1 in
        U.int_fold ~f:(fun s' j ->
          let j' = i' - j + 1 in
          Printf.sprintf "%s,%f" s' mat.{i',j'}
        ) ~x:s ~n:i'
      ) ~x:(string_of_int k) ~n
    in
    Printf.fprintf chan "%s\n" line
  in
  let start k0 mat0 =
    let n = Lac.Mat.dim1 mat0 in
    assert (Lac.Mat.dim2 mat0 = n) ;
    n_r := Some n ;
    let header = U.int_fold ~f:(fun s i ->
        let i' = n - i + 1 in
        U.int_fold ~f:(fun s' j ->
          let j' = i' - j + 1 in
          Printf.sprintf "%s,{%i|%i}" s' i' j'
        ) ~x:s ~n:i'
      ) ~x:"k" ~n
    in
    Printf.fprintf chan "%s\n" header ;
    output k0 mat0
  in
  let out k x =
    if (k >= n_burn) && (k mod n_thin = 0) then
      output k x
  in
  let return kf xf =
    (kf, xf)
  in
  Util.Out.{ start ; out ; return }
