open Sig


type _ t =
  | Base : {
      draw : (Random.State.t -> 'a -> 'a) ;
      log_pd_ratio : ('a -> 'a -> float list) ;
      (** log_pd_ratio x x' is logP(x | x') - logP(x' | x) *)
    } -> 'a t
  | Dist :
      ('a -> 'a Dist.t) -> 'a t
  | Constant_flat :
      'a t
  | Option :
      'a t -> 'a option t
  | Conditional : {
      depends : 'a Dist.t ;
      draw : (Random.State.t -> 'a -> 'b -> 'b) ;
      (* the log ratio should take depends into account *)
      log_pd_ratio : ('b -> 'b -> float list) ;
    } -> 'b t
  | Also : {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      sub : 'b t ;
      base : 'a t
    } -> 'a t
    (* dimensions should remain independent *)


let rec draw_from : type v. Random.State.t -> v -> v t -> v = fun rng x ->
  function
  | Dist f ->
      Dist.draw_from rng (f x)
  | Base { draw ; _ } ->
      draw rng x
  | Constant_flat ->
      x
  | Option prop ->
      U.Option.map (fun y -> draw_from rng y prop) x
  | Conditional { depends ; draw ; _ } ->
      draw rng (Dist.draw_from rng depends) x
  | Also { get ; set ; sub ; base } ->
      let x' = draw_from rng x base in
      let y = get x' in
      let y' = draw_from rng y sub in
      set x' y'


(* log P(x | x') - log P(x' | x) *)
let rec log_dens_proba_ratio : type v. v t -> v -> v -> float list = fun prop x x' ->
  match prop with
  | Dist f ->
      let ldp = Dist.log_dens_proba (f x') x in
      let ldp' = Dist.log_dens_proba (f x) x' in
      Dist.partsub ldp ldp'
  | Base { log_pd_ratio ; _ } ->
      log_pd_ratio x x'
  | Constant_flat ->
      [0.]
  | Option prop' ->
      begin match x, x' with
      | None, None ->
          [0.]
      | Some _, None
      | None, Some _ ->
          [neg_infinity]
      | Some y, Some y' ->
          log_dens_proba_ratio prop' y y'
      end
  | Conditional { log_pd_ratio ; _ } ->
      log_pd_ratio x x'
  | Also { get ; sub ; base ; _ } ->
      (* independent *)
        (log_dens_proba_ratio base x x')
      @ (log_dens_proba_ratio sub (get x) (get x'))


let multi_gaussian_chol ?store_std ?store draw log_pd_ratio c =
  let chol = ref (U.Chol c) in
  let depends = Dist.Multinormal_center { store_std ; store ; cov = chol } in
  Conditional { depends ; draw ; log_pd_ratio }


let multi_gaussian_id ?store_std ?store draw log_pd_ratio beta c =
  let n = Lac.Mat.dim1 c in
  let c' = Lac.Mat.identity n in
  Lac.Mat.scal (U.sq (beta *. 0.1) /. float n) c' ;
  Lac.Mat.axpy ~alpha:(U.sq ((1. -. beta) *. 2.38) /. float n) c c' ;
  let cov = ref (U.Cov c') in
  let depends = Dist.Multinormal_center { store_std ; store ; cov } in
  Conditional { depends ; draw ; log_pd_ratio }


let vector_draw draws rng v x =
  A.fold_lefti (fun x' i d -> d rng v.{i + 1} x') x draws
