open Sig

(* FIXME propagate the U.Float/Int types to return types *)
(* for now _float. can we extend ? *)

(* FIXME should we use richer function return types + Eval
 * instead of passing args ? 
 * We don't even necessarily need Eval.
 * It would make at least Normal proposals and the like look cleaner
 * (no need to go through the constructor each time) *)

(** Distributions, that we can draw from and compute log-likelihoods on. *)
type _ dist =
  | Base : 
      { draw : (Random.State.t -> 'a) ;
        log_dens_proba : ('a -> float list) } -> 'a dist
  (** User-defined distribution *)
  | Choose :
      'a list -> 'a dist
  (** Uniform choice in list *)
  | Constant :
      'a -> 'a dist  (* careful with floats *)
  (** Certain variable [x]. 
      The likelihood is [0.] for [y <> x]. *)
  | Constant_flat :
      'a -> 'a dist
  (** Certain variable [x].
      The likelihood is always [1.] (improper). *)
  | Bernoulli :
      U.anyproba -> bool dist
  (** Bernoulli variable of proba [p]. *)
  | Iuniform :
      int * int -> int dist
  (** Uniform variable in integer interval. *)
  | Binomial :
      U.anyposint * U.anyproba -> int dist
  (** Binomial variable. *)
  | Poisson :
      'a U.anypos -> int dist
  (** Poisson variable. *)
  | Negbin :
      'a U.anypos * U.anyproba -> int dist
  (** Negative binomial variable. *)
  | Flat :
      'a dist
  (** Flat distribution : cannot draw from it,
      and the likelihood is [1.] everywhere. *)
  | Uniform :
      float * float -> float dist
  (** Uniform distribution in [[a, b]]. *)
  | Exponential :
      'a U.anypos -> float dist
  (** Exponential distribution with rate [lbd]. *)
  | Normal :
      float * 'a U.anypos -> float dist
  (** Normal distribution. *)
  | Lognormal :
      float * 'a U.anypos -> float dist
  (** Lognormal distribution. *)
  | Bound :
      { low : float ;
        high : float ;
        probin : float ;
        base : float dist } -> float dist
  (** Bounded version of [base] in [[low, high]],
      with [P_base([low, high]) = probin]. *)
  | Multinormal : {
      store : Lac.Vec.t option ;
      mean : Lac.Vec.t ;
      cov : U.covariance ;
    } -> Lac.Vec.t dist
  (** Multinormal distribution *)
  | Multinormal_center : {
      store_std : Lac.Vec.t option ;
      store : Lac.Vec.t option ;
      cov : U.covariance ;
    } -> Lac.Vec.t dist
  (** Proposal multinormal distribution *)
  | Also :
      { get : ('a -> 'b) ;
        set : ('a -> 'b -> 'a) ;
        sub : 'b dist ;
        base : 'a dist } -> 'a dist
  (** Draw from [base] distribution, then from [sub] *)
  | Option :
      U.anyneg * 'a dist -> 'a option dist
  (** [Option (logp, d)] is [None] with probability [exp logp],
   *  and otherwise [Some d]. *)


type 'a t = 'a dist


let variance x : 'a U.pos = F.of_float_unsafe x


let rec draw_from : type v. Random.State.t -> v dist -> v = fun rng ->
  function
  | Base { draw ; _ } ->
      draw rng
  | Choose l ->
      let n = Random.int (List.length l) in
      List.nth l n
  | Constant x ->
      x
  | Constant_flat x ->
      x
  | Bernoulli p ->
      U.rand_bernoulli ~rng p
  | Iuniform (low, high) ->
      (high - low)
      |> U.Int.Pos.of_int
      |> U.rand_int ~rng
      |> U.Int.to_int
      |> (fun n -> low + n)
  | Binomial (n, p) ->
      U.rand_binomial ~rng n p
  | Poisson lbd ->
      (* for now we don't propagate the U types *)
      U.Int.to_int (U.rand_poisson ~rng lbd)
  | Negbin (r, p) ->
      U.Int.to_int (U.rand_negbin ~rng r p)
  | Flat ->
      raise (Draw_error "invalid flat distribution")
  | Uniform (low, high) ->
      (high -. low)
      |> F.Pos.of_float
      |> U.rand_float ~rng
      |> F.to_float
      |> (fun x -> low +. x)
  | Exponential lbd ->
      F.to_float (U.rand_exp ~rng lbd)
  | Normal (m, v) ->
      U.rand_normal ~rng m v
  | Lognormal (m, v) ->
      U.rand_lognormal ~rng m v
  | Bound { low ; high ; base ; _ } as d ->
      let x = draw_from rng base in
      (* ! doesn't necessarily terminate *)
      if (low <= x) && (x <= high) then x else draw_from rng d
  | Multinormal { store ; mean ; cov } ->
      begin match U.rand_multi_normal ~rng ?store mean cov with
      | x ->
          x
      | exception Failure _ -> (* assume Cholesky failure *)
          Printf.eprintf
            "invalid covariance matrix for Cholesky decomposition" ;
          raise (Draw_error
            "invalid covariance matrix for Cholesky decomposition"
          )
      end
  | Multinormal_center { store_std ; store ; cov } ->
      let n = U.dim1 cov in
      let m = Lac.Vec.make0 n in
      begin match U.rand_multi_normal ~rng ?store_std ?store m cov with
      | x ->
          x
      | exception Failure _ ->
          raise (Draw_error
            "invalid covariance matrix for Cholesky decomposition"
          )
      end
  | Also { set ; sub ; base ; _ } ->
      set (draw_from rng base) (draw_from rng sub)
  | Option (logp, d') ->
      if U.rand_bernoulli ~rng (F.Neg.exp logp) then
        None
      else
        Some (draw_from rng d')


let sum l =
  List.fold_left (+.) 0. l




let partmap2 f l l' =
  let rec g nl l l' =
    match l, l' with
    | [], [] -> nl
    | _ :: _, [] -> (List.rev l) @ nl
    | [], _ :: _ -> (List.rev l') @ nl
    | x :: tl, x' :: tl' -> g ((f x x') :: nl) tl tl'
  in List.rev (g [] l l')


let partsub ldp ldp' =
  let dn = L.length ldp - L.length ldp' in
  if dn = 0 then
    L.map2 (-.) ldp ldp'
  else if dn > 0 then
    let zeros = L.make dn 0. in
    L.map2 (-.) ldp (ldp' @ zeros)
  else
    let zeros = L.make (~- dn) 0. in
    L.map2 (-.) (ldp @ zeros) ldp'


let rec string_of : type v. v dist -> string =
  function
  | Base _ ->
      "?"
  | Choose _ ->
      "U(l)"
  | Constant _ ->
      "delta"
  | Constant_flat _ ->
      "_delta"
  | Bernoulli p ->
      Printf.sprintf "B(%f)" (F.to_float p)
  | Iuniform (low, high) ->
      Printf.sprintf "IU([%i,%i])" low high
  | Binomial (n, p) ->
      Printf.sprintf "B(%i,%f)" (U.Int.to_int n) (F.to_float p)
  | Poisson lbd ->
      Printf.sprintf "P(%f)" (F.to_float lbd)
  | Negbin (r, p) ->
      Printf.sprintf "NB(%f,%f" (F.to_float r) (F.to_float p)
  | Flat ->
      "flat"
  | Uniform (low, high) ->
      Printf.sprintf "U([%f,%f])" low high
  | Exponential lbd ->
      Printf.sprintf "E(%f)" (F.to_float lbd)
  | Normal (m, v) ->
      Printf.sprintf "N(%f,%f)" m (F.to_float v)
  | Lognormal (m, v) ->
      Printf.sprintf "LN(%f,%f)" m (F.to_float v)
  | Bound { low ; high ; base ; _ } ->
      Printf.sprintf "%s AND IN [%f,%f]" (string_of base) low high
  | Multinormal _ ->
      (* FIXME print m and cov *)
      "MN"
  | Multinormal_center _ ->
      "MNC"
  | Also { sub ; base ; _ } ->
      Printf.sprintf "%s/(%s)" (string_of base) (string_of sub)
  | Option (_, d') ->
      Printf.sprintf "?(%s)" (string_of d')


let rec dens_proba : type v. v dist -> v -> float list = fun d x ->
  match d with
  | Base { log_dens_proba ; _ } ->
      List.map exp (log_dens_proba x)
  (* probability *)
  | Choose l ->
      let lik =
        if List.mem x l then
          ~-. (float @@ List.length @@ l)
        else
          0.
      in [lik]
  | Constant x' ->
      let lik =
        if x = x' then
          1.
        else
          0.
      in [lik]
  | Constant_flat _ ->
      [1.]
  | Bernoulli p ->
      [F.to_float (U.p_bernoulli p x)]
  | Iuniform (low, high) ->
      [F.to_float (U.d_unif_int low high x)]
  | Binomial (n, p) ->
      [F.to_float (U.p_binomial n p x)]
  | Poisson lbd ->
      [F.to_float (U.p_poisson lbd x)]
  | Negbin (r, p) ->
      [F.to_float (U.p_negbin r p x)]
  | Flat ->
      [1.]
  (* density *)
  | Uniform (low, high) ->
      [F.to_float (U.d_unif low high x)]
  | Exponential lbd ->
      [F.to_float (U.d_exp lbd x)]
  | Normal (m, v) ->
      [F.to_float (U.d_normal m v x)]
  | Lognormal (m, v) ->
      [F.to_float (U.d_lognormal m v x)]
  | Bound { low ; high ; probin ; base } ->
      if (low <= x) && (x <= high) then
        (1. /. probin) :: (dens_proba base x)
      else
        0. :: (dens_proba base x)
  | Multinormal { mean ; cov ; _ } ->
      [F.to_float (U.d_multi_normal mean cov x)]
  | Multinormal_center { cov ; _ } ->
      [F.to_float (U.d_multi_normal_center cov x)]
  | Also { get ; sub ; base ; _ } ->
      (dens_proba base x) @ (dens_proba sub (get x))
  | Option (logp, d') ->
      begin match x with
      | Some x ->
          F.to_float (F.Proba.compl (F.Neg.exp logp))
          :: (dens_proba d' x)
      | None ->
          [F.to_float (F.Neg.exp logp)]
      end


let rec log_dens_proba : type v. v dist -> v -> float list = fun d x ->
  match d with
  | Base { log_dens_proba ; _ } ->
      log_dens_proba x
  | Choose l ->
      let loglik =
        if List.mem x l then
          ~-. (log @@ float @@ List.length @@ l)
        else
          neg_infinity
      in [loglik]
  | Constant x' ->
      let loglik =
        if x = x' then
          0.
        else
          neg_infinity
      in [loglik]
  | Constant_flat _ ->
      [0.]
  | Bernoulli p ->
      [F.to_float (U.logp_bernoulli p x)]
  | Iuniform (low, high) ->
      [F.to_float (U.logd_unif_int low high x)]
  | Binomial (n, p) ->
      [F.to_float (U.logp_binomial n p x)]
  | Poisson lbd ->
      [F.to_float (U.logp_poisson lbd x)]
  | Negbin (r, p) ->
      [F.to_float (U.logp_negbin r p x)]
  | Flat ->
      [0.]
  | Uniform (low, high) ->
      [F.to_float (U.logd_unif low high x)]
  | Exponential lbd ->
      [F.to_float (U.logd_exp lbd x)]
  | Normal (m, v) ->
      [F.to_float (U.logd_normal m v x)]
  | Lognormal (m, v) ->
      [F.to_float (U.logd_lognormal m v x)]
  | Bound { low ; high ; probin ; base } ->
      if (low <= x) && (x <= high) then
        ~-. (log probin) :: (log_dens_proba base x)
      else
        begin
          (* Printf.eprintf "x=%f outside [low,high]=[%f,%f]\n" x low high ; *)
          (* FIXME second term for homogeneity *)
          neg_infinity :: (log_dens_proba base x)
        end
  | Multinormal { mean ; cov ; _ } ->
      [F.to_float (U.logd_multi_normal mean cov x)]
  | Multinormal_center { cov ; _ } ->
      [F.to_float (U.logd_multi_normal_center cov x)]
  | Also { get ; sub ; base ; _ } ->
      (log_dens_proba base x) @ (log_dens_proba sub (get x))
  | Option (logp, d') ->
      begin match x with
      | Some x ->
          (log (F.to_float (F.Proba.compl (F.Neg.exp logp))))
          :: (log_dens_proba d' x)
      | None ->
          [F.to_float logp]
      end


let sample rng ~n d =
  let rec f acc = function
    | 0 -> acc
    | k -> f (draw_from rng d :: acc) (k - 1)
  in f [] n


let eprint ?name values =
  begin match name with
  | None ->
      ()
  | Some s ->
      Printf.eprintf "%s\n" s
  end ;
  List.iter (Printf.eprintf "%f ") values ;
  Printf.eprintf "\n"


let assert_no_neg_infty ?name l =
  try
    List.iter (fun x -> assert (x <> neg_infinity)) l
  with Assert_failure _ as e ->
    begin
      eprint ?name l ;
      raise e
    end


let assert_no_nan ?name l =
  try
    List.iter (fun x -> assert (classify_float x <> FP_nan)) l
  with Assert_failure _ as e ->
    begin
      eprint ?name l ;
      raise e
    end


let and_dist sub base =
  Also {
    get=(fun x -> x) ;
    set=(fun _ x -> x) ;
    sub ;
    base ;
  }


let and_proposal p' p =
  (fun x -> and_dist (p' x) (p x))


let vector_getset getsets =
  let n = A.length getsets in
  let v = Lac.Vec.make0 n in
  let get th =
    A.iteri (fun i (get, _) -> v.{i+1} <- get th) getsets ;
    v
  in
  let set th v =
    A.fold_lefti (fun th' i (_, set) -> set th' v.{i+1}) th getsets
  in
  (get, set)


let vector_getset_random getsets =
  let n = A.length getsets in
  let v = Lac.Vec.make0 n in
  let get th =
    A.iteri (fun i (get, _) -> v.{i+1} <- get th) getsets ;
    v
  in
  let set rng th v =
    A.fold_lefti (fun th' i (_, set) -> set rng th' v.{i+1}) th getsets
  in
  (get, set)


let vector_set_trmv getsets triang =
  Printf.eprintf "vector_set_trmv : %i\n%!" (BatArray.length getsets) ;
  let set th v =
    Lac.trmv ~up:false triang v ;
    BatArray.fold_lefti (fun th' i (_, set) -> set th' v.{i+1}) th getsets
  in
  set
