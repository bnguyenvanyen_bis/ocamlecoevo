module P = Param

type 'a t =
  | Free of [`Custom | `Adapt | `Prior]
  | Fixed of 'a


let custom = Free `Custom
let adapt = Free `Adapt
let prior = Free `Prior

let free mode = Free mode
let fixed x = Fixed x


let only_custom ~tag ~get ~set ~prior ~proposal =
  function
  | Free `Custom ->
      P.Custom { tag ; get ; set ; prior ; proposal }
  | Free (`Adapt | `Prior) ->
      invalid_arg "only custom"
  | Fixed value ->
      P.Fixed { tag ; get ; set ; value }


let maybe_adaptive 
  (type a) ~tag ~(get : ('par -> a)) ~set ~prior ~proposal ~jumps =
  function
  | Free adapt ->
      begin match adapt with
      | `Adapt ->
          P.Adaptive_fragments { tag ; get ; set ; prior ; jumps }
      | `Custom ->
          P.Custom { tag ; get ; set ; prior ; proposal }
      | `Prior ->
          let proposal = Propose.Dist (fun _ -> prior) in
          P.Custom { tag ; get ; set ; prior ; proposal }
      end
  | Fixed value ->
      P.Fixed { tag ; get ; set ; value }


let maybe_adaptive_random
  (type a)
  ~tag
  ~(get : ('par -> a))
  ~set
  ~prior
  ~proposal
  ~jumps
  ~log_pd_ratio =
  function
  | Free adapt ->
      begin match adapt with
      | `Adapt ->
          P.Adaptive_frag_rand {
            tag ; get ; set ; prior ; log_pd_ratio ; jumps
          }
      | `Custom ->
          P.Custom { tag ; get ; set ; prior ; proposal }
      | `Prior ->
          let proposal = Propose.Dist (fun _ -> prior) in
          P.Custom { tag ; get ; set ; prior ; proposal }
      end
  | Fixed value ->
      P.Fixed { tag ; get ; set ; value }



let maybe_adaptive_float ~tag ~get ~set ~prior ~proposal ~jump =
  function
  | Free adapt ->
      begin match adapt with
      | `Adapt ->
          P.Adaptive { tag ; get ; set ; prior ; jump }
      | `Custom ->
          P.Custom { tag ; get ; set ; prior ; proposal }
      | `Prior ->
          let proposal = Propose.Dist (fun _ -> prior) in
          P.Custom { tag ; get ; set ; prior ; proposal }
      end
  | Fixed value ->
      P.Fixed { tag ; get ; set ; value }


