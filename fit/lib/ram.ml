(** Robust Adaptive Metropolis algorithm
 *
 *  Vihola, Mcmc., 2012.
 *  Robust adaptive Metropolis algorithm with coerced acceptance rate.
 *  Statistics and Computing, 22(5), pp.997-1008.
 *
 *  This is the same algorithm as the Metropolis-Hastings in Mcmc,
 *  but the proposal is multivariate normal with a robust-pseudo-covariance
 *  updated every iteration to reach a target acceptance rate.
 *
 *)


open Sig


let eta gamma k =
  (float k) ** (~-. gamma)


let adjust ?(k0=0) ~gamma n =
  let storage = Lac.Mat.make0 n n in
  (* we re-use memory for x, which is updated by chol_update,
   * then we discard on next copy *)
  let x_mem = Lac.Vec.make0 n in
  (* We make a Cholesky up/down date
   * S S_T +/- f(eta,k) Delta_ratio / || u || ** 2 S U U_T S_T
   * which is
   * S S_T +/- (a S U) * (a S U)_T
   * with
   * a = sqrt(f(eta,k) |Delta_ratio|) / || u ||
   *)
  let adj k triang u d =
    (* keep a snapshot of triang in storage *)
    let _ = Lac.lacpy ~b:storage triang in
    let delta =
      sqrt ((min 1. (float n *. eta gamma (k0 + k))) *. abs_float d)
    in
    (* alpha = a *)
    let alpha = delta /. (Lac.nrm2 u) in
    (* u <- triang * u * delta / ||u|| *)
    (* use x for next operations (stored in x_mem) *)
    let x = Lac.copy ~y:x_mem u in
    (* FIXME if dim is 0 it fails here *)
    Lac.trmv ~up:false triang x ;
    Lac.scal alpha x ;
    (* x now contains [a S U] *)
    let up =
      if d > 0. then
        `Up
      else
        `Down
    in
    (* on a downdate, this can create an invalid matrix
     * we force the diagonal elements to be > 0
     * we should actually force the eigenvalues to be in [a,b]
     * to ensure stability *)
    U.chol_update ~up triang x ;
    let dmin = Lac.Vec.min (Lac.Mat.copy_diag triang) in
    let sum = Lac.Mat.sum triang in
    let cfsum = classify_float sum in
    (* if the new triang is bad *)
    if (dmin <= 0.)
    || (cfsum = FP_infinite)
    || (cfsum = FP_nan) then
      (* restore it from storage *)
      ignore (Lac.lacpy ~b:triang storage) ;
  in
  if n = 0 then
    (fun _ _ _ _ -> ())
  else
    adj


let sample ?(adapt=true) ?(gamma=(2./.3.)) ?(target_ratio=0.234) ?k0 ?proposal
           ?(log_pd_ratio=(fun _ _ -> [0.]))
           ~chol ~prior ~draws ~likelihood =
  let n = A.length draws in
  (* chol should be a square matrix *)
  assert (Lac.Mat.dim1 chol = Lac.Mat.dim2 chol) ;
  (* u will contain the last multi normal standard draw *)
  let u = Lac.Vec.make0 n in
  (* v will contain the last multi normal centered draw *)
  let v = Lac.Vec.make0 n in
  let update k logv =
    let d = (min 1. (exp logv)) -. target_ratio in
    (* let d = atan (logv -. log target_ratio) in *)
    (* let d = exp (logv -. log target_ratio) -. 1. in *)
    adjust ?k0 ~gamma n k chol u d
  in
  let draw = Propose.vector_draw draws in
  let multi =
    Propose.multi_gaussian_chol ~store_std:u ~store:v draw log_pd_ratio chol
  in
  let prop =
    match proposal with
    | None ->
        multi
    | Some prop ->
        Propose.Also {
          get = (fun x -> x) ;
          set = (fun _ x -> x) ;
          sub = multi ;
          base = prop ;
        }
  in
  let sample_f = Mcmc.sample prior prop likelihood in
  (fun ~rng k smpl ->
     let res = sample_f ~rng smpl in
     if adapt then begin Mcmc.(
       match res with
       | Bad_proposal _
       | Bad_prior _ 
       | Bad_likelihood _ ->
           update k neg_infinity
       | Reject { logv ; _ }
       | Accept { logv ; _ } ->
           update k logv
     )
     end ;
     res
  )


let posterior ~(output : ('a, 'b) output)
              ~prior ?proposal ?log_pd_ratio ~draws ~likelihood
              ?adapt ?gamma ?target_ratio ?k0 ?chol
              ?seed ~n_iter
              par =
  let chol =
    match chol with
    | Some c ->
        c
    | None ->
        Lac.Mat.identity (A.length draws)
  in
  let sample_f = sample
      ?adapt
      ?gamma
      ?target_ratio
      ?proposal
      ?k0
      ~chol
      ~prior
      ?log_pd_ratio
      ~draws
      ~likelihood
  in
  let rng = U.rng seed in
  let rec to_the_end smpl = function
    | 0 ->
        smpl
    | i when i < 0 ->
        invalid_arg "to_the_end : negative i"
    | i ->
        let res = sample_f ~rng (n_iter - i) smpl in
        output.out (n_iter - i) (res, chol) ;
        (* oevery_chol ~n_thin chan' i chol ; *)
        Mcmc.(match res with
           | Bad_proposal _
           | Bad_prior _
           | Bad_likelihood _
           | Reject _ ->
               to_the_end smpl (i - 1)
           | Accept { sample ; logprior ; loglik ; _ } ->
               to_the_end { sample ; logprior ; loglik } (i - 1)
        )
  in
  let init = Mcmc.init prior likelihood in
  let smpl0 = init par in
  output.start 0 (Mcmc.accepted_sample smpl0, chol) ;
  let smplf = to_the_end smpl0 n_iter in
  output.return n_iter (Mcmc.accepted_sample smplf, chol)
