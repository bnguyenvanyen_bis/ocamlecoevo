(** Acceptance-rejection algorithm *)

open Sig


let sum xs = F.of_float (L.fold_left (+.) 0. xs)


let sample ~rng proposal target_log_dens_proba =
  let x = Dist.draw_from rng proposal in
  let log_prop = sum (Dist.log_dens_proba proposal x) in
  let log_target = target_log_dens_proba x in
  match F.Neg.of_anyfloat F.Op.(log_target - log_prop) with
  | log_v ->
      let log_u = F.Pos.log (U.rand_float ~rng F.one) in
      if F.Op.(log_u <= log_v) then
        Some x
      else
        None
  | exception Invalid_argument _ ->
      (* log_v is positive *)
      invalid_arg
      "Fit.Acrej.sample : target density greater than proposal scaled density"


let posterior ?seed ~(output : ('a, 'b) output) ~proposal ~target ~n_iter =
  let rng = U.rng seed in
  output.start 0 None ;
  U.int_fold ~f:(fun () i ->
    let x = sample ~rng proposal target in
    output.out (n_iter - i) x ;
    ()
  ) ~x:() ~n:n_iter ;
  output.return n_iter None
