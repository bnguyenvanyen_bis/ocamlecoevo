open Sig


type (_, _) eq = Eq : ('a, 'a) eq


(* set and jump *)
module SJ =
  struct
    (*
     * set should be symmetric in the sense that
     * set x par = par' <=> set (~-. x) par' = par
     *)
    type 'par t = {
      set : (float -> 'par -> 'par) ;
      jump : float ;
    }


    let map (type par) ~get ~set (sj : par t) =
      let set' x th =
        set th (sj.set x (get th))
      in
      { set = set' ;
        jump = sj.jump }
  end


(* draw and jump *)
module DJ =
  struct
    type 'par t = {
      draw : (U.rng -> float -> 'par -> 'par) ;
      jump : float ;
    }

    let map (type par) ~get ~set (rj : par t) =
      let draw' rng x th =
        set th (rj.draw rng x (get th))
      in
      { draw = draw' ;
        jump = rj.jump }
  end


type (_, _, _) t =
  | Fixed : {
      tag : 'tag ;
      get : ('par -> 'a) ;
      set : ('par -> 'a -> 'par) ;
      value : 'a
    } -> ('tag, 'par, 'a) t
  (** When the value is fixed for this run *)
  | Custom : {
      tag : 'tag ;
      get : ('par -> 'a) ;
      set : ('par -> 'a -> 'par) ;
      prior : 'a Dist.t ;
      proposal : 'a Propose.t
    } -> ('tag, 'par, 'a) t
  (** With a custom proposal *)
  | Adaptive : {
      tag : 'tag ;
      get : ('par -> float) ;
      set : ('par -> float -> 'par) ;
      prior : float Dist.t ;
      jump : float
    } -> ('tag, 'par, float) t
  (* Here also set should be symmetric
   * set par x = par' <=> set par' (~-. x) = par
   *)
  (** With adaptive proposal *)
  | Adaptive_fragments : {
      tag : 'tag ;
      get : ('par -> 'a) ;
      set : ('par -> 'a -> 'par) ;
      prior : 'a Dist.t ;
      jumps : 'par SJ.t list
    } -> ('tag, 'par, 'a) t
  (** For multi-dim adaptive proposal *)
  | Adaptive_frag_rand : {
      tag : 'tag ;
      get : ('par -> 'a) ;
      set : ('par -> 'a -> 'par) ;
      prior : 'a Dist.t ;
      log_pd_ratio : ('par -> 'par -> float list) option ;
      jumps : 'par DJ.t list
    } -> ('tag, 'par, 'a) t
  (** For multi-dim conditional adaptive proposal *)


type ('tag, 'par, 'a) fitparam = ('tag, 'par, 'a) t


let prior_elt : type a. ('tag, 'par, a) t -> a Dist.t =
  function
  | Fixed { value ; _ } ->
      Dist.Constant value
  | Custom { prior ; _ } ->
      prior
  | Adaptive { prior ; _ } ->
      prior
  | Adaptive_fragments { prior ; _ } ->
      prior
  | Adaptive_frag_rand { prior ; _ } ->
      prior


module List =
  struct
    type (_, _, _, _) t =
      | [] :
          ('tag, 'par, 'v, 'v) t
      | (::) :
          ('tag, 'par, 'a) fitparam * ('tag, 'par, 'ty, 'v) t ->
            ('tag, 'par, 'a * 'ty, 'v) t


    type ('tag, 'par, 'b) poly =
      { f : 'a. 'b -> ('tag, 'par, 'a) fitparam -> 'b }


    let rec fold_left :
      type a v. ('tag, 'par, 'b) poly -> 'b -> ('tag, 'par, a, v) t -> 'b =
      fun { f } y ->
      function
      | [] ->
          y
      | x :: tl ->
          fold_left { f } (f y x) tl


    let rec append :
      type a b c.
      ('t, 'p, a, b) t -> ('t, 'p, b, c) t -> ('t, 'p, a, c) t =
      fun l l' ->
      match l with
      | [] ->
          l'
      | h :: tl ->
          h :: append tl l'

    let (@) = append
  end


module Combine =
  struct
    module D = Dist

    let prior :
      type tag par a. par D.t -> (tag, par, a) t -> par D.t =
      fun base ->
      function
      | Custom { get ; set ; prior ; _ } ->
          D.Also { get ; set ; sub = prior ; base }
      | Fixed { get ; set ; value ; _ } ->
          D.Also { get ; set ; sub = D.Constant value ; base }
      | Adaptive { get ; set ; prior ; _ } ->
          D.Also { get ; set ; sub = prior ; base }
      | Adaptive_fragments { get ; set ; prior ; _ } ->
          D.Also { get ; set ; sub = prior ; base }
      | Adaptive_frag_rand { get ; set ; prior ; _ } ->
          D.Also { get ; set ; sub = prior ; base }

    let custom_proposal :
      type tag par a.
      par Propose.t -> (tag, par, a) t -> par Propose.t = 
      fun base ->
      function
      | Fixed _ ->
          base
      | Adaptive _ ->
          base
      | Adaptive_fragments _ ->
          base
      | Adaptive_frag_rand _ ->
          base
      | Custom { get ; set ; proposal ; _ } ->
          Propose.Also {
            get ;
            set ;
            sub = proposal ;
            base
          }

    let draw :
      type tag par a.
      (U.rng -> float -> par -> par) list ->
      (tag, par, a) t ->
        (U.rng -> float -> par -> par) list =
      fun l ->
      function
      | Fixed _ ->
          l
      | Custom _ ->
          l
      | Adaptive { get ; set ; _ } ->
          (fun _ dx th -> set th (get th +. dx)) :: l
      | Adaptive_fragments { jumps ; _ } ->
          (L.map (fun SJ.{ set ; _ } -> (fun _ -> set)) jumps) @ l
      | Adaptive_frag_rand { jumps ; _ } ->
          (L.map (fun DJ.{ draw ; _ } -> draw) jumps) @ l

    let jump :
      type tag par a. float list -> (tag, par, a) t -> float list =
      fun l ->
      function
      | Fixed _ ->
          l
      | Custom _ ->
          l
      | Adaptive { jump ; _ } ->
          jump :: l
      | Adaptive_fragments { jumps ; _ } ->
          (L.map (fun SJ.{ jump ; _ } -> jump) jumps) @ l
      | Adaptive_frag_rand { jumps ; _ } ->
          (L.map (fun DJ.{ jump ; _ } -> jump) jumps) @ l

    let log_pd_ratio :
      type tag par a.
      (par -> par -> float list) option ->
      (tag, par, a) t ->
        (par -> par -> float list) option =
      fun ratio ->
      function
      | Fixed _ ->
          ratio
      | Custom _ ->
          (* here we only want the adaptive part,
           * we then multiply with the custom part as part of the also
           * (and we assume that we have the right to do this) *)
          ratio
      | Adaptive _ ->
          (* assume symmetrical *)
          ratio
      | Adaptive_fragments _ ->
          (* assume symmetrical *)
          ratio
      | Adaptive_frag_rand { log_pd_ratio ; _ } ->
          begin match ratio, log_pd_ratio with
          | None, None ->
              None
          | Some f, None
          | None, Some f ->
              Some f
          | Some _, Some _ ->
              invalid_arg "more than one non-symmetrical adaptive proposal"
          end

    let fix_par : type tag par a. par -> (tag, par, a) t -> par =
      fun par ->
      function
      | Fixed { set ; value ; _ } ->
          set par value
      | Custom _ ->
          par
      | Adaptive _ ->
          par
      | Adaptive_fragments _ ->
          par
      | Adaptive_frag_rand _ ->
          par

    let infer_tag : type tag par a. tag list -> (tag, par, a) t -> tag list =
      fun tags ->
      function
      | Fixed _ ->
          tags
      | Custom { tag ; _ } ->
          tag :: tags
      | Adaptive { tag ; _ } ->
          tag :: tags
      | Adaptive_fragments { tag ; _ } ->
          tag :: tags
      | Adaptive_frag_rand { tag ; _ } ->
          tag :: tags

    let adapt_tag : type tag par a. tag list -> (tag, par, a) t -> tag list =
      fun tags ->
      function
      | Fixed _ ->
          tags
      | Custom _ ->
          tags
      | Adaptive { tag ; _ } ->
          tag :: tags
      | Adaptive_fragments { tag ; _ } ->
          tag :: tags
      | Adaptive_frag_rand { tag ; _ } ->
          tag :: tags
  end


let prior th l =
  List.fold_left { f = Combine.prior } (Dist.Constant_flat th) l


let custom_proposal l =
  List.fold_left
    { f = Combine.custom_proposal }
    Propose.Constant_flat
    l


let draws l =
  List.fold_left { f = Combine.draw } [] l


let draw l =
  l
  |> draws
  |> A.of_list
  |> Propose.vector_draw


let jumps l =
  List.fold_left { f = Combine.jump } [] l


let log_pd_ratio l =
  List.fold_left { f = Combine.log_pd_ratio } None l

let fix_par l par =
  List.fold_left { f = Combine.fix_par } par l

let infer_tags l =
  List.fold_left { f = Combine.infer_tag } [] l


let adapt_tags l = 
  List.fold_left { f = Combine.adapt_tag } [] l
