module L = BatList
module A = BatArray

module Lac = Lacaml.D

module U = Util
module F = U.Float


exception Simulation_error of string
exception Bad_init
exception Draw_error of string


type ('a, 'b) output = (int, 'a, 'b) Util.Out.t
