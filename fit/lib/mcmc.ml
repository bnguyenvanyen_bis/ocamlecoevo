open Sig


(* This module implements the basic Metropolis-Hastings MCMC sampler,
 * in the Bayesian context, to sample posterior = prior * likelihood *)

type 'a weighted_sample = {
  sample : 'a ;
  logprior : float list ;
  loglik : float list; 
}


type 'a return =
  | Bad_proposal of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
    }
  | Bad_prior of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
    }
  | Bad_likelihood of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
      move_loglik : float list ;
      p'p : float ;
      qq' : float ;
    }
  | Reject of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
      move_loglik : float list ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }
  | Accept of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }


type 'a sampler =
  rng:Random.State.t ->
  'a weighted_sample ->
    'a return


type 'a prior = 'a Dist.t

type 'a proposal = 'a Propose.t

type 'a likelihood = 'a Dist.t


let return_to_sample =
  function
  | Bad_proposal { sample ; _ } ->
      sample
  | Bad_prior { sample ; _ } ->
      sample
  | Bad_likelihood { sample ; _ } ->
      sample
  | Reject { sample ; _ } ->
      sample
  | Accept { sample ; _ } ->
      sample


let return_to_move =
  function
  | Bad_proposal _ ->
      invalid_arg "return_to_move : bad_proposal"
  | Bad_prior { move ; _ } ->
      move
  | Bad_likelihood { move ; _ } ->
      move
  | Reject { move ; _ } ->
      move
  | Accept { sample ; _ } ->
      sample


let accepted_sample { sample ; logprior ; loglik } =
  Accept {
    sample ;
    logprior ;
    loglik ;
    logu=0. ;
    p'p=infinity ;
    qq'=infinity ;
    l'l=infinity ;
    logv=infinity
  }


let init prior likelihood sample =
  let logprior =
    match Dist.log_dens_proba prior sample with
    | x ->
        x
    | exception Simulation_error _ ->
        [neg_infinity]
  in
  let loglik =
    match Dist.log_dens_proba likelihood sample with
    | x ->
        x
    | exception Simulation_error _ ->
        [neg_infinity]
  in
  { sample ; logprior ; loglik }


let sample prior proposal likelihood ~rng =
  let sample { sample ; logprior ; loglik } =
    match Propose.draw_from rng sample proposal with
    | exception Draw_error _ ->
        Bad_proposal { sample ; logprior ; loglik }
    | move ->
    (* logprior might have changed since the last time *)
    let logprior = Dist.log_dens_proba prior sample in
    let logprior' = Dist.log_dens_proba prior move in
    (* Printf.eprintf "%s\n" (D.string_of hprior) ; *)
    let p'p = Dist.sum (Dist.partsub logprior' logprior) in
    (* Printf.eprintf "p'p = %f\n" p'p ; *)
    if (p'p = neg_infinity) || (classify_float p'p = FP_nan) then
      Bad_prior {
        sample ;
        logprior ;
        loglik ;
        move ;
        move_logprior = logprior' ;
      }
    else
    (* proposal from par' *)
    let qq' = Dist.sum (Propose.log_dens_proba_ratio proposal sample move) in
    if (qq' = neg_infinity) || (classify_float qq' = FP_nan) then
      Bad_proposal { sample ; logprior ; loglik }
    else
      try
        let loglik' =
          (* This can change logp' if it's mutable (happens with prm) *)
          match Dist.log_dens_proba likelihood move with
          | l ->
              l
          | exception Simulation_error s ->
              Printf.eprintf "Simulation error: %s\n" s ;
              [neg_infinity]
        in
        let ldif = Dist.partsub loglik' loglik in
        let l'l = Dist.sum ldif in
        if (l'l = neg_infinity) || (classify_float l'l = FP_nan) then
          begin
            (* Dist.eprint ~name:"loglik'" loglik' ; *)
            Bad_likelihood {
              sample ;
              logprior ;
              loglik ;
              move ;
              move_logprior = logprior' ;
              move_loglik = loglik' ;
              p'p ;
              qq' ;
            }
          end
        else
          begin
            let logv = p'p +. qq' +. l'l in
            if logv > 0. then
              Accept {
                sample = move ;
                logprior = logprior' ;
                loglik = loglik' ;
                logu = 0. ;
                p'p ;
                qq' ;
                l'l ;
                logv ;
              }
            else
              begin
                let logu = F.to_float (F.Pos.log (U.rand_float ~rng F.one)) in
                if logu <= logv then
                  Accept {
                    sample = move ;
                    logprior = logprior' ;
                    loglik = loglik' ;
                    logu ;
                    p'p ;
                    qq' ;
                    l'l ;
                    logv ;
                  }
                else
                  Reject {
                    sample ;
                    logprior = logprior ;
                    loglik = loglik ;
                    move ;
                    move_logprior = logprior' ;
                    move_loglik = loglik' ;
                    logu ;
                    p'p ;
                    qq' ;
                    l'l ;
                    logv ;
                  }
              end
          end
      with _ as e ->
        Printf.eprintf "caught error during sampling\n%!" ;
        raise e
        (* Bad_likelihood par' *)
  in sample


let integrate (prior : 'a prior)
      (proposal : 'a proposal) (likelihood : 'a likelihood) =
    ((init prior likelihood), (sample prior proposal likelihood))


let posterior ~(output : ('b, 'c) output)
              ?seed ~init
              ~(sample : 'a sampler)
              ~n_iter
              par =
  let rng = U.rng seed in
  let next_sample = sample ~rng in
  let rec to_the_end i wsmpl =
    match i with
    | 0 -> wsmpl
    | _ ->
      (let res = next_sample wsmpl in
       output.out (n_iter - i) res ;
       match res with
       | Accept { sample ; logprior ; loglik ; _ } ->
           (* accept *)
           to_the_end (i - 1) { sample ; logprior ; loglik }
       | Bad_proposal _
       | Bad_prior _
       | Bad_likelihood _ 
       | Reject _ ->
           (* reject *)
           to_the_end (i - 1) wsmpl)
  in
  let wsmpl0 = init par in
  output.start 0 (accepted_sample wsmpl0) ;
  let wsmplf = to_the_end n_iter wsmpl0 in
  output.return n_iter (accepted_sample wsmplf)
