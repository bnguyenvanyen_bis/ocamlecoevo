open Sig

type fl = float list

(*
type ('a, 'b) state = ('a * 'b * fl * fl) list
*)


type ('a, 'b) particle = {
  theta : 'a ;
  state : 'b ;
  logp : fl ;
  loglik : fl ;
}


type ('a, 'b) fp = float * ('a, 'b) particle

(*
type ('a, 'b, 'c, 'd) sampler =
  ('a, float * 'b, ('c, 'd) fp, ('c, 'd) fp) S.sampler
*)

type ('a, 'b) prior = ('a -> 'b Dist.t)

(* je crois ? *)
type ('a, 'b) resampler = ('a -> 'b list -> 'b list Dist.t)

type ('a, 'b, 'c) likelihood = ('a -> 'b -> 'c Dist.t)

type ('a, 'b) sim = ('a -> float -> 'b -> float -> 'b)



let integrate
  (prior : ('a, 'b * 'c) prior)
  (* (resampler : ('a, 'b * 'c) resampler) *)
  (sim : ('b, 'c) sim)
  (likelihood : ('a, 'b * 'c, 'd) likelihood) =
  let init ~n_particles rng hypar =
    let hprior = prior hypar in
    let rec f acc = function
      | 0 -> acc
      | n when n < 0 -> invalid_arg "n should be positive"
      | n ->
          let state = Dist.draw_from rng hprior in
          f (state :: acc) (n - 1)
    in f [] n_particles
  in
  let flow_particle t t' particle =
    let xt' = sim particle.theta t particle.state t' in
    { particle with state = xt' }
  in
  let sample (* rng *) hypar (t', datapoint) (t, particles) =
    (* let hprior = prior hypar in *)
    let hlik = likelihood hypar in
    (* simulate the particles forward to the new datapoint *)
    let particles' = L.map (flow_particle t t') particles in
    (* new likelihood components for the new datapoint *)
    let logliks = L.map (fun p ->
      Dist.log_dens_proba (hlik (p.theta, p.state)) datapoint
    ) particles'
    in
    logliks
  in (init, sample)
