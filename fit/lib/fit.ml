
(** @canonical Fit.Sig *)
module Sig = Sig

(** @canonical Fit.Csv *)
module Csv = Fit__Csv

(** @canonical Fit.Dist *)
module Dist = Dist

(** @canonical Fit.Propose *)
module Propose = Propose

(** @canonical Fit.Sample *)
module Sample = Sample

(** @canonical Fit.Acrej *)
module Acrej = Acrej

(** @canonical Fit.Mcmc *)
module Mcmc = Mcmc

(** @canonical Fit.Am *)
module Am = Am

(** @canonical Fit.Ram *)
module Ram = Ram


(** @canonical Fit.Param *)
module Param = Param

(** @canonical Fit.Infer *)
module Infer = Infer
