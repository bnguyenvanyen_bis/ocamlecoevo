open Sig


(** Results from sampling the chain *)

type 'a weighted_sample = {
  sample : 'a ;
  logprior : float list ;
  loglik : float list; 
}


type 'a return =
  | Bad_proposal of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
    }
  | Bad_prior of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
    }
  | Bad_likelihood of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
      move_loglik : float list ;
      p'p : float ;
      qq' : float ;
    }
  | Reject of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      move : 'a ;
      move_logprior : float list ;
      move_loglik : float list ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }
  | Accept of {
      sample : 'a ;
      logprior : float list ;
      loglik : float list ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }


(** the corresponding sample of this return *)
val return_to_sample : 'a return -> 'a


(** the move from this return.
 *  raises [Invalid_argument] if [Bad_proposal] *)
val return_to_move : 'a return -> 'a


(** dummy [Accept] return *)
val accepted_sample : 'a weighted_sample -> 'a return


type 'a sampler =
  rng:Random.State.t ->
  'a weighted_sample ->
    'a return



(** Prior parameter distribution *)
type 'a prior = 'a Dist.t


(** Proposal parameter distribution *)
type 'a proposal = 'a Propose.t


(** Likelihood parameter distribution *)
type 'a likelihood = 'a Dist.t


(** [init prior likelihood] is a MCMC sampler initializer [iz].
    So that [iz ?n_tries ~rng paro] is some ['a] value with positive
    prior and likelihood values,
    equal to [paro] if suitable, otherwise drawn from the prior,
    but equal to [None] if after [n_tries] no suitable value has been found. *)
val init :
  'a prior ->
  'a likelihood ->
  'a ->
    'a weighted_sample


(** [sample prior proposal likelihood] is a MCMC sampler,
  * with the specified [prior], [proposal] and [likelihood]. *)
val sample :
  'a prior ->
  'a proposal ->
  'a likelihood ->
  rng:Random.State.t ->
  'a weighted_sample ->
    'a return
  

(** [integrate prior proposal likelihood] 
 *  returns a MCMC initializer and sampler with the given
 *  [prior], [proposal], and [likelihood]. *)
val integrate :
  'a prior ->
  'a proposal ->
  'a likelihood ->
    ('a -> 'a weighted_sample) * 'a sampler


(** Call a sampler again and again,
 *  executing some actions along the trajectory. *)


(** [posterior ~output ?seed init sample n_iter paro]
 *  will initialize the chain with [init] and [paro],
 *  then call [sample] [n_iter] times,
 *  with a PRNG initialized from [seed].
 *  The output is controlled from [output]. *)
val posterior :
  output:('a return, 'b) output ->
  ?seed : int ->
  init : ('a -> 'a weighted_sample) ->
  sample : 'a sampler ->
  n_iter : int ->
  'a ->
    'b
