module Make (P : Prm.PRM) =
  struct
    include (Symbolic.Make (Ctmjp__Prm__Impl.Make (P)))
  end
