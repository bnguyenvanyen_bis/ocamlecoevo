open Ctmjp__Sig


module Make (C : Prm.COLOR) =
  struct
    module I = Ctmjp__Impl.Prm.Make (C)

    module P = Prm.Make (C)


    module Auto =
      struct
        module I = I.Auto

        module S = Symbolic.Make (I)

        let rec eval :
          type a. a S.Modif.symb -> a I.modif =
          S.Modif.(
            let fail () = failwith "Not_implemented" in
            function
            | Identity ->
                (fun _ x -> x)
            | Transfo f ->
                f
            | Bernoulli proba ->
                (fun pt x ->
                  let p = proba x in
                  F.Op.(pt.u <= p))
            | Bernoulli_lifted proba ->
                (fun pt y x ->
                  let p = proba y x in
                  F.Op.(pt.u <= p))
            | Erase (Bernoulli proba, e') ->
                (* Or is it here that we change r for e' ? How ? *)
                let modif = eval e' in
                (fun pt x ->
                  (* FIXME or possibly we would change the pt passed
                   * onto modif here *)
                  let p = proba x in
                  let cp = F.Proba.compl p in
                  if F.Op.(pt.u <= p) then
                    x
                  else
                    (* u 'reverses' its meaning *)
                    modif Prm.({ pt with u = F.Pos.Op.(pt.u / cp) }) x)
            | Erase_lifted (Bernoulli_lifted proba, e') ->
                (* Or is it here that we change r for e' ? How ? *)
                let modif = eval e' in
                (fun pt y x ->
                  (* FIXME or possibly we would change the pt passed
                   * onto modif here *)
                  let p = proba y x in
                  let cp = F.Proba.compl p in
                  if F.Op.(pt.u <= p) then
                    x
                  else
                    (* u 'reverses' its meaning *)
                    modif Prm.({ pt with u = F.Pos.Op.(pt.u / cp) }) y x)
            | Map (e, e') ->
                let transfo = eval e in
                let modif = eval e' in
                (fun pt x ->
                  let y = transfo pt x in
                  modif pt y x)
            | Chain (e, e') ->
                let m = eval e in
                let m' = eval e' in
                (fun pt x -> m' pt (m pt x))
            | Chain_lifted (e, e') ->
                let m = eval e in
                let m' = eval e' in
                (fun pt y x -> m' pt y (m pt y x))
            | Lift e ->
                let modif = eval e in
                (fun pt (y, x) -> (y, modif pt x))
            (* cases we don't handle (sort of exhaustive) *)
            | Erase _ ->
                fail ()
            | Erase_lifted _ ->
                fail ()
          )
       

        let events colors (S.Colors l) =
          L.fold_left2 (fun evm c (rate, smodif) ->
            P.Cm.add c (rate, eval smodif) evm
          ) P.Cm.empty colors l
      end

    module Timedep =
      struct
        module I = I.Timedep

        module S = Symbolic.Make (I)

        let rec eval :
          type a. a S.Modif.symb -> a I.modif =
          S.Modif.(
            let fail () = failwith "Not_implemented" in
            function
            | Identity ->
                (fun _ _ x -> x)
            | Transfo f ->
                f
            | Bernoulli proba ->
                (fun pt t x ->
                  let p = proba t x in
                  F.Op.(pt.u <= p))
            | Bernoulli_lifted proba ->
                (fun pt t y x ->
                  let p = proba t y x in
                  F.Op.(pt.u <= p))
            | Erase (Bernoulli proba, e') ->
                (* Or is it here that we change r for e' ? How ? *)
                let modif = eval e' in
                (fun pt t x ->
                  (* FIXME or possibly we would change the pt passed
                   * onto modif here *)
                  let p = proba t x in
                  let cp = F.Proba.compl p in
                  if F.Op.(pt.u <= p) then
                    x
                  else
                    (* u 'reverses' its meaning *)
                    modif Prm.({ pt with u = F.Pos.Op.(pt.u / cp) }) t x)
            | Erase_lifted (Bernoulli_lifted proba, e') ->
                (* Or is it here that we change r for e' ? How ? *)
                let modif = eval e' in
                (fun pt t y x ->
                  (* FIXME or possibly we would change the pt passed
                   * onto modif here *)
                  let p = proba t y x in
                  let cp = F.Proba.compl p in
                  if F.Op.(pt.u <= p) then
                    x
                  else
                    (* u 'reverses' its meaning *)
                    modif Prm.({ pt with u = F.Pos.Op.(pt.u / cp) }) t y x)
            | Map (e, e') ->
                let transfo = eval e in
                let modif = eval e' in
                (fun pt t x ->
                  let y = transfo pt t x in
                  modif pt t y x)
            | Chain (e, e') ->
                let m = eval e in
                let m' = eval e' in
                (fun pt t x -> m' pt t (m pt t x))
            | Chain_lifted (e, e') ->
                let m = eval e in
                let m' = eval e' in
                (fun pt t y x -> m' pt t y (m pt t y x))
            | Lift e ->
                let modif = eval e in
                (fun pt t (y, x) -> (y, modif pt t x))
            (* cases we don't handle *)
            | Erase _ ->
                fail ()
            | Erase_lifted _ ->
                fail ()
          )
       

        let events colors (S.Colors l) =
          (* Warn about Non autonomous system *)
          Printf.eprintf
          "Warning:
           Simulation of a non-autonomous system is not necessarily exact." ;
          L.fold_left2 (fun evm c (rate, smodif) ->
            P.Cm.add c (rate, eval smodif) evm
          ) P.Cm.empty colors l
      end
  end
