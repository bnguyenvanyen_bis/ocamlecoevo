open Ctmjp__Sig

module L = BatList

module U = Util
module F = U.Float

module I = Ctmjp__Impl.Rng


module Auto =
  struct
    module S = Symbolic.Make (I.Auto)

    (* remarque, on doit peut-être mettre le t déjà ici quand même,
     * mais se souvenir qu'on n'en avait pas besoin *)

    let rec eval :
      type a. a S.Modif.symb -> a I.Auto.modif = S.Modif.(
      function
      | Identity ->
          (fun _ x -> x)
      | Transfo f ->
          f
      | Bernoulli proba ->
          (fun rng x -> U.rand_bernoulli ~rng (proba x))
      | Bernoulli_lifted proba ->
          (fun rng y x -> U.rand_bernoulli ~rng (proba y x))
      (*
      | Colors cols ->
          let weights, es = L.split cols in
          let modifs = L.map eval es in
          (fun rng x ->
             let ws = L.map (fun w -> w x) weights in
             let mws = L.combine modifs ws in
             let choice = U.rand_choose ~rng mws in
             choice rng x
          )
      *)
      | Erase (e, e') ->
          let pred = eval e in
          let modif = eval e' in
          (fun rng x -> if pred rng x then x else modif rng x)
      | Erase_lifted (e, e') ->
          let pred = eval e in
          let modif = eval e' in
          (fun rng y x ->
             if pred rng y x then
               x
             else
               modif rng y x)
      | Map (e, e') ->
          let transfo = eval e in
          let modif = eval e' in
          (fun rng x ->
            let y = transfo rng x in
            modif rng y x)
      | Chain (e, e') ->
          let m = eval e in
          let m' = eval e' in
          (fun rng x -> m' rng (m rng x))
      | Chain_lifted (e, e') ->
          let m = eval e in
          let m' = eval e' in
          (fun rng y x -> m' rng y (m rng y x))
      | Lift e ->
          let modif = eval e in
          (fun rng (y, x) -> (y, modif rng x))
    )


    let event (S.Colors l) =
      (* unsymb *)
      let l' = L.map (fun (rate, smodif) -> (rate, eval smodif)) l in
      let cumul x (_, tot) (r', m') =
        (m', F.Pos.add tot (r' x))
      in
      (fun _ x ->
        let (_, tot), revmr =
          U.cumul_fold_left (cumul x) ((fun _ _ x -> x), F.zero) l'
        in
        let choose rng = U.rand_cumul_rev_choose ~rng ~tot revmr in
        let modif rng =
          let m = choose rng in
          let partial = m rng in
          (* lift for time *)
          (fun _ -> partial)
        in
        (tot, modif)
      )


  end


module Timedep =
  struct
    module S = Symbolic.Make (I.Timedep)

    let rec eval :
      type a. a S.Modif.symb -> a I.Timedep.modif = S.Modif.(
      function
      | Identity ->
          (fun _ _ x -> x)
      | Transfo f ->
          f
      | Bernoulli proba ->
          (fun rng t x -> U.rand_bernoulli ~rng (proba t x))
      | Bernoulli_lifted proba ->
          (fun rng t y x -> U.rand_bernoulli ~rng (proba t y x))
      | Erase (e, e') ->
          let pred = eval e in
          let modif = eval e' in
          (fun rng t x ->
             if pred rng t x then
               x
             else
               modif rng t x)
      | Erase_lifted (e, e') ->
          let pred = eval e in
          let modif = eval e' in
          (fun rng t y x ->
             if pred rng t y x then
               x
             else
               modif rng t y x)
      | Map (e, e') ->
          let transfo = eval e in
          let modif = eval e' in
          (fun rng t x ->
            let y = transfo rng t x in
            modif rng t y x)
      | Chain (e, e') ->
          let m = eval e in
          let m' = eval e' in
          (fun rng t x -> m' rng t (m rng t x))
      | Chain_lifted (e, e') ->
          let m = eval e in
          let m' = eval e' in
          (fun rng t y x -> m' rng t y (m rng t y x))
      | Lift e ->
          let modif = eval e in
          (fun rng t (y, x) -> (y, modif rng t x))
    )


    (* FIXME fixme *)
    let event (S.Colors l) =
      (* Warn about non autonomous system *)
      Printf.eprintf
      "Warning:
        Simulation of a non-autonomous system is not necessarily exact." ;
      (* unsymb *)
      let l' = L.map (fun (rate, smodif) -> (rate, eval smodif)) l in
      let cumul t x (_, tot) (r', m') =
        (m', F.Pos.add tot (r' t x))
      in
      (fun t x ->
        let (_, tot), revmr =
          U.cumul_fold_left (cumul t x) ((fun _ _ x -> x), F.zero) l'
        in
        let choose rng = U.rand_cumul_rev_choose ~rng ~tot revmr in
        let modif rng =
          let m = choose rng in
          m rng
        in
        (tot, modif)
      )
  end


module M = Auto.S.Modif
module M' = Timedep.S.Modif


let rec as_timedep :
  type a. a M.symb -> a M'.symb =
  function
  | M.Identity ->
      M'.Identity
  | M.Transfo f ->
      M'.Transfo (fun rng _ -> f rng)
  | M.Bernoulli proba ->
      M'.Bernoulli (fun _ -> proba)
  | M.Bernoulli_lifted proba ->
      M'.Bernoulli_lifted (fun _ -> proba)
  | M.Erase (e, e') ->
      M'.Erase (as_timedep e, as_timedep e')
  | M.Erase_lifted (e, e') ->
      M'.Erase_lifted (as_timedep e, as_timedep e')
  | M.Map (e, e') ->
      M'.Map (as_timedep e, as_timedep e')
  | M.Chain (e, e') ->
      M'.Chain (as_timedep e, as_timedep e')
  | M.Chain_lifted (e, e') ->
      M'.Chain_lifted (as_timedep e, as_timedep e')
  | M.Lift e ->
      M'.Lift (as_timedep e)
