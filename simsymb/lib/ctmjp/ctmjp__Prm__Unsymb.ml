open Ctmjp__Sig


module Make (P : Prm.PRM) =
  struct
    module Symb = Ctmjp__Prm__Symb.Make (P)

    let eval : (P.point -> 'a -> 'a) Symb.t -> (P.point -> 'a -> 'a) =
      Symb.eval

    let events_auto (cres : (P.color * (('a -> 'b U.pos) * (P.point -> 'a -> 'a) Symb.t)) list) =
      L.fold_left (fun evm (c, (rate, e)) ->
        P.Cm.add c ((fun _ -> rate), eval e) evm
      ) P.Cm.empty cres

    let events_timedep (cres : (P.color * ((time -> 'a -> 'b U.pos) * (P.point -> 'a -> 'a) Symb.t)) list) =
      L.fold_left (fun evm (c, (rate, e)) ->
        P.Cm.add c (rate, eval e) evm
      ) P.Cm.empty cres
  end
