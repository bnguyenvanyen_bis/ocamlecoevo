(lang dune 1.11)
(name ecoevo)

(generate_opam_files true)

(license LGPL-3.0-only)
(maintainers "Benjamin Nguyen-Van-Yen <bnguyen@biologie.ens.fr>")
(authors "Benjamin Nguyen-Van-Yen <bnguyen@biologie.ens.fr>")
(source (uri https://gitlab.com/bnguyenvanyen/ocamlecoevo.git))

(using menhir 2.0)

(package
  (name ecoevo)
  (synopsis
  "Simulations and inference for ecology and evolution"
  )
  (description
   "The package contains generic libraries to simulates ODEs
    and Markov jump processes (sim), to perform bayesian inference (fit),
    as well as to manipulate
    and simulate trees (tree), populations (pop), and sequences (seqs, seqsim).
    Some example systems are implemented in eco, and epi,
    with inference in ecofit and epifit."
  )
  (depends
    (cmdliner (>= 1.0.2))
    (batteries (>= 2.8))
    (csv (>= 2.1))
    (menhir (>= 20180905))
    (lacaml (>= 11.0.1))
    (odepack (>= 0.6.8))
    (cairo2 (>= 0.6.1))
    (ocamlgraph (>= 1.8.8))
  )
)
