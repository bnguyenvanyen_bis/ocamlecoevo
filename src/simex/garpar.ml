exception Found of float

type param = {
  b : float ;  (* individual birth rate *)
  d : float ;  (* competitive death rate *)
  mu : float ;  (* mutation probability on birth *)
  v : float ;  (* variance of mutations *)
}

type trait = float

type t = {
  total : int ;
  counts : (float, int) Hashtbl.t
}

(* use a custom pop without ids *)
module P =
  struct
    type nonrec t = t

    let create_empty n = { total = 0 ; counts = Hashtbl.create n }

    let get_count z x =
      try
        Hashtbl.find z.counts x
      with Not_found ->
        0

    let add z x =
      let n = get_count z x in
      Hashtbl.replace z.counts x (n + 1) ;
      { z with total = z.total + 1 }

    let remove z x =
      let n = get_count z x in
      begin
        match n with
        | 0 -> failwith "can't remove, is not present"
        | 1 -> Hashtbl.remove z.counts x
        | _ -> Hashtbl.replace z.counts x (n - 1)
      end ;
      { z with total = z.total - 1 }

    let choose rng z =
      let n = Util.rand_int rng z.total in
      let f_fold x k n' =
        let n'' = n' + k in
        if n'' < n then n'' else raise (Found x) (* early stop *)
      in
      try
        ignore (Hashtbl.fold f_fold z.counts 0) ; raise Not_found
      with Found x ->
        x
  end


let upper_birth_rate par z =
  par.b *. float z.total

let indiv_birth_proba x par z =
  1. /. 2.  +. atan x /. Util.pi

let indiv_birth_rate x par z =
  par.b *. indiv_birth_proba x par z

let upper_death_rate par z =
  let n = z.total in
  par.d *. float n *. float (n - 1)

let indiv_death_rate x par z =
  par.d *. float (z.total - 1)

let indiv_death_proba x par z =
  1.

let mutate par rng x =
  Util.rand_normal rng x par.v

let birth_modif x par rng z =
  let x' =
    if Util.rand_bernoulli rng par.mu then
      mutate par rng x
    else
      x
  in P.add z x'

let death_modif x par rng z =
  P.remove z x

module Gs =
  struct
    type nonrec param = param
    type nonrec t = t

    let f_fold par z x n events =
      let birth = (indiv_birth_rate x par z, birth_modif x) in
      let death = (indiv_death_rate x par z, death_modif x) in
      let rec f k acc =
        match k with
        | 0 -> acc
        | _ -> f (k - 1) (birth :: death :: acc)
      in f n events

    let events par z =
      Hashtbl.fold (f_fold par z) z.counts []
  end

let addxev fev x n events =
  let ev = fev x in
  let rec f k acc =
    match k with
    | 0 -> acc
    | _ -> f (k - 1) (ev :: acc)
  in f n events

let birthev x =
  (indiv_birth_proba x, birth_modif x)

let deathev x =
  (indiv_death_proba x, death_modif x)

module Gars =
  struct
    type nonrec param = param
    type nonrec t = t

    let eventgroups par z =
      [ (upper_birth_rate par z,
         Hashtbl.fold (addxev birthev) z.counts []) ;
        (upper_death_rate par z,
         Hashtbl.fold (addxev deathev) z.counts []) ]
  end

module Pars =
  struct
    type nonrec param = param
    type nonrec t = t

    let birthg =
      (upper_birth_rate,
       fun _ z -> Hashtbl.fold (addxev birthev) z.counts [])

    let deathg =
      (upper_death_rate,
       fun _ z -> Hashtbl.fold (addxev deathev) z.counts [])

    let eventgroups = [ birthg ; deathg ]
  end

module Sg = Sim.Ngill.Make (Gs)
module Sgar = Sim.Gill_acrej.Make (Gars)
module Spar = Sim.Euler_poisson_acrej.Make (Pars)
