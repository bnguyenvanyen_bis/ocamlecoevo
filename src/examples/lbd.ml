open Lbd

module I = Sim.Gill.Make (S') (O)

let add_new z x =
  let i = P.new_ident z in
  P.add_descent 0. z None (Some i) x

let add_k_new z x k =
  let rec f j =
    match j with
    | 0 -> ()
    | _ -> ignore (add_new z x) ; f (j - 1)
  in f k ;
  z

let apar_r = ref { Sim.Sig.seed = Some 0 }

let opar_r = ref O.default

let gtr_r = ref (Jc69.to_gtr { Jc69.mu = 1e-4 })

let par_r = ref {
  b = 1. ;
  c = 1e-4 ;
  d = 0. ;
  evo =
    {
    Mut.snp = Gtr.to_snp !gtr_r ;
    mu_del = 1e-6 ;
    mu_rep = 1e-6 ;
    repdelen = 0.3 ;
  }
}

let p_keep_r = ref 1e-2

let tf_r = ref 10.

let n_r = ref 10000

let k_r = ref 10000

let specl = (Sim.Arg.specl_of apar_r Sim.Gill.specl)
          @ (Sim.Arg.specl_of gtr_r Gtr.specl)
          @ (Sim.Arg.specl_of opar_r O.specl)
          @ (Sim.Arg.specl_of par_r specl)
          @ [("-pkeep",
              Arg.Set_float p_keep_r,
              "Probability of sampling individual x trait.") ;
             ("-tf",
              Arg.Set_float tf_r,
              "Simulation time.") ;
             ("-n0",
              Arg.Set_int n_r,
              "Initial number of individuals.") ;
             ("-k",
              Arg.Set_int k_r,
              "All individuals are given the same initial random sequence of length k.")]
              

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Simulate the neutral mutations of sequences in a LBD population."


let main () =
  begin
    Printf.printf "##INIT##\n" ;
    Arg.parse specl anon_fun usage_msg ;
    let par = { !par_r with evo = { !par_r.evo with Mut.snp = Gtr.to_snp !gtr_r } } in
    let seq0 = Seq.random !k_r in
    let z0 = P.create_empty () in
    (* FIXME not too sure about this *)
    begin
      match !apar_r.Sim.Sig.seed with
      | None ->
        ()
      | Some n ->
        (z0.Seqpop.rng <- Random.State.make (Array.init 1 (fun _ -> n)))
    end ;
    z0.Seqpop.p_keep <- !p_keep_r ;
    ignore (add_k_new z0 (seq0, [(0., Seq.Sig.Not)]) !n_r) ;
    Printf.printf "Input read, starting simulation\n%!" ;
    let auxf ?(auxi=()) _ _ _ = () in
    let pred t _ _ = (t >= !tf_r) in
    I.simulate ~opar:!opar_r ~apar:!apar_r par auxf pred z0
  end;;


main ()
