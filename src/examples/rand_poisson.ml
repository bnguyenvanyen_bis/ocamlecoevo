
let n_r = ref 10000
let lbd_r = ref 50.

let specl = [("-n", Arg.Set_int n_r,
              "Number of samples.") ;
             ("-lbd", Arg.Set_float lbd_r,
              "Parameter of the Poisson variables.")]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s
let usage_msg = "Draw samples from the Poisson distribution with one of two metohds. Default : exponential draws."
             
let main () =
  Arg.parse specl anon_fun usage_msg ;
  let rng = Random.get_state () in
  let rec draw f acc k =
    match k with
    | 0 ->
      acc
    | _ ->
      let i = f rng !lbd_r in
      draw f (i :: acc) (k - 1)
  in
  let exp_l = draw Util.rand_poisson_exp [] !n_r in
  let acrej_l = draw Util.rand_poisson_acrej [] !n_r in
  let fil k l =
    List.filter (fun i -> i = k) l
  in
  let fn = float !n_r in
  let cmp_proba k =
    let true_proba = exp (Util.logp_poisson !lbd_r k) in
    let exp_count = List.length (fil k exp_l) in
    let acrej_count = List.length (fil k acrej_l) in
    let exp_proba = float exp_count /. fn in
    let acrej_proba = float acrej_count /. fn in
    (true_proba, exp_proba, acrej_proba)
  in
  for k = 0 to (2 * int_of_float !lbd_r) do
    let truep, expp, acrejp = cmp_proba k in
    Printf.printf "%i;%f;%f;%f\n" k truep expp acrejp
  done;;


main ()
