open Exp_mcmc

let opar = stdout
let apar = { Mcmc.seed = None ; n_gap = 1000 ; n_iter = 2000000 }
let hypar = { n = 100 ; v = `V 0.01 ; upper = 10. }
let target_lbd = 1.
let target_d = Dist.Many (Util.l_repeat hypar.n (Dist.Exponential target_lbd))
(* we cheat a little here too *)
let data = Dist.draw_from (Random.State.make_self_init ()) target_d;;

Fit.posterior opar apar hypar data
