open Cointoss

module I = Sim.Poisson.Make (S) (O)

let opar = {
  Sim.Out.dt_print = 0. ;
  chan = stdout
}

let apar = {
  Sim.Sig.seed = Some 123456789
}

let par_r = ref {
  rab = 1. ;
  rba = 1. ;
}

let tf_r = ref 1.

let start_with_b_r = ref false

let specl = [
  ("-rAB", Arg.Float (fun x -> par_r := { !par_r with rab = x }),
   "Rate of mutation from A to B") ;
  ("-rBA", Arg.Float (fun x -> par_r := { !par_r with rba = x }),
   "Rate of mutation from B to A") ;
  ("-tf", Arg.Set_float tf_r,
   "Duration of simulation") ;
  ("-B", Arg.Set start_with_b_r,
   "Start in B state (default A state)")
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Simulate the transitions between a A state and B state with the Poisson algorithm."

let x0 = if !start_with_b_r then B else A;;

Arg.parse specl anon_fun usage_msg

let auxf ?(auxi=()) par t x = ()
let p t x auxi = (t >= !tf_r);;

I.simulate ~opar ~apar !par_r auxf p x0
