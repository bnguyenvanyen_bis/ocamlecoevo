open Garpar

type sim_mode = Gill | Gar | Epar

let of_symb s =
  if s = "gill" then
    Gill
  else if s = "gar" then
    Gar
  else if s = "epar" then
    Epar
  else
    invalid_arg "invalid symbol"

let sim_mode_r = ref Gill

let tf_r = ref 100.

let h_r = ref 0.1
let overshoot_r = ref 1.01

let par_r = ref {
  b = 10. ;
  d = 0.01 ;
  mu = 0.01 ;
  v = 1.
}

let x0_r = ref 0.

let dprint_r = ref 0.

let seed_r = ref None

let out_r = ref stdout

(* those default values simulate an order of 1e6 events *)

let specl = [("-tf", Arg.Set_float tf_r,
              "Simulation time.") ;
             ("-b", Arg.Float (fun x -> par_r := { !par_r with b = x }),
              "Individual base birth rate.") ;
             ("-d", Arg.Float (fun x -> par_r := { !par_r with d = x }),
              "Individual competitive death rate.") ;
             ("-mu", Arg.Float (fun x -> par_r := { !par_r with mu = x }),
              "Mutation probability on birth.") ;
             ("-v", Arg.Float (fun x -> par_r := { !par_r with v = x }),
              "Mutation variance.") ;
             ("-x0", Arg.Set_float x0_r,
              "Initial trait value.") ;
             ("-seed", Arg.Int (fun n -> seed_r := Some n),
              "Seed of the PRNG.") ;
             ("-dprint", Arg.Set_float dprint_r,
              "Min duration between prints.") ;
             ("-sim", Arg.Symbol (["gill" ; "gar" ; "epar"], (fun s -> sim_mode_r := of_symb s)),
              "Simulation algorithm.") ;
             ("-h", Arg.Set_float h_r,
              "Simulation step size (only used with 'epar').") ;
             ("-over", Arg.Set_float overshoot_r,
              "Overshoot for poisson draws and rejections.") ;
             ("-out", Arg.String (fun s -> out_r := open_out s),
              "File to output to.")]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Comparison of simulation algorithms on a logistic branching process."

let print_state outchan t z =
  Printf.fprintf outchan "%f" t ;
  Hashtbl.iter (fun x n -> Printf.fprintf outchan ";%f:%i" x n) z.counts ;
  Printf.fprintf outchan "\n"
  

let simulate_until stepper outchan dprint rng par tf x0 =
  let z0 = P.create_empty (1 + int_of_float (par.b /. par.d)) in
  let z0 = P.add z0 x0 in
  print_state outchan 0. z0 ;
  let rec f prevt t z =
    let (nt, nz) = stepper rng par t z in
    if nt >= tf then
      ()
    else
      begin
        if nt >= prevt +. dprint then
          begin
            print_state outchan nt nz ;
            f nt nt nz
          end
        else
          f prevt nt nz
      end
  in f 0. 0. z0


let main () =
  Arg.parse specl anon_fun usage_msg ;
  let rng =
    match !seed_r with
    | None -> Random.State.make_self_init ()
    | Some n -> Random.State.make (Array.init 1 (fun _ -> n))
  in
  (* basically we simulate_until using one of the methods *)
  let epar_apar = {
    Sim.Euler_poisson_acrej.step_size = !h_r ;
    overshoot = !overshoot_r
  } in
  let stepper =
    match !sim_mode_r with
    | Gill -> Sg.next
    | Gar -> Sgar.next
    | Epar -> (Spar.next epar_apar)
  in
  let exect = Sys.time () in
  simulate_until stepper !out_r !dprint_r rng !par_r !tf_r !x0_r ;
  Printf.printf "Execution time : %f\n" (Sys.time () -. exect);;

main ()
