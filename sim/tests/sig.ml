module L = BatList

module U = Util
module F = U.Float
module I = U.Int

module SC = Sim.Ctmjp
module SCU = SC.Util


let fpof = F.Pos.of_float
let ipoi = I.Pos.of_int
