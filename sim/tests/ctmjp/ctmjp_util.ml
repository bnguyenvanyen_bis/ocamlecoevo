module F = Util.Float
module SCU = Sim.Ctmjp.Util


let string_of_bool b =
  if b then "true" else "false"

(* test chain order *)
let%expect_test _ =
  let first _ _ x =
    Printf.printf "first\n" ;
    x
  in
  let second _ _ x  =
    Printf.printf "second\n" ;
    x
  in
  let third _ _ x =
    Printf.printf "third\n" ;
    x
  in
  let m =
    SCU.chain
      (SCU.chain first second) 
      third
  in m () () () ;
  [%expect{|
    first
    second
    third |}]


(* test add *)
let%expect_test _ =
  (* this modif should always end up chosen *)
  let rm = (fun _ _ -> (F.one, (fun _ -> true))) in
  (* and this one never *)
  let rm' = (fun _ _ -> (F.zero, (fun _ -> false))) in
  let rm'' = SCU.add rm rm' in
  (* with any rng *)
  let rng = Util.rng None in
  let r, m = rm'' F.zero true in
  let b = m rng in
  Printf.printf "(%f, %s)\n" (F.to_float r) (string_of_bool b) ;
  [%expect{| (1.000000, true) |}]


(* test add_many *)
let%expect_test _ =
  (* this modif should always end up chosen *)
  let rm = (fun _ _ -> (F.one, (fun _ -> "good"))) in
  (* and those ones never *)
  let rm' = (fun _ _ -> (F.zero, (fun _ -> "bad"))) in
  let rm'' = (fun _ _ -> (F.zero, (fun _ -> "also bad"))) in
  let rm_many = SCU.add_many [rm ; rm' ; rm''] in
  let r, m = rm_many F.zero "whatever" in
  (* with any rng *)
  let rng = Util.rng None in
  let s = m rng in
  Printf.printf "(%f, %s)\n" (F.to_float r) s ;
  [%expect{| (1.000000, good) |}] ;
  (* what if we change the order of rms ? *)
  let rm_many' = SCU.add_many [rm'' ; rm' ; rm] in
  let r, m = rm_many' F.zero "whatever" in
  (* with any rng *)
  let rng = Util.rng None in
  let s = m rng in
  Printf.printf "(%f, %s)\n" (F.to_float r) s ;
  [%expect{| (1.000000, good) |}] ;
