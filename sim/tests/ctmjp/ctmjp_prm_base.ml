open Sim_test.Sig

module Pb = Prm_test.Prm_base


module C = SC.Prm.Make (Pb.Point)

module Ca = SC.Prm_approx.Make (Pb.Point)


(*
let create () =
  Prm_base.create ()
*)


let evm () =
  Pb.Point.Colors.singleton () (
    (fun _ _ -> fpof 0.5),
    (fun _ _ n -> n + 1)
  )


let combine evm =
  Pb.Point.Colors.map (fun (rate, modif) ->
    SCU.combine rate modif
  ) evm
