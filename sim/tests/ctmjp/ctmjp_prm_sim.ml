open Sim_test.Sig
open Ctmjp_prm_base



let%expect_test _ =
  let evm = combine (evm ()) in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    C.P.Ps.add pt pts
  ) C.P.Ps.empty Pb.points 
  in
  let prm = C.P.create ~time_range ~ntslices ~vectors points in
  let output = Sim.Csv.convert
    ?dt:None
    ~header:[[]]
    ~line:(fun t n -> [ F.to_string t ; string_of_int n ])
    ~chan:stdout
  in
  ignore (C.simulate_until ~output evm 0 prm) ;
  [%expect{|
    Accepted points : 2 out of 4

    0.,0
    0.35,0
    0.6,1 |}]
