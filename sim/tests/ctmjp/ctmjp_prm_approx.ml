open Sim_test.Sig
open Ctmjp_prm_base


let%expect_test _ =
  let evm = evm () in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    Ca.P.Ps.add pt pts
  ) Ca.P.Ps.empty Pb.points 
  in
  let prm = Ca.P.create ~time_range ~ntslices ~vectors points in
  let header = [[]] in
  let line t n =
    [ F.to_string t ; string_of_int n ]
  in
  let output = Sim.Csv.convert ?dt:None ~header ~line ~chan:stdout in
  ignore (Ca.simulate_until ~output evm 0 prm) ;
  [%expect{|
    0.,0
    0.,0
    0.5,1 |}]
