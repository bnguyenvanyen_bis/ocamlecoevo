open Sim_test.Sig


module Color =
  struct
    type t = unit
    let compare () () = 0
    let to_string () = "()"
    let of_string =
      function
      | "()" ->
          Some ()
      | _ ->
          None
    let equal () () = true
  end


module Point = Sim.Prm.Point (Color)
module Point_time = Sim.Prm.Compare_time (Point)


module P = Sim.Prm.Make (Point_time)


let point ~k ~t ~u =
  Sim.Prm.{ c = () ; k ; t ; u }


let time_range = (F.zero, F.one)

let ntslices : 'a U.posint = I.two

let vectors () =
  [(I.two, point ~k:1 ~t:F.zero ~u:F.one)]

let points = [
  point ~k:0
        ~t:(fpof 0.1)
        ~u:(fpof 0.85) ;
  point ~k:0
        ~t:(fpof 0.35)
        ~u:(fpof 0.25) ;
  point ~k:0
        ~t:(fpof 0.6)
        ~u:(fpof 0.15) ;
  point ~k:0
        ~t:(fpof 0.9)
        ~u:(fpof 0.75) ;
]

let pointset = 
  L.fold_left (fun pts pt -> P.Ps.add pt pts) P.Ps.empty points


let create () =
  P.create
    ~time_range
    ~ntslices
    ~vectors:(vectors ())
    pointset


let rand_draw () =
  let rngm = Point.Colors.singleton () (U.rng (Some 0)) in
  P.rand_draw
    ~rngm
    ~time_range
    ~ntslices
    ~vectors:(vectors ())


let print_points =
  P.Ps.iter (fun pt ->
    Printf.printf "(%f, %f) ;\n" (F.to_float pt.t) (F.to_float pt.u)
  )


let print tsl =
  P.Tsl.iter_points (fun pt ->
    Printf.printf "(%f, %f) ;\n" (F.to_float pt.t) (F.to_float pt.u) ;
    `Continue
  ) tsl
