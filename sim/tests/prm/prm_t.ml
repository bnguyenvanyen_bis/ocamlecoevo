open Sim_test.Sig


(* create and show points in the two slices *)
let%expect_test _ = Prm_base.(
  let prm = create () in
  let ltsl = P.t_slice_of prm (fpof 0.1) in
  print_points (P.Tsl.points ltsl) ;
  [%expect{|
    (0.100000, 0.850000) ;
    (0.350000, 0.250000) ; |}] ;
  let rtsl = P.t_slice_of prm (fpof 0.8) in
  print_points (P.Tsl.points rtsl) ;
  [%expect{|
    (0.600000, 0.150000) ;
    (0.900000, 0.750000) ; |}]
)


(* hide_downto *)
(* hide the upper slice of the t slice on the right *)
let%expect_test _ = Prm_base.(
  let prm = create () in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  (* hide the upper slice *)
  P.hide_downto tsl () (fpof 0.75) ;
  print tsl ;
  [%expect{| (0.600000, 0.150000) ; |}]
)

(* hide then hide down to 0 does nothing more *)
let%test _ = Prm_base.(
  let prm = create () in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  let csl = P.Tsl.find tsl () in
  P.hide_downto tsl () (fpof 0.7) ;
  let points = P.Csl.points csl in
  (* try to hide down to zero -- does nothing more *)
  P.hide_downto tsl () F.zero ;
  let points' = P.Csl.points csl in
  P.Ps.equal points points'
)


(* reveal_upto *)
(* reveal_upto cancels hide_downto *)
let%expect_test _ = Prm_base.(
  let prm = create () in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  let csl = P.Tsl.find tsl () in
  Printf.printf "%f" (F.to_float (P.Csl.umax_revealed csl)) ;
  [%expect{| 1.000000 |}] ;
  print tsl ;
  [%expect{|
    (0.600000, 0.150000) ;
    (0.900000, 0.750000) ; |}] ;
  P.hide_downto tsl () (fpof 0.75) ;
  let csl = P.Tsl.find tsl () in
  Printf.printf "%f" (F.to_float (P.Csl.umax_revealed csl)) ;
  [%expect{| 0.666667 |}] ;
  print tsl ;
  [%expect{| (0.600000, 0.150000) ; |}] ;
  P.reveal_upto tsl () (fpof 0.95) ;
  print tsl ;
  [%expect{|
    (0.600000, 0.150000) ;
    (0.900000, 0.750000) ; |}]
)

(* reveal_upto a u that is too high does nothing *)
let%test _ = Prm_base.(
  let prm = create () in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  let csl = P.Tsl.find tsl () in
  P.reveal_upto tsl () F.one ;
  let points = P.Csl.points csl in
  (* try to reveal up to 2. -- does nothing more *)
  P.reveal_upto tsl () F.two ;
  let points' = P.Csl.points csl in
  P.Ps.equal points points'
)


(* reveal_upto cancels hide_downto for logp *)
let%test _ = Prm_base.(
  let prm = create () in
  let logp = P.logp prm in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  P.hide_downto tsl () (fpof 0.75) ;
  P.reveal_upto tsl () (fpof 0.95) ;
  let logp' = P.logp prm in
  logp = logp'
)


(* hide_downto cancels graft_upto for logp *)
let%test _ = Prm_base.(
  let prm = rand_draw () in
  let logp = P.logp prm in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  P.graft_upto tsl () (fpof 1.2) ;
  P.hide_downto tsl () F.one ;
  let logp' = P.logp prm in
  logp = logp'
)


(* to test merge we need at least two colors *)
module Col' =
  struct
    type t = bool
    let compare = compare
    let to_string =
      function
      | true ->
          "true"
      | false ->
          "false"
    let of_string =
      function
      | "true" ->
          Some true
      | "false" ->
          Some false
      | _ ->
          None
    let equal = (=)
  end


module Pt' = Sim.Prm.Point (Col')

module Pt_t' = Sim.Prm.Compare_time (Pt')

module P' = Sim.Prm.Make (Pt_t')

let point' ~c ~k ~t ~u =
  Sim.Prm.{ c ; k ; t ; u }

let true_points =
  let c = true in
  let k = 0 in
  P'.Ps.of_list [
    point' ~c ~k ~t:(fpof 0.1) ~u:(fpof 0.9) ;
    point' ~c ~k ~t:(fpof 0.2) ~u:(fpof 0.8) ;
    point' ~c ~k ~t:(fpof 0.8) ~u:(fpof 0.2) ;
    point' ~c ~k ~t:(fpof 0.9) ~u:(fpof 0.1) ;
  ]

let false_points =
  let c = false in
  let k = 0 in
  P'.Ps.of_list [
    point' ~c ~k ~t:(fpof 0.3) ~u:(fpof 0.7) ;
    point' ~c ~k ~t:(fpof 0.4) ~u:(fpof 0.6) ;
    point' ~c ~k ~t:(fpof 0.7) ~u:(fpof 0.3) ;
    point' ~c ~k ~t:(fpof 0.6) ~u:(fpof 0.4) ;
  ]

let vectors' c =
  [(I.two, point' ~c ~k:1 ~t:F.zero ~u:F.one)]

let create' () =
  P'.create
    ~time_range:(F.zero, F.one)
    ~ntslices:I.two
    ~vectors:(vectors' true)
    true_points

let rand_draw' () =
  let rngm = Pt'.Colors.singleton false (U.rng (Some 0)) in
  P'.rand_draw
    ~rngm
    ~time_range:(F.zero, F.one)
    ~ntslices:I.two
    ~vectors:(vectors' false)


(* merging twice the same prm is invalid (same color) *)
let%test _ = Prm_base.(
  let prm = create () in
  match P.merge prm prm with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true
)

(* merged prm has the points of both prms *)
let%test _ =
  let prm = create' () in
  let prm' = rand_draw' () in
  let prm'' = P'.merge prm prm' in
  let tsl = P'.t_slice_of prm F.zero in
  let tsl' = P'.t_slice_of prm' F.zero in
  let tsl'' = P'.t_slice_of prm'' F.zero in
  let pts = P'.Tsl.points tsl in
  let pts' = P'.Tsl.points tsl' in
  let pts'' = P'.Tsl.points tsl'' in
  P'.Ps.equal (P'.Ps.union pts pts') pts''


(* after map_uk_slices, c_slices are not shared
 * if they are shared this is a serious bug
 *)
let%test _ =
  let old_nu = P'.merge (create' ()) (rand_draw' ()) in
  let tsl = P'.t_slice_of old_nu F.zero in
  let new_nu = P'.map_uk_slices (fun x -> x) old_nu tsl true in
  (* a c_slice is shared between old_nu and new_nu *)
  let old_tsl = P'.t_slice_of old_nu F.zero in
  let new_tsl = P'.t_slice_of new_nu F.zero in
  (* old_csl is the same mutable value as new_csl *)
  P'.graft_upto new_tsl false (fpof 1.5) ;
  let deep_vol = F.Pos.Op.(
        P'.Csl.volume (P'.Tsl.find old_tsl false)
      + P'.Csl.volume (P'.Tsl.find old_tsl true)
    )
  in F.(Op.(abs (P'.Tsl.volume old_tsl - deep_vol) < F.of_float 1e-9))
