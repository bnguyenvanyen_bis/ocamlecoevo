open Sim_test.Sig

module P = Prm_base.P


let create () = Prm_base.(
  let key = F.one in
  let range = (F.zero, F.one) in
  let vectors = [(I.Pos.of_int 2), point ~k:1 ~t:F.one ~u:F.one] in
  P.Tsl.create ~key range vectors pointset
)


let draw () = Prm_base.(
  let key = F.one in
  let range = (F.zero, F.one) in
  let vectors = [(I.Pos.of_int 2), point ~k:1 ~t:F.one ~u:F.one] in
  let rngm = P.Cm.singleton () (U.rng (Some 0)) in
  P.Tsl.rand_draw ~rngm ~key range vectors
)


(* key *)
let%test _ =
  let tsl = create () in
  F.Op.(F.one = P.Tsl.key tsl)


(* range *)
let%expect_test _ =
  let tsl = create () in
  let t0, dt = P.Tsl.range tsl in
  Printf.printf "(%f, %f)" (F.to_float t0) (F.to_float dt) ;
  [%expect{| (0.000000, 1.000000) |}]


(* volume *)
let%expect_test _ =
  let tsl = create () in
  let vol = P.Tsl.volume tsl in
  Printf.printf "%f" (F.to_float vol) ;
  [%expect{| 1.000000 |}]


(* add_points *)
let pt () =
  Prm_base.point ~k:0 ~t:(fpof 0.5) ~u:(fpof 0.24)

let%test _ =
  let tsl = create () in
  let points = P.Tsl.points tsl in
  let pts = P.Ps.singleton (pt ()) in
  P.Tsl.add_points pts tsl ;
  let diff_points = P.Ps.diff (P.Tsl.points tsl) points in
  P.Ps.equal pts diff_points


(*
(* add_volume *)
let%test _ =
  let tsl = create () in
  let vol = P.Tsl.volume tsl in
  let dvol = F.one in
  P.Tsl.add_volume dvol tsl ;
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol + dvol = vol')
*)


(* add_color *)
let%test _ =
  let tsl = create () in
  let csl = Prm_csl.create () in
  match P.Tsl.add_color csl tsl with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* volume is identical after hide_u then reveal_u *)
let%test _ =
  let tsl = create () in
  let vol = P.Tsl.volume tsl in
  let _ = P.Tsl.hide_u tsl () in
  let _ = P.Tsl.reveal_u tsl () in
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol = vol')


(*
(* dvol returned makes sense on hide_u *)
let%test _ =
  let tsl = create () in
  let vol = P.Tsl.volume tsl in
  let _ = P.Tsl.hide_u tsl () in
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol = dvol + vol')
*)


(*
(* dvol returned makes sense on reveal_u *)
let%test _ =
  let tsl = create () in
  let _ = P.Tsl.hide_u tsl () in
  let vol = P.Tsl.volume tsl in
  let _, dvol = P.Tsl.reveal_u tsl () in
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol + dvol = vol')
*)


(* graft_u fails on ungraftable *)
let%test _ =
  let tsl = create () in (* ungraftable *)
  match P.Tsl.graft_u tsl () with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* vol is identical after graft_u then hide_u *)
let%test _ =
  let tsl = draw () in
  let vol = P.Tsl.volume tsl in
  let _ = P.Tsl.graft_u tsl () in
  let _ = P.Tsl.hide_u tsl () in
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol = vol')


(*
(* dvol returned makes sense on graft_u *)
let%test _ =
  let tsl = draw () in
  let vol = P.Tsl.volume tsl in
  let _, dvol = P.Tsl.graft_u tsl () in
  let vol' = P.Tsl.volume tsl in
  F.Op.(vol + dvol = vol')
*)
