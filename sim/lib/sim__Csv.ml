open Sig


let convert ?dt ~header ~line ~chan =
  let nar = F.Pos.narrow in
  let chan = Csv.to_channel chan in
  (* previous printing time *)
  let t_r = ref F.zero in
  (* previous value of x (or None before init and after end) *)
  let xo_r = ref None in
  let output t x =
    Csv.output_record chan (line (nar t) x)
  in
  let start (t0 : 't U.anypos) x0 =
    (* I think the problem happens here *)
    t_r := F.Pos.close t0 ;
    xo_r := Some x0 ;
    Csv.output_all chan header ;
    output t0 x0
  in
  (* can we print potentially MORE than that ?
   * to try to print every dt *)
  let out (t : 'u U.anypos) x =
    begin
      match !xo_r with
      | None ->
          failwith "Sim.Csv.output : x not initialized\n"
      | Some x' ->
          (match dt with
           | None ->
               output t x
           | Some dt ->
               while F.Op.((F.Pos.add !t_r dt) < t) do
                 t_r := F.Pos.add !t_r dt ;
                 output !t_r x'
               done
          )
    end ;
    xo_r := Some x
  in
  let return (tf : 'v U.anypos) zf =
    t_r := F.zero ;
    xo_r := None ;
    (nar tf, zf)
  in
  Util.Out.{ start ; out ; return }


(*
let output sim ?(chan=stdout) ?dt ~header ~line =
  let start, out, return = convert ?dt ~header ~line ~chan in
  let output : ('a, 't U.pos * 'a) output = { start ; out ; return } in
  sim ~output
*)


(* compatible with cadlag *)
let convert_compat ?dt ~header ~line ~chan =
  let Util.Out.{ start ; out ; return } =
    convert ?dt ~header ~line ~chan
  in
  (* replace return *)
  let return tf zf =
    ignore (return tf zf) ;
    []
  in
  Util.Out.{ start ; out ; return }
