open Sig


let convert ?dt ~conv =
  let conv' t x =
    (t, conv (F.Pos.narrow t) x)
  in
  (* how do we get cad back ? *)
  let cad_r = ref [] in
  let record t x =
    (* Printf.eprintf "Cadlag record at %f\n" (F.to_float t) ; *)
    cad_r := (conv' t x) :: !cad_r
  in
  let t_r = ref F.zero in
  let xo_r = ref None in
  let start t0 x0 =
    let t0 = F.Pos.close t0 in
    t_r := t0 ;
    xo_r := Some x0 ;
    record t0 x0
  in
  let out t x =
    begin
      match !xo_r with
      | None ->
          failwith "Sim.Csv.output : x not initialized\n"
      | Some x' ->
          begin match dt with
          | None ->
              record (F.Pos.close t) x
          | Some dt ->
              while F.Op.((F.Pos.add !t_r dt) < t) do
                t_r := F.Pos.add !t_r dt ;
                record !t_r x'
              done
          end
    end ;
    xo_r := Some x
  in
  let return tf xf =
    t_r := F.zero ;
    xo_r := None ;
    record (F.Pos.close tf) xf ;
    List.rev !cad_r
  in
  Util.Out.{ start ; out ; return }


(*
let output (sim : (output:('a,'c) output -> 'a -> 'd)) ?dt ~conv =
  (* We'd like smi to accept a [output] argument,
   * but if we try to convert from a [any_output] we obtained 
   * [closed_pos] functions *)
  let start, out, return = convert ?dt ~conv in
  let output : ('a, 'b) output = { start ; out ; return } in
  (* here we want to finish calling sim before returning cad_r.
   * How do we do that ? *)
  sim ~output
*)


(*
let simulate ~conv ?dt ~next auxf p x0 =
  let obegin, oevery, oend, cad_r = convert ~conv ?dt in
  ignore (Loop.simulate ~obegin ~oevery ~oend ~next auxf p x0) ;
  List.rev !cad_r

let simulate_until ~conv ?dt ~next tf x0 =
  let obegin, oevery, oend, cad_r = convert ~conv ?dt in
  ignore (Loop.simulate_until ~obegin ~oevery ~oend ~next tf x0) ;
  List.rev !cad_r
*)
