(* Implements the midpoint method. *)

open Sig

module Lac = Lacaml.D
module Vec = Lac.Vec
module Mat = Lac.Mat

type t = Lac.vec

type algparam = U.closed_pos

let specl = [
  ("-h", Lift_arg.Float (fun _ x -> F.Pos.of_float x),
   "Integration step size") ;
]

let default = F.Pos.of_float 1e-3


let integrate ?tmp ?(apar=default) dim in_domain to_domain field =
  let h = apar in
  let hd2 = F.Pos.Op.(h / F.two) in
  (* tmp vectors of storage *)
  let tmp1, tmp2, ymid =
    match tmp with
    | None ->
        (Vec.make0 dim, Vec.make0 dim, Vec.make0 dim)
    | Some a ->
        a
  in
  let next t y =
    let _ = field ~z:tmp1 t y in
    Lac.scal ~n:dim (F.to_float hd2) tmp1 ;
    let _ = Vec.add ~n:dim ~z:ymid y tmp1 in
    let tmid = F.Pos.Op.(t + hd2) in
    let _ = field ~z:tmp2 tmid ymid in
    Lac.axpy ~n:dim ~alpha:(F.to_float h) tmp2 y ;
    if not (in_domain y) then (to_domain ~z:y y) ;
    let t' = F.Pos.Op.(t + h) in
    (t', y)
  in next

