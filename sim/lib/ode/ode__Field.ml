module Lac = Lacaml.D

let gemv_field dim init update par =
  let a = Lac.Mat.make0 dim dim in
  init ~z:a par ;
  let field ~z t y =
    update ~z:a par t y ;
    Lac.gemv ~alpha:1. ~beta:0. ~y:z a y
  in field
