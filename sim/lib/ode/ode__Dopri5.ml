(* A bit of a simplified port of dopri5.f : less options *)
(* Implements the Dormand-Prince method of order 5 (4) *)

open Sig

module Lac = Lacaml.D
module Vec = Lac.Vec
module Mat = Lac.Mat

type t = Lac.vec

type algparam = {
  h0 : U.closed_pos ;
  delta : U.closed_pos ;
  min_step : U.closed_pos ;
  max_step : U.closed_pos
}

let specl = [
  ("-h0",
   Lift_arg.Float (fun apar x -> { apar with h0 = F.Pos.of_float x }),
   "Initial integration step size") ;
  ("-delta",
   Lift_arg.Float (fun apar x -> { apar with delta = F.Pos.of_float x }),
   "Dormand Prince method delta parameter") ;
  ("-min_step",
   Lift_arg.Float (fun apar x -> { apar with min_step = F.Pos.of_float x }),
   "Minimum integration step size") ;
  ("-max_step",
   Lift_arg.Float (fun apar x -> { apar with max_step = F.Pos.of_float x }),
   "Maximum integration step size") ;
]

let h0 apar = F.Pos.narrow apar.h0
let delta apar = F.Pos.narrow apar.delta
let min_step apar = F.Pos.narrow apar.min_step
let max_step apar = F.Pos.narrow apar.max_step

let default = {
  h0 = F.Pos.of_float 1e-3 ;
  delta = F.Pos.of_float 0.1 ;
  min_step = F.Pos.of_float 1e-6 ;
  max_step = F.Pos.of_float 1.
}

(*
module type SYSTEM =
  sig
    type param
    val n : int

    val a : Lacaml.D.Mat.t

    val init_a :
      param ->
      unit

    val f :
      param ->
      ?z:t ->
      float -> 
      t -> 
      t
    (*
    val aux_fun :
      ?auxi : t ->
      param ->
      float ->
      t ->
      t
    *)
    val check_in_domain :
      t ->
      bool
    val shift_in_domain : 
      ?z:t ->
      t ->
      t 
  end


module type SPEC =
  sig
    type param
    val n : int
    val aux_line : 'a -> string list
  end
*)

(* declare the coefficients in the top namespace *)
let c = Vec.of_array 
        [| 
          0.; 1. /. 5.; 3. /. 10.; 4. /. 5.; 8. /. 9.; 1.; 1. 
        |];;

let b5 = Vec.of_array 
         [| 
           35. /. 384. ; 
           0. ; 
           500. /. 1113. ; 
           125. /. 192. ; 
           ~-. 2187. /. 6784. ;
           11. /. 84. ; 
           0. 
         |];;

let b4 = Vec.of_array 
          [|
            5179. /. 57600. ;
            0. ;
            7571. /. 16695. ;
            393. /. 640. ;
            ~-. 92097. /. 339200. ;
            187. /. 2100. ;
            1. /. 40.
          |]

let a = Mat.of_array 
        [|
          [| 0.; 0.; 0.; 0.; 0.; 0.; 0. |] ;
          [| 1. /. 5.; 0.; 0.; 0.; 0.; 0.; 0. |] ;
          [| 3. /. 40.; 9. /. 40.; 0.; 0.; 0.; 0.; 0. |] ;
          [| 44. /. 45.; ~-. 56. /. 15.; 32. /. 9.; 0.; 0.; 0.; 0. |] ;
          [| 19372. /.6561.; ~-. 25360. /. 2187.; 64448. /. 6561.; 
            ~-. 212. /. 729.; 0.; 0.; 0. |] ; [| 9017. /. 3168.; ~-. 355. /. 33.; 46732. /. 5247.; 
            49. /. 176.; ~-. 5103. /. 18656.; 0.; 0. |] ;
          [| 35. /. 384.; 0.; 500. /. 1113.; 125. /. 192.; 
            ~-. 2187. /. 6784.; 11. /. 84.; 0. |] 
        |]	


(*
module Csv (S : SPEC) :
  (OUTSPEC with type t = t
            and type param = S.param) =
  struct
    module CsvO = Sim_out.Csv (struct
      type nonrec t = t
      type param = S.param
      let header =
        [["n=" ^ string_of_int S.n]]
      (* FIXME find a way to output h ("%.2e" h after t before) *)
      let line par t y =
        let l = ref [] in
        Vec.iter (fun x -> l := Printf.sprintf "%.3e" x :: !l) y ;
        (Printf.sprintf "%f" t) :: (List.rev !l)
    end)
    include CsvO
  end
*)


(* FIXME add n m for gemv *)
let update_k ~tmp1 ~tmp2 ~dim ~field t h k y i =
  let tmp1 = Lac.copy ~n:dim ~y:tmp1 y in
  let tmp1 = Lac.gemv
    ~trans:`N
    ~alpha:(F.to_float h)
    ~beta:1.
    ~y:tmp1
    k
    (Mat.copy_row a i)
  in
  let tmp2 =
    field ~z:tmp2 F.Pos.Op.(t + h * F.of_float_unsafe c.{i}) tmp1
  in
  for j = 1 to dim do
    k.{j,i} <- tmp2.{j}
  done;
  k
    
let compute_k ~tmp1 ~tmp2 ~dim ~field t h k y =
  (* we start with a clean k again *)
  Mat.fill k 0.;
  for i = 1 to 7 do
    ignore (update_k ~tmp1 ~tmp2 ~dim ~field t h k y i) ;
  done;
  k

(* FIXME add n m for copy gemv etc *)
(* fourth-order evaluation *)
let runge_kutta ~tmp1 h k ny y b =
  (* put y in tmp1 *)
  let tmp1 = Lac.copy ~y:tmp1 y in
  let tmp1 = Lac.gemv ~trans:`N ~alpha:(F.to_float h) ~beta:1. ~y:tmp1 k b in
  (* let fmt = Format.std_formatter in *)
  (* L.pp_vec fmt tmp1 ; *)
  (* put the result in ny *)
  Lac.copy ~y:ny tmp1


(* positive 0 decimal 9 *)
let p0d9 = F.Pos.of_float 0.9
(* positive 0 decimal 2 *)
let p0d2 = F.Pos.of_float (1. /. 5.)


(* FIXME This is weird h is not used *)
let update_h apar e _ (* h *) =
  let scl = F.Pos.Op.(p0d9 * ((delta apar) / e) ** p0d2) in
  let h = F.Pos.Op.(p0d9 * scl) in
  if F.Op.(h < (min_step apar)) then
    (min_step apar)
  else if F.Op.(h > (max_step apar))
       || (F.classify h = FP_nan) then
    (max_step apar)
  else 
    h


let integrate ?tmp ?(apar=default) dim in_domain to_domain field =
    (* tmp storages, for solutions of order 4 and 5, and RK matrix *)
    let tmp1, tmp2, y4, y5, k =
      match tmp with
      | None ->
          (Vec.make0 dim, Vec.make0 dim,
           Vec.make0 dim, Vec.make0 dim,
           Mat.make0 dim 7)
      | Some a ->
          a
    in
    let h_r = ref (h0 apar) in
    let next (t : 'a U.anypos) y =
      (* L.pp_vec fmt y4 ; *)
      let h = !h_r in
      (* L.pp_mat fmt k ; *)
      let k = compute_k ~tmp1 ~tmp2 ~dim ~field t h k y in
      (* note that on a big system it would be interesting to parallelize here *)

      (* L.pp_mat fmt k ; *)
      ignore (runge_kutta ~tmp1 h k y4 y b4) ;
      (* L.pp_vec fmt y4 ; *)
      ignore (runge_kutta ~tmp1 h k y5 y b5) ;
      (* L.pp_vec fmt y4 ; *)
      let _ = Vec.sub ~n:dim ~z:tmp1 y5 y4 in (* tmp gets y5 - y4 *)
      let t' = F.Pos.Op.(t + h) in
      (* e is positive because amax looks at absolute values *)
      let e = F.of_float_unsafe (Lac.amax tmp1) in
      h_r := update_h apar e h ;
      let y = Lac.copy ~y y5 in
      (* L.pp_vec fmt y4 ; *)
      (* FIXME alternatively retry with smaller h ? *)
      if not (in_domain y) then (to_domain ~z:y y) ;
      (* L.pp_vec fmt y4 ; *)
      (t', y)
    in next

