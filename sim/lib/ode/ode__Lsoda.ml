(* Provide an interface to [Odepack.lsoda] similar to other modules. 
 *
 * Because Odepack provides a different system, where one advances
 * the solution to the times one desires.
 * That is fundamentally different from our 'next' system.
 * It also requires the user to provide the starting condition
 * and final time to start the solver.
 *)

open Sig
module O = Odepack

type vofon =
  | Vec of O.vec
  | Pos of U.closed_pos
  | Nothing

type algparam = {
  atol : vofon ;
  rtol : vofon ;
  debug : bool option ;
  mxstep : int option ;
  mimic_h : U.closed_pos ;
}


module Term =
  struct
    open Cmdliner

    let atol =
      let doc =
        "Absolute tolerance parameter."
      in
      let arg =
        Arg.(value
           & opt (some (U.Term.positive ())) None
           & info ["ode-atol"] ~docv:"ODE-ATOL" ~doc
        )
      in
      let f =
        function
        | None ->
            (fun apar -> apar)
        | Some atol ->
            (fun apar -> { apar with atol = Pos atol })
      in
      Term.(const f
          $ arg
      )

    let rtol =
      let doc =
        "Relative tolerance parameter."
      in
      let arg =
        Arg.(value
           & opt (some (U.Term.positive ())) None
           & info ["ode-rtol"] ~docv:"ODE-RTOL" ~doc
        )
      in
      let f =
        function
        | None ->
            (fun apar -> apar)
        | Some rtol ->
            (fun apar -> { apar with rtol = Pos rtol })
      in
      Term.(const f
          $ arg
      )

    let mimic_h =
      let doc =
        "Integration step size."
      in
      let arg =
        Arg.(value
           & opt (some (U.Term.positive ())) None
           & info ["ode-h"] ~docv:"ODE-H" ~doc
        )
      in
      let f =
        function
        | None ->
            (fun apar -> apar)
        | Some mimic_h ->
            (fun apar -> { apar with mimic_h })
      in
      Term.(const f
          $ arg
      )

    let mxstep =
      let doc =
          "Maximum number of steps allowed during one call to the solver. " 
        ^ "Defaults to 500."
      in
      let arg =
        Arg.(value
           & opt (some int) None
           & info ["ode-mxstep"] ~docv:"ODE-MXSTEP" ~doc
        )
      in
      let f mxstep apar =
        { apar with mxstep }
      in
      Term.(const f
          $ arg
      )

    let apar =
      let f atol rtol mimic_h mxstep apar =
        apar
        |> atol
        |> rtol
        |> mimic_h
        |> mxstep
      in
      Term.(const f
          $ atol
          $ rtol
          $ mimic_h
          $ mxstep
      )
  end


let specl = [
  ("-atol",
   Lift_arg.Float (fun apar x -> { apar with atol = Pos (F.Pos.of_float x) }),
   "Absolute tolerance parameter.") ;
  ("-rtol",
   Lift_arg.Float (fun apar x -> { apar with rtol = Pos (F.Pos.of_float x) }),
   "Relative tolerance parameter.") ;
  ("-h",
   Lift_arg.Float (fun apar x -> { apar with mimic_h = F.Pos.of_float x }),
   "Integration step size.") ;
  ("-mxstep", Lift_arg.Int (fun apar n -> { apar with mxstep = Some n }),
   "Maximum number of steps allowed during one call to the solver. Defaults to 500.") ;
]

let default = {
  atol = Nothing ;
  rtol = Nothing ;
  debug = None ;
  mxstep = None ;
  mimic_h = F.Pos.of_float 1e-3 ;
}

let tol : vofon -> (float option * O.vec option) = function
  | Vec v ->
      (None, Some v)
  | Pos x ->
      (Some (F.to_float x), None)
  | Nothing ->
      (None, None)


let integrate ?(apar=default) ?jac in_domain to_domain field x0 =
  let f t y y' =
    (* of_float_unsafe unsatisfactory but we don't want to check every time *)
    ignore (field ~z:y' (F.of_float_unsafe t) y)
  in
  let h = apar.mimic_h in
  let rtol, rtol_vec = tol apar.rtol in
  let atol, atol_vec = tol apar.atol in
  let debug = apar.debug in
  let mxstep = apar.mxstep in
  let sol =
    O.lsoda ?rtol ?rtol_vec ?atol ?atol_vec ?debug ?mxstep ?jac f x0 0. 0.
  in
  (* FIXME it's a bit weird that we don't need y -- is that really ok ? *)
  let next t _ =
    let t' = F.Pos.Op.(t + h) in
    O.advance ~time:(F.to_float t') sol ;
    let t'' = F.of_float_unsafe (O.time sol) in
    (* y' should share memory with y ? how to ensure that ? *)
    let y' = O.vec sol in
    if not (in_domain y') then (to_domain ~z:y' y') ;
    assert F.Op.(t'' = t') ;
    (t', y')
  in next

