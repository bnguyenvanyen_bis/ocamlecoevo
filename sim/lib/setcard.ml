module Make (Ord : BatSet.OrderedType) :
  (BatSet.S with type elt = Ord.t) =
  struct
    module Set = BatSet.Make (Ord)

    type elt = Ord.t
    type t = Set.t * int

    let lift set =
      (set, Set.cardinal set)

    let empty = (Set.empty, 0)

    let is_empty (_, n) = (n = 0)

    let singleton x = (Set.singleton x, 1)

    let mem x (set, _) = Set.mem x set [@@ inline]

    let find x (set, _) = Set.find x set [@@ inline]

    let add x (set, n) =
      (Set.add x set, if Set.mem x set then n else n + 1)

    let remove x (set, n) =
      (Set.remove x set, if Set.mem x set then n - 1 else n)

    let update _ _ _ =
      failwith "Not_implemented"

    let union (set, _) (set', _) =
      lift (Set.union set set')

    let inter (set, _) (set', _) =
      lift (Set.inter set set')

    let diff (set, _) (set', _) =
      lift (Set.diff set set')

    let sym_diff (set, _) (set', _) =
      lift (Set.sym_diff set set')

    let compare (set, _) (set', _) =
      Set.compare set set' [@@ inline]

    let equal (set, _) (set', _) =
      Set.equal set set' [@@ inline]

    let subset (set, _) (set', _) =
      Set.subset set set' [@@ inline]

    let disjoint (set, _) (set', _) =
      Set.disjoint set set' [@@ inline]

    let compare_subset (set, _) (set', _) =
      Set.compare_subset set set' [@@ inline]

    let iter f (set, _) =
      Set.iter f set [@@ inline]

    let at_rank_exn i (set, n) =
      if i >= n then
        invalid_arg "at_rank_exn"
      else
        Set.at_rank_exn i set

    let map f (set, _) =
      lift (Set.map f set)

    let filter f (set, _) =
      lift (Set.filter f set)

    let filter_map f (set, _) =
      lift (Set.filter_map f set)

    let fold f (set, _) a =
      Set.fold f set a [@@ inline]

    let for_all f (set, _) =
      Set.for_all f set [@@ inline]

    let exists f (set, _) =
      Set.exists f set [@@ inline]

    let partition f (set, n) =
      let set', set'' = Set.partition f set in
      let n' = Set.cardinal set' in
      ((set', n'), (set'', n - n'))

    let split x (set, n) =
      let setl, present, setr = Set.split x set in
      let nl = Set.cardinal setl in
      ((setl, nl), present, (setr, if present then n - nl - 1 else n - nl))

    let split_opt x (set, _) =
      let setl, maybe_v, setr = Set.split_opt x set in
      (lift setl, maybe_v, lift setr)

    let split_lt x (set, n) =
      let setl, setr = Set.split_lt x set in
      let nl = Set.cardinal setl in
      ((setl, nl), (setr, n - nl))

    let split_le x (set, n) =
      let setl, setr = Set.split_le x set in
      let nl = Set.cardinal setl in
      ((setl, nl), (setr, n - nl))

    let cardinal (_, n) =
      n [@@ inline]

    let elements (set, _) =
      Set.elements set [@@ inline]

    let to_list (set, _) =
      Set.to_list set [@@ inline]

    let to_array (set, _) =
      Set.to_array set [@@ inline]

    let min_elt (set, _) =
      Set.min_elt set [@@ inline]

    let pop_min (set, n) =
      let x, set' = Set.pop_min set in
      (* if it worked, n - 1 (otherwise Not_found already) *)
      x, (set', n - 1)

    let pop_max (set, n) =
      let x, set' = Set.pop_max set in
      (* if it worked, n - 1 (otherwise Not_found already) *)
      x, (set', n - 1)

    let max_elt (set, _) =
      Set.max_elt set [@@ inline]

    let choose (set, _) =
      Set.choose set [@@ inline]

    let any (set, _) =
      Set.any set [@@ inline]

    let pop (set, n) =
      let x, set' = Set.pop set in
      (* if it worked, n - 1 (otherwise Not_found already) *)
      x, (set', n - 1)

    let enum (set, _) =
      Set.enum set [@@ inline]

    let backwards (set, _) =
      Set.backwards set [@@ inline]

    let of_enum e =
      lift (Set.of_enum e)

    let of_list l =
      lift (Set.of_list l)

    let of_array a =
      lift (Set.of_array a)

    let print ?first ?last ?sep elt_printer output (set, _) =
      Set.print ?first ?last ?sep elt_printer output set

    module Infix =
      struct
        let (<--) s x = add x s

        let (<.) _ _ =
          failwith "Not_implemented"

        let (>.) _ _ =
          failwith "Not_implemented"

        let (<=.) s s' =
          subset s s'

        let (>=.) _ _ =
          failwith "Not_implemented"

        let (-.) s s' =
          diff s s'

        let (&&.) s s' =
          inter s s'

        let (||.) s s' =
          union s s'
      end

    module Exceptionless =
      struct
        let min_elt (set, _) =
          Set.Exceptionless.min_elt set

        let max_elt (set, _) =
          Set.Exceptionless.max_elt set

        let choose (set, _) =
          Set.Exceptionless.choose set

        let any (set, _ ) =
          Set.Exceptionless.any set

        let find x (set, _) =
          Set.Exceptionless.find x set
      end

    module Labels =
      struct
        let iter ~f s =
          iter f s

        let fold ~f s ~init =
          fold f s init

        let for_all ~f s =
          for_all f s

        let exists ~f s =
          exists f s

        let map ~f s =
          map f s

        let filter ~f s =
          filter f s

        let filter_map ~f s =
          filter_map f s

        let partition ~f s =
          partition f s
      end
  end
