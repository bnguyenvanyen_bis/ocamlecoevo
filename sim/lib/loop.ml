open Sig


(* annoyingly we use this a lot *)
let nar = F.Pos.narrow


(* let em9 = F.Pos.of_float 1e-9 *)
let em8 = F.Pos.of_float 1e-8
let em6 = F.Pos.of_float 1e-6


(*
let combine f
  (o : ('a,'b,'s) any_output)
  (o' : ('a,'c,'t) any_output) :
    ('a,'d,'u) any_output = {
  start = (fun t x -> o.start (nar t) x ; o'.start (nar t) x) ;
  out = (fun t x -> o.out (nar t) x ; o'.out (nar t) x) ;
  return = (fun t x -> f (o.return (nar t) x) (o'.return (nar t) x)) ;
}
*)


let smallest ?(n=20) p t =
  assert F.Op.(t < F.infinity) ;
  let rec f n t =
    match n with
    | 0 ->
        failwith "exhausted payload"
    | _ ->
        let t' = F.Pos.mul F.two t in
        if p t' then
          t'
        else
          f (n - 1) t'
  in
  if F.Op.(t <> F.zero) then
    f n (nar t)
  else
    f n (F.Pos.of_float 1e-1)


let bissect ?(tol=em8) p t dt =
  assert F.Op.(dt < F.infinity) ;
  let rec f t dt =
    if F.Op.(dt < tol) then
      F.Pos.add t dt
    else
      let dt' = F.Pos.div dt F.two in
      let t' = F.Pos.add t dt' in
      if p t' then
        (* left interval *)
        f t dt'
      else
        (* right interval *)
        f t' dt'
  in f (nar t) (nar dt)


let simulate
    ~(next:('a,'v,'w) nextfun)
    ~(output:('a,'b,'t) output)
    (x0:'a)
    (auxf:('a,'c,'x) auxfun)
    (p:('a,'c,'y) pred) =
  (* We need to call this function in many places to make the types
   * behave like we want them to (it should be a noop though) *)
  let pred x auxi t =
    let nauxi = auxf ~auxi (nar t) x in
    p (nar t) x nauxi
  in
  let bissect (t, x, auxi) (t', x', auxi') =
    let p = pred x auxi in
    (* we need to see if only the new state fulfills the condition *)
    if   (p (nar t'))
      && (p (F.Pos.of_anyfloat F.Op.(t' - em6))) then
      (* ok so we can actually bissect *)
      let t' =
        if F.Op.(t' = F.infinity) then
          smallest p t
        else
          nar t'
      in
      let t'' = bissect p t t' in
      (nar t'', x, auxf ~auxi (nar t'') x)
    else
      (nar t', x', auxi')
  in
  let rec to_the_end t x auxi =
    output.out (nar t) x ;
    let nt, nx = next (nar t) x in
    let nauxi = auxf ~auxi (nar nt) nx in
    if not (p (nar nt) nx nauxi) then
      to_the_end (F.Pos.close nt) nx nauxi
    else
      let tf, xf, auxif = bissect (t, x, auxi) (nt, nx, nauxi) in
      (tf, xf, auxif)
  in
  let t0 = F.zero in
  output.start t0 x0 ;
  let auxi0 = auxf (nar t0) x0 in
  let tf, xf, _ = to_the_end (F.Pos.close t0) x0 auxi0 in
  output.return (nar tf) xf


let simulate_return ~next x0 auxf pred =
  let output : ('a, 'b, 't) output = {
    start = (fun _ _ -> ()) ;
    out = (fun _ _ -> ()) ;
    return = (fun t x -> (t, x))
  } in
  simulate ~output ~next x0 auxf pred


let simulate_until ~next ~output x0 tf =
  let auxf ?(auxi=()) _ _ = ignore auxi ; () in
  (* FIXME is that right ? *)
  let pred t _ _ = F.Op.(t + em8 >= tf) in
  simulate ~output ~next x0 auxf pred
  
