open Ctmjp__Sig


let combine rate modif =
  (fun t x -> (rate t x, (fun stoch -> modif stoch t x)))


let combine_auto rate modif =
  (fun x -> (rate x, (fun stoch -> modif stoch x)))


(* this is pretty dumb to use *)
let split rate_modif =
  let rate t x =
    let r, _ = rate_modif t x in
    r
  in
  let modif t x =
    let _, m = rate_modif t x in
    m
  in (rate, modif)


let chain modif modif' stoch t x =
  x
  |> modif stoch t
  |> modif' stoch t


let chain_auto modif modif' stoch x  =
  x
  |> modif stoch
  |> modif' stoch


(* Functions with rng *)

let choose_between p m m' rng t x =
  if U.rand_bernoulli ~rng (p t x) then
    m rng t x
  else
    m' rng t x


let erase p modif rng t x =
  if U.rand_bernoulli ~rng (p t x) then
    x
  else
    modif rng t x


let erase_auto p modif rng x =
  if U.rand_bernoulli ~rng (p x) then
    x
  else
    modif rng x


let add rm rm' t x =
  let r, m = rm t x in
  let r', m' = rm' t x in
  (* p is r / (r + r') *)
  let tot, p, _ = F.Pos.proportions r r' in
  let m_choice rng =
    if U.rand_bernoulli ~rng p then
      m rng
    else
      m' rng
  in (tot, m_choice)


let add_many rms t x =
  (* compute the rates and prepare the modifs *)
  let rs, ms = L.split (L.map (fun rm -> rm t x) rms) in
  (* partial_sums is the partial cumulative sums of rates in decreasing order
   * so the order is reversed compared to rs and ms *)
  let tot, partial_sums = U.cumul_fold_left F.Pos.add F.zero rs in
  let m_choice rng =
    (* we need to reverse partial_sums to get back the right correspondence
     * with rs *)
    let m =
      U.rand_cumul_choose ~rng ~tot (L.combine ms (L.rev partial_sums))
    in m rng
  in (tot, m_choice)


(* Functions with point *)

let erase_point p modif (pt : 'a Prm.point) t x =
  if Prm.bernoulli pt (p t x) then
    x
  else
    (* FIXME do we rescale u here ? *)
    modif pt t x


let erase_point_auto p modif (pt : 'a Prm.point) x =
  if Prm.bernoulli pt (p x) then
    x
  else
    (* FIXME do we rescale u here ? *)
    modif pt x


(* we should never need to use this *)
let add_point rm rm' t x =
  let r, m = rm t x in
  let r', m' = rm' t x in
  (* p is r / (r + r') *)
  let tot, p, _ = F.Pos.proportions r r' in
  let m_choice pt =
    (* FIXME might need to rescale here *)
    if Prm.bernoulli pt p then
      m pt
    else
      m' pt
  in (tot, m_choice)


(* also useless *)
let add_many_point rms t x =
  let rs, ms = L.split (L.map (fun rm -> rm t x) rms) in
  let tot, partial_sums = U.cumul_fold_left F.Pos.add F.zero rs in
  let m_choice pt =
    let m =
      Prm.cumul_choose pt ~tot (L.combine ms (L.rev partial_sums))
    in m pt
  in (tot, m_choice)
