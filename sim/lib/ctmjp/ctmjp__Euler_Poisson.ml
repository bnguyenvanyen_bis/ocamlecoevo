open Ctmjp__Sig


type algparam = epar_algparam


let specl = Lift_arg.([
  ("-overshoot",
   Float (fun apar x ->
       { apar with overshoot = F.Pos.close (F.Pos.of_float x) }),
   "Additional proposed events simulated (to avoid redrawing variables).") ;
  ("-step_size",
   Float (fun apar x ->
       { apar with step_size = F.Pos.close (F.Pos.of_float x) }),
   "Integration step size.") ;
])


let default = {
  overshoot = F.Pos.of_float 0.01 ;
  step_size = F.Pos.of_float 1e-3 ;
}


(*
let nomodif _ _ x = x
*)

let erase p modif x rng =
  if U.rand_bernoulli ~rng p then
    x
  else
    modif rng


let integrate ?(apar=default) (rate_modif : ('a, 't, 's) rng_rate_modif) rng =
  let over = F.Pos.narrow apar.overshoot in
  let step_size = F.Pos.narrow apar.step_size in
  let overp1 = F.Pos.Op.(over + F.one) in
  let rec process_event t _ x n_events lbd =
    match n_events with
    | 0 -> x (* all events have been processed *)
    | _ ->
        begin
          (* compute the natural rate of the system *)
          let lbd', modif = rate_modif t x in
          let nlbd, n_add, erase_modif =
            let olbd = F.Pos.Op.(lbd * overp1) in
            if F.Op.(lbd' > olbd) then
              (* n_add : add new events *)
              (* dlbd positive here since lbd' > lbd *)
              let dlbd = F.promote_unsafe F.Op.(lbd' - lbd) in
              let lambd = F.Pos.Op.(step_size * overp1 * dlbd) in
              let n_add = I.to_int (Util.rand_poisson ~rng lambd) in
              (lbd', n_add, modif)
            else (* this is the hot path hopefully *)
              (* erase_event : reject over-dropped events *)
              (* FIXME could this use its arguments for proba ? *)
              (* FIXME can't "symbolically" erase here,
               * could we just provide a different "compilation" path ? *)
              let p = F.promote_unsafe F.Pos.Op.(lbd' / olbd) in
              (lbd, 0, erase p modif x)
          in
          let nx = erase_modif rng in
          (* we have processed one event but possibly added new ones *)
          process_event t x nx (n_events - 1 + n_add) nlbd
        end
  in
  let next t x =
    (* initialize rate and number of events *)
    let lbd0, _ = rate_modif t x in
    (* Generate more events than strictly needed
     *  (then reject the over-dropped events) *)
    let lambd0 = F.Pos.Op.(step_size * overp1 * lbd0) in
    let n_events0 = I.to_int (Util.rand_poisson ~rng lambd0) in
    let next_t = F.Pos.Op.(t + step_size) in
    let next_x = process_event t x x n_events0 lbd0 in
    (next_t, next_x)
  in next
