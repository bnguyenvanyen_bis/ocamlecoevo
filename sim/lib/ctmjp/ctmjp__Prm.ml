open Ctmjp__Sig


let ounit _ _ = ()


module Make (Pt : POINT) =
  struct
    (* FIXME use custom hash function ? *)
    module H = BatHashtbl
    module P = Prm.Make (Prm.Compare_time (Pt))
    module Cm = Pt.Colors

    let incr = F.Pos.add (F.Pos.of_float 0.1) Prm.incrp1

    let increase_bounds
      ?(adjust=true)
      rmaxs rate_vals tsl t x c rate_modif break =
      (* c_slice corresponding to the color *)
      let csl = P.Tsl.find tsl c in
      (* An assert check that everything goes well,
       * FIXME Should be taken out when we're confident enough. *)
      P.Csl.compare_bounds csl ;
      (* tested event rate (in memory or recomputed) *)
      let r = (* rate t x in
      if (H.find rmaxs c) < r then (H.replace rmaxs c r) ;
      *)
        match H.find rate_vals c with
        | Some r ->
            r
        | None ->
            let r, _ = rate_modif t x in
            (* store new rate *)
            H.replace rate_vals c (Some r) ;
            (* replace rmax if needed *)
            if H.find rmaxs c < r then
              H.replace rmaxs c r ;
            r
      in
      let uf = P.Csl.umax_revealed csl in
      if adjust then
        begin
          (* r dangerous *)
          let sclr = F.Pos.Op.(incr * r) in
          if sclr > uf then begin
            (* stop : reveal or graft *)
            P.reveal_graft_upto tsl c sclr ;
            `Refresh
          end else
            break
          end
      else
        if r > uf then
          failwith "Out_of_bounds"
        else
          break
      

    let lower_bounds
      ?(adjust=true)
      tsl c rmax =
      (* c_slice corresponding to the color *)
      let csl = P.Tsl.find tsl c in
      let uf = P.Csl.umax_revealed csl in
      if adjust then begin
        let sclr = F.Pos.Op.(incr * rmax) in
        if F.Pos.Op.(incr * sclr) < uf then begin
          (* hide *)
          P.hide_downto tsl (* csl *) c sclr
        end
      end


    let simulate_until
      ~(output : ('b,'c,'t) output)
      (evm : 'a P.map)
      x0 (prm : P.t) =
      (* ~~ *)
      (* FIXME should remove at some point, but for now it's useful info *)
      let total_points = ref 0 in
      let accepted_points = ref 0 in
      let t0, tf = P.time_range prm in
      (* store of values of rates for all colors *)
      let rate_vals = H.create (Cm.cardinal evm) in
      (* store of max rate value observed for all colors in time slice *)
      let rmaxs = H.create (Cm.cardinal evm) in
      (* partial application *)
      let incr_bnds = increase_bounds ~adjust:true rmaxs rate_vals in
      let lwr_bnds = lower_bounds ~adjust:true in
      Cm.iter (fun c _ ->
        (* init rate_vals *)
        H.add rate_vals c None ;
        H.add rmaxs c F.zero
      ) evm ;
      let refresh_bounds tsl t x =
        Cm.fold (incr_bnds tsl t x) evm `Continue
      in
      (* for each point *)
      let f tsl (pt : P.point) x =
        total_points := 1 + !total_points ;
        let color = Pt.color pt in
        let rate_modif = Cm.find color evm in
        let t = Pt.time pt in
        (* we don't use rh here because we want to be exact *)
        let r, modif = rate_modif t x in
        (* FIXME should we only when something happens ?
         * But we're not really sure that something happens... *)
        let x' =
          if F.Op.(Pt.value pt <= r) then
            begin
              output.out t x ;
              accepted_points := 1 + !accepted_points ;
              (* forget rates *)
              H.map_inplace (fun _ _ -> None) rate_vals ;
              (* apply modif *)
              modif (Pt.with_value F.Pos.Op.(Pt.value pt / r) pt)
            end
          else
            x
        in
        match refresh_bounds tsl t x' with
        | `Continue ->
            `Continue x'
        | `Refresh ->
            `Refresh x'
      in
      (* for each time slice *)
      let g tsl x =
        ignore (refresh_bounds tsl (tsl |> P.Tsl.range |> fst) x) ;
        let x' = P.Tsl.fold_points (f tsl) tsl x in
        H.iter (lwr_bnds tsl) rmaxs ;
        (* at the end of the slice we can reinitialize rmax *)
        H.map_inplace (fun _ _ -> F.Pos.narrow F.zero) rmaxs ;
        x'
      in
      output.start t0 x0 ;
      let xf = P.fold_slices g prm x0 in
      Printf.eprintf "Accepted points : %i out of %i\n%!"
      !accepted_points !total_points ;
      output.return tf xf
  end
