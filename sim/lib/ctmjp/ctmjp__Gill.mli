(** Simulate a Markovian jump process where events are grouped together.
    Each group has an associated rate and each event of the group has
    a probability of accepting the event.
    This algorithm is equivalent to the classical SSA
    (so in particular it is exact),
    but scales well with the number of simultaneous events.
    As such it is a good choice for a biological population,
    where the number of events, similar but different, can be very high. *)

open Ctmjp__Sig


val specl : 'a specs

val integrate :
  ('a, 's, 't) rng_rate_modif ->
  rng ->
  's U.anypos ->
  'a ->
    ('t U.pos * 'a)
