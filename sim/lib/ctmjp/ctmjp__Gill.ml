module L = List

module U = Util
module F = U.Float

open Ctmjp__Sig


let specl = []


(* it looks like it just doesn't want to admit that I can return
 * a 'a pos. why ? It worked inside Util (outside Float)... *)

let integrate (rate_modif : ('a,'s,'t) rng_rate_modif) rng =
  let next (t : 'b U.anypos) (x : 'a) =
    (* get the rate *)
    (* FIXME here lambda is closed_pos why ? *)
    let lambd, modif = rate_modif t x in
    if F.Op.(lambd < F.zero) then
      (* FIXME replace by Error `Negative_rate *)
      raise Simulation_error
    else if F.Op.(lambd = F.zero) then
      (* FIXME replace by Ok *)
      (F.infinity, x)
    else
      begin
        let elapsed_t = U.rand_exp ~rng lambd in
        let next_t = F.Pos.Op.(t + elapsed_t) in
        (* get the new state *)
        let next_x = modif rng in
        (* FIXME replace by Ok *)
        (next_t, next_x)
      end
  in next

