(** Types and signatures reused elsewhere. *)
include Sig

(* FIXME we'd like versions that allow the use of 'a Prm.point instead of rng *)

(* As this is used as argument, we do the same weird thing as in Sig,
 *  go from anypos to pos. *)

type ('a, 's, 't) rate = ('s U.pos -> 'a -> 't U.anypos)

type ('a, 'stoch, 't) modif = ('stoch -> 't U.pos -> 'a -> 'a)

type ('a, 'stoch, 's, 't) rate_modif =
  ('s U.pos -> 'a -> 't U.anypos * ('stoch -> 'a))


type ('a, 't) rng_modif = ('a, rng, 't) modif

type ('a, 's, 't) rng_rate_modif = ('a, rng, 's, 't) rate_modif


type 'a specs = (string * 'a Lift_arg.spec * string) list



type step_algparam = {
  step_size : closed_pos
}

type epar_algparam = {
  overshoot : closed_pos ;
  step_size : closed_pos ;
}
