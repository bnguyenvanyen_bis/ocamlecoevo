(** Simulate a Markovian jump process
    by drawing an heterogeneous poissonian number of events on fixed timesteps.
    This is again an exact simulation, and the algorithm is very similar
    to Gill_acrej, but instead of drawing events one after another
    with exponential variables,
    they are clumped together through a Poisson variable.
    This can give an appreciable speedup but doesn't reduce the complexity. *)

open Ctmjp__Sig

type algparam = epar_algparam

val specl : epar_algparam specs

val integrate :
  ?apar:epar_algparam ->
  ('a, 's, 't) rng_rate_modif ->
  rng ->
  's U.anypos ->
  'a ->
    ('t U.pos * 'a)
