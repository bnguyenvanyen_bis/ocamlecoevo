(** Use the internal time algorithm in the PRM setting :
  * For each event, we are given a rate, a modif, and a set of points.
  * For each point, its time is an internal time, that passes at [rate]
  * times the real time. *)

open Sig


module type POINT =
  sig
    type t
    type color

    module Color : (COLOR with type t = color)
    module Colors : (BatMap.S with type key = color)

    (** there is a zero point for every color *)
    val zero : color -> t

    (** a point has a color *)
    val color : t -> color

    (** a point has a time *)
    val time : t -> 'a U.pos

    (** a point's time can be changed *)
    val with_time : 'a U.anypos -> t -> t

    (** points can be added *)
    val add : t -> t -> t

    (** [volume ~vector] is the volume of the rectangular cuboid
     *  with the origin in one corner, and [vector] in the other. *)
    val volume :
      vector : t ->
        'a U.pos

    (** [inside ~origin ~vector pt] is [true] if [pt]
     *  is in the rectangular cuboid formed by taking [origin] as one corner,
     *  and [add origin vector] as the other. *)
    val inside :
      origin : t ->
      vector : t ->
      t ->
        bool

    (** [lower pt pt'] is the lower (left) corner of the smallest cuboid
     *  containing both [pt] and [pt']. *)
    val lower : t -> t -> t

    (** [upper pt pt'] is the upper (right) corner of the smallest cuboid
     *  containing both [pt] and [pt']. *)
    val upper : t -> t -> t

    (** a point can be randomly drawn,
     *  with the given color and number,
     *  and time and value from the volume bounded by [origin] and [vector].
     *  If [k] is not given, it should be 0. *)
    val rand_point :
      rng : U.rng ->
      color ->
      origin : t ->
      vector : t ->
        t

    val columns : string list

    val extract : t -> string -> string

    (** a point can be represented as a string list. *)
    val line : t -> string list

    (** a point can be read from a CSV row. *)
    val read : Csv.Row.t -> t option

    (** points are ordered *)
    val compare : t -> t -> int
  end


type 'a point = {
  c : 'a ;
  t : U.closed_pos ;
}


module Point (Color : COLOR) : (POINT with type color = Color.t
                                       and type t = Color.t point) =
  struct
    type color = Color.t

    type t = color point

    module Color = Color
    module Colors = BatMap.Make (Color)

    let zero c = { c ; t = F.zero }

    let color pt = pt.c

    let time pt = F.Pos.narrow pt.t

    let with_time t pt =
      { pt with t = F.Pos.close t }

    let add pt pt' =
      if not (Color.equal (color pt) (color pt')) then
        invalid_arg "different colors"
      else
        let c = color pt in
        let t = F.Pos.add (time pt) (time pt') in
        { c ; t }

    let rand_point ~rng c ~origin ~vector =
      let t = F.Pos.add origin.t (U.rand_float ~rng vector.t) in
      { c ; t }

    let volume ~vector =
      (* we don't count the index in color *)
      time vector

    let inside ~origin ~vector pt =
      let dest = add origin vector in
      F.Op.(
         (time origin <= time pt)
      && (time pt < time dest)
      )

    let lower pt pt' =
      if not (Color.equal (color pt) (color pt')) then
        invalid_arg "Point.lower : different colors" ;
      let c = color pt in
      let t = F.Pos.min (time pt) (time pt') in
      { c ; t }

    let upper pt pt' =
      if not (Color.equal (color pt) (color pt')) then
        invalid_arg "Point.upper : different colors" ;
      let c = color pt in
      let t = F.Pos.max (time pt) (time pt') in
      { c ; t }

    let columns = [ "c" ; "t" ]

    let extract { c ; t } =
      function
      | "c" ->
          Color.to_string c
      | "t" ->
          F.to_string t
      | _ ->
          invalid_arg "unrecognized column"

      let line pt =
        L.map (extract pt) columns

      let read row =
        let c_o = U.Csv.Row.find_conv Color.of_string row "c" in
        let t_o = U.Csv.Row.find_conv F.Pos.of_string row "t" in
        U.Option.map2 (fun c t -> { c ; t }) c_o t_o

      let compare pt pt' =
        let c = F.compare (time pt) (time pt') in
        if c = 0 then
          Color.compare (color pt) (color pt')
        else
          c
  end


module type EVM =
  sig
    type ('a, 's, 't) t
      constraint 's = 's F.anypos
      constraint 't = 't F.pos

    type color
    type point

    module Color : (COLOR with type t = color)
    module Colors : (BatMap.S with type key = color)
    module Point : (POINT with type t = point
                           and type color = color)

    type ('a, 's, 't) elt =
        ('s U.anypos -> 'a -> 't U.pos)
      * ('s U.anypos -> 'a -> Point.t -> ('a, 's, 't) t -> 'a * ('a, 's, 't) t)

    val any : _ t -> color

    val find :
      color ->
      ('a, 's, 't) t ->
        ('a, 's, 't) elt
    
    val map :
      (color -> ('a, 's, 't) elt -> 'b) ->
      ('a, 's, 't) t ->
        'b Colors.t
  end



module Make (Evm : EVM) =
  struct
    (* order points by time *)
    module Point = Evm.Point
    module Points = BatSet.Make (Evm.Point)

    module Cm = Evm.Colors

    (* For now *)
    type t = Points.t Cm.t

    (* The work of creating / changing points is for now left to the user *)

    (* FIXME if we reach a point in simulation where an event has no
     * points anymore, we could add points.
     * For now we raise since it's simpler. *)

    let simulate_until ~(output : ('a,'b,'t) output) evm x0 prm tf =
      let color_any = Evm.any evm in
      let rec f t evm prm x =
        (* output *)
        output.out t x ;
        (* we ignore colors in prm that don't have an event in evm *)
        let dtimes = Evm.map (fun c (rate, modif) ->
            (* find associated points *)
            let points = Cm.find c prm in
            (* compute rate *)
            let r = rate t x in
            (* point with smallest internal time *)
            match Points.min_elt points with
            | pt ->
                (* time before this event would happen *)
                let dt = F.Pos.div (Point.time pt) r in
                (dt, r, modif, pt, points)
            | exception Not_found ->
                (* no more points remaining for at least one color ->
                 * raise *)
                invalid_arg "Exhausted points to simulate with"
          ) evm
        in
        (* Find the event that will happen next *)
        let zero =
          F.infinity,
          (fun _ x _ evm' -> (x, evm')),
          (Point.zero color_any)
        in
        let next_dt, next_modif, next_pt =
          Cm.fold (fun _ (dt', _, modif', pt', _) (dt, modif, pt) ->
            if F.Op.(dt < dt') then
              (dt, modif, pt)
            else if F.Op.(dt > dt') then
              (dt', modif', pt')
            else (* equal *)
              (* FIXME Not sure what to do here *)
              failwith "not handled"
          ) dtimes zero
        in
        (* Make the event happen *)
        let prm' = Cm.map (fun (_, r, _, _, pts) ->
            let ds = F.Pos.mul r next_dt in
            Points.filter_map (fun pt ->
              let s' =
                pt
                |> Point.time
                   (* if we handled the comparisons badly this might raise *)
                |> (fun s -> F.positive_part F.Op.(s - ds))
              in
              (* s' might be exactly 0. in which case we remove the point *)
              if F.Op.(s' = F.zero) then
                None
              else
                Some (Point.with_time s' pt)
            ) pts
          ) dtimes
        in
        let t' = F.Pos.add t next_dt in
        (* when the point happens, its internal time is zero *)
        let pt' = Point.with_time F.zero next_pt in
        let x', evm' = next_modif t' x pt' evm in
        if F.Op.(t' > tf) then
          tf, x
        else
          f t' evm' prm' x'
      in
      let t0 = F.zero in
      output.start t0 x0 ;
      let tf, xf = f t0 evm prm x0 in
      output.return tf xf
  end
