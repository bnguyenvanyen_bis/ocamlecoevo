open Ctmjp__Sig


let ounit _ _ = ()


(* algorithm is :
 * (the time slices should be relatively small)
 * For each t slice,
 * - for each c slice
 *   - compute the rate for that color at the left boundary of the t slice
 *   - reveal up to the rate
 *   - add up all the counts below the last slice
 *     (all those points are below the rate)
 *   - for the last slice, count the points below the rate
 *   - remember the total count of events
 * - once you've gone through all colors,
 *   do all the modifications *)


module Make (Pt : POINT) =
  struct
    module P = Prm.Make (Prm.Compare_color (Pt))
    module Cm = Pt.Colors

    let simulate_until
      ~(output : ('b,'c,'t) output)
      (evm : 'a P.map)
      x0 prm =
      (* ~~ *)
      (* let total_points = ref 0 in *)
      (* let accepted_points = ref 0 in *)
      let t0, tf = P.time_range prm in
      (* for each color and rate_modif of evm *)
      let f tsl x color (rate, modif) modif_acc =
        let t, _ = P.Tsl.range tsl in
        match P.Tsl.find tsl color with
        | csl ->
            (* compute the rate *)
            let r = rate t x in
            let m = modif () t in
            (* FIXME or maybe rather check with umax what we should be doing,
             * could avoid a call to hide_downto *)
            (* hide down to under the rate *)
            P.hide_downto tsl color r ;
            (* then reveal up to the rate :
             * now we know that precisely the right slices
             * are revealed, that is, the last revealed slice contains r *)
            P.reveal_graft_upto tsl color r ;
            (* count sure points then
             * count remaining points in last u slice *)
            let count = P.Csl.fold_rvl_slices (fun usl n ->
              let _, umax = P.UKsl.range usl in
              let count =
                if F.Op.(umax <= r) then begin
                  let c = P.UKsl.count usl in
                  (* total_points := !total_points + (I.to_int c) ; *)
                  (* accepted_points := !accepted_points + (I.to_int c) ; *)
                  c
                end else begin (* last slice *)
                  (* since the points are compared by u, 
                   * we can use split instead of checking point by point.
                   * For now we do both and check that
                   * both counts are the same *)
                  let module Ps = P.Ps in
                  let points = P.UKsl.points usl in
                  (* FIXME points might be empty ! *)
                  let c : 'i U.posint =
                    match Ps.any points with
                    | any ->
                        let pt_at_r =
                          any
                          |> Pt.with_value r
                          |> Pt.with_time F.infinity
                        in
                        let under, _over = Ps.split_le pt_at_r points in
                        I.of_int_unsafe (Ps.cardinal under)
                    | exception Not_found ->
                        I.zero
                  in c
                end
              in I.Pos.add n count
            ) csl I.zero
            in
            (* FIXME we'd prefer a function
             * that directly does k modifications*)
            U.int_fold ~f:(fun m' _ ->
              (fun x' -> m (m' x'))
            ) ~x:modif_acc ~n:(I.to_int count)
        | exception Not_found ->
            (* nothing happens *)
            Printf.eprintf "color %s not found at %f\n"
            (Pt.Color.to_string color)
            (F.to_float t) ;
            modif_acc
      in
      (* for each time slice *)
      let g tsl x =
        let t, _ = P.Tsl.range tsl in
        output.out t x ;
        let modif = Cm.fold (f tsl x) evm (fun x -> x) in
        modif x
      in
      output.start t0 x0 ;
      let xf = P.fold_slices g prm x0 in
      (* Printf.eprintf "Accepted points : %i out of %i\n%!"
      !accepted_points !total_points ; *)
      output.return tf xf
  end
