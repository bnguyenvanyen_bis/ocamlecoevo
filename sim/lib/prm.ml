(** Poisson Random Measures *)

open Sig
module S = BatSet
module M = BatMap


exception Ungraftable


type closed_pos = F.closed_pos F.t
type closed_posint = I.anypos I.t

(* let incr : U.minpos = F.of_float_unsafe 1.5 *)
let incr : closed_pos = F.of_float_unsafe 0.5

let incrp1 = F.Pos.add F.one incr

let next_du du =
  F.Pos.mul incr du

let next_total_du du =
  F.Pos.mul incrp1 du

(* assume we have the right, but risky (might be the first slice *)
let prev_total_du du =
  F.Pos.div du incrp1


type 'a point = {
  c : 'a ;
  k : int ;
  t : closed_pos ;
  u : closed_pos ;
}


module Point (Color : COLOR) :
  (POINT with type color = Color.t
          and type t = Color.t point) =
  struct
    type color = Color.t

    type t = color point

    module Color = Color
    module Colors = M.Make (Color)

    let zero c = { c ; k = 0 ; t = F.zero ; u = F.zero }

    let color pt = pt.c

    let number pt = pt.k

    let time pt = F.Pos.narrow pt.t

    let value pt = F.Pos.narrow pt.u

    let with_number k pt = { pt with k = k }

    let with_time t pt = { pt with t = (F.Pos.close t) }

    let with_value u pt = { pt with u = (F.Pos.close u) }

    (* only possible if they have the same color :
     * should we make color a richer type ? *)
    let add pt pt' =
      if Color.equal (color pt) (color pt') then
        let c = color pt in
        let k = pt.k + pt'.k in
        let t = F.Pos.close (F.Pos.add pt.t pt'.t) in
        let u = F.Pos.close (F.Pos.add pt.u pt'.u) in
        { c ; k ; t ; u }
      else
        invalid_arg "Point.add : colors are different"

    let rand_point ~rng ?(k=0) c ~origin ~vector =
      let t = F.Pos.add origin.t (U.rand_float ~rng vector.t) in
      let u = F.Pos.add origin.u (U.rand_float ~rng vector.u) in
      { c ; k ; t ; u }

    let volume ~vector =
      F.Pos.mul
        (F.Pos.of_float (float vector.k))
        (F.Pos.mul vector.t vector.u)

    let inside ~origin ~vector pt =
      let dest = add origin vector in
      if not (Color.equal (color origin) (color pt)) then
        invalid_arg "Point.inside : colors are different" ;
         (origin.k <= pt.k)
      && (pt.k < dest.k)
      && F.Op.(
           (origin.t <= pt.t)
        && (origin.u <= pt.u)
        && (pt.t < dest.t)
        && (pt.u < dest.u)
      )

    let lower pt pt' =
      if not (Color.equal (color pt) (color pt')) then
        invalid_arg "Point.lower : different colors" ;
      let c = color pt in
      let k = min (number pt) (number pt') in
      let t = F.Pos.min (time pt) (time pt') in
      let u = F.Pos.min (value pt) (value pt') in
      { c ; k ; t ; u }

    let upper pt pt' =
      if not (Color.equal (color pt) (color pt')) then
        invalid_arg "Point.upper : different colors" ;
      let c = color pt in
      let k = max (number pt) (number pt') in
      let t = F.Pos.max (time pt) (time pt') in
      let u = F.Pos.max (value pt) (value pt') in
      { c ; k ; t ; u }

    let columns = [ "c" ; "k" ; "t" ; "u" ]

    let extract { c ; k ; t ; u } =
      function
      | "c" ->
          Color.to_string c
      | "k" ->
          string_of_int k
      | "t" ->
          F.to_string t
      | "u" ->
          F.to_string u
      | _ ->
          invalid_arg "unrecognized column"

    let line pt =
      L.map (extract pt) columns

    let read row =
      let int_of = U.Csv.conv_opt int_of_string in
      let float_of = U.Csv.conv_opt (fun s ->
        F.Pos.of_float (float_of_string s)
      )
      in
      let c_o = U.Csv.Row.find_conv Color.of_string row "c" in
      let k_o = U.Csv.Row.find_conv int_of row "k" in
      let t_o = U.Csv.Row.find_conv float_of row "t" in
      let u_o = U.Csv.Row.find_conv float_of row "u" in
      match c_o, k_o, t_o, u_o with
      | Some c, Some k, Some t, Some u ->
          Some { c ; k ; t ; u }
      | _ ->
          None
  end


let subdivide dx k =
  F.Pos.div (F.Pos.close dx) (I.Pos.to_float k)

let substart x subdx i =
  F.Pos.Op.((F.Pos.close x) + (I.Pos.to_float i) * (F.Pos.close subdx))


let bernoulli pt p =
  F.Op.(pt.u <= p)


(* FIXME same argument about u *)
(* copies Util.Rand.rand_cumul_choose a lot *)
let cumul_choose pt ?tot csp_l =
  let tot =
    match tot with
    | None ->
        F.one
    | Some x ->
        x
  in
  let u = F.Pos.div pt.u tot in
  let rec f = function
    | [] ->
        invalid_arg "Sim.Prm.cumul_choose: total too small"
    | (c, sp) :: tl ->
        (match F.Op.(u <= sp) with
         | true -> c
         | false -> f tl)
  in
  match csp_l with
  | [] ->
      invalid_arg "Empty"
  | _ ->
      f csp_l


module Im = M.Int


module Is = S.Int


module F_Comparable =
  struct
    type t = closed_pos
    let compare x x' = F.compare_approx x x'
  end


module Fm = M.Make (F_Comparable)


module Fs = S.Make (F_Comparable)


module FIm = M.Make (struct
  type t = closed_pos * int
  let compare (u, (k : int)) (u', (k' : int)) =
    let c = F.compare u u' in
    if c = 0 then
      BatInt.compare k k'
    else
      c
  end)




module Compare_time (P : POINT) :
  (COMPARABLE_POINT with type t = P.t
                     and type color = P.color
                     and module Colors = P.Colors) =
  struct
    include P

    let compare pt pt' =
      let c = F.compare (time pt) (time pt') in
      if c = 0 then
        let c' = Color.compare (color pt) (color pt') in
        if c' = 0 then
          F.compare (value pt) (value pt')
        else
          c'
      else
        c
  end


module Compare_color (P : POINT) :
(COMPARABLE_POINT with type t = P.t
                   and type color = P.color
                   and module Colors = P.Colors) =
  struct
    include P

    let compare pt pt' =
      let c = Color.compare (color pt) (color pt') in
      if c = 0 then
        let c' = F.compare (value pt) (value pt') in
        if c' = 0 then
          F.compare (time pt) (time pt')
        else
          c'
      else
        c
  end


module Make (Point : COMPARABLE_POINT) :
  (PRM with type point = Point.t
        and type color = Point.color
        and type 'a map = 'a Point.Colors.t
        and module Cm = Point.Colors) =
  struct
    module Pre = U.Precompute ( )

    type point = Point.t
    type color = Point.color

    module Point = Point

    module Cs = S.Make (Point.Color)

    module Cm = Point.Colors

    type 'a map = 'a Cm.t

    module Ps = Setcard.Make (Point)

    type bounds = {
      origin : Point.t ;
      vector : Point.t ;
    }

    let u0 { origin ; _ } =
      F.Pos.narrow (Point.value origin)

    let uf { origin ; vector } =
      F.Pos.add (Point.value origin) (Point.value vector)

    let volume { vector ; _ } =
      Point.volume ~vector

    (* assume we have the right to cut *)
    let cut_u ({ origin ; vector } as bds : bounds) =
      let du = Point.value vector in
      let left_du = prev_total_du du in
      let right_du = next_du left_du in
      let right_u0 = F.Pos.add (Point.value origin) left_du in
      ({ bds with vector = Point.with_value left_du vector },
       { vector = Point.with_value right_du vector ;
         origin = Point.with_value right_u0 origin })

    (* fold over u-cut bounds n times *)
    let bounds_fold n f x0 bounds =
      let leftmost_bounds, x' = U.int_fold ~f:(fun (bds, x) _ ->
          let lbds, rbds = cut_u bds in
          let x' = f rbds x in
          (lbds, x')
        ) ~x:(bounds, x0) ~n:(n - 1)
      in f leftmost_bounds x'

    (*
    let logd_poisson_process { vector ; _ } count =
      let v = Point.volume ~vector in
      count
      |> I.of_int_unsafe
      |> Pre.logd_poisson_process v
      |> F.to_float
    *)

    let any_at t pts =
      Point.with_time t (Ps.any pts)

    let split_le_at t points =
      match any_at t points with
      | pt_at_t ->
          Ps.split_le pt_at_t points
      | exception Not_found ->
          (* empty *)
          Ps.empty, Ps.empty

    let points_after_t ?t bpoints =
      match t with
      | None ->
          bpoints
      | Some t ->
          let _, strictly_greater = split_le_at t bpoints in
          strictly_greater


    (* Random functions *)
    let rngm ~rng colors =
      L.fold_left (fun m c ->
        Cm.add c (U.rand_rng rng) m
      ) Cm.empty colors

    let rng_of_color rngm c =
      Cm.find c rngm

    let rand_point_from ~rng points =
      let n = I.of_int_unsafe (Ps.cardinal points) in
      let k = I.to_int (U.rand_int ~rng n) in
      Ps.at_rank_exn k points

    let rand_points ~rng c { origin ; vector } =
      let v = Point.volume ~vector in
      let k = I.to_int (U.rand_poisson ~rng v) in
      Ps.of_list (L.init k (fun _ ->
        Point.rand_point ~rng c ~origin ~vector)
      )

    module UKsl =
      struct
        type t = {
          color : color ;
          number : int ;
          bounds : bounds ;
          points : Ps.t ;
        }

        let color uksl =
          uksl.color

        let number uksl =
          uksl.number

        let bounds uksl =
          uksl.bounds

        let points uksl =
          uksl.points

        let count uksl : 'a U.posint =
          I.Pos.of_int (Ps.cardinal (points uksl))

        let volume uksl =
          volume (bounds uksl)

        let density uksl =
          F.Pos.div (I.Pos.to_float (count uksl)) (volume uksl)

        let logp uksl =
          (* Pre.logd_poisson_process (volume uksl) (count uksl) *)
          Pre.logp_poisson (volume uksl) (I.to_int (count uksl))

        let range uksl =
          let bds = uksl.bounds in
          (u0 bds, uf bds)

        let key uksl =
          let _, uf = range uksl in
          (uf, number uksl)

        (* now, logp should be handled at call site *)
        let replace_points points uksl =
          { uksl with points }

        let add_points pts uksl =
          let cnt = Ps.cardinal pts in
          let count = Ps.cardinal uksl.points in
          let points' = Ps.union uksl.points pts in
          let count' = Ps.cardinal points' in
          (* assert all points are new *)
          assert (count + cnt = count') ;
          replace_points points' uksl

        let remove_points pts uksl =
          let cnt = Ps.cardinal pts in
          let count = Ps.cardinal uksl.points in
          let points' = Ps.diff uksl.points pts in
          let count' = Ps.cardinal points' in
          (* assert all removed points were present *)
          assert (count = cnt + count') ;
          replace_points points' uksl

        let rand_draw ~rng color bounds : t =
          let number =
            assert (Point.number bounds.vector = 1) ;
            Point.number bounds.origin
          in
          let points = rand_points ~rng color bounds in
          (* let count = I.Pos.of_int (Ps.cardinal points) in *)
          { color ; number ; bounds ; points (* ; count *) }

        let create color bounds points : t =
          let number =
            assert (Point.number bounds.vector = 1) ;
            Point.number bounds.origin
          in
          (* let count = I.Pos.of_int (Ps.cardinal points) in *)
          { color ; number ; bounds ; points (* ; count *) }

        let rand_move ~rng dk uksl =
          let { origin ; vector } = uksl.bounds in
          if dk > 0 then
            let g ~rng =
              let points' = Ps.of_list (L.init dk (fun _ ->
                  Point.rand_point ~rng uksl.color ~origin ~vector
                ))
              in
              match add_points points' uksl with
              | uksl' ->
                  Some uksl'
              | exception Assert_failure _ ->
                  (* point was already present *)
                  Printf.eprintf "Prm.rand_move: on adding %i points,"  dk ;
                  Printf.eprintf "points already present, retrying\n" ;
                  None
            in U.insist ~rng g
          else if dk < 0 then
            let points' =
              try
                U.int_fold ~f:(fun pts' _ ->
                    Ps.remove (rand_point_from ~rng pts') pts'
                  ) ~x:uksl.points ~n:(~- dk)
              with Invalid_argument _ ->
                invalid_arg
                "rand_move: can't remove more points than there are"
            in replace_points points' uksl
          else (* dk = 0 *)
            uksl

        let logp_move f uksl uksl' =
          let k = count uksl in
          let k' = count uksl' in
          let dk = I.Op.(k' - k) in
          let logp_move =
            if I.Op.(dk > I.zero) then
              (* dk points were added uniformly *)
              let v = volume uksl in
              let logd = F.neg (F.Pos.log v) in
              F.Op.((I.to_float dk) * logd)
            else if I.Op.(dk < I.zero) then
              (* dk points were chosen among k and removed *)
              U.logp_n_choice_k ~choose:(I.to_int (I.neg dk))
                                ~among:(I.to_int k)
            else (* dk = 0 *)
              F.zero
          in F.add (f uksl dk) logp_move

        let map_conserve_bounds f uksl =
          let uksl' = f uksl in
          assert (bounds uksl' = bounds uksl) ;
          uksl'
      end

    module Csl =
      struct
        exception Already_hidden
        exception Already_revealed
        exception Not_all_revealed

        (* uk_slices indexed by their upper u bound *)
        type t =
          | Immutable of {
              color : color ;
              umax : closed_pos ;
              points : Ps.t ;
              (* count : U.anyposint ; *)
              (* volume : U.closed_pos ; *)
              uk_slices : UKsl.t FIm.t ;
              us : Fs.t ;
              ks : Is.t ;
            }
          | Ungraftable of {
              color : color ;
              (* total maximum u value *)
              umax_total : closed_pos ;
              (* revealed maximum u value *)
              mutable umax_revealed : closed_pos ;
              (* revealed points *)
              mutable points : Ps.t ;
              (* mutable count : U.anyposint ; *)
              (* mutable volume : U.closed_pos ; *)
              uk_slices : UKsl.t FIm.t ;
              mutable revealed_us : Fs.t ;
              mutable hidden_us : Fs.t ;
              mutable revealed_ks : Is.t ;
              mutable hidden_ks : Is.t ;
            }
          | Graftable of {
              color : color ;
              (* rng to graft new points *)
              rng : U.rng ;
              (* total maximum u value *)
              mutable umax_total : closed_pos ;
              (* revealed maximum u value *)
              mutable umax_revealed : closed_pos ;
              (* revealed points *)
              mutable points : Ps.t ;
              (* mutable count : U.anyposint ; *)
              (* mutable volume : U.closed_pos ; *)
              mutable uk_slices : UKsl.t FIm.t ;
              mutable revealed_us : Fs.t ;
              mutable hidden_us : Fs.t ;
              mutable revealed_ks : Is.t ;
              mutable hidden_ks : Is.t ;
            }


        let color =
          function
          | Immutable { color ; _ } ->
              color
          | Ungraftable { color ; _ } ->
              color
          | Graftable { color ; _ } ->
              color


        let umax_revealed =
          function
          | Immutable { umax ; _ } ->
              F.Pos.narrow umax
          | Ungraftable { umax_revealed ; _ } ->
              F.Pos.narrow umax_revealed
          | Graftable { umax_revealed ; _ } ->
              F.Pos.narrow umax_revealed

        let umax_total =
          function
          | Immutable { umax ; _ } ->
              F.Pos.narrow umax
          | Ungraftable { umax_total ; _ } ->
              F.Pos.narrow umax_total
          | Graftable { umax_total ; _ } ->
              F.Pos.narrow umax_total

        let time_range =
          let f uksls =
            (* assume valid csl : all uk_slices have the same time range *)
            let _, uksl = FIm.any uksls in
            let { origin ; vector } = UKsl.bounds uksl in
            let t0 = Point.time origin in
            let dt = Point.time vector in
            (F.Pos.narrow t0, F.Pos.narrow dt)
          in
          function
          | Immutable { uk_slices ; _ } ->
              f uk_slices
          | Ungraftable { uk_slices ; _ } ->
              f uk_slices
          | Graftable { uk_slices ; _ } ->
              f uk_slices

        let kmax =
          let max2 rks hks =
            match Is.max_elt rks with
            | k ->
                begin match Is.max_elt hks with
                | k' ->
                    max k k'
                | exception Not_found ->
                    k
                end
            | exception Not_found ->
                begin match Is.max_elt hks with
                | k' ->
                    k'
                | exception (Not_found as e) ->
                    (* this should never happen *)
                    raise e
                end
          in
          function
          | Immutable { ks ; _ } ->
              Is.max_elt ks (* should not raise *)
          | Ungraftable { revealed_ks ; hidden_ks ; _ } ->
              max2 revealed_ks hidden_ks
          | Graftable { revealed_ks ; hidden_ks ; _ } ->
              max2 revealed_ks hidden_ks

        let points =
          function
          | Immutable { points ; _ } ->
              points
          | Ungraftable { points ; _ } ->
              points
          | Graftable { points ; _ } ->
              points

        let slices =
          function
          | Immutable { uk_slices ; _ } ->
              uk_slices
          | Ungraftable { uk_slices ; _ } ->
              uk_slices
          | Graftable { uk_slices ; _ } ->
              uk_slices

        let revealed_us =
          function
          | Immutable { us ; _ } ->
              us
          | Ungraftable { revealed_us ; _ } ->
              revealed_us
          | Graftable { revealed_us ; _ } ->
              revealed_us

        let revealed_ks =
          function
          | Immutable { ks ; _ } ->
              ks
          | Ungraftable { revealed_ks ; _ } ->
              revealed_ks
          | Graftable { revealed_ks ; _ } ->
              revealed_ks

        let hidden_us =
          function
          | Immutable _ ->
              Fs.empty
          | Ungraftable { hidden_us ; _ } ->
              hidden_us
          | Graftable { hidden_us ; _ } ->
              hidden_us

        let hidden_ks =
          function
          | Immutable _ ->
              Is.empty
          | Ungraftable { hidden_ks ; _ } ->
              hidden_ks
          | Graftable { hidden_ks ; _ } ->
              hidden_ks

        let copy =
          function
          | Immutable r ->
              Immutable { r with color = r.color }
          | Ungraftable r ->
              Ungraftable { r with color = r.color }
          | Graftable r ->
              Graftable { r with color = r.color }

        let count csl =
          I.of_int_unsafe (Ps.cardinal (points csl))

        let bounds csl =
          let umax = umax_total csl in
          let kmax = kmax csl in
          let _, uksl = FIm.any (slices csl) in
          let bds = UKsl.bounds uksl in
          let origin =
            bds.origin
            |> Point.with_number 0
            |> Point.with_value F.zero
          in
          let vector =
            bds.vector
            |> Point.with_number (kmax + 1)
            |> Point.with_value umax
          in { origin ; vector }

        let compare_bounds csl =
          let advertised_umax = umax_revealed csl in
          let actual_umax = Fs.max_elt (revealed_us csl) in
          assert F.Op.(advertised_umax - actual_umax <= (F.of_float 1e-8))

        let u_boundaries csl =
          csl
          |> revealed_us
          |> Fs.to_list
          |> L.map F.Pos.narrow
          |> L.cons F.zero

        let add_points =
          fun pts ->
          function
          | Immutable _ ->
              invalid_arg "Immutable c slice"
          | Ungraftable ({ points ; _ } as r) ->
              r.points <- Ps.union points pts
          | Graftable ({ points ; _ } as r) ->
              r.points <- Ps.union points pts

        let remove_points =
          fun pts ->
          function
          | Immutable _ ->
              invalid_arg "Immutable c slice"
          | Ungraftable ({ points ; _ } as r) ->
              r.points <- Ps.diff points pts
          | Graftable ({ points ; _ } as r) ->
              r.points <- Ps.diff points pts

        (*
        let add_volume =
          fun volume ->
          function
          | Immutable _ ->
              invalid_arg "Immutable c slice"
          | Ungraftable r ->
              (* r.count <- I.Pos.add r.count count ;*)
              r.volume <- F.Pos.add r.volume volume
          | Graftable r ->
              (* r.count <- I.Pos.add r.count count ; *)
              r.volume <- F.Pos.add r.volume volume

        let subtract_volume =
          fun volume ->
          function
          | Immutable _ ->
              invalid_arg "Immutable c slice"
          | Ungraftable r ->
              (* r.count <- I.Pos.of_anyint (I.sub r.count count) ; *)
              r.volume <- F.Pos.of_anyfloat (F.sub r.volume volume)
          | Graftable r ->
              (* r.count <- I.Pos.of_anyint (I.sub r.count count) ; *)
              r.volume <- F.Pos.of_anyfloat (F.sub r.volume volume)
        *)


        let add_slice f k bounds (uksls, us, ks) =
          (* bounds is what it should be from the body of the double fold *)
          let u = uf bounds in
          let key = (u, k) in
          let uksl = f bounds in
          let uksls' = FIm.add key uksl uksls in
          (* FIXME we actually need to do this only once *)
          let us' = Fs.add u us in
          let ks' = Is.add k ks in
          (uksls', us', ks')

        let union_points slices = FIm.fold (fun _ uksl pts ->
            Ps.union pts (UKsl.points uksl)
          ) slices Ps.empty

        (*
        let sum_logps slices = FIm.fold (fun _ uksl logp ->
            logp +. (UKsl.logp uksl)
          ) slices 0.
        *)

        let rand_draw ~rng color bounds nuslices =
          (* a new rng is needed for each k slice to ensure reproducibility,
           * when the order of grafts changes *)
          let rng = U.rand_rng rng in
          let draw_uksl bds =
            UKsl.rand_draw ~rng color bds
          in
          assert (Point.number bounds.origin = 0) ;
          let uk_slices, us, ks = U.int_fold
            ~f:(fun uksluks j ->
              let k = pred j in
              let k_bounds = {
                origin = Point.with_number k bounds.origin ;
                vector = Point.with_number 1 bounds.vector ;
              }
              in
              bounds_fold (I.to_int nuslices) (add_slice draw_uksl k) uksluks k_bounds
            )
            ~x:(FIm.empty, Fs.empty, Is.empty)
            ~n:(Point.number bounds.vector)
          in
          let points = union_points uk_slices in
          (* let count = I.Pos.of_int (Ps.cardinal points) in *)
          (* let volume = volume bounds in *)
          let umax = uf bounds in
          Graftable {
            color ;
            rng ;
            umax_total = umax ;
            umax_revealed = umax ;
            points ;
            (* count ; *)
            (* volume ; *)
            uk_slices ;
            revealed_us = us ;
            hidden_us = Fs.empty ;
            revealed_ks = ks ;
            hidden_ks = Is.empty ;
          }

        let create color bounds nuslices points =
          let create_uksl pts bds =
            UKsl.create color bds pts
          in
          let (uk_slices, us, ks), rem_pts = U.int_fold
            ~f:(fun (uksluks, pts) j ->
              let k = j - 1 in
              let k_bounds = {
                origin = Point.with_number k bounds.origin ;
                vector = Point.with_number 1 bounds.vector ;
              }
              in
              let f_fold ({ origin ; vector } as bds) (uksluks, pts) =
                let pts_in, pts_out = Ps.partition (fun pt ->
                    Point.inside ~origin ~vector pt
                  ) pts
                in
                let uksluks' = add_slice (create_uksl pts_in) k bds uksluks in
                (uksluks', pts_out)
              in
              bounds_fold (I.to_int nuslices) f_fold (uksluks, pts) k_bounds
            )
            ~x:((FIm.empty, Fs.empty, Is.empty), points)
            ~n:(Point.number bounds.vector)
          in
          assert (rem_pts = Ps.empty) ;
          (* let count = I.Pos.of_int (Ps.cardinal points) in *)
          (* let volume = volume bounds in *)
          let umax = uf bounds in
          Ungraftable {
            color ;
            umax_total = umax ;
            umax_revealed = umax ;
            points ;
            (* count ; *)
            (* volume ; *)
            uk_slices ;
            revealed_us = us ;
            hidden_us = Fs.empty ;
            revealed_ks = ks ;
            hidden_ks = Is.empty ;
          }

        let find csl (u, k) =
          let u = F.Pos.close u in
          FIm.find (u, k) (slices csl)

        let propagate_hide csl points =
          remove_points points csl

        let propagate_reveal csl points =
          add_points points csl

        let validate_k csl k =
          match csl with
          | Immutable { ks ; _ } ->
              Is.mem k ks
          | Ungraftable { hidden_ks ; revealed_ks ; _ } ->
              (Is.mem k revealed_ks) || (Is.mem k hidden_ks)
          | Graftable { hidden_ks ; revealed_ks ; _ } ->
              (Is.mem k revealed_ks) || (Is.mem k hidden_ks)

        let hide_k csl k =
          (* set k to hidden
           * raises if k not in numbers
           * does nothing if k is hidden *)
          let propagate () =
            (* do the changes to points and logp *)
            Fs.fold (fun u points' ->
              let uksl = find csl (u, k) in
              let points = UKsl.points uksl in
              propagate_hide csl points ;
              Ps.union points points'
            ) (revealed_us csl) Ps.empty
          in
          if not (validate_k csl k) then
            raise Not_found ;
          if Is.mem k (revealed_ks csl) then begin
            begin match csl with
            | Immutable _ ->
                invalid_arg "Immutable c slice"
            | Ungraftable r ->
                r.revealed_ks <- Is.remove k r.revealed_ks ;
                r.hidden_ks <- Is.add k r.hidden_ks
            | Graftable r ->
                r.revealed_ks <- Is.remove k r.revealed_ks ;
                r.hidden_ks <- Is.add k r.hidden_ks
            end ;
            propagate ()
          end else
            raise Already_hidden
            (* (Ps.empty, 0.) *)

        (* might raise Not_found if all slices are hidden,
         * but that's ok since that should never happen.
         * If there is only one remaining revealed slice,
         * returns None *)
        let pop_penultimate_revealed_u csl =
          let u, revealed_us' = Fs.pop_max (revealed_us csl) in
          if Fs.equal revealed_us' Fs.empty then
            None
          else
            Some (u, revealed_us')

        let hide_u csl =
          let propagate u =
            (* do the changes to points, count, volume *)
            Is.fold (fun k points' ->
              let uksl = find csl (u, k) in
              let points = UKsl.points uksl in
              propagate_hide csl points ;
              Ps.union points points'
            ) (revealed_ks csl) Ps.empty
          in
          match pop_penultimate_revealed_u csl with
          | None ->
              raise Already_hidden
              (* (Ps.empty, 0.) *)
          | Some (u, revealed_us') ->
              begin match csl with
              | Immutable _ ->
                  invalid_arg "Immutable c slice"
              | Ungraftable r ->
                  r.umax_revealed <- Fs.max_elt revealed_us' ;
                  r.revealed_us <- revealed_us' ;
                  r.hidden_us <- Fs.add u r.hidden_us
              | Graftable r ->
                  r.umax_revealed <- Fs.max_elt revealed_us' ;
                  r.revealed_us <- revealed_us' ;
                  r.hidden_us <- Fs.add u r.hidden_us
              end ;
              propagate u
          | exception Not_found ->
              failwith "Csl.hide_u : all slices are hidden"

        let reveal_k csl k =
          let propagate () =
            Fs.fold (fun u points' ->
              let uksl = find csl (u, k) in
              let points = UKsl.points uksl in
              propagate_reveal csl points ;
              Ps.union points points'
            ) (revealed_us csl) Ps.empty
          in
          if not (validate_k csl k) then
            raise Not_found ;
          if Is.mem k (hidden_ks csl) then begin
            begin match csl with
            | Immutable _ ->
                invalid_arg "Immutable c slice"
            | Ungraftable r ->
                r.revealed_ks <- Is.add k r.revealed_ks ;
                r.hidden_ks <- Is.remove k r.hidden_ks
            | Graftable r ->
                r.revealed_ks <- Is.add k r.revealed_ks ;
                r.hidden_ks <- Is.remove k r.hidden_ks
            end ;
            propagate ()
          end else
            raise Already_revealed
            (* (Ps.empty, 0.) *)


        let pop_next_hidden_u csl =
          match Fs.pop_min (hidden_us csl) with
          | v ->
              Some v
          | exception Not_found ->
              None

        let reveal_u csl =
          let propagate u =
            (* do the changes to points and logp *)
            Is.fold (fun k points' ->
              let uksl = find csl (u, k) in
              let points = UKsl.points uksl in
              propagate_reveal csl points ;
              Ps.union points points'
            ) (revealed_ks csl) Ps.empty
          in
          match pop_next_hidden_u csl with
          | None ->
              raise Already_revealed
              (* (Ps.empty, 0.) *)
          | Some (u, hidden_us') ->
              begin match csl with
              | Immutable _ ->
                  invalid_arg "Immutable c slice"
              | Ungraftable r ->
                  r.umax_revealed <- u ;
                  r.hidden_us <- hidden_us' ;
                  r.revealed_us <- Fs.add u r.revealed_us
              | Graftable r ->
                  r.umax_revealed <- u ;
                  r.hidden_us <- hidden_us' ;
                  r.revealed_us <- Fs.add u r.revealed_us
              end ;
              propagate u

        let graft_k =
          function
          | Immutable _ | Ungraftable _ ->
              invalid_arg "Immutable color slice"
          | Graftable ({
              color ;
              rng ;
              uk_slices ;
              revealed_us ;
              hidden_us ;
              revealed_ks ;
              hidden_ks ;
              _
            } as r) as csl ->
              if not (hidden_ks = Is.empty) then
                raise Not_all_revealed ;
              let k = Is.max_elt revealed_ks + 1 in
              r.revealed_ks <- Is.add k revealed_ks ;
              let draw_add u =
                let uksl = find csl (u, k - 1) in
                let bounds = UKsl.bounds uksl in
                let bounds' = {
                  bounds with origin = Point.with_number k bounds.origin
                }
                in
                let uksl' = UKsl.rand_draw ~rng color bounds' in
                r.uk_slices <- FIm.add (u, k) uksl' uk_slices ;
                uksl'
              in
              let all_us = Fs.union hidden_us revealed_us in
              let dpts =
                Fs.fold (fun u points' ->
                    let uksl = draw_add u in
                    if Fs.mem u revealed_us then
                      let points = UKsl.points uksl in
                      propagate_reveal csl points ;
                      Ps.union points points'
                    else
                      points'
                  ) all_us Ps.empty
              in k, dpts

        let graft_u =
          function
          | Immutable _ | Ungraftable _ ->
              invalid_arg "Immutable color slice"
          | Graftable ({
              color ;
              rng ;
              umax_total ;
              revealed_us ;
              hidden_us ;
              revealed_ks ;
              hidden_ks ;
              _
            } as r) as csl ->
              if not (hidden_us = Fs.empty) then
                raise Not_all_revealed ;
              let du' = next_du umax_total in
              let umax_total' = next_total_du umax_total in
              r.umax_total <- umax_total' ;
              r.umax_revealed <- umax_total' ;
              r.revealed_us <- Fs.add umax_total' revealed_us ;
              let draw_add k =
                let uksl = find csl (umax_total, k) in
                let bounds = UKsl.bounds uksl in
                let bounds' = {
                  origin = Point.with_value umax_total bounds.origin ;
                  vector = Point.with_value du' bounds.vector ;
                }
                in
                let uksl' = UKsl.rand_draw ~rng color bounds' in
                r.uk_slices <- FIm.add (umax_total', k) uksl' r.uk_slices ;
                uksl'
              in
              let all_ks = Is.union revealed_ks hidden_ks in
              Is.fold (fun k points' ->
                  let uksl = draw_add k in
                  if Is.mem k revealed_ks then
                    let points = UKsl.points uksl in
                    propagate_reveal csl points ;
                    Ps.union points points'
                  else  (* we ignore hidden_ks for propagation *)
                    points'
                ) all_ks Ps.empty

        let revealed csl (u, k) =
             (Fs.mem u (revealed_us csl))
          && (Is.mem k (revealed_ks csl))

        let map_slices f csl =
          (* volume doesn't change, because map conserve_bounds check *)
          let slices' = FIm.map (UKsl.map_conserve_bounds f) (slices csl) in
          let rvl_slices' = FIm.filter (fun uk _ -> revealed csl uk) slices' in
          let points' = union_points rvl_slices' in
          match csl with
          | Immutable r ->
              Immutable { r with uk_slices = slices' ;
                                 points = points' ;
              }
          | Ungraftable r ->
              Ungraftable { r with uk_slices = slices' ;
                                 points = points' ;
              }
          | Graftable r ->
              Graftable { r with uk_slices = slices' ;
                                 points = points' ;
              }

        let fold_rvl_slices f csl x0 =
          Is.fold (fun k x ->
            Fs.fold (fun u x' ->
              f (find csl (u, k)) x'
            ) (revealed_us csl) x
          ) (revealed_ks csl) x0

        let volume csl =
          F.Pos.narrow (fold_rvl_slices (fun uksl v ->
            F.Pos.add v (UKsl.volume uksl)
          ) csl F.zero)

        let logp csl =
          F.Neg.narrow (fold_rvl_slices (fun uksl x ->
            F.Neg.add x (UKsl.logp uksl)
          ) csl F.zero)
      end

    module Tsl =
      struct
        type t =
          | Immutable of {
              key : closed_pos ;
              range : closed_pos * closed_pos ;
              points : Ps.t ;
              c_slices : Csl.t Cm.t ;
            }
          | Mutable of {
              key : closed_pos ;
              range : closed_pos * closed_pos ;
              mutable points : Ps.t ;
              c_slices : Csl.t Cm.t ;
            }


        let key =
          function
          | Immutable { key ; _ } ->
              F.Pos.narrow key
          | Mutable { key ; _ } ->
              F.Pos.narrow key

        let range =
          function
          | Immutable { range = (t0, dt) ; _ } ->
              (F.Pos.narrow t0, F.Pos.narrow dt)
          | Mutable { range = (t0, dt) ; _ } ->
              (F.Pos.narrow t0, F.Pos.narrow dt)

        let points =
          function
          | Immutable { points ; _ } ->
              points
          | Mutable { points ; _ } ->
              points

        let slices =
          function
          | Immutable { c_slices ; _ } ->
              c_slices
          | Mutable { c_slices ; _ } ->
              c_slices

        let count tsl =
          I.of_int_unsafe (Ps.cardinal (points tsl))

        let find tsl c =
          match tsl with
          | Immutable { c_slices ; _ } ->
              Cm.find c c_slices
          | Mutable { c_slices ; _ } ->
              Cm.find c c_slices

        let add_color csl tsl =
          let c = Csl.color csl in
          if Cm.mem c (slices tsl) then
            invalid_arg "Tsl.add_color : color already present" ;
          let points = Ps.union (Csl.points csl) (points tsl) in
          let c_slices = Cm.add c csl (slices tsl) in
          match csl, tsl with
          | Immutable _, Immutable r ->
              Immutable { r with points ; c_slices }
          | (Ungraftable _ | Graftable _), Immutable r ->
              Mutable {
                key = r.key ;
                range = r.range ;
                points ;
                c_slices ;
              }
          | _, Mutable r ->
              Mutable { r with points ; c_slices }


        let add_points pts =
          function
          | Immutable _ ->
              invalid_arg "Immutable t slice"
          | Mutable ({ points ; _ } as r) ->
              r.points <- Ps.union points pts

        let remove_points pts =
          function
          | Immutable _ ->
              invalid_arg "Immutable t slice"
          | Mutable ({ points ; _ } as r) ->
              r.points <- Ps.diff points pts

        (*
        let add_volume vol =
          function
          | Immutable _ ->
              invalid_arg "Immutable t slice"
          | Mutable r ->
              r.volume <- F.Pos.add r.volume vol

        let subtract_volume vol =
          function
          | Immutable _ ->
              invalid_arg "Immutable t slice"
          | Mutable r ->
              r.volume <- F.Pos.of_anyfloat (F.sub r.volume vol)
        *)

        let union_points slices = Cm.fold (fun _ csl pts ->
            Ps.union pts (Csl.points csl)
          ) slices Ps.empty

        (*
        let sum_volumes slices =
          F.Pos.narrow (
            Cm.fold (fun _ csl vol ->
              F.Pos.add vol (Csl.volume csl)
            ) slices F.zero
          )
        *)

        let from_slices c_slices =
          let points = union_points c_slices in
          (* let volume = sum_volumes c_slices in *)
          (* check time range *)
          let rgo = Cm.fold (fun _ csl ->
              function
              | None ->
                  Some (Csl.time_range csl)
              | (Some (t0, dt)) as rgo ->
                  let t0', dt' = Csl.time_range csl in
                  assert F.Op.((t0 = t0') && (dt = dt')) ;
                  rgo
            ) c_slices None
          in
          match rgo with
          | None ->
              invalid_arg "Tsl.from_slices : empty list"
          | Some ((t0, dt) as range) ->
              let key = F.Pos.add t0 dt in
              Mutable { key ; range ; points ; c_slices }

        let rand_choose ~rng tsl =
          let binds = Cm.bindings (slices tsl) in
          let _, csl = U.rand_unif_choose ~rng binds in
          csl

        let rand_draw ~rngm ~key (t0, dt) vectors =
          let c_slices = L.fold_left (fun csls (nuslices, vector) ->
            let c = Point.color vector in
            let rng = rng_of_color rngm c in
            let origin = Point.with_time t0 (Point.zero c) in
            let vector = Point.with_time dt vector in
            (* how can we create bounds ? *)
            let csl = Csl.rand_draw ~rng c { origin ; vector } nuslices in
            Cm.add c csl csls
          ) Cm.empty vectors
          in
          let points = union_points c_slices in
          let range = (t0, dt) in
          Mutable {
            key ;
            range ;
            points ;
            c_slices ;
          }

        let create ~key (t0, dt) vectors points =
          let rec f_c csls points vectors =
            match vectors with
            | [] ->
                assert (points =  Ps.empty) ;
                csls
            | (nuslices, vector) :: vectors' ->
                let c = Point.color vector in
                let points_in, points_out = Ps.partition (fun pt ->
                  Point.Color.equal (Point.color pt) c
                ) points
                in
                let origin = Point.with_time t0 (Point.zero c) in
                let vector = Point.with_time dt vector in
                let csl =
                  Csl.create c { origin ; vector } nuslices points_in
                in
                f_c (Cm.add c csl csls) points_out vectors'
          in
          let c_slices = f_c Cm.empty points vectors in
          let range = (t0, dt) in
          Mutable {
            key ;
            range ;
            points ;
            c_slices ;
          }

        let propagate_hide tsl points =
          remove_points points tsl

        let propagate_reveal tsl points =
          add_points points tsl

        let hide_k tsl c k =
          let points = Csl.hide_k (find tsl c) k in
          propagate_hide tsl points

        let hide_u tsl c =
          let csl = find tsl c in
          let points = Csl.hide_u csl in
          propagate_hide tsl points ;
          let hid_u = Csl.umax_revealed csl in
          hid_u

        let reveal_k tsl c k =
          let points = Csl.reveal_k (find tsl c) k in
          propagate_reveal tsl points

        let reveal_u tsl c =
          let csl = find tsl c in
          let points = Csl.reveal_u csl in
          propagate_reveal tsl points ;
          let revealed_u = Csl.umax_revealed csl in
          revealed_u

        let graft_k tsl c =
          let k, points = Csl.graft_k (find tsl c) in
          propagate_reveal tsl points ;
          k

        let graft_u tsl c =
          let csl = find tsl c in
          let points = Csl.graft_u csl in
          propagate_reveal tsl points ;
          let grafted_u = Csl.umax_revealed csl in
          grafted_u

        let with_slice csl' tsl =
          (* let's try not to use csl *)
          let color = Csl.color csl' in
          assert (Cm.mem color (slices tsl)) ;
          let slices' = Cm.add color csl' (slices tsl) in
          let points' = union_points slices' in
          match tsl with
          | Immutable r ->
              Immutable { r with c_slices = slices' ;
                                 points = points' ;
              }
          | Mutable r ->
              Mutable { r with c_slices = slices' ;
                               points = points' ;
              }

        let map_slices f tsl =
          (* FIXME should we check something on f (like conserve_bounds) *)
          let slices' = Cm.map f (slices tsl) in
          let points' = union_points slices' in
          match tsl with
          | Immutable r ->
              Immutable { r with c_slices = slices' ;
                                 points = points' ;
              }
          | Mutable r ->
              Mutable { r with c_slices = slices' ;
                               points = points' ;
              }

        let map_uk_slices f tsl c =
          let old_csl = find tsl c in
          let old_csl_points = Csl.points old_csl in
          let new_csl = Csl.map_slices f old_csl in
          let new_csl_points = Csl.points new_csl in
          let tsl' =
            let c_slices = Cm.mapi (fun color csl ->
                if Point.Color.equal c color then
                  new_csl
                else
                  Csl.copy csl
              ) (slices tsl)
            in
            let points = Ps.union
              (Ps.diff (points tsl) old_csl_points)
              new_csl_points
            in
            match tsl with
            | Immutable r ->
                Immutable { r with c_slices ; points }
            | Mutable r ->
                Mutable { r with c_slices ; points }
          in
          tsl'

        let fold_slices f tsl x0 =
          let f' _ = f in
          Cm.fold f' (slices tsl) x0

        let iter_points f tsl =
          let exception Break of closed_pos in
          let maybe_refresh pt =
            match f pt with
            | `Continue ->
                ()
            | `Refresh ->
                raise (Break (Point.time pt))
          in
          let rec g t =
            match Ps.iter maybe_refresh (points_after_t ~t (points tsl)) with
            | () ->
                ()
            | exception Break t' ->
                g t'
          in g (F.Pos.narrow F.zero)

        let fold_points
          (type a)
          (f : point -> a -> [`Continue of a | `Refresh of a])
          tsl (x0 : a) =
          let exception Break of closed_pos * a in
          let maybe_refresh pt x =
            match f pt x with
            | `Continue x'->
                x'
            | `Refresh x' ->
                raise (Break (Point.time pt, x'))
          in
          let rec g t x =
            match Ps.fold maybe_refresh (points_after_t ~t (points tsl)) x with
            | x' ->
                x'
            | exception Break (t', x') ->
                g t' x'
          in g (F.Pos.narrow F.zero) x0

        let volume tsl =
          F.Pos.narrow (fold_slices (fun csl v ->
            F.Pos.add v (Csl.volume csl)
          ) tsl F.zero)

        let logp tsl =
          F.Neg.narrow (fold_slices (fun csl x ->
            F.Neg.add x (Csl.logp csl)
          ) tsl F.zero)
      end


    (* This implementation is mutable but persistent through redraws :
     * hiding, revealing and grafting modify the c_slice
     * (and so, the t_slice and prm) in place,
     * but redrawing yields a new prm with the new slices *)
    type uk_slice = UKsl.t

    type c_slice = Csl.t

    type t_slice = Tsl.t

    type t = {
      range : closed_pos * closed_pos ;
      colors : color list ;
      (* slices of time, key = ending time *)
      t_slices : t_slice Fm.t ;
    }


    let time_range prm =
      let t0, dt = prm.range in
      (F.Pos.narrow t0, F.Pos.narrow dt)

    let colors prm =
      prm.colors

    let slices prm =
      prm.t_slices

    let t_slice_at prm t =
      Fm.find (F.Pos.close t) prm.t_slices

    (* careful : returns the t_slice containing t+ *)
    let t_slice_of prm t =
      let _, _, tsls = Fm.split (F.Pos.close t) prm.t_slices in
      let _, tsl = Fm.min_binding tsls in
      tsl

    let count prm =
      I.Pos.narrow (Fm.fold (fun _ tsl c ->
        I.Pos.add c (Tsl.count tsl)
      ) (slices prm) I.zero)

    let volume prm =
      F.Pos.narrow (Fm.fold (fun _ tsl v ->
        F.Pos.add v (Tsl.volume tsl)
      ) (slices prm) F.zero)

    let logp prm =
      (* Pre.logd_poisson_process (volume prm) (count prm) *)
      (* Pre.logp_poisson (volume prm) (I.to_int (count prm)) *)
      F.Neg.narrow (Fm.fold (fun _ tsl x ->
        F.Neg.add x (Tsl.logp tsl)
      ) (slices prm) F.zero)

    let nslices prm =
      I.Pos.of_int (Fm.cardinal (slices prm))

    let iter_slices f prm =
      let f' _ = f in
      Fm.iter f' prm.t_slices

    let fold_slices f prm x0 =
      let f' _ = f in
      Fm.fold f' (slices prm) x0

    (*
    let check_logp nu =
      let lp = logp nu in
      let dlp = deep_logp nu in
      if abs_float (lp -. dlp) > 1e-9 then begin
        Printf.eprintf "nslices %i\n%!" (I.to_int (nslices nu)) ;
        Printf.eprintf "logp %f ; deep logp %f\n%!" lp dlp ;
        iter_slices (fun tsl ->
          let lp = Tsl.logp tsl in
          let dlp = Tsl.deep_logp tsl in
          (* Printf.eprintf "t key %f\n%!" (F.to_float (Tsl.key tsl)) ; *)
          if abs_float (lp -. dlp) > 1e-9 then
            Printf.eprintf "tsl logp %f ; deep logp %f\n" lp dlp
        ) nu ;
        failwith "wrong logp"
      end
    *)

    let rand_draw ~rngm ~time_range ~ntslices ~vectors =
      let t0, dt = time_range in
      let subdt = subdivide dt ntslices in
      let colors = L.map (fun (_, pt) -> Point.color pt) vectors in
      let t_slices = U.int_fold ~f:(fun tsls ti ->
        let t0' = substart t0 subdt (I.Pos.of_int (ti - 1)) in
        let tf' = substart t0 subdt (I.Pos.of_int ti) in
        let tsl = Tsl.rand_draw ~rngm ~key:tf' (t0', subdt) vectors in
        (* key is final time, to then split properly *)
        Fm.add tf' tsl tsls
      ) ~x:Fm.empty ~n:(I.to_int ntslices)
      in
      let range = (F.Pos.close t0, F.Pos.close dt) in
      { range ; colors ; t_slices }

    let create ~time_range ~ntslices ~vectors points =
      let t0, dt = time_range in
      let subdt = subdivide dt ntslices in
      let colors = L.map (fun (_, pt) -> Point.color pt) vectors in
      let rec f_t points tsls =
        function
        | 0 ->
            (* if this fails, some points are not in time_range *)
            assert (points = Ps.empty) ;
            tsls
        | n when n < 0 ->
            invalid_arg "negative n"
        | n ->
            let t0' = substart t0 subdt (I.Pos.of_int (n - 1)) in
            let tf' = substart t0 subdt (I.Pos.of_int n) in
            let points_in, points_out = Ps.partition (fun pt ->
                let t = Point.time pt in
                F.Op.((t0' <= t) && (t < tf'))
              ) points
            in
            let tsl = Tsl.create ~key:tf' (t0', subdt) vectors points_in in
            f_t points_out (Fm.add tf' tsl tsls) (n - 1)
      in
      let t_slices = f_t points Fm.empty (I.to_int ntslices)
      in
      let range = (F.Pos.close t0, F.Pos.close dt) in
      { range ; colors ; t_slices }


    let rand_choose ~rng prm =
      let binds = Fm.bindings prm.t_slices in
      let _, tsl = U.rand_unif_choose ~rng binds in
      tsl

    let hide_k tsl c k =
      begin match Tsl.hide_k tsl c k with
      | _ ->
          ()
      | exception Csl.Already_hidden ->
          ()
      end

    let hide_downto tsl c r =
      let rec f u =
        if F.Op.(u > r) then begin
          match Tsl.hide_u tsl c with
          | u' ->
              f u'
          | exception Csl.Already_hidden ->
              ()
        end else
          ()
      in
      f (Csl.umax_revealed (Tsl.find tsl c))

    let reveal_k tsl c k =
      ignore (Tsl.reveal_k tsl c k)

    let reveal_upto_exn tsl c r =
      let csl = Tsl.find tsl c in
      let umax = Csl.umax_revealed csl in
      let rec f u =
        if F.Op.(u <= r) then begin
          let u' = Tsl.reveal_u tsl c in
          f u'
        end else
          ()
      in
      f umax

    let reveal_upto tsl c r =
      try
        reveal_upto_exn tsl c r
      with Csl.Already_revealed ->
        ()


    let graft_k tsl c =
      begin match Tsl.graft_k tsl c with
      | _ ->
          ()
      | exception Csl.Not_all_revealed ->
          invalid_arg "graft_k : all k slices must be revealed"
      end

    let graft_upto tsl c r =
      let rec f u =
        if F.Op.(u <= r) then
          match Tsl.graft_u tsl c with
          | u' ->
              f u'
          | exception Csl.Not_all_revealed ->
              invalid_arg "graft_upto : all u slices must be revealed"
        else
          ()
      in
      f (Csl.umax_revealed (Tsl.find tsl c))

    let reveal_graft_upto tsl c r =
      try
        (* first, reveal if needed *)
        reveal_upto_exn tsl c r
      with Csl.Already_revealed ->
        (* then, graft if needed (if reveal raises) *)
        graft_upto tsl c r

    let with_slice tsl' nu =
      let t = Tsl.key tsl' in
      assert (Fm.mem t (slices nu)) ;
      let slices' = Fm.add t tsl' (slices nu) in
      { nu with t_slices = slices' }

    let map_slices f prm =
      let slices' = Fm.map f (slices prm) in
      { prm with t_slices = slices' }

    (* this returns a new prm with a new tsl and a new csl *)
    let map_uk_slices f prm tsl c =
      let tsl' = Tsl.map_uk_slices f tsl c in
      let t = Tsl.key tsl' in
      assert (Fm.mem t prm.t_slices) ;
      (* Here it would be better to copy the slices,
       * but actually it does not matter
       * (everything is recomputed at the prm level) *)
      let slices' = Fm.add t tsl' prm.t_slices in
      { prm with t_slices = slices' }

    let add_many cxs =
      L.fold_left (fun m (c, x) -> Cm.add c x m) Cm.empty cxs

    let merge prm prm' =
      (* prm and prm' should have no color in common *)
      let cols = Cs.of_list (colors prm) in
      let cols' = Cs.of_list (colors prm') in
      if not (Cs.is_empty (Cs.inter cols cols')) then
        invalid_arg "merge : color(s) in common" ;
      (* the two prms should have the same range *)
      if not (time_range prm = time_range prm') then
        invalid_arg "merge : different time range" ;
      (* the two prms should have the same time slicing *)
      let time_keys prm =
        let ts, _ = L.split (Fm.bindings (slices prm)) in
        Fs.of_list ts
      in
      if not (Fs.equal (time_keys prm) (time_keys prm')) then
        invalid_arg "merge : different t slicing" ;
      let range = time_range prm in
      let colors = (colors prm) @ (colors prm') in
      let t_slices = Fm.map (fun tsl ->
          let tsl' = t_slice_at prm' (Tsl.key tsl) in
          Tsl.fold_slices (fun csl' tsl'' ->
              Tsl.add_color csl' tsl''
            ) tsl' tsl
        ) (slices prm)
      in
      {
        range ;
        colors ;
        t_slices ;
      }

    let partition f prm =
      let t_slices_filt, t_slices_rem = Fm.fold (fun t tsl (filt, rem) ->
          (* partition c_slices depending on color *)
          let csl_filt, csl_rem = Cm.partition (fun c _ ->
              f c
            ) (Tsl.slices tsl)
          in
          (* now create two t_slice from those *)
          let tsl_filt = Tsl.from_slices csl_filt in
          let tsl_rem = Tsl.from_slices csl_rem in
          (Fm.add t tsl_filt filt, Fm.add t tsl_rem rem)
        ) (slices prm) (Fm.empty, Fm.empty)
      in
      let colors_filt, colors_rem = L.partition f (colors prm) in
      let range = time_range prm in
      let prm_filt = {
        range ;
        colors = colors_filt ;
        t_slices = t_slices_filt ;
      }
      in
      let prm_rem = {
        range ;
        colors = colors_rem ;
        t_slices = t_slices_rem ;
      }
      in prm_filt, prm_rem


    let slicing prm =
      let time_range = time_range prm in
      let ntslices = I.Pos.of_int (Fm.cardinal (slices prm)) in
      (time_range, ntslices)


    (* it would be nice to warn/error on ignored points *)
    let map_points f points prm =
      let prm' = map_slices (fun tsl ->
        (* keep points that fall in tsl *)
        let t0, dt = Tsl.range tsl in
        let tf = F.Pos.add t0 dt in
        let tsl_pts = Ps.filter (fun pt ->
            let t = Point.time pt in
            F.Op.((t0 <= t) && (t < tf))
          ) points
        in Tsl.map_slices (fun csl ->
          (* keep points with csl color *)
          let color = Csl.color csl in
          let csl_pts = Ps.filter (fun pt ->
              Point.Color.equal color (Point.color pt)
            ) tsl_pts
          in Csl.map_slices (fun uksl ->
            (* keep points in uksl *)
            let { origin ; vector } = UKsl.bounds uksl in
            let uksl_pts = Ps.filter (Point.inside ~origin ~vector) csl_pts in
            f uksl_pts uksl
          ) csl
        ) tsl
      ) prm
      in
      prm'

    let add_points = map_points UKsl.add_points

    let remove_points = map_points UKsl.remove_points

    (*
    let count nu =
      fold_slices (fun tsl c ->
        I.Pos.add c (Tsl.count tsl)
      ) nu I.zero

    let volume nu =
      F.Pos.narrow (fold_slices (fun tsl v ->
        F.Pos.add v (Tsl.volume tsl)
      ) nu F.zero)
    *)

    let density nu =
      F.Pos.div (I.Pos.to_float (count nu)) (volume nu)


    module Points =
      struct
        type elt = point

        type nonrec t = t

        let fold f nu x =
          fold_slices (fun tsl x' ->
            Ps.fold f (Tsl.points tsl) x'
          ) nu x

        let unfold _ _ =
          failwith "Not_implemented"
      end

    module Slices =
      struct
        type elt = uk_slice

        type nonrec t = t

        let fold f nu x =
          fold_slices (Tsl.fold_slices (Csl.fold_rvl_slices f)) nu x

        let unfold _ _ =
          failwith "Not_implemented"
      end

    module Csv_points = U.Csv.Make (Points)
    module Csv_grid = U.Csv.Make (Slices)

    let output_points chan prm =
      let columns = Point.columns in
      let f = U.Csv.Row.unfold ~columns Point.extract in
      Csv.output_record chan Point.columns ;
      Csv_points.write ~f ~chan prm ;
      Csv.close_out chan

    let columns_grid =
          (L.map (fun s -> "origin_" ^ s) Point.columns)
        @ (L.map (fun s -> "vector_" ^ s) Point.columns)
        @ ["count"]

    let extract_grid uksl =
      function
      | "count" ->
          I.to_string (UKsl.count uksl)
      | s ->
        begin match Scanf.sscanf s "origin_%s" (fun s' -> s') with
        | s' ->
            Point.extract uksl.bounds.origin s'
        | exception Scanf.Scan_failure _ ->
            begin match Scanf.sscanf s "vector_%s" (fun s' -> s') with
            | s' ->
                Point.extract uksl.bounds.vector s'
            | exception Scanf.Scan_failure _ ->
                invalid_arg "unrecognized column"
            end
        end

    let output_grid chan prm =
      let columns = columns_grid in
      let f = U.Csv.Row.unfold ~columns extract_grid in
      Csv.output_record chan columns ;
      Csv_grid.write ~f ~chan prm ;
      Csv.close_out chan

    (* for now we cheat for vectors *)
    let read ~time_range ~ntslices (* ~vectors *) du0 fname =
      Printf.eprintf "read\n%!" ;
      let exception Bad_input in
      let c =
        Csv.of_channel ~has_header:true (open_in fname)
      in
      try
        let points = Csv.Rows.fold_left ~f:(fun pts row ->
            match Point.read row with
            | None ->
                raise Bad_input
            | Some pt ->
                Ps.add pt pts
          ) ~init:Ps.empty c
        in
        Printf.eprintf "read %i points\n%!" (Ps.cardinal points) ;
        (* look for the point with max value *)
        (* we want to compare only color by color *)
        (* FIXME this doesn't take into account other dimensions
         * of a point that might exist *)
        Printf.eprintf "highest\n%!" ;
        let tops = Ps.fold (fun pt tops ->
          let c = Point.color pt in
          match Cm.find c tops with
          | top ->
              Cm.add c (Point.upper pt top) tops
          | exception Not_found -> (* first hit on this color *)
              Cm.add c pt tops
        ) points Cm.empty
        (* then how many slices are needed to reach that point ? *)
        in
        Printf.eprintf "nuslices\n%!" ;
        let nuslices umax =
          let rec f n du =
            if du > umax then
              (n, du)
            else
              f (I.Pos.succ n) (next_total_du du)
          in f I.one (F.Pos.narrow du0)
        in
        let vector_map = Cm.map (fun top ->
          let n, du = nuslices (Point.value top) in
          let dk = Point.number top + 1 in
          Printf.eprintf "du = %f, dk = %i\n%!" (F.to_float du) dk ;
          let v =
            top
            |> Point.with_value du
            |> Point.with_number dk
          in
          (n, v)
        ) tops
        in
        let _, vectors = L.split (Cm.bindings vector_map) in
        Printf.eprintf "create\n%!" ;
        Some (create ~time_range ~ntslices ~vectors points)
      with Bad_input ->
        None

  end
