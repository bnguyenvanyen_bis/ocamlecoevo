(** Iterate through the simulation of a stochastic process,
    and output the trajectory in Csv format *)

open Sig


(** [convert ~header ~line ~chan ~dt] is the tuple of functions needed
 *  to output information to a csv channel *)
val convert :
  ?dt: 't U.anypos ->
  header: string list list ->
  line: ('s U.pos -> 'a -> string list) ->
  chan: out_channel ->
    ('a, closed_pos * 'a, 'u) output
  (*
      ('u U.anypos -> 'a -> unit)
    * ('v U.anypos -> 'a -> unit)
    * ('w U.anypos -> 'a -> closed_pos * 'a)
   *)


(*
(** [output sim ?chan ?dt ~header ~line] is a simulation that outputs
 *  to the channel [chan] every [dt] with header [header]
 *  and line conversion function [line]. *)
val output :
  (output:('a, ('t U.pos * 'a) as 'b) output -> 'a -> 'b) ->
  ?chan:out_channel ->
  ?dt:'v U.anypos ->
  header:string list list ->
  line:('w U.pos -> 'a -> string list) ->
  'a ->
    'b
*)


(** [convert_compat ~header ~line ~chan ~dt] is {!convert} but it returns
 *  an empty list, so that it is compatible with {!Cadlag.convert}. *)
val convert_compat :
  ?dt: 't U.anypos ->
  header: string list list ->
  line: ('s U.pos -> 'a -> string list) ->
  chan: out_channel ->
    ('a, 'b list, 'u) output
  (*
      ('u U.anypos -> 'a -> unit)
    * ('v U.anypos -> 'a -> unit)
    * ('w U.anypos -> 'a -> 'b list)
   *)


