(** Types and signatures reused elsewhere. *)
exception Simulation_error

module L = BatList

module U = Util
module F = U.Float
module I = U.Int


type rng = Random.State.t
type closed_pos = F.closed_pos F.t
(* type 'a proba = 'a Util.proba *)
type time = closed_pos


(** Those functions are used as arguments to other functions,
 *  so they should be as general (?) as possible *)
type ('a, 't, 's) nextfun =
  't U.pos ->
  'a ->
    ('s U.anypos * 'a)


type ('a, 'b, 't) output = ('t U.anypos, 'a, 'b) Util.Out.t

(* FIXME rm
(* for its side-effects *)
type 'a out = (closed_pos -> 'a -> unit)


type ('a, 'b) output = {
  start : 'a out ;
  out : 'a out ;
  return : (closed_pos -> 'a -> 'b)
}


type ('a, 'b, 't) any_output = {
  start : ('t U.anypos -> 'a -> unit) ;
  out : ('t U.anypos -> 'a -> unit) ;
  return : ('t U.anypos -> 'a -> 'b)
}
*)


type ('a, 'b, 't) auxfun = ?auxi:'b -> 't U.pos -> 'a -> 'b
type ('a, 'b, 't) pred = ('t U.pos -> 'a -> 'b -> bool)


module type COLOR =
  sig
    open Util.Interfaces

    type t
    include COMPARABLE with type t := t
    include EQUALABLE with type t := t
    include REPRABLE with type t := t
  end


module type POINT =
  sig
    type t
    type color

    module Color : (COLOR with type t = color)
    module Colors : (BatMap.S with type key = color)

    (** there is a zero point for every color *)
    val zero : color -> t

    (** a point has a color *)
    val color : t -> color

    (** a point has a number *)
    val number : t -> int

    (** a point has a time *)
    val time : t -> 'a U.pos

    (** a point has a value *)
    val value : t -> 'a U.pos

    (** a point's number can be changed *)
    val with_number : int -> t -> t

    (** a point's time can be changed *)
    val with_time : 'a U.anypos -> t -> t

    (** a point's value can be changed *)
    val with_value : 'a U.anypos -> t -> t

    (** points can be added *)
    val add : t -> t -> t

    (** [volume ~vector] is the volume of the rectangular cuboid
     *  with the origin in one corner, and [vector] in the other. *)
    val volume :
      vector : t ->
        'a U.pos

    (** [inside ~origin ~vector pt] is [true] if [pt]
     *  is in the rectangular cuboid formed by taking [origin] as one corner,
     *  and [add origin vector] as the other. *)
    val inside :
      origin : t ->
      vector : t ->
      t ->
        bool

    (** [lower pt pt'] is the lower (left) corner of the smallest cuboid
     *  containing both [pt] and [pt']. *)
    val lower : t -> t -> t

    (** [upper pt pt'] is the upper (right) corner of the smallest cuboid
     *  containing both [pt] and [pt']. *)
    val upper : t -> t -> t

    (** a point can be randomly drawn,
     *  with the given color and number,
     *  and time and value from the volume bounded by [origin] and [vector].
     *  If [k] is not given, it should be 0. *)
    val rand_point :
      rng : U.rng ->
      ?k : int ->
      color ->
      origin : t ->
      vector : t ->
        t

    val columns : string list

    val extract : t -> string -> string

    (** a point can be represented as a string list. *)
    val line : t -> string list

    (** a point can be read from a CSV row. *)
    val read : Csv.Row.t -> t option
  end


module type COMPARABLE_POINT =
  sig
    include POINT
    include Util.Interfaces.COMPARABLE with type t := t
  end


(*
module type SLICE =
  sig
    type points
    type 'a t

    val logp : 'a t -> float

    val points : 'a t -> points

    val add_points : points -> 'a t -> unit

    val remove_points : points -> 'a t -> unit

    val add_logp : float -> 'a t -> unit

    val subtract_logp : float -> 'a t -> unit
  end
*)


module type PRM =
  sig
    type color
    type point

    (*
    type 'a _mutable = [< `Graftable | `Ungraftable ] as 'a

    type _ is_graftable =
      | Is_immutable : [`Immutable] is_graftable
      | Is_ungraftable : [`Ungraftable] is_graftable
      | Is_graftable : [`Graftable] is_graftable
    *)

    module Point : (COMPARABLE_POINT with type t = point
                                      and type color = color)
    (* module Cm = Point.Colors *)
    module Ps : (BatSet.S with type elt = point)

    module Cm : (BatMap.S with type key = color)

    type bounds = {
      origin : point ;
      vector : point ;
    }

    (** [rand_points_from ~rng points] is a point drawn uniformly from [points]. *)
    val rand_point_from : rng:U.rng -> Ps.t -> Point.t

    (** [rngm ~rng colors] associates a new PRNG to each color in [colors]. *)
    val rngm : rng:U.rng -> color list -> U.rng Cm.t

    module UKsl :
      sig
        type t

        (** a uk slice has a color *)
        val color : t -> color

        (** a uk slice has a number *)
        val number : t -> int

        (** a uk slice has bounds *)
        val bounds : t -> bounds

        (** a uk slice covers a volume *)
        val volume : t -> 'a U.pos

        (** a uk slice contains points *)
        val points : t -> Ps.t

        (** the contained points are counted *)
        val count : t -> 'a U.posint

        (** a uk slice has a number of points by unit of volume *)
        val density : t -> 'a U.pos

        (** a uk slice spans a range *)
        val range : t -> 'a U.pos * 'b U.pos

        (** a uk slice has a key (in a color slice) *)
        val key : t -> 'a U.pos * int

        (** points can be added to a uk slice *)
        val add_points : Ps.t -> t -> t

        (** points can be removed from a uk slice *)
        val remove_points : Ps.t -> t -> t

        (** a uk slice can be randomly drawn
         *  under the homogeneous Poisson process *)
        val rand_draw : rng:U.rng -> color -> bounds -> t

        (** a uk slice can be created from specified points *)
        val create : color -> bounds -> Ps.t -> t

        (** [rand_move ~rng dk uk_slice] adds [dk] uniformly drawn points
         *  to [uk_slice]. If [dk < 0], [~- dk] points are chosen randomly
         *  and removed. *)
        val rand_move : rng:U.rng -> int -> t -> t

        (** [logp_move f uksl uksl'] computes the log-probability/density
         *  of moving from [uksl] to [uksl'] with [f] giving the log-p/d
         *  of going from the count of [uksl] to the count of [uksl'].
         *  [uksl] and [uksl'] should have the same bounds. *)
        val logp_move : (t -> U._int -> U._float) -> t -> t -> U._float

      end

    module Csl :
      sig
        exception Already_hidden
        exception Already_revealed
        exception Not_all_revealed

        type t

        val color : t -> color

        val umax_revealed : t -> 'b U.pos

        val kmax : t -> int

        (* val logp : t -> float *)

        val points : t -> Ps.t

        val count : t -> 'a U.posint

        (*
        (** [deep_logp csl] recomputes the logp of [csl]
         *  from its revealed uk slices. *)
        val deep_logp : t -> float
        *)

        (** [bounds csl] is the bounds of the csl *)
        val bounds : t -> bounds

        val volume : t -> 'a U.pos

        val u_boundaries : t -> 'a U.pos list

        (* might Assertion_error *)
        val compare_bounds : t -> unit

        val add_points : Ps.t -> t -> unit

        val remove_points : Ps.t -> t -> unit

        (* val add_volume : 'a U.anypos -> t -> unit *)

        (* val subtract_volume : 'a U.anypos -> t -> unit *)

        val find : t -> ('b U.anypos * int) -> UKsl.t

        (** [create color bounds nuslices points] is an ungraftable color
         *  of color [color] over the volume bounded by [bounds],
         *  split into [nuslices] slices for [u],
         *  with points [points].
         *  The origin should have number [k = 0] and the total number
         *  of [k] slices [kmax] is given by the vector's number.
         *  The uk slices are then numeroted [0] to [kmax - 1]. *)
        val create :
          color ->
          bounds ->
          U.anyposint ->
          Ps.t ->
            t

        (** [rand_draw ~rng color bounds nuslices] is a graftable color slice
         *  with points generated under a homogeneous Poisson process
         *  of intensity [1.], and is otherwise like {!create}. *)
        val rand_draw :
          rng:U.rng ->
          color ->
          bounds ->
          U.anyposint ->
            t

        (* Invalid_argument, Not_found, Already_hidden *)
        val hide_k : t -> int -> Ps.t
        val hide_u : t -> Ps.t
        (* Invalid_argument, Already_revealed *)
        val reveal_k : t -> int -> Ps.t
        val reveal_u : t -> Ps.t
        (* Invalid_argument, Not_all_revealed *)
        val graft_k : t -> int * Ps.t
        val graft_u : t -> Ps.t

        (** [map_slices f csl] is [csl] with [f] applied to all of its
         *  revealed slices.
         *  [f] should not change the bounds of the uk slices. *)
        val map_slices :
          (UKsl.t -> UKsl.t) ->
          t ->
            t

        (** [fold_rvl_slices f csl x] is [f] folded over all the revealed slices
         *  of [csl]. *)
        val fold_rvl_slices :
          (UKsl.t -> 'b -> 'b) ->
          t ->
          'b ->
            'b
      end

    module Tsl :
      sig
        type t

        val key : t -> 's U.pos

        val range : t -> 's U.pos * 't U.pos

        (* val logp : t -> float *)

        val points : t -> Ps.t

        val count : t -> 'a U.posint

        val volume : t -> 'a U.pos

        (*
        val deep_logp : t -> float
        *)

        (** [add_color csl tsl] is [tsl] with a new color slice [csl].
         *  If [csl]'s color is already present, [Invalid_argument] is raised.
         *  The result is mutable if [tsl] is mutable,
         *  or if [csl] is mutable. *)
        val add_color : Csl.t -> t -> t

        val add_points : Ps.t -> t -> unit

        val remove_points : Ps.t -> t -> unit

        (* val add_volume : 'a U.anypos -> t -> unit *)

        (* val subtract_volume : 'a U.anypos -> t -> unit *)

        (* val add_logp : float -> t -> unit *)

        (* val subtract_logp : float -> t -> unit *)

        val find : t -> color -> Csl.t

        val rand_draw :
          rngm:U.rng Cm.t ->
          key:U.closed_pos ->
          U.closed_pos * U.closed_pos ->
          (U.anyposint * point) list ->
            t

        val create :
          key:U.closed_pos ->
          U.closed_pos * U.closed_pos ->
          (U.anyposint * point) list ->
          Ps.t ->
            t

        val hide_k : t -> color -> int -> unit
        val hide_u : t -> color -> 'a U.pos
        val reveal_k : t -> color -> int -> unit
        val reveal_u : t -> color -> 'a U.pos
        val graft_k : t -> color -> int
        val graft_u : t -> color -> 'a U.pos

        (** [rand_choose ~rng tsl] is a uniformly chosen color slice from [tsl]. *)
        val rand_choose :
          rng:Random.State.t ->
          t ->
            Csl.t

        (** [with_slice csl tsl] is [tsl] with a replaced slice [csl]. *)
        val with_slice :
          Csl.t ->
          t ->
            t

        val map_slices :
          (Csl.t -> Csl.t) ->
          t ->
            t

        val fold_slices :
          (Csl.t -> 'b -> 'b) ->
          t ->
          'b ->
            'b

        (** [iter_points f tsl] applies [f] in turn to all points of the time slice [tsl]
         *  for its side-effects, with a refresh of the subsequent points
         *  when [f] returns [`Refresh]. *)
        val iter_points :
          (point -> [`Continue | `Refresh]) ->
          t ->
            unit

        (** [fold_points f tsl x] folds [f] in turn over all points of the time slice [tsl],
         *  with the subsequent points refreshed when [f] returns [`Refresh _]. *)
        val fold_points :
          (point -> 'b -> [`Continue of 'b | `Refresh of 'b]) ->
          t ->
          'b ->
            'b
      end

    (** the type of a [u-k] slice of a color slice. *)
    type uk_slice = UKsl.t

    (** the type of a color slice of a time slice. *)
    type c_slice = Csl.t

    (** the type of a time slice of a prm. *)
    type t_slice = Tsl.t

    (** the type of Poisson random measure (prm) realisations. *)
    type t

    type 'a map (* = 'a Cm.t *)

    val add_many : (color * 'a) list -> 'a map

    (** [time_range prm] is [(t0, dt)] the time range of [prm]. *)
    val time_range :
      t ->
        's U.pos * 't U.pos

    val colors : t -> color list

    val count : t -> 'a U.posint

    val volume : t -> 'a U.pos

    (** [logp prm] is the (approximate) log-probability of [prm]. *)
    val logp : t -> 'a U.neg

    val nslices : t -> 'a U.posint

    (*
    val deep_logp : t -> float

    (** assert that logp equal deep logp *)
    val check_logp : t -> unit
    *)

    (** [t_slice_at prm t] is the time slice of [prm] with key [t]
     *  Can raise Not_found. *)
    val t_slice_at :
      t ->
      's U.anypos ->
        t_slice

    (** [t_slice_of prm t] is the time slice of [prm] containing [t+]. *)
    val t_slice_of :
      t ->
      's U.anypos ->
        t_slice

    (** [create ~time_range ~ntslices ~vectors points] is an ungraftable prm
     *  over the time range [time_range] with [ntslices] time slices, 
     *  and for each color [c] and value [(umax, n_uslices)] of [cranges],
     *  an [u] range of [0.] to [umax] and [n_uslices] u slices.
     *  with the points [points]. *)
    val create :
      time_range : ('s U.anypos * 't U.anypos) ->
      ntslices : U.anyposint ->
      vectors : (U.anyposint * point) list ->
      Ps.t ->
        t

    (** [rand_draw ~rngm ~time_range ~ntslices ~vectors] is a graftable prm,
     *  with random points drawn for each color with the corresponding
     *  rng in [rngm], and otherwise like {!create}. *)
    val rand_draw :
      rngm : Random.State.t Cm.t ->
      time_range : ('s U.anypos * 't U.anypos) ->
      ntslices : U.anyposint ->
      vectors : (U.anyposint * point) list ->
        t

    (** [rand_choose ~rng prm] is a uniformly chosen time slice from [prm]. *)
    val rand_choose :
      rng:Random.State.t ->
      t ->
        t_slice

    (** [hide_k tsl c k] hides all points of color [c] and number [k]
     *  from the time slice [tsl] in [prm].
     *  If there are no points of number [k], exception [Not_found] is raised.
     *  If [tsl] is immutable, exception [Invalid_argument] is raised.
     *  If number [k] is already hidden, the function does nothing. *)
    val hide_k :
      t_slice ->
      color ->
      int ->
        unit

    (** [hide_downto tsl c r] hides points of color [c]
     *  from the time slice [tsl] in [prm],
     *  so that all remaining points have a value strictly smaller than [r],
     *  except for points in the smallest slice,
     *  which always stay revealed.
     *  If [tsl] is immutable, [Invalid_argument] is raised. *)
    val hide_downto :
      t_slice ->
      color ->
      'b U.anypos ->
        unit

    (** [reveal_k tsl c k] reveals points of color [c] and number [k]
     *  from the time slice [tsl] in [prm].
     *  If there are no points of number [k], exception [Not_found] is raised.
     *  If [tsl] is immutable, [Invalid_argument] is raised.
     *  If number [k] is already revealed, the function does nothing. *)
    val reveal_k :
      t_slice ->
      color ->
      int ->
        unit

    (** [reveal_upto tsl c r] reveals points of color [c]
     *  from the time slice [tsl] in [prm],
     *  so that no point with a value smaller than [r] stays hidden.
     *  If [tsl] is immutable, [Invalid_argument] is raised. *)
    val reveal_upto :
      t_slice ->
      color ->
      'b U.anypos ->
        unit

    (** [graft_k tsl c] grafts new slices of color [c]
     *  to the time slice [tsl] in [prm], so that their number
     *  is the next unused number.
     *  If not all numbers are revealed, [Invalid_argument] is raised.
     *  If [tsl] is ungraftable, [Invalid_argument] is raised. *)
    val graft_k :
      t_slice ->
      color ->
        unit

    (** [graft_upto tsl c r] grafts new slices of color [c]
     *  to the time slice [tsl] in [prm], so that [r] becomes covered.
     *  If not all [u] values are revealed, [Invalid_argument] is raised.
     *  If [tsl] is ungraftable, [Invalid_argument] is raised. *)
    val graft_upto :
      t_slice ->
      color ->
      'a U.anypos ->
        unit

    (** [reveal_graft_upto tsl c r] reveals if needed,
     *  then grafts, if needed, new slices of color [c]
     *  to the time slice [tsl] in [prm], so that [r] becomes covered.
     *  If [tsl] is immutable, [Invalid_argument] is raised.
     *  If [tsl] is ungraftable, [Invalid_argument] might be raised,
     *  if grafting was required to reach [r]. *)
    val reveal_graft_upto :
      t_slice ->
      color ->
      'a U.anypos ->
        unit

    val with_slice :
      Tsl.t ->
      t ->
        t

    (** [map_uk_slices f prm tsl csl] replaces each [u] slice [usl] of [csl]
     *  by [f usl]. *)
    val map_uk_slices :
      (uk_slice -> uk_slice) ->
      t ->
      t_slice ->
      color ->
        t

    (** [map_slices f prm] replaces each [t] slice [tsl] of [prm] by [f tsl]. *)
    val map_slices :
      (t_slice -> t_slice) ->
      t ->
        t

    (** [iter_slices f prm] applies [f] in turn to all time slices
     *  of [prm]. *)
    val iter_slices :
      (t_slice -> unit) ->
      t ->
        unit

    (** [fold_slices f prm] folds [f] in turn over all the time slices
     *  of [prm]. *)
    val fold_slices :
      (t_slice -> 'b -> 'b) ->
      t ->
      'b ->
        'b

    (** [output_points chan prm] outputs all points of [prm] in csv format
     *  to [chan]. *)
    val output_points : U.Csv.out_channel -> t -> unit

    val columns_grid : string list

    val extract_grid : uk_slice -> string -> string

    val output_grid : U.Csv.out_channel -> t -> unit

    (** [read ~time_range ~ntslices du0 fname] is some prm
     *  created as for {!create}, with points read from [chan],
     *  or [None] if no points are read. *)
    val read :
      time_range : ('s U.anypos * 't U.anypos) ->
      ntslices : U.anyposint ->
      (* vectors : (U.anyposint * point) list -> *)
      'c U.anypos ->
      string ->
        t option

    (** [merge prm prm'] is a [t] where for each [t_slice], 
     *  the color slices of [prm'] are added to those of [prm].
     *  (with {!Tsl.add_color}).
     *  If [prm] and [prm'] don't cover the same range, have color(s)
     *  in common, or have a different time slicing,
     *  then [Invalid_argument] is raised. *)
    val merge : t -> t -> t

    (** [partition f prm] is [(prm_filt, prm_rem)]
     *   where [prm_filt] contains the colors that pass the predicate [f],
     *   and [prm_rem] contains the remaining colors. *)
    val partition : (color -> bool) -> t -> t * t

    (** [slicing prm] is the range and number of time slices of [prm]. *)
    val slicing : t -> ('s U.pos * 't U.pos) * 'a U.posint

    (** [add_points points prm] adds the points that are in bounds to [prm].
     *  The points not in range are ignored. *)
    val add_points : Ps.t -> t -> t

    (** [remove_points points prm] removes the points that are
     *  in bounds from [prm].
     *  The points not in range are ignored.
     *  If a point in range is not present in [prm],
     *  an [Assertion_error] is raised. *)
    val remove_points : Ps.t -> t -> t

    val density : t -> 'a U.pos

    module Points : (Util.Interfaces.SEQ with type elt = point
                                          and type t = t)

    module Slices : (Util.Interfaces.SEQ with type elt = uk_slice
                                          and type t = t)
  end


