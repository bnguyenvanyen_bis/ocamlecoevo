(** Iterate through the simulation of a stochastic process,
    returning the trajectory as a list. *)

open Sig


(** [convert ~conv ~dt] is the tuple of functions needed to
 *  store information in a ref. *)
val convert :
  ?dt: 't U.anypos ->
  conv: ('s U.pos -> 'a -> 'b) ->
    ('a, (closed_pos * 'b) list, 't) output
  (*
      ('u U.anypos -> 'a -> unit)
    * ('v U.anypos -> 'a -> unit)
    * ('w U.anypos -> 'a -> (closed_pos * 'b) list)
   *)


(*
(** [output sim ?dt ~conv] is a simulation that outputs a list of values,
 *  one every [dt], with the conversion function [conv]. *)
val output :
  (output:('a, (closed_pos * 'b) list) output -> 'a -> 'd) ->
  ?dt:'u U.anypos ->
  conv:('v U.pos -> 'a -> 'b) ->
  'a ->
    'd
*)



(*
(** [simulate ~conv ?dt ~seed next auxf p x0]
    simulates the system defined by the stepping function `next`
    from the initial condition `x0` until the predicate `p` becomes true.
    It stores the trajectory as a list as specified by `line`,
    with a point at most every `dt` (default 0.).
    Finally, it returns the trajectory. *)
val simulate :
  conv: ('s U.pos -> 'a -> 'b) ->
  ?dt: 't U.anypos ->
  next: ('a,'u,'v) nextfun ->
  ('a,'b,'w) auxfun ->
  ('a,'b,'x) pred ->
  'a ->
  (closed_pos * 'b) list


(** [simulate_until ~conv ~dt ~seed  next tf x0]
    simulates the system defined by the stepping function `next`
    from the initial condition `x0` until the final time `tf`.
    It stores the trajectory as a list as specified by `line`,
    with a point at most every `dt` (default 0.).
    Finally, it returns the trajectory. *)
val simulate_until :
  conv : ('s U.pos -> 'a -> 'b) ->
  ?dt : 't U.anypos ->
  next : ('a,'u,'v) nextfun ->
  'w U.anypos ->
  'a ->
  (closed_pos * 'b) list
*)
