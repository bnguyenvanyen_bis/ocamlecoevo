(** Iterate through the simulation of a stochastic process *)

open Sig


(*
(** [combine output output'] combines both outputs in one. *)
val combine :
  ('b -> 'c -> 'd) ->
  ('a, 'b, F.closed_pos) any_output ->
  ('a, 'c, F.closed_pos) any_output ->
    ('a, 'd, 'u) any_output
*)


(** [simulate ~next ~output x0 auxf p]
    simulates the system defined by the stepping function [next]
    from the initial condition [x0] until the predicate [p] becomes true,
    with a random generator seeded by [seed] (self-seeded by default).
    Before, during, and after the simulation,
    it calls [output.start], [output.out] and [output.return]. *)
val simulate :
  next : ('a,'v,'w) nextfun ->
  output : ('a, 'b, F.closed_pos) output ->
  'a ->
  ('a,'c,'x) auxfun ->
  ('a, 'c, 'y) pred ->
    'b


val simulate_return :
  next : ('a,'v,'w) nextfun ->
  'a ->
  ('a,'c,'x) auxfun ->
  ('a, 'c, 'y) pred ->
    closed_pos * 'a


(** [simulate_until ~next ~output x0 tf]
    simulates the system defined by the stepping function [next]
    from the initial condition [x0] until the final time [tf],
    with a random generator seeded by [seed] (self-seeded by default).
    Before, during, and after the simulation,
    it calls [out_start], [out] and [out_end], which by default do nothing.
    Finally it returns the final time and state reached by the system. *)
val simulate_until :
  next : ('a,'v,'w) nextfun ->
  output : ('a, 'b, F.closed_pos) output ->
  'a ->
  'y U.anypos ->
    'b
