open Popsim__Sig

module U = Util


(* If we want the Auto and Timedep cases,
 * we need two write more functors *)
module Make (Sym : SYMBOLIC_AUTO) =
  struct
    open Sym

    let rec eval_rate :
      type a. a Rate.t -> a = Rate.(
      function
      | Eval (ef, ex) ->
          let f = eval_rate ef in
          let x = eval_rate ex in
          f x
      | Lift_left ex ->
          let x = eval_rate ex in
          (fun _ -> x)
      | Lift_right ef ->
          let f = eval_rate ef in
          (fun x _ -> f x)
      | Lift_fun ef ->
          let f = eval_rate ef in
          (fun g c -> f (g c))
      | Lift_double ef ->
          let f = eval_rate ef in
          (fun g g' d -> f (g d) (g' d))
      | Constant ex ->
          eval_value ex
      | Count (`Id group) ->
          (fun z -> Pop.count_id ~group z)
      | Count (`Nonid group) ->
          (fun z -> Pop.count_nonid ~group z)
      | Max_score ei ->
          let i = U.Int.to_int (eval_value ei) in
          (fun z ->
            z
            |> Pop.max ~i
            |> fst
            |> U.Float.Pos.of_float
          )
      | Fun f ->
          f
      | Time ->
          U.Float.Pos.narrow
      | Float_of_int ->
          U.Int.Pos.to_float
      | Add_int ->
          U.Int.Pos.add
      | Mul_int ->
          U.Int.Pos.mul
      | Add ->
          U.Float.Pos.add
      | Mul ->
          U.Float.Pos.mul
      | Div ->
          U.Float.Pos.div
      | Pow ei ->
          let i = U.Int.to_int (eval_value ei) in
          (fun x -> U.Float.Pos.pow x i)
    )

    let rec eval_dist :
      type a. a Dist.t -> (Random.State.t -> a) = Dist.(
      function
      | Eval (ef, ex) ->
          let f = eval_dist ef in
          let x = eval_dist ex in
          (fun rng -> f rng (x rng))
      | Chain (e, e') ->
          let f = eval_dist e in
          let f' = eval_dist e' in
          (fun rng a -> f' rng (f rng a))
      | Constant ex ->
          let x = eval_value ex in
          (fun _ -> x)
      | Rng_auto e ->
          Sim.Ctmjp.Rng_unsymb.Auto.eval e
      | Rng_timedep e ->
          Sim.Ctmjp.Rng_unsymb.Timedep.eval e
      | Unichoice ->
          (fun rng l -> U.rand_unif_choose ~rng l)
      | Exp ->
          (fun rng lbd -> U.rand_exp ~rng lbd)
      | Uniform (ea, eb) ->
          let a = eval_value ea in
          let b = eval_value eb in
          let bma = U.Float.Pos.of_anyfloat (U.Float.sub b a) in
          (fun rng -> U.Float.add a (U.rand_float ~rng bma))
      | Poisson ->
          (fun rng lbd -> U.rand_poisson ~rng lbd)
      | Negbin eovd ->
          let ovd = eval_value eovd in
          (fun rng lbd -> U.rand_negbin_over ~rng ovd lbd)
      | Normal ev ->
          let v = eval_value ev in
          (* FIXME a bit dumb that we're not in U.Float world *)
          (fun rng x ->
            x
            |> U.Float.to_float
            |> (fun x' -> U.rand_normal ~rng x' v)
            |> U.Float.of_float)
      | Lognormal ev ->
          let v = eval_value ev in
          (fun rng x ->
            x
            |> U.Float.to_float
            |> (fun x' -> U.rand_lognormal ~rng x' v)
            |> U.Float.of_float)
      | Multinormal ecov ->
          let mat = eval_value ecov in
          let cov = U.mat_to_cov mat in
          (fun rng x -> U.rand_multi_normal ~rng x cov)
      | Multinormal_std ed ->
          let d = U.Int.to_int (eval_value ed) in
          (fun rng -> U.rand_multi_normal_std ~rng d)
    )


    (* FIXME Need a version that allows for time *)

    let rec eval_choice :
      type a b. (a, b) T.Choice.t -> (Random.State.t -> a -> b) = T.C.(
      function
      | And (c, c') ->
          let f = eval_choice c in
          let f' = eval_choice c' in
          (fun rng z -> (f rng z, f' rng z))
      | Choose group ->
          (fun rng z -> Pop.choose ~rng ~group z)
    )


    (* This depends on whether we're in the Auto or Timedep case *)

    let rec eval_action :
      type a b. (a, b) T.Action.t -> (Random.State.t -> a -> b) = T.A.(
      function
      | And (a, a') ->
          let f = eval_action a in
          let f' = eval_action a' in
          (fun rng (inds, inds') z -> f' rng inds' (f rng inds z))
      | Remove ->
          (fun _ ind z -> Pop.remove z ind ; z)
      | Remove_from g ->
          (fun _ z ->
            Pop.remove z (Pop.new_indiv_nonid (group_to_trait g)) ;
            z
          )
      | Copy ->
          (fun _ ind z -> Pop.add z ind ; z)
      | Copy_mutate dist ->
          let d = eval_dist dist in
          (fun rng ind z ->
             let ind' = Pop.new_indiv_id z (d rng (Pop.trait_of ind)) in
             Pop.add z ind' ;
             z
          )
      | New_id ed ->
          let d = eval_dist ed in
          (fun rng z ->
             Pop.add z (Pop.new_indiv_id z (d rng)) ;
             z
          )
      | New_from g ->
          (fun _ z ->
            Pop.add z (Pop.new_indiv_nonid (group_to_trait g)) ;
            z
          )
    )


    let eval_transfo =
      function
      | T.Choice c ->
          eval_choice c
      | T.Action a ->
          eval_action a


    let rec eval_proba :
      type a. a Proba.t -> a = Proba.(
      function
      | Eval (ef, ex) ->
          let f = eval_proba ef in
          let x = eval_proba ex in
          f x
      | Lift_left ep ->
          let p = eval_proba ep in
          (fun _ -> p)
      | Lift_right ef ->
          let f = eval_proba ef in
          (fun x _ -> f x)
      | Lift_fun ef ->
          let f = eval_proba ef in
          (fun g c -> f (g c))
      | Lift_double ef ->
          let f = eval_proba ef in
          (fun g g' d -> f (g d) (g' d))
      | Constant pv ->
          U.Float.Proba.narrow (eval_value pv)
      | Count (`Id group) ->
          (fun z -> Pop.count_id ~group z)
      | Count (`Nonid group) ->
          (fun z -> Pop.count_nonid ~group z)
      | Float_of_int ->
          U.Int.Pos.to_float
      | Rel_max_score ei ->
          let i = U.Int.to_int (eval_value ei) in
          (fun ind z ->
            let score = Pop.scores ~i z in
            (* if ind is an indiv of z, then we know
             * that score on ind is smaller that max score
             * so we promote to proba *)
            U.Float.promote_unsafe (
              U.Float.div
                (U.Float.Pos.of_float (score (Pop.trait_of ind)))
                (U.Float.Pos.of_float (fst (Pop.max ~i z)))
            )
          )
      | Time ->
          U.Float.Pos.narrow
      | Add_int ->
          U.Int.Pos.add
      | Mul_int ->
          U.Int.Pos.mul
      | Add ->
          U.Float.Pos.add
      | Mul ->
          U.Float.Proba.mul
      | Prop ->
          (fun a b -> let _, p, _ = U.Float.Pos.proportions a b in p)
    )


    module Auto =
      struct
        module S = Auto.S
        module S' = Sim.Ctmjp.Rng_unsymb.Auto.S

        (* from S to S' *)
        let rec eval :
          type a. a S.Modif.symb -> a S'.Modif.symb =
          function
          | S.Modif.Identity ->
              S'.Modif.Identity
          | S.Modif.Transfo tr ->
              S'.Modif.Transfo (eval_transfo tr)
          | S.Modif.Bernoulli p ->
              S'.Modif.Bernoulli (eval_proba p)
          | S.Modif.Bernoulli_lifted p ->
              S'.Modif.Bernoulli_lifted (eval_proba p)
          | S.Modif.Erase (pred, e) ->
              S'.Modif.Erase (eval pred, eval e)
          | S.Modif.Erase_lifted (pred, e) ->
              S'.Modif.Erase_lifted (eval pred, eval e)
          | S.Modif.Map (e, e') ->
              S'.Modif.Map (eval e, eval e')
          | S.Modif.Chain (e, e') ->
              S'.Modif.Chain (eval e, eval e')
          | S.Modif.Chain_lifted (e, e') ->
              S'.Modif.Chain_lifted (eval e, eval e')
          | S.Modif.Lift e ->
              S'.Modif.Lift (eval e)
      end

    module Timedep =
      struct
        module S = Timedep.S
        module S' = Sim.Ctmjp.Rng_unsymb.Timedep.S

        (* from S to S' *)
        let rec eval :
          type a. a S.Modif.symb -> a S'.Modif.symb =
          function
          | S.Modif.Identity ->
              S'.Modif.Identity
          | S.Modif.Transfo tr ->
              (* So far nothing can depend on time in Transfo *)
              let f = eval_transfo tr in
              S'.Modif.Transfo (fun rng _ -> f rng)
          | S.Modif.Bernoulli p ->
              S'.Modif.Bernoulli (eval_proba p)
          | S.Modif.Bernoulli_lifted p ->
              S'.Modif.Bernoulli_lifted (eval_proba p)
          | S.Modif.Erase (pred, e) ->
              S'.Modif.Erase (eval pred, eval e)
          | S.Modif.Erase_lifted (pred, e) ->
              S'.Modif.Erase_lifted (eval pred, eval e)
          | S.Modif.Map (e, e') ->
              S'.Modif.Map (eval e, eval e')
          | S.Modif.Chain (e, e') ->
              S'.Modif.Chain (eval e, eval e')
          | S.Modif.Chain_lifted (e, e') ->
              S'.Modif.Chain_lifted (eval e, eval e')
          | S.Modif.Lift e ->
              S'.Modif.Lift (eval e)
      end
  end

