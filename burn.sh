#!/bin/sh
# $1 : file to read from
# $2 : line of the header (starting from 0)
# $3 : number of lines to discard
# output header
head -n $(($2+1)) "$1" | tail -n 1
# output lines we keep
start=$(($2+$3+2))
tail -n +"$start" "$1"
