(** Getting and setting in vector via its group *)

open Sig

(* for the ode case *)

module Make (T : 
  sig
    include TRAIT
    (* this is needed as input because
     * we might want to not consider some of the groups
     * to reduce the size of the system *)
    val group_to_int : 'a group -> int
  end) :
  (
    sig
      type t = vec * U.closed_pos
      module Get : (GET with type t = t)
      module Set : (SET with type t = vec)
      include (MATSET with module T = T)
      val to_tuple : vec -> (float * float * float * float)
    end
  ) =
  struct
    module T = T

    type t = Lacaml.D.Vec.t * U.closed_pos

    type 'a group = 'a T.group

    let iog = T.group_to_int

    (* FIXME unsafe... this should be positive *)
    (* on a positive vector *)
    let get (type a) (grp : a group) =
      match iog grp with
      | i ->
          (* (fun (v, _) : 'a F.pos F.t -> F.of_float_unsafe v.{i}) *)
          (fun (v, _) : 'a F.pos F.t -> F.Pos.of_float v.{i})
      | exception Invalid_argument _ ->
          (fun _ : 'a F.pos F.t -> F.zero)

    module Get =
      struct
        type nonrec t = t

        (* FIXME we would like to not eta-expand *)
        let sus = get T.Sus
        let exp = get T.Exp
        let inf = get T.Inf
        let rem = get T.Rem
        let cas = get T.Cas
        let out = get T.Out
        let tot (_, x) = F.Pos.narrow x
      end

    let set grp =
      match iog grp with
      | i ->
          let f v x =
            v.{i} <- F.to_float x
          in f
      | exception (Invalid_argument _ as e) ->
          let f _ _ =
            raise e
          in f

    module Set =
      struct
        type t = vec
        
        let sus = set T.Sus
        let exp = set T.Exp
        let inf = set T.Inf
        let rem = set T.Rem
        let cas = set T.Cas
        let out = set T.Out
      end
    (*
    let set grp grp' mat x =
      mat.{iog grp, iog grp'} <- x
    *)

    let inc grp grp' mat x =
      let i = iog grp in
      let i' = iog grp' in
      mat.{i, i'} <- mat.{i, i'} +. x

    let to_tuple v =
      let vtot = (v, F.zero) in
      let s = F.to_float (Get.sus vtot) in
      let e = F.to_float (Get.exp vtot) in
      let i = F.to_float (Get.inf vtot) in
      let r = F.to_float (Get.rem vtot) in
      (s, e, i, r)
  end
