open Sig


include Sim.Prm.Point (Color)


(* reference volume for base slices *)
let v0 = F.Pos.of_float 11.
let ten = F.Pos.of_float 10.

let ntslices ~h tf =
  I.Pos.of_int (int_of_float (F.to_float (F.div tf h)))

let vectors ~h colors =
  let umax = F.Pos.Op.(v0 / (F.one + ten * h)) in
  L.map (fun c ->
    (I.one, Sim.Prm.{ c ; k = 1 ; t = F.zero ; u = umax })
  ) colors
