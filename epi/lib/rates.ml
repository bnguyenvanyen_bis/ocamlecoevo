open Sig

module Make (Get : GET) =
  struct
    type t = Get.t

    (* building blocks *)
    let infect beta z =
      let s = Get.sus z in
      let n = Get.tot z in
      F.Pos.Op.(beta * s / n)

    (* coalescence rate between two `I lineages *)
    let coal_ii infct z =
      let i = Get.inf z in
      (* maybe remove this condition for easier estimation ? *)
      F.Pos.Op.(infct / i)

    (* coalescence rate between a `E and a `I lineage *)
    let coal_ei infct z =
      let e = Get.exp z in
      F.Pos.Op.(infct / e)

    let reportr_e _ par z =
      let e = Get.exp z in
      F.Pos.Op.((Param.rho par) * (Param.sigma par) * e)

    let reportr_i infct par z =
      let i = Get.inf z in
      F.Pos.Op.((Param.rho par) * infct * i)

    (* actual rates *)
    let infection_of infctvt par t z =
      let i = Get.inf z in
      F.Pos.mul (infect (infctvt par t) z) i

    let leave_exposed par _ z =
      let e = Get.exp z in
      F.Pos.Op.((Param.sigma par) * e)

    let recovery par _ z =
      let i = Get.inf z in
      F.Pos.Op.((Param.nu par) * i)

    let immunity_loss par _ z =
      let r = Get.rem z in
      F.Pos.Op.((Param.gamma par) * r)

    let out_infection_of infctvt par t z =
      F.Pos.mul (infect (infctvt par t) z) (Param.eta par)

    let out_death par _ z =
      let o = Get.out z in
      (* o - 1 *)
      let om1 = F.Pos.of_anyfloat (F.sub o F.one) in
      F.Pos.Op.((Param.r_outdeath par) * om1 * o)

    let host_birth par _ z =
      F.Pos.Op.(Get.tot z * Param.host_birth par)

    let sus_host_death par _ z =
      F.Pos.Op.((Param.host_death par) * Get.sus z)

    let exp_host_death par _ z =
      F.Pos.Op.((Param.host_death par) * Get.exp z)

    let inf_host_death par _ z =
      F.Pos.Op.((Param.host_death par) * Get.inf z)

    let rem_host_death par _ z =
      F.Pos.Op.((Param.host_death par) * Get.rem z)

  module Out =
    struct
      let header = Out.header

      let conv infctvt coal reportr par t z =
        let get_seirco z =
          (Get.sus z, Get.exp z, Get.inf z, Get.rem z, Get.cas z, Get.out z)
        in
        let f par t z =
          let s, e, i, r, c, o = get_seirco z in
          let n = Get.tot z in
          let infct = infect (infctvt par t) z in
          let reffv = Seirco.reff infct par in
          let coalv = coal infct z in
          let reportrv = reportr infct par z in
          Traj.{ s ; e ; i ; r ; c ; o ; n ;
                 reff = reffv ;
                 coal = coalv ;
                 reportr = reportrv
               }
        in f par t z

      let line infctvt coal reportr par t z =
        let soaf x = F.to_string x in
        let pt = conv infctvt coal reportr par t z in
        Traj.[ soaf t ;
          soaf pt.s ; soaf pt.e ; soaf pt.i ; soaf pt.r ;
          soaf pt.c ; soaf pt.o ; soaf pt.n ;
          soaf pt.reff ; soaf pt.coal ; soaf pt.reportr ]
    end

  end 

