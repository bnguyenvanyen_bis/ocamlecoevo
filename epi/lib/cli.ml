(** Useful functions for an interface *)

open Sig

module La = Lift_arg
module Di = Default_init


let load_par_row =
  Read.Reader.load


(* read the k-th row, or the last one *)
let load_par_from reads theta ?k s =
  let csv = Csv.Rows.load ~separator:',' ~has_header:true s in
  let row =
    match k with
    | None ->
        csv |> L.rev |> L.hd
    | Some k ->
        (* we would like to check that the column "k" exists *)
        L.find (fun row ->
          int_of_string (Csv.Row.find row "k") = k
        ) csv
  in
  load_par_row reads row theta


(* FIXME we need to hide the type in the end. How ? *)
let version =
  function
  | "gill" ->
      V Unit_gill
  | "prm" ->
      V Unit_prm
  | "prm_approx" ->
      V Unit_prm_approx
  | "ode" ->
      V Unit_ode
  | _ ->
      invalid_arg "wrong symbol"



let model v = Model.(
  function
  | "sir" ->
      E (Sir v)
  | "seir" ->
      E (Seir v)
  | "sirs" ->
      E (Sirs v)
  | "seirs" ->
      E (Seirs v)
  | _ ->
      invalid_arg "wrong symbol"
)


module Unit =
  struct
    class param = Param.t_unit

    class init = object
      inherit param
      inherit Param.tfable
      inherit Param.dtable
    end


    module Gill =
      struct
        let cv s =
          s |> float_of_string |> int_of_float

        class immediate_init = object
          inherit init
          inherit Param.seedable
          inherit Param.init_sir_int
        end

        class latent_init = object
          inherit init
          inherit Param.seedable
          inherit Param.init_seir_int
        end

        module Make (S : MODEL_SPEC) =
          struct
            module E = Process.Unit.Make (S)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if S.sp.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv ; seed]
              @ (if S.sp.circular then circular () else [])
              @ (if S.sp.latency then latent () else [])
              @ base ()
            )

            (* FIXME would like to also pass ?k somehow *)
            let load_par s x =
              load_par_from reads s x

            (* Here we would like to distinguish between the case
             * with and without latency,
             * but we actually don't need to,
             * since init_seir_int is a subtype of init_sir_int *)
            let init_r = ref (new latent_init)

            let out_r = ref None

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ La.specl_of init_r [
                dt ;
                seed ;
                tf ;
                s0_int
              ]
              @ La.specl_of init_r (
                if S.sp.latency then [e0_int] else []
              )
              @ La.specl_of init_r [
                i0_int ;
                r0_int ;
                (init_from load_par) ;
            ]) @ (La.specl_of init_r (E.specl ()))

            
            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name S.sp

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ " system, with the Gillespie algorithm."

            let main () =
              begin
                (* Printf.eprintf "main\n%!" ; *)
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                let seed = init#seed in
                let dt = Param.dt init in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None -> raise (Arg.Bad "-out argument is required")
                  | Some s -> s
                in
                let z0 =
                  if S.sp.latency then
                    E.of_seir init#seir
                  else
                    E.of_sir init#sir
                in
                let output = E.Csv.convert ?seed ?dt ~out par tf z0 in
                E.Gill.sim ?seed ~output par z0 tf
                (* E.Csv.output ?seed ~dt ~out (E.Gill.sim ?seed par) par tf z0 *)
              end
          end
      end


    module Prm =
      struct
        let cv = Gill.cv

        class latent_init = object
          inherit Gill.latent_init
          inherit Param.prm_h_able
        end

        module Make (S : MODEL_SPEC) =
          struct
            module E = Process.Unit.Make (S)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if S.sp.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv ; seed]
              @ (if S.sp.circular then circular () else [])
              @ (if S.sp.latency then latent () else [])
              @ base ()
            )

            let load_par ipar s =
              load_par_from reads ipar s

            (* Here we would like to distinguish between the case
             * with and without latency,
             * but we actually don't need to,
             * since init_seir_int is a subtype of init_sir_int *)
            let init_r = ref (new latent_init)

            let input_prm_r = ref None

            let out_r = ref None

            let exact_prm_r = ref false

            let output_points_r = ref false

            let output_grid_r = ref false

            let no_sim_r = ref false

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ [ La.spec_of input_prm_r init_prm_from ]
              @ [ exact_prm exact_prm_r ]
              @ [ output_points output_points_r ]
              @ [ output_grid output_grid_r ]
              @ [ no_sim no_sim_r ]
              @ La.specl_of init_r [
                dt ;
                h ;
                seed ;
                tf ;
                s0_int
              ]
              @ La.specl_of init_r (
                if S.sp.latency then [e0_int] else []
              )
              @ La.specl_of init_r [
                i0_int ;
                r0_int ;
                (init_from load_par) ;
            ]) @ (La.specl_of init_r (E.specl ()))

            
            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name S.sp

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ " system, with the Prm integrator."

            let main () =
              begin
                (* Printf.eprintf "main\n%!" ; *)
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                let dt = Param.dt init in
                let h =
                  match Param.h init with
                  | pos ->
                      pos
                  | exception Invalid_argument _ ->
                      raise (Arg.Bad "must supply a positive '-h' argument.")
                in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None ->
                      raise (Arg.Bad "missing required '-out' argument.")
                  | Some s ->
                      s
                in
                let z0 =
                  if S.sp.latency then
                    E.of_seir init#seir
                  else
                    E.of_sir init#sir
                in
                (* prm *)
                let seed = init#seed in
                let mod_sim_prm =
                  if !exact_prm_r then
                    (module E.Prm : E.SIM_PRM)
                  else
                    (module E.Prm_approx : E.SIM_PRM)
                in
                let module Sim_Prm = (val mod_sim_prm : E.SIM_PRM) in
                let outsim prm = Sim_Prm.sim_csv
                  ?seed
                  ?dt
                  ~nosim:!no_sim_r
                  ~out
                  ~output_points:!output_points_r
                  ~output_grid:!output_grid_r
                  par
                  tf
                  z0
                  prm
                in
                let input_prm = !input_prm_r in
                let time_range = (F.zero, tf) in
                let ntslices = Point.ntslices ~h tf in
                match seed, input_prm with
                | None, None | Some _, Some _ ->
                    raise (Arg.Bad
                "Either '-seed' or '-init-prm-from' must be given, but not both."
                    )
                | (Some _) as seed, None ->
                    let rng = U.rng seed in
                    let rngm = Sim_Prm.Prm.rngm ~rng E.Prm.colors in
                    let vectors = Point.vectors ~h E.Prm.colors in
                    let prm =
                      Sim_Prm.Prm.rand_draw ~rngm ~time_range ~ntslices ~vectors
                    in outsim prm
                | None, Some fname ->
                    begin match Sim_Prm.Prm.read ~time_range ~ntslices Point.v0 fname with
                    | Some prm ->
                        outsim prm
                    | None ->
                        raise (Arg.Bad "Bad file format for '-init-prm-from'")
                    | exception Sys_error _ ->
                        raise (Arg.Bad "Bad path for '-init-prm-from'.")
                    end
              end
          end
      end


    module Ode =
      struct
        let cv = float_of_string

        class immediate_init = object
          inherit init
          inherit Param.lsodable
          inherit Param.init_sir_float
        end

        class latent_init = object
          inherit init
          inherit Param.lsodable
          inherit Param.init_seir_float
        end

        module Make (S : MODEL_SPEC) =
          struct
            module Om = Process.Ode.Make (S)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if S.sp.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv]
              @ (if S.sp.circular then circular () else [])
              @ (if S.sp.latency then latent () else [])
              @ base ()
            )

            let load_par s x =
              load_par_from reads s x

            let init_r = ref (new latent_init)

            let out_r = ref None

            let dt_print_r = ref 0.

            (* let apar_r = ref Sim.Ode.Lsoda.default *)

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ La.specl_of init_r [
                dt ;
                tf ;
                s0_float ;
              ]
              @ La.specl_of init_r
                (if S.sp.latency then [e0_float] else [])
              @ La.specl_of init_r [
                i0_float ;
                r0_float ;
                (init_from load_par) ;
              ]
              @ La.specl_of init_r (lsoda_par ())
              ) @ (La.specl_of init_r (Om.specl ()))
                 (* @ (La.specl_of apar_r Sim.Ode.Lsoda.specl) *)

            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name S.sp

            let usage_msg =
                " Run a deterministic simulation of a "
              ^ model_name
              ^ "  system."

            let main () =
              begin
                Printf.eprintf "Simulate %s with ODE (Lsoda)\n" model_name ;
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                let apar = init#lsoda_par in
                let dt = Param.dt init in
                let par = (init :> param) in
                let tf = Param.tf init in
                let out =
                  match !out_r with
                  | None -> raise (Arg.Bad "-out argument is required")
                  | Some s -> s
                in
                let z0 =
                  if S.sp.latency then
                    Om.of_seir (Param.seir init)
                  else
                    Om.of_sir (Param.sir init)
                in
                let output = Om.csv_convert ?dt ~out par tf z0 in
                Om.sim ~output ~apar par z0 z0 tf
                (* Om.csv_output ~dt ~out (Om.sim ~apar par z0) par tf z0 *)
              end
          end
      end
  end


module Seq =
  struct
    module Ss = Seqsim

    class param = Param.t_seq

    class init = object
      (* Param.t_seq - Param.mutating = Param.t_unit + Param.outpool *)
      (* because mutating already in gtrable *)
      inherit Param.t_unit
      inherit Param.outpool
      (* ~ *)
      inherit Param.tfable
      inherit Param.dtable
      inherit Param.seedable
      inherit Param.gtrable
    end


    module Gill =
      struct
        let cv s =
          s |> float_of_string |> int_of_float

        class immediate_init = object
          inherit init
          inherit Param.init_sir_int
        end

        class latent_init = object
          inherit init
          inherit Param.init_seir_int
        end

        module Make (S : MODEL_SPEC) =
          struct
            module E = Process.Seq.Make (S)

            let reads = Read.Reader.(
                [s0 cv ; r0 cv ; seed]
              @ (if S.sp.circular then outpool () else [])
              @ mutating ()
              @ (if S.sp.circular then circular () else [])
              @ (if S.sp.latency then latent () else [])
              @ base ()
            )

            let load_par s x =
              load_par_from reads s x

            (* Same remark as in Cli.Unit : we don't need to differentiate *)
            let init_r = ref (new latent_init)

            let opar_r = ref Seqsim.Seqpop.Out.default

            let e_seq_o_r = ref (Some "initdenv1.fasta")

            let i_seq_o_r = ref (Some "initdenv1.fasta")

            let specl = Specs.(
                [ La.spec_of i_seq_o_r i0_seq ]
              @ (if S.sp.latency then [La.spec_of e_seq_o_r e0_seq] else [])
              @ La.specl_of init_r [
                  dt ;
                  seed ;
                  tf ;
                  s0_int ;
                  r0_int ;
                  (init_from load_par) ;
              ]) @ (La.specl_of init_r (La.map
                      (fun th -> th#with_gtr)
                      (fun th -> th#gtr)
                    Seqsim.Gtr.specl))
                 @ (La.specl_of opar_r Seqsim.Seqpop.Out.specl)
                 @ (La.specl_of init_r (E.specl ()))

            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name S.sp

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ "system, with pathogen sequences."

            let main () =
              begin
                Arg.parse specl anon_fun usage_msg ;
                let out = !opar_r in
                let init = !init_r in
                let seed = init#seed in
                let dt = Param.dt init in
                let tf = Param.tf init in
                let s, _, r = init#sir in
                let par = (init :> param) in
                Printf.printf "Reading fasta input\n%!" ;
                (* FIXME allow a tree to be specified for relations between individuals ? *)
                let i0_chan =
                  match !i_seq_o_r with
                  | None -> stdin
                  | Some s -> (open_in s)
                in
                let i0_al = Seqs.Io.assoc_of_fasta i0_chan in
                (* FIXME the genealogy from merge does not correspond to the initial roots *)
                let z0 =
                  if S.sp.latency then
                    let e0_chan =
                      match !e_seq_o_r with
                      | None -> stdin
                      | Some s -> (open_in s)
                    in
                    let e0_al = Seqs.Io.assoc_of_fasta e0_chan in
                    let o0_al = List.map (fun (_, seq) ->
                      (1, seq)
                    ) (e0_al @ i0_al) in
                    E.of_seiro (s, e0_al, i0_al, r, o0_al)
                  else
                    E.of_sir (s, i0_al, r)
                in
                Printf.printf "Input read, starting simulation\n%!" ;
                let output = E.Csv.convert ?seed ?dt ~out par tf z0 in
                E.Gill.sim ?seed ~output par z0 tf
                (* E.Csv.output ?seed ~dt ~out (E.Gill.sim ?seed par) par tf z0 *)
              end
          end
      end
  end
