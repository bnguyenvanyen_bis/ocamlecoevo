open Sig


module Trait : Pop.Sig.TRAIT

module Param :
  sig
    type t
  end

module Pop : (Pop.Sig.POP_WITH_ALL with type 'a stoch = U.rng
                               and type 'a isid = 'a
                               and type 'a trait = 'a Trait.t
                               and type 'a group = 'a Trait.group
                               and type group_data = Pop.With_id.group_data)


type seqs = (int * Seqs.t) list

type seir = {
  s : int ;
  e : seqs ;
  i : seqs ;
  r : int ;
}

type sero = {
  s : int ;
  e : seqs ;
  i : seqs ;
  igm : int ;
  igg : int ;
  r : int ;
}

type y0 = {
  passive : seir ;
  active : seir ;
  sero : sero ;
}


module Events :
  sig
    val of_tuples : y0 -> Pop.t

    module Gill :
      sig
        val sim :
          output : (Pop.t, 'b, F.closed_pos) Sim.Sig.output ->
          ?seed : int ->
          Param.t ->
          Pop.t ->
          't U.anypos ->
            'b
      end

    module Csv :
      sig
        val convert :
          ?seed : int ->
          ?dt : 's U.anypos ->
          out : string ->
          Param.t ->
          't U.anypos ->
          y0 ->
            (Pop.t, U.closed_pos * Pop.t, 'v) Sim.Sig.output
      end
  end


module Cli : functor ( ) ->
  sig
    val main : unit -> Sim.Sig.time * Pop.t
  end
