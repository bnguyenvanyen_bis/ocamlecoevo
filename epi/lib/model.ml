(* To show what it could be like,
 * but is pretty horrible *)

open Sig

module Di = Default_init


module Param =
  struct 
    open Param

    (* Here we'd like open class types instead ? *)

    class sir = object
      inherit base
    end

    class seir = object
      inherit base
      inherit latent
    end

    class sirs = object
      inherit base
      inherit circular
    end

    class seirs = object
      inherit base
      inherit latent
      inherit circular
    end

    module Init =
      struct
        class t = object
          inherit Param.tfable
          inherit Param.dtable
          inherit Param.seedable
        end

        class ['a] sir_ sir0 = object
          inherit sir
          inherit ['a] Param.init_sir sir0
          inherit t
        end

        class ['a] seir_ seir0 = object
          inherit seir
          inherit ['a] Param.init_seir seir0
          inherit t
        end

        class ['a] sirs_ sir0 = object
          inherit sirs
          inherit ['a] Param.init_sir sir0
          inherit t
        end

        class ['a] seirs_ seir0 = object
          inherit seirs
          inherit ['a] Param.init_seir seir0
          inherit t
        end
      end
  end


module Color =
  struct
    type sir = color_base

    type seir = [ color_base | color_latent ]

    type sirs = [ color_base | color_circular ]

    type seirs = [ color_base | color_latent | color_circular ]
  end


(* Add another parameter to change Param, and passed to constructors
 * through something symbolic ?
 * relating to a tuple of types for init pop values
 * --> might have only one (good) of_tuple function *)


(*
type ('par, 'colors) t =
  | Sir : (Param.sir, Color.sir)  t
  | Seir : (Param.seir, Color.seir) t
  | Sirs : (Param.sirs, Color.sirs) t
  | Seirs : (Param.seirs, Color.seirs) t
*)


type ('par, 'colors) t =
  | Sir : 'v version ->
      ('v Param.Init.sir_, Color.sir) t
  | Seir : 'v version ->
      ('v Param.Init.seir_, Color.seir) t
  | Sirs : 'v version ->
      ('v Param.Init.sirs_, Color.sirs) t
  | Seirs : 'v version ->
      ('v Param.Init.seirs_, Color.seirs) t


type exist = E : ('par, 'v) t -> exist


let name : type par c. (par, c) t -> string =
  function
  | Sir _ ->
      "SIR"
  | Seir _ ->
      "SEIR"
  | Sirs _ ->
      "SIRS"
  | Seirs _ ->
      "SEIRS"



let sir0_int = (Di.s0, Di.i0, Di.r0)

let sir0_float = (float Di.s0, float Di.i0, float Di.r0)

let seir0_int = (Di.s0, Di.e0, Di.i0, Di.r0)

let seir0_float = (float Di.s0, float Di.e0, float Di.i0, float Di.r0)


let new_ : type par c. (par, c) t -> par =
  function
  | Sir v ->
      begin match v with
      | Unit_gill ->
          new Param.Init.sir_ sir0_int
      | Unit_prm ->
          new Param.Init.sir_ sir0_int 
      | Unit_prm_approx ->
          new Param.Init.sir_ sir0_int
      | Unit_ode ->
          new Param.Init.sir_ sir0_float
      end
  | Seir v ->
      begin match v with
      | Unit_gill ->
          new Param.Init.seir_ seir0_int
      | Unit_prm ->
          new Param.Init.seir_ seir0_int
      | Unit_prm_approx ->
          new Param.Init.seir_ seir0_int
      | Unit_ode ->
          new Param.Init.seir_ seir0_float
      end
  | Sirs v ->
      begin match v with
      | Unit_gill ->
          new Param.Init.sirs_ sir0_int
      | Unit_prm ->
          new Param.Init.sirs_ sir0_int
      | Unit_prm_approx ->
          new Param.Init.sirs_ sir0_int
      | Unit_ode ->
          new Param.Init.sirs_ sir0_float
      end
  | Seirs v ->
      begin match v with
      | Unit_gill ->
          new Param.Init.seirs_ seir0_int
      | Unit_prm ->
          new Param.Init.seirs_ seir0_int
      | Unit_prm_approx ->
          new Param.Init.seirs_ seir0_int
      | Unit_ode ->
          new Param.Init.seirs_ seir0_float
      end




(* no "or" matching with GADTs -> makes this really horrible *)
let specl (type par c) (model : (par, c) t) :
  (string * par Lift_arg.spec * string) list = Specs.(
  match model with
  | Sir _ ->
      [beta ; nu ; rho]
  | Seir _ ->
      [beta ; sigma ; nu ; rho]
  | Sirs _ ->
      [betavar ; nu ; rho ; gamma ; eta ; freq ; phase]
  | Seirs _ ->
      [betavar ; sigma ; nu ; rho ; gamma ; eta ; freq ; phase]
)


let header_par (type par c) (model : (par, c) t) : string list =
  match model with
  | Sir _ ->
      ["beta" ; "nu" ; "rho" ;
       "seed" ; "dt" ; "tf" ; "s0" ; "i0" ; "r0"]
  | Seir _ ->
      ["beta" ; "nu" ; "rho" ;
       "sigma" ;
       "seed" ; "dt" ; "tf" ; "s0" ; "e0" ; "i0" ; "r0"]
  | Sirs _ ->
      ["beta" ; "nu" ; "rho" ;
       "betavar" ; "gamma" ; "eta" ; "freq" ; "phase" ;
       "seed" ; "dt" ; "tf" ; "s0" ; "i0" ; "r0"]
  | Seirs _ ->
      ["beta" ; "nu" ; "rho" ;
       "sigma" ;
       "betavar" ; "gamma" ; "eta" ; "freq" ; "phase" ;
       "seed" ; "dt" ; "tf" ; "s0" ; "e0" ; "i0" ; "r0"]


(* or it could be from the init type, that has the s0 ... *)
(* FIXME now par has the right type, we need to get the things from it *)
let line_par (type par c) (model : (par, c) t)
             ?seed ?dt (p : par) tf (s0, e0, i0, r0) =
  let sof = string_of_float in
  let dt =
    match dt with
    | None ->
        0.
    | Some x ->
        F.to_float x
  in
  let tf = F.to_float tf in
  let seed =
    match seed with
    | None ->
        "NA"
    | Some n ->
        string_of_int n
  in
  match model with
  | Sir _ ->
      [sof p#beta ; sof p#nu ; sof p#rho ;
       seed ; sof dt ; sof tf ; sof s0 ; sof i0 ; sof r0]
  | Seir _ ->
      [sof p#beta ; sof p#nu ; sof p#rho ;
       sof p#sigma ;
       seed ; sof dt ; sof tf ; sof s0 ; sof e0 ; sof i0 ; sof r0]
  | Sirs _ ->
      [sof p#beta ; sof p#nu ; sof p#rho ;
       sof p#betavar ; sof p#gamma ; sof p#eta ; sof p#freq ; sof p#phase ;
       seed ; sof dt ; sof tf ; sof s0 ; sof i0 ; sof r0]
  | Seirs _ ->
      [sof p#beta ; sof p#nu ; sof p#rho ;
       sof p#sigma ;
       sof p#betavar ; sof p#gamma ; sof p#eta ; sof p#freq ; sof p#phase ;
       seed ; sof dt ; sof tf ; sof s0 ; sof e0 ; sof i0 ; sof r0]


let output_par (type par c) (model : (par, c) t)
               ?seed ?dt (par : par) tf seir0 chan =
  let csv = Csv.to_channel chan in
  Csv.output_record csv (header_par model) ;
  Csv.output_record csv (line_par model ?seed ?dt par tf seir0) ;
  close_out chan


let infectivity (type par c) (model : (par, c) t) :
  (par -> 'a U.anyfloat -> 'b U.pos) =
  match model with
  | Sir _ ->
      Seirco.fixed_infectivity
  | Seir _ ->
      Seirco.fixed_infectivity
  | Sirs _ ->
      Seirco.var_infectivity
  | Seirs _ ->
      Seirco.var_infectivity


let colors : type par c. (par, c) t -> c list =
  function
  | Sir _ ->
      [`I_infection ; `Recovery]
  | Seir _ ->
      [`I_infection ; `Leave_exposed ; `Recovery]
  | Sirs _ ->
      [`I_infection ; `Recovery ;
       `Immunity_loss ; `O_infection]
  | Seirs _ ->
      [`I_infection ; `Leave_exposed ; `Recovery ;
       `Immunity_loss ; `O_infection]


module T = Events.Unit.T
module G = Events.Unit.G
module R = Events.Unit.Rates
module M = Events.Unit.Modifs
module P = Events.Unit.P
module Out = Events.Unit.Out


let events :
  type par c stoch.
  (par -> stoch -> 's U.pos -> 'a -> 'a) ->
  (par, c) t ->
  par ->
    (c * (
     ('t U.anyfloat -> 'a -> 'r U.pos) *
     (stoch -> 'u U.pos -> 'a -> 'a))) list =
  let recovery_ev par =
    (`Recovery, (
      R.recovery par,
      M.recovery
    ))
  in
  let ii_infection_ev mbc infc par =
    (`I_infection, (
      R.infection_of infc par,
      M.inf_inf_infection_and (mbc par)
    ))
  in
  let ie_infection_ev infc par =
    (`I_infection, (
      R.infection_of infc par,
      M.inf_exp_infection
    ))
  in
  let leave_exposed_ev mbc par =
    (`Leave_exposed, (
      R.leave_exposed par,
      M.leave_exposed_and (mbc par)
    ))
  in
  let immunity_loss_ev par =
    (`Immunity_loss, (
      R.immunity_loss par,
      M.immunity_loss
    ))
  in
  let oi_infection_ev mbc infc par =
    (`O_infection, (
      R.out_infection_of infc par,
      M.out_inf_infection_and (mbc par)
    ))
  in
  let oe_infection_ev infc par =
    (`O_infection, (
      R.out_infection_of infc par,
      M.out_exp_infection
    ))
  in
  fun maybe_case model ->
  let infctvt = infectivity model in
  match model with
  | Sir _ -> (fun par -> [
      ii_infection_ev maybe_case infctvt par ;
      recovery_ev par ;
    ])
  | Seir _ -> (fun par -> [
      ie_infection_ev infctvt par ;
      leave_exposed_ev maybe_case par ;
      recovery_ev par ;
    ])
  | Sirs _ -> (fun par -> [
      ii_infection_ev maybe_case infctvt par ;
      recovery_ev par ;
      immunity_loss_ev par ;
      oi_infection_ev maybe_case infctvt par ;
    ])
  | Seirs _ -> (fun par -> [
      ie_infection_ev infctvt par ;
      leave_exposed_ev maybe_case par ;
      recovery_ev par ;
      immunity_loss_ev par ;
      oe_infection_ev infctvt par ;
    ])



let event_sum model par =
  let evs = events M.maybe_case_rng model par in
  let _, rate_and_modifs = L.split evs in
  let rate_modifs = L.map (fun (r, m) -> SCU.combine r m) rate_and_modifs in
  SCU.add_many rate_modifs


let coal : type par c. (par, c) t -> ('s U.anypos -> 'b -> 't U.pos) =
  function
  | Sir _ ->
      R.coal_ii
  | Sirs _ ->
      R.coal_ii
  | Seir _ ->
      R.coal_ei
  | Seirs _ ->
      R.coal_ei


let reportr : type par c. (par, c) t -> ('s U.anypos -> par -> 'b -> 't U.pos) =
  function
  | Sir _ ->
      R.reportr_i
  | Sirs _ ->
      R.reportr_i
  | Seir _ ->
      R.reportr_e
  | Seirs _ ->
      R.reportr_e


let create ~nindivs =
  ignore nindivs ;
  P.create ~ngroups:7


let of_tuple (type par c) (model : (par, c) t) (s, e, i, r) =
  let z = create ~nindivs:(s + e + i + r) in
  ignore (P.add_new ~k:s z T.S) ;
  begin match model with
  | Sir _ ->
      ()
  | Sirs _ ->
      ()
  | Seir _ ->
      ignore (P.add_new ~k:e z (T.E ()))
  | Seirs _ ->
      ignore (P.add_new ~k:e z (T.E ()))
  end ;
  ignore (P.add_new ~k:i z (T.I ())) ;
  ignore (P.add_new ~k:r z T.R) ;
  z


let of_sir : type par c. (par, c) t ->  'a =
  function
  | Sir _ as model ->
      (fun (s, i, r) -> of_tuple model (s, 0, i, r))
  | Sirs _ as model ->
      (fun (s, i, r) -> of_tuple model (s, 0, i, r))
  | Seir _ ->
      invalid_arg "wrong model"
  | Seirs _ ->
      invalid_arg "wrong model"


let of_seir = of_tuple


let max_of_sir model n = of_sir model (n, n, n)


let max_of_seir model n = of_seir model (n, n, n, n)


let to_tuple z =
  let s = F.to_float (G.sus z) in
  let e = F.to_float (G.exp z) in
  let i = F.to_float (G.inf z) in
  let r = F.to_float (G.rem z) in
  (s, e, i, r)


module type SPEC =
  sig
    val model : ('par, 'c) t
  end


module Make (S : SPEC) =
  struct
    let specl () = specl S.model

    let output_par ?seed ?dt par tf seir0 chan =
      output_par S.model ?seed ?dt par tf seir0 chan

    let infectivity par t =
      infectivity S.model par t

    let coal infct z =
      coal S.model infct z

    let reportr infct par z =
      reportr S.model infct par z

    let create = create

    let of_tuple = of_tuple S.model

    let of_sir = of_sir S.model

    let of_seir = of_tuple
    
    let max_of_sir = max_of_sir S.model

    let max_of_seir = max_of_seir S.model

    let to_tuple = to_tuple

    let conv par t z =
      Out.conv infectivity coal reportr par t z

    let line par t z =
      Out.line infectivity coal reportr par t z

    module Gill =
      struct
        let events par = event_sum S.model par

        let next ?seed par =
          let ev = event_sum S.model par in
          Sim.Ctmjp.Gill.integrate ev (Util.rng seed)

        let sim ~output ?seed par =
          let next = next ?seed par in
          Sim.Loop.simulate_until ~next ~output
      end

    module Cm = Point.Colors

    module Prm_approx =
      struct
        module C = Sim.Ctmjp.Prm_approx.Make (Point)

        let colors = colors S.model

        let events par =
          let evs = events M.no_case S.model par in
          let evm =
            L.fold_left (fun m (c, rm) ->
              Cm.add c rm m
            ) Cm.empty evs
          in evm

        let sim ~output par =
          let evm = events par in
          C.simulate_until ~output evm
      end

    module Prm =
      struct
        module C = Sim.Ctmjp.Prm.Make (Point)

        (* colors should not need par ! *)
        let colors = colors S.model

        let events par =
          let evs = events M.maybe_case_pt S.model par in
          let evm =
            L.fold_left (fun map (c, (r, m)) ->
              Cm.add c (SCU.combine r m) map
            ) Cm.empty evs
          in evm

        let sim ~output par =
          let evm = events par in
          C.simulate_until ~output evm
      end

    module Csv =
      struct
        let header = Out.header

        let convert ?seed ?dt ~out par tf y0 =
          let line = line par in
          let chan = open_out (out ^ ".traj.csv") in
          let chan_par = open_out (out ^ ".par.csv") in
          output_par ?seed ?dt par tf (to_tuple y0) chan_par ;
          close_out chan_par ;
          let output = Sim.Csv.convert ?dt ~header ~line ~chan in
          let return tf yf =
            let ret = output.return tf yf in
            close_out chan ;
            ret
          in { output with return = return }


        (*
        let output sim ?seed ?dt ~out par tf y0 =
          let line = line par in
          let chan = open_out (out ^ ".traj.csv") in
          let chan_par = open_out (out ^ ".par.csv") in
          output_par ?seed ?dt par tf (to_tuple y0) chan_par ;
          let ret = Sim.Csv.output ~header ~line ~chan ?dt sim y0 tf in
          close_out chan_par ; close_out chan ;
          ret
        *)
      end

  end 


(*
let conv (type par c) (model : (par, c) t) (par : par) =
  let infctvt = infectivity model in
  let coal = coal model in
  let reportr = reportr model in
  match model with
  | Sir ->
      (fun t z -> Out.conv infctvt coal reportr par t z)
  | Seir ->
      (fun t z -> Out.conv infctvt coal reportr par t z)
  | Sirs ->
      (fun t z -> Out.conv infctvt coal reportr par t z)
  | Seirs ->
      (fun t z -> Out.conv infctvt coal reportr par t z)


let line (type par c) (model : (par, c) t) (par : par) =
  let infctvt = infectivity model in
  let coal = coal model in
  let reportr = reportr model in
  match model with
  | Sir ->
      (fun t z -> Out.line infctvt coal reportr par t z)
*)
