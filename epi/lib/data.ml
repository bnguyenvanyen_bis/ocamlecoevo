open Sig

module J = Jump


let bin_cases =
  J.int_diff


let bin_reportr r =
  r
  |> J.Pos.areas


let case_data traj =
  traj
  |> Traj.cumulative_cases
  |> J.map (fun c -> int_of_float (F.to_float c))
  |> bin_cases


let binned_reportr_data traj =
  traj
  |> Traj.reporting_rate
  |> bin_reportr


let noisy_reportr_data ~rng ovd traj =
  traj
  |> binned_reportr_data
  |> J.map (Util.rand_negbin_over ~rng ovd)


let neff_data traj =
  Traj.expected_neff traj
