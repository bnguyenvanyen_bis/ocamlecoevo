(*

open Cmdliner
open Sig


(* FIXME first try to write the corresponding function,
 * then we will see *)

(* But we don't do that tomorrow...
 * Don't control quality of Prm_approx for now
 * Try to make epifit work with Prm_approx.
 * And read (and apply) the Beast articles for what we can.
 * Then try to demonstrate something on a more complex dataset
 * mimicking the Kampong Cham setting *)


let count =
  let doc = "Repeat the message $(docv) times." in
  Arg.(value & opt int 10 & info ["c"; "count"] ~docv:"COUNT" ~doc)


let beta_opt =
  let doc = "Base infectivity." in
  Arg.(value & opt (some float) None & info ["beta"] ~docv:"BETA" ~doc)


let beta_f =
  function
  | None ->
      (fun par -> par)
  | Some x ->
      (fun par -> par#with_beta x)


let beta = Term.(const beta_f $ beta_opt)


let out =
  let doc = "Prefix of names of files to output to." in
  Arg.(value & pos 0 string "out" & info [] ~docv:"OUT" ~doc)


let tech_tag =
  let doc = "Technique to simulate the system." in
  let symbols = Arg.[
    `Gill, info ["gill"] ~doc ;
    `Prm, info ["prm"] ~doc ;
    `Prm_approx, info ["prm-approx"] ~doc ;
    `Ode, info ["ode"] ~doc ;
  ]
  in
  Arg.(value & vflag `Ode symbols)


let model_tag = Model.(
  let doc = "Model of transmission." in
  let symbols = Arg.[
    `Sir, info ["sir"] ~doc ;
    `Seir, info ["seir"] ~doc ;
    `Sirs, info ["sirs"] ~doc ;
    `Seirs, info ["seirs"] ~doc ;
  ]
  in
  Arg.(value & vflag `Sir symbols)
)


let model_f tech_tag model_tag =
  let tech =
    match tech_tag with
    | `Gill ->
        V Unit_gill
    | `Prm ->
        V Unit_prm
    | `Prm_approx ->
        V Unit_prm_approx
    | `Ode ->
        V Unit_ode
  in
  match tech with V v ->
  match model_tag with
  | `Sir ->
      Model.E (Sir v)
  | `Seir ->
      Model.E (Seir v)
  | `Sirs ->
      Model.E (Sirs v)
  | `Seirs ->
      Model.E (Seirs v)


let model = Term.(const model_f $ tech_tag $ model_tag)


(* We want to use different terms depending on tech and model...
 * How can we achieve this ?
 * What is the general function that we can write ?
 * Can we have *)


let simulate (type par c a) emodel from tf dt out =
  (* depending on model, create the right starting parameter value,
   * and modify it by reading the appropriate way from 'from',
   * and from the command line.
   * That second part sounds difficult ? *)
  match emodel with Model.E model ->
    let par = Model.new_ model in
    (* read from 'from' if it's some *)
    let par = Model.read_from model from in
    (* read what you should from the command line *)
    (* the only function that relates to that is eval_choices
     * but it doesn't define a term so we can't do it twice.
     * The list of arguments mostly depends on model,
     * but also on technique (for ode_apar and such)
     * (basically it defines the simulation function to use)
     * Or we build one term per model_x_tech choice
     * We just need the right read functions.
     * But it's not very elegant.
     * We would much prefer a function to build a term which,
     * upon evaluation, depending on the evaluation of the first,
     * will evaluate one of the others,
     * but I think the only term combinator, ($), always evaluates everything.
     *)

*)
