open Sig

module SCU = Sim.Ctmjp.Util


module Unit =
  struct
    module E = Events.Unit
    module T = E.T
    module P = E.P
    module G = E.G
    module R = E.Rates
    module M = E.Modifs
    module Out = E.Out

    let specl (sp : model_spec) = Specs.(
        [host_birth ; host_death]
      @ [(if sp.circular then betavar else beta)]
      @ (if sp.latency then [sigma] else [])
      @ [nu ; rho]
      @ (if sp.circular then [gamma ; eta ; freq ; phase] else [])
    )


    (* FIXME use Util.Csv.output *)
    let output_par (sp : model_spec) c ?seed ?dt p tf (s0, e0, i0, r0) =
      let pf = Printf.fprintf in
      let dt = match dt with
        | None -> 0.
        | Some x -> F.to_float x
      in
      let tf = F.to_float tf in
      let seed = match seed with
        | None -> "NA"
        | Some n -> string_of_int n
      in
      pf c "host_birth,host_death," ;
      pf c "beta,nu,rho" ;
      if sp.latency then pf c ",sigma" ;
      if sp.circular then pf c ",betavar,gamma,eta,freq,phase" ;
      pf c ",seed,dt,tf,s0,e0,i0,r0" ;
      pf c "\n" ;
      (* ********************** *)
      pf c "%f,%f," p#host_birth p#host_death ;
      pf c "%f,%f,%f" p#beta p#nu p#rho ;
      if sp.latency then pf c ",%f" p#sigma ;
      if sp.circular then pf c ",%f,%f,%f,%f,%f"
        p#betavar p#gamma p#eta p#freq p#phase ;
      pf c ",%s,%f,%f,%f,%f,%f,%f"
            seed dt tf s0 e0 i0 r0 ;
      pf c "\n"


    let infectivity (sp : model_spec) =
      if sp.circular then
        Seirco.var_infectivity
      else
        Seirco.fixed_infectivity


    (* FIXME problem of synchronization with next function *)
    let colors sp =
        (if sp.latency then
           [`I_infection ; `Leave_exposed]
         else
           [`I_infection])
      @ [`Recovery]
      @ (if sp.circular then [`Immunity_loss] else [])
      @ (if sp.circular then [`O_infection] else [])
      @ (if sp.latency then
           [`S_birth ; `S_death ; `E_death ; `I_death ; `R_death]
         else
           [`S_birth ; `S_death ; `I_death ; `R_death])


    let color_groups sp =
        (if sp.latency then
           [[`I_infection] ; [`Leave_exposed]]
        else
           [[`I_infection]])
      @ [[`Recovery]]
      @ (if sp.circular then [[`Immunity_loss]] else [])
      @ (if sp.circular then [[`O_infection]] else [])
      @ (if sp.latency then
           [[`S_birth ; `S_death ; `E_death ; `I_death ; `R_death]]
         else
           [[`S_birth ; `S_death ; `I_death ; `R_death]])


    let events (type b) (maybe_case : ('a -> b -> 'c U.pos -> 'd)) (sp : model_spec) =
      let infctvt = infectivity sp in
      let inf_infection_modif par =
        if sp.latency then
          M.inf_exp_infection
          (* M.infection_nonid (P.indiv_from_group T.Exp) *)
        else
          M.inf_inf_infection_and (maybe_case par)
          (* M.case_inf_infection_nonid par *)
      in
      let out_infection_modif par =
        if sp.latency then
          M.out_exp_infection
        else
          M.out_inf_infection_and (maybe_case par)
      in
      let infection_event par =
        [`I_infection,
         ((R.infection_of infctvt par), (inf_infection_modif par))]
      in
      let leave_exposed_event par =
        if sp.latency then
          [`Leave_exposed,
           ((R.leave_exposed par),
            (M.leave_exposed_and (maybe_case par)))]
        else
          []
      in
      let recovery_event par =
        [`Recovery,
         ((R.recovery par), M.recovery)]
      in
      let immunity_loss_event par =
        if sp.circular then
          [`Immunity_loss,
           ((R.immunity_loss par), M.immunity_loss)]
        else
          []
      in
      let out_infection_event par =
        if sp.circular then
          [`O_infection,
           ((R.out_infection_of infctvt par), (out_infection_modif par))]
        else
          []
      in
      let host_birth_event par =
        [`S_birth,
         ((R.host_birth par), M.host_birth)]
      in
      let sus_death_event par =
        [`S_death,
         ((R.sus_host_death par), M.sus_host_death)]
      in
      let exp_death_event par =
        if sp.latency then
          [`E_death,
           ((R.exp_host_death par), M.exp_host_death)]
        else
          []
      in
      let inf_death_event par =
        [`I_death,
         ((R.inf_host_death par), M.inf_host_death)]
      in
      let rem_death_event par =
        [`R_death,
         ((R.rem_host_death par), M.rem_host_death)]
      in
      let events par =
          (infection_event par)
        @ (leave_exposed_event par)
        @ (recovery_event par)
        @ (immunity_loss_event par)
        @ (out_infection_event par)
        @ (host_birth_event par)
        @ (sus_death_event par)
        @ (exp_death_event par)
        @ (inf_death_event par)
        @ (rem_death_event par)
      in
      events


    let event_sum sp par =
      let evs = events M.maybe_case_rng sp par in
      let _, rate_and_modifs = L.split evs in
      let rate_modifs = L.map (fun (r, m) -> SCU.combine r m) rate_and_modifs in
      SCU.add_many rate_modifs


    module Cm = BatMap.Make (Color)


    module Make (S : MODEL_SPEC) =
      struct
        let specl () = specl S.sp

        let output_par chan ?seed ?dt par tf y0 =
          output_par S.sp chan ?seed ?dt par tf y0

        let infectivity par t = infectivity S.sp par t

        let coal, reportr = R.(
          if S.sp.latency then
            coal_ei, reportr_e
          else
            coal_ii, reportr_i
        )

        let create ~nindivs =
          (* never more than two indivs in one event -> mem:3 
           * 6 groups -> ngroups:7 *)
          ignore nindivs ;
          P.create ~ngroups:7

        let of_tuple (s, e, i, r) =
          let z = create ~nindivs:(s + e + i + r) in
          ignore (P.add_new ~k:s z T.S) ;
          if S.sp.latency then ignore (P.add_new ~k:e z (T.E ())) ;
          ignore (P.add_new ~k:i z (T.I ())) ;
          ignore (P.add_new ~k:r z T.R) ;
          z

        let e_i_equilibrium par e_plus_i =
          if not S.sp.latency then
            assert false
          else
            let death = Param.host_death par in
            let nu = Param.nu par in
            let sigma = Param.sigma par in
            let coeff = F.Pos.Op.((death + nu) / sigma) in
            let i_eq = F.Pos.Op.(e_plus_i / (coeff + F.one)) in
            let e_eq = F.Pos.mul coeff i_eq in
            (e_eq, i_eq)

        let sepir_to_seir par (s, e_plus_i, r) =
          let e, i = e_i_equilibrium par e_plus_i in
          (s, e, i, r)

        (* call this only when you should *)
        let of_sir (s, i, r) = of_tuple (s, 0, i, r)

        let of_seir = of_tuple

        let max_of_sir n = of_sir (n, n, n)

        let max_of_seir n = of_seir (n, n, n, n)

        let to_tuple z =
          let s = F.to_float (G.sus z) in
          let e = F.to_float (G.exp z) in
          let i = F.to_float (G.inf z) in
          let r = F.to_float (G.rem z) in
          (s, e, i, r)

        let conv par t z =
          Out.conv infectivity coal reportr par t z

        let line par t z =
          Out.line infectivity coal reportr par t z

        module Csv =
          struct
            let header = Out.header

            let convert ?seed ?dt ~out par tf y0 =
              let line = line par in
              let chan = open_out (out ^ ".traj.csv") in
              let chan_par = open_out (out ^ ".par.csv") in
              output_par chan_par ?seed ?dt par tf (to_tuple y0) ;
              close_out chan_par ;
              let output = Sim.Csv.convert ?dt ~header ~line ~chan in
              let return tf yf =
                let ret = output.return tf yf in
                close_out chan ;
                ret
              in
              { output with return = return }
          end

        module Cadlag =
          struct
            let convert ?dt par =
              let conv = conv par in
              Sim.Cadlag.convert ?dt ~conv
          end

        module Gill =
          struct
            let events par = event_sum S.sp par

            let next ?seed par =
              let ev = event_sum S.sp par in
              Sim.Ctmjp.Gill.integrate ev (Util.rng seed)

            let sim ~output ?seed par =
              let next = next ?seed par in
              Sim.Loop.simulate_until ~next ~output
          end

        module Cm = Point.Colors

        module type SIM_PRM =
          sig
            module Prm : (Sim.Sig.PRM with type color = Color.t
                                       and type point = Color.t Sim.Prm.point)

            val sim_csv :
              ?seed:int ->
              ?dt:('s U.anypos) ->
              nosim:bool ->
              out:string ->
              output_points:bool ->
              output_grid:bool ->
              Param.t_unit ->
              't U.anypos ->
              R.t ->
              Prm.t ->
                Sim.Sig.time * R.t
          end

        let no_sim ~(output : ('a, 'b, 'c) U.Out.t) z0 tf =
          output.start F.zero z0 ;
          output.return tf z0

        (* needs to be before Prm because it shadows Epi.Prm *)
        module Prm_approx =
          struct
            module C = Sim.Ctmjp.Prm_approx.Make (Point)

            module Prm = Prm_color

            let colors = colors S.sp

            let color_groups = color_groups S.sp

            let events par =
              let evs = events M.no_case S.sp par in
              let evm =
                L.fold_left (fun m (c, rm) ->
                  Cm.add c rm m
                ) Cm.empty evs
              in evm

            let sim ~output par =
              let evm = events par in
              C.simulate_until ~output evm

            let csv_convert ?seed ?dt ~output_points ~output_grid ~out par tf y0 prm =
              let output = Csv.convert ?seed ?dt ~out par tf y0 in
              let return tf yf =
                (* Here we're counting on prm being mutable *)
                let ret = output.return tf yf in
                if output_points then begin
                  let chan_pts = U.Csv.open_out (out ^ ".prm.points.csv") in
                  Prm_color.output_points chan_pts prm ;
                  U.Csv.close_out chan_pts
                end ;
                if output_grid then begin
                  let chan_grid = U.Csv.open_out (out ^ ".prm.grid.csv") in
                  Prm_color.output_grid chan_grid prm ;
                  U.Csv.close_out chan_grid
                end ;
                ret
              in
              { output with return = return }

            let sim_csv ?seed ?dt ~nosim ~out ~output_points ~output_grid (par : Param.t_unit) (tf : 'a U.anypos) z0 prm =
              let output = csv_convert
                ?seed
                ?dt
                ~output_points
                ~output_grid
                ~out
                par
                (F.narrow tf)
                z0
                prm
              in
              if nosim then
                no_sim ~output z0 (F.Pos.narrow tf)
              else
                sim ~output par z0 prm
          end

        module Prm =
          struct
            module C = Sim.Ctmjp.Prm.Make (Point)

            module Prm = Prm_time

            (* colors should not need par ! *)
            let colors = colors S.sp

            let color_groups = color_groups S.sp

            let events par =
              let evs = events M.maybe_case_pt S.sp par in
              let evm =
                L.fold_left (fun map (c, (r, m)) ->
                  Cm.add c (SCU.combine r m) map
                ) Cm.empty evs
              in evm

            let sim ~output par =
              let evm = events par in
              C.simulate_until ~output evm

            let csv_convert ?seed ?dt ~output_points ~output_grid ~out par tf y0 prm =
              let output = Csv.convert ?seed ?dt ~out par tf y0 in
              let return tf yf =
                (* Here we're counting on prm being mutable *)
                let ret = output.return tf yf in
                if output_points then begin
                  let chan_pts = U.Csv.open_out (out ^ ".prm.points.csv") in
                  Prm_time.output_points chan_pts prm ;
                  U.Csv.close_out chan_pts
                end ;
                if output_grid then begin
                  let chan_grid = U.Csv.open_out (out ^ ".prm.grid.csv") in
                  Prm_time.output_grid chan_grid prm ;
                  U.Csv.close_out chan_grid
                end ;
                ret
              in
              { output with return = return }

            let sim_csv ?seed ?dt ~nosim ~out ~output_points ~output_grid par tf z0 prm =
              let output = csv_convert
                ?seed
                ?dt
                ~output_points
                ~output_grid
                ~out
                par
                (F.narrow tf)
                z0
                prm
              in
              if nosim then
                no_sim ~output z0 (F.Pos.narrow tf)
              else
                sim ~output par z0 prm
          end
      end
  end


(* could be under Unit, but I put it on its own *)
module Ode =
  struct
    module Make (S : MODEL_SPEC) =
      struct
        module E = Unit.Make (S)

        module P = Getsets.Make (struct
          include Trait.Unit
          let group_to_int : type a. a group -> int =
            if S.sp.latency then
              let fail () = invalid_arg "not SEIR" in
              function
              | Sus -> 1
              | Exp -> 2
              | Inf -> 3
              | Rem -> 4
              | Cas -> fail ()
              | Out -> fail ()
            else
              let fail () = invalid_arg "not SIR" in
              function
              | Sus -> 1
              | Inf -> 2
              | Rem -> 3
              | Exp -> fail ()
              | Cas -> fail ()
              | Out -> fail ()
        end)

        module O = Ode.Make (P)

        open P

        let dim = if S.sp.latency then 4 else 3

        (* eta-expansion *)
        let infectivity par t = Unit.infectivity S.sp par t

        let coal =
          if S.sp.latency then
            O.R.coal_ei
          else
            O.R.coal_ii

        let reportr =
          if S.sp.latency then
            O.R.reportr_e
          else
            O.R.reportr_i

        (* FIXME tot will vary *)
        let field ~tot par =
          let death = Param.host_death par in
          let sigma = Param.sigma par in
          let nu = Param.nu par in
          let gamma = Param.gamma par in
          let eta = Param.eta par in
          let infity = infectivity par in
          if (not S.sp.latency) && (not S.sp.circular) then
            (* SIR *)
            let f ~z t y =
              let ytot = (y, tot) in
              let birth = O.R.host_birth par t ytot in
              let betas = O.R.infect (infity t) ytot in
              let s = Get.sus ytot in
              let i = Get.inf ytot in
              let r = Get.inf ytot in
              Set.sus z F.Op.(~- betas * i - death * s + birth) ;
              Set.inf z F.Op.((betas - nu - death) * i) ;
              Set.rem z F.Op.(nu * i - death * r) ;
              z
            in f
          else if S.sp.latency && (not S.sp.circular) then
            (* SEIR *)
            let f ~z t y =
              let ytot = (y, tot) in
              let birth = O.R.host_birth par t ytot in
              let betas = O.R.infect (infity t) (y, tot) in
              let s = Get.sus ytot in
              let e = Get.exp ytot in
              let i = Get.inf ytot in
              let r = Get.inf ytot in
              Set.sus z F.Op.(~- betas * i - death * s + birth) ;
              Set.exp z F.Op.(betas * i - (sigma + death) * e) ;
              Set.inf z F.Op.(sigma * e - (nu + death) * i) ;
              Set.rem z F.Op.(nu * i - death * r) ;
              z
            in f
          else if (not S.sp.latency) && S.sp.circular then
            (* SIRS *)
            let f ~z t y =
              let ytot = (y, tot) in
              let birth = O.R.host_birth par t ytot in
              let betas = O.R.infect (infity t) ytot in
              let s = Get.sus ytot in
              let i = Get.inf ytot in
              let i' = F.Pos.Op.(i + eta) in
              let r = Get.rem ytot in
              Set.sus z F.Op.(~- betas * i' + gamma * r - death * s + birth) ;
              Set.inf z F.Op.(betas * i' - (nu + death) * i) ;
              Set.rem z F.Op.(nu * i - (gamma + death) * r) ;
              z
            in f
          else
            (* SEIRS *)
            let f ~z t y =
              let ytot = (y, tot) in
              let birth = O.R.host_birth par t ytot in
              let betas = O.R.infect (infity t) ytot in
              let s = Get.sus ytot in
              let e = Get.exp ytot in
              let i = Get.inf ytot in
              let i' = F.Pos.Op.(i + eta) in
              let r = Get.rem ytot in
              Set.sus z F.Op.(~- betas * i' + gamma * r - death * s + birth) ;
              Set.exp z F.Op.(betas * i' - (sigma + death) * e) ;
              Set.inf z F.Op.(sigma * e - (nu + death) * i) ;
              Set.rem z F.Op.(nu * i - (gamma + death) * r) ;
              z
            in f

        let sepir_to_seir = E.sepir_to_seir

        let of_sir ?z (s, i, r) =
          let z =
            match z with
            | None ->
                Lac.Vec.make0 dim
            | Some z ->
                z
          in
          (* FIXME not actually the right type *)
          Set.sus z s ;
          Set.inf z i ;
          Set.rem z r ;
          z

        let of_seir ?z (s, e, i, r) =
          let z =
            match z with
            | None ->
                Lac.Vec.make0 dim
            | Some z ->
                z
          in
          (* FIXME again *)
          Set.sus z s ;
          Set.exp z e ;
          Set.inf z i ;
          Set.rem z r ;
          z

        let max_of_sir x =
          of_sir (x, x, x)

        let max_of_seir x =
          of_seir (x, x, x, x)

        let to_sir z =
          let (s, _, i, r) = to_tuple z in
          (s, i, r)

        let to_seir = to_tuple

        let total y = 
          F.Pos.of_float (Lac.Vec.sum y)

        let and_total y = (y, total y)

        let sim ~output ?apar par y0 =
          let tot = total y0 in
          let f = field ~tot par in
          let next = Sim.Ode.Lsoda.integrate ?apar
            (O.in_domain ~n:dim) (O.to_domain dim) f y0
          in
          Sim.Loop.simulate_until ~output ~next

        let conv par t y =
          O.Out.conv infectivity coal reportr par t (and_total y)

        let line par t y =
          O.Out.line infectivity coal reportr par t (and_total y)

        let csv_convert ?dt ~out par tf y0 =
          let header = Out.header in
          let line = line par in
          let chan = open_out (out ^ ".traj.csv") in
          let chan_par = open_out (out ^ ".par.csv") in
          E.output_par chan_par par ?dt tf (to_tuple y0) ;
          let output = Sim.Csv.convert ?dt ~header ~line ~chan in
          let return tf yf =
            let ret = output.return tf yf in
            close_out chan_par ; close_out chan ;
            ret
          in { output with return = return }

        module Cadlag =
          struct
            let convert ?dt par =
              let conv = conv par in
              Sim.Cadlag.convert ?dt ~conv
          end

        let specl = E.specl
      end
  end


module Seq =
  struct
    module Mut = Seqsim.Mut

    module E = Events.Seq
    module T = E.T
    module P = E.P
    module G = E.G
    module R = E.Rates
    module M = E.Modifs
    module Out = E.Out

    type pop = E.P.t
             * (int * (Seqs.t * (float * Seqs.Diff.t) list)) list


    let empty_record = [(0., Seqs.Sig.Not)]


    let specl (sp : model_spec) = Specs.(
        [host_birth ; host_death]
      @ [(if sp.circular then betavar else beta)]
      @ (if sp.latency then [sigma] else [])
      @ [nu ; rho]
      @ (if sp.circular then
           [gamma ; eta ; freq ; phase ; p_outseed ; r_outdeath]
         else
           [])
      @ [p_keep]
      @ (mut ())
    )


    (* FIXME use Util.Csv.output *)
    let output_par (sp : model_spec) c ?seed ?dt p tf (s0, e0, i0, r0) =
      let pf = Printf.fprintf in
      let dt = match dt with
        | None -> 0.
        | Some x -> F.to_float x
      in
      let tf = F.to_float tf in
      let seed = match seed with
        | None -> "NA"
        | Some n -> string_of_int n
      in
      pf c "host_birth,host_death," ;
      pf c "beta,nu,rho" ;
      if sp.latency then pf c ",sigma" ;
      if sp.circular then pf c ",betavar,gamma,eta,freq,phase" ;
      pf c "," ; Mut.out_par_head c ;
      pf c ",p_keep" ;
      if sp.circular then pf c ",p_outseed,r_outdeath" ;
      pf c ",seed,dt,tf,s0,e0,i0,r0" ;
      pf c "\n" ;
      (* ********************** *)
      pf c "%f,%f," p#host_birth p#host_death ;
      pf c "%f,%f,%f" p#beta p#nu p#rho ;
      if sp.latency then pf c ",%f" p#sigma ;
      if sp.circular then pf c ",%f,%f,%f,%f,%f"
        p#betavar p#gamma p#eta p#freq p#phase ;
      pf c "," ; Mut.out_par_values c p#evo ;
      pf c ",%f" p#p_keep ;
      if sp.circular then pf c ",%f,%f"
        p#p_outseed p#r_outdeath ;
      pf c ",%s,%f,%f,%f,%f,%f,%f"
            seed dt tf s0 e0 i0 r0 ;
      pf c "\n"


    let infectivity (sp : model_spec) =
      if sp.circular then
        Seirco.var_infectivity
      else
        Seirco.fixed_infectivity


    let event (sp : model_spec) =
      let infctvt = infectivity sp in
      let inf_infection_modif par =
        if sp.latency then
          M.inf_exp_infection
        else
          M.case_inf_inf_infection par
      in
      let recovery_modif par =
        if sp.circular then
          M.record_outseed_recovery par
        else
          M.record_recovery par
      in
      let out_infection_modif par =
        if sp.latency then
          M.out_exp_infection
        else
          M.case_out_inf_infection par
      in
      let infection_event par =
        [`I_infection,
         SCU.combine (R.infection_of infctvt par) (inf_infection_modif par)]
      in
      let leave_exposed_event par =
        if sp.latency then
          [`Leave_exposed,
           SCU.combine (R.leave_exposed par) (M.case_leave_exposed par)]
        else
          []
      in
      let recovery_event par =
        [`Recovery,
         SCU.combine (R.recovery par) (recovery_modif par)]
      in
      let immunity_loss_event par =
        if sp.circular then
          [`Immunity_loss,
           SCU.combine (R.immunity_loss par) M.immunity_loss]
        else
          []
      in
      let inf_subst_mutation_event par =
        [`I_subst,
         SCU.combine
          (R.mutation T.Inf Mut.subst_rate par)
          (M.mutation T.Inf Mut.subst_modif par)]
      in
      let inf_rep_mutation_event par =
        [`I_rep,
         SCU.combine
          (R.mutation T.Inf Mut.rep_subseq_rate par)
          (M.mutation T.Inf Mut.rep_subseq_modif par)]
      in
      let inf_del_mutation_event par =
        [`I_del,
         SCU.combine
          (R.mutation T.Inf Mut.del_subseq_rate par)
          (M.mutation T.Inf Mut.del_subseq_modif par)]
      in
      let exp_subst_mutation_event par =
        if sp.latency then
          [`E_subst,
           SCU.combine
            (R.mutation T.Exp Mut.subst_rate par)
            (M.mutation T.Exp Mut.subst_modif par)]
        else
          []
      in  
      let exp_rep_mutation_event par =
        if sp.latency then
          [`E_rep,
           SCU.combine
            (R.mutation T.Exp Mut.rep_subseq_rate par)
            (M.mutation T.Exp Mut.rep_subseq_modif par)]
        else
          []
      in
      let exp_del_mutation_event par =
        if sp.latency then
          [`E_del,
           SCU.combine
            (R.mutation T.Exp Mut.del_subseq_rate par)
            (M.mutation T.Exp Mut.del_subseq_modif par)]
        else
          []
      in
      let out_infection_event par =
        if sp.circular then
          [`O_infection,
           SCU.combine
            (R.out_infection_of infctvt par)
            (out_infection_modif par)
          ]
        else
          []
      in
      let out_death_event par =
        if sp.circular then
          [`O_death, SCU.combine (R.out_death par) (M.out_death)]
        else
          []
      in
      let host_birth_event par =
        [`S_birth,
         SCU.combine (R.host_birth par) M.host_birth]
      in
      let sus_death_event par =
        [`S_death,
         SCU.combine (R.sus_host_death par) M.sus_host_death]
      in
      let exp_death_event par =
        if sp.latency then
          [`E_death,
           SCU.combine (R.exp_host_death par) M.exp_host_death]
        else
          []
      in
      let inf_death_event par =
        [`I_death,
         SCU.combine (R.inf_host_death par) M.inf_host_death]
      in
      let rem_death_event par =
        [`R_death,
         SCU.combine (R.rem_host_death par) M.rem_host_death]
      in
      let event par =
        let events = 
            (infection_event par)
          @ (leave_exposed_event par)
          @ (recovery_event par)
          @ (immunity_loss_event par)
          @ (inf_subst_mutation_event par)
          @ (inf_rep_mutation_event par)
          @ (inf_del_mutation_event par)
          @ (exp_subst_mutation_event par)
          @ (exp_rep_mutation_event par)
          @ (exp_del_mutation_event par)
          @ (out_infection_event par)
          @ (out_death_event par)
          @ (host_birth_event par)
          @ (sus_death_event par)
          @ (exp_death_event par)
          @ (inf_death_event par)
          @ (rem_death_event par)
        in
        let _, rate_modifs = L.split events in
        SCU.add_many rate_modifs
      in event


    module Make (S : MODEL_SPEC) =
      struct
        let specl () = specl S.sp

        let output_par chan ?seed ?dt par tf y0 =
          output_par S.sp chan ?seed ?dt par tf y0

        let infectivity par t = infectivity S.sp par t

        let coal, reportr = R.(
          if S.sp.latency then
            coal_ei, reportr_e
          else
            coal_ii, reportr_i
        )

        (* FIXME Put this in Pop.Events *)
        let add_many z0 =
          L.iter (fun (k, state) ->
            ignore (P.add_new ~k z0 state)
          )

        let create ~nindivs =
          (* never more than two indivs in one event -> mem:3
           * 6 groups -> ngroups:7 *)
          P.create ~mem:3 ~ngroups:7 ~scores:[Trait.Seq.score] ~nindivs

        let of_tuple (s, e, i, r, o) =
          let er = empty_record in
          let len = L.length in
          let z0 = create ~nindivs:(s + len e + len i + r + len o)
          in
          ignore (P.add_new ~k:s z0 T.S) ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.E (seq, er)))) @@ e ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.I (seq, er)))) @@ i ;
          ignore (P.add_new ~k:r z0 T.R) ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.O (seq, er)))) @@ o ;
          z0

        let of_sir (s, i, r) = of_tuple (s, [], i, r, [])

        let of_seiro = of_tuple

        let to_tuple z =
          let s = F.to_float (E.G.sus z) in
          let e = F.to_float (E.G.exp z) in
          let i = F.to_float (E.G.inf z) in
          let r = F.to_float (E.G.rem z) in
          (s, e, i, r)

        let output_seqs chan record =
          let fmap (i, (t_death, trait)) =
            let seq, _ = Trait.Seq.payload trait in
            (i, t_death, seq)
          in
          record
          |> Tree.Genealogy.Intbl.to_list
          |> L.map fmap
          |> Seqs.Io.stamped_fasta_of_assoc chan

        let line par t z =
          R.Out.line infectivity coal reportr par t z

        let conv par t z =
          R.Out.conv infectivity coal reportr par t z

        module Gill =
          struct
            let next ?seed par =
              Sim.Ctmjp.Gill.integrate (event S.sp par) (Util.rng seed)

            let sim ~output ?seed par =
              let next = next ?seed par in
              Sim.Loop.simulate_until ~next ~output
          end


        module Csv =
          struct
            let header = Out.header

            open Seqsim.Seqpop
            
            let convert ?seed ?dt ~out par tf z0 =
              let chan = out.csvchan in
              output_par out.parchan ?seed ?dt par tf (to_tuple z0) ;
              Out.close_par out ;
              let line = line par in
              let output = Sim.Csv.convert ?dt ~header ~line ~chan in
              let return tf zf =
                let ret = output.return tf zf in
                P.remove_erase_living (F.to_float tf) zf ;
                let genealogy = P.to_tree_genealogy zf in
                (* FIXME to_recorded_death_traits instead ? *)
                let record = P.to_recorded_leaf_death_traits zf in
                output_seqs out.seqchan record ;
                Out.output_trees_from_genealogy out.treechan genealogy ;
                Out.close_csv out ; Out.close_seq out ; Out.close_tree out ;
                ret
              in { output with return = return }
          end
      end
  end
