(** SEIRS models *)
open Sig


let model_spec = {
  latency = true ;
  circular = true ;
}


module Model =
  struct
    let sp = model_spec
  end
