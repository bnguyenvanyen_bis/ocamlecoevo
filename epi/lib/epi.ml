(** @canonical Epi.Sig *)
module Sig = Sig

(** @canonical Epi.Seirco *)
module Seirco = Seirco

(** @canonical Epi.Param *)
module Param = Param

(** @canonical Epi.Color *)
module Color = Color

(** @canonical Epi.Point *)
module Point = Point

(** @canonical Epi.Prm_time *)
module Prm_time = Prm_time

(** @canonical Epi.Prm_color *)
module Prm_color = Prm_color

(** @canonical Epi.Default_init *)
module Default_init = Default_init

(** @canonical Epi.Specs *)
module Specs = Specs

(** @canonical Epi.Out *)
module Out = Out

(** @canonical Epi.Read *)
module Read = Read

(** @canonical Epi.Cli *)
module Cli = Cli

(** @canonical Epi.Process *)
module Process = Process

(** @canonical Epi.Sir *)
module Sir = Sir

(** @canonical Epi.Seir *)
module Seir = Seir

(** @canonical Epi.Sirs *)
module Sirs = Sirs

(** @canonical Epi.Seirs *)
module Seirs = Seirs

(** @canonical Epi.Panic *)
module Panic = Panic

(** @canonical Epi.Traj *)
module Traj = Traj

(** @canonical Epi.Data *)
module Data = Data

(** @canonical Epi.Map_csv *)
module Map_csv = Epi__Map_csv
