open Sig


module Reader =
  struct
    type 'a t = {
      column : string ;
      optional : bool ;
      read : string -> 'a -> 'a ;
    }


    let map ~get ~set rdr =
      { rdr with read = fun s y ->
          let x = get y in
          let x' = rdr.read s x in
          set x' y
      }

    let load reads row x =
      L.fold_left (fun y { column ; optional ; read } ->
        let f =
          match Util.Csv.Row.find row column with
          | None ->
              if optional then
                (fun y -> y)
              else
                invalid_arg "row missing required column"
          | Some s ->
              read s
        in f y
      ) x reads

    (*
    let inject reads col =
      let valid = L.filter (fun { column ; _ } -> (col = column)) reads in
      match valid with
      | [] ->
          None
      | read :: [] ->
          Some read
      | _ ->
          invalid_arg "reads contains duplicate columns"

    let load_opt reads row x =
      Util.Csv.read_row
        ~inject:(inject reads)
        ~columns
    *)

    let load_opt reads row x =
      let opt_reads = L.map (fun rdr -> { rdr with optional = true }) reads in
      load opt_reads row x

    let fos = float_of_string

    let ios = int_of_string

    let fpos x =
      x
      |> F.Pos.of_string
      |> Util.Option.some
      |> F.to_float

    let fpros x =
      x
      |> F.Proba.of_string
      |> Util.Option.some
      |> F.to_float

    let req column read = {
      column ;
      optional = false ;
      read ;
    }

    let req_pos : type a. string -> (a -> float -> a) -> a t =
      fun column using -> {
      column ;
      optional = false ;
      read = fun s th -> using th (fpos s) ;
    }

    let birth () =
      req_pos "host_birth" (fun th x -> th#with_host_birth x)

    let death () =
      req_pos "host_death" (fun th -> th#with_host_death)

    let beta () =
      req_pos "beta" (fun th -> th#with_beta)

    let betavar () =
      req_pos "betavar" (fun th -> th#with_betavar)

    let phase () =
      req_pos "phase" (fun th -> th#with_phase)

    let sigma () =
      req_pos "sigma" (fun th -> th#with_sigma)

    let nu () =
      req_pos "nu" (fun th -> th#with_nu)

    let gamma () =
      req_pos "gamma" (fun th -> th#with_gamma)

    let rho () =
      req_pos "rho" (fun th -> th#with_rho)

    let eta () =
      req_pos "eta" (fun th -> th#with_eta)

    let s0 cv = {
      column = "s0" ;
      optional = false ;
      read = fun s th -> th#with_s0 (cv s) ;
    }

    let e0 cv = {
      column = "e0" ;
      optional = false ;
      read = fun s th -> th#with_e0 (cv s) ;
    }

    let i0 cv = {
      column = "i0" ;
      optional = false ;
      read = fun s th -> th#with_i0 (cv s) ;
    }

    let r0 cv = {
      column = "r0" ;
      optional = false ;
      read = fun s th -> th#with_r0 (cv s) ;
    }

    (* FIXME the optional argument thing is a bit dumb *)
    let seed = {
      column = "seed" ;
      optional = true ;
      read = fun s th -> th#with_seed (Some (ios s)) ;
    }

    let tf () =
      req_pos "tf" (fun th -> th#with_tf)

    let dt () =
      req_pos "dt" (fun th -> th#with_dt)

    let base () = [
      birth () ;
      death () ;
      beta () ;
      nu () ;
      rho () ;
      tf () ;
      dt () ;
    ]

    let latent () = [
      sigma ()
    ]


    let circular () = [
      betavar () ;
      phase () ;
      gamma () ;
      eta () ;
    ]

    module Mutating =
      struct
        module Snp = Seqsim.Snp
        module Mut = Seqsim.Mut

        let direct column using = {
          column ;
          optional = false ;
          read = (fun s evo -> using evo (fpos s)) ;
        }

        let mu_del = direct "mu_del" Mut.with_mu_del

        let mu_rep = direct "mu_rep" Mut.with_mu_rep

        let repdel_len = direct "repdel_len" Mut.with_repdel_len

        let map_snp_fpos column using = {
          column ;
          optional = false ;
          read = (fun s evo -> Mut.map_snp using evo (fpos s))
        }

        let map_snp column using = {
          column ;
          optional = false ;
          read = (fun s evo -> Mut.map_snp using evo (fpros s)) ;
        }

        let mu_snp = map_snp_fpos "mu_snp" Snp.with_mu

        let relmu_a = map_snp "relmu_a" Snp.with_relmu_a

        let relmu_t = map_snp "relmu_t" Snp.with_relmu_t

        let relmu_c = map_snp "relmu_c" Snp.with_relmu_c

        let relmu_g = map_snp "relmu_g" Snp.with_relmu_g

        let p_at = map_snp "p_at" Snp.with_p_at

        let p_ac = map_snp "p_ac" Snp.with_p_ac

        let p_ag = map_snp "p_ag" Snp.with_p_ag

        let p_ta = map_snp "p_ta" Snp.with_p_ta

        let p_tc = map_snp "p_tc" Snp.with_p_tc

        let p_tg = map_snp "p_tg" Snp.with_p_tg

        let p_ca = map_snp "p_ca" Snp.with_p_ca

        let p_ct = map_snp "p_ct" Snp.with_p_ct

        let p_cg = map_snp "p_cg" Snp.with_p_cg

        let p_ga = map_snp "p_ga" Snp.with_p_ga

        let p_gt = map_snp "p_gt" Snp.with_p_gt

        let p_gc = map_snp "p_gc" Snp.with_p_gc
      end

    let map_evo rdr =
      map ~get:(fun th -> th#evo) ~set:(fun x th -> th#with_evo x) rdr

    let p_keep = {
      column = "p_keep" ;
      optional = false ;
      read = fun s th -> th#with_p_keep (fpros s) ;
    }

    let mutating () = Mutating.[
      map_evo mu_del ;
      map_evo mu_rep ;
      map_evo repdel_len ;
      map_evo mu_snp ;
      map_evo relmu_a ;
      map_evo relmu_t ;
      map_evo relmu_c ;
      map_evo relmu_g ;
      map_evo p_at ;
      map_evo p_ac ;
      map_evo p_ag ;
      map_evo p_ta ;
      map_evo p_tc ;
      map_evo p_tg ;
      map_evo p_ca ;
      map_evo p_ct ;
      map_evo p_cg ;
      map_evo p_ga ;
      map_evo p_gt ;
      map_evo p_gc ;
      p_keep
    ]

    let p_outseed = {
      column = "p_outseed" ;
      optional = false ;
      read = fun s th -> th#with_p_outseed (fpros s) ;
    }

    let r_outdeath = {
      column = "r_outdeath" ;
      optional = false ;
      read = fun s th -> th#with_r_outdeath (fpos s) ;
    }

    let outpool () = [
      p_outseed ;
      r_outdeath ;
    ]
  end


let base row x = Reader.(load (base ()) row x)

let latent row x = Reader.(load (latent ()) row x)

let circular row x = Reader.(load (circular ()) row x)

let mutating row x = Reader.(load (mutating ()) row x)

let outpool row x = Reader.(load (outpool ()) row x)
