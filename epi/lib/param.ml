open Sig

module Di = Default_init

let foi = float_of_int

(** {1:model Model dynamic parameters} *)

(** The most basic (and pervasive) parameters *)
class host_vita = object
  (* val host_eq_nbv = 1e6 *)
  val host_birthv = (1. /. 70.)
  val host_deathv = (1. /. 70.)
  (*
  val host_birthv = 0.
  val host_deathv = 0.
  *)

  (* method host_eq_nb = host_eq_nbv *)
  method host_birth = host_birthv
  method host_death = host_deathv

  (* method with_host_eq_nb x = {< host_eq_nbv = x >} *)
  method with_host_birth x = {< host_birthv = x >}
  method with_host_death x = {< host_deathv = x >}
end

class base = object
  inherit host_vita

  val beta_v = 70.
  val nu_v = 52.
  val rho_v = 0.1


  method beta = beta_v  (** infectivity *)

  method nu = nu_v  (** recovery rate (inv duration of infection) *)

  method rho = rho_v  (** reporting proba on infection *)

  method with_beta x = {< beta_v = x >}
  method with_nu x = {< nu_v = x >}
  method with_rho x = {< rho_v = x >}
end


(** Additional parameter when there is a latent class *)
class latent = object
  val sigma_v = 30.

  method sigma = sigma_v

  method with_sigma x = {< sigma_v = x >}
end


(** Additional parameters when there is seasonal and circular dynamics. *)
class circular = object
  val betavar_v = 0.35
  val gamma_v = 0.2
  val eta_v = 0.
  val freq_v = 1.
  val phase_v = 0.

  method betavar = betavar_v
  method gamma = gamma_v
  method eta = eta_v
  method freq = freq_v
  method phase = phase_v

  method with_betavar x = {< betavar_v = x >}
  method with_gamma x = {< gamma_v = x >}
  method with_eta x = {< eta_v = x >}
  method with_freq x = {< freq_v = x >}
  method with_phase x = {< phase_v = x >}
end


(** Additional parameters when pathogens carry mutating sequences. *)
class mutating = object
  val evo_v = Seqsim.Mut.default
  val p_keep_v = 1.

  method evo = evo_v
  method p_keep = p_keep_v

  method with_evo x = {< evo_v = x >}
  method with_p_keep x = {< p_keep_v = x >}
end


(** Additional parameters when there is an outside global pool of pathogens. *)
class outpool = object
  val p_outseed_v = 0.
  val r_outdeath_v = 1.

  method p_outseed = p_outseed_v
  method r_outdeath = r_outdeath_v

  method with_p_outseed x = {< p_outseed_v = x >}
  method with_r_outdeath x = {< r_outdeath_v = x >}
end


(** Parameters for a model without sequences. *)
class t_unit = object
  inherit base
  inherit latent
  inherit circular
end


(** Parameters for a model with sequences. *)
class t_seq = object
  inherit base
  inherit latent
  inherit circular
  inherit mutating
  inherit outpool
end


(** {2:init Initialization parameters} *)


(** Parameters when a seed can be supplied (stochastic models) *)
class seedable = object
  val seed_v = None

  method seed = seed_v

  method with_seed (no : int option) = {< seed_v = no >}
end


class prm_h_able = object
  val h_v : float option = None

  method h = h_v

  method with_h x = {< h_v = Some x >} 
end

class time_prmable = object
  val prm_v : Prm_time.t option = None

  method prm = prm_v

  method with_prm mo = {< prm_v = mo >}
end


class color_prmable = object
  val prm_v : Prm_color.t option = None

  method prm = prm_v

  method with_prm mo = {< prm_v = mo >}
end


class lsodable = object
  val lsoda_par_v = Sim.Ode.Lsoda.default

  method lsoda_par = lsoda_par_v

  method with_lsoda_par apar = {< lsoda_par_v = apar >}
end


(** Parameters when a final time can be supplied *)
class tfable = object
  val tf_v = Di.tf

  method tf = tf_v

  method with_tf x = {< tf_v = x >}
end


(** Parameters when a time increment for printing can be supplied *)
class dtable = object
  val dt_v : float option = None  (* None means "print/keep everything" *)

  method dt = dt_v

  method with_dt x = {< dt_v = Some x >}
end


(** Parameters for GTR evolution of sequences *)
class gtrable = object(self)
  inherit mutating

  val gtr_v = Seqsim.Jc69.(to_gtr { mu = Di.mu })

  method gtr = gtr_v

  method with_gtr x = {<
    gtr_v = x ;
    evo_v = Seqsim.({ self#evo with snp = Gtr.to_snp x })
  >}
end


class ['a] init_sir ((s0, i0, r0) : ('a * 'a * 'a)) = object
  val s0_v = s0
  val i0_v = i0
  val r0_v = r0

  method s0 = s0_v
  method i0 = i0_v
  method r0 = r0_v

  method sir = (s0_v, i0_v, r0_v)

  method with_s0 x = {< s0_v = x >}
  method with_i0 x = {< i0_v = x >}
  method with_r0 x = {< r0_v = x >}

  method with_sir (s0, i0, r0) =
    {< s0_v = s0 ; i0_v = i0 ; r0_v = r0 >}
end


(** Parameters for initialization from int S, I, and R *)
class init_sir_int =
  object
    inherit [int] init_sir (Di.s0, Di.i0, Di.r0)
  end


(** Parameters for initialization from float S, I, and R *)
class init_sir_float =
  object
    inherit [float] init_sir (foi Di.s0, foi Di.i0, foi Di.r0)
  end


class ['a] init_e (e0 : 'a) = object
  val e0_v = e0

  method e0 = e0_v

  method with_e0 x = {< e0_v = x >}
end


class init_e_int =
  object
    inherit [int] init_e Di.e0
  end


class init_e_float =
  object
    inherit [float] init_e (foi Di.e0)
  end


class ['a] init_seir (s0, e0, i0, r0) = object
  inherit ['a] init_sir (s0, i0, r0)
  inherit ['a] init_e e0
end


(** Parameters for initialization from int S, E, I, and R *)
class init_seir_int = object
  inherit init_sir_int
  inherit init_e_int

  method seir = (s0_v, e0_v, i0_v, r0_v)
  
  method with_seir (s0, e0, i0, r0) =
    {< s0_v = s0 ; e0_v = e0 ; i0_v = i0 ; r0_v = r0 >}
end


(** Parameters for initialization from float S, E, I, and R *)
class init_seir_float = object
  inherit init_sir_float
  inherit init_e_float

  method seir = (s0_v, e0_v, i0_v, r0_v)
  
  method with_seir (s0, e0, i0, r0) =
    {< s0_v = s0 ; e0_v = e0 ; i0_v = i0 ; r0_v = r0 >}

end



(** {3:conv Accessor functions to do the appropriate conversions} *)

(* For parameters that we need to convert to pos / proba etc *)

let fpof = F.Pos.of_float
let ipoi = I.Pos.of_int


(*
let host_eq_nb par =
  fpof par#host_eq_nb
*)


let host_birth par =
  fpof par#host_birth

let host_death par =
  fpof par#host_death

let beta par =
  fpof par#beta

let nu par =
  fpof par#nu

let rho par =
  F.Proba.of_float par#rho

let sigma par =
  fpof par#sigma

let betavar par =
  F.Proba.of_float par#betavar

let gamma par =
  fpof par#gamma

let eta par =
  fpof par#eta

let freq par =
  fpof par#freq

(* F.Proba ? *)
let phase par =
  fpof par#phase

let p_keep par =
  F.Proba.of_float par#p_keep

let p_outseed par =
  F.Proba.of_float par#p_outseed

let r_outdeath par =
  fpof par#r_outdeath

let tf par =
  fpof par#tf

let dt par =
  U.Option.map fpof par#dt

let h par =
  U.Option.some (U.Option.map fpof par#h)

let seed par =
  par#seed

let sir p =
  let s, i, r = p#sir in
  (fpof s, fpof i, fpof r)

let sir_int p =
  let s, i, r = p#sir in
  (ipoi s, ipoi i, ipoi r)

let seir p =
  let s, e, i, r = p#seir in
  (fpof s, fpof e, fpof i, fpof r)
