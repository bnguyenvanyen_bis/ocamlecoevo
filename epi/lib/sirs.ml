(** SIRS models (periodic) for stochastic and deterministic simulations *)


(** About the dynamics of this model
 * (Note that the infectivity function basically defines the model
 *  and is reused in other modules (Sirs_seq))
 * There is a 'local' R0 that varies between betamin /. nu and betamax /. nu
 * If beta was constant, the equilibrium populations would be
 * S = nu /. beta *. N
 * I = gamma /. beta *. (beta -. nu) /. (nu +. gamma) *. N
 * R = nu /. beta *. (beta -. nu) /. (nu +. gamma) *. N
 * which gives us an idea of how low the infected population might go.
 *)

open Sig


let model_spec = {
  latency = false ;
  circular = true ;
}


module Model =
  struct
    let sp = model_spec
  end
