(** Default init values *)

let seed = None

let tf = 1.

let mu = 1e-4

let s0 = 899_980
let e0 = 10
let i0 = 10
let r0 = 100_000
let o0 = 10
