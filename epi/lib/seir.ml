(* SEIR model *)
open Sig


let model_spec = {
  latency = true ;
  circular = false ;
}


module Model =
  struct
    let sp = model_spec
  end
