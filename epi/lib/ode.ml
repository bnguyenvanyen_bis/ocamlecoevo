open Sig


module Make (P : sig
    type t = vec * U.closed_pos
    module Get : (GET with type t = t)
    include MATSET
    (* vec * sum vec *)
  end) =
  struct
    (* write the f components to build a f from
     * give a general domain thing
     * event matrixes and do the sum ?
     * or matrix modification and fold -- seems more efficient *)
    
    module R = Rates.Make (P.Get)

    let recovery par a = P.(
      inc T.Inf T.Inf a (~-. (par#nu)) ;
      inc T.Rem T.Inf a par#nu
    )

    let infection_of grp infctvt par t y a =
      let beta_s = F.to_float (R.infect (infctvt par t) y) in
      (* Printf.eprintf "beta_s : %f\n" beta_s ; *)
      P.(inc T.Sus T.Inf a (~-. beta_s)) ;
      P.(inc grp T.Inf a beta_s)

    let in_domain ?n y =
      match Lac.Vec.iter (fun x -> assert (x >= 0.)) ?n y with
      | () ->
          true
      | exception Assert_failure _ ->
          false
   
    (* FIXME some reflexion or h reduction would be better ? *)
    let to_domain dim ~z y =
      (* Printf.eprintf "Passed through boundary\n" ; *)
      for i = 1 to dim do
        if y.{i} < 0. then
          let a = y.{i} /. float (dim - 1) in
          for j = 1 to (i - 1) do
            z.{j} <- y.{j} +. a
          done;
          for j = (i + 1) to dim do
            z.{j} <- y.{j} +. a
          done;
          z.{i} <- 0.;
      done

    module Out = R.Out
  end

