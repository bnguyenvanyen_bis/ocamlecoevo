open Sig

module J = Jump


type point = {
  s : U.closed_pos ;
  e : U.closed_pos ;
  i : U.closed_pos ;
  r : U.closed_pos ;
  c : U.closed_pos ;
  o : U.closed_pos ;
  n : U.closed_pos ;
  reff : U.closed_pos ;
  coal : U.closed_pos ;
  reportr : U.closed_pos ;
}


type t = (Sim.Sig.time * point) list


let infected t =
  L.map (fun (t, pt) -> (F.to_float t, pt.i)) t


let cumulative_cases t =
  L.map (fun (t, pt) -> (F.to_float t, pt.c)) t


let reporting_rate t =
  L.map (fun (t, pt) -> (F.to_float t, pt.reportr)) t


let expected_neff t =
  L.map (fun (t, pt) -> (F.to_float t, F.Pos.invert pt.coal)) t
