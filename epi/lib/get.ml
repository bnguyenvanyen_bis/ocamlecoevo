open Sig

(* for Unit and Seq pops *)

(* the two functors share the same code
 * but require different signature constraints *)


(* This is the old version, that we're now reusing.
 * I'm leaving the other ones because I'm still not sure what is the best *)
module Make
  (T : TRAIT)
  (P : Pop.Sig.POP with type 'a isid = 'a 
                    and type 'a trait = 'a T.t
                    and type 'a group = 'a T.group) :
  (GET with type t = P.t) =
  struct
    type t = P.t

    let sus z = z |> P.count ~group:T.Sus |> I.Pos.to_float
    let exp z = z |> P.count ~group:T.Exp |> I.Pos.to_float
    let inf z = z |> P.count ~group:T.Inf |> I.Pos.to_float 
    let rem z = z |> P.count ~group:T.Rem |> I.Pos.to_float
    let cas z = z |> P.count ~group:T.Cas |> I.Pos.to_float
    let out z = z |> P.count ~group:T.Out |> I.Pos.to_float

    let tot z =
      I.Pos.to_float (I.Pos.Op.(
          P.count ~group:T.Sus z
        + P.count ~group:T.Exp z
        + P.count ~group:T.Inf z
        + P.count ~group:T.Rem z
      ))
  end


module Make_unit
  (T : TRAIT with type idor = nonid)
  (P : Pop.Sig.POP with type 'a isid = nonid
                    and type 'a trait = 'a T.t
                    and type 'a group = 'a T.group) :
  (GET with type t = P.t) =
  struct
    type t = P.t

    let sus z = z |> P.count ~group:T.Sus |> I.Pos.to_float
    let exp z = z |> P.count ~group:T.Exp |> I.Pos.to_float
    let inf z = z |> P.count ~group:T.Inf |> I.Pos.to_float 
    let rem z = z |> P.count ~group:T.Rem |> I.Pos.to_float
    let cas z = z |> P.count ~group:T.Cas |> I.Pos.to_float
    let out z = z |> P.count ~group:T.Out |> I.Pos.to_float

    let tot z =
      I.Pos.to_float (I.Pos.Op.(
          P.count ~group:T.Sus z
        + P.count ~group:T.Exp z
        + P.count ~group:T.Inf z
        + P.count ~group:T.Rem z
      ))
  end


(*
module Unit = Make_unit (Trait.Unit) (Epi__Pop.Unit)
*)


module Make_seq
  (T : TRAIT with type idor = id)
  (P : Pop.Sig.POP with type 'a isid = 'a
                    and type 'a trait = 'a T.t
                    and type 'a group = 'a T.group) :
  (GET with type t = P.t) =
  struct
    type t = P.t

    let sus z = z |> P.count ~group:T.Sus |> I.Pos.to_float
    let exp z = z |> P.count ~group:T.Exp |> I.Pos.to_float
    let inf z = z |> P.count ~group:T.Inf |> I.Pos.to_float 
    let rem z = z |> P.count ~group:T.Rem |> I.Pos.to_float
    let cas z = z |> P.count ~group:T.Cas |> I.Pos.to_float
    let out z = z |> P.count ~group:T.Out |> I.Pos.to_float

    let tot z =
      I.Pos.to_float (I.Pos.Op.(
          P.count ~group:T.Sus z
        + P.count ~group:T.Exp z
        + P.count ~group:T.Inf z
        + P.count ~group:T.Rem z
      ))
  end


module Seq = Make_seq (Trait.Seq) (Epi__Pop.Seq)
