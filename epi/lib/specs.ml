(** Specs (for Arg) *)

module La = Lift_arg

(*
let host_eq_nb = (
  "-n",
  La.Float (fun par x -> par#with_host_eq_nb x),
  "Host equilibrium population size
   -- only reached if host birth and death rates are equal."
)
*)

let host_birth = (
  "-birth",
  La.Float (fun par x -> par#with_host_birth x),
  "Host birth rate rescaled by the equilibrium host population size."
)

let host_death = (
  "-death",
  La.Float (fun par x -> par#with_host_death x),
  "Host per capita death rate."
)

let beta = (
  "-beta",
  La.Float (fun par x -> par#with_beta x),
  "Base infectivity."
)

(* incompatible with beta *)
let betavar = (
  "-beta",
  La.Tuple [
    La.Float (fun par x -> par#with_beta x ) ;
    La.Float (fun par x -> par#with_betavar x)
  ],
  "Mean and half-delta of infectivity. Should have beta > 2.*.betavar."
)

let sigma = (
  "-sigma",
  La.Float (fun par x -> par#with_sigma x),
  "Rate at which an exposed host becomes infective (1/latency)."
)

let nu = (
  "-nu",
  La.Float (fun par x -> par#with_nu x),
  "Individual recovery rate."
)

let rho = (
  "-rho",
  La.Float (fun par x -> par#with_rho x),
  "Reporting probability."
)

let gamma = (
  "-gamma",
  La.Float (fun par x -> par#with_gamma x),
  "Loss of immunity rate."
)

let eta = (
  "-eta",
  La.Float (fun par x -> par#with_eta x),
  "Rate of infection from outside."
)

let freq = (
  "-freq",
  La.Float (fun par x -> par#with_freq x),
  "Infectivity variation frequency"
)

let phase = (
  "-phase",
  La.Float (fun par x -> par#with_phase x),
  "Infectivity variation phase"
)

let p_keep = (
  "-p-keep",
  La.Float (fun par x -> par#with_p_keep x),
  "Probability of sampling the individual on recovery."
)

let p_outseed = (
  "-p-outseed",
  La.Float (fun par x -> par#with_p_outseed x),
  "Probability of seeding the global pool on recovery."
)

let r_outdeath = (
  "-r-outdeath",
  La.Float (fun par x -> par#with_r_outdeath x),
  "Competitive sequence death rate in the global pool."
)

let mut () =
  La.map (fun p -> p#with_evo) (fun p -> p#evo) Seqsim.Mut.specl


(* Specs for initial conditions and controlling input / output *)

(* Could we put those three in Lift_arg or Sim for instance ? *)

let out_string = (
  "-out",
  La.String (fun _ s -> Some s),
  "Path prefix to output to."
)

let dt = (
  "-dt",
  La.Float (fun ipar x -> ipar#with_dt x),
  "Minimum interval between prints."
)

let tf = (
  "-tf",
  La.Float (fun ipar x -> ipar#with_tf x),
  "Simulate until."
)

let seed = (
  "-seed",
  La.Int (fun ipar n -> ipar#with_seed (Some n)),
  "Seed for the PRNG."
)

let s0_int = (
  "-s0",
  La.Int (fun ipar n -> ipar#with_s0 n),
  "Initial number of susceptibles."
)

let e0_int = (
  "-e0",
  La.Int (fun ipar n -> ipar#with_e0 n),
  "Initial number of exposed."
)

let i0_int = (
  "-i0",
  La.Int (fun ipar n -> ipar#with_i0 n),
  "Initial number of infected."
)

let r0_int = (
  "-r0",
  La.Int (fun ipar n -> ipar#with_r0 n),
  "Initial number of removed."
)

let s0_float = (
  "-s0",
  La.Float (fun ipar x -> ipar#with_s0 x),
  "Initial number of susceptibles."
)

let e0_float = (
  "-e0",
  La.Float (fun ipar x -> ipar#with_e0 x),
  "Initial number of exposed."
)

let i0_float = (
  "-i0",
  La.Float (fun ipar x -> ipar#with_i0 x),
  "Initial number of infected."
)

let r0_float = (
  "-r0",
  La.Float (fun ipar x -> ipar#with_r0 x),
  "Initial number of removed."
)

(* FIXME What to do with this ? *)
let e0_seq = (
  "-e0",
  La.String (fun _ s -> Some s),
  "Channel/file to get initial E pathogen sequence(s) from."
)

let i0_seq = (
  "-i0",
  La.String (fun _ s -> Some s),
  "Channel/file to get initial I pathogen sequence(s) from."
)

(* careful, incompatible with Ode ! *)
let h = (
  "-h",
  La.Float (fun ipar x -> ipar#with_h x),
  "Time slice width for the Poisson random measure."
)

(* careful, incompatible with h *)
let lsoda_par () =
  La.map (fun p -> p#with_lsoda_par) (fun p -> p#lsoda_par) Sim.Ode.Lsoda.specl


let init_from load = (
  "-init-from",
  La.String load,
  "Set param values from the last row of a CSV file."
)

let init_prm_from = (
  "-init-prm-from",
  La.String (fun _ s -> Some s),
  "Set the prm from a file. Incompatible with '-seed'."
)

let output_points r = (
  "-output-points",
  Arg.Set r,
  "Output the points of the prm to '{out}.prm.points.csv'."
)

let output_grid r = (
  "-output-grid",
  Arg.Set r,
  "Output the grid of the prm to '{out}.prm.grid.csv'."
)

let exact_prm r = (
  "-exact",
  Arg.Set r,
  "Simulate the system with the exact prm method."
)

let no_sim r = (
  "-no-sim",
  Arg.Set r,
  "Don't simulate the system."
)

(*
let approx = (
  "-approx",
  La.Bool (fun _ b -> b),
  "Flag to simulate approximately."
)
*)
