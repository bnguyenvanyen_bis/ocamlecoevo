(* We want a model that will take into account all the sources of data
 * we will obtain from the PANIC study in Kampong Cham.
 * This includes :
 * - different periods :
 *   - the long term surveillance (for which we have subtype proportion estimates)
 *   - the DVI study (which for now only gives us sequences)
 *   - the PANIC study
 * - different cohorts :
 *   - the general Kampong Cham cohort, out of which we get severe cases that
 *     get examined at the health centers
 *   - the seroconversion cohort : school-children that give a saliva sample
 *     every three months, to test IgG levels.
 *     (Seroconversion can indicate dengue or another flavivirus)
 *     (but we might see asymptomatic cases)
 *   - the active surveillance cohort :
 *     school age children which we follow the temperature of,
 *     and if they have symptoms they get tested.
 *     This might give us an idea of the proportion of true symptomatic cases in time
 *     and of the total rate of symptomatic cases.
 *   - then there are tests on contacts of those confirmed cases.
 *
 *
 * So how do we model this ? For the local model
 *
 * - We need to take into account asymptomatic cases (so SEIAR)
 * - We need to take into account the cohorts
 * - We might need to take into account in general children / adults
 * - We might need to take into account school / non school
 * - In the seroconversion cohort we need to track who seroconverted and who did not
 *   maybe we just add additional compartments before R corresponding
 *   to the decrease in antibodies ? -> go through 'IgM' before R.
 *
 *   So let's say we have four host classes
 *   - adults
 *   - children
 *   - children from seroprevalence cohort
 *   - children from temperature cohort
 *   (maybe to start with we merge adults and children)
 *
 *   We'll suppose all children are equivalent in terms of contact.
 *   
 *   And we have 5-6 disease status classes
 *   - S
 *   - E
 *   - I
 *   - A
 *   - (IgM)
 *   - R
 *
 *  Should the probability of being asymptomatic be the same for children
 *  and adults ? (depends on immune history)
 *  (to start with yes)
 *
 *  Then what we observe is :
 *  - Every three months, the proportion of IgM (and R ?) in the seroprevalence
 *    cohort.
 *  - When fever happens in the temperature cohort (actually more rich than that)
 *    and in some cases whether it is due to dengue
 *  - Other confirmed cases in the general cohort (also suspected cases ?)
 *
 *  How do we take into account the confusions ?
 *  (Or rather how do we simulate them ?)
 *  - we want fever to sometimes happen without dengue
 *  - we want IgM to sometimes happen without dengue
 *  To be explicit we can add a Sxfever and a SxIgM compartment,
 *  for the two cohorts respectively, that the children might go through.
 *  
 *
 *  In parallel another more general (deterministic) model for surveillance ?
 *  Where we can force the estimates for
 *  
 *)



(* We can't use the same code as for SEIRCOx
 * (if we had open constructors it would be easier) *)


open Sig


module Trait =
  struct
    (* version without confusion *)

    type aps = [`Active | `Passive | `Sero]
    type seq = Seqs.t * (float * Seqs.Diff.t) list

    type _ t =
      | S : aps -> id t
      | E : aps * seq -> id t
      | I : aps * seq * [`Sympto | `Asympto] -> id t
      | R : aps -> id t
      | R_IgM : id t (* 'id' is forced by recovery (must be same as R) *)
      | R_IgG : nonid t
      | C_active : seq option -> id t
      | C_passive : seq option -> id t
      | C_sero : seq option -> id t

    type _ group =
      | Sus : id group
      | Exp : id group
      | Inf : id group
      | Rem : id group
      | Rem_IgM : id group
      | Rem_IgG : nonid group
      | Cas_active : id group
      | Cas_passive : id group
      | Cas_sero : id group

    let isid : type a. a group -> a Pop.Sig.isid =
      function
      | Sus -> Isid Eq
      | Exp -> Isid Eq
      | Inf -> Isid Eq
      | Rem -> Isid Eq
      | Rem_IgM -> Isid Eq
      | Rem_IgG -> Isnotid Eq
      | Cas_active -> Isid Eq
      | Cas_passive -> Isid Eq
      | Cas_sero -> Isid Eq

    let group_of : type a. a t -> a group =
      function
      | S _ -> Sus
      | E _ -> Exp
      | I _ -> Inf
      | R _ -> Rem
      | R_IgM -> Rem_IgM
      | R_IgG -> Rem_IgG
      | C_active _ -> Cas_active
      | C_passive _ -> Cas_passive
      | C_sero _ -> Cas_sero

    let of_group : nonid group -> nonid t =
      function
      | Rem_IgG -> R_IgG
    
    let copy_seqo =
      function
      | None ->
          None
      | Some seq ->
          Some (Seqsim.Trait.copy seq)

    let copy : type a. a t -> a t =
      function
      | S cohort ->
          S cohort
      | E (cohort, seq) ->
          E (cohort, Seqsim.Trait.copy seq)
      | I (cohort, seq, symptom) ->
          I (cohort, Seqsim.Trait.copy seq, symptom)
      | R cohort ->
          R cohort
      | R_IgM ->
          R_IgM
      | R_IgG ->
          R_IgG
      | C_active seqo ->
          C_active (copy_seqo seqo)
      | C_passive seqo ->
          C_passive (copy_seqo seqo)
      | C_sero seqo ->
          C_sero (copy_seqo seqo)

    (* We only need to know which seq is the longest *)
    let score : id t -> float =
      let conv n = float (I.to_int n) in
      function
      | S _ ->
          0.
      | E (_, seq) ->
          conv (Seqsim.Trait.length seq)
      | I (_, seq, _) ->
          conv (Seqsim.Trait.length seq)
      | R _ ->
          0.
      | R_IgM ->
          0.
      | C_active _ ->
          0.
      | C_passive _ ->
          0.
      | C_sero _ ->
          0.
  end


module Param =
  struct
    include Param

    class sero = object
      val igm_loss_v = 0.05 (* ~ three weeks *)
      val igg_loss_v = 0.25

      method igm_loss = igm_loss_v
      method igg_loss = igg_loss_v

      method with_igm_loss x = {< igm_loss_v = x >}
      method with_igg_loss x = {< igg_loss_v = x >}
    end

    class asympto = object
      val p_asympto_v = 0.6
        
      method p_asympto = p_asympto_v

      method with_p_asympto x = {< p_asympto_v = x >}
    end

    (*
    class evolvable = object
      val evo_v = Seqsim.Mut.default

      method evo = evo_v

      method with_evo x = {< evo_v = x >}
    end
    *)

    class recordable = object
      val p_seq_v = 0.1

      method p_seq = p_seq_v

      method with_p_seq x = {< p_seq_v = x >}
    end

    class mutator = object
      inherit recordable
      inherit gtrable
    end

    class t = object
      inherit base
      inherit latent
      inherit circular
      inherit sero
      inherit asympto
      inherit mutator
    end

    let igm_loss par = fpof par#igm_loss

    let igg_loss par = fpof par#igg_loss

    let p_asympto par = F.Proba.of_float par#p_asympto
    
    let p_seq par = F.Proba.of_float par#p_seq
  end


module Pop = Pop.With_events.Make (Trait)


module Get =
  struct
    open Trait

    type t = Pop.t

    let sus z = z |> Pop.count ~group:Sus |> I.Pos.to_float
    let exp z = z |> Pop.count ~group:Exp |> I.Pos.to_float
    let inf z = z |> Pop.count ~group:Inf |> I.Pos.to_float 
    let rem z = z |> Pop.count ~group:Rem |> I.Pos.to_float
    let cas z = z |> Pop.count ~group:Cas_passive |> I.Pos.to_float
    let out _ = failwith "no out"

    let tot z =
      I.Pos.to_float (I.Pos.Op.(
          Pop.count ~group:Sus z
        + Pop.count ~group:Exp z
        + Pop.count ~group:Inf z
        + Pop.count ~group:Rem z
      ))
  end


module Rates =
  struct
    include Rates.Make (Get)

    let mutation group mut_rate par t z =
      let f seq =
        F.Pos.mul
          (mut_rate par#evo t seq)
          (I.Pos.to_float (Pop.count ~group z))
      in
      let _, x = Pop.max ~i:0 z in
      Trait.(
        match x with
        | E (_, seq) ->
            f seq
        | I (_, seq, _) ->
            f seq
        | _ ->
            F.zero
      )

    let igm_loss par _ z =
      F.Pos.mul
        (Param.igm_loss par)
        (I.Pos.to_float (Pop.count ~group:Rem_IgM z))

    let igm_host_death par _ z =
      F.Pos.mul
        (Param.host_death par)
        (I.Pos.to_float (Pop.count ~group:Rem_IgM z))

    let igg_loss par _ z =
      F.Pos.mul
        (Param.igg_loss par)
        (I.Pos.to_float (Pop.count ~group:Rem_IgG z))

    let igg_host_death par _ z =
      F.Pos.mul
        (Param.host_death par)
        (I.Pos.to_float (Pop.count ~group:Rem_IgG z))
  end


module Modifs =
  struct
    (* as it is we can't reuse the Modifs of Events,
     * as they rely on the constructors of Trait.
     * We would need to make the signature of Trait more like GET.
     * Still it seems much better than rewriting... *)

    open Trait

    module SCU = Sim.Ctmjp.Util

    let ifg = Pop.indiv_from_group


    let is_to_e i s =
      match i, s with
      | I (_, seq, _), S cohort ->
          E (cohort, seq)
      | _ ->
          failwith "not S"

    let igm_to_igg =
      function
      | R_IgM ->
          R_IgG
      | _ ->
          failwith "not IgM"

    let igg_to_rsero =
      function
      | R_IgG ->
          R `Sero
      | _ ->
          .

    let r_to_s =
      function
      | R x -> S x
      | _ -> failwith "not R"

    (*
    let f_first f (x, y) =
      (f x, y)
    *)

    let mut_first f = Some (fun stoch t x _ ->
      f stoch t x
    )


    let immunity_loss rng t z =
      let mut = Some (fun _ _ x _ -> r_to_s x) in
      let rem = Pop.choose ~rng ~group:Rem z in
      Pop.pair_bd_modif ~mut rem rem rng t z

    let infection rng t z =
      let mut = Some (fun _ _ -> is_to_e) in
      let inf = Pop.choose ~rng ~group:Inf z in
      let sus = Pop.choose ~rng ~group:Sus z in
      Pop.pair_bd_modif ~mut inf sus rng t z


    let leave_exposed_mutate par rng _ =
      let p_asympto = Param.p_asympto par in
      function
      | E (cohort, seq) ->
          if U.rand_bernoulli ~rng p_asympto then
            I (cohort, seq, `Asympto)
          else
            I (cohort, seq, `Sympto)
      | _ ->
          failwith "not E"


    let leave_exposed par rng t z =
      let mut = Some (fun rng t x _ -> leave_exposed_mutate par rng t x) in
      let exp = Pop.choose ~rng ~group:Exp z in
      Pop.pair_bd_modif ~mut exp exp rng t z

    let recovery par rng t z =
      let p_cas = Param.rho par in
      let p_seq = Param.p_seq par in
      let maybe_seq rng seq =
        if U.rand_bernoulli ~rng p_seq then
          Some seq
        else
          None
      in
      let mut_rem _ _ =
        function
        | I (`Sero, _, _) ->
            (* FIXME should we also maybe get a case ? *)
            R_IgM
        | I (cohort, _, _) ->
            R cohort
        | _ ->
            failwith "not I"
      in
      let mut_cas rng _ =
        function
        | I (`Active, seq, `Sympto) ->
            Some (C_active (maybe_seq rng seq))
        | I ((`Passive | `Sero) as x, seq, `Sympto) ->
            if U.rand_bernoulli ~rng p_cas then
              match x with
              | `Passive ->
                  Some (C_passive (maybe_seq rng seq))
              | `Sero ->
                  Some (C_sero (maybe_seq rng seq))
            else
              None
        | I (_, _, `Asympto) ->
            None
        | _ ->
            failwith "not I"
      in
      let inf = Pop.choose ~rng ~group:Inf z in
      z
      |> Pop.birth_modif ~mut:mut_rem inf rng t
      |> Pop.birth_modif_maybe ~mut:mut_cas inf rng t
      |> Pop.death_modif inf rng t

    let igm_loss rng t z =
      let mut = Some (fun _ _ _ -> igm_to_igg) in
      let igm = Pop.choose ~rng ~group:Rem_IgM z in
      Pop.pair_bd_modif ~mut igm igm rng t z

    let igg_loss rng t z =
      let mut = Some (fun _ _ _ -> igg_to_rsero) in
      let igg = ifg Rem_IgG in
      Pop.pair_bd_modif ~mut igg igg rng t z

    let host_birth stoch t z =
      let sus = Pop.new_indiv z (S `Passive) in
      Pop.birth_modif sus stoch t z

    let host_death group stoch t z =
      let ind = Pop.choose ~rng:stoch ~group z in
      Pop.death_modif ind stoch t z

    let sus_host_death stoch =
      host_death Sus stoch

    let exp_host_death stoch =
      host_death Exp stoch

    let inf_host_death stoch =
      host_death Inf stoch

    let igm_host_death stoch =
      host_death Rem_IgM stoch

    let igg_host_death stoch =
      host_death Rem_IgG stoch

    let rem_host_death stoch =
      host_death Rem stoch

    let mutation_on_ind mut' par ind stoch t z =
      let mutate stoch t x = Trait.(
        let f y =
          mut' par#evo stoch t y
        in
        match x with
        | E (cohort, seq) ->
            E (cohort, f seq)
        | I (cohort, seq, symptom) ->
            I (cohort, f seq, symptom)
        | _ ->
            failwith "not E or I"
      )
      in
      let mut = mut_first mutate in
      Pop.pair_bd_modif ~mut ind ind stoch t z

    let mutation group mut par rng t z =
      let ind = Pop.choose ~rng ~group z in
      mutation_on_ind mut par ind rng t z

  end


type seqs = (int * Seqs.t) list

type seir = {
  s : int ;
  e : seqs ;
  i : seqs ;
  r : int ;
}

type sero = {
  s : int ;
  e : seqs ;
  i : seqs ;
  igm : int ;
  igg : int ;
  r : int ;
}

type y0 = {
  passive : seir ;
  active : seir ;
  sero : sero ;
}


module With =
  struct
    let s0 (y0 : y0) s =
      { y0 with passive = { y0.passive with s } }

    let e0 (y0 : y0) e =
      { y0 with passive = { y0.passive with e } }

    let i0 (y0 : y0) i =
      { y0 with passive = { y0.passive with i } }

    let r0 (y0 : y0) r =
      { y0 with passive = { y0.passive with r } }

    let active_s0 (y0 : y0) s =
      { y0 with active = { y0.active with s } }

    let active_e0 (y0 : y0) e =
      { y0 with active = { y0.active with e } }

    let active_i0 (y0 : y0) i =
      { y0 with active = { y0.active with i } }

    let active_r0 (y0 : y0) r =
      { y0 with active = { y0.active with r } }

    let sero_s0 (y0 : y0) s =
      { y0 with sero = { y0.sero with s } }

    let sero_e0 (y0 : y0) e =
      { y0 with sero = { y0.sero with e } }

    let sero_i0 (y0 : y0) i =
      { y0 with sero = { y0.sero with i } }

    let sero_igm0 (y0 : y0) igm =
      { y0 with sero = { y0.sero with igm } }

    let sero_igg0 (y0 : y0) igg =
      { y0 with sero = { y0.sero with igg } }

    let sero_r0 (y0 : y0) r =
      { y0 with sero = { y0.sero with r } }

    let map_y0 with_ th n =
      th#with_y0 (with_ th#y0 n)
  end


module Specs =
  struct
    include Specs

    let igm_loss = (
      "-igm-loss",
      La.Float (fun par x -> par#with_igm_loss x),
      "Rate at which IgM becomes undetectable in saliva."
    )

    let igg_loss = (
      "-igg-loss",
      La.Float (fun par x -> par#with_igg_loss x),
      "Rate at which IgG becomes undetectable in saliva."
    )

    let p_asympto = (
      "-p-asympto",
      La.Float (fun par x -> par#with_p_asympto x),
      "Proportion of asymptomatic infections."
    )

    let p_seq = (
      "-p-seq",
      La.Float (fun par x -> par#with_p_seq x),
      "Probability of sequencing a case."
    )

    let s0 = (
      "-s0",
      La.Int (With.map_y0 With.s0),
      "Initial number of susceptibles in the general population."
    )

    let r0 = (
      "-r0",
      La.Int (With.map_y0 With.r0),
      "Initial number of removed in the general population."
    )

    let active_s0 = (
      "-active-s0",
      La.Int (With.map_y0 With.active_s0),
      "Initial number of susceptibles in the active surveillance cohort."
    )

    let active_r0 = (
      "-active-r0",
      La.Int (With.map_y0 With.active_r0),
      "Initial number of removed in the active surveillance cohort."
    )

    let sero_s0 = (
      "-sero-s0",
      La.Int (With.map_y0 With.sero_s0),
      "Initial number of susceptibles in the seroconversion cohort."
    )

    let sero_igm0 = (
      "-sero-igm0",
      La.Int (With.map_y0 With.sero_igm0),
      "Initial number of IgM-positive in the seroconversion cohort."
    )

    let sero_igg0 = (
      "-sero-igg0",
      La.Int (With.map_y0 With.sero_igg0),
      "Initial number of IgG-positive in the seroconversion cohort."
    )

    let sero_r0 = (
      "-sero-s0",
      La.Int (With.map_y0 With.sero_r0),
      "Initial number of removed in the seroconversion cohort."
    )

    let siaof s =
      let c = open_in s in
      let seqs = Seqs.Io.assoc_of_fasta c in
      close_in c ;
      seqs

    let e0 = (
      "-e0",
      La.String (With.map_y0 (fun y0 s -> With.e0 y0 (siaof s))),
      "Initial sequences for latent individuals in the general population."
    )

    let i0 = (
      "-i0",
      La.String (With.map_y0 (fun y0 s -> With.i0 y0 (siaof s))),
      "Initial sequences for infectious individuals in the general population."
    )

    let active_e0 = (
      "-active-e0",
      La.String (With.map_y0 (fun y0 s -> With.active_e0 y0 (siaof s))),
      "Initial sequences for latent individuals in the active surveillance cohort."
    )

    let active_i0 = (
      "-active-i0",
      La.String (With.map_y0 (fun y0 s -> With.active_i0 y0 (siaof s))),
      "Initial sequences for infectious individuals in the active surveillance cohort."
    )

    let sero_e0 = (
      "-sero-e0",
      La.String (With.map_y0 (fun y0 s -> With.sero_e0 y0 (siaof s))),
      "Initial sequences for latent individuals in the seroconversion cohort."
    )

    let sero_i0 = (
      "-sero-i0",
      La.String (With.map_y0 (fun y0 s -> With.sero_i0 y0 (siaof s))),
      "Initial sequences for infectious individuals in the seroconversion cohort."
    )
  end


module Events =
  struct
    module R = Rates
    module M = Modifs

    module Mut = Seqsim.Mut


    let infectivity = Seirco.var_infectivity

    let events par =
      (* The cycle of SEIRS events *)
      let infection_ev = (
        R.infection_of infectivity par,
        M.infection
      )
      in
      let leave_exposed_ev = (
        R.leave_exposed par,
        M.leave_exposed par
      )
      in
      let recovery_ev = (
        R.recovery par,
        M.recovery par
      )
      in
      let igm_loss_ev = (
        R.igm_loss par,
        M.igm_loss
      )
      in
      let igg_loss_ev = (
        R.igg_loss par,
        M.igg_loss
      )
      in
      let immunity_loss_ev = (
        R.immunity_loss par,
        M.immunity_loss
      )
      in
      (* immigrations *)
      let out_infection_ev = (
        R.out_infection_of infectivity par,
        M.infection
      )
      in
      (* mutations *)
      let mutation_evs =
        let muts = [
          (Mut.subst_rate, Mut.subst_modif) ;
          (Mut.rep_subseq_rate, Mut.rep_subseq_modif) ;
          (Mut.del_subseq_rate, Mut.del_subseq_modif) ;
        ]
        in
        L.fold_left (fun evs g ->
            (L.map (fun (mut, mut') ->
              (R.mutation g mut par, M.mutation g mut' par)
            ) muts) @ evs
          ) [] [Inf ; Exp]
      in
      (* host demography *)
      let sus_birth_ev = (
        R.host_birth par,
        M.host_birth
      )
      in
      let sus_death_ev = (
        R.sus_host_death par,
        M.sus_host_death
      )
      in
      let exp_death_ev = (
        R.exp_host_death par,
        M.exp_host_death
      )
      in
      let inf_death_ev = (
        R.inf_host_death par,
        M.inf_host_death
      )
      in
      let igm_death_ev = (
        R.igm_host_death par,
        M.igm_host_death
      )
      in
      let igg_death_ev = (
        R.igg_host_death par,
        M.igg_host_death
      )
      in
      let rem_death_ev = (
        R.rem_host_death par,
        M.rem_host_death
      )
      (* FIXME add mutations *)
      in [
        infection_ev ;
        leave_exposed_ev ;
        recovery_ev ;
        igm_loss_ev ;
        igg_loss_ev ;
        immunity_loss_ev ;
        out_infection_ev ;
        sus_birth_ev ;
        sus_death_ev ;
        exp_death_ev ;
        inf_death_ev ;
        igm_death_ev ;
        igg_death_ev ;
        rem_death_ev ;
      ] @ mutation_evs

    let event_sum par =
      let rate_and_modifs = events par in
      let rate_modifs = L.map (fun (r, m) -> SCU.combine r m) rate_and_modifs in
      SCU.add_many rate_modifs

    let specs = Specs.[
      host_birth ; host_death ;
      betavar ; sigma ; nu ; rho ; gamma ; eta ; freq ; phase ;
      igm_loss ; igg_loss ; p_asympto ; p_seq ;
    ]

    let par_header = [
      "host_birth" ; "host_death" ;
      "beta" ; "nu" ; "rho" ;
      "sigma" ;
      "betavar" ; "gamma" ; "eta" ; "freq" ; "phase" ;
      "igm_loss" ; "igg_loss" ; "p_asympto" ; "p_seq" ;
      "seed" ; "dt" ; "tf" ;
      "s0" ; "e0" ; "i0" ; "r0" ;
      "active_s0" ; "active_e0" ; "active_i0" ; "active_r0" ;
      "sero_s0" ; "sero_e0" ; "sero_i0" ; "sero_igm0" ; "sero_igg0" ; "sero_r0" ;
    ]

    let par_line
        ?seed ?dt (p : Param.t) tf { passive = pas ; active = act ; sero } =
      let sof = string_of_float in
      let soi = string_of_int in
      let len = L.length in
      let dt =
        match dt with
        | None ->
            0.
        | Some x ->
            F.to_float x
      in
      let tf = F.to_float tf in
      let seed =
        match seed with
        | None ->
            "NA"
        | Some n ->
            soi n
      in
      [
        sof p#host_birth ; sof p#host_death ;
        sof p#beta ; sof p#nu ; sof p#rho ;
        sof p#sigma ;
        sof p#betavar ; sof p#gamma ; sof p#eta ; sof p#freq ; sof p#phase ;
        sof p#igm_loss ; sof p#igg_loss ; sof p#p_asympto ; sof p#p_seq ;
        seed ; sof dt ; sof tf ;
        soi pas.s ; soi (len pas.e) ; soi (len pas.i) ; soi pas.r ;
        soi act.s ; soi (len act.e) ; soi (len act.i) ; soi act.r ;
        soi sero.s ; soi (len sero.e) ; soi (len sero.i) ;
        soi sero.igm ; soi sero.igg ; soi sero.r ;
      ]
   
    (* let coal = R.coal_ei *)
 
    let add_many z0 =
      L.iter (fun (k, state) ->
        ignore (Pop.add_new ~k z0 state)
      )

    let of_tuples { passive = pas ; active = act ; sero } =
      let er = Process.Seq.empty_record in
      let len = L.length in
      let nindivs =
          pas.s + len pas.e + len pas.i + pas.r
        + act.s + len act.e + len act.i + act.r
        + sero.s + len sero.e + len sero.i + sero.igm + sero.igg + sero.r
      in
      let z = Pop.create ~ngroups:6 ~nindivs ~mem:5 ~scores:[Trait.score] in
      Trait.(
      ignore (Pop.add_new ~k:pas.s z (S `Passive)) ;
      pas.e
      |> L.map (fun (k, seq) -> (k, E (`Passive, (seq, er))))
      |> add_many z ;
      (* FIXME also `Sympto ? *)
      pas.i
      |> L.map (fun (k, seq) -> (k, I (`Passive, (seq, er), `Asympto)))
      |> add_many z ;
      ignore (Pop.add_new ~k:pas.r z (R `Passive)) ;
      ignore (Pop.add_new ~k:act.s z (S `Active)) ;
      act.e
      |> L.map (fun (k, seq) -> (k, E (`Active, (seq, er))))
      |> add_many z ;
      (* FIXME also `Sympto ? *)
      act.i
      |> L.map (fun (k, seq) -> (k, I (`Active, (seq, er), `Asympto)))
      |> add_many z ;
      ignore (Pop.add_new ~k:act.r z (R `Active)) ;
      ignore (Pop.add_new ~k:sero.s z (S `Sero)) ;
      sero.e
      |> L.map (fun (k, seq) -> (k, E (`Sero, (seq, er))))
      |> add_many z ;
      (* FIXME also `Sympto ? *)
      sero.i
      |> L.map (fun (k, seq) -> (k, I (`Sero, (seq, er), `Asympto)))
      |> add_many z ;
      ignore (Pop.add_new ~k:sero.igm z R_IgM) ;
      ignore (Pop.add_new ~k:sero.igg z R_IgG) ;
      ignore (Pop.add_new ~k:sero.r z (R `Sero)) ;
      z
      )

    (* to_tuples --> can't count everything ! *)

    let conv t z = (
      t,
      Get.sus z,
      Get.exp z,
      Get.inf z,
      Get.rem z,
      I.to_float (Pop.count ~group:Rem_IgM z)
    )

    let header =
      [["t" ; "s" ; "e" ; "i" ; "r" ; "igm"]]

    let line t z =
      let sof x = string_of_float (F.to_float x) in
      let _, s, e, i, r, igm = conv t z in
      [ sof t ; sof s ; sof e ; sof i ; sof r ; sof igm ]


    (* the only one we can use for now *)
    module Gill =
      struct
        let events par = event_sum par

        let next ?seed par =
          let ev = events par in
          Sim.Ctmjp.Gill.integrate ev (Util.rng seed)

        let sim ~output ?seed par =
          let next = next ?seed par in
          Sim.Loop.simulate_until ~next ~output
      end

    module Csv =
      struct
        let convert ?seed ?dt ~out par tf y0 =
          let par_chan = Csv.to_channel (open_out (out ^ ".par.csv")) in
          Csv.output_record par_chan par_header ;
          let pl = par_line ?seed ?dt par tf y0 in
          Csv.output_record par_chan pl ;
          Csv.close_out par_chan ;
          let chan = open_out (out ^ ".traj.csv") in
          let output = Sim.Csv.convert ?dt ~header ~line ~chan in
          let return tf yf =
            let ret = output.return tf yf in
            close_out chan ;
            ret
          in { output with return = return }
      end
  end


module Read =
  struct
    include Read

    module Reader' =
      struct
        include Reader

        let igm_loss () =
          req_pos "igm_loss" (fun th -> th#with_igm_loss)

        let igg_loss () =
          req_pos "igg_loss" (fun th -> th#with_igg_loss)

        let p_asympto = {
          column = "p_asympto" ;
          optional = false ;
          read = fun s th -> th#with_p_asympto (fpros s) ;
        }

        let p_seq = {
          column = "p_seq" ;
          optional = false ;
          read = fun s th -> th#with_p_seq (fpros s) ;
        }

        let panic () = [
          igm_loss () ;
          igg_loss () ;
          p_asympto ;
          p_seq ;
        ]

        let int_req column using = {
          column ;
          optional = false ;
          read = fun s th -> using th (int_of_string s) ;
        }

        module Y0 =
          struct
            let map_passive rdr =
              map
                ~get:(fun y0 -> y0.passive)
                ~set:(fun x y0 -> { y0 with passive = x })
                rdr

            let map_active rdr =
              map
                ~get:(fun y0 -> y0.active)
                ~set:(fun x y0 -> { y0 with active = x })
                rdr

            let map_sero rdr =
              map
                ~get:(fun y0 -> y0.sero)
                ~set:(fun x y0 -> { y0 with sero = x })
                rdr
   
            let with_s (seir : seir) s = { seir with s }

            let with_r (seir : seir) r = { seir with r }

            let s0 = map_passive
              (int_req "s0" with_s)

            let r0 = map_passive
              (int_req "r0" with_r)

            let active_s0 = map_active
              (int_req "active_s0" with_s)

            let active_r0 = map_active
              (int_req "active_r0" with_r)

            let sero_s0 = map_sero
              (int_req "sero_s0" (fun sero s -> { sero with s }))

            let sero_igm0 = map_sero
              (int_req "sero_igm0" (fun sero igm -> { sero with igm }))

            let sero_igg0 = map_sero
              (int_req "sero_igg0" (fun sero igg -> { sero with igg }))

            let sero_r0 = map_sero
              (int_req "sero_r0" (fun sero r -> { sero with r }))
          end


        let map_y0 rdr =
          map ~get:(fun th -> th#y0) ~set:(fun x th -> th#with_y0 x) rdr

        let y0 () = Y0.[
          map_y0 s0 ; map_y0 r0 ;
          map_y0 active_s0 ; map_y0 active_r0 ;
          map_y0 sero_s0 ; map_y0 sero_igm0 ; map_y0 sero_igg0 ; map_y0 sero_r0 ;
        ]
      end

    (*
    let igm_loss th row =
      th#with_igm_loss (ff "igm_loss" row)

    let igg_loss th row =
      th#with_igg_loss (ff "igg_loss" row)

    let p_asympto th row =
      th#with_p_asympto (ff "p_asympto" row)

    let p_seq th row =
      th#with_p_seq (ff "p_seq" row)


    let i_f = int_from

    let s0 (y0 : y0) row =
      let _, e, i, r = y0.passive in
      { y0 with passive = (i_f "s0" row, e, i, r) }

    let r0 (y0 : y0) row =
      let s, e, i, _ = y0.passive in
      { y0 with passive = (s, e, i, i_f "r0" row) }

    let active_s0 (y0 : y0) row =
        let _, e, i, r = y0.active in
        { y0 with active = (i_f "active_s0" row, e, i, r) }

    let active_r0 (y0 : y0) row =
        let s, e, i, _ = y0.active in
        { y0 with active = (s, e, i, i_f "active_r0" row) }

    let sero_s0 (y0 : y0) row =
        let _, e, i, igm, igg, r = y0.sero in
        { y0 with sero = (i_f "sero_s0" row, e, i, igm, igg, r) }

    let sero_igm0 (y0 : y0) row =
        let s, e, i, _, igg, r = y0.sero in
        { y0 with sero = (s, e, i, i_f "sero_igm0" row, igg, r) }

    let sero_igg0 (y0 : y0) row =
        let s, e, i, igm, _, r = y0.sero in
        { y0 with sero = (s, e, i, igm, i_f "sero_igg0" row, r) }

    let sero_r0 (y0 : y0) row =
        let s, e, i, igm, igg, _ = y0.sero in
        { y0 with sero = (s, e, i, igm, igg, i_f "sero_r0" row) }
    *)

  end


module Cli ( ) =
  struct
    module La = Lift_arg

    (* move to Param *)
    class with_y0 = object
      val y0_v = 
        let s = 0 in
        let e = [] in
        let i = [] in
        let igm = 0 in
        let igg = 0 in
        let r = 0 in
        {
        passive = { s ;  e ; i ; r } ;
        active = { s ; e ; i ; r } ;
        sero = { s ; e ; i ; igm ; igg ; r } ;
        }

      method y0 = y0_v

      method with_y0 (x : y0) = {< y0_v = x >}
    end

    class init = object
      inherit Param.t
      inherit with_y0
      inherit Param.tfable
      inherit Param.dtable
      inherit Param.seedable
    end


    let reads = Read.Reader'.(
        y0 ()
      @ [seed]
      @ panic ()
      @ mutating ()
      @ circular ()
      @ latent ()
      @ base ()
    )

    let load_par s x =
      Cli.load_par_from reads s x

    let out_r = ref None

    let init_r = ref (new init)

    let specs =
      La.specl_of init_r Specs.[
        s0 ; e0 ; i0 ; r0 ;
        active_s0 ; active_e0 ; active_i0 ; active_r0 ;
        sero_s0 ; sero_e0 ; sero_i0 ; sero_igm0 ; sero_igg0 ; sero_r0 ;
        dt ; tf ; seed ;
        (Specs.init_from load_par) ;
      ] @ (
        La.specl_of init_r (La.map
          (fun th -> th#with_gtr)
          (fun th -> th#gtr)
          Seqsim.Gtr.specl
        )
      ) @ (
        La.specl_of init_r Events.specs
      ) @ [
        La.spec_of out_r Specs.out_string
      ]

    let anon_fun s =
      Printf.printf "Ignored anonymous argument : %s" s

    let usage_msg =
        "Run a stochastic simulation of the PANIC three-cohort model,
         with Gillespie's algorithm."

    let main () =
      begin
        Arg.parse specs anon_fun usage_msg ;
        let init = !init_r in
        let seed = Param.seed init in
        let dt = Param.dt init in
        let tf = Param.tf init in
        let par = (init :> Param.t) in
        match !out_r with
        | None ->
            raise (Arg.Bad "missing mandatory argument '-out'.")
        | Some out ->
        let output = Events.Csv.convert ?seed ?dt ~out par tf init#y0 in
        let z0 = Events.of_tuples init#y0 in
        Events.Gill.sim ?seed ~output par z0 tf
      end
  end
