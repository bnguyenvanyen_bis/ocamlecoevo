(* Load a CSV file in the SEIRCO output format (see Seirco.Events(P).Out.line),
 * loads the "t", "c", and "coal" columns, and makes cadlags out of them,
 * regulates those, does the diff of "c" column
 * (so that the new column now contains number of cases in each interval),
 * then writes this to a new csv file.
 * By default the output file name is the input file name with ".traj.csv"
 * replaced by ".data.csv".
 *)

module J = Jump


let input_r = ref None


let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin. Also affects output") ;
]


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Create dataset from trajectory."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chan_cases, chan_neff =
      match !input_r with
      | None ->
          stdin,
          stdout,
          stdout
      | Some s ->
          open_in s,
          open_out (Data.cases_csv s),
          open_out (Data.neff_csv s)
    in
    (* we don't 'regulate' as it would translate everything by eps *)
    let traj = Data.load_traj chan_in in
    let cases =
      traj
      |> J.map (fun (c,_,_) -> c)
      |> Epi.Data.bin_cases
    in
    let neff =
      J.map (fun (_,_,coa) -> 1. /. coa) traj
    in
    Data.output_cases chan_cases cases ;
    Data.output_neff chan_neff neff ;
    close_out chan_cases ;
    (* FIXME does this work if both are stdout ? *)
    close_out chan_neff
  end;;


main ()
