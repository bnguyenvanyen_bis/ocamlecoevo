(* Load a CSV file in the SEIRCO output format (see Seirco.Events(P).Out.line),
 * loads the "t", "reportr", and "coal" columns, and makes cadlags out of them,
 * (so that the new column now contains expected number of cases in each interval),
 * then writes this to a new csv file.
 * By default the output file name is the input file name suffixed by "_data".
 *)

open Epi.Sig

module J = Jump


let input_r = ref None

let noisy_r = ref None
let seed_r = ref None

let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin. Also affects output.") ;
  ("-noisy", Arg.Float (fun x -> noisy_r := Some x),
   "Whether to make the data noisy, and with what overdispersion parameter.") ;
  ("-seed",Arg.Int (fun n -> seed_r := Some n),
   "Seed for drawing from the negative binomial distribution.") ;
]


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Create dataset from deterministic trajectory."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chan_cases, chan_neff =
      match !input_r with
      | None ->
          stdin,
          stdout,
          stdout
      | Some s ->
          open_in s,
          open_out (Data.cases_csv s),
          open_out (Data.neff_csv s)
    in
    let traj = Data.load_traj chan_in in
    begin match !noisy_r with
    | None ->
        traj
        |> J.map (fun (_,r,_) -> F.Pos.of_float r)
        |> Epi.Data.bin_reportr
        |> Data.output_reportr chan_cases
    | Some x ->
        let ovd = F.Pos.of_float (1. /. x) in
        let rng = Util.rng !seed_r in
        traj
        |> J.map (fun (_,r,_) -> F.Pos.of_float r)
        |> Epi.Data.bin_reportr
        |> J.map (Util.rand_negbin_over ~rng ovd)
        |> J.map Util.Int.to_int
        |> Data.output_cases chan_cases
    end ;
    let neff =
      J.map (fun (_,_,coa) -> 1. /. coa) traj
    in
    Data.output_neff chan_neff neff ;
    close_out chan_neff
  end;;


main ()
