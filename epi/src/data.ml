let cases_csv s =
  (* drops s's separator, and replaces the end by '.data.neff.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.cases.csv"


let neff_csv s =
  (* drops s's separator, and replaces the end by '.data.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.neff.csv"


let load_traj chan =
  let float_from row colname =
    float_of_string (Csv.Row.find row colname)
  in
  let read_row row =
    let t = float_from row "t" in
    let cas = int_of_float (float_from row "c") in
    let rep = float_from row "reportr" in
    let coa = float_from row "coal" in
    (t, (cas, rep, coa))
  in
  let csv_chan = Csv.of_channel ~separator:',' ~has_header:true chan in
  let csv = Csv.Rows.input_all csv_chan in
  List.map read_row csv


let output_cases chan data =
  let sof = string_of_float in
  let soi = string_of_int in
  let csv_chan = Csv.to_channel chan in
  Csv.output_record csv_chan ["t" ; "cases"] ;
  List.iter (fun (t, cases) ->
      Csv.output_record csv_chan [sof t ; soi cases]
  ) data


let output_reportr chan data =
  let sof = string_of_float in
  let sop = Util.Float.to_string in
  let csv_chan = Csv.to_channel chan in
  Csv.output_record csv_chan ["t" ; "cases"] ;
  List.iter (fun (t, binned_reportr) ->
      Csv.output_record csv_chan [sof t ; sop binned_reportr]
  ) data


let output_neff chan data =
  let sof = string_of_float in
  let csv_chan = Csv.to_channel chan in
  Csv.output_record csv_chan ["t" ; "neff"] ;
  List.iter (fun (t, neff) ->
      Csv.output_record csv_chan [sof t ; sof neff]
  ) data


