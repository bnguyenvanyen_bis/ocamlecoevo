module String = BatString

(* Merge data from traj with bayesian skyline data *)

let merge_csv s =
  let n = String.length s in
  let suffix = String.right s 9 in
  assert (suffix = ".data.csv") ;
  let s' = String.left s (n - 9) in
  s' ^ ".bsp.data.csv"

let from f row colname =
  f (Csv.Row.find row colname)

let ff = from float_of_string
let fi = from int_of_string

let load_data chan =
  let read_row row =
    let t = ff row "t" in
    let cases = fi row "cases" in
    let neff = ff row "neff" in
    (t, (cases, neff))
  in
  let csv = Csv.Rows.input_all chan in
  List.map read_row csv


let load_bsp chan =
  let header = Csv.Rows.header chan in
  (* get the times *)
  let neff_cols = List.filter (fun s ->
    (String.left s 1) = "t"
  ) header
  in
  let times = List.map (fun s ->
    Scanf.sscanf s "t%f" (fun t -> t)
  ) neff_cols
  in
  let read_row row =
    let k = fi row "k" in
    let neff = List.map2 (fun t c ->
      (t, ff row c)
    ) times neff_cols
    in (k, neff)
  in
  let csv = Csv.Rows.input_all chan in
  List.map read_row csv


let output_header chan bsp =
  let l = [ "t" ; "cases" ; "neff" ]
        @ (List.map (fun (k, _) -> Printf.sprintf "neff:%i" k) bsp)
  in Csv.output_record chan l


let output_row chan bsp (t, (cases, neff)) =
  let sof = string_of_float in
  let soi = string_of_int in
  let l = [ sof t ; soi cases ; sof neff ]
        @ (List.map (fun (_, neff_estim) -> sof (Jump.eval neff_estim t)) bsp)
  in Csv.output_record chan l


let in_bsp_r = ref None
let in_data_r = ref None
let out_r = ref None
let translate_r = ref 0.

let specl = [
  ("-in-bsp", Arg.String (fun s -> in_bsp_r := Some s),
   "Path to BSP input.") ;
  ("-in-data", Arg.String (fun s -> in_data_r := Some s),
   "Path to traj data input.") ;
  ("-out", Arg.String (fun s -> out_r := Some s),
   "Path to output file. Defaults to '{stripped-in-data}.bsp.csv'.") ;
  ("-translate", Arg.Set_float translate_r,
   "Amount of time to translate the BSP data timepoints by (added).") ;
]


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Merge traj data and BSP data and output to '.bsp.data.csv'."


let some =
  function
  | None ->
      raise (Arg.Bad "this argument needs a value")
  | Some s ->
      s


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_bsp =
      (Csv.of_channel ~has_header:true) @@ open_in @@ some @@ !in_bsp_r
    in
    let chan_data =
      (Csv.of_channel ~has_header:true) @@ open_in @@ some @@ !in_data_r
    in
    let chan_out =
      match !out_r with
      | Some s ->
          Csv.to_channel @@ open_out @@ s
      | None ->
          Csv.to_channel @@ open_out @@ merge_csv @@ some @@ !in_data_r
    in
    let data = load_data chan_data in
    let bsp = load_bsp chan_bsp in
    let dt = !translate_r in
    let bsp = List.map (fun (k, neffest) ->
      (k, List.map (fun (t, neff) -> (t +. dt, neff)) neffest)
    ) bsp
    in
    output_header chan_out bsp ;
    List.iter (output_row chan_out bsp) data ;
    Csv.close_in chan_bsp ;
    Csv.close_in chan_data ;
    Csv.close_out chan_out
  end;;


main ()
