open Epi
open Sig

module E = Sir.Events ( )

let main () =
  begin
    Gc.set { (Gc.get ()) with minor_heap_size = 4 * 1024 * 1024 ;
                              major_heap_increment = 30 } ;
    let z0 = Prm_sir.of_sir (5000, 10, 4990) in
    let z0' = Prm_sir.E.P.copy z0 in
    let seed = 1234 in
    let dt = F.Pos.of_float 0.9 in
    let par = Sir.default#with_beta 100. in
    let tf = F.one in
    let nrep = 100 in
    Printf.printf "Before Prm : %f\n%!" (Sys.time ()) ;
    Printf.printf "Avec Prm :\n" ;
    Prm_sir.simulate_until ~k:nrep ~seed ~dt par tf z0 ;
    Printf.printf "Before Gill : %f\n%!" (Sys.time ()) ;
    Printf.printf "Avec Gill :\n" ;
    for j = 1 to nrep do
      Printf.eprintf "j=%i at %f\n%!" j (Sys.time ()) ;
      E.Csv.simulate_until ~out:"test" ~seed ~dt par tf z0'
    done ;
    Printf.printf "After Gill : %f\n%!" (Sys.time ())
  end;;

main ()
