module F = Util.Float

module E = Epi.Process.Unit.Make (Epi.Sir.Model)


let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let header = Epi.Out.header in
  let line = E.line par in
  let z0 = E.of_sir (4, 4, 4) in
  let tf = F.Pos.of_float 0.1 in
  let output = Sim.Csv.convert ?dt:None ~header ~line ~chan:stdout in
  ignore (E.Gill.sim ~output ~seed:432 par z0 tf) ;
  [%expect{|
    t,s,e,i,r,c,o,n,reff,coal,reportr
    0.,4.,0.,4.,4.,0.,0.,12.,0.448717948718,5.83333333333,9.33333333333
    0.,4.,0.,4.,4.,0.,0.,12.,0.448717948718,5.83333333333,9.33333333333
    0.0111350487942,3.,0.,5.,4.,0.,0.,12.,0.336538461538,3.5,8.75
    0.0120208592262,3.,0.,4.,5.,0.,0.,12.,0.336538461538,4.375,7.
    0.0126296905347,3.,0.,3.,6.,0.,0.,12.,0.336538461538,5.83333333333,5.25
    0.0139220242788,3.,0.,2.,7.,0.,0.,12.,0.336538461538,8.75,3.5
    0.0293561557239,3.,0.,1.,8.,0.,0.,12.,0.336538461538,17.5,1.75
    0.0329187330235,3.,0.,0.,9.,0.,0.,12.,0.336538461538,inf,0. |}]


(* same but with data *)
let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let z0 = E.of_sir (10, 10, 10) in
  let tf = F.one in
  let dt = F.Pos.of_float 0.1 in
  let output = E.Cadlag.convert ~dt par in
  let traj = E.Gill.sim ~output ~seed:432 par z0 tf in
  Printf.printf "t,cases\n" ;
  traj
  |> Epi.Data.case_data
  |> List.iter (fun (t, c) -> Printf.printf "%f,%i\n" t c)
  ;
  [%expect{|
    t,cases
    0.100000,3
    1.000000,0 |}]
