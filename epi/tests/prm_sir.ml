open Epi
open Sig

module E = Process.Unit.Make (Sir.Model)


let setup () =
  let par = (new Param.t_unit)#with_host_birth 0. in
  let header = Out.header in
  let line = E.line par in
  let sir = (2, 4, 6) in
  let tf = F.Pos.of_float 0.1 in
  let time_range = (F.zero, tf) in
  let ntslices = I.Pos.of_int 2 in
  (* we need a good guess for ranges *)
  let h = F.Pos.Op.(tf / I.Pos.to_float ntslices) in
  let colors = E.Prm.colors in
  let vectors = Point.vectors ~h colors in
  let rngm = Prm_time.rngm ~rng:(U.rng (Some 1234)) colors in
  let prm = Prm_time.rand_draw ~rngm ~time_range ~ntslices ~vectors in
  let sim = E.Prm.sim par in
  (header, line, sim, sir, prm)


let%expect_test _ =
  let header, line, sim, sir, prm = setup () in
  let output = Sim.Csv.convert ?dt:None ~header ~line ~chan:stdout in
  ignore (sim ~output (E.of_sir sir) prm) ;
  [%expect{|
    Accepted points : 6 out of 34
    t,s,e,i,r,c,o,n,reff,coal,reportr
    0.,2.,0.,4.,6.,0.,0.,12.,0.224358974359,2.91666666667,4.66666666667
    0.00332912028516,2.,0.,4.,6.,0.,0.,12.,0.224358974359,2.91666666667,4.66666666667
    0.00448414669226,1.,0.,5.,6.,1.,0.,12.,0.112179487179,1.16666666667,2.91666666667
    0.00490555821781,1.,0.,4.,7.,1.,0.,12.,0.112179487179,1.45833333333,2.33333333333
    0.00597338689993,1.,0.,3.,8.,1.,0.,12.,0.112179487179,1.94444444444,1.75
    0.0127954029433,1.,0.,2.,9.,1.,0.,12.,0.112179487179,2.91666666667,1.16666666667
    0.0147542676251,1.,0.,1.,10.,1.,0.,12.,0.112179487179,5.83333333333,0.583333333333 |}]


(* Check that hiding (and revealing) sort of work. *)
let%expect_test _ =
  let _, _, sim, sir, prm = setup () in
  let output = Util.Out.convert_null in
  ignore (sim ~output (E.of_sir sir) prm) ;
  ignore (sim ~output (E.of_sir sir) prm) ;
  ignore (sim ~output (E.of_sir sir) prm) ;
  [%expect{|
    Accepted points : 6 out of 34
    Accepted points : 6 out of 34
    Accepted points : 6 out of 34 |}]
