module F = Util.Float

module Om = Epi.Process.Ode.Make (Epi.Sir.Model)


let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let header = Epi.Out.header in
  let line = Om.line par in
  let x = F.of_float 4. in
  let z0 = Om.of_sir (x, x, x) in
  let tf = F.Pos.of_float 0.1 in
  let dt = F.Pos.of_float 0.02 in
  let output = Sim.Csv.convert ~dt ~header ~line ~chan:stdout in
  ignore (Om.sim ~output par z0 z0 tf) ;
  [%expect {|
    t,s,e,i,r,c,o,n,reff,coal,reportr
    0.,4.,0.,4.,4.,0.,0.,12.,0.448717948718,5.83333333333,9.33333333333
    0.02,2.82775336531,0.,2.0814173602,7.08818414958,0.,0.,11.9973548751,0.317285860785,7.92674505185,3.43410235367
    0.04,2.37953685368,0.,0.992622490757,8.62361737104,0.,0.,11.9957767155,0.267029202329,13.9887204354,1.37830779798
    0.06,2.19436067533,0.,0.457308009389,9.34306292829,0.,0.,11.994731613,0.246270375882,28.0031385477,0.585631359903
    0.08,2.11421801968,0.,0.207633413262,9.67208538651,0.,0.,11.9939368195,0.23729178848,59.427684625,0.256202460702 |}]



let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let x = F.of_float 4. in
  let z0 = Om.of_sir (x, x, x) in
  let tf = F.Pos.of_float 0.1 in
  let dt = F.Pos.of_float 0.02 in
  let output = Om.Cadlag.convert ~dt par in
  let traj = Om.sim ~output par z0 z0 tf in
  Printf.printf "t,cases\n" ;
  traj
  |> Epi.Data.binned_reportr_data
  |> List.iter (fun (t, r) -> Printf.printf "%f,%f\n" t (F.to_float r))
  ;
  [%expect{|
    t,cases
    0.020000,0.186667
    0.040000,0.068682
    0.060000,0.027566
    0.080000,0.011713
    0.100000,0.005124 |}]
